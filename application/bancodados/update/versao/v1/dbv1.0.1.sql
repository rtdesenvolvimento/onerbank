#v1
alter table lancamentos add column `fornecedor_id`              INT(11)       NULL     DEFAULT NULL;
alter table produtos_os add column `data_produzido_conclusao`    DATE         NULL;
alter table produtos_os add column `hora_produzido_conclusao`    TIME         NULL;
alter table servicos_os add column `data_produzido_conclusao`    DATE         NULL;
alter table servicos_os add column `hora_produzido_conclusao`    TIME         NULL;


#v2
ALTER TABLE clientes  ADD COLUMN `inativo`   INT(11)  NOT NULL DEFAULT 0;
ALTER TABLE clientes  ADD COLUMN `observacaoInativo`  LONGTEXT  NULL     DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `produto_filial` (
  `idProdutoFilial`  INT(11)        NOT NULL AUTO_INCREMENT,
  `observacao`       longtext default null ,
  `localizacao`      longtext default null ,
  `estoque`          double      NOT NULL default 0,
  `reservado`        double      NOT NULL default 0,
  `bloqueado`        double      NOT NULL default 0,
  `estoqueMinimo`    double      NOT NULL default 0,
  `precoVenda`       double      NOT NULL default 0,
  `precoCompra`      double      NOT NULL default 0,
  `filial_id`        INT(11)     NOT NULL,
  `produto_id`       INT(11)     NOT NULL,
  PRIMARY KEY (`idProdutoFilial`),
  FOREIGN KEY (`filial_id`)
  REFERENCES `filial` (`idFilial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY (`produto_id`)
  REFERENCES `produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  AUTO_INCREMENT = 2
  DEFAULT CHARACTER SET = latin1;


CREATE TABLE IF NOT EXISTS `historico_estoque` (
  `idHistoricoEstoque`  INT(11)        NOT NULL AUTO_INCREMENT,
  `observacao`          longtext default null ,
  `origem`              longtext default null ,
  `tipo`                varchar(300) default null ,
  `estoque`             double      NOT NULL default 0,
  `precoVenda`          double      NOT NULL default 0,
  `precoCompra`         double      NOT NULL default 0,
  `filial_id`           INT(11)     NOT NULL,
  `produto_id`          INT(11)     NOT NULL,
  `venda_id`            INT(11)     NULL,
  `item_venda_id`       INT(11)     NULL,
  `transferencia_id`    INT(11)     NULL,
  `item_transferencia_id`   INT(11)     NULL,
  `pedido_id`           INT(11)     NULL,
  `item_pedido_id`      INT(11)     NULL,
  `inventario_id`       INT(11)     NULL,
  `item_iventario_id`   INT(11)     NULL,
  `os_id`               INT(11)     NULL,
  `item_os_id`          INT(11)     NULL,
  `data`                date NULL DEFAULT NULL,
  `hora`                time NULL DEFAULT NULL,
  PRIMARY KEY (`idHistoricoEstoque`),
  FOREIGN KEY (`filial_id`)
  REFERENCES `filial` (`idFilial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY (`produto_id`)
  REFERENCES `produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  AUTO_INCREMENT = 2
  DEFAULT CHARACTER SET = latin1;


#v3