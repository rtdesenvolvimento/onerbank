CREATE TABLE IF NOT EXISTS `forma_pagamento` (
  `idFormaPagamento`     INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                    VARCHAR(45)    NOT NULL,
  `descricao`               VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`               INT(11)      NOT NULL,
  `tipo`        TINYINT(1)  NULL,
  PRIMARY KEY (`idFormaPagamento`)
)
  AUTO_INCREMENT = 1
  DEFAULT CHARACTER SET = latin1;


INSERT INTO `forma_pagamento` (`idFormaPagamento`, `nome`,`descricao`,`filial_id`)
VALUES
  (1, 'DINHEIRO', 'DINHEIRO' , 1),
  (2, 'DEPOSITO EM CONTA', 'DEPOSITO EM CONTA' , 1),
  (3, 'CHEQUE', 'CHEQUE' , 1),
  (4, 'CARTÃO DE DÉBITO', 'CARTÃO DE DÉBITO' , 1),
  (5, 'CARTÃO DE CRÉDITO', 'CARTÃO DE CRÉDITO' , 1);



CREATE TABLE IF NOT EXISTS `condicao_pagamento` (
  `idCondicaoPagamento`     INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                    VARCHAR(45)    NOT NULL,
  `descricao`               VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`               INT(11)      NOT NULL,
  PRIMARY KEY (`idCondicaoPagamento`)
)
  AUTO_INCREMENT = 1
  DEFAULT CHARACTER SET = latin1;


INSERT INTO `condicao_pagamento` (`idCondicaoPagamento`, `nome`,`descricao`,`filial_id`)
VALUES
  (1, 'A Vista', 'A Vista' , 1);


CREATE TABLE IF NOT EXISTS `despesa` (
  `idDespesa`               INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                    VARCHAR(45)    NOT NULL,
  `codigo`                  VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`               INT(11)      NOT NULL,
  `despesaSuperior_id`      INT(11)      NOT NULL,
  PRIMARY KEY (`idDespesa`)
)
  AUTO_INCREMENT = 1
  DEFAULT CHARACTER SET = latin1;


INSERT INTO `despesa` (`idDespesa`, `nome`,`codigo`,`filial_id`)
VALUES (1, 'Compras de mercadoria', '1.0.0' , 1);


CREATE TABLE IF NOT EXISTS `receita` (
  `idReceita`               INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                    VARCHAR(45)    NOT NULL,
  `codigo`                  VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`               INT(11)      NOT NULL,
  `receitaSuperior_id`      INT(11)      NOT NULL,
  PRIMARY KEY (`idReceita`)
)
  AUTO_INCREMENT = 1
  DEFAULT CHARACTER SET = latin1;


INSERT INTO `receita` (`idReceita`, `nome`,`codigo`,`filial_id`)
VALUES (1, 'Ordem de serviço', '1.0.0' , 1);


CREATE TABLE IF NOT EXISTS `pedido` (
  `idPedido`        INT         NOT NULL AUTO_INCREMENT,
  `dataPedido`      DATE        NOT NULL,
  `previsaoEntrega` DATE        NULL,
  `valorTotal`      double default 0,
  `valorFrete`      double default 0,
  `desconto`        double default 0,
  `faturado`        TINYINT(1)  NULL,
  `fornecedor_id`   INT(11)     NOT NULL,
  `usuarios_id`     INT(11)     NULL,
  `lancamentos_id`  INT(11)     NULL,
  `filial_id`       INT(11)      NOT NULL,
  `condicao_pagamento_id`       INT(11)  NULL,
  `forma_pagamento_id`       INT(11)  NULL,
  `status`          VARCHAR(45)  NULL     DEFAULT 'Aberto',
  `observacao`      longtext  DEFAULT NULL,
  `contatoEntrega`               VARCHAR(70)  NULL,
  `cepEntrega`               VARCHAR(70)  NULL,
  `ruaEntrega`               VARCHAR(70)  NULL,
  `numeroEntrega`            VARCHAR(15)  NULL,
  `bairroEntrega`            VARCHAR(45)  NULL,
  `cidadeEntrega`            VARCHAR(45)  NULL,
  `estadoEntrega`            VARCHAR(20)  NULL,
  `telefoneEntrega`          VARCHAR(20)  NULL,
  `emailEntrega`             VARCHAR(255) NULL,
  `complementoEntrega`       LONGTEXT NULL,
  PRIMARY KEY (`idPedido`),
  INDEX `fk_pedido_fornecedor1` (`fornecedor_id` ASC),
  INDEX `fk_pedido_usuarios1` (`usuarios_id` ASC),
  INDEX `fk_pedido_lancamentos1` (`lancamentos_id` ASC),
  INDEX `fk_pedido_condicao_pagamento1` (`condicao_pagamento_id` ASC),
  INDEX `fk_pedido_forma_pagamento1` (`forma_pagamento_id` ASC),
  CONSTRAINT `fk_pedido_clientes1`
  FOREIGN KEY (`fornecedor_id`)
  REFERENCES `fornecedor` (`idFornecedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedido_usuarios1`
  FOREIGN KEY (`usuarios_id`)
  REFERENCES `usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedido_lancamentos1`
  FOREIGN KEY (`lancamentos_id`)
  REFERENCES `lancamentos` (`idLancamentos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedido_condicao_pagamento1`
  FOREIGN KEY (`condicao_pagamento_id`)
  REFERENCES `condicao_pagamento` (`idCondicaoPagamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedido_forma_pagamento1`
  FOREIGN KEY (`forma_pagamento_id`)
  REFERENCES `forma_pagamento` (`idFormaPagamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `itensPedido` (
  `idItens`     INT    NOT NULL AUTO_INCREMENT,
  `subTotal`    double default   0,
  `custo`       double NULL     DEFAULT 0,
  `status`      VARCHAR(45)  NULL     DEFAULT NULL,
  `quantidade`  INT(11)        NULL,
  `pedido_id`   INT            NOT NULL,
  `produtos_id` INT(11)        NOT NULL,
  `filial_id`   INT(11)      NOT NULL,
  PRIMARY KEY (`idItens`),
  INDEX `fk_itensPedido_vendas1` (`pedido_id` ASC),
  INDEX `fk_itensPedido_produtos1` (`produtos_id` ASC),
  CONSTRAINT `fk_itensPedido_vendas1`
  FOREIGN KEY (`pedido_id`)
  REFERENCES `pedido` (`idPedido`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_itensPedido_produtos1`
  FOREIGN KEY (`produtos_id`)
  REFERENCES `produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


ALTER TABLE lancamentos ADD COLUMN `pedido` INT(11)        NULL     DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `transferencia` (
  `idTransferencia`        INT         NOT NULL AUTO_INCREMENT,
  `dataTransferencia`     DATE        NOT NULL,
  `previsaoEntrega`       DATE        NULL,
  `usuarios_id`           INT(11)     NULL,
  `lancamentos_id`        INT(11)     NULL,
  `filial_id`             INT(11)      NOT NULL,
  `filial_destino_id`     INT(11)      NOT NULL,
  `status`                VARCHAR(45)  NULL     DEFAULT 'Aberto',
  `observacao`            longtext  DEFAULT NULL,
  `contatoEntrega`          VARCHAR(70)  NULL,
  `cepEntrega`               VARCHAR(70)  NULL,
  `ruaEntrega`               VARCHAR(70)  NULL,
  `numeroEntrega`            VARCHAR(15)  NULL,
  `bairroEntrega`            VARCHAR(45)  NULL,
  `cidadeEntrega`            VARCHAR(45)  NULL,
  `estadoEntrega`            VARCHAR(20)  NULL,
  `telefoneEntrega`          VARCHAR(20)  NULL,
  `emailEntrega`             VARCHAR(255) NULL,
  `complementoEntrega`       LONGTEXT NULL,
  PRIMARY KEY (`idTransferencia`),
  INDEX `fk_transferencia_usuarios1` (`usuarios_id` ASC),
  INDEX `fk_transferencia_lancamentos1` (`lancamentos_id` ASC),
  INDEX `fk_transferencia_filial_destino1` (`filial_destino_id` ASC),
  CONSTRAINT `fk_transferencia_usuarios1`
  FOREIGN KEY (`usuarios_id`)
  REFERENCES `usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transferencia_lancamentos1`
  FOREIGN KEY (`lancamentos_id`)
  REFERENCES `lancamentos` (`idLancamentos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transferencia_filial_destino1`
  FOREIGN KEY (`filial_destino_id`)
  REFERENCES `filial` (`idFilial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `itensTransferencia` (
  `idItens`     INT    NOT NULL AUTO_INCREMENT,
  `subTotal`    double default   0,
  `custo`       double NULL     DEFAULT 0,
  `status`      VARCHAR(45)  NULL     DEFAULT NULL,
  `quantidade`  INT(11)        NULL,
  `transferencia_id`   INT            NOT NULL,
  `produtos_id` INT(11)        NOT NULL,
  `filial_id`   INT(11)      NOT NULL,
  PRIMARY KEY (`idItens`),
  INDEX `fk_itensTransferencia_transferencia1` (`transferencia_id` ASC),
  INDEX `fk_itensTransferncia_produtos1` (`produtos_id` ASC),
  CONSTRAINT `fk_itensTransferencia_transferencia1`
  FOREIGN KEY (`transferencia_id`)
  REFERENCES `transferencia` (`idTransferencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_itensTransferencia_produtos1`
  FOREIGN KEY (`produtos_id`)
  REFERENCES `produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `inventario` (
  `idInventario`       INT         NOT NULL AUTO_INCREMENT,
  `dataInventario`     DATE        NOT NULL,
  `usuarios_id`        INT(11)     NULL,
  `filial_id`          INT(11)      NOT NULL,
  `status`             VARCHAR(45)  NULL     DEFAULT 'Aberto',
  `observacao`         longtext  DEFAULT NULL,
  PRIMARY KEY (`idInventario`),
  INDEX `fk_inventario_usuarios1` (`usuarios_id` ASC),
  CONSTRAINT `fk_inventario_usuarios1`
  FOREIGN KEY (`usuarios_id`)
  REFERENCES `usuarios` (`idUsuarios`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `itensInventario` (
  `idItens`     INT    NOT NULL AUTO_INCREMENT,
  `subTotal`    double default   0,
  `custo`       double NULL     DEFAULT 0,
  `status`      VARCHAR(45)  NULL     DEFAULT NULL,
  `quantidade`  INT(11)        NULL,
  `inventario_id`   INT            NOT NULL,
  `produtos_id` INT(11)        NOT NULL,
  `filial_id`   INT(11)      NOT NULL,
  PRIMARY KEY (`idItens`),
  INDEX `fk_itensInventario_inventario1` (`inventario_id` ASC),
  INDEX `fk_itensInventario_produtos1` (`produtos_id` ASC),
  CONSTRAINT `fk_itensInventario_inventario1`
  FOREIGN KEY (`inventario_id`)
  REFERENCES `inventario` (`idInventario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_itensInventario_produtos1`
  FOREIGN KEY (`produtos_id`)
  REFERENCES `produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

ALTER TABLE veiculos add column  `municipio`  VARCHAR(300);
ALTER TABLE veiculos add column  `uf`         VARCHAR(80);
ALTER TABLE veiculos add column  `situacao`   VARCHAR(80);

ALTER TABLE itensInventario  ADD COLUMN   `venda`               double NULL     DEFAULT 0;