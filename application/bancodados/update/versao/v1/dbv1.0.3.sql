
alter table clientes add column `origem` VARCHAR(100);

alter table veiculos add column `gasolina` VARCHAR(80);
alter table veiculos add column `etanol` VARCHAR(80);
alter table veiculos add column `diesel` VARCHAR(80);
alter table veiculos add column `gnv` VARCHAR(80);

ALTER TABLE os CHANGE veiculo veiculo_id INT(11) NULL DEFAULT 0;

ALTER TABLE os add column   `gnv`               VARCHAR(15)      NULL;
ALTER TABLE os add column   `tanque`            VARCHAR(15)      NULL;

CREATE TABLE IF NOT EXISTS `arquivos_veiculo` (
  `idFotoVeiculo` INT          NOT NULL AUTO_INCREMENT,
  `anexo`         VARCHAR(70)  NULL,
  `observacaoimg` TEXT         NULL,
  `path`          VARCHAR(300) NULL,
  `url`           VARCHAR(300) NULL,
  `data`          DATE NOT NULL ,
  `hora`          TIME NOT NULL ,
  `filial_id`     INT(11)      NOT NULL,
  `veiculos_id`   INT(11)      NOT NULL,
  PRIMARY KEY (`idFotoVeiculo`)
);


alter table arquivos_veiculo add column   `thumb`  VARCHAR(45)  NULL;