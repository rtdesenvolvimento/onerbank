CREATE TABLE IF NOT EXISTS `configuracao` (
  `idConfiguracao`                            INT(11)         NOT NULL AUTO_INCREMENT,
  `serieRps`                                  VARCHAR(120)    NULL     DEFAULT NULL,
  `numeroProximoNumeroRps`                    VARCHAR(120)    NULL     DEFAULT NULL,
  `naturezaOperacaoServicoPadrao`             VARCHAR(150)    NULL     DEFAULT NULL,
  `isCalcularValorISS`                        TINYINT(1)      NOT NULL DEFAULT 2,
  `isReterISS`                                TINYINT(1)      NOT NULL DEFAULT 2,
  `aliquotaIR`                                DECIMAL(10, 2)  NULL     DEFAULT 0,
  `aliquotaINSS`                              DECIMAL(10, 2)  NULL     DEFAULT 11,
  `aliquotaCofins`                            DECIMAL(10, 2)  NULL     DEFAULT 3,
  `aliquotaPIS`                               DECIMAL(10, 2)  NULL     DEFAULT 0.65,
  `aliquotaContribuicaoSocial`                DECIMAL(10, 2)  NULL     DEFAULT 1,
  `textoPadraoIR`                             VARCHAR(150)    NULL     DEFAULT '( - ) IRenda Fonte 1,5%',
  `textoPadraoIRISENTO`                       VARCHAR(150)    NULL     DEFAULT 'IR Isento Cfe. Lei nro. 9430/96 Art.64',
  `descontarIRValor`                          TINYINT(1)      NOT NULL DEFAULT 2,
  `valorMinimoCalculoIR`                      DECIMAL(10, 2)  NULL     DEFAULT 0,
  `reterCSLLPISCONFINSNotaAcima5000`          TINYINT(1)      NOT NULL DEFAULT 2,
  `reterCSLLPISCONFINSSomaImpostosAcima`      DECIMAL(10, 2)  ,
  `isReterINSS`                               TINYINT(1)      NOT NULL DEFAULT 2,
  `isUsarAliquotaTabelaIBPT`                  TINYINT(1)      NOT NULL DEFAULT 1,
  `aliquotaAproximadaImpostosNotaServico`     DECIMAL(10, 2)  NULL     DEFAULT 0,
  `isEnviarBoletoContaPagarEmailNotaServico`  TINYINT(1)      NOT NULL DEFAULT 2,
  `textoPadraoAssuntoEmailNotaServico`        VARCHAR(350)    NULL     DEFAULT '( - ) IRenda Fonte 1,5%',
  `textoPadraoCorpoEmailNotaServico`          VARCHAR(9999)  NULL     DEFAULT '<p>Segue o link para acesso a NFS-e online. Qualquer d&uacute;vida entrar em contato.</p>',
  `filial_id`         INT(11)         NOT NULL,
PRIMARY KEY (`idConfiguracao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `configuracao_tecnospeed` (
  `idConfiguracaotecnospeed`      INT(11)        NOT NULL AUTO_INCREMENT,
  `descricao`                     VARCHAR(45)    NOT NULL,
  `grupo`                         VARCHAR(45)    NOT NULL,
  `usuario`                       VARCHAR(45)    NOT NULL,
  `senha`                         VARCHAR(45)    NOT NULL,
  `filial_id`                     INT(11)        NOT NULL,
PRIMARY KEY (`idConfiguracaotecnospeed`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

alter table servicos add column  isISS             TINYINT(1)     NULL;
alter table servicos add column  codigoServico     VARCHAR(45)    NOT NULL;
alter table servicos add column  aliquotaISS       DECIMAL(10, 2) NULL     DEFAULT 0;
alter table servicos add column  cnae              VARCHAR(45)    NOT NULL;

alter table emitente add column inscst varchar (50);
alter table emitente add column insmun varchar (50);
alter table emitente add column nomeFanstasia varchar (150);
alter table emitente add column cep varchar (50);
alter table emitente add column complemento varchar (120);
alter table emitente add column cnae varchar (50);
alter table emitente add column regimeTributario  INT NOT NULL default 1;
alter table emitente add column csc  varchar (120);
alter table emitente add column idcsc  varchar (120);
alter table emitente add column tokenibpt varchar (255);
alter table emitente add column ambiente INT NOT NULL default 2;
alter table emitente add column certificado varchar (2) default 'A1';
alter table emitente add column configuracao_tecnospeed_id INT(11);
alter table emitente add column configuracao_id INT(11);
alter table emitente add column codigoCidade varchar(50);
alter table emitente add column dddTelefone varchar(50);
alter table emitente add column regimeEspecialTributacao  INT NOT NULL default 1;
alter table emitente add column cMun varchar(50);
alter table emitente add column cert varchar(150);
alter table emitente add column certsenha varchar(150);
alter table emitente add column sistema_operacional INT NOT NULL default 1;

alter table produtos add column cProd VARCHAR(80) NULL;
alter table produtos add column cEAN VARCHAR(80) NULL;
alter table produtos add column CFOP VARCHAR(80) NULL;
alter table produtos add column CEST VARCHAR(80) NULL;
alter table produtos add column origem VARCHAR(80) NULL;

alter table clientes add column codIBGECidade VARCHAR(80) NULL;
alter table clientes add column codIBGEEstado VARCHAR(80) NULL;
alter table clientes add column IE VARCHAR(80) NULL;
alter table clientes add column IESUF VARCHAR(80) NULL;
alter table clientes add column IEST VARCHAR(80) NULL;
alter table clientes add column IEMunicipal VARCHAR(80) NULL;

alter table os add column nfe_gerada  TINYINT(1)  NOT NULL DEFAULT  0;
alter table os add column nfce_gerada TINYINT(1)  NOT NULL DEFAULT  0;
alter table os add column nfse_gerada TINYINT(1)  NOT NULL DEFAULT  0;

alter table os add column nNFE int(11)  NOT NULL DEFAULT  0;
alter table os add column nNFSE int(1)  NOT NULL DEFAULT  0;
