CREATE TABLE IF NOT EXISTS `parcela` (
  `idParcela`          INT(11)        NOT NULL AUTO_INCREMENT,
  `status`             VARCHAR(255)   DEFAULT 'ABERTA',
  `numeroParcela`      INT(11)        NULL     DEFAULT 0,
  `totalParcelas`      INT(11)        NULL     DEFAULT 0,
  `valorVencimento`    double         DEFAULT 0,
  `acrescimo`          double         DEFAULT 0,
  `desconto`           double         DEFAULT 0,
  `valorPago`          double         DEFAULT 0,
  `valorPagar`         double         DEFAULT 0,
  `dtVencimento`       DATE           NOT NULL,
  `dtUltimoPagamento`  DATE           NULL     DEFAULT NULL,
  `faturada`           TINYINT(1)     NULL     DEFAULT 0,
  `lancamentos_id`     INT(11)        NULL     DEFAULT NULL,
  `filial_id`          INT(11)        NOT NULL,
PRIMARY KEY (`idParcela`),
INDEX `fk_parcela_lancamentos` (`lancamentos_id` ASC),
CONSTRAINT `fk_parcela_lancamentos`
FOREIGN KEY (`lancamentos_id`)
REFERENCES `lancamentos` (`idLancamentos`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `fatura` (
  `idFatura`           INT(11)        NOT NULL AUTO_INCREMENT,
  `codigo`             VARCHAR(255)   DEFAULT '',
  `status`             VARCHAR(255)   DEFAULT 'ABERTA',
  `numeroControle`     VARCHAR(255)   DEFAULT '',
  `strPlanoContas`     VARCHAR(9999)  DEFAULT '',
  `tipoOperacao`       VARCHAR(255)   ,
  `totalCredito`       double         DEFAULT 0,
  `totalDebito`        double         DEFAULT 0,
  `valorFatura`        double         DEFAULT 0,
  `totalAcrescimo`     double         DEFAULT 0,
  `totalDesconto`      double         DEFAULT 0,
  `valorPago`          double         DEFAULT 0,
  `valorPagar`         double         DEFAULT 0,
  `multa`              double         DEFAULT 0,
  `percentualMulta`    double         DEFAULT 0,
  `mora`               double         DEFAULT 0,
  `percentualMora`     double         DEFAULT 0,
  `dtFaturamento`      DATE           NULL     DEFAULT NULL,
  `dtVencimento`       DATE           NULL     DEFAULT NULL,
  `dtUltimoPagamento`  DATE           NULL     DEFAULT NULL,
  `credito`            TINYINT(1)     NULL     DEFAULT 0,
  `clientes_id`        INT(11)        NULL     DEFAULT NULL,
  `fornecedor_id`      INT(11)        NULL     DEFAULT NULL,
  `filial_id`          INT(11)        NOT NULL,
PRIMARY KEY (`idFatura`),
INDEX `fk_fatura_clientes` (`clientes_id` ASC),
CONSTRAINT `fk_fatura_clientes`
FOREIGN KEY (`clientes_id`)
REFERENCES `clientes` (`idClientes`),
INDEX `fk_fatura_fornecedor` (`fornecedor_id` ASC),
CONSTRAINT `fk_fatura_fornecedor`
FOREIGN KEY (`fornecedor_id`)
REFERENCES `fornecedor` (`idFornecedor`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `fatura_parcela` (
  `idFaturaParcela`    INT(11)        NOT NULL AUTO_INCREMENT,
  `parcela_id`         INT(11)        NOT NULL,
  `fatura_id`          INT(11)        NOT NULL,
  `filial_id`          INT(11)        NOT NULL,
PRIMARY KEY (`idFaturaParcela`),
INDEX `fk_fatura_parcela_parcela` (`parcela_id` ASC),
CONSTRAINT `fk_fatura_parcela_parcela`
FOREIGN KEY (`parcela_id`)
REFERENCES `parcela` (`idParcela`),
INDEX `fk_fatura_parcela_fatura` (`fatura_id` ASC),
CONSTRAINT `fk_fatura_parcela_fatura`
FOREIGN KEY (`fatura_id`)
REFERENCES `fatura` (`idFatura`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `conta_bancaria` (
  `idContaBancaria`      INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                 VARCHAR(45)    NOT NULL,
  `descricao`            VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`            INT(11)      NOT NULL,
  `tipo`                 TINYINT(1)  NULL,
PRIMARY KEY (`idContaBancaria`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `pagamento` (
  `idPagamento`        INT(11)        NOT NULL AUTO_INCREMENT,
  `status`             VARCHAR(255)   DEFAULT 'ABERTA',
  `observacao`         VARCHAR(9999)  DEFAULT '',
  `strPlanoContas`     VARCHAR(9999)  DEFAULT '',
  `valor`              double         DEFAULT 0,
  `dtPagamento`        DATE           NOT NULL,
  `hrPagamento`        TIME           NOT NULL,
  `credito`            TINYINT(1)     NULL     DEFAULT 0,
  `conciliado`         TINYINT(1)     NULL     DEFAULT 0,
  `dtConciliacao`      DATE           NOT NULL,
  `fatura_id`          INT(11)        NOT NULL,
  `clientes_id`        INT(11)        NULL     DEFAULT NULL,
  `fornecedor_id`      INT(11)        NULL     DEFAULT NULL,
  `formaPagamento_id`  INT(11)        NULL     DEFAULT NULL,
  `conta_bancaria_id`  INT(11)        NULL     DEFAULT NULL,
  `caixa_id`           INT(11)        NULL     DEFAULT NULL,
  `filial_id`          INT(11)        NOT NULL,
PRIMARY KEY (`idPagamento`),
INDEX `fk_pagamento_formaPagamento` (`formaPagamento_id` ASC),
CONSTRAINT `fk_pagamento_formaPagamento`
FOREIGN KEY (`formaPagamento_id`)
REFERENCES `forma_pagamento` (`idFormaPagamento`),
INDEX `fk_pagamento_caixa` (`caixa_id` ASC),
CONSTRAINT `fk_pagamento_caixa`
FOREIGN KEY (`caixa_id`)
REFERENCES `caixa` (`idCaixa`),
INDEX `fk_pagamento_contaBancaria` (`conta_bancaria_id` ASC),
CONSTRAINT `fk_pagamento_contaBancaria`
FOREIGN KEY (`conta_bancaria_id`)
REFERENCES `conta_bancaria` (`idContaBancaria`),
INDEX `fk_pagamento_clientes` (`clientes_id` ASC),
CONSTRAINT `fk_pagamento_clientes`
FOREIGN KEY (`clientes_id`)
REFERENCES `clientes` (`idClientes`),
INDEX `fk_pagamento_fornecedor` (`fornecedor_id` ASC),
CONSTRAINT `fk_pagamento_fornecedor`
FOREIGN KEY (`fornecedor_id`)
REFERENCES `fornecedor` (`idFornecedor`),
INDEX `fk_pagamento_fatura` (`fatura_id` ASC),
CONSTRAINT `fk_pagamento_fatura`
FOREIGN KEY (`fatura_id`)
REFERENCES `fatura` (`idFatura`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `movimentador_saldo` (
  `idMovimentadorSaldo` INT(11)        NOT NULL AUTO_INCREMENT,
  `saldo`               double         DEFAULT 0,
  `caixa_id`            INT(11)        NOT NULL,
  `contaBancaria_id`    INT(11)        NOT NULL,
  `formaPagamento_id`   INT(11)        NOT NULL,
  `filial_id`           INT(11)        NOT NULL,
PRIMARY KEY (`idMovimentadorSaldo`),
INDEX `fk_movimentador_saldo_contaBancaria` (`contaBancaria_id` ASC),
CONSTRAINT `fk_movimentador_saldo_contaBancaria`
FOREIGN KEY (`contaBancaria_id`)
REFERENCES `conta_bancaria` (`idContaBancaria`),
INDEX `fk_movimentador_saldo_caixa` (`caixa_id` ASC),
CONSTRAINT `fk_movimentador_saldo_caixa`
FOREIGN KEY (`caixa_id`)
REFERENCES `caixa` (`idCaixa`),
INDEX `fk_movimentador_saldo_formaPagamento` (`formaPagamento_id` ASC),
CONSTRAINT `fk_movimentador_saldo_formaPagamento`
FOREIGN KEY (`formaPagamento_id`)
REFERENCES `forma_pagamento` (`idFormaPagamento`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `boleto` (
  `idBoleto`      INT(11)        NOT NULL AUTO_INCREMENT,
  `cliente_id`    INT(11)        NOT NULL,
  `fatura_id`     INT(11)        NOT NULL,
  `url_boleto`                      LONGTEXT,
  `transactions_integration_id`     LONGTEXT,
  `transactions_status`             LONGTEXT,
  `transactions_status_boleto`      LONGTEXT,
  `barcode`                      LONGTEXT,
  `filial_id`     INT(11)        NOT NULL,
PRIMARY KEY (`idBoleto`),
INDEX `fk_boleto_cliente` (`cliente_id` ASC),
CONSTRAINT `fk_boleto_cliente`
FOREIGN KEY (`cliente_id`)
REFERENCES `clientes` (`idClientes`),
INDEX `fk_boleto_fatura` (`fatura_id` ASC),
CONSTRAINT `fk_boleto_fatura`
FOREIGN KEY (`fatura_id`)
REFERENCES `fatura` (`idFatura`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
);

Alter table clientes add column client_integration_id  VARCHAR(255);

Alter table emitente add column frase_efeito  VARCHAR(9999);
Alter table emitente add column marketplace_id  VARCHAR(9999);
Alter table emitente add column publishable_key  VARCHAR(9999);
Alter table emitente add column seller_id  VARCHAR(9999);
Alter table emitente add column on_behalf_of  VARCHAR(9999);

