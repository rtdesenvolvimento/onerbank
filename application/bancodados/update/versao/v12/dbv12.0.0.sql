alter table condicao_pagamento add column  `parcelas` INT(11) NOT NULL DEFAULT 1;
Alter table lancamentos add column `condicao_pagamento_id` INT(11) NULL DEFAULT NULL;
Alter table boleto add column `parcela_id`     INT(11)        NOT NULL;


Alter table `empresa` add column `instrucaoBoletoOnline`  LONGTEXT;
Alter table `empresa` add column `marketplace_id`  VARCHAR(9999);
Alter table `empresa` add column `publishable_key`  VARCHAR(9999);
Alter table `empresa` add column `seller_id`  VARCHAR(9999);
Alter table `empresa` add column `on_behalf_of`  VARCHAR(9999);