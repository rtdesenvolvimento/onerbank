alter table pagamento add column transaction_id  LONGTEXT;
Alter table pagamento add column authorizer_id  LONGTEXT;
Alter table pagamento add column authorization_code  LONGTEXT;
Alter table pagamento add column authorization_nsu  LONGTEXT;

Alter table link_pagamento MODIFY token LONGTEXT;
Alter table link_pagamento MODIFY url  LONGTEXT;
