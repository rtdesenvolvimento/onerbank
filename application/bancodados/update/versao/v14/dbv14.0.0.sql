CREATE TABLE IF NOT EXISTS `taxas` (
    `idTaxas`        INT(11)        NOT NULL AUTO_INCREMENT,
    `tarifaFixa`             double default   0,
    `taxaIntermediacao`      double default   0,
    `taxaParcelamento1x`     double default   0,
    `taxaParcelamento2x`     double default   0,
    `taxaParcelamento3x`     double default   0,
    `taxaParcelamento4x`     double default   0,
    `taxaParcelamento5x`     double default   0,
    `taxaParcelamento6x`     double default   0,
    `taxaParcelamento7x`     double default   0,
    `taxaParcelamento8x`     double default   0,
    `taxaParcelamento9x`     double default   0,
    `taxaParcelamento10x`     double default   0,
    `taxaParcelamento11x`     double default   0,
    `taxaParcelamento12x`     double default   0,
    `taxaParcelamentoVendedor1x`     double default   0,
    `taxaParcelamentoVendedor2x`     double default   0,
    `taxaParcelamentoVendedor3x`     double default   0,
    `taxaParcelamentoVendedor4x`     double default   0,
    `taxaParcelamentoVendedor5x`     double default   0,
    `taxaParcelamentoVendedor6x`     double default   0,
    `taxaParcelamentoVendedor7x`     double default   0,
    `taxaParcelamentoVendedor8x`     double default   0,
    `taxaParcelamentoVendedor9x`     double default   0,
    `taxaParcelamentoVendedor10x`     double default   0,
    `taxaParcelamentoVendedor11x`     double default   0,
    `taxaParcelamentoVendedor12x`     double default   0,
    `filial_id`               INT(11)        NOT NULL,
    PRIMARY KEY (`idTaxas`)
)  AUTO_INCREMENT = 1   DEFAULT CHARACTER SET = latin1;

INSERT INTO `taxas` (`tarifaFixa`, `taxaIntermediacao`, `filial_id`) VALUES (0,0,1);


Alter table link_pagamento add column parcela_id  int(11);

CREATE TABLE IF NOT EXISTS `desconto_parcela` (
    `idDescontoparcela`      INT(11)        NOT NULL AUTO_INCREMENT,
    `valor`                   double default   0,
    `data`                    DATE,
    `parcela_id`              INT(11)        NOT NULL,
    `filial_id`               INT(11)        NOT NULL,
    PRIMARY KEY (`idDescontoparcela`)
    )
    AUTO_INCREMENT = 1
    DEFAULT CHARACTER SET = latin1;


Alter table parcela add column taxas  double default 0;
Alter table parcela add column linkpagamento_id  INT(11);
Alter table emitente add column adiantamento_parcela  INT(1) default 1;


