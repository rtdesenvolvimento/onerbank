Alter table configuracao add column qtdDiasVencimentoEnviarCobranca  INT(11);
Alter table configuracao add column prazoMaximoPagamentoAposVencimento  INT(11);
Alter table configuracao add column multaAtrasoBoleto  double default 0;
Alter table configuracao add column jurosAtrasoBoleto  double default 0;