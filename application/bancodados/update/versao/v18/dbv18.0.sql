Alter table nfe add column cPais varchar(300) default '1058';
Alter table nfe add column xPais varchar(300) default 'BRASIL';

Alter table boleto add column gateway_authorizer varchar(300);
Alter table boleto add column reference_number varchar(300);
Alter table boleto add column document_number varchar(300);
Alter table boleto add column sequence varchar(300);
Alter table boleto add column description varchar(900);
Alter table boleto add column bank_code varchar(300);
Alter table boleto add column expiration_date varchar(300);
Alter table boleto add column payment_limit_date varchar(300);
Alter table boleto add column multa double;
Alter table boleto add column juros double;

CREATE TABLE IF NOT EXISTS `tipo_cobranca` (
    `idTipocobranca`      INT(11)        NOT NULL AUTO_INCREMENT,
    `descricao`           varchar(300),
    `tipo`                varchar(300),
    `filial_id`           INT(11)        NOT NULL,
    PRIMARY KEY (`idTipocobranca`)
) AUTO_INCREMENT = 1 DEFAULT CHARACTER SET = latin1;

INSERT INTO `tipo_cobranca` (`descricao`, `tipo`, `filial_id`) VALUES ('Boleto','boleto',1);
INSERT INTO `tipo_cobranca` (`descricao`, `tipo`, `filial_id`) VALUES ('Cartão','cartao',1);
INSERT INTO `tipo_cobranca` (`descricao`, `tipo`, `filial_id`) VALUES ('Em caixa','caixa',1);

Alter table lancamentos add column tipo_cobranca_id INT(11);
Alter table lancamentos add column taxa double;
Alter table lancamentos add column parcelapai_id int(11);
Alter table parcela add column lancamentopai_id int(11);
Alter table emitente add column taxa_boleto double;

update emitente set taxa_boleto = 2.90 where id = 1;
update lancamentos set  tipo_cobranca_id = 1;
