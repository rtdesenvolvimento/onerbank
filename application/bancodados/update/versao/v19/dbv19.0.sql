CREATE TABLE IF NOT EXISTS `recibo` (
    `idRecibo`          INT(11)        NOT NULL AUTO_INCREMENT,
    `valor`             double,
    `dataPagamento`     DATE ,
    `nomePessoa`        varchar(100),
    `cpfCnpjPessoa`     varchar(100),
    `referencia`        longtext,
    `formaPagamento`    longtext,
    `credito`           TINYINT(1),
    `grupo`             TINYINT(1),
    `pagamentoId`       INT(11),
    `filial_id`         INT(11)        NOT NULL,
    PRIMARY KEY (`idRecibo`)
) AUTO_INCREMENT = 1 DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `recibo_parcela` (
    `idReciboParcela`   INT(11)        NOT NULL AUTO_INCREMENT,
    `recibo_id`         INT(11)        NOT NULL,
    `parcela_id`        INT(11)        NOT NULL,
    `dataVencimento`    DATE           NOT NULL,
    `nParcela`          varchar(100),
    `refPedido`         varchar(100),
    `refVenda`          varchar(100),
    `refNF`             varchar(100),
    `filial_id`         INT(11)        NOT NULL,
    PRIMARY KEY (`idReciboParcela`)
) AUTO_INCREMENT = 1 DEFAULT CHARACTER SET = latin1;

Alter table `vendas` add column `fiscal` tinyint(1) default 0;
Alter table `vendas` add column `acrescimo` double default 0;
Alter table `vendas` add column `valorParcelado` double default 0;
Alter table `vendas` add column `condicaoPagamentoParcelado` int(11) default NULL;
Alter table `vendas` add column `totalPago` double default 0;
Alter table `vendas` add column `lancamentoId` int(11) default NULL;
Alter table `vendas` add column `lancamentoParceladoId` int(11) default NULL;

Alter table  `configuracao` add column `permiteEstoqueNegativo` TINYINT(1) DEFAULT 1;
Alter table  `configuracao` add column `usarNFCE` TINYINT(1) DEFAULT 0;
Alter table  `configuracao` add column `numeroDeSerieNFE` int(11) default 1;
Alter table  `configuracao` add column `numeroDeSerieNFCE` int(11) default 1;

Alter table `vendas` add column `dinheiro` double default 0;
Alter table `vendas` add column `cartao_credito` double default 0;
Alter table `vendas` add column `cartao_debito` double default 0;

Alter table `nfce` add column `dinheiro` double default 0;
Alter table `nfce` add column `cartao_credito` double default 0;
Alter table `nfce` add column `cartao_debito` double default 0;
Alter table `nfce` add column `valorParcelado` double default 0;

Alter table  `configuracao` add column `tBand` varchar(300);
Alter table  `configuracao` add column `CNPJCredenciadoraCartao` varchar(300);

Alter table `vendas` add column `troco` double default 0;
Alter table `nfce` add column `vTroco` double default 0;
Alter table `nfce` add column `acrescimo` double default 0;

ALTER TABLE parcela DROP FOREIGN KEY fk_parcela_lancamentos;
ALTER TABLE fatura DROP FOREIGN KEY fk_fatura_clientes;
ALTER TABLE pagamento DROP FOREIGN KEY fk_pagamento_formaPagamento;
ALTER TABLE pagamento DROP FOREIGN KEY fk_pagamento_clientes;

Alter table `produtonfe`  add column `vOutro` double default 0;
Alter table `lancamentos` add column `totalAcrescimo` double default 0;
Alter table `lancamentos` add column `totalDesconto` double default 0;

