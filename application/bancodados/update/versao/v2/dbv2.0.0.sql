
alter table configuracao add column numeroProximoNumeroLote VARCHAR(120)    NULL     DEFAULT NULL;
alter table configuracao add column descricao varchar(250);

alter table produtos add column CST varchar(80);

alter table configuracao_tecnospeed add column cod_grupo VARCHAR(45) NOT NULL;

alter table empresa add column natureza_juridica VARCHAR(200);
alter table empresa add column IE varchar(250) ;
alter table empresa add column IESUF varchar(250);
alter table empresa add column IEST varchar(250);
alter table empresa add column configuracao_tecnospeed_id int(11);

alter table empresa add column insmun varchar(250);
alter table empresa add column cnae varchar(250);
alter table empresa add column csc varchar(250);
alter table empresa add column idcsc varchar(250);
alter table empresa add column tokenibpt varchar(250);
alter table empresa add column incentivadorCultural varchar(2)   null default  'N';
alter table empresa add column incentivoFiscal varchar(2)   null default  'N';
alter table empresa add column tipoTributacao varchar(30);
alter table empresa add column naturezaTributacao varchar(30);
alter table empresa add column codIBGECidade varchar(30);
alter table empresa add column codIBGEEstado varchar(30);
alter table empresa add column regimeTributario varchar(30);
alter table empresa add column ramo_atividade varchar(30);
alter table empresa add column ambiente INT(11) NULL DEFAULT 2;
alter table empresa add column certificado VARCHAR(2)  default 'A1';
alter table empresa add column regimeEspecialTributacao varchar(2) null default  1;
alter table empresa add column cert varchar(350) NULL;
alter table empresa add column certsenha varchar(350) NULL;

alter table emitente add column incentivadorCultural varchar(2)   null default  'N';
alter table emitente add column incentivoFiscal varchar(2)   null default  'N';
alter table emitente add column tipoTributacao varchar(30);
alter table emitente add column naturezaTributacao varchar(30);

alter table configuracao add column numeroProximoNNFE varchar(30) NULL     DEFAULT NULL;

alter table nfse add column codIBGECidade VARCHAR(30)   NULL;

DROP table  configuracao;
CREATE TABLE IF NOT EXISTS `configuracao` (
  `idConfiguracao`                            INT(11)         NOT NULL AUTO_INCREMENT,
  `descricao`                                 VARCHAR(120)    NULL     DEFAULT NULL,
  `serieRps`                                  VARCHAR(120)    NULL     DEFAULT NULL,
  `numeroProximoNumeroRps`                    VARCHAR(120)    NULL     DEFAULT NULL,
  `numeroProximoNumeroLote`                   VARCHAR(120)    NULL     DEFAULT NULL,
  `numeroProximoNNFE`                         VARCHAR(120)    NULL     DEFAULT NULL,
  `naturezaOperacaoServicoPadrao`             VARCHAR(150)    NULL     DEFAULT NULL,
  `isCalcularValorISS`                        TINYINT(1)      NOT NULL DEFAULT 2,
  `isReterISS`                                TINYINT(1)      NOT NULL DEFAULT 1,
  `aliquotaIR`                                DECIMAL(10, 2)  NULL     DEFAULT 0,
  `aliquotaINSS`                              DECIMAL(10, 2)  NULL     DEFAULT 11,
  `aliquotaCofins`                            DECIMAL(10, 2)  NULL     DEFAULT 3,
  `aliquotaPIS`                               DECIMAL(10, 2)  NULL     DEFAULT 0.65,
  `aliquotaContribuicaoSocial`                DECIMAL(10, 2)  NULL     DEFAULT 1,
  `textoPadraoIR`                             VARCHAR(150)    NULL     DEFAULT '( - ) IRenda Fonte 1,5%',
  `textoPadraoIRISENTO`                       VARCHAR(150)    NULL     DEFAULT 'IR Isento Cfe. Lei nro. 9430/96 Art.64',
  `descontarIRValor`                          TINYINT(1)      NOT NULL DEFAULT 2,
  `valorMinimoCalculoIR`                      DECIMAL(10, 2)  NULL     DEFAULT 10,
  `reterCSLLPISCONFINSNotaAcima5000`          TINYINT(1)      NOT NULL DEFAULT 2,
  `reterCSLLPISCONFINSSomaImpostosAcima`      DECIMAL(10, 2)  NOT NULL DEFAULT 5000,
  `isReterINSS`                               TINYINT(1)      NOT NULL DEFAULT 2,
  `isUsarAliquotaTabelaIBPT`                  TINYINT(1)      NOT NULL DEFAULT 1,
  `aliquotaAproximadaImpostosNotaServico`     DECIMAL(10, 2)  NULL     DEFAULT 0,
  `isEnviarBoletoContaPagarEmailNotaServico`  TINYINT(1)      NOT NULL DEFAULT 2,
  `textoPadraoAssuntoEmailNotaServico`        VARCHAR(350)    NULL     DEFAULT '( - ) IRenda Fonte 1,5%',
  `textoPadraoCorpoEmailNotaServico`          VARCHAR(9999)  NULL     DEFAULT '<p>Segue o link para acesso a NFS-e online. Qualquer d&uacute;vida entrar em contato.</p>',
  `filial_id`         INT(11)         NOT NULL,
PRIMARY KEY (`idConfiguracao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

