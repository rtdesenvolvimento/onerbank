CREATE TABLE IF NOT EXISTS `centro_custo` (
  `idCentrocusto`           INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                    VARCHAR(45)    NOT NULL,
  `codigo`                  VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`               INT(11)      NOT NULL,
  `centroCustoSuperior_id`      INT(11)      NULL,
PRIMARY KEY (`idCentrocusto`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


INSERT INTO `centro_custo` (`idCentrocusto`, `nome`,`codigo`,`filial_id`)
VALUES (1, 'Matriz', '1.0.0' , 1);


CREATE TABLE IF NOT EXISTS `centro_lucro` (
  `idCentrolucro`           INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`                    VARCHAR(45)    NOT NULL,
  `codigo`                  VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`               INT(11)      NOT NULL,
  `centroCustoSuperior_id`      INT(11)      NULL,
PRIMARY KEY (`idCentrolucro`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


INSERT INTO `centro_lucro` (`idCentrolucro`, `nome`,`codigo`,`filial_id`)
VALUES (1, 'Matriz', '1.0.0' , 1);

alter table produtos add column centro_custo_id INT(11)  NULL;
alter table produtos add column origemAquisicao VARCHAR(150) NULL;
alter table produtos add column marca           VARCHAR(150) NULL;
alter table produtos add column modelo          VARCHAR(150) NULL;
alter table produtos add column numeroSerie     VARCHAR(150) NULL;
alter table produtos add column localizacao     VARCHAR(150) NULL;
alter table produtos add column dataAquisicao   DATE NULL;
alter table produtos add column numeroNFCompra  VARCHAR(150) NULL;
alter table produtos add column valorAquisicao  DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column patrimonio      TINYINT(1)      DEFAULT 0;
alter table produtos add column valorResidual         DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column valorDepreciar         DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column vidaUtil              INT(11) NULL     DEFAULT 0;
alter table produtos add column valorDepreciacao      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column valorDepreciado      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column tempoUso              INT(11) NULL     DEFAULT 0;
alter table produtos add column depreciacaoPercentual      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column valorAtualPosDepreciacao      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table produtos add column tipoDepreciacao      TINYINT(1)      DEFAULT 1;

alter table veiculos add column centro_custo_id INT(11)  NULL;
alter table veiculos add column origemAquisicao VARCHAR(150) NULL;
alter table veiculos add column localizacao     VARCHAR(150) NULL;
alter table veiculos add column dataAquisicao   DATE NULL;
alter table veiculos add column numeroNFCompra  VARCHAR(150) NULL;
alter table veiculos add column valorAquisicao  DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column patrimonio      TINYINT(1)      DEFAULT 0;
alter table veiculos add column valorResidual         DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column vidaUtil              INT(11) NULL     DEFAULT 0;
alter table veiculos add column valorDepreciacao      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column valorDepreciado      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column valorDepreciar         DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column tempoUso              INT(11) NULL     DEFAULT 0;
alter table veiculos add column depreciacaoPercentual      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column valorAtualPosDepreciacao      DECIMAL(10, 2) NULL     DEFAULT 0;
alter table veiculos add column tipoDepreciacao      TINYINT(1)      DEFAULT 1;

alter table os add column manutencao_id              INT(11);

alter table lancamentos add column    receita_id       INT(11)        NULL     DEFAULT NULL;
alter table lancamentos add column    despesa_id       INT(11)        NULL     DEFAULT NULL;


CREATE TABLE IF NOT EXISTS `manutencao_preventiva` (
  `idManutencaopreventiva`  INT(11)      NOT NULL AUTO_INCREMENT,
  `tipoManutencao`          VARCHAR(45)  NULL,
  `dataInicialPreventiva`   DATE         NULL,
  `indice`                  INT(11)      NOT NULL,
  `peca_id`                 INT(11)      NULL,
  `servico_id`              INT(11)      NULL,
  `produto_id`              INT(11)      NULL,
  `periodo`                 INT(11)      NULL,
  `filial_id`               INT(11)      NOT NULL,
  `situacao`                VARCHAR(45)   DEFAULT 1,
PRIMARY KEY (`idManutencaopreventiva`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


CREATE TABLE IF NOT EXISTS `manutencao_preventiva_veiculo` (
  `idManutencaopreventivaveiculo`   INT(11)      NOT NULL AUTO_INCREMENT,
  `tipoManutencao`                  VARCHAR(45)  NULL,
  `dataInicialPreventiva`           DATE         NULL,
  `indice`                          INT(11)      NOT NULL,
  `veiculo_id`                      INT(11)      NULL,
  `servico_id`                      INT(11)      NULL,
  `peca_id`                         INT(11)      NULL,
  `periodo`                         INT(11)      NULL,
  `filial_id`                       INT(11)      NOT NULL,
  `situacao`                        VARCHAR(45)   DEFAULT 1,
PRIMARY KEY (`idManutencaopreventivaveiculo`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


CREATE TABLE IF NOT EXISTS `programacao_manutencao_preventiva` (
  `idProgramacaomanutencao`   INT(11)       NOT NULL AUTO_INCREMENT,
  `dataAgendamento`           DATE          NULL,
  `produto_id`                INT(11)       NOT NULL,
  `manutencao_preventiva_id`  INT(11)       NOT NULL,
  `fornecedor_id`             INT(11)       NULL,
  `numeroNotaFiscal`          VARCHAR(45)   NULL,
  `numeroProgramacao`         VARCHAR(45)   NOT NULL,
  `observacao`                VARCHAR(9999) NULL,
  `prazoEntrega`              VARCHAR(45)   NULL,
  `situacao`                  VARCHAR(45)   DEFAULT 1,
  `valorManutencao`           DECIMAL(10, 2) NULL     DEFAULT 0,
  `os_id`                     INT(11)        NULL,
  `filial_id`                 INT(11)      NOT NULL,
PRIMARY KEY (`idProgramacaomanutencao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


CREATE TABLE IF NOT EXISTS `programacao_manutencao_preventiva_veiculo` (
  `idProgramacaomanutencaoveiculo`   INT(11)       NOT NULL AUTO_INCREMENT,
  `dataAgendamento`           DATE          NULL,
  `veiculo_id`                INT(11)       NOT NULL,
  `manutencao_preventiva_id`  INT(11)       NOT NULL,
  `cliente_id`                INT(11)       NULL,
  `numeroNotaFiscal`          VARCHAR(45)   NULL,
  `numeroProgramacao`         VARCHAR(45)   NOT NULL,
  `observacao`                VARCHAR(9999) NULL,
  `prazoEntrega`              VARCHAR(45)   NULL,
  `situacao`                  VARCHAR(45)   DEFAULT 1,
  `valorManutencao`           DECIMAL(10, 2) NULL     DEFAULT 0,
  `os_id`                     INT(11)        NULL,
  `filial_id`                 INT(11)      NOT NULL,
PRIMARY KEY (`idProgramacaomanutencaoveiculo`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `programacao_manutencao_preventiva_servico` (
  `idProgramacaomanutencaoservico`  INT(11)       NOT NULL AUTO_INCREMENT,
  `servico_id`                      INT(11)       NOT NULL,
  `peca_id`                         INT(11)      NULL,
  `programacao_id`                  INT(11)       NOT NULL,
  `valor`                           DECIMAL(10, 2) NULL     DEFAULT 0,
  `filial_id`                       INT(11)      NOT NULL,
  `dataFinalGarantia`               DATE          NULL,
PRIMARY KEY (`idProgramacaomanutencaoservico`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `programacao_manutencao_preventiva_servico_veiculo` (
  `idProgramacaomanutencaoservicoveiculo`  INT(11)       NOT NULL AUTO_INCREMENT,
  `servico_id`                              INT(11)       NOT NULL,
  `peca_id`                                 INT(11)      NULL,
  `programacao_id`                          INT(11)       NOT NULL,
  `valor`                                   DECIMAL(10, 2) NULL     DEFAULT 0,
  `filial_id`                               INT(11)      NOT NULL,
  `dataFinalGarantia`                       DATE          NULL,
PRIMARY KEY (`idProgramacaomanutencaoservicoveiculo`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;