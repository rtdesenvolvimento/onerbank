CREATE TABLE IF NOT EXISTS `caixa` (
  `idCaixa`           INT(11)        NOT NULL AUTO_INCREMENT,
  `usuario_id`        VARCHAR(45)    NOT NULL,
  `troco`             DECIMAL(10, 2) NULL     DEFAULT 0,
  `data_abertura`     DATE,
  `data_fechamento`   DATE,
  `situacao`          TINYINT(1)  NOT NULL   DEFAULT 1,
  `filial_id`               INT(11)      NOT NULL,
PRIMARY KEY (`idCaixa`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


alter table configuracao add column numeroProximoNFCE int(11);

alter table vendas add column  nfcegerada   TINYINT(1)  DEFAULT 0;
alter table vendas add column  nfegerada   TINYINT(1)  DEFAULT 0;
alter table vendas add column  caixa_id    INT(11);
alter table vendas add column  status   TINYINT(1)  DEFAULT 0;
alter table vendas add column  vendapdv   TINYINT(1)  DEFAULT 0;
alter table vendas add column  nNF    INT(11);


CREATE TABLE IF NOT EXISTS `imagens_produto` (
  `idImagemproduto` INT          NOT NULL AUTO_INCREMENT,
  `anexo`         VARCHAR(70)  NULL,
  `observacaoimg` TEXT         NULL,
  `path`          VARCHAR(300) NULL,
  `url`           VARCHAR(300) NULL,
  `data`          DATE NOT NULL ,
  `hora`          TIME NOT NULL ,
  `thumb`         VARCHAR(45)  NULL,
  `filial_id`     INT(11)      NOT NULL,
  `produto_id`   INT(11)      NOT NULL,
PRIMARY KEY (`idImagemproduto`)
);

alter table produtos add column  imagem    varchar(9999);
