ALTER TABLE itensInventario MODIFY quantidade double;
ALTER TABLE produtos_os MODIFY quantidade double;
ALTER TABLE itens_de_vendas MODIFY quantidade double;
ALTER TABLE itensPedido MODIFY quantidade double;
ALTER TABLE itensTransferencia MODIFY quantidade double;

ALTER TABLE produtos MODIFY estoque double;
ALTER TABLE produtos MODIFY estoqueMinimo double;


alter table vendas MODIFY valorTotal double default 0;



CREATE TABLE IF NOT EXISTS `contador` (
  `idContador`     INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`         VARCHAR(45)    NOT NULL,
  `cnpjCpf`      VARCHAR(45),
  `login`        VARCHAR(45),
  `senha`        VARCHAR(45),
PRIMARY KEY (`idContador`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


alter table empresa add column contador_id int(11);

alter table produtos add column permiteEstoqueNegativo TINYINT(1) DEFAULT 1;
alter table produtos add column usarInformacoesFiscaisProduto TINYINT(1) DEFAULT 1;
alter table produtos add column percentualIBPT  double default 0;
alter table produtos add column infoNota longtext;

alter table produtos add column pIPI double default 0;
alter table produtos add column CSTIPI VARCHAR(150) NULL;

alter table produtos add column pPIS double default 0;
alter table produtos add column CSTPIS VARCHAR(150) NULL;

alter table produtos add column pCOFINS double default 0;
alter table produtos add column CSTCOFINS VARCHAR(150) NULL;

alter table produtos add column pICMSST double default 0;

alter table produtos add column modBCST VARCHAR(150) NULL;

alter table produtos add column cProdANP VARCHAR(150) NULL;
alter table produtos add column descANP VARCHAR(150) NULL;
alter table produtos add column UFCons VARCHAR(150) NULL;


alter table produtonfe add column descANP VARCHAR(150) NULL;
alter table produtonfe add column UFCons VARCHAR(150) NULL;

alter table produtonfe add column pGLP  VARCHAR(150) NULL;
alter table produtonfe add column pGNn  VARCHAR(150) NULL;
alter table produtonfe add column pGNi  VARCHAR(150) NULL;
alter table produtonfe add column vPart  VARCHAR(150) NULL;
alter table produtonfe add column CODIF  VARCHAR(150) NULL;
alter table produtonfe add column qTemp  VARCHAR(150) NULL;
alter table produtonfe add column vCIDE  VARCHAR(150) NULL;



alter table configuracao add column pICMSST double default  0;
alter table configuracao add column pIPI double default  0;
alter table configuracao add column pPIS double default  0;
alter table configuracao add column pCOFINS double default  0;


CREATE TABLE IF NOT EXISTS `naturezaoperacao` (
  `idNaturezaoperacao`     INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`        VARCHAR(300)    NOT NULL,
  `ICMS`        VARCHAR(45),
  `IPI`        VARCHAR(45),
  `PIS`        VARCHAR(45),
  `COFINS`        VARCHAR(45),
  `CFOP`        VARCHAR(45),
  `observacao`  longtext,
  `filial_id`             INT(11)      NOT NULL,
PRIMARY KEY (`idNaturezaoperacao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


alter table nfe add column naturezaoperacao_id INT(11) NULL;

alter table configuracao add column naturezaoperacaodentroestado_id double default  0;
alter table configuracao add column naturezaoperacaoforaestado_id double default  0;

alter table nfe add column percentualICMS double default  0;

alter table nfce add column naturezaoperacao_id INT(11) NULL;

alter table nfce add column vICMSSTnf decimal(15,2) DEFAULT '0.00';

alter table naturezaoperacao add column tipoEstoque varchar(45) default 'NAOMOVER';


alter table historico_estoque add column nfe_id INT(11) NULL;
alter table historico_estoque add column nfe_item_id INT(11) NULL;

alter table lancamentos add column nfe int(11) null;

ALTER TABLE produtonfe MODIFY qtd double default NULL;


INSERT INTO `naturezaoperacao` (`idNaturezaoperacao`, `nome`, `ICMS`, `IPI`, `PIS`, `COFINS`, `CFOP`, `observacao`, `filial_id`, `tipoEstoque`) VALUES
(2, 'Venda dentro do estado', '102', '', '', '', '5102', NULL, 1, 'SAIDA'),
(3, 'Venda para fora do estado', '102', '', '', '', '6102', NULL, 1, 'SAIDA'),
(4, 'SIMPLES REMESSA', '41', '', '', '', '6949', NULL, 1, 'SAIDA'),
(5, 'DEVOLUCAO', '102', '', '', '', '5202', NULL, 1, 'ENTRADA'),
(6, 'REMESSA POR CONTA E ORDEM DE TERCEIROS', '41', '', '', '', '5924', NULL, 1, 'SAIDA'),
(7, 'AMOSTRA GRATIS', '40', '', '', '', '5911', NULL, 1, 'SAIDA'),
(8, 'Remessa de mercadoria por conta e ordem de terceiros', '41', '', '', '', '5923', NULL, 1, 'SAIDA');


alter table usuarios add column `telefoneUsuario` VARCHAR(20);
alter table permissoes add column `telefoneUsuario` VARCHAR(20);
alter table usuarios add column `photo` longtext;


CREATE TABLE IF NOT EXISTS `tipotarefa` (
  `idTipoTarefa` INT(11)        NOT NULL AUTO_INCREMENT,
  `descricao`  VARCHAR(45)    NULL     DEFAULT NULL,
  `filial_id`             INT(11)      NOT NULL,
PRIMARY KEY (`idTipoTarefa`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


alter table tipotarefa add column `telefoneUsuario` VARCHAR(20);


CREATE TABLE IF NOT EXISTS `formulario` (
  `idFormulario` INT(11)        NOT NULL AUTO_INCREMENT,
  `descricao`  VARCHAR(45)    NULL     DEFAULT NULL,
  `ativo`  VARCHAR(45),
  `filial_id`             INT(11)      NOT NULL,
  `telefoneUsuario` VARCHAR(20) NOT NULL,
PRIMARY KEY (`idFormulario`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;



CREATE TABLE IF NOT EXISTS `perguntaformulario` (
  `idPerguntaFormulario` INT(11)        NOT NULL AUTO_INCREMENT,
  `formulario`   INT(11) ,
  `pergunta`   INT(11) ,
  `tipoCampo`    VARCHAR(45) ,
  `descricao`    VARCHAR(200) ,
  `peso`    VARCHAR(200) ,
  `obrigatorio`    VARCHAR(200) ,
  `ativo`    VARCHAR(200) ,
  `anterior`    VARCHAR(200) ,
  `proxima`    VARCHAR(200) ,
  `alias`    VARCHAR(200) ,
  `pagina`    VARCHAR(200) ,
  `ordem`    VARCHAR(200) ,
  `filial_id`             INT(11)      NOT NULL,
  `telefoneUsuario` VARCHAR(20) NOT NULL,
PRIMARY KEY (`idPerguntaFormulario`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;



CREATE TABLE IF NOT EXISTS `perguntaformularioopcao` (
  `idPerguntaFormularioOpcao` INT(11)        NOT NULL AUTO_INCREMENT,
  `formulario`   INT(11) ,
  `pergunta`   INT(11) ,
  `tipoCampo`    VARCHAR(45) ,
  `descricao`    VARCHAR(200) ,
  `peso`    VARCHAR(200) ,
  `obrigatorio`    VARCHAR(200) ,
  `opcao`    VARCHAR(200) ,
  `ativo`    VARCHAR(200) ,
  `anterior`    VARCHAR(200) ,
  `proxima`    VARCHAR(200) ,
  `alias`    VARCHAR(200) ,
  `pagina`    VARCHAR(200) ,
  `ordem`    VARCHAR(200) ,
  `filial_id`             INT(11)      NOT NULL,
  `telefoneUsuario` VARCHAR(20) NOT NULL,
PRIMARY KEY (`idPerguntaFormularioOpcao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;



CREATE TABLE IF NOT EXISTS `tarefa` (
  `idTarefa` INT(11)        NOT NULL AUTO_INCREMENT,
  `responsavel`             INT(11)      NOT NULL,
  `tipoTarefa`             INT(11)      NOT NULL,
  `atribuir`    VARCHAR(200) ,
  `status`    VARCHAR(200) ,
  `descricao`    VARCHAR(9000) ,
  `titulo`    VARCHAR(9000) ,
  `data`    VARCHAR(200) ,
  `horaInicio`    VARCHAR(200) ,
  `horaFinal`    VARCHAR(200) ,
  `filial_id`             INT(11)      NOT NULL,
  `telefoneUsuario` VARCHAR(20) NOT NULL,
PRIMARY KEY (`idTarefa`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;




CREATE TABLE IF NOT EXISTS `proposta` (
  `idProposta` INT(11)        NOT NULL AUTO_INCREMENT,
  `status`       VARCHAR(45)    ,
  `dataProposta`       VARCHAR(45)    ,
  `dataAberta`       VARCHAR(45)    ,
  `dataAguardando`       VARCHAR(45)    ,
  `dataFechamento`       VARCHAR(45)    ,
  `latitude`       VARCHAR(45)    ,
  `longitude`       VARCHAR(45)    ,
  `altitude`       VARCHAR(45)    ,
  `visitaConfirmada`       VARCHAR(45)    ,
  `telefoneUsuario`       VARCHAR(45)    ,
  `descricao`  VARCHAR(200)    NULL     DEFAULT NULL,
  `observacao`  longtext,
  `formulario`             INT(11)      NOT NULL,
  `cliente`             INT(11)      NOT NULL,
  `filial_id`             INT(11)      NOT NULL,
PRIMARY KEY (`idProposta`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


CREATE TABLE IF NOT EXISTS `respostaproposta` (
  `idRespostaProposta` INT(11)        NOT NULL AUTO_INCREMENT,
  `nome`       VARCHAR(45)    NOT NULL,
  `proposta`             INT(11),
  `formulario`             INT(11),
  `pergunta`             INT(11),
  `resposta`  VARCHAR(999),
  `tipoCampo`  VARCHAR(999),
  `descricao`  VARCHAR(999),
  `peso`  VARCHAR(999),
  `obrigatorio`  VARCHAR(999),
  `anterior`  VARCHAR(999),
  `proxima`  VARCHAR(999),
  `alias`  VARCHAR(999),
  `telefoneUsuario`  VARCHAR(999),
  `filial_id`             INT(11)      NOT NULL,
PRIMARY KEY (`idRespostaProposta`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


CREATE TABLE IF NOT EXISTS `respostapropostaopcao` (
  `idRespostaPropostaOpcao` INT(11)        NOT NULL AUTO_INCREMENT,
  `descricao`  VARCHAR(45),
  `pergunta`             INT(11),
  `formulario`             INT(11),
  `proposta`             INT(11),
  `formularioOpcao`             INT(11),
  `opcao`  VARCHAR(999),
  `resposta`  VARCHAR(999),
  `peso`  VARCHAR(999),
  `anterior`  VARCHAR(999),
  `proxima`  VARCHAR(999),
  `telefoneUsuario`  VARCHAR(999),
  `filial_id`             INT(11)      NOT NULL,
PRIMARY KEY (`idRespostaPropostaOpcao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

alter table clientes add column `cargoContato` VARCHAR(255);
alter table clientes add column `telefoneUsuario`       VARCHAR(100);

CREATE TABLE IF NOT EXISTS `digitalizacao` (
  `idDigitalizacao`     INT(11)         NOT NULL AUTO_INCREMENT,
  `cliente`            INT(11) ,
  `observacao`              LONGTEXT   ,
  `documento`              LONGTEXT   ,
  `filial_id`             INT(11)      NOT NULL,
PRIMARY KEY (`idDigitalizacao`)
)
  AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

alter table digitalizacao add column `telefoneUsuario` VARCHAR(200);

alter table tarefa add column  `app_id` VARCHAR(30);
alter table proposta add column  `app_id` VARCHAR(30);
alter table respostaproposta add column  `app_id` VARCHAR(30);
alter table respostapropostaopcao add column  `app_id` VARCHAR(30);
alter table clientes add column  `app_id` VARCHAR(30);
alter table digitalizacao add column  `app_id` VARCHAR(30);

alter table proposta add column responsavel INT(11);
alter table clientes add column responsavel INT(11);