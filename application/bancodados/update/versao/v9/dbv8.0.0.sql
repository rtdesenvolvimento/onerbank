CREATE TABLE IF NOT EXISTS `unidade_produto` (
  `idUnidadeProduto` INT(11)      NOT NULL AUTO_INCREMENT,
  `unidade_origem`   VARCHAR(45)  NOT NULL,
  `unidade`          VARCHAR(45)  NOT NULL,
  `produto_id`       INT(11)      NOT NULL,
  `filial_id`        INT(11)      NOT NULL,
  `quantidade`       double        NULL     DEFAULT 0,
PRIMARY KEY (`idUnidadeProduto`)
) AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

Alter table unidade_produto add column quantidade double        NULL     DEFAULT 0;