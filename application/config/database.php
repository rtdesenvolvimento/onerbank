
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
//$active_group = '28200252000183';

$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'onerbank_masterSystem';
$db['default']['password'] = '2078404abC@';
$db['default']['database'] = 'onerbank_masterSystem';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['28200252000183']['hostname'] = "localhost";
$db['28200252000183']['username'] = "root";
$db['28200252000183']['password'] = "";
$db['28200252000183']['database'] = "28200252000183";
$db['28200252000183']['dbdriver'] = "mysqli";
$db['28200252000183']['dbprefix'] = "";
$db['28200252000183']['pconnect'] = FALSE;
$db['28200252000183']['db_debug'] = TRUE;
$db['28200252000183']['cache_on'] = FALSE;
$db['28200252000183']['cachedir'] = "";
$db['28200252000183']['char_set'] = "utf8";
$db['28200252000183']['dbcollat'] = "utf8_general_ci";
$db['28200252000183']['swap_pre'] = "";
$db['28200252000183']['autoinit'] = TRUE;
$db['28200252000183']['stricton'] = FALSE;

$db['29466796000154']['hostname'] = "localhost";
$db['29466796000154']['username'] = "onerbank_masterSystem";
$db['29466796000154']['password'] = "2078404abC@";
$db['29466796000154']['database'] = "onerbank_29466796000154";
$db['29466796000154']['dbdriver'] = "mysql";
$db['29466796000154']['dbprefix'] = "";
$db['29466796000154']['pconnect'] = FALSE;
$db['29466796000154']['db_debug'] = TRUE;
$db['29466796000154']['cache_on'] = FALSE;
$db['29466796000154']['cachedir'] = "";
$db['29466796000154']['char_set'] = "utf8";
$db['29466796000154']['dbcollat'] = "utf8_general_ci";
$db['29466796000154']['swap_pre'] = "";
$db['29466796000154']['autoinit'] = TRUE;
$db['29466796000154']['stricton'] = FALSE;

$db['92946359000174']['hostname'] = "localhost";
$db['92946359000174']['username'] = "onerbank_masterSystem";
$db['92946359000174']['password'] = "2078404abC@";
$db['92946359000174']['database'] ="onerbank_92946359000174";
$db['92946359000174']['dbdriver'] = "mysql";
$db['92946359000174']['dbprefix'] = "";
$db['92946359000174']['pconnect'] = FALSE;
$db['92946359000174']['db_debug'] = TRUE;
$db['92946359000174']['cache_on'] = FALSE;
$db['92946359000174']['cachedir'] = "";
$db['92946359000174']['char_set'] = "utf8";
$db['92946359000174']['dbcollat'] = "utf8_general_ci";
$db['92946359000174']['swap_pre'] = "";
$db['92946359000174']['autoinit'] = TRUE;
$db['92946359000174']['stricton'] = FALSE;

$db['92363580044']['hostname'] = "localhost";
$db['92363580044']['username'] = "onerbank_masterSystem";
$db['92363580044']['password'] = "2078404abC@";
$db['92363580044']['database'] = "onerbank_92363580044";
$db['92363580044']['dbdriver'] = "mysql";
$db['92363580044']['dbprefix'] = "";
$db['92363580044']['pconnect'] = FALSE;
$db['92363580044']['db_debug'] = TRUE;
$db['92363580044']['cache_on'] = FALSE;
$db['92363580044']['cachedir'] = "";
$db['92363580044']['char_set'] = "utf8";
$db['92363580044']['dbcollat'] = "utf8_general_ci";
$db['92363580044']['swap_pre'] = "";
$db['92363580044']['autoinit'] = TRUE;
$db['92363580044']['stricton'] = FALSE;

$db['18788643000107']['hostname'] = "localhost";
$db['18788643000107']['username'] = "onerbank_masterSystem";
$db['18788643000107']['password'] = "2078404abC@";
$db['18788643000107']['database'] = "onerbank_18788643000107";
$db['18788643000107']['dbdriver'] = "mysql";
$db['18788643000107']['dbprefix'] = "";
$db['18788643000107']['pconnect'] = FALSE;
$db['18788643000107']['db_debug'] = TRUE;
$db['18788643000107']['cache_on'] = FALSE;
$db['18788643000107']['cachedir'] = "";
$db['18788643000107']['char_set'] = "utf8";
$db['18788643000107']['dbcollat'] = "utf8_general_ci";
$db['18788643000107']['swap_pre'] = "";
$db['18788643000107']['autoinit'] = TRUE;
$db['18788643000107']['stricton'] = FALSE;

$db['24104060000112']['hostname'] = "localhost";
$db['24104060000112']['username'] = "onerbank_masterSystem";
$db['24104060000112']['password'] = "2078404abC@";
$db['24104060000112']['database'] = "onerbank_24104060000112";
$db['24104060000112']['dbdriver'] = "mysql";
$db['24104060000112']['dbprefix'] = "";
$db['24104060000112']['pconnect'] = FALSE;
$db['24104060000112']['db_debug'] = TRUE;
$db['24104060000112']['cache_on'] = FALSE;
$db['24104060000112']['cachedir'] = "";
$db['24104060000112']['char_set'] = "utf8";
$db['24104060000112']['dbcollat'] = "utf8_general_ci";
$db['24104060000112']['swap_pre'] = "";
$db['24104060000112']['autoinit'] = TRUE;
$db['24104060000112']['stricton'] = FALSE;

$db['18714001000163']['hostname'] = "localhost";
$db['18714001000163']['username'] = "onerbank_masterSystem";
$db['18714001000163']['password'] = "2078404abC@";
$db['18714001000163']['database'] = "onerbank_18714001000163";
$db['18714001000163']['dbdriver'] = "mysql";
$db['18714001000163']['dbprefix'] = "";
$db['18714001000163']['pconnect'] = FALSE;
$db['18714001000163']['db_debug'] = TRUE;
$db['18714001000163']['cache_on'] = FALSE;
$db['18714001000163']['cachedir'] = "";
$db['18714001000163']['char_set'] = "utf8";
$db['18714001000163']['dbcollat'] = "utf8_general_ci";
$db['18714001000163']['swap_pre'] = "";
$db['18714001000163']['autoinit'] = TRUE;
$db['18714001000163']['stricton'] = FALSE;

$db['30962930000197']['hostname'] = "localhost";
$db['30962930000197']['username'] = "onerbank_masterSystem";
$db['30962930000197']['password'] = "2078404abC@";
$db['30962930000197']['database'] = "onerbank_30962930000197";
$db['30962930000197']['dbdriver'] = "mysql";
$db['30962930000197']['dbprefix'] = "";
$db['30962930000197']['pconnect'] = FALSE;
$db['30962930000197']['db_debug'] = TRUE;
$db['30962930000197']['cache_on'] = FALSE;
$db['30962930000197']['cachedir'] = "";
$db['30962930000197']['char_set'] = "utf8";
$db['30962930000197']['dbcollat'] = "utf8_general_ci";
$db['30962930000197']['swap_pre'] = "";
$db['30962930000197']['autoinit'] = TRUE;
$db['30962930000197']['stricton'] = FALSE;

$db['23493449000134']['hostname'] = "localhost";
$db['23493449000134']['username'] = "onerbank_masterSystem";
$db['23493449000134']['password'] = "2078404abC@";
$db['23493449000134']['database'] = "onerbank_23493449000134";
$db['23493449000134']['dbdriver'] = "mysql";
$db['23493449000134']['dbprefix'] = "";
$db['23493449000134']['pconnect'] = FALSE;
$db['23493449000134']['db_debug'] = TRUE;
$db['23493449000134']['cache_on'] = FALSE;
$db['23493449000134']['cachedir'] = "";
$db['23493449000134']['char_set'] = "utf8";
$db['23493449000134']['dbcollat'] = "utf8_general_ci";
$db['23493449000134']['swap_pre'] = "";
$db['23493449000134']['autoinit'] = TRUE;
$db['23493449000134']['stricton'] = FALSE;

$db['27067511000187']['hostname'] = "localhost";
$db['27067511000187']['username'] = "onerbank_masterSystem";
$db['27067511000187']['password'] = "2078404abC@";
$db['27067511000187']['database'] = "onerbank_27067511000187";
$db['27067511000187']['dbdriver'] = "mysql";
$db['27067511000187']['dbprefix'] = "";
$db['27067511000187']['pconnect'] = FALSE;
$db['27067511000187']['db_debug'] = TRUE;
$db['27067511000187']['cache_on'] = FALSE;
$db['27067511000187']['cachedir'] = "";
$db['27067511000187']['char_set'] = "utf8";
$db['27067511000187']['dbcollat'] = "utf8_general_ci";
$db['27067511000187']['swap_pre'] = "";
$db['27067511000187']['autoinit'] = TRUE;
$db['27067511000187']['stricton'] = FALSE;

$db['27420256000104']['hostname'] = "localhost";
$db['27420256000104']['username'] = "onerbank_masterSystem";
$db['27420256000104']['password'] = "2078404abC@";
$db['27420256000104']['database'] = "onerbank_27420256000104";
$db['27420256000104']['dbdriver'] = "mysql";
$db['27420256000104']['dbprefix'] = "";
$db['27420256000104']['pconnect'] = FALSE;
$db['27420256000104']['db_debug'] = TRUE;
$db['27420256000104']['cache_on'] = FALSE;
$db['27420256000104']['cachedir'] = "";
$db['27420256000104']['char_set'] = "utf8";
$db['27420256000104']['dbcollat'] = "utf8_general_ci";
$db['27420256000104']['swap_pre'] = "";
$db['27420256000104']['autoinit'] = TRUE;
$db['27420256000104']['stricton'] = FALSE;

$db['27067495000122']['hostname'] = "localhost";
$db['27067495000122']['username'] = "onerbank_masterSystem";
$db['27067495000122']['password'] = "2078404abC@";
$db['27067495000122']['database'] = "onerbank_27067495000122";
$db['27067495000122']['dbdriver'] = "mysql";
$db['27067495000122']['dbprefix'] = "";
$db['27067495000122']['pconnect'] = FALSE;
$db['27067495000122']['db_debug'] = TRUE;
$db['27067495000122']['cache_on'] = FALSE;
$db['27067495000122']['cachedir'] = "";
$db['27067495000122']['char_set'] = "utf8";
$db['27067495000122']['dbcollat'] = "utf8_general_ci";
$db['27067495000122']['swap_pre'] = "";
$db['27067495000122']['autoinit'] = TRUE;
$db['27067495000122']['stricton'] = FALSE;

$db['18371200000116']['hostname'] = "localhost";
$db['18371200000116']['username'] = "onerbank_masterSystem";
$db['18371200000116']['password'] = "2078404abC@";
$db['18371200000116']['database'] = "onerbank_18371200000116";
$db['18371200000116']['dbdriver'] = "mysql";
$db['18371200000116']['dbprefix'] = "";
$db['18371200000116']['pconnect'] = FALSE;
$db['18371200000116']['db_debug'] = TRUE;
$db['18371200000116']['cache_on'] = FALSE;
$db['18371200000116']['cachedir'] = "";
$db['18371200000116']['char_set'] = "utf8";
$db['18371200000116']['dbcollat'] = "utf8_general_ci";
$db['18371200000116']['swap_pre'] = "";
$db['18371200000116']['autoinit'] = TRUE;
$db['18371200000116']['stricton'] = FALSE;

$db['34238001000190']['hostname'] = "localhost";
$db['34238001000190']['username'] = "onerbank_masterSystem";
$db['34238001000190']['password'] = "2078404abC@";
$db['34238001000190']['database'] = "onerbank_34238001000190";
$db['34238001000190']['dbdriver'] = "mysql";
$db['34238001000190']['dbprefix'] = "";
$db['34238001000190']['pconnect'] = FALSE;
$db['34238001000190']['db_debug'] = TRUE;
$db['34238001000190']['cache_on'] = FALSE;
$db['34238001000190']['cachedir'] = "";
$db['34238001000190']['char_set'] = "utf8";
$db['34238001000190']['dbcollat'] = "utf8_general_ci";
$db['34238001000190']['swap_pre'] = "";
$db['34238001000190']['autoinit'] = TRUE;
$db['34238001000190']['stricton'] = FALSE;

$db['34238001000190']['hostname'] = "localhost";
$db['34238001000190']['username'] = "onerbank_masterSystem";
$db['34238001000190']['password'] = "2078404abC@";
$db['34238001000190']['database'] = "onerbank_34238001000190";
$db['34238001000190']['dbdriver'] = "mysql";
$db['34238001000190']['dbprefix'] = "";
$db['34238001000190']['pconnect'] = FALSE;
$db['34238001000190']['db_debug'] = TRUE;
$db['34238001000190']['cache_on'] = FALSE;
$db['34238001000190']['cachedir'] = "";
$db['34238001000190']['char_set'] = "utf8";
$db['34238001000190']['dbcollat'] = "utf8_general_ci";
$db['34238001000190']['swap_pre'] = "";
$db['34238001000190']['autoinit'] = TRUE;
$db['34238001000190']['stricton'] = FALSE;

$db['06911877709']['hostname'] = "localhost";
$db['06911877709']['username'] = "onerbank_masterSystem";
$db['06911877709']['password'] = "2078404abC@";
$db['06911877709']['database'] = "onerbank_06911877709";
$db['06911877709']['dbdriver'] = "mysql";
$db['06911877709']['dbprefix'] = "";
$db['06911877709']['pconnect'] = FALSE;
$db['06911877709']['db_debug'] = TRUE;
$db['06911877709']['cache_on'] = FALSE;
$db['06911877709']['cachedir'] = "";
$db['06911877709']['char_set'] = "utf8";
$db['06911877709']['dbcollat'] = "utf8_general_ci";
$db['06911877709']['swap_pre'] = "";
$db['06911877709']['autoinit'] = TRUE;
$db['06911877709']['stricton'] = FALSE;

$db['19393066000117']['hostname'] = "localhost";
$db['19393066000117']['username'] = "onerbank_masterSystem";
$db['19393066000117']['password'] = "2078404abC@";
$db['19393066000117']['database'] = "onerbank_19393066000117";
$db['19393066000117']['dbdriver'] = "mysql";
$db['19393066000117']['dbprefix'] = "";
$db['19393066000117']['pconnect'] = FALSE;
$db['19393066000117']['db_debug'] = TRUE;
$db['19393066000117']['cache_on'] = FALSE;
$db['19393066000117']['cachedir'] = "";
$db['19393066000117']['char_set'] = "utf8";
$db['19393066000117']['dbcollat'] = "utf8_general_ci";
$db['19393066000117']['swap_pre'] = "";
$db['19393066000117']['autoinit'] = TRUE;
$db['19393066000117']['stricton'] = FALSE;

$db['09675402750']['hostname'] = "localhost";
$db['09675402750']['username'] = "onerbank_masterSystem";
$db['09675402750']['password'] = "2078404abC@";
$db['09675402750']['database'] = "onerbank_09675402750";
$db['09675402750']['dbdriver'] = "mysql";
$db['09675402750']['dbprefix'] = "";
$db['09675402750']['pconnect'] = FALSE;
$db['09675402750']['db_debug'] = TRUE;
$db['09675402750']['cache_on'] = FALSE;
$db['09675402750']['cachedir'] = "";
$db['09675402750']['char_set'] = "utf8";
$db['09675402750']['dbcollat'] = "utf8_general_ci";
$db['09675402750']['swap_pre'] = "";
$db['09675402750']['autoinit'] = TRUE;
$db['09675402750']['stricton'] = FALSE;

$db['26148127000146']['hostname'] = "localhost";
$db['26148127000146']['username'] = "onerbank_masterSystem";
$db['26148127000146']['password'] = "2078404abC@";
$db['26148127000146']['database'] = "onerbank_26148127000146";
$db['26148127000146']['dbdriver'] = "mysql";
$db['26148127000146']['dbprefix'] = "";
$db['26148127000146']['pconnect'] = FALSE;
$db['26148127000146']['db_debug'] = TRUE;
$db['26148127000146']['cache_on'] = FALSE;
$db['26148127000146']['cachedir'] = "";
$db['26148127000146']['char_set'] = "utf8";
$db['26148127000146']['dbcollat'] = "utf8_general_ci";
$db['26148127000146']['swap_pre'] = "";
$db['26148127000146']['autoinit'] = TRUE;
$db['26148127000146']['stricton'] = FALSE;

$db['01691334782']['hostname'] = "localhost";
$db['01691334782']['username'] = "onerbank_masterSystem";
$db['01691334782']['password'] = "2078404abC@";
$db['01691334782']['database'] = "onerbank_01691334782";
$db['01691334782']['dbdriver'] = "mysql";
$db['01691334782']['dbprefix'] = "";
$db['01691334782']['pconnect'] = FALSE;
$db['01691334782']['db_debug'] = TRUE;
$db['01691334782']['cache_on'] = FALSE;
$db['01691334782']['cachedir'] = "";
$db['01691334782']['char_set'] = "utf8";
$db['01691334782']['dbcollat'] = "utf8_general_ci";
$db['01691334782']['swap_pre'] = "";
$db['01691334782']['autoinit'] = TRUE;
$db['01691334782']['stricton'] = FALSE;

$db['26487592000101']['hostname'] = "localhost";
$db['26487592000101']['username'] = "onerbank_masterSystem";
$db['26487592000101']['password'] = "2078404abC@";
$db['26487592000101']['database'] = "onerbank_26487592000101";
$db['26487592000101']['dbdriver'] = "mysql";
$db['26487592000101']['dbprefix'] = "";
$db['26487592000101']['pconnect'] = FALSE;
$db['26487592000101']['db_debug'] = TRUE;
$db['26487592000101']['cache_on'] = FALSE;
$db['26487592000101']['cachedir'] = "";
$db['26487592000101']['char_set'] = "utf8";
$db['26487592000101']['dbcollat'] = "utf8_general_ci";
$db['26487592000101']['swap_pre'] = "";
$db['26487592000101']['autoinit'] = TRUE;
$db['26487592000101']['stricton'] = FALSE;

$db['30686486000124']['hostname'] = "localhost";
$db['30686486000124']['username'] = "onerbank_masterSystem";
$db['30686486000124']['password'] = "2078404abC@";
$db['30686486000124']['database'] = "onerbank_30686486000124";
$db['30686486000124']['dbdriver'] = "mysql";
$db['30686486000124']['dbprefix'] = "";
$db['30686486000124']['pconnect'] = FALSE;
$db['30686486000124']['db_debug'] = TRUE;
$db['30686486000124']['cache_on'] = FALSE;
$db['30686486000124']['cachedir'] = "";
$db['30686486000124']['char_set'] = "utf8";
$db['30686486000124']['dbcollat'] = "utf8_general_ci";
$db['30686486000124']['swap_pre'] = "";
$db['30686486000124']['autoinit'] = TRUE;
$db['30686486000124']['stricton'] = FALSE;

$db['30523796000128']['hostname'] = "localhost";
$db['30523796000128']['username'] = "onerbank_masterSystem";
$db['30523796000128']['password'] = "2078404abC@";
$db['30523796000128']['database'] = "onerbank_30523796000128";
$db['30523796000128']['dbdriver'] = "mysql";
$db['30523796000128']['dbprefix'] = "";
$db['30523796000128']['pconnect'] = FALSE;
$db['30523796000128']['db_debug'] = TRUE;
$db['30523796000128']['cache_on'] = FALSE;
$db['30523796000128']['cachedir'] = "";
$db['30523796000128']['char_set'] = "utf8";
$db['30523796000128']['dbcollat'] = "utf8_general_ci";
$db['30523796000128']['swap_pre'] = "";
$db['30523796000128']['autoinit'] = TRUE;
$db['30523796000128']['stricton'] = FALSE;

$db['99906791004']['hostname'] = "localhost";
$db['99906791004']['username'] = "onerbank_masterSystem";
$db['99906791004']['password'] = "2078404abC@";
$db['99906791004']['database'] = "onerbank_99906791004";
$db['99906791004']['dbdriver'] = "mysql";
$db['99906791004']['dbprefix'] = "";
$db['99906791004']['pconnect'] = FALSE;
$db['99906791004']['db_debug'] = TRUE;
$db['99906791004']['cache_on'] = FALSE;
$db['99906791004']['cachedir'] = "";
$db['99906791004']['char_set'] = "utf8";
$db['99906791004']['dbcollat'] = "utf8_general_ci";
$db['99906791004']['swap_pre'] = "";
$db['99906791004']['autoinit'] = TRUE;
$db['99906791004']['stricton'] = FALSE;

$db['80636101053']['hostname'] = "localhost";
$db['80636101053']['username'] = "onerbank_masterSystem";
$db['80636101053']['password'] = "2078404abC@";
$db['80636101053']['database'] = "onerbank_80636101053";
$db['80636101053']['dbdriver'] = "mysql";
$db['80636101053']['dbprefix'] = "";
$db['80636101053']['pconnect'] = FALSE;
$db['80636101053']['db_debug'] = TRUE;
$db['80636101053']['cache_on'] = FALSE;
$db['80636101053']['cachedir'] = "";
$db['80636101053']['char_set'] = "utf8";
$db['80636101053']['dbcollat'] = "utf8_general_ci";
$db['80636101053']['swap_pre'] = "";
$db['80636101053']['autoinit'] = TRUE;
$db['80636101053']['stricton'] = FALSE;

$db['11878512722']['hostname'] = "localhost";
$db['11878512722']['username'] = "onerbank_masterSystem";
$db['11878512722']['password'] = "2078404abC@";
$db['11878512722']['database'] = "onerbank_11878512722";
$db['11878512722']['dbdriver'] = "mysql";
$db['11878512722']['dbprefix'] = "";
$db['11878512722']['pconnect'] = FALSE;
$db['11878512722']['db_debug'] = TRUE;
$db['11878512722']['cache_on'] = FALSE;
$db['11878512722']['cachedir'] = "";
$db['11878512722']['char_set'] = "utf8";
$db['11878512722']['dbcollat'] = "utf8_general_ci";
$db['11878512722']['swap_pre'] = "";
$db['11878512722']['autoinit'] = TRUE;
$db['11878512722']['stricton'] = FALSE;

$db['30218099000163']['hostname'] = "localhost";
$db['30218099000163']['username'] = "onerbank_masterSystem";
$db['30218099000163']['password'] = "2078404abC@";
$db['30218099000163']['database'] = "onerbank_30218099000163";
$db['30218099000163']['dbdriver'] = "mysqli";
$db['30218099000163']['dbprefix'] = "";
$db['30218099000163']['pconnect'] = FALSE;
$db['30218099000163']['db_debug'] = TRUE;
$db['30218099000163']['cache_on'] = FALSE;
$db['30218099000163']['cachedir'] = "";
$db['30218099000163']['char_set'] = "utf8";
$db['30218099000163']['dbcollat'] = "utf8_general_ci";
$db['30218099000163']['swap_pre'] = "";
$db['30218099000163']['autoinit'] = TRUE;
$db['30218099000163']['stricton'] = FALSE;


$db['30218099000163']['hostname'] = "localhost";
$db['30218099000163']['username'] = "onerbank_masterSystem";
$db['30218099000163']['password'] = "2078404abC@";
$db['30218099000163']['database'] = "onerbank_30218099000163";
$db['30218099000163']['dbdriver'] = "mysqli";
$db['30218099000163']['dbprefix'] = "";
$db['30218099000163']['pconnect'] = FALSE;
$db['30218099000163']['db_debug'] = TRUE;
$db['30218099000163']['cache_on'] = FALSE;
$db['30218099000163']['cachedir'] = "";
$db['30218099000163']['char_set'] = "utf8";
$db['30218099000163']['dbcollat'] = "utf8_general_ci";
$db['30218099000163']['swap_pre'] = "";
$db['30218099000163']['autoinit'] = TRUE;
$db['30218099000163']['stricton'] = FALSE;


$db['10339919701']['hostname'] = "localhost";
$db['10339919701']['username'] = "onerbank_masterSystem";
$db['10339919701']['password'] = "2078404abC@";
$db['10339919701']['database'] = "onerbank_10339919701";
$db['10339919701']['dbdriver'] = "mysqli";
$db['10339919701']['dbprefix'] = "";
$db['10339919701']['pconnect'] = FALSE;
$db['10339919701']['db_debug'] = TRUE;
$db['10339919701']['cache_on'] = FALSE;
$db['10339919701']['cachedir'] = "";
$db['10339919701']['char_set'] = "utf8";
$db['10339919701']['dbcollat'] = "utf8_general_ci";
$db['10339919701']['swap_pre'] = "";
$db['10339919701']['autoinit'] = TRUE;
$db['10339919701']['stricton'] = FALSE;
                