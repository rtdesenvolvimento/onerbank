<?php
$config = array(


    'clientes' => array(array(
        'field' => 'nomeCliente',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ))
,

    'fornecedor' => array(array(
        'field' => 'nomeFornecedor',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ))
,
    'empresa' => array(array(
        'field' => 'nomeEmpresa',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ))
,

    'servicos' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))
,
    'aparelho' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))
,
    'grupoproduto' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))

,
    'setor' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))
,
    'naturezaoperacao' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ) )
,
    'contador' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'cnpjCpf',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))
,
    'configuracao_tecnospeed' => array(array(
        'field' => 'grupo',
        'label' => 'grupo',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))
,
    'configuracao' => array(
        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ))
,
    'vistoria' => array(array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'datavistoria',
            'label' => 'Data da Vistoria',
            'rules' => ''
        ),

        array(
            'field' => 'horavistoria',
            'label' => 'Hora da Vistoria',
            'rules' => ''
        ),

        array(
            'field' => 'gnv',
            'label' => 'GNV',
            'rules' => ''
        ),

        array(
            'field' => 'tanque',
            'label' => 'Tanque',
            'rules' => ''
        ),

        array(
            'field' => 'kmentrada',
            'label' => 'KM Entrada',
            'rules' => ''
        ),

        array(
            'field' => 'kmsaida',
            'label' => 'KM Entrada',
            'rules' => ''
        ),

        array(
            'field' => 'datahoraexecusao',
            'label' => 'Data/Hora Execusão da Vistoria',
            'rules' => ''
        ),

        array(
            'field' => 'datahoracadastro',
            'label' => 'Data/Hora Cadastro da Vistoria',
            'rules' => ''
        ),

        array(
            'field' => 'observacao',
            'label' => 'Observacao',
            'rules' => ''
        ),

        array(
            'field' => 'pertencesPortaLuva',
            'label' => 'Pertences no Porta Luva',
            'rules' => ''
        ),

        array(
            'field' => 'clientes_id',
            'label' => 'Cliente',
            'rules' => 'required|trim|xss_clean'
        ),

        array(
            'field' => 'usuarios_id',
            'label' => 'Responsável',
            'rules' => 'required|trim|xss_clean'
        ),

        array(
            'field' => 'veiculo_id',
            'label' => 'Veículo',
            'rules' => 'required|trim|xss_clean'
        )
    )
,
    'acessorio' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        )
    ,

        array(
            'field' => 'requer_observacao',
            'label' => '',
            'rules' => ''
        ))
,
    'tipoavaria' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        )
    )
,
    'visaoautomovel' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        )
    )
,
    'equipamentoveiculo' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        )
    ,

        array(
            'field' => 'idVisaoautomovel',
            'label' => '',
            'rules' => ''
        )
    ,
        array(
            'field' => 'tipoVeiculo_id',
            'label' => '',
            'rules' => ''
        )
    ,
        array(
            'field' => 'client_x',
            'label' => '',
            'rules' => ''
        )
    ,
        array(
            'field' => 'client_y',
            'label' => '',
            'rules' => ''
        )
    )
,
    'tipoveiculo' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),

        array(
            'field' => 'descricao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'numero_eixo',
            'label' => '',
            'rules' => ''
        )
    ,
        array(
            'field' => 'numero_unidade_tracao',
            'label' => '',
            'rules' => ''
        ),
        array(
            'field' => 'numero_unidade_acoplada',
            'label' => '',
            'rules' => ''
        ),
        array(
            'field' => 'anexo',
            'label' => '',
            'rules' => ''
        )
    )
,

    'tipoveiculopneu' => array(array(
        'field' => 'idTipoveiculo',
        'label' => 'Tipo veiculo',
        'rules' => ''
    ),

        array(
            'field' => 'posicao',
            'label' => '',
            'rules' => 'trim|xss_clean'
        )
    )
,

    'filial' => array(array(
        'field' => 'idEmpresa',
        'label' => 'Empresa',
        'rules' => ''
    ),

        array(
            'field' => 'nome',
            'label' => '',
            'rules' => 'trim|xss_clean'
        )
    )
,
    'produtos' => array(array(
        'field' => 'descricao',
        'label' => '',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'unidade',
            'label' => 'Unidade',
            'rules' => 'required|trim|xss_clean'
        ))
,
    'patrimonio' => array(array(
        'field' => 'descricao',
        'label' => '',
        'rules' => 'required|trim|xss_clean'
    ) )
,
    'veiculo' => array(array(
        'field' => 'placa',
        'label' => '',
        'rules' => 'required|trim|xss_clean'
    ) )
,
    'programacao_manutencao_preventiva' => array(array(
        'field' => 'produto_id',
        'label' => '',
        'rules' => ''
    ) )
,
    'usuarios' => array(array(
        'field' => 'nome',
        'label' => 'Nome',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'usuariologin',
            'label' => 'Usuário',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'rg',
            'label' => 'RG',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'cpf',
            'label' => 'CPF',
            'rules' => 'trim|xss_clean|is_unique[usuarios.cpf]'
        ),
        array(
            'field' => 'rua',
            'label' => 'Rua',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'numero',
            'label' => 'Numero',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'bairro',
            'label' => 'Bairro',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'cidade',
            'label' => 'Cidade',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'estado',
            'label' => 'Estado',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|trim|valid_email|xss_clean'
        ),
        array(
            'field' => 'senha',
            'label' => 'Senha',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'telefone',
            'label' => 'Telefone',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'situacao',
            'label' => 'Situacao',
            'rules' => 'required|trim|xss_clean'
        ))
,
    'os' => array(array(
        'field' => 'dataInicial',
        'label' => 'DataInicial',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'dataFinal',
            'label' => 'DataFinal',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'garantia',
            'label' => 'Garantia',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'descricaoProduto',
            'label' => 'DescricaoProduto',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'defeito',
            'label' => 'Defeito',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'status',
            'label' => 'Status',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'observacoes',
            'label' => 'Observacoes',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'clientes_id',
            'label' => 'clientes',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'laudoTecnico',
            'label' => 'Laudo Tecnico',
            'rules' => 'trim|xss_clean'
        ))

,
    'tiposUsuario' => array(array(
        'field' => 'nomeTipo',
        'label' => 'NomeTipo',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'situacao',
            'label' => 'Situacao',
            'rules' => 'required|trim|xss_clean'
        ))

,
    'receita' => array(array(
        'field' => 'nome',
        'label' => 'nome',
        'rules' => 'trim|xss_clean'
    ))
,
    'linkpagamento' => array(array(
        'field' => 'descricao',
        'label' => 'descricao',
        'rules' => 'trim|xss_clean'
    ))
,
    'taxas' => array(array(
        'field' => 'tarifaFixa',
        'label' => 'tarifaFixa',
        'rules' => 'trim|xss_clean'
    ))
,
    'lancamentos' => array(array(
        'field' => 'valor',
        'label' => 'valor'
    ))
,
    'condicaopagamento' => array(array(
        'field' => 'nome',
        'label' => 'nome',
        'rules' => 'trim|xss_clean'
    ))
,
    'despesa' => array(array(
        'field' => 'descricao',
        'label' => 'Descrição',
        'rules' => 'trim|xss_clean'
        ))
,
    'vendas' => array(array(

        'field' => 'dataVenda',
        'label' => 'Data da Venda',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'clientes_id',
            'label' => 'clientes',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        )),

    'nfse' => array(
        array(
            'field' => 'dataEmissao',
            'label' => 'Data de emissao',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'numeroRps',
            'label' => 'Número do RPS',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'serieRps',
            'label' => 'Série do RPS',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'numeroNFSe',
            'label' => 'Número da NFS-e',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'clientes_id',
            'label' => 'clientes',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        )),
    'nfe' => array(
        array(
            'field' => 'modelo',
            'label' => 'Modelo',
        ),
        array(
            'field' => 'serie',
            'label' => 'Série',
        ),
        array(
            'field' => 'nNF',
            'label' => 'Número',
        ),
        array(
            'field' => 'clientes_id',
            'label' => 'clientes',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        )),
    'nfce' => array(
        array(
            'field' => 'modelo',
            'label' => 'Modelo',
        ),
        array(
            'field' => 'serie',
            'label' => 'Série',
        ),
        array(
            'field' => 'nNF',
            'label' => 'Número',
        ),
        array(
            'field' => 'clientes_id',
            'label' => 'clientes',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        )),
    'pedido' => array(array(

        'field' => 'dataPedido',
        'label' => 'Data do Pedido',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'fornecedor_id',
            'label' => 'Fornecedor',
            'rules' => 'trim|xss_clean|required'
        ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        )),
    'transferencia' => array(array(

        'field' => 'dataTransferencia',
        'label' => 'Data da Transferência',
        'rules' => 'required|trim|xss_clean'
    ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean|required'
        )),
    'inventario' => array(array(

        'field' => 'dataInventario',
        'label' => 'Data do Inventário',
        'rules' => 'trim|xss_clean'
    ),
        array(
            'field' => 'usuarios_id',
            'label' => 'usuarios_id',
            'rules' => 'trim|xss_clean'
        ))
);
			   