<?php

class ZTestingBoletoController extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('unit_test');
    }

    function index(){
        $this->unit->run($this->soma(1,1), 2, "Testando a soma de dois numeros", ' O resultado esperado é de 2');

        $this->load->view('test/test');
    }

    public function soma($numero1, $numero2) {
        return $numero1 + $numero2;
    }

}

