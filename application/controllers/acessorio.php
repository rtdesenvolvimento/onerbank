<?php

class acessorio extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('acessorio_model', '', TRUE);
        $this->data['menuAcessorio'] = 'Acessorios';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vAcessorio')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar acessorios.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/acessorio/gerenciar/';
        $config['total_rows'] = $this->acessorio_model->count('acessorio');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->acessorio_model->get('acessorio','idAcessorio,nome,descricao, requer_observacao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'acessorio/acessorio';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {
        

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aAcessorio')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar acessorios.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('acessorio') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'requer_observacao' => set_value('requer_observacao'),
                'descricao' => set_value('descricao')
            );

            if ($this->acessorio_model->add('acessorio', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Acessório adicionado com sucesso!');
                redirect(base_url() . 'index.php/acessorio/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'acessorio/adicionarAcessorio';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eAcessorio')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar acessorios.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('acessorio') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'requer_observacao' => set_value('requer_observacao'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->acessorio_model->edit('acessorio', $data, 'idAcessorio', $this->input->post('idAcessorio')) == TRUE) {
                $this->session->set_flashdata('success', 'Aparelho editado com sucesso!');
                redirect(base_url() . 'index.php/acessorio/editar/'.$this->input->post('idAcessorio'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->acessorio_model->getById($this->uri->segment(3));
        $this->data['view'] = 'acessorio/editarAcessorio';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dAcessrio')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir acessorios.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir acessórios.');
            redirect(base_url().'index.php/acessorio/gerenciar/');
        }

        $this->acessorio_model->delete('acessorio','idAcessorio',$id);

        $this->session->set_flashdata('success','Acessório excluido com sucesso!');
        redirect(base_url().'index.php/acessorio/gerenciar/');
    }
}

