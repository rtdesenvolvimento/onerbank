<?php

class Aparelho extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('aparelho_model', '', TRUE);
        $this->data['menuAparelho'] = 'Aparelho';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vAparelho')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar aparelhos.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/aparelho/gerenciar/';
        $config['total_rows'] = $this->aparelho_model->count('aparelho');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->aparelho_model->get('aparelho','idAparelho,nome,descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'aparelho/aparelhos';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aAparelho')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar aparelhos.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('aparelho') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao')
            );

            if ($this->aparelho_model->add('aparelho', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Aparelho adicionado com sucesso!');
                redirect(base_url() . 'index.php/aparelho/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'aparelho/adicionarAparelho';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eAparelho')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar aparelhos.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('aparelho') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
             $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->aparelho_model->edit('aparelho', $data, 'idAparelho', $this->input->post('idAparelho')) == TRUE) {
                $this->session->set_flashdata('success', 'Aparelho editado com sucesso!');
                redirect(base_url() . 'index.php/aparelho/editar/'.$this->input->post('idAparelho'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->aparelho_model->getById($this->uri->segment(3));
        $this->data['view'] = 'aparelho/editarAparelho';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dAparelho')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir aparelhos.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir aparelho.');
            redirect(base_url().'index.php/aparelho/gerenciar/');
        }

        $this->aparelho_model->delete('aparelho','idAparelho',$id);

        $this->session->set_flashdata('success','Aparelho excluido com sucesso!');
        redirect(base_url().'index.php/aparelho/gerenciar/');
    }
}

