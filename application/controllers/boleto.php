<?php

class boleto extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('mapos_model','',TRUE);

        $this->load->model('financeiro_model','',TRUE);
        $this->load->model('site_model','',TRUE);
        $this->load->model('mapos_model','',TRUE);
        $this->load->model('clientes_model','',TRUE);
        $this->load->model('site_model','',TRUE);
    }

    function index(){}

    public function carne($lancamentoId) {
        $this->load->helper('mpdf');

        $parcelas = $this->financeiro_model->getParcelasByLancamento($lancamentoId);
        $emitente = $this->mapos_model->getEmitente();

        $this->data['parcelas'] = $parcelas;
        $this->data['emitente'] = $emitente;

        $this->data['view'] = 'boleto/carne';
        $html = $this->load->view('tema/blank', $this->data, TRUE);

        echo $html;
    }

    public function gerarBoleto($cnpjempresa, $transactions_integration_id) {

        $this->load->helper('mpdf');

        $otherdb = $this->load->database($cnpjempresa, TRUE);
        $otherdb->where('transactions_integration_id',$transactions_integration_id);
        $otherdb->limit(1);
        $boleto = $otherdb->get('boleto')->row();
        $url = $boleto->url_boleto;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    public function enviarEmail()
    {
        $this->load->library('email');
        $emitente = $this->mapos_model->getEmitente();

        $assunto = $this->input->post('assunto');
        $mensagem = $this->input->post('mensagem');
        $email = $this->input->post('para');
        $transactions_integration_id = $this->input->post('transactions_integration_id');

        $urlBoleto = '<a href="'.base_url().'boleto/gerarBoleto/'.$this->session->userdata('cnpjempresa').'/'.$transactions_integration_id.'" target="_blank">Clique aqui para exibir o boleto.</a>';
        $mensagem = str_replace('[LINK_BOLETO]', $urlBoleto,  $mensagem);

        $config = $this->getConfiguracaoEnvioEmail($emitente);

        $this->email->initialize($config);

        if ($emitente->email) $this->email->from($emitente->email, $emitente->nome);
        else $this->email->from('envioboleto@onerbank.com.br', $emitente->nome);

        if ($email)  $this->email->to($email);

        $this->email->subject($assunto);
        $this->email->message($mensagem);

        $this->email->send();
        //echo $this->email->print_debugger();

        $this->session->set_flashdata('success', 'E-mail enviado com sucesso!');
        redirect(base_url() . 'index.php/financeiro');
    }

    function notifica($id, $cnpjempresa) {

        $emitente   =$this->getEmitente($cnpjempresa);

        $boleto = $this->getBoletoByIdTransacao($id, $cnpjempresa);
        $fatura = $this->getFaturaById($boleto->fatura_id, $cnpjempresa);
        $parcela = $this->getParcelaById($boleto->parcela_id, $cnpjempresa);
        $lancamento = $this->getLancamentoById($parcela->lancamentos_id,$cnpjempresa);
        $cliente    =  $this->getClienteByid($lancamento->clientes_id, $cnpjempresa);
        $receita    =  $this->getReceitaById($lancamento->receita_id, $cnpjempresa);

        $this->data['fatura'] = $fatura;
        $this->data['emitente'] = $emitente;
        $this->data['boleto'] = $boleto;
        $this->data['cliente'] = $cliente;
        $this->data['receita'] = $receita;

        $this->data['view'] = 'financeiro/notificacaoboleto';
        $this->load->view('tema/blank', $this->data);
    }


    public function action($cnpjempresa) {
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $emitente   = $this->getEmitente($cnpjempresa);
        $qtdDiasVencimentoEnviarCobranca = $emitente->qtdDiasVencimentoEnviarCobranca;

        $where = "status = 'FATURADA' and
                  DATEDIFF (dtVencimento, '".date('Y-m-d')."')  > -".$qtdDiasVencimentoEnviarCobranca." and 
                 DATEDIFF (dtVencimento, '".date('Y-m-d')."') <= ".$qtdDiasVencimentoEnviarCobranca;
        $otherdb->where($where);

        $parcelas =  $otherdb->get('parcela')->result();

        foreach ($parcelas as $parcela) {
            $boleto = $this->getBoletoByParcela($parcela->idParcela, $cnpjempresa);

            if (count($boleto) > 0 ) {

                $this->load->library('email');

                $boleto = $this->getBoletoByIdTransacao($boleto->transactions_integration_id, $cnpjempresa);
                $fatura = $this->getFaturaById($boleto->fatura_id, $cnpjempresa);
                $parcela = $this->getParcelaById($boleto->parcela_id, $cnpjempresa);
                $lancamento = $this->getLancamentoById($parcela->lancamentos_id, $cnpjempresa);
                $cliente = $this->getClienteByid($lancamento->clientes_id, $cnpjempresa);
                $receita = $this->getReceitaById($lancamento->receita_id, $cnpjempresa);

                $this->data['fatura'] = $fatura;
                $this->data['emitente'] = $emitente;
                $this->data['boleto'] = $boleto;
                $this->data['cliente'] = $cliente;
                $this->data['receita'] = $receita;

                $email = $cliente->email;
                echo 'email enviado para '.$email;

                $config = $this->getConfiguracaoEnvioEmail($emitente);

                $this->email->initialize($config);

                $this->email->from($emitente->email, $emitente->nome);

                if ($email) $this->email->to($email);

                $this->data['view'] = 'financeiro/notificacaoboleto';
                $mensagem = $this->load->view('tema/blank', $this->data, true);

                $this->email->subject('Boleto cód. ' . $boleto->idBoleto . ' emitido para você por ' . $emitente->nome);
                $this->email->message($mensagem);

                $this->email->send();
                //echo $this->email->print_debugger();
            }
        }
    }

    function getConfiguracaoEnvioEmail($emitente) {
        $config = array(
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => '587',
            'smtp_user' => 'onerbanksystem@gmail.com',
            'smtp_pass' => 'onerbank2020@',
            'crlf' => "\r\n",
            'protocol' => 'sendmail',//smtp',
            'validation' => TRUE,
            'smtp_crypto' => 'tls',
            'starttls' => false,
        );

        // Send email
        $config['useragent'] = $emitente->nome;
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        return $config;
    }

    function getBoletoByParcela($parcela_id, $cnpjempresa) {
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $fatura = $this->getFaturaByParcela($parcela_id, $cnpjempresa);
        $otherdb->where('fatura_id', $fatura->fatura_id);
        $otherdb->where('url_boleto is not null');//TODO VERIFICAR SE E UM BOLETO VALIDO
        $otherdb->limit(1);
        return $otherdb->get('boleto')->row();
    }

    function getFaturaByParcela($idParcela, $cnpjempresa){

        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('parcela_id',$idParcela);
        $otherdb->limit(1);
        return $otherdb->get('fatura_parcela')->row();
    }

    public function email($id, $cnpjempresa)
    {
        $this->load->library('email');

        $emitente   =$this->getEmitente($cnpjempresa);

        $boleto = $this->getBoletoByIdTransacao($id, $cnpjempresa);
        $fatura = $this->getFaturaById($boleto->fatura_id, $cnpjempresa);
        $parcela = $this->getParcelaById($boleto->parcela_id, $cnpjempresa);
        $lancamento = $this->getLancamentoById($parcela->lancamentos_id,$cnpjempresa);
        $cliente    =  $this->getClienteByid($lancamento->clientes_id, $cnpjempresa);
        $receita    =  $this->getReceitaById($lancamento->receita_id, $cnpjempresa);

        $this->data['fatura'] = $fatura;
        $this->data['emitente'] = $emitente;
        $this->data['boleto'] = $boleto;
        $this->data['cliente'] = $cliente;
        $this->data['receita'] = $receita;

        $email = $cliente->email;

        $config = $this->getConfiguracaoEnvioEmail($emitente);

        $this->email->initialize($config);

        $this->email->from($emitente->email, $emitente->nome);

        if ($email) $this->email->to($email);

        $this->data['view'] = 'financeiro/notificacaoboleto';
        $mensagem = $this->load->view('tema/blank', $this->data, true);

        $this->email->subject('Boleto cód. '.$boleto->idBoleto.' emitido para você por '.$emitente->nome);
        $this->email->message($mensagem);

        $this->email->send();
        //echo $this->email->print_debugger();
    }


    function getEmitente($cnpjempresa)
    {
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->limit(1);
        $otherdb->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id');
        return $otherdb->get('emitente')->row();
    }

    function getBoletoByIdTransacao($transaction_id, $cnpjempresa){
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);
        $otherdb->where('transactions_integration_id',$transaction_id);
        $otherdb->limit(1);
        return $otherdb->get('boleto')->row();
    }

    function getFaturaById($id, $cnpjempresa){
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('idFatura',$id);
        $otherdb->limit(1);
        return $otherdb->get('fatura')->row();
    }

    function getParcelaById($id, $cnpjempresa){
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('idParcela',$id);
        $otherdb->limit(1);
        return $otherdb->get('parcela')->row();
    }

    function getClienteByid($id, $cnpjempresa){
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('idClientes',$id);
        $otherdb->limit(1);
        return $otherdb->get('clientes')->row();
    }

    function getLancamentoById($id, $cnpjempresa){
        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('idLancamentos',$id);
        $otherdb->limit(1);
        return $otherdb->get('lancamentos')->row();
    }

    function getReceitaById($id, $cnpjempresa)
    {

        $this->load->helper('mpdf');
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('idReceita', $id);
        $otherdb->limit(1);
        return $otherdb->get('receita')->row();
    }
}