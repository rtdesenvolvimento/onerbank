<?php

class Clientes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            //redirect('mapos/login');
        }

        $this->load->helper(array('codegen_helper'));
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('veiculos_model', '', TRUE);
        $this->load->model('tipoveiculo_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('financeiro_model', '', TRUE);
        $this->load->model('zoop_model', '', TRUE);

        $this->data['menuClientes'] = 'clientes';
        $this->data['menuCadastro'] = 'Cadastro';
    }

    function index()
    {
        $this->gerenciar();
    }

    function buscar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar clientes.');
            redirect(base_url());
        }
        $this->load->library('table');
        $this->load->library('pagination');


        $config['base_url'] = base_url() . 'index.php/clientes/gerenciar/';
        $config['total_rows'] = $this->clientes_model->count('clientes');
        $config['per_page'] = 1000000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $parametro = $this->uri->segment(3);
        $parametro = str_replace("%20", "%", $parametro);

        $this->pagination->initialize($config);
        $where = '';
        if ($parametro) {
            $where = 'nomeCliente` like "%' . $parametro . '%" 
						OR documento like "%' . $parametro . '%"  
						OR telefone like "%' . $parametro . '%"
						OR celular like "%' . $parametro . '%"
						OR rua like "%' . $parametro . '%"
						OR bairro like "%' . $parametro . '%"
						OR cidade like "%' . $parametro . '%"
						OR estado like "%' . $parametro . '%"
						OR cep like "%' . $parametro . '%"						
						OR email like "%' . $parametro . '%"';
        }

        if ($parametro) {
            $where .= ' OR idClientes  in ( select cliente_id from veiculos 
										where 
											modelo LIKE "%' . $parametro . '%" 
											OR marca LIKE "%' . $parametro . '%" 
											OR placa LIKE "%' . $parametro . '%" 
											OR tipoVeiculo LIKE "%' . $parametro . '%" 
											OR cor LIKE "%' . $parametro . '%" 
											OR ano LIKE "%' . $parametro . '%" 
									)';
        }

        $this->data['results']  = $this->clientes_model->get('clientes', '*', $where, $config['per_page'], '');
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['termo']    = $parametro;
        $this->data['view'] = 'clientes/clientes';


        $this->load->view('theme/topo', $this->data);
    }

    function gerenciar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar clientes.');
            redirect(base_url());
        }
        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/clientes/gerenciar/';
        $config['total_rows'] = $this->clientes_model->count('clientes');
        $config['per_page'] = 1000000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->clientes_model->get('clientes', 'idClientes,nomeCliente,documento,telefone,celular,email,rua,numero,bairro,cidade,estado,cep,inativo,observacaoInativo, origem, observacao', '', $config['per_page'], $this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['termo']    = '';
        $this->data['view'] = 'clientes/clientes';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar clientes.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $senha = $this->input->post('senha');
            if (!$senha) {
                $senha = $this->input->post('documento');
            }

            //$this->load->library('encrypt');
            //$senha = $this->encrypt->sha1($senha);

            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeCliente'           => $this->input->post('nomeCliente'),
                'origem'                => $this->input->post('origem'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'IE'                    => $this->input->post('IE'),
                'IESUF'                 => $this->input->post('IESUF'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeCliente'    => $this->input->post('contatoNomeCliente'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'dataCadastro'          => date('Y-m-d'),
                'senha'                 => $senha
            );

            $idCliente = $this->clientes_model->add('clientes', $data, TRUE);

            if ($idCliente) {
                $this->session->set_flashdata('success', 'Cliente adicionado com sucesso!');
                redirect(base_url() . 'index.php/clientes/editar/'.$idCliente);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'clientes/adicionarCliente';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionarIframe() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar clientes.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $senha = $this->input->post('senha');
            if (!$senha) {
                $senha = $this->input->post('documento');
            }

            //$this->load->library('encrypt');
            //$senha = $this->encrypt->sha1($senha);

            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeCliente'           => $this->input->post('nomeCliente'),
                'origem'                => $this->input->post('origem'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'IE'                    => $this->input->post('IE'),
                'IESUF'                 => $this->input->post('IESUF'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeCliente'    => $this->input->post('contatoNomeCliente'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'dataCadastro'          => date('Y-m-d'),
                'senha'                 => $senha
            );

            $idCliente = $this->clientes_model->add('clientes', $data, TRUE);

            if ($idCliente) {
                $this->session->set_flashdata('success', 'Cliente adicionado com sucesso!');
                redirect(base_url() . 'index.php/clientes/editar/'.$idCliente);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'clientes/adicionarCliente';
        $this->load->view('tema/blank_script', $this->data);
    }

    function editarIframe() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar clientes.');
            redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $senha = $this->input->post('senha');
            if (!$senha) {
                $senha = $this->input->post('documento');
            }

            //$this->load->library('encrypt');
            //$senha = $this->encrypt->sha1($senha);

            $inativo            =  $this->input->post('inativo');
            $observacaoInativo  =  $this->input->post('observacaoInativo');

            if ($inativo == 'on') {
                $inativo = 1;
            } else {
                $observacaoInativo = '';
                $inativo = 0;
            }

            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeCliente'           => $this->input->post('nomeCliente'),
                'origem'                => $this->input->post('origem'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'IE'                    => $this->input->post('IE'),
                'IESUF'                 => $this->input->post('IESUF'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeCliente'    => $this->input->post('contatoNomeCliente'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'inativo'               => $inativo,
                'observacaoInativo'     => $observacaoInativo,
                'senha'                 => $senha
            );

            if ($this->clientes_model->edit('clientes', $data, 'idClientes', $this->input->post('idClientes')) == TRUE) {
                $this->session->set_flashdata('success', 'Cliente editado com sucesso!');
                redirect(base_url() . 'index.php/clientes/editar/' . $this->input->post('idClientes'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['tiposveiculo'] = $this->tipoveiculo_model->getAll();
        $this->data['veiculos'] = $this->clientes_model->getVeiculos($this->uri->segment(3));
        $this->data['result'] = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['view'] = 'clientes/editarCliente';
        $this->load->view('tema/blank_script', $this->data);
    }

    function adicionarAjax()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar clientes.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $cliente_id = $this->input->post('cliente_id');

            $senha = $this->input->post('senha');
            if (!$senha) {
                $senha = $this->input->post('documento');
            }

            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeCliente'           => $this->input->post('nomeCliente'),
                'origem'                => $this->input->post('origem'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'IE'                    => $this->input->post('IE'),
                'IESUF'                 => $this->input->post('IESUF'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeCliente'    => $this->input->post('contatoNomeCliente'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'dataCadastro'          => date('Y-m-d'),
                'senha'                 => $senha
            );

            if ($cliente_id) {
                 $this->clientes_model->edit('clientes', $data, 'idClientes', $cliente_id);
            } else  {
                $cliente_id = $this->clientes_model->add('clientes', $data, TRUE);
            }

            if ($cliente_id) {
                echo json_encode($this->clientes_model->getById($cliente_id));
            }
        }
    }

    function consultaCliente() {
        $idCliente = $this->input->post('cliente_id');

        if ($idCliente)  echo json_encode($this->clientes_model->getById($idCliente));
        else echo json_decode([]);
    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar clientes.');
            redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('clientes') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $senha = $this->input->post('senha');
            if (!$senha) {
                $senha = $this->input->post('documento');
            }

            //$this->load->library('encrypt');
            //$senha = $this->encrypt->sha1($senha);

            $inativo            =  $this->input->post('inativo');
            $observacaoInativo  =  $this->input->post('observacaoInativo');

            if ($inativo == 'on') {
                $inativo = 1;
            } else {
                $observacaoInativo = '';
                $inativo = 0;
            }

            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeCliente'           => $this->input->post('nomeCliente'),
                'origem'                => $this->input->post('origem'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'IE'                    => $this->input->post('IE'),
                'IESUF'                 => $this->input->post('IESUF'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeCliente'    => $this->input->post('contatoNomeCliente'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'inativo'               => $inativo,
                'observacaoInativo'     => $observacaoInativo,
                'senha'                 => $senha
            );

            if ($this->clientes_model->edit('clientes', $data, 'idClientes', $this->input->post('idClientes')) == TRUE) {
                $this->session->set_flashdata('success', 'Cliente editado com sucesso!');
                redirect(base_url() . 'index.php/clientes');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['tiposveiculo'] = $this->tipoveiculo_model->getAll();
        $this->data['veiculos'] = $this->clientes_model->getVeiculos($this->uri->segment(3));
        $this->data['result'] = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['view'] = 'clientes/editarCliente';
        $this->load->view('theme/topo', $this->data);
    }

    public function profile()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar clientes.');
            redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->data['veiculos'] = $this->clientes_model->getVeiculos($this->uri->segment(3));
        $this->data['vendas'] = $this->clientes_model->getVendas($this->uri->segment(3));
        $this->data['result'] = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['results'] = $this->clientes_model->getOsByCliente($this->uri->segment(3));
        $this->data['view'] = 'clientes/profile';
        $this->load->view('theme/topo', $this->data);
    }

    public function visualizar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar clientes.');
            redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->data['veiculos'] = $this->clientes_model->getVeiculos($this->uri->segment(3));
        $this->data['vendas'] = $this->clientes_model->getVendas($this->uri->segment(3));
        $this->data['result'] = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['results'] = $this->clientes_model->getOsByCliente($this->uri->segment(3));
        $this->data['view'] = 'clientes/visualizar';
        $this->load->view('tema/topo', $this->data);
    }

    public function excluir()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para excluir clientes.');
            redirect(base_url());
        }

        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir cliente.');
            redirect(base_url() . 'index.php/clientes/gerenciar/');
        }

        //$id = 2;
        // excluindo OSs vinculadas ao cliente
        $this->db->where('clientes_id', $id);
        $os = $this->db->get('os')->result();

        if ($os != null) {

            foreach ($os as $o) {
                $this->db->where('os_id', $o->idOs);
                $this->db->delete('servicos_os');

                $this->db->where('os_id', $o->idOs);
                $this->db->delete('produtos_os');

                $this->db->where('idOs', $o->idOs);
                $this->db->delete('os');
            }
        }

        // excluindo Vendas vinculadas ao cliente
        $this->db->where('clientes_id', $id);
        $vendas = $this->db->get('vendas')->result();

        if ($vendas != null) {

            foreach ($vendas as $v) {
                $this->db->where('vendas_id', $v->idVendas);
                $this->db->delete('itens_de_vendas');

                $this->db->where('idVendas', $v->idVendas);
                $this->db->delete('vendas');
            }
        }

        //excluindo receitas vinculadas ao cliente
        $this->db->where('clientes_id', $id);
        $this->db->delete('lancamentos');


        $this->clientes_model->delete('clientes', 'idClientes', $id);

        $this->session->set_flashdata('success', 'Cliente excluido com sucesso!');
        redirect(base_url() . 'index.php/clientes/gerenciar/');
    }


    public function adicionarVeiculo()
    {

        $modelo             = $this->input->post('modelo');
        $marca              = $this->input->post('marca');
        $placa              = $this->input->post('placa');
        $cor                = $this->input->post('cor');
        $ano                = $this->input->post('ano');
        $tipoVeiculo        = $this->input->post('tipoVeiculo');
        $idClienteProduto   = $this->input->post('idClienteProduto');
        $idVeiculo          = $this->input->post('idVeiculo');
        $chassi             = $this->input->post('chassi');
        $observacao         = $this->input->post('observacao');
        $tipoveiculo_id     = $this->input->post('tipoveiculo_id');
        $municipio          = $this->input->post('municipio');
        $uf                 = $this->input->post('uf');
        $situacao           = $this->input->post('situacao');
        $gasolina           = $this->input->post('gasolina');
        $etanol             = $this->input->post('etanol');
        $gnv                = $this->input->post('gnv');
        $diesel             = $this->input->post('diesel');

        $data = array(
            'modelo'            => $modelo,
            'chassi'            => $chassi,
            'observacao'        => $observacao,
            'marca'             => $marca,
            'placa'             => $placa,
            'cor'               => $cor,
            'ano'               => $ano,
            'tipoVeiculo'       => $tipoVeiculo,
            'tipoveiculo_id'    => $tipoveiculo_id,
            'cliente_id'        => $idClienteProduto,
            'municipio'         => $municipio,
            'uf'                => $uf,
            'situacao'          => $situacao,
            'gasolina'          => $gasolina,
            'etanol'            => $etanol,
            'gnv'               => $gnv,
            'diesel'            => $diesel
        );

        if ($idVeiculo) {
            if ($this->clientes_model->edit('veiculos', $data, 'idVeiculo', $idVeiculo) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            $veiculo_id = $this->clientes_model->add('veiculos', $data, TRUE);
            if ($veiculo_id) {
                for($i=1;$i<=10;$i++) {
                    $manutencao = array(
                        'indice'        => $i,
                        'veiculo_id'    => $veiculo_id,
                    );
                    $this->clientes_model->add('manutencao_preventiva_veiculo', $manutencao);
                }
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        }
    }

    function adicionarFotoVeiculo() {

        $this->load->library('upload');
        $this->load->library('image_lib');

        $upload_conf = array(
            'upload_path' => realpath('./assets/anexos'),
            'encrypt_name' => TRUE,
            'allowed_types' => 'jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG|pdf|PDF|cdr|CDR|docx|DOCX|txt', // formatos permitidos para anexos de os
            'max_size' => 0,
        );

        $this->upload->initialize($upload_conf);

        // Change $_FILES to new vars and loop them
        foreach ($_FILES['userfile'] as $key => $val) {
            $i = 1;
            foreach ($val as $v) {
                $field_name = "file_" . $i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }
        // Unset the useless one ;)
        unset($_FILES['userfile']);

        // Put each errors and upload data to an array
        $error = array();
        $success = array();

        // main action to upload each file
        foreach ($_FILES as $field_name => $file) {
            if (!$this->upload->do_upload($field_name)) {
                // if upload fail, grab error
                $error['upload'][] = $this->upload->display_errors();
            } else {
                // otherwise, put the upload datas here.
                // if you want to use database, put insert query in this loop
                $upload_data = $this->upload->data();

                if ($upload_data['is_image'] == 1) {

                    // set the resize config
                    $resize_conf = array(
                        // it's something like "/full/path/to/the/image.jpg" maybe
                        'source_image' => $upload_data['full_path'],
                        // and it's "/full/path/to/the/" + "thumb_" + "image.jpg
                        // or you can use 'create_thumbs' => true option instead
                        'new_image' => $upload_data['file_path'] . 'thumbs/thumb_' . $upload_data['file_name'],
                        'width' => 200,
                        'height' => 125
                    );

                    // initializing
                    $this->image_lib->initialize($resize_conf);

                    // do it!
                    if (!$this->image_lib->resize()) {
                        // if got fail.
                        $error['resize'][] = $this->image_lib->display_errors();
                    } else {
                        // otherwise, put each upload data to an array.
                        $success[] = $upload_data;
                        $this->clientes_model->anexarArquivoVeiculo($this->input->post('idVeiculoArquivo'),$this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./assets/anexos/'));

                    }
                } else {
                    $success[] = $upload_data;
                    $this->clientes_model->anexarArquivoVeiculo($this->input->post('idVeiculoArquivo'),$this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', '', realpath('./assets/anexos/'));
                }
            }
        }

        // see what we get
        if (count($error) > 0) {
            //print_r($data['error'] = $error);
            echo json_encode(array('result' => false, 'mensagem' => 'Nenhum arquivo foi anexado.'));
        } else {
            //print_r($data['success'] = $upload_data);
            echo json_encode(array('result' => true, 'mensagem' => 'Arquivo(s) anexado(s) com sucesso .'));
        }
    }

    function buscar_veiculo() {
        $veiculo_id  = $this->input->post('veiculo_id');
        echo json_encode($this->veiculos_model->getById($veiculo_id));
    }

    function excluirVeiculo()
    {
        $ID = $this->input->post('idVeiculo');
        if ($this->clientes_model->delete('veiculos', 'idVeiculo', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    function getVeiculos() {
        $cliente_id = $this->input->post('cliente_id');
        echo json_encode($this->clientes_model->getVeiculos($cliente_id));
    }

    function search() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar clientes.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $termo = $this->input->post('termo');;

        $config['base_url'] = base_url() . 'index.php/clientes/search/';
        $config['total_rows'] = $this->clientes_model->count('clientes');

        if (!$termo) {
            $config['per_page'] = 1000000;
        } else {
            $config['per_page'] = 10000;
        }

        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->clientes_model->search($termo, $config['per_page'], $this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['termo']    = $termo;
        $this->data['view']     = 'clientes/search';
        $this->load->view('tema/blank', $this->data);
    }

    function getConsutaWSCliente($cnpj) {
        header('Content-Type: application/json');
        $url = "https://www.receitaws.com.br/v1/cnpj/".$cnpj;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if(!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    function cadVeiculoModal() {
        $this->data['tiposveiculo'] = $this->tipoveiculo_model->getAll();
        $this->data['view']         = 'veiculos/cadVeiculoModal';
        $this->load->view('tema/blank', $this->data);
    }

    public function excluirAnexo($id = null)
    {
        if ($id == null || !is_numeric($id)) {
            echo json_encode(array('result' => false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
        } else {

            $this->db->where('idFotoVeiculo', $id);
            $file = $this->db->get('arquivos_veiculo', 1)->row();

            unlink($file->path . '/' . $file->anexo);

            if ($file->thumb != null) {
                unlink($file->path . '/thumbs/' . $file->thumb);
            }

            if ($this->clientes_model->delete('arquivos_veiculo', 'idFotoVeiculo', $id) == true) {
                echo json_encode(array('result' => true, 'mensagem' => 'Anexo excluído com sucesso.'));
            } else {
                echo json_encode(array('result' => false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
            }
        }
    }

    public function downloadanexo($id = null)
    {

        if ($id != null && is_numeric($id)) {

            $this->db->where('idFotoVeiculo', $id);
            $file = $this->db->get('arquivos_veiculo', 1)->row();

            $this->load->library('zip');

            $path = $file->path;

            $this->zip->read_file($path . '/' . $file->anexo);

            $this->zip->download('file' . date('d-m-Y-H.i.s') . '.zip');
        }
    }

    public function suggestions() {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->clientes_model->suggestions($q);
            $this->send_json($rows);
        }
    }

    public function getCliente($id) {
        if ($id) {
            $cliente = $this->clientes_model->getCliente($id);
            $this->send_json(array(array('id' => $cliente->id, 'text' =>  $cliente->text)));
        }
    }

    public function send_json($data) {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    function pageCadCliente() {

        $this->load->library('encrypt');

        $cnpjempresa    = $this->uri->segment(3);
        $usuariologin   = 'admin';//TODO
        $senha          = '123456';//TODO
        $filial         = 1;//TODO
        $senha = $this->encrypt->sha1($senha);

        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $otherdb->where('usuariologin', $usuariologin);
        $otherdb->where('senha', $senha);
        $otherdb->where('situacao', 1);
        $otherdb->limit(1);
        $usuario = $otherdb->get('usuarios')->row();

        if (count($usuario) > 0) {
            $dados = array(
                'nome'          => $usuario->nome,
                'id'            => $usuario->idUsuarios,
                'setor_id'      => $usuario->setor_id,
                'permissao'     => $usuario->permissoes_id,
                'filial_id'     => $filial,
                'cnpjempresa'   => $cnpjempresa,
                'logado'        => TRUE);

            if (!isset($_SESSION)):
                session_start();
            endif;

            $this->session->set_userdata($dados);
            $_SESSION['BD'] = $cnpjempresa;
        }
        redirect(base_url()."clientes/emitirBoleto/".$cnpjempresa);
    }

    function emitirBoleto() {

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('clientes/pageCadCliente/'.$this->uri->segment(3));
        }

        $this->data['emitente'] =  $this->mapos_model->getEmitente();
        $this->data['view']     = 'clientes/cadPessoaOnlineIntegracao';
        $this->load->view('tema/blank', $this->data);
    }


    function cadPessoaOnlineIntegracaoSubmit() {

        $tipoPessoa = $this->input->post('tipoPessoa');
        $cpf = $this->input->post('cpf');
        $nome = $this->input->post('nome');
        $sexo = $this->input->post('sexo');
        $rg = $this->input->post('rg');
        $celular = $this->input->post('celular');
        $email = $this->input->post('email');
        $cep = $this->input->post('cep');
        $endereco =  $this->input->post('endereco');
        $numero = $this->input->post('numero');
        $complemento = $this->input->post('complemento');
        $bairro = $this->input->post('bairro');
        $cidade = $this->input->post('cidade');
        $estado = $this->input->post('estado');

        if ($tipoPessoa == 'pf') $tipoPessoa = 'PF';
        else $tipoPessoa = 'PJ';

        $data = array(
             'tipoPessoa'            => $tipoPessoa,
             'nomeCliente'           => $nome,
             'sexo'                  => $sexo,
             'documento'             => $cpf,
             'rg'                    => $rg,
             'celular'               => $celular,
             'email'                 => $email,
             'rua'                   => $endereco,
             'numero'                => $numero,
             'bairro'                => $bairro,
             'cidade'                => $cidade,
             'estado'                => $estado,
             'cep'                   => $cep,
             'complemento'           => $complemento,
             'dataCadastro'          => date('Y-m-d')
         );

         $cliente  =  $this->clientes_model->getByDocumento($cpf);

         if (count($cliente) > 0) {
             $this->clientes_model->edit('clientes', $data, 'idClientes',$cliente->idClientes);
             $idCliente = $cliente->idClientes;
         } else {
             $idCliente = $this->clientes_model->add('clientes', $data, TRUE);
         }

         $cliente = $this->clientes_model->getById($idCliente);
         $this->cadClienteZoop($cliente);

         $this->send_json($cliente);
    }

    function cadClienteZoop($cliente) {

        if ($cliente->client_integration_id == '') {
            $jsonRetorno = json_decode($this->zoop_model->postCadBuyers($cliente));
            if ($jsonRetorno->id != null) $this->clientes_model->edit('clientes', array('client_integration_id' => $jsonRetorno->id), 'idClientes', $cliente->idClientes);
        } else {
            $jsonRetorno = json_decode($this->zoop_model->putGetCadBuyers($cliente));
            if ($jsonRetorno->id != null) $this->clientes_model->edit('clientes', array('client_integration_id' => $jsonRetorno->id), 'idClientes', $cliente->idClientes);
        }
    }

    function cadFinanceiroOnlineIntegracaoSubmit() {

        $valorVencimento    = $this->input->post('valorVencimento');
        $dataVencimento     = $this->input->post('dataVencimento');
        $limitePagamento    = $this->input->post('limitePagamento');
        $descricaoBoleto    = $this->input->post('descricaoBoleto');
        $cliente_id         = $this->input->post('idPessoaPrincipal');
        $dataVencimento     = date("Y-m-d", strtotime($dataVencimento));
        $receita_id         = 1;
        $condicao_pagamento_id = 1;

        $data = array(
            'descricao'             => $descricaoBoleto,
            'valor'                 => $valorVencimento,
            'receita_id'            => $receita_id,
            'clientes_id'           => $cliente_id,
            'data_vencimento'       => $dataVencimento,
            'tipo'                  => 'receita',
            'condicao_pagamento_id' => $condicao_pagamento_id
        );
        $idLancamento = $this->financeiro_model->add('lancamentos',$data, [$valorVencimento], [$dataVencimento], 0);

        $this->gerarBoletoZoop($idLancamento, $limitePagamento, $descricaoBoleto);
    }

    function gerarBoletoZoop($idLancamento, $carencia, $descricaoBoleto) {

        $parcelas = $this->financeiro_model->getParcelasByLancamento($idLancamento);
        $boleto = null;

        foreach ($parcelas as $parcela) {
            $faturas = $this->financeiro_model->getFaturasParcelaByLancamento($parcela->idParcela);
            foreach ($faturas as $fat) {

                $fatura     =  $this->financeiro_model->getFaturaById($fat->fatura_id);
                $cliente    =  $this->clientes_model->getById($fatura->clientes_id);

                if ($cliente->client_integration_id != '') {
                    $jsonRetorno = json_decode($this->zoop_model->postCadTransactionsBoleto($fatura, $cliente, $carencia, $descricaoBoleto));

                    $boleto = array(
                        'cliente_id' => $cliente->idClientes,
                        'fatura_id' => $fatura->idFatura,
                        'parcela_id' => $parcela->idParcela,
                        'transactions_integration_id' => $jsonRetorno->id,
                        'url_boleto' => $jsonRetorno->payment_method->url,
                        'transactions_status' => $jsonRetorno->status,
                        'transactions_status_boleto' => $jsonRetorno->payment_method->status,
                        'barcode' => $jsonRetorno->payment_method->barcode,
                    );
                    $boletoId = $this->clientes_model->add('boleto', $boleto, TRUE);

                }
            }
        }

        $boleto = $this->financeiro_model->getBoletoById($boletoId);
        if ($boleto->transactions_integration_id) $this->send_json($boleto);
        else  $this->send_json($jsonRetorno);
    }

    function verificarCadatro() {
        $cpf = $this->input->post('cpf');
        $cliente  =  $this->clientes_model->getByDocumento($cpf);
        if (count($cliente) > 0 ) $this->send_json($this->clientes_model->getById($cliente->idClientes));
        else $this->send_json("[]");
    }

}

