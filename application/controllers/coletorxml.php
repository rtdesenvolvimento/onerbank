<?php

class coletorxml extends CI_Controller {

    function __construct() {

        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('coletorxml_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('mapos_model','', TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->data['menuColetorXML'] = 'Coletor de XML';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/coletorxml/gerenciar/';
        $config['total_rows'] = $this->coletorxml_model->count('manifesta');
        $config['per_page'] = 200;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->coletorxml_model->get('manifesta','*','',$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'coletorxml/coletorxml';
        $this->load->view('theme/topo',$this->data);
    }

    function uploadXMLNF() {

        $cpt = count($_FILES['files']['name']);
        $files = $_FILES;

        for($i=0; $i<$cpt; $i++) {

            $_FILES['files']['name']    = $files['files']['name'][$i];
            $_FILES['files']['type']    = $files['files']['type'][$i];
            $_FILES['files']['tmp_name']= $files['files']['tmp_name'][$i];
            $_FILES['files']['error']   = $files['files']['error'][$i];
            $_FILES['files']['size']    = $files['files']['size'][$i];

            $xmlNF = $this->do_upload();

            if ($xmlNF['file_name'] != '') {
                $this->lendoXML($xmlNF['file_name']);
            }
        }
        $this->gerenciar();
    }

    function lendoXML($nomeArquivo) {
        // Transformando arquivo XML em Objeto
        $origem = './assets/arquivos/xml/temporario/'.$nomeArquivo;

        $importarNotasSaida = $this->input->post('importarNotasSaida');

        $xml    = simplexml_load_file($origem);
        $CNPJ   = $this->session->userdata('cnpjempresa');
        $xNome  = '';
        $chave  = '';
        $vNF    = 0;
        $mod    = '55';

        foreach($xml->NFe  as $NFe):
            foreach($NFe->infNFe  as $infNFe):

                foreach($infNFe->ide  as $ide):

                    $dhEmiT = $ide->dhEmi;
                    $chave  = str_replace ('NFe','',$infNFe['Id'][0]);
                    $dhEmi  = date('d/m/Y', strtotime($dhEmiT));
                    $mod    = $ide->mod;

                endforeach;

                foreach($infNFe->emit  as $emit):
                    $xNome = $emit->xNome;
                endforeach;

                if ($mod == '55') {
                    if ($importarNotasSaida == 1) {
                        foreach ($infNFe->dest as $dest):
                            $xNome = $dest->xNome;
                        endforeach;
                    }
                }

                foreach($infNFe->total  as $total):
                    foreach($total->ICMSTot  as $ICMSTot):
                        $vNF = $ICMSTot->vNF;
                    endforeach;
                endforeach;
            endforeach;
        endforeach;

        if ($CNPJ != '') {

            $verifica = $this->coletorxml_model->getByChave($chave);

            if (count($verifica) == 0) {
                $data = array(
                    'chave' => $chave,
                    'data' => $dhEmi,
                    'xNome' => $xNome . '',
                    'valor' => $vNF,
                    'manifestada' => 'S',
                    'donwload' => 'S',
                    'importada' => 'N',
                );

                $this->coletorxml_model->add('manifesta', $data, TRUE);

                $destino = './emissores/v4/nfephp-master/XML/' . $CNPJ . '/NF-e/producao/recebidas/' . $chave . '-nfe.xml';
                if (copy($origem, $destino)) ;

            } else {
                $data = array(
                    'chave' => $chave,
                    'data' => $dhEmi,
                    'xNome' => $xNome . '',
                    'valor' => $vNF,
                    'manifestada' => 'S',
                    'donwload' => 'S',
                    'importada' => 'N',
                );

                $this->coletorxml_model->edit('manifesta', $data, 'manifestaid', $verifica->manifestaid);
                $destino = './emissores/v4/nfephp-master/XML/' . $CNPJ . '/NF-e/producao/recebidas/' . $chave . '-nfe.xml';
                if (copy($origem, $destino));

            }
        }
    }


    public function do_upload(){

        $config['upload_path']      = './assets/arquivos/xml/temporario/';
        $config['allowed_types']    = 0;
        $config['max_size']         = 0;
        $config['max_width']        = '3000';
        $config['max_height']       = '2000';
        $config['encrypt_name']     = false;

        if (!is_dir('./assets/arquivos/xml/temporario')) mkdir('./assets/arquivos/xml/temporario', 0777, TRUE);
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('files')) {
             return FALSE;
        } else {
            return $this->upload->data();
        }
    }
}

