<?php

class condicaopagamento extends CI_Controller {

    function __construct() {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('condicao_pagamento_model', '', TRUE);
        $this->data['menuCondicaoPagamento'] = 'Condições de pagamento';
        $this->data['menuApoio'] = 'Apoio';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/condicaopagamento/gerenciar/';
        $config['total_rows'] = $this->condicao_pagamento_model->count('condicao_pagamento');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->condicao_pagamento_model->get('condicao_pagamento','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'condicaopagamento/condicaopagamento';
        $this->load->view('theme/topo',$this->data);
    }

    function adicionar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('condicaopagamento') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'parcelas' => $this->input->post('parcelas'),
                'nome' => $this->input->post('nome'),
            );

            if ($this->condicao_pagamento_model->add('condicao_pagamento', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Condição de pagamento adicionado com sucesso!');
                redirect(base_url() . 'index.php/condicaopagamento/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['view'] = 'condicaopagamento/adicionarCondicaoPagamento';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('condicaopagamento') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'parcelas' => $this->input->post('parcelas'),
                'nome' => $this->input->post('nome'),
            );

            if ($this->condicao_pagamento_model->edit('condicao_pagamento', $data, 'idCondicaoPagamento', $this->input->post('idCondicaoPagamento')) == TRUE) {
                $this->session->set_flashdata('success', 'Condicação de pagamento editado com sucesso!');
                redirect(base_url() . 'index.php/condicaopagamento/editar/'.$this->input->post('idCondicaoPagamento'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->condicao_pagamento_model->getById($this->uri->segment(3));
        $this->data['view'] = 'condicaopagamento/editarCondicaoPagamento';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){
        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir condição de pagamento.');
            redirect(base_url().'index.php/condicaopagamento/gerenciar/');
        }

        $this->condicao_pagamento_model->delete('condicao_pagamento','idCondicaoPagamento',$id);

        $this->session->set_flashdata('success','Condicação de pagamento excluido com sucesso!');
        redirect(base_url().'index.php/condicaopagamento/gerenciar/');
    }
}

