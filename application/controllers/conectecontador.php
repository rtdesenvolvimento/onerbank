<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(E_ALL ^ E_DEPRECATED);

class conectecontador extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Conecte_model');
        $this->load->model('Relatorios_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->load->model('centrocusto_model', '', TRUE);
        $this->load->model('coletorxml_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('inventario_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->load->model('usuarios_model','', TRUE);
        $this->load->model('filial_model','',TRUE);
    }

    public function index()
    {
        $cnpjempresa = $this->session->userdata('cnpjempresa');
        redirect('conectecontador/contador/'.$cnpjempresa);
    }
    public function contador() {
        $this->load->view('conectecontador/login');
    }

    public function sair()
    {
        $cnpjempresa = $this->session->userdata('cnpjempresa');
        $this->session->sess_destroy();
        redirect('conectecontador/contador/'.$cnpjempresa);
    }

    public function painel($mes)
    {
        if ((!$this->session->userdata('session_id')) ||
            (!$this->session->userdata('conectado') ||
                ($this->session->userdata('token') != 20540658))) {
            redirect('conectecontador');
        }


        if ($mes == '') $mes = '01';

        $data['menuPainel']         = 'painel';

        //NFCE
        $data['nfceAutorizadas']    = $this->Conecte_model->getNFCEAutorizadas($mes);
        $data['nfceCanceladas']     = $this->Conecte_model->getNFCECanceladas($mes);
        $data['nfceInutilizadas']   = $this->Conecte_model->getNFCEInutilizadas($mes);
        $data['nfcePendentes']      = $this->Conecte_model->getNFCEPendentesVazio($mes);
        $data['nfcePendentesNull']  = $this->Conecte_model->getNFCEPendentesNull($mes);
        $data['graficoNFCE']        = $this->Conecte_model->getNFCEGrafico();

        //NFE
        $data['nfeAutorizadas']    = $this->Conecte_model->getNFEAutorizadas($mes);
        $data['nfeCanceladas']     = $this->Conecte_model->getNFECanceladas($mes);
        $data['nfeInutilizadas']   = $this->Conecte_model->getNFEInutilizadas($mes);
        $data['nfePendentes']      = $this->Conecte_model->getNFEPendentesVazio($mes);
        $data['nfePendentesNull']  = $this->Conecte_model->getNFEPendentesNull($mes);
        $data['graficoNFE']        = $this->Conecte_model->getNFEGrafico();

        $data['strMes']             = $this->getMes($mes);
        $data['mes']                = $mes;
        $data['output']             = 'conectecontador/painel';
        $this->load->view('conectecontador/template', $data);

    }

    function getMes($mes) {
        if ($mes == '01') {
            return 'Janeiro';
        } else if ($mes == '02'){
            return 'Fevereiro';
        } else if ($mes == '03'){
            return 'Março';
        } else if ($mes == '04'){
            return 'Abril';
        } else if ($mes == '05'){
            return 'Maio';
        } else if ($mes == '06'){
            return 'Junho';
        } else if ($mes == '07'){
            return 'Julho';
        } else if ($mes == '08'){
            return 'Agosto';
        } else if ($mes == '09'){
            return 'Setembro';
        } else if ($mes == '10'){
            return 'Outubro';
        } else if ($mes == '11'){
            return 'Novembro';
        } else if ($mes == '12'){
            return 'Dezembro';
        }
    }

    public function login()
    {
        $ajax = $this->input->get('ajax');

        $cnpjempresa    = $this->input->post('cnpjempresa');
        $otherdb        = $this->load->database($cnpjempresa, TRUE);

        $login = $this->input->post('login');
        $senha = $this->input->post('senha');

        $otherdb->where('login', $login);
        $otherdb->where('senha', $senha);
        $otherdb->limit(1);
        $contador = $otherdb->get('contador')->row();

        if (count($contador) > 0) {
            if (!isset($_SESSION)):
                session_start();
            endif;
            $_SESSION['BD'] = $cnpjempresa;

            $dados = array(
                'nome'          => $contador->nome,
                'id'            => $contador->idContador,
                'cnpjempresa'   => $cnpjempresa,
                'filial_id'     => 1,
                'token'         => 20540658,
                'conectado'     => TRUE
            );
            $this->session->set_userdata($dados);

            if ($ajax == true) {
                $json = array('result' => true);
                echo json_encode($json);
            } else {
                redirect(base_url() . 'conecte');
            }

        } else {
            if ($ajax == true) {
                $json = array('result' => false);
                echo json_encode($json);
            } else {
                $this->session->set_flashdata('error', 'Os dados de acesso estão incorretos.');
                redirect(base_url() . 'conectecontador');
            }
        }
    }

    public function pgdas()
    {

        if ((!$this->session->userdata('session_id')) ||
            (!$this->session->userdata('conectado') ||
                ($this->session->userdata('token') != 20540658))) {
            redirect('conectecontador');
        }

        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['centroCustos']         = $this->centrocusto_model->getAll();
        $this->data['output']               = 'conectecontador/rel_pgdas';
        $this->load->view('conectecontador/template', $this->data);

    }

    public function inventarioestoque() {

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/conectecontador/inventarioestoque/';
        $config['total_rows'] = $this->inventario_model->count('inventario');
        $config['per_page'] = 100;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->inventario_model->getContador('inventario', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['output'] = 'conectecontador/inventario';
        $this->load->view('conectecontador/template', $this->data);
    }

    function editarInventarioEstoque() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('inventario') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'numeroLivro' => $this->input->post('numeroLivro')
            );

            if ($this->inventario_model->edit('inventario', $data, 'idInventario', $this->input->post('idInventario')) == TRUE) {
                $this->session->set_flashdata('success', 'Invenatário editado com sucesso!');
                redirect(base_url() . 'index.php/conectecontador/editarInventarioEstoque/' . $this->input->post('idInventario'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['origem']               = $this->filial_model->getFilialCurrent();
        $this->data['usuarios']             = $this->usuarios_model->getAll();
        $this->data['produtosList']         = $this->produtos_model->getAll();
        $this->data['result']               = $this->inventario_model->getById($this->uri->segment(3));
        $this->data['produtos']             = $this->inventario_model->getProdutos($this->uri->segment(3));

        $this->data['output'] = 'conectecontador/editarInventario';
        $this->load->view('conectecontador/template', $this->data);

    }

    public function relatorioEstoqueHtmlHeader($data) {
        $strHora = $this->site_model->dataDeHojePorExtensoRetorno( strtotime($data) );
        $html = '
                    <br/><br/><br/>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%;"></td>
                            <td style="width: 50%;text-align: right;">
                                Estoque existente em: '.$strHora.'<br/>
                                * * * * * Registro de Inventário * * * * *
                            </td>
                        </tr>
                    </table>
                    
                    <table style="width: 100%;font-size: 12px;">
                        <thead>
                        <tr>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Código</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">Descrição do Artigo</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Quantidade</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">Unidade</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Unitário</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Total</th>
                        </tr>
                        </thead>
                        <tbody>';

        return $html;
    }


    public function relatorioEstoqueHtmlFooter() {
        $html = '</table>';

        return $html;
    }

    public function relatorioEstoqueTableHtmlFooter($total) {
        $html = '<tfoot>
                <tr>
                    <td colspan="6" style="border-top: 1px solid #2c2f3b;text-align: right;">Valor. . . . . . . . . . . . : '.$this->site_model->formatarValorMonetario($total).'</td>
                </tr>
                </tfoot> ';

        return $html;
    }

    public function relatorioEstoqueSubTotalTableHtmlFooter($total) {
        $html = '<tfoot>
                <tr>
                    <td colspan="6" style="text-align: right;">Total. . . . . . . . . . . . : '.$this->site_model->formatarValorMonetario($total).'</td>
                </tr>
                </tfoot> ';

        return $html;
    }

    public function relatorioEstoque() {

        $this->load->helper('mpdf');
        $filial = $this->db->get_where('filial', array('idFilial'=>$this->session->userdata('filial_id')))->row();
        $emitente = $this->db->get_where('emitente', array('filial_id'=>$this->session->userdata('filial_id')))->row();

        $stream = TRUE;
        $orientation='A4';
        $filename = 'relatorio_inventario';
        $client = $filial->nome.' CNPJ: '.$emitente->cnpj.' I.E.: '.$emitente->ie;

        define('_MPDF_PATH','application/lib/mpdf/');
        require('application/lib/mpdf/mpdf.php');

        $pdf_c = 'c';

        $mpdf=new mPDF($pdf_c,$orientation,'','',5,5,5,5,9,9);
        $mpdf->debug = false;
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;

        $mpdf->SetProtection(array('copy','print'), '', '123456');
        $mpdf->SetTitle($client);
        $mpdf->SetAuthor($client);
        $mpdf->SetCreator($client);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetAutoPageBreak(TRUE);

        $produtos = $this->inventario_model->getProdutos($this->uri->segment(3));
        $result = $this->inventario_model->getById($this->uri->segment(3));

        $mpdf->SetHeader($client.'|| Livro No. '.$result->numeroLivro.' Folha: {PAGENO}', '', TRUE); // For simple text header

        $subTotal = 0;
        $total = 0;
        $contadorItens = 1;
        $contadorTotal = 0;
        $exibirHeader = true;
        $QUANTIDADE_ITENS_EXIBIR = 47;

        foreach ($produtos as $produto) {

            $estoque = $this->produtos_model->getProdutoFilialById($produto->produtos_id);

            $precoDeCompra = $estoque->precoCompra;
            $estoqueContado = $produto->quantidade;

            $valorEmEstoque = $precoDeCompra * $estoqueContado;

            $total = $total + $valorEmEstoque;
            $subTotal = $subTotal + $valorEmEstoque;

            if ($exibirHeader) {
                $mpdf->WriteHTML($this->relatorioEstoqueHtmlHeader($result->dataInventario));
                $exibirHeader = false;
            }

            $htmlProdutos = ' <tr>
                        <td style="text-align: center;">'.$produto->cProd.'</td>
                        <td style="text-align: left;">'.$produto->descricao.'</td>
                        <td style="text-align: right;">'.$this->site_model->formatarValorMonetario($estoqueContado, 3).'</td>
                        <td style="text-align: left;">'.$produto->unidade.'</td>
                        <td style="text-align: right;">'.$this->site_model->formatarValorMonetario($precoDeCompra, 6).'</td>
                        <td style="text-align: right;">'.$this->site_model->formatarValorMonetario($precoDeCompra* $estoqueContado).'</td>
                    </tr>';

            $mpdf->WriteHTML($htmlProdutos);

            if ($contadorItens == $QUANTIDADE_ITENS_EXIBIR || $contadorTotal == count($produtos)-1) {

                $mpdf->WriteHTML($this->relatorioEstoqueTableHtmlFooter($total));

                if ($contadorTotal == count($produtos)-1) {
                    $mpdf->WriteHTML($this->relatorioEstoqueSubTotalTableHtmlFooter($subTotal));
                }

                $mpdf->WriteHTML($this->relatorioEstoqueHtmlFooter());

                if ($contadorTotal < count($produtos)-1) {
                    $mpdf->SetFooter('{PAGENO}/{nbpg}', '', TRUE); // For simple text header
                    $mpdf->AddPage();
                }

                $contadorItens = 1;
                $total = 0;
                $exibirHeader = true;
            }

            $contadorItens++;
            $contadorTotal++;
        }

        $mpdf->SetFooter('{PAGENO}/{nbpg}', '', TRUE); // For simple text header

        if ($stream) {
            $mpdf->Output($filename . '.pdf', 'D');
        } else  {
            $mpdf->Output('assets/uploads/' . $filename . '.pdf', 'F');
            return 'assets/uploads/' . $filename . '.pdf';
        }

    }


    function nfentradas(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/conectecontador/nfentradas/';
        $config['total_rows'] = $this->coletorxml_model->count('manifesta');
        $config['per_page'] = 200;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $mes = $this->input->get('mes');

        if ($mes == '') $mes = date('m');

        $where = 'SUBSTRING(data,4,7) = "'.$mes.'/'.date('Y').'"';

        $this->data['mes']              = $mes;
        $this->data['results']          = $this->coletorxml_model->get('manifesta','*',$where,$config['per_page'],$this->uri->segment(3));
        $this->data['manifestadaAll']   = $this->coletorxml_model->getAllByGraficsManifestada();
        $this->data['naoManifestadaAll']= $this->coletorxml_model->getAllByGraficsNaoManifestada();

        $this->data['output']       = 'conectecontador/coletorxml';
        $this->load->view('conectecontador/template', $this->data);
    }

    public function pgdasCustomPreview()
    {
        if ((!$this->session->userdata('session_id')) ||
            (!$this->session->userdata('conectado') ||
                ($this->session->userdata('token') != 20540658))) {
            redirect('conectecontador');
        }

        $dataInicio = $this->input->get('dataInicio');
        $dataFinal = $this->input->get('dataFinal');

        $data['nfe'] = $this->Relatorios_model->pgdasNFeCustom($dataInicio, $dataFinal);
        $data['nfce'] = $this->Relatorios_model->pgdasNFCeCustom($dataInicio, $dataFinal);

        $this->load->helper('mpdf');
        $html = $this->load->view('conectecontador/imprimirPgdas', $data, true);
        echo pdf_create($html, 'relatorio_produtos' . date('d-m-y'), FALSE, 'A4-L');
    }

    public function pgdasCustom()
    {

        if ((!$this->session->userdata('session_id')) ||
            (!$this->session->userdata('conectado') ||
                ($this->session->userdata('token') != 20540658))) {
            redirect('conectecontador');
        }

        $dataInicio = $this->input->get('dataInicio');
        $dataFinal = $this->input->get('dataFinal');

        $data['nfe'] = $this->Relatorios_model->pgdasNFeCustom($dataInicio, $dataFinal);
        $data['nfce'] = $this->Relatorios_model->pgdasNFCeCustom($dataInicio, $dataFinal);

        $this->load->helper('mpdf');
        $html = $this->load->view('conectecontador/imprimirPgdas', $data, true);
        pdf_create($html, 'relatorio_pgdas' . date('d/m/y'), TRUE);
    }


    function uploadXMLNF() {

        $cpt = count($_FILES['files']['name']);
        $files = $_FILES;

        for($i=0; $i<$cpt; $i++) {

            $_FILES['files']['name']    = $files['files']['name'][$i];
            $_FILES['files']['type']    = $files['files']['type'][$i];
            $_FILES['files']['tmp_name']= $files['files']['tmp_name'][$i];
            $_FILES['files']['error']   = $files['files']['error'][$i];
            $_FILES['files']['size']    = $files['files']['size'][$i];

            $xmlNF = $this->do_upload();

            if ($xmlNF['file_name'] != '') {
                $this->lendoXML($xmlNF['file_name']);
            }
        }
        $this->gerenciar();
    }

    function lendoXML($nomeArquivo) {
        // Transformando arquivo XML em Objeto
        $origem = './assets/arquivos/xml/temporario/'.$nomeArquivo;
        $emitente = $this->mapos_model->getEmitente();

        $importarNotasSaida = $this->input->post('importarNotasSaida');

        $xml    = simplexml_load_file($origem);
        $CNPJ   = $emitente->cnpj;
        $xNome  = '';
        $chave  = '';
        $vNF    = 0;
        $mod    = '55';

        foreach($xml->NFe  as $NFe):
            foreach($NFe->infNFe  as $infNFe):

                foreach($infNFe->ide  as $ide):

                    $dhEmiT = $ide->dhEmi;
                    $chave  = str_replace ('NFe','',$infNFe['Id'][0]);
                    $dhEmi  = date('d/m/Y', strtotime($dhEmiT));
                    $mod    = $ide->mod;

                endforeach;

                foreach($infNFe->emit  as $emit):
                    $xNome = $emit->xNome;
                endforeach;

                if ($mod == '55') {
                    if ($importarNotasSaida == 1) {
                        foreach ($infNFe->dest as $dest):
                            $xNome = $dest->xNome;
                        endforeach;
                    }
                }

                foreach($infNFe->total  as $total):
                    foreach($total->ICMSTot  as $ICMSTot):
                        $vNF = $ICMSTot->vNF;
                    endforeach;
                endforeach;
            endforeach;
        endforeach;

        if ($CNPJ != '') {

            $verifica = $this->coletorxml_model->getByChave($chave);

            if (count($verifica) == 0) {
                $data = array(
                    'chave' => $chave,
                    'data' => $dhEmi,
                    'xNome' => $xNome . '',
                    'valor' => $vNF,
                    'manifestada' => 'S',
                    'donwload' => 'S',
                    'importada' => 'N',
                );

                $this->coletorxml_model->add('manifesta', $data, TRUE);

                $destino = './emissores/v4/nfephp-master/XML/' . $CNPJ . '/NF-e/producao/recebidas/' . $chave . '-nfe.xml';
                if (copy($origem, $destino)) ;
            }
        }
    }

    public function do_upload(){

        $config['upload_path']      = './assets/arquivos/xml/temporario/';
        $config['allowed_types']    = 0;
        $config['max_size']         = 0;
        $config['max_width']        = '3000';
        $config['max_height']       = '2000';
        $config['encrypt_name']     = false;

        if (!is_dir('./assets/arquivos/xml/temporario')) mkdir('./assets/arquivos/xml/temporario', 0777, TRUE);
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('files')) {
            return FALSE;
        } else {
            return $this->upload->data();
        }
    }

}
