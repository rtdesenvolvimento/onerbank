<?php

class configuracao extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('configuracao_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('naturezaoperacao_model', '', TRUE);

        $this->data['menuConfiguracao'] = 'Configuração';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vConfiguracao')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar configuracao.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/configuracao/gerenciar/';
        $config['total_rows'] = $this->configuracao_model->count('configuracao');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->configuracao_model->get('configuracao','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'configuracao/configuracao';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aConfiguracao')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar configuração.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('configuracao') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'tBand' => $this->input->post('tBand'),
                'CNPJCredenciadoraCartao' => $this->input->post('CNPJCredenciadoraCartao'),
                'numeroDeSerieNFE' => $this->input->post('numeroDeSerieNFE'),
                'numeroDeSerieNFCE' => $this->input->post('numeroDeSerieNFCE'),
                'permiteEstoqueNegativo' => $this->input->post('permiteEstoqueNegativo'),
                'usarNFCE' => $this->input->post('usarNFCE'),
                'serieRps' => $this->input->post('serieRps'),
                'numeroProximoNumeroRps' => $this->input->post('numeroProximoNumeroRps'),
                'numeroProximoNumeroLote' => $this->input->post('numeroProximoNumeroLote'),
                'naturezaOperacaoServicoPadrao' => $this->input->post('naturezaOperacaoServicoPadrao'),
                'pICMSST' => $this->input->post('pICMSST'),
                'pIPI' => $this->input->post('pIPI'),
                'pPIS' => $this->input->post('pPIS'),
                'pCOFINS' => $this->input->post('pCOFINS'),
                'isCalcularValorISS' => $this->input->post('isCalcularValorISS'),
                'qtdDiasVencimentoEnviarCobranca' => $this->input->post('qtdDiasVencimentoEnviarCobranca'),
                'prazoMaximoPagamentoAposVencimento' => $this->input->post('prazoMaximoPagamentoAposVencimento'),
                'multaAtrasoBoleto' => $this->input->post('multaAtrasoBoleto'),
                'jurosAtrasoBoleto' => $this->input->post('jurosAtrasoBoleto'),
                'isReterISS' => $this->input->post('isReterISS'),
                'isReterINSS' => $this->input->post('isReterINSS'),
                'aliquotaIR' => $this->input->post('aliquotaIR'),
                'aliquotaINSS' => $this->input->post('aliquotaINSS'),
                'aliquotaCofins' => $this->input->post('aliquotaCofins'),
                'aliquotaPIS' => $this->input->post('aliquotaPIS'),
                'aliquotaContribuicaoSocial' => $this->input->post('aliquotaContribuicaoSocial'),
                'descontarIRValor' => $this->input->post('descontarIRValor'),
                'valorMinimoCalculoIR' => $this->input->post('valorMinimoCalculoIR'),
                'reterCSLLPISCONFINSNotaAcima5000' => $this->input->post('reterCSLLPISCONFINSNotaAcima5000'),
                'reterCSLLPISCONFINSSomaImpostosAcima' => $this->input->post('reterCSLLPISCONFINSSomaImpostosAcima'),
                'naturezaoperacaodentroestado_id' => $this->input->post('naturezaoperacaodentroestado_id'),
                'naturezaoperacaoforaestado_id' => $this->input->post('naturezaoperacaoforaestado_id'),
            );

            if ($this->configuracao_model->add('configuracao', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Configuração adicionada com sucesso!');
                redirect(base_url() . 'index.php/configuracao/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['naturezasoperacao']    = $this->naturezaoperacao_model->getAll();
        $this->data['view']                 = 'configuracao/adicionarConfiguracao';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eConfiguracao')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar da configuração.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('configuracao') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'tBand' => $this->input->post('tBand'),
                'CNPJCredenciadoraCartao' => $this->input->post('CNPJCredenciadoraCartao'),
                'numeroDeSerieNFE' => $this->input->post('numeroDeSerieNFE'),
                'numeroDeSerieNFCE' => $this->input->post('numeroDeSerieNFCE'),
                'serieRps' => $this->input->post('serieRps'),
                'usarNFCE' => $this->input->post('usarNFCE'),
                'permiteEstoqueNegativo' => $this->input->post('permiteEstoqueNegativo'),
                'pICMSST' => $this->input->post('pICMSST'),
                'pIPI' => $this->input->post('pIPI'),
                'pPIS' => $this->input->post('pPIS'),
                'pCOFINS' => $this->input->post('pCOFINS'),
                'cliente_padrao_id' => $this->input->post('cliente_padrao_id'),
                'numeroProximoNumeroRps' => $this->input->post('numeroProximoNumeroRps'),
                'numeroProximoNumeroLote' => $this->input->post('numeroProximoNumeroLote'),
                'numeroProximoNNFE' => $this->input->post('numeroProximoNNFE'),
                'numeroProximoNFCE' => $this->input->post('numeroProximoNFCE'),
                'naturezaOperacaoServicoPadrao' => $this->input->post('naturezaOperacaoServicoPadrao'),
                'isCalcularValorISS' => $this->input->post('isCalcularValorISS'),
                'qtdDiasVencimentoEnviarCobranca' => $this->input->post('qtdDiasVencimentoEnviarCobranca'),
                'prazoMaximoPagamentoAposVencimento' => $this->input->post('prazoMaximoPagamentoAposVencimento'),
                'multaAtrasoBoleto' => $this->input->post('multaAtrasoBoleto'),
                'jurosAtrasoBoleto' => $this->input->post('jurosAtrasoBoleto'),
                'isReterISS' => $this->input->post('isReterISS'),
                'isReterINSS' => $this->input->post('isReterINSS'),
                'aliquotaIR' => $this->input->post('aliquotaIR'),
                'aliquotaINSS' => $this->input->post('aliquotaINSS'),
                'aliquotaCofins' => $this->input->post('aliquotaCofins'),
                'aliquotaPIS' => $this->input->post('aliquotaPIS'),
                'aliquotaContribuicaoSocial' => $this->input->post('aliquotaContribuicaoSocial'),
                'descontarIRValor' => $this->input->post('descontarIRValor'),
                'valorMinimoCalculoIR' => $this->input->post('valorMinimoCalculoIR'),
                'reterCSLLPISCONFINSNotaAcima5000' => $this->input->post('reterCSLLPISCONFINSNotaAcima5000'),
                'reterCSLLPISCONFINSSomaImpostosAcima' => $this->input->post('reterCSLLPISCONFINSSomaImpostosAcima'),
                'naturezaoperacaodentroestado_id' => $this->input->post('naturezaoperacaodentroestado_id'),
                'naturezaoperacaoforaestado_id' => $this->input->post('naturezaoperacaoforaestado_id'),
            );

            if ($this->configuracao_model->edit('configuracao', $data, 'idConfiguracao', $this->input->post('idConfiguracao')) == TRUE) {
                $this->session->set_flashdata('success', 'Configuração editada com sucesso!');
                redirect(base_url() . 'index.php/configuracao/editar/'.$this->input->post('idConfiguracao'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['naturezasoperacao']    = $this->naturezaoperacao_model->getAll();
        $this->data['clientes'] = $this->clientes_model->getAll();
        $this->data['result'] = $this->configuracao_model->getById($this->uri->segment(3));
        $this->data['view'] = 'configuracao/editarConfiguracao';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dSetor')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir setores.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir setor.');
            redirect(base_url().'index.php/configuracao/gerenciar/');
        }

        $this->configuracao_model->delete('configuracao','idConfiguracao',$id);

        $this->session->set_flashdata('success','Configuração excluida com sucesso!');
        redirect(base_url().'index.php/setor/gerenciar/');
    }
}

