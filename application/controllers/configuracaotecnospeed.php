<?php

class configuracaotecnospeed extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('configuracaotecnospeed_model', '', TRUE);
        $this->data['menuConfiguracaoTecnoSpeed'] = 'Configuração TecnoSpeed';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/configuracaotecnospeed/gerenciar/';
        $config['total_rows'] = $this->configuracaotecnospeed_model->count('configuracao_tecnospeed');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->data['results'] = $this->configuracaotecnospeed_model->get('configuracao_tecnospeed','*','',$config['per_page'],$this->uri->segment(3));
        $this->data['output'] = 'configuracaotecnospeed/configuracaotecnospeed';
        $this->load->view('master/template',$this->data);
    }

    function adicionar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('configuracao_tecnospeed') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'cod_grupo' => $this->input->post('cod_grupo'),
                'descricao' => $this->input->post('descricao'),
                'grupo' => $this->input->post('grupo'),
                'usuario' => $this->input->post('usuario'),
                'senha' => $this->input->post('senha')
            );
            if ($this->configuracaotecnospeed_model->add('configuracao_tecnospeed', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Configuração TecnoSpeed adicionado com sucesso!');
                redirect(base_url() . 'index.php/configuracaotecnospeed/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['output'] = 'configuracaotecnospeed/adicionarConfiguracaoTecnospeed';
        $this->load->view('master/template', $this->data);
    }

    function editar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('configuracao_tecnospeed') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'cod_grupo' => $this->input->post('cod_grupo'),
                'descricao' => $this->input->post('descricao'),
                'grupo' => $this->input->post('grupo'),
                'usuario' => $this->input->post('usuario'),
                'senha' => $this->input->post('senha')
            );

            if ($this->configuracaotecnospeed_model->edit('configuracao_tecnospeed', $data, 'idConfiguracaotecnospeed', $this->input->post('idConfiguracaotecnospeed')) == TRUE) {
                $this->session->set_flashdata('success', 'Configuração TecnoSpeed editado com sucesso!');
                redirect(base_url() . 'index.php/configuracaotecnospeed/editar/'.$this->input->post('idConfiguracaotecnospeed'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->configuracaotecnospeed_model->getById($this->uri->segment(3));
        $this->data['output'] = 'configuracaotecnospeed/editarConfiguracaoTecnospeed';
        $this->load->view('master/template', $this->data);
    }

    function excluir(){

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir setor.');
            redirect(base_url().'index.php/configuracaotecnospeed/gerenciar/');
        }

        $this->configuracaotecnospeed_model->delete('configuracao_tecnospeed','idConfiguracaotecnospeed',$id);
        $this->session->set_flashdata('success','Configuração excluida com sucesso!');
        redirect(base_url().'index.php/configuracaotecnospeed/gerenciar/');
    }
}

