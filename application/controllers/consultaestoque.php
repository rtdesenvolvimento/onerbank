<?php

class consultaestoque extends CI_Controller {

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('setor_model', '', TRUE);
        $this->load->model('filial_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->data['menuConsultaEstoque'] = 'Consulta de Estoque';
        $this->data['menuEstoque'] = 'Estoque';
    }

    function  consultaEstoque() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $termo = $this->input->post('termo');

        $tipoConsultaEstoque = $this->input->post('tipoConsultaEstoque');

        $this->data['termo']    = $termo;
        $this->data['tipoConsultaEstoque'] = $tipoConsultaEstoque;
        $this->data['results']  = $this->produtos_model->getProdutosFiliais('', $termo, $tipoConsultaEstoque);
        $this->data['view']     = 'produtos/consultaEstoque';
        $this->load->view('theme/topo',$this->data);
    }

    function consultaEstoqueIFrame() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $termo = $this->input->post('termo');

        $tipoConsultaEstoque = $this->input->post('tipoConsultaEstoque');

        $this->data['termo']    = $termo;
        $this->data['tipoConsultaEstoque'] = $tipoConsultaEstoque;
        $this->data['results']  = $this->produtos_model->getProdutosFiliais('', $termo, $tipoConsultaEstoque);
        $this->data['view']     = 'produtos/consultaEstoqueIFrame';
        $this->load->view('tema/blank_script',$this->data);
    }

    function  historicoEstoque($produto, $filial) {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $this->data['results'] = $this->produtos_model->getHistoricoEstoque($produto, $filial);
        $this->data['view'] = 'produtos/historicoEstoque';
        $this->load->view('theme/topo',$this->data);
    }
}

