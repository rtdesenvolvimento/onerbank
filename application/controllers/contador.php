<?php

class contador extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('contador_model', '', TRUE);
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){


        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/contador/gerenciar/';
        $config['total_rows'] = $this->contador_model->count('contador');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->contador_model->get('contador','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['output'] = 'contador/contador';
        $this->load->view('master/template',$this->data);
    }

    function adicionar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('contador') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'nome' => $this->input->post('nome'),
                'cnpjCpf' => $this->input->post('cnpjCpf'),
                'login' => $this->input->post('login'),
                'senha' => $this->input->post('senha')
            );

            if ($this->contador_model->add('contador', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Contador adicionado com sucesso!');
                redirect(base_url() . 'index.php/contador/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['output'] = 'contador/adicionarContador';
        $this->load->view('master/template', $this->data);
    }

    function editar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('contador') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'cnpjCpf' => $this->input->post('cnpjCpf'),
                'login' => $this->input->post('login'),
                'senha' => $this->input->post('senha')
            );

            if ($this->contador_model->edit('contador', $data, 'idContador', $this->input->post('idContador')) == TRUE) {
                $this->session->set_flashdata('success', 'Contador editado com sucesso!');
                redirect(base_url() . 'index.php/contador/editar/'.$this->input->post('idContador'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->contador_model->getById($this->uri->segment(3));
        $this->data['output'] = 'contador/editarContador';
        $this->load->view('master/template', $this->data);
    }

    function excluir()
    {

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir contador.');
            redirect(base_url().'index.php/contador/gerenciar/');
        }

        $this->contador_model->delete('contador','idContador',$id);

        $this->session->set_flashdata('success','Contador excluido com sucesso!');
        redirect(base_url().'index.php/contador/gerenciar/');
    }

}

