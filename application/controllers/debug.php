<?php

class debug extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'codegen_helper'));

        $this->load->model('nfce_model', '', TRUE);

    }

    function index(){

    }

    function corrigir() {

        $this->corrigirNFCE();

        $notas = $this->getAllNFCEFilial1();

        foreach ($notas as $nota) {
            $venda = $this->getVendaBynNF($nota->nNF);

            if (count($venda) > 0) {

                $lancamento = $this->getLancamentoByVenda($venda->idVendas);

                $this->edit('vendas', array('filial_id' => '1'), 'idVendas', $venda->idVendas);
                echo '<br/>';
                echo  'Venda ='.$venda->idVendas;

                if (count($lancamento) > 0) {
                    $this->edit('lancamentos', array('filial_id' => '1'), 'idLancamentos', $lancamento->idLancamentos);
                    echo '<br/>';
                    echo  'Lancamento ='.$lancamento->idLancamentos;


                    $parcelas = $this->getParcelasByLancamentoId($lancamento->idLancamentos);
                    foreach ($parcelas as $parcela) {
                        $this->edit('parcela', array('filial_id' => '1'), 'idParcela', $parcela->idParcela);
                        echo '<br/>';
                        echo  'Parcela ='.$parcela->idParcela;


                        $faturas = $this->getFaturasParcelaByLancamento($parcela->idParcela);
                        foreach ($faturas as $fatura) {
                            $this->edit('fatura', array('filial_id' => '1'), 'idFatura', $fatura->fatura_id);
                            echo '<br/>';
                            echo  'Fatura ='.$fatura->fatura_id;

                            $pagamentos = $this->getPagamentoByFatura( $fatura->fatura_id);
                            foreach ($pagamentos as $pagamento) {
                                $this->edit('pagamento', array('filial_id' => '1'), 'idPagamento', $pagamento->idPagamento);
                                echo '<br/>';
                                echo  'Pagamento ='.$fatura->fatura_id;
                            }
                        }
                    }
                }
            }
        }
    }

    function getPagamentoByFatura($faturaId){
        $this->db->where('fatura_id',$faturaId);
        return $this->db->get('pagamento')->result();
    }

    function getFaturasParcelaByLancamento($idParcela){
        $this->db->where('parcela_id',$idParcela);
        return $this->db->get('fatura_parcela')->result();
    }

    function getParcelasByLancamentoId($lancamentos_id){
        $this->db->where('lancamentos_id',$lancamentos_id);
        return $this->db->get('parcela')->result();
    }

    function corrigirNFCE() {
        $notas = $this->getAllNFCEFilial2();
        foreach ($notas as $nota) {
            $this->edit('nfce', array('filial_id' => '1'),'nNF', $nota->nNF);
        }
    }

    function edit($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) return TRUE;
        return FALSE;
    }


    public function getAllNFCEFilial1()
    {
        $this->db->where('filial_id',1);
        $this->db->from('nfce');
        return $this->db->get()->result();
    }

    public function getAllNFCEFilial2()
    {
        $this->db->from('nfce');
        $this->db->where('filial_id',2);
        return $this->db->get()->result();
    }

    public function getVendaBynNF($nNF)
    {
        $this->db->where('nNF',$nNF);
        $this->db->from('vendas');
        $this->db->limit(1);

        return $this->db->get()->row();
    }

    public function getLancamentoByVenda($venda)
    {
        $this->db->where('venda',$venda);
        $this->db->from('lancamentos');
        $this->db->limit(1);

        return $this->db->get()->row();
    }

}

