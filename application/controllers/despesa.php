<?php

class despesa extends CI_Controller {

    function __construct() {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('despesa_model', '', TRUE);
        $this->data['menuDespesa'] = 'Plano de contas - Despesa';
        $this->data['menuApoio'] = 'Apoio';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/despesa/gerenciar/';
        $config['total_rows'] = $this->despesa_model->count('despesa');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->despesa_model->get('',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'despesa/despesa';
        $this->load->view('theme/topo',$this->data);
    }

    function adicionar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('despesa') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'codigo' => $this->input->post('codigo'),
                'nome' => $this->input->post('nome'),
                'despesaSuperior_id' => $this->input->post('despesaSuperior_id'),
            );

            if ($this->despesa_model->add('despesa', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Despesa adicionada com sucesso!');
                redirect(base_url() . 'index.php/despesa/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['despesas'] = $this->despesa_model->getAll();
        $this->data['view'] = 'despesa/adicionarDespesa';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('despesa') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'codigo' => $this->input->post('codigo'),
                'nome' => $this->input->post('nome'),
                'despesaSuperior_id' => $this->input->post('despesaSuperior_id'),
            );

            if ($this->despesa_model->edit('despesa', $data, 'idDespesa', $this->input->post('idDespesa')) == TRUE) {
                $this->session->set_flashdata('success', 'Despesa editada com sucesso!');
                redirect(base_url() . 'index.php/despesa/editar/'.$this->input->post('idDespesa'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['despesas'] = $this->despesa_model->getAll();
        $this->data['result'] = $this->despesa_model->getById($this->uri->segment(3));
        $this->data['view'] = 'despesa/editarDespesa';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){
        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir tipo de despesa.');
            redirect(base_url().'index.php/despesa/gerenciar/');
        }

        $this->despesa_model->delete('despesa','idDespesa',$id);

        $this->session->set_flashdata('success','Despesa excluida com sucesso!');
        redirect(base_url().'index.php/despesa/gerenciar/');
    }
}

