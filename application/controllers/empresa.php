<?php

class empresa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('codegen_helper'));

        $this->load->model('empresa_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('configuracaotecnospeed_model', '', TRUE);
        $this->load->model('tecnospeed_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('contador_model', '', TRUE);

        $this->data['menuEmpresa'] = 'empresa';
        $this->data['menuCadastro'] = 'Cadastro';
    }

    function index()
    {
        $this->gerenciar();
    }

    function buscar()
    {
        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/empresa/gerenciar/';
        $config['total_rows'] = $this->empresa_model->count('empresa');
        $config['per_page'] = 10000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->data['results']  = $this->empresa_model->get('empresa', '*', '', $config['per_page'], '');
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['output']   = 'empresa/empresa';
        $this->load->view('master/template', $this->data);
    }

    function gerenciar()
    {
        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/empresa/gerenciar/';
        $config['total_rows'] = $this->empresa_model->count('empresa');
        $config['per_page'] = 10000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->empresa_model->get('empresa', '*', '', $config['per_page'], '');
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['output']   = 'empresa/empresa';
        $this->load->view('master/template', $this->data);
    }

    function adicionar()
    {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('empresa') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $cnpj = $this->input->post('documento');

            $this->mapos_model->criacaoPastasArquivos($cnpj);
            $cert   = $this->mapos_model->gravarCertificado($cnpj, $_FILES);

            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeEmpresa'           => $this->input->post('nomeEmpresa'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $cnpj,
                'rg'                    => $this->input->post('rg'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'natureza_juridica'     => $this->input->post('natureza_juridica'),
                'incentivadorCultural'  => $this->input->post('incentivadorCultural'),
                'incentivoFiscal'       => $this->input->post('incentivoFiscal'),
                'tipoTributacao'        => $this->input->post('tipoTributacao'),
                'naturezaTributacao'    => $this->input->post('naturezaTributacao'),
                'IE'                    => $this->input->post('IE'),
                'IESUF'                 => $this->input->post('IESUF'),
                'IEST'                  => $this->input->post('IEST'),
                'insmun'                => $this->input->post('insmun'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'certsenha'             => $this->input->post('certsenha'),
                'cert'                  => $cert,
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeEmpresa'    => $this->input->post('contatoNomeEmpresa'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'hostname'              => $this->input->post('hostname'),
                'username'              => $this->input->post('username'),
                'password'              => $this->input->post('password'),
                'certificado'           => $this->input->post('certificado'),
                'ambiente'              => $this->input->post('ambiente'),
                'regimeTributario'      => $this->input->post('regimeTributario'),
                'cnae'                  => $this->input->post('cnae'),
                'csc'                   => $this->input->post('csc'),
                'idcsc'                 => $this->input->post('idcsc'),
                'tokenibpt'             => $this->input->post('tokenibpt'),
                'contador_id'           => $this->input->post('contador_id'),
                'dataCadastro'          => date('Y-m-d'),
                'status'                => 1,

                'marketplace_id'         => $this->input->post('marketplace_id'),
                'publishable_key'        => $this->input->post('publishable_key'),
                'on_behalf_of'           => $this->input->post('on_behalf_of'),

                'configuracao_tecnospeed_id'    => $this->input->post('configuracao_tecnospeed_id'),
                'regimeEspecialTributacao'      => $this->input->post('regimeEspecialTributacao'),
            );

            if ($this->empresa_model->add('empresa', $data) == TRUE) {

                //adicionar o arquivo do cliente para controle de login
                $file_path          = APPPATH.'config/clientes/'.$cnpj.'.txt';
                $file_path_database   = APPPATH.'config/database.php';

                if ( ! file_exists($file_path)) {
                      file_put_contents($file_path, 'sim', FILE_APPEND);
                }

                $prefixo = PREFIXO_BD;
                //$prefixo = '';
                //$cnpjpconexao = substr ( $cnpj , 0, 7 );
                $cnpjpconexao = $cnpj;

                $texto = '
                
                    $db[\''.$cnpj.'\'][\'hostname\'] = "'.$this->input->post('hostname').'";
                    $db[\''.$cnpj.'\'][\'username\'] = "'.$this->input->post('username').'";
                    $db[\''.$cnpj.'\'][\'password\'] = "'.$this->input->post('password').'";
                    $db[\''.$cnpj.'\'][\'database\'] = "'.$prefixo.$cnpjpconexao.'";
                    $db[\''.$cnpj.'\'][\'dbdriver\'] = "mysqli";
                    $db[\''.$cnpj.'\'][\'dbprefix\'] = "";
                    $db[\''.$cnpj.'\'][\'pconnect\'] = FALSE;
                    $db[\''.$cnpj.'\'][\'db_debug\'] = TRUE;
                    $db[\''.$cnpj.'\'][\'cache_on\'] = FALSE;
                    $db[\''.$cnpj.'\'][\'cachedir\'] = "";
                    $db[\''.$cnpj.'\'][\'char_set\'] = "utf8";
                    $db[\''.$cnpj.'\'][\'dbcollat\'] = "utf8_general_ci";
                    $db[\''.$cnpj.'\'][\'swap_pre\'] = "";
                    $db[\''.$cnpj.'\'][\'autoinit\'] = TRUE;
                    $db[\''.$cnpj.'\'][\'stricton\'] = FALSE;
                ';

                file_put_contents($file_path_database, $texto, FILE_APPEND);

                $this->session->set_flashdata('success', 'Oficina adicionado com sucesso!');
                redirect(base_url() . 'index.php/empresa');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['contadores']   = $this->contador_model->getAll();
        $this->data['tecnospeeds']  = $this->configuracaotecnospeed_model->getAll();
        $this->data['output']       = 'empresa/adicionarEmpresa';
        $this->load->view('master/template', $this->data);
    }

    function criarBanco($idEmpresa) {

        $prefixo = PREFIXO_BD;
        //$prefixo = '';
        $empresa    = $this->empresa_model->getById($idEmpresa);

        $hostname   = $empresa->hostname;
        $username   = $empresa->username;
        $password   = $empresa->password;
        $documento  = $empresa->documento;

        $databasecreate = $prefixo.$documento;
        $conn = new PDO("mysql:host=$hostname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $conn->exec("CREATE DATABASE `$databasecreate`;
                CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';
                GRANT ALL ON `$documento`.* TO '$username'@'localhost';
                FLUSH PRIVILEGES;")
        or die(print_r($conn->errorInfo(), true));

        $json = array('result' => $conn->errorInfo());
        echo json_encode($json);
    }

    function criarTabelas($idEmpresa) {

        require "leitor_sql.php";

        $prefixo = PREFIXO_BD;
        //$prefixo = '';
        $empresa = $this->empresa_model->getById($idEmpresa);

        $hostname   = $empresa->hostname;
        $username   = $empresa->username;
        $password   = $empresa->password;
        $documento  = $empresa->documento;

        $conn = new PDO('mysql:host=' .$hostname. ';dbname=' . $prefixo.$documento, $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $bancoDados   =  APPPATH.'/bancodados/banco.sql';

        new leitor_sql($bancoDados, $conn);

        //criar a empresa
        $data_empresa = array(
            'tipoPessoa'            => $empresa->tipoPessoa,
            'nomeEmpresa'           => $empresa->nomeEmpresa,
            'sexo'                  => $empresa->sexo,
            'nomeFantasiaApelido'   => $empresa->nomeFantasiaApelido,
            'documento'             => $documento,
            'rg'                    => $empresa->rg,
            'codIBGECidade'         => $empresa->codIBGECidade,
            'codIBGEEstado'         => $empresa->codIBGEEstado,
            'natureza_juridica'     => $empresa->natureza_juridica,
            'IE'                    => $empresa->IE,
            'IESUF'                 => $empresa->IESUF,
            'IEST'                  => $empresa->IEST,
            'incentivadorCultural'  => $empresa->incentivadorCultural,
            'incentivoFiscal'       => $empresa->incentivoFiscal,
            'tipoTributacao'        => $empresa->tipoTributacao,
            'naturezaTributacao'    => $empresa->naturezaTributacao,
            'insmun'                => $empresa->insmun,
            'orgaoEmissor'          => $empresa->orgaoEmissor,
            'estadoOrgaoEmissor'    => $empresa->estadoOrgaoEmissor,
            'dataOrgaoEmissor'      => $empresa->dataOrgaoEmissor,
            'data_nascimento'       => $empresa->data_nascimento,
            'telefone'              => $empresa->telefone,
            'celular'               => $empresa->celular,
            'email'                 => $empresa->email,
            'rua'                   => $empresa->rua,
            'numero'                => $empresa->numero,
            'bairro'                => $empresa->bairro,
            'cidade'                => $empresa->cidade,
            'estado'                => $empresa->estado,
            'cep'                   => $empresa->cep,
            'complemento'           => $empresa->complemento,
            'observacao'            => $empresa->observacao,
            'contatoNomeEmpresa'    => $empresa->contatoNomeEmpresa,
            'contatoSexo'           => $empresa->contatoSexo,
            'contatoCpf'            => $empresa->contatoCpf,
            'contatoEmail'          => $empresa->contatoEmail,
            'contatoDataNascimento' => $empresa->contatoDataNascimento,
            'contatoTelefone'       => $empresa->contatoTelefone,
            'contatoCelular'        => $empresa->contatoCelular,
            'status'                => $empresa->status,
            'hostname'              => $empresa->hostname,
            'username'              => $empresa->username,
            'password'              => $empresa->password,
            'dataCadastro'          => date('Y-m-d'),
            'filial_id'             => 1,
            'cert'                  => $empresa->cert,
            'certsenha'             => $empresa->certsenha,
        );

        $this->empresa_model->addSelectBD('empresa', $data_empresa, $documento);

        //criar a filial na base do cliente
        $data_filial = array(
            'idEmpresa'     => 1,
            'nome'          => $empresa->nomeEmpresa,
            'cnpj'          => $documento,
            'cep'           => $empresa->cep,
            'rua'           => $empresa->rua,
            'numero'        => $empresa->numero,
            'bairro'        => $empresa->bairro,
            'cidade'        => $empresa->cidade,
            'estado'        => $empresa->estado,
            'telefone'      => $empresa->telefone,
            'email'         => $empresa->email,
            'complemento'   => $empresa->complemento,
        );
        $this->empresa_model->addSelectBD('filial', $data_filial, $documento);

        //criar a emitente na base do cliente
        $data_emitente = array(
            'nome'                  => $empresa->nomeEmpresa,
            'cnpj'                  => $empresa->documento,
            'cep'                   => $empresa->cep,
            'complemento'           => $empresa->complemento,
            'rua'                   => $empresa->rua,
            'numero'                => $empresa->numero,
            'bairro'                => $empresa->bairro,
            'cidade'                => $empresa->cidade,
            'uf'                    => $empresa->estado,
            'telefone'              => $empresa->telefone,
            'email'                 => $empresa->email,
            'ie'                    => $empresa->IE,
            'cnae'                  => $empresa->cnae,
            'csc'                   => $empresa->csc,
            'idcsc'                 => $empresa->idcsc,
            'tokenibpt'             => $empresa->tokenibpt,
            'inscst'                => $empresa->IEST,
            'insmun'                => $empresa->insmun,
            'nomeFanstasia'         => $empresa->nomeFantasiaApelido,
            'codigoCidade'          => $empresa->codIBGECidade,
            'cMun'                  => $empresa->codIBGECidade,
            'cert'                  => $empresa->cert,
            'certsenha'             => $empresa->certsenha,
            'regimeTributario'      => $empresa->regimeTributario,
            'incentivadorCultural'  => $empresa->incentivadorCultural,
            'incentivoFiscal'       => $empresa->incentivoFiscal,
            'tipoTributacao'        => $empresa->tipoTributacao,
            'naturezaTributacao'    => $empresa->naturezaTributacao,
            'certificado'           => $empresa->certificado,
            'regimeEspecialTributacao' => $empresa->regimeEspecialTributacao,
            'configuracao_tecnospeed_id' => 1,
            'configuracao_id'            => 1,
            'filial_id'                  => 1,
            'ramo_atividade'             => 1,//apenas para manutencao de veiculos
        );
        $this->empresa_model->addSelectBD('emitente', $data_emitente, $documento);

        //criar grupo tecnospeed na base do cliente
        if ($empresa->configuracao_tecnospeed_id) {

            $gupo_tecnospeed = $this->configuracaotecnospeed_model->getById($empresa->configuracao_tecnospeed_id);
            $dados_grupo_tecnospeed = array(
                'cod_grupo'          => $gupo_tecnospeed->cod_grupo,
                'descricao'          => $gupo_tecnospeed->descricao,
                'grupo'              => $gupo_tecnospeed->grupo,
                'usuario'            => $gupo_tecnospeed->usuario,
                'senha'              => $gupo_tecnospeed->senha,
                'filial_id'          => 1
            );
            $this->empresa_model->addSelectBD('configuracao_tecnospeed', $dados_grupo_tecnospeed, $documento);

            //criar empresa dentro do servidor da tecnospeed
             $this->tecnospeed_model->cadastrarEmpresa($empresa, $gupo_tecnospeed->cod_grupo);
        }

        //adicionar configuracao geral do sistema
        $dados_configuracao = array(
            'descricao'          => 'Configuração geral',
            'filial_id'          => 1
        );
        $this->empresa_model->addSelectBD('configuracao', $dados_configuracao, $documento);

        //criar a filial na base master
        $data_filial = array(
            'idEmpresa'     =>$empresa->idEmpresa,
            'cep'           => $empresa->cep,
            'nome'          => $empresa->nomeEmpresa,
            'cnpj'          => $documento,
            'rua'           => $empresa->rua,
            'numero'        => $empresa->numero,
            'bairro'        => $empresa->bairro,
            'cidade'        => $empresa->cidade,
            'estado'        => $empresa->estado,
            'telefone'      => $empresa->telefone,
            'email'         => $empresa->email,
            'complemento'   => $empresa->complemento,
        );
        $idFilialBaseMaster = $this->empresa_model->add('filial', $data_filial, true);

        //criar contador
        if ($empresa->contador_id) {
            $contador = $this->contador_model->getById($empresa->contador_id);
            $dados_contador = array(
                'nome'          => $contador->nome,
                'cnpjCpf'       => $contador->cnpjCpf,
                'login'         => $contador->login,
                'senha'         => $contador->senha
            );
            $this->empresa_model->addSelectBD('contador', $dados_contador, $documento);
        }

        $data = array(
            'filial_id'    => $idFilialBaseMaster,
        );
        $this->empresa_model->edit('empresa', $data, 'idEmpresa', $idEmpresa);
    }

    function adicionarFilial() {

        $idFilial           =  $this->input->post('idFilial');
        $idEmpresa          = $this->input->post('idEmpresa');
        $idFilialCliente    =  $this->input->post('idFilialCliente');

        if ($idFilial) {
            $this->editarFilial($idFilial, $idFilialCliente);
        } else {
            $empresa = $this->empresa_model->getById($idEmpresa);

            $data_filial = array(
                'idEmpresa' => 1,
                'nome'      => $this->input->post('nome'),
                'ie'        => $this->input->post('ie'),
                'cep'       => $this->input->post('cep'),
                'cnpj'      => $this->input->post('cnpj'),
                'rua'       => $this->input->post('rua'),
                'numero'    => $this->input->post('numero'),
                'bairro'    => $this->input->post('bairro'),
                'cidade'    => $this->input->post('cidade'),
                'estado'    => $this->input->post('estado'),
                'telefone'  => $this->input->post('telefone'),
                'email'     => $this->input->post('email'),
                'complemento' => $this->input->post('complemento'),
            );

            //atualizar os dados da filial na base do cliente
            $idFilialCliente = $this->empresa_model->addSelectBD('filial', $data_filial, $empresa->documento, TRUE);

            $data_usuario_filial = array(
                'usuario_id' => 1,
                'filial' => $idFilialCliente,
            );
            $this->empresa_model->addSelectBD('usuarios_filial', $data_usuario_filial, $empresa->documento, TRUE);

            //criar a emitente na base do cliente
            $data_emitente = array(
                'nome' => $this->input->post('nome'),
                'cnpj' => $this->input->post('cnpj'),
                'rua' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'bairro' => $this->input->post('bairro'),
                'cidade' => $this->input->post('cidade'),
                'uf' => $this->input->post('estado'),
                'telefone' => $this->input->post('telefone'),
                'email' => $this->input->post('email'),
                'filial_id' => $idFilialCliente
            );
            $idEmitenteCliente = $this->empresa_model->addSelectBD('emitente', $data_emitente, $empresa->documento, TRUE);

            $data_filial_master = array(
                'idEmpresa' => $idEmpresa,
                'nome' => $this->input->post('nome'),
                'ie' => $this->input->post('ie'),
                'cep'               => $this->input->post('cep'),
                'cnpj' => $this->input->post('cnpj'),
                'rua' => $this->input->post('rua'),
                'numero' => $this->input->post('numero'),
                'bairro' => $this->input->post('bairro'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
                'telefone' => $this->input->post('telefone'),
                'status'            => $this->input->post('status'),
                'email' => $this->input->post('email'),
                'complemento' => $this->input->post('complemento'),
                'idFilialCliente' => $idFilialCliente,
                'idEmitenteCliente' => $idEmitenteCliente,
            );

            //atualizar os dados da filial na base do master
            $this->empresa_model->add('filial', $data_filial_master);

            $this->session->set_flashdata('success', 'Nova Filial da Oficina criada com sucesso!');
        }
        redirect(base_url() . 'index.php/empresa/editar/' . $idEmpresa);
    }

    public function editarFilial($idFilial, $idFilialCliente) {

        $data_filial_master = array(
            'nome'              => $this->input->post('nome'),
            'ie'                => $this->input->post('ie'),
            'cep'               => $this->input->post('cep'),
            'cnpj'              => $this->input->post('cnpj'),
            'rua'               => $this->input->post('rua'),
            'numero'            => $this->input->post('numero'),
            'bairro'            => $this->input->post('bairro'),
            'cidade'            => $this->input->post('cidade'),
            'estado'            => $this->input->post('estado'),
            'telefone'          => $this->input->post('telefone'),
            'email'             => $this->input->post('email'),
            'status'            => $this->input->post('status'),
            'complemento'       => $this->input->post('complemento'),
         );

        //atualizar os dados da filial na base do master
        $this->empresa_model->edit('filial', $data_filial_master, 'idFilial',$idFilial);

        //atualizar os dados da filial na base do master
        $this->empresa_model->editSelectBD('filial', $data_filial_master, 'idFilial',$this->input->post('idFilialCliente'), $this->input->post('documento'));

        //criar a emitente na base do cliente
        $data_emitente = array(
            'nome'          => $this->input->post('nome'),
            'cnpj'          => $this->input->post('cnpj'),
            'rua'           => $this->input->post('rua'),
            'numero'        => $this->input->post('numero'),
            'bairro'        => $this->input->post('bairro'),
            'cidade'        => $this->input->post('cidade'),
            'uf'            => $this->input->post('estado'),
            'telefone'      => $this->input->post('telefone'),
            'email'         => $this->input->post('email'),
        );

        //atualizar os dados do emitente na base do cliente
        $this->empresa_model->editSelectBD('emitente', $data_emitente, 'id', $idFilialCliente, $this->input->post('documento'));
        $this->session->set_flashdata('success', 'Filial da Oficina atualizada com sucesso!');
    }

    function atualizacaoTabelas($idEmpresa) {

        require "leitor_sql.php";

        $empresa = $this->empresa_model->getById($idEmpresa);
        $prefixo = PREFIXO_BD;
        //$prefixo = '';

        $hostname   = $empresa->hostname;
        $username   = $empresa->username;
        $password   = $empresa->password;
        $documento  = $empresa->documento;

        $conn = new PDO('mysql:host=' .$hostname. ';dbname=' . $prefixo.$documento, $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $bancoDados   =  APPPATH.'/bancodados/update/versao/v1/banco.sql';

        new leitor_sql($bancoDados, $conn);

        $this->session->set_flashdata('success', 'Oficina editada com sucesso!');
        redirect(base_url() . 'index.php/empresa/editar/' . $idEmpresa);
    }

    function editar()
    {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        $empresa = $this->empresa_model->getById($this->uri->segment(3));

        if ($this->form_validation->run('empresa') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $cnpj = $this->input->post('documento');
            $this->mapos_model->criacaoPastasArquivos($cnpj);
            $cert   = $this->mapos_model->gravarCertificado($cnpj, $_FILES);

            if (!$cert) {
                $cert = $empresa->cert;
            }
            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeEmpresa'           => $this->input->post('nomeEmpresa'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'codIBGECidade'         => $this->input->post('codIBGECidade'),
                'codIBGEEstado'         => $this->input->post('codIBGEEstado'),
                'natureza_juridica'     => $this->input->post('natureza_juridica'),
                'IE'                    => $this->input->post('IE'),
                'incentivadorCultural'  => $this->input->post('incentivadorCultural'),
                'incentivoFiscal'       => $this->input->post('incentivoFiscal'),
                'tipoTributacao'        => $this->input->post('tipoTributacao'),
                'naturezaTributacao'    => $this->input->post('naturezaTributacao'),
                'IESUF'                 => $this->input->post('IESUF'),
                'IEST'                  => $this->input->post('IEST'),
                'insmun'                => $this->input->post('insmun'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'cert'                  => $cert,
                'certsenha'             => $this->input->post('certsenha'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeEmpresa'    => $this->input->post('contatoNomeEmpresa'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'hostname'              => $this->input->post('hostname'),
                'username'              => $this->input->post('username'),
                'password'              => $this->input->post('password'),
                'certificado'           => $this->input->post('certificado'),
                'ambiente'              => $this->input->post('ambiente'),
                'regimeTributario'      => $this->input->post('regimeTributario'),
                'cnae'                  => $this->input->post('cnae'),
                'csc'                   => $this->input->post('csc'),
                'idcsc'                 => $this->input->post('idcsc'),
                'tokenibpt'             => $this->input->post('tokenibpt'),
                'contador_id'           => $this->input->post('contador_id'),
                'marketplace_id'        => $this->input->post('marketplace_id'),
                'publishable_key'       => $this->input->post('publishable_key'),
                'on_behalf_of'          => $this->input->post('on_behalf_of'),
                'instrucaoBoletoOnline'          => $this->input->post('instrucaoBoletoOnline'),
                'regimeEspecialTributacao'    => $this->input->post('regimeEspecialTributacao'),
                'configuracao_tecnospeed_id'  => $this->input->post('configuracao_tecnospeed_id'),
                'status'                        => 1,
            );

            if ($this->empresa_model->edit('empresa', $data, 'idEmpresa', $this->input->post('idEmpresa')) == TRUE) {

                $empresa = $this->empresa_model->getById($this->uri->segment(3));

                $data_filial = array(
                    'nome'          => $empresa->nomeEmpresa,
                    'cnpj'          => $empresa->documento,
                    'rua'           => $empresa->rua,
                    'numero'        => $empresa->numero,
                    'bairro'        => $empresa->bairro,
                    'cidade'        => $empresa->cidade,
                    'estado'        => $empresa->estado,
                    'telefone'      => $empresa->telefone,
                    'email'         => $empresa->email,
                    'complemento'   => $empresa->complemento,
                );

                //atualizar os dados da filial na base do cliente
                $this->empresa_model->editSelectBD('filial', $data_filial, 'idFilial', 1, $this->input->post('documento'));

                //atualizar os dados da filial na base do master
                $this->empresa_model->edit('filial', $data_filial, 'idFilial', $this->input->post('filial_id'));

                $data_emitente = array(
                    'nome'                  => $empresa->nomeEmpresa,
                    'cnpj'                  => $empresa->documento,
                    'cep'                   => $empresa->cep,
                    'complemento'           => $empresa->complemento,
                    'rua'                   => $empresa->rua,
                    'numero'                => $empresa->numero,
                    'bairro'                => $empresa->bairro,
                    'cidade'                => $empresa->cidade,
                    'uf'                    => $empresa->estado,
                    'telefone'              => $empresa->telefone,
                    'email'                 => $empresa->email,
                    'ie'                    => $empresa->IE,
                    'cnae'                  => $empresa->cnae,
                    'csc'                   => $empresa->csc,
                    'idcsc'                 => $empresa->idcsc,
                    'tokenibpt'             => $empresa->tokenibpt,
                    'inscst'                => $empresa->IEST,
                    'insmun'                => $empresa->insmun,
                    'nomeFanstasia'         => $empresa->nomeFantasiaApelido,
                    'ambiente'              => $empresa->ambiente,
                    'codigoCidade'          => $empresa->codIBGECidade,
                    'cMun'                  => $empresa->codIBGECidade,
                    'cert'                  => $empresa->cert,
                    'certsenha'             => $empresa->certsenha,
                    'regimeTributario'      => $empresa->regimeTributario,
                    'regimeEspecialTributacao' => $empresa->regimeEspecialTributacao,
                    'incentivadorCultural'  => $empresa->incentivadorCultural,
                    'incentivoFiscal'       => $empresa->incentivoFiscal,
                    'tipoTributacao'        => $empresa->tipoTributacao,
                    'naturezaTributacao'    => $empresa->naturezaTributacao,
                    'certificado'           => $empresa->certificado,
                    'marketplace_id'        => $empresa->marketplace_id,
                    'publishable_key'       => $empresa->publishable_key,
                    'on_behalf_of'          => $empresa->on_behalf_of,
                    'instrucaoBoletoOnline' => $empresa->instrucaoBoletoOnline,
                );

                //atualizar os dados do emitente na base do cliente
                $this->empresa_model->editSelectBD('emitente', $data_emitente, 'id', 1, $this->input->post('documento'));

                //atualizar os dados da empresa na base do cliente
                $this->empresa_model->editSelectBD('empresa', $data, 'idEmpresa', 1, $this->input->post('documento'));

                //adicionar o arquivo do cliente para controle de login
                $file_path = APPPATH.'config/clientes/'.$this->input->post('documento').'.txt';

                if ( ! file_exists($file_path)) file_put_contents($file_path, '', FILE_APPEND);

                if ($empresa->configuracao_tecnospeed_id) {

                    $gupo_tecnospeed    = $this->configuracaotecnospeed_model->getById($empresa->configuracao_tecnospeed_id);
                    $configuracao       = $this->empresa_model->getConfiguracaoTecnoSpeedByFilialBD(1,  $this->input->post('documento'));

                    $dados_grupo_tecnospeed = array(
                        'cod_grupo' => $gupo_tecnospeed->cod_grupo,
                        'descricao' => $gupo_tecnospeed->descricao,
                        'grupo' => $gupo_tecnospeed->grupo,
                        'usuario' => $gupo_tecnospeed->usuario,
                        'senha' => $gupo_tecnospeed->senha,
                        'filial_id' => 1
                    );

                    if (count($configuracao) > 0) {
                        $this->empresa_model->editSelectBD('configuracao_tecnospeed', $dados_grupo_tecnospeed, 'idConfiguracaotecnospeed',$configuracao->idConfiguracaotecnospeed, $empresa->documento);
                    } else {
                        $this->empresa_model->addSelectBD('configuracao_tecnospeed', $dados_grupo_tecnospeed, $empresa->documento);
                    }
                    //criar empresa dentro do servidor da tecnospeed
                     $this->tecnospeed_model->cadastrarEmpresa($empresa, $gupo_tecnospeed->cod_grupo);
                }


                //criar contador
                if ($empresa->contador_id) {

                    $contadorPai = $this->contador_model->getById($empresa->contador_id);
                    $contador    = $this->empresa_model->getContadorById($contadorPai->idContador, $empresa->documento);

                    $dados_contador = array(
                        'nome'      => $contadorPai->nome,
                        'cnpjCpf'   => $contadorPai->cnpjCpf,
                        'login'     => $contadorPai->login,
                        'senha'     => $contadorPai->senha
                    );

                    if (count($contador) >0) {
                        $this->empresa_model->editSelectBD('contador', $dados_contador, 'idContador',$contador->idContador, $empresa->documento);
                    } else {
                        $this->empresa_model->addSelectBD('contador', $dados_contador, $empresa->documento);
                    }
                }

                $this->session->set_flashdata('success', 'Oficina editada com sucesso!');
                redirect(base_url() . 'index.php/empresa/editar/' . $this->input->post('idEmpresa'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['contadores']   = $this->contador_model->getAll();
        $this->data['tecnospeeds']  = $this->configuracaotecnospeed_model->getAll();
        $this->data['filiais']      = $this->empresa_model->getFiliaisEmpresa($this->uri->segment(3));
        $this->data['result']       =  $empresa;
        $this->data['output']       = 'empresa/editarEmpresa';
        $this->load->view('master/template', $this->data);
    }

    public function visualizar()
    {
        $this->data['custom_error'] = '';
        $this->data['result'] = $this->empresa_model->getById($this->uri->segment(3));
        $this->data['output'] = 'empresa/visualizar';
        $this->load->view('master/template', $this->data);
    }

    public function inativar() {
        $data = array(
            'status' => 2,
        );

        $empresa = $this->empresa_model->getById($this->input->post('id'));

        $this->empresa_model->edit('empresa', $data, 'idEmpresa', $empresa->idEmpresa);

        print_r($empresa);

        //atualizar os dados da filial na base do master
        $this->empresa_model->editSelectBD('filial', $data, 'idFilial', 1 ,$empresa->documento);

        redirect(base_url() . 'index.php/empresa/gerenciar/');
    }

    public function ativar() {
        $data = array(
            'status' => 1,
        );
        $empresa = $this->empresa_model->getById($this->input->post('id'));

        $this->empresa_model->edit('empresa', $data, 'idEmpresa', $empresa->idEmpresa);

        //atualizar os dados da filial na base do master
        $this->empresa_model->editSelectBD('filial', $data, 'idFilial', 1 ,$empresa->documento);
        redirect(base_url() . 'index.php/empresa/gerenciar/');
    }

    public function getFilialByIDJSON($idFilial) {
        $filial = $this->empresa_model->getByFilialId($idFilial);
        $json = array('filial' => $filial);
        echo json_encode($json);
    }

    public function excluir()
    {
        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir empresa.');
            redirect(base_url() . 'index.php/empresa/gerenciar/');
        }

        $this->empresa_model->delete('empresa', 'idEmpresa', $id);
        $this->session->set_flashdata('success', 'Cliente excluido com sucesso!');
        redirect(base_url() . 'index.php/empresa/gerenciar/');
    }

    public function sair() {
        $this->session->sess_destroy();
    }

    public function getCEP($cep) {

    }

}

