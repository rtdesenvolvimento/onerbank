<?php

class equipamentoveiculo extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('equipamentoveiculo_model', '', TRUE);
        $this->load->model('visaoautomovel_model', '', TRUE);
        $this->load->model('tipoveiculo_model', '', TRUE);

        $this->data['menuEquipamentoveiculo'] = 'Equipamento veículo';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vEquipamentoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar equipamentos do veículo.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/equipamentoveiculo/gerenciar/';
        $config['total_rows'] = $this->equipamentoveiculo_model->count('equipamentoveiculo');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->equipamentoveiculo_model->get('equipamentoveiculo','tipoVeiculo_id,idEquipamentoveiculo,nome,descricao, idVisaoautomovel','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'equipamentoveiculo/equipamentoveiculo';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aEquipamentoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar equipamento do veículo.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('equipamentoveiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'idVisaoautomovel' => set_value('idVisaoautomovel'),
                'tipoVeiculo_id' => set_value('tipoVeiculo_id'),
                'client_x' => set_value('client_x'),
                'client_y' => set_value('client_y'),
                'descricao' => set_value('descricao')
            );

            if ($this->equipamentoveiculo_model->add('equipamentoveiculo', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Equipamento veículo adicionado com sucesso!');
                redirect(base_url() . 'index.php/equipamentoveiculo/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['tiposveiculo']  = $this->tipoveiculo_model->getAll();
        $this->data['visualautomovel']  = $this->visaoautomovel_model->getAll();
        $this->data['view']             = 'equipamentoveiculo/adicionarEquipamentoveiculo';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eEquipamentoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar equipamento veículo.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('equipamentoveiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'idVisaoautomovel' => $this->input->post('idVisaoautomovel'),
                'tipoVeiculo_id' => $this->input->post('tipoVeiculo_id'),
                'client_x' => $this->input->post('client_x'),
                'client_y' => $this->input->post('client_y'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->equipamentoveiculo_model->edit('equipamentoveiculo', $data, 'idEquipamentoveiculo', $this->input->post('idEquipamentoveiculo')) == TRUE) {
                $this->session->set_flashdata('success', 'Equipvamento veículo editado com sucesso!');
                redirect(base_url() . 'index.php/equipamentoveiculo/editar/'.$this->input->post('idEquipamentoveiculo'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['tiposveiculo']  = $this->tipoveiculo_model->getAll();
        $this->data['visualautomovel']  = $this->visaoautomovel_model->getAll();
        $this->data['result'] = $this->equipamentoveiculo_model->getById($this->uri->segment(3));
        $this->data['view'] = 'equipamentoveiculo/editarEquipamentoveiculo';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dEquipamentoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir equipamento veículo.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir equipamento veículo.');
            redirect(base_url().'index.php/equipamentoveiculo/gerenciar/');
        }

        $this->equipamentoveiculo_model->delete('equipamentoveiculo','idEquipamentoveiculo',$id);

        $this->session->set_flashdata('success','Equipamento veículo excluido com sucesso!');
        redirect(base_url().'index.php/equipamentoveiculo/gerenciar/');
    }
}

