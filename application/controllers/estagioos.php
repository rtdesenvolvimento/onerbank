<?php

class estagioos extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('estagioos_model', '', TRUE);
        $this->data['menuEstagioos'] = 'Estagioos';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vEstagioos')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar estágios.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/estagioos/gerenciar/';
        $config['total_rows'] = $this->estagioos_model->count('estagioos');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->estagioos_model->get('estagioos','idEstagioos,nome,alias, status, descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'estagioos/estagioos';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aEstagioos')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar estágio.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('estagioos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome'      => set_value('nome'),
                'alias'     => $this->input->post('alias'),
                'status'    => $this->input->post('status'),
                'descricao' => set_value('descricao')
            );

            if ($this->estagioos_model->add('estagioos', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Estágio adicionado com sucesso!');
                redirect(base_url() . 'index.php/estagioos/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'estagioos/adicionarEstagioos';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eEstagioos')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar estágio.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('estagioos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome'      => $this->input->post('nome'),
                'alias'     => $this->input->post('alias'),
                'status'    => $this->input->post('status'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->estagioos_model->edit('estagioos', $data, 'idEstagioos', $this->input->post('idEstagioos')) == TRUE) {
                $this->session->set_flashdata('success', 'Aparelho editado com sucesso!');
                redirect(base_url() . 'index.php/estagioos/editar/'.$this->input->post('idEstagioos'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->estagioos_model->getById($this->uri->segment(3));
        $this->data['view'] = 'estagioos/editarEstagioos';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dEstagioos')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir estágios.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir estágio.');
            redirect(base_url().'index.php/estagioos/gerenciar/');
        }

        $this->estagioos_model->delete('estagioos','idEstagioos',$id);

        $this->session->set_flashdata('success','Estágio excluido com sucesso!');
        redirect(base_url().'index.php/estagioos/gerenciar/');
    }
}

