<?php

class filial extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('filial_model', '', TRUE);
        $this->load->model('empresa_model', '', TRUE);

        $this->data['menuFilial'] = 'Filial';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vFilial')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar filial.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/filial/gerenciar/';
        $config['total_rows'] = $this->filial_model->count('filial');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->filial_model->get('filial','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'filial/filial';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aFilial')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar filial.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('filial') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'idEmpresa' => set_value('idEmpresa'),
                'nome' => set_value('nome')
            );

            if ($this->filial_model->add('filial', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Filial adicionada com sucesso!');
                redirect(base_url() . 'index.php/filial/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['empresas']  = $this->empresa_model->getAll();
        $this->data['view'] = 'filial/adicionarFilial';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eFilial')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar filial.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('filial') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'idEmpresa' => $this->input->post('idEmpresa'),
                'nome' => $this->input->post('nome')
            );

            if ($this->filial_model->edit('filial', $data, 'idFilial', $this->input->post('idFilial')) == TRUE) {
                $this->session->set_flashdata('success', 'Filial editada com sucesso!');
                redirect(base_url() . 'index.php/filial/editar/'.$this->input->post('idFilial'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['empresas']  = $this->empresa_model->getAll();
        $this->data['result'] = $this->filial_model->getById($this->uri->segment(3));
        $this->data['view'] = 'filial/editarFilial';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dFilial')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir filial.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir esta filial.');
            redirect(base_url().'index.php/filial/gerenciar/');
        }

        $this->filial_model->delete('filial','idFilial',$id);

        $this->session->set_flashdata('success','Filial excluida com sucesso!');
        redirect(base_url().'index.php/filial/gerenciar/');
    }
}

