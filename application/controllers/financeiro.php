<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financeiro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
        	redirect('mapos/login');
        }

        $this->load->model('mapos_model','',TRUE);
        $this->load->model('financeiro_model','',TRUE);
		$this->load->model('zoop_model','',TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('fornecedor_model', '', TRUE);
        $this->load->model('forma_pagamento_model', '', TRUE);
        $this->load->model('receita_model', '', TRUE);
        $this->load->model('despesa_model', '', TRUE);
        $this->load->model('condicao_pagamento_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->load->model('linkpagamento_model', '', TRUE);
        $this->load->model('tipo_cobranca_model', '', TRUE);

        $this->load->model('reciboservice_model', '', TRUE);
        $this->load->model('recibo_model', '', TRUE);
        $this->load->model('reciborepository_model', '', TRUE);
        $this->load->model('vendas_model', '', TRUE);
        $this->load->model('nfce_model', '', TRUE);

        $this->data['menuFinanceiro'] = 'financeiro';
        $this->data['menuTela'] = 'financeiro';
        $this->load->helper(array('codegen_helper'));
	}
	public function index(){
		$this->lancamentos();
	}

	public function gerarRecibo($id) {
        return $this->reciboservice_model->buscar($id);
    }

    public function excluir($id){
        try {
            $this->reciboservice_model->excluir($id);
        } catch (Exception $e) {
            print_r($e);
        }
    }

    public function faturas(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vLancamento')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar lançamentos.');
            redirect(base_url());
        }

        $periodo        = $this->input->get('periodo');
        $situacao       = $this->input->get('situacao');
        $tipolancamento = $this->input->get('tipolancamento');
        $termo          = $this->input->get('termo');
        $clienteId      = $this->input->get('clienteId');

        if (!$periodo) $periodo = 'mes';
        if (!$tipolancamento)  $tipolancamento = 'todos';
        if (!$situacao)  $situacao = 'FATURADA_BOLETO';

        $where = 'parcela.idParcela is not null ';

        if ($tipolancamento == 'receita') $where .= "  and lancamentos.tipo = 'receita' ";
        else if ($tipolancamento == 'despesa') $where .= " and lancamentos.tipo = 'despesa' ";

        if($situacao != 'todos') {
            if ($situacao == 'FATURADA_BOLETO') $where .= ' and ( parcela.status = "FATURADA" OR parcela.status = "Aberta")';
            else if ($situacao == 'ATRASADA') {
                $where .= ' and parcela.dtVencimento < "'.date('Y-m-d').'"';
                $where .= ' and ( parcela.status != "PAGO" AND parcela.status != "cancelada" ) ';
            } else $where .= ' and parcela.status = "'.$situacao.'"';
        }

        if ($termo != '') $where = $where.' and ( upper(nomeCliente) like upper("%'.$termo.'%") or upper(nomeFornecedor) like upper("%'.$termo.'%") ) ';
        if ($periodo == 'dia') $where = $where.' and parcela.dtVencimento = "'.date('Y-m-d').'"';
        if ($periodo == 'mes') $where = $where.' and DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"';
        if ($periodo == 'ano') $where = $where.' and DATE_FORMAT(parcela.dtVencimento,"%Y") = "'.date('Y').'"';

        if ($periodo == 'filtros_por_data') {

            if ($this->input->get('dataInicial') != '') $where = $where.' and parcela.dtVencimento >= "'.$this->input->get('dataInicial').'"';
            if ($this->input->get('dataFinal') != '') $where = $where.' and parcela.dtVencimento <= "'.$this->input->get('dataFinal').'"';

            if ($this->input->get('dataPagamentoInicial') != '') $where = $where.' and parcela.dtUltimoPagamento >= "'.$this->input->get('dataPagamentoInicial').'"';
            if ($this->input->get('dataPagamentoFinal') != '') $where = $where.' and parcela.dtUltimoPagamento <= "'.$this->input->get('dataPagamentoFinal').'"';

        }

        $semana = $this->getThisWeek();
        if ($periodo == 'semana')  $where = $where.' and parcela.dtVencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'"';

        $where = $where.' and clientes.idClientes = '.$clienteId;

        $this->load->library('pagination');
        $config['base_url'] = base_url().'financeiro/lancamentos';
        $config['total_rows'] = $this->financeiro_model->count('lancamentos');
        $config['per_page'] = 1000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $this->data['condicoes']         = $this->condicao_pagamento_model->getAll();
        $this->data['despesas']          = $this->despesa_model->getAll();
        $this->data['receitas']          = $this->receita_model->getAll();
        $this->data['formasPagamento']   = $this->forma_pagamento_model->getAll();
        $this->data['fornecedores']      = $this->fornecedor_model->getAll();
        $this->data['clientes']          = $this->clientes_model->getAll();
        $this->data['tiposcobranca']     = $this->tipo_cobranca_model->getAll();
        $this->data['clienteId']         = $clienteId;

        $this->data['results'] = $this->financeiro_model->get('parcela','idLancamentos,custo,data_vencimento,data_pagamento,previsaoPagamento,baixado,os,venda,clientes_id,fornecedor_id,lancamentos.tipo,forma_pgto',$where,$config['per_page'],$this->uri->segment(3));
        $this->data['view'] = 'financeiro/faturas';
        $this->load->view('theme/topo-blank',$this->data);
    }

    public function lancamentos(){

		if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vLancamento')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar lançamentos.');
           redirect(base_url());
        }

		$periodo        = $this->input->get('periodo');
		$situacao       = $this->input->get('situacao');
        $tipolancamento = $this->input->get('tipolancamento');
        $termo          = $this->input->get('termo');

        if (!$periodo) $periodo = 'mes';
        if (!$tipolancamento)  $tipolancamento = 'todos';
        if (!$situacao)  $situacao = 'FATURADA_BOLETO';

        $where = 'parcela.idParcela is not null ';

        if ($tipolancamento == 'receita') $where .= " and lancamentos.tipo = 'receita' ";
        else if ($tipolancamento == 'despesa') $where .= " and lancamentos.tipo = 'despesa' ";

        if($situacao != 'todos') {
            if ($situacao == 'FATURADA_BOLETO') $where .= ' and parcela.status = "FATURADA"';
            else if ($situacao == 'ATRASADA') {
                $where .= 'and parcela.dtVencimento < "'.date('Y-m-d').'"';
                $where .= ' and ( parcela.status != "PAGO" AND parcela.status != "cancelada" ) ';
            } else $where .= ' and parcela.status = "'.$situacao.'"';
        }

        if ($termo != '') $where = $where.' and ( upper(nomeCliente) like upper("%'.$termo.'%") or upper(nomeFornecedor) like upper("%'.$termo.'%") ) ';

        if ($periodo == 'dia') $where = $where.' and parcela.dtVencimento = "'.date('Y-m-d').'"';
        if ($periodo == 'mes') $where = $where.' and DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"';
        if ($periodo == 'ano') $where = $where.' and DATE_FORMAT(parcela.dtVencimento,"%Y") = "'.date('Y').'"';

        if ($periodo == 'filtros_por_data') {
            if ($this->input->get('dataInicial') != '') $where = $where.' and parcela.dtVencimento >= "'.$this->input->get('dataInicial').'"';
            if ($this->input->get('dataFinal') != '') $where = $where.' and parcela.dtVencimento <= "'.$this->input->get('dataFinal').'"';

            if ($this->input->get('dataPagamentoInicial') != '') $where = $where.' and parcela.dtUltimoPagamento >= "'.$this->input->get('dataPagamentoInicial').'"';
            if ($this->input->get('dataPagamentoFinal') != '') $where = $where.' and parcela.dtUltimoPagamento <= "'.$this->input->get('dataPagamentoFinal').'"';
        }

        $semana = $this->getThisWeek();
        if ($periodo == 'semana')  $where = $where.' and parcela.dtVencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'"';

        $this->load->library('pagination');
        $config['base_url'] = base_url().'financeiro/lancamentos';
        $config['total_rows'] = $this->financeiro_model->count('lancamentos');
        $config['per_page'] = 1000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';	
        $this->pagination->initialize($config);

        $this->data['condicoes']         = $this->condicao_pagamento_model->getAll();
        $this->data['despesas']          = $this->despesa_model->getAll();
        $this->data['receitas']          = $this->receita_model->getAll();
        $this->data['formasPagamento']   = $this->forma_pagamento_model->getAll();
        $this->data['fornecedores']      = $this->fornecedor_model->getAll();
        $this->data['clientes']          = $this->clientes_model->getAll();
        $this->data['tiposcobranca']     = $this->tipo_cobranca_model->getAll();

		$this->data['results'] = $this->financeiro_model->get('parcela','idLancamentos,custo,data_vencimento,data_pagamento,previsaoPagamento,baixado,os,venda,clientes_id,fornecedor_id,lancamentos.tipo,forma_pgto',$where,$config['per_page'],$this->uri->segment(3));
	    $this->data['view'] = 'financeiro/lancamentos';
       	$this->load->view('theme/topo',$this->data);
	}

    public function lancamentos2(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vLancamento')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar lançamentos.');
            redirect(base_url());
        }

        $where          = '';
        $whereData      = '';
        $periodo        = $this->input->get('periodo');
        $situacao       = $this->input->get('situacao');
        $tipolancamento = $this->input->get('tipolancamento');

        // busca todos os lançamentos
        if($periodo == 'todos'){

            if($situacao == 'previsto'){
                $where = 'data_vencimento > "'.date('Y-m-d').'" AND baixado = "0"';
            } else{
                if($situacao == 'atrasado'){
                    $where = 'data_vencimento < "'.date('Y-m-d').'" AND baixado = "0"';
                }
                else{

                    if($situacao == 'realizado'){
                        $where = 'baixado = "1"';
                    }

                    if($situacao == 'pendente'){
                        $where = 'baixado = "0"';
                    }
                }
            }
        }  else{

            // busca lançamentos do dia
            if($periodo == null || $periodo == 'dia'){
                $where = 'data_vencimento = "'.date('Y-m-d'.'"');
            } // fim lançamentos dia

            else{

                // busca lançamentos da semana
                if($periodo == 'semana'){
                    $semana = $this->getThisWeek();

                    if(! isset($situacao) || $situacao == 'todos'){
                        $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'"';
                    }
                    else{
                        if($situacao == 'previsto'){
                            $where = 'data_vencimento BETWEEN "'.date('Y-m-d').'" AND "'.$semana[1].'" AND baixado = "0"';
                        }
                        else{
                            if($situacao == 'atrasado'){
                                $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.date('Y-m-d').'" AND baixado = "0"';
                            }
                            else{
                                if($situacao == 'realizado'){
                                    $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'" AND baixado = "1"';
                                }
                                else{
                                    $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'" AND baixado = "0"';
                                }
                            }
                        }
                    }

                } // fim lançamentos dia
                else{

                    // busca lançamento do mês


                    if($periodo == 'mes'){

                        $mes = $this->getThisMonth();

                        if(! isset($situacao) || $situacao == 'todos'){

                            $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.$mes[1].'"';

                        }
                        else{
                            if($situacao == 'previsto'){
                                $where = 'data_vencimento BETWEEN "'.date('Y-m-d').'" AND "'.$mes[1].'" AND baixado = "0"';
                            }
                            else{
                                if($situacao == 'atrasado'){
                                    $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.date('Y-m-d').'" AND baixado = "0"';
                                }
                                else{
                                    if($situacao == 'realizado'){
                                        $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.$mes[1].'" AND baixado = "1"';
                                    }
                                    else{
                                        $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.$mes[1].'" AND baixado = "0"';
                                    }

                                }
                            }
                        }
                    }

                    // busca lançamentos do ano
                    else if ($periodo == 'filtros_por_data') {

                        $dataInicial = $this->input->get('dataInicial');
                        $dataFinal = $this->input->get('dataFinal');

                        $previsaoPagamentoInicial = $this->input->get('previsaoPagamentoInicial');
                        $previsaoPagamentoFinal = $this->input->get('previsaoPagamentoFinal');

                        if ($dataInicial != null) {
                            $whereData .= 'data_vencimento >= "'.$dataInicial.'" AND ';
                        }

                        if ($dataFinal != null) {
                            $whereData .= 'data_vencimento <= "'.$dataFinal.'" AND ';
                        }

                        if ($previsaoPagamentoInicial != null) {
                            $whereData .= 'previsaoPagamento >= "'.$previsaoPagamentoInicial.'" AND ';
                        }

                        if ($previsaoPagamentoFinal != null) {
                            $whereData .= 'previsaoPagamento <= "'.$previsaoPagamentoFinal.'" AND ';
                        }


                        if($situacao == 'pendente'){
                            $where = 'baixado = "0" AND ';
                        }

                        if($situacao == 'realizado'){
                            $where = 'baixado = "1" AND ';
                        }

                        $igual = '';
                        if ($where) {
                            $igual = '1=1';
                        }

                        if ($whereData) {
                            $igual = '1=1';
                        }
                        $where = $where.$whereData.$igual;


                    } else {
                        $ano = $this->getThisYear();

                        if(! isset($situacao) || $situacao == 'todos'){
                            $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.$ano[1].'"';
                        }
                        else{
                            if($situacao == 'previsto'){
                                $where = 'data_vencimento BETWEEN "'.date('Y-m-d').'" AND "'.$ano[1].'" AND baixado = "0"';
                            }
                            else{
                                if($situacao == 'atrasado'){
                                    $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.date('Y-m-d').'" AND baixado = "0"';
                                }
                                else{
                                    if($situacao == 'realizado'){
                                        $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.$ano[1].'" AND baixado = "1"';
                                    }
                                    else{
                                        $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.$ano[1].'" AND baixado = "0"';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($tipolancamento == 'receita') {
            $where = "tipo = 'receita' ";
        } else if ($tipolancamento == 'despesa') {
            $where = "tipo = 'despesa' ";
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url().'financeiro/lancamentos';
        $config['total_rows'] = $this->financeiro_model->count('lancamentos');
        $config['per_page'] = 1000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $this->data['formasPagamento']     = $this->forma_pagamento_model->getAll();
        $this->data['fornecedores']     = $this->fornecedor_model->getAll();
        $this->data['clientes']     = $this->clientes_model->getAll();
        $this->data['despesas']          = $this->despesa_model->getAll();
        $this->data['receitas']          = $this->receita_model->getAll();
        $this->data['results'] = $this->financeiro_model->getv1('lancamentos','*',$where,$config['per_page'],$this->uri->segment(3));
        $this->data['view'] = 'financeiro/lancamentos2';
        $this->load->view('tema/topo',$this->data);
    }

    public function lancamentosRelatorioExecuta()
    {

        $periodo        = $this->input->get('periodo');
        $situacao       = $this->input->get('situacao');
        $tipolancamento = $this->input->get('tipolancamento');
        $termo          = $this->input->get('termo');
        $clienteId      = $this->input->get('clienteId');

        if (!$periodo) $periodo = 'mes';
        if (!$tipolancamento)  $tipolancamento = 'todos';
        if (!$situacao)  $situacao = 'FATURADA_BOLETO';

        $where = 'parcela.idParcela is not null';

        if ($tipolancamento == 'receita') $where = "lancamentos.tipo = 'receita'";
        else if ($tipolancamento == 'despesa') $where = "lancamentos.tipo = 'despesa'";

        if($situacao != 'todos') {
            if ($situacao == 'FATURADA_BOLETO') $where = 'parcela.status = "FATURADA"';
            else if ($situacao == 'ATRASADA') {
                $where = 'parcela.dtVencimento < "'.date('Y-m-d').'"';
                $where .= ' AND ( parcela.status != "PAGO" AND parcela.status != "cancelada" ) ';
            } else $where = 'parcela.status = "'.$situacao.'"';
        }

        if ($termo != '') $where = $where.' and ( upper(nomeCliente) like upper("%'.$termo.'%") or upper(nomeFornecedor) like upper("%'.$termo.'%") ) ';
        if ($periodo == 'dia') $where = $where.' and parcela.dtVencimento = "'.date('Y-m-d').'"';
        if ($periodo == 'mes') $where = $where.' and DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"';
        if ($periodo == 'ano') $where = $where.' and DATE_FORMAT(parcela.dtVencimento,"%Y") = "'.date('Y').'"';

        if ($periodo == 'filtros_por_data') {
            if ($this->input->get('dataInicial') != '') $where = $where.' and parcela.dtVencimento >= "'.$this->input->get('dataInicial').'"';
            if ($this->input->get('dataFinal') != '') $where = $where.' and parcela.dtVencimento <= "'.$this->input->get('dataFinal').'"';
        }

        $semana = $this->getThisWeek();
        if ($periodo == 'semana')  $where = $where.' and parcela.dtVencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'"';

        if ($clienteId) $where = $where.' and   clientes.idClientes = '.$clienteId;

        $this->data['termo'] = $termo;
        $this->data['periodo'] = $periodo;
        $this->data['situacao'] = $situacao;
        $this->data['tipolancamento'] = $tipolancamento;
        $this->data['dataInicial'] = $this->input->get('dataInicial');
        $this->data['dataFinal'] = $this->input->get('dataFinal');

        $this->data['results'] = $this->financeiro_model->get('parcela','idLancamentos,custo,data_vencimento,data_pagamento,previsaoPagamento,baixado,os,venda,clientes_id,fornecedor_id,lancamentos.tipo,forma_pgto',$where,$config['per_page'],$this->uri->segment(3));

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirRelatorioDetalhado', $this->data, true);

        pdf_create($html, 'relatorio_lancamentos' . date('d/m/y'), TRUE);
    }

    function adicionarReceita() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aLancamento')){
            $this->session->set_flashdata('error','Você não tem permissão para adicionar lançamentos.');
            redirect(base_url());
        }

        $vencimento             = $this->input->post('vencimento');
        $recebimento            = $this->input->post('recebimento');
        $previsaoPagamento      = $this->input->post('previsaoPagamento');

        if($vencimento == null) $vencimento = date('Y-m-d');

        $data = array(
            'descricao' => $this->input->post('descricao'),
            'valor' => $this->input->post('valor'),
            'custo' => $this->input->post('custoReceita'),
            'receita_id' => $this->input->post('receita_id'),
            'clientes_id' => $this->input->post('cliente'),
            'baixado' => $this->input->post('recebido'),
            'forma_pgto' => $this->input->post('formaPgto'),
            'condicao_pagamento_id' => $this->input->post('condicao_pagamento_id'),
            'tipo_cobranca_id' => $this->input->post('tipo_cobranca_id'),
            'tipo' => 'receita',
            'data_vencimento' => $vencimento,
            'previsaoPagamento' => $previsaoPagamento
        );

        $idLancamento = $this->financeiro_model->add('lancamentos',$data,$this->input->post('valorVencimento'), $this->input->post('vencimentoParcelas'), 0);
        $encontrouErroGeracao = FALSE;

        if ($idLancamento == TRUE) {

            $tipoCobranca = $this->tipo_cobranca_model->getById($this->input->post('tipo_cobranca_id'));

            if ($tipoCobranca->tipo === 'boleto') {
                $parcelas = $this->financeiro_model->getParcelasByLancamento($idLancamento);

                foreach ($parcelas as $parcela) {
                    $retorno = $this->gerarBoleto($parcela->idParcela);

                    if ($retorno['boletoId'] == '') {
                        if (property_exists($retorno['error']->error,'reasons')) {
                            $encontrouErroGeracao =  $retorno['error']->error->reasons[0];
                        } else {
                            $encontrouErroGeracao = $retorno['error']->error->message;
                        }
                    }
                }
            }

            if ($encontrouErroGeracao) {
                $this->excluirLancamento($parcela->idParcela);
                echo $encontrouErroGeracao;
            } else {
                echo '';
            }

        } else {
            $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
        }
    }

	function adicionarDespesa() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aLancamento')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar lançamentos.');
           redirect(base_url());
        }

        $vencimento             = $this->input->post('vencimento');
        $recebimento            = $this->input->post('recebimento');
        $previsaoPagamento      = $this->input->post('previsaoPagamento');

        if($vencimento == null) $vencimento = date('Y-m-d');

        $data = array(
            'descricao' => $this->input->post('descricao'),
            'valor' => $this->input->post('valor'),
            'custo' => $this->input->post('custoReceita'),
            'despesa_id' => $this->input->post('despesa_id'),
            'clientes_id' => $this->input->post('cliente'),
            'baixado' => $this->input->post('recebido'),
            'forma_pgto' => $this->input->post('formaPgto'),
            'condicao_pagamento_id' => $this->input->post('condicao_pagamento_id'),
            'tipo_cobranca_id' => $this->input->post('tipo_cobranca_id'),
            'tipo' => 'despesa',
            'data_vencimento' => $vencimento,
            'previsaoPagamento' => $previsaoPagamento,
            'data_pagamento' => $recebimento,
        );

        if ($this->financeiro_model->add('lancamentos',$data, $this->input->post('valorVencimento'), $this->input->post('vencimentoParcelas'), 0) == TRUE) {
            $this->session->set_flashdata('success','Receita adicionada com sucesso!');
            exit();
        } else {
            $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
        }
    }

    public function gerarBoleto($idParcela) {

        $parcela    = $this->financeiro_model->getParcelaById($idParcela);
        $lancamento = $this->financeiro_model->getById($parcela->lancamentos_id);
        $faturas    = $this->financeiro_model->getFaturasParcelaByLancamento($idParcela);
        $emitente   = $this->mapos_model->getEmitente();
        $erro = false;

        foreach ($faturas as $fat) {

            $fatura = $this->financeiro_model->getFaturaById($fat->fatura_id);
            $cliente = $this->clientes_model->getById($fatura->clientes_id);

            if ($cliente->client_integration_id == '') {
                $jsonRetorno = json_decode($this->zoop_model->postCadBuyers($cliente));

                if (property_exists($jsonRetorno,'id')) {
                    $this->clientes_model->edit('clientes', array('client_integration_id' => $jsonRetorno->id), 'idClientes', $cliente->idClientes);
                } else {
                    $erro = $jsonRetorno;
                }

            } else {
                $jsonRetorno = json_decode($this->zoop_model->putGetCadBuyers($cliente));

                if (property_exists($jsonRetorno,'id')) {
                    $this->clientes_model->edit('clientes', array('client_integration_id' => $jsonRetorno->id), 'idClientes', $cliente->idClientes);
                } else {
                    $erro = $jsonRetorno;
                }
            }

            if ($erro) {
                $retorno = array(
                    'boletoId' => '',
                    'error' => $erro
                );
                return $retorno;
            }

            $cliente = $this->clientes_model->getById($fatura->clientes_id);
            $instrucoes = 'Parcela ' . $parcela->numeroParcela . '/' . $parcela->totalParcelas . '  -  ' . $lancamento->descricao . ' - ' . $emitente->instrucaoBoletoOnline;

            if ($cliente->client_integration_id != '') {

                $jsonRetorno = json_decode($this->zoop_model->postCadTransactionsBoleto($fatura, $cliente, 3, $instrucoes));

                if (property_exists($jsonRetorno,'id')) {

                    $configuracao = $this->zoop_model->getConfiguracaoFinanceira();
                    $multa = $configuracao->multaAtrasoBoleto;
                    $juros = $configuracao->jurosAtrasoBoleto;

                    $dados_boleto = array(
                        'cliente_id' => $cliente->idClientes,
                        'fatura_id' => $fatura->idFatura,
                        'parcela_id' => $idParcela,
                        'transactions_integration_id' => $jsonRetorno->id,
                        'url_boleto' => $jsonRetorno->payment_method->url,
                        'transactions_status' => $jsonRetorno->status,
                        'transactions_status_boleto' => $jsonRetorno->payment_method->status,
                        'barcode' => $jsonRetorno->payment_method->barcode,
                        'gateway_authorizer' => $jsonRetorno->gateway_authorizer,
                        'reference_number' => $jsonRetorno->payment_method->reference_number,
                        'document_number' => $jsonRetorno->payment_method->document_number,
                        'sequence' => $jsonRetorno->payment_method->sequence,
                        'description' => $jsonRetorno->payment_method->description,
                        'bank_code' => $jsonRetorno->payment_method->bank_code,
                        'expiration_date' => $jsonRetorno->payment_method->expiration_date,
                        'payment_limit_date' => $jsonRetorno->payment_method->payment_limit_date,
                        'multa' => $multa,
                        'juros' => $juros,
                    );

                    $retorno = array(
                        'boletoId' => $this->clientes_model->add('boleto', $dados_boleto, TRUE),
                        'error' => ''
                    );

                    return $retorno;
                } else {
                    $retorno = array(
                        'boletoId' => '',
                        'error' => $jsonRetorno
                    );

                    return $retorno;
                }
            }
        }
    }

    public function gerarBoletoZoop() {
        $retorno = $this->gerarBoleto($this->input->post('idParcela') );
        $encontrouErroGeracao = '';

	    if ($retorno['boletoId'] == '') {
            if (property_exists($retorno['error']->error,'reasons')) {
                $encontrouErroGeracao =  $retorno['error']->error->reasons[0];
            } else {
                $encontrouErroGeracao = $retorno['error']->error->message;
            }
        }
	    echo utf8_encode($encontrouErroGeracao);
    }

    public function send_json($data) {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    public function editar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eLancamento')){
           $this->session->set_flashdata('error','Você não tem permissão para editar lançamentos.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $urlAtual = $this->input->post('urlAtual');

        $this->form_validation->set_rules('valor', '', 'trim|required|xss_clean');
        $this->form_validation->set_rules('vencimento', '', 'trim|required|xss_clean');
        $this->form_validation->set_rules('pagamento', '', 'trim|xss_clean');

        if ($this->form_validation->run() == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $vencimento         = $this->input->post('vencimento');
            $pagamento          = $this->input->post('pagamento');
			$previsaoPagamento  = $this->input->post('previsaoPagamento');
            $tipo               = $this->input->post('tipo');

            if($vencimento == null){
                $vencimento = date('d/m/Y');
            }

            if ($tipo == 'receita') {
                $data = array(
                    'descricao' => $this->input->post('descricao'),
                    'valor' => $this->input->post('valor'),
                    'custo' => $this->input->post('custo'),
                    'clientes_id' => $this->input->post('cliente'),
                    'data_vencimento' => $vencimento,
                    'data_pagamento' => $pagamento,
                    'previsaoPagamento' => $previsaoPagamento,
                    'baixado' => $this->input->post('pago'),
                    'forma_pgto' => $this->input->post('formaPgto'),
                    'tipo' => $this->input->post('tipo')
                );
            } else {
                $data = array(
                    'descricao' => $this->input->post('descricao'),
                    'valor' => $this->input->post('valor'),
                    'custo' => $this->input->post('custo'),
                    'fornecedor_id' => $this->input->post('fornecedor_id'),
                    'data_vencimento' => $vencimento,
                    'data_pagamento' => $pagamento,
                    'previsaoPagamento' => $previsaoPagamento,
                    'baixado' => $this->input->post('pago'),
                    'forma_pgto' => $this->input->post('formaPgto'),
                    'tipo' => $this->input->post('tipo')
                );
            }

            if ($this->financeiro_model->edit('lancamentos',$data,'idLancamentos',$this->input->post('id')) == TRUE) {
                $this->session->set_flashdata('success','lançamento editado com sucesso!');
                redirect($urlAtual);
            } else {
                $this->session->set_flashdata('error','Ocorreu um erro ao tentar editar lançamento!');
                redirect($urlAtual);
            }
        }

        $this->session->set_flashdata('error','Ocorreu um erro ao tentar editar lançamento.');
        redirect($urlAtual);
    }

    public function editarParcela(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eLancamento')){
            $this->session->set_flashdata('error','Você não tem permissão para editar lançamentos.');
            redirect(base_url());
        }

        $vencimento = $this->input->post('vencimento');

        if($vencimento == null) $vencimento = date('d/m/Y');

        $data = array(
            'dtVencimento'  => $vencimento,
        );

        if ($this->financeiro_model->edit('parcela',$data,'idParcela', $this->input->post('id')) == TRUE) {
            $this->session->set_flashdata('success','Parcela editado com sucesso!');
            exit();
        } else {
            $this->session->set_flashdata('error','Ocorreu um erro ao tentar editar lançamento!');
            exit();
        }
    }

    public function pagar() {
	    $parcelaId = $this->input->post('idParcela');
	    $observacao = $this->input->post('descricao');

	    $formaPagamentos = $this->input->post('formaPgto');
        $dataPagamentos = $this->input->post('dataPagamento');
        $valorPagamentos = $this->input->post('valorPagamento');

        $this->financeiro_model->pagar($parcelaId, $formaPagamentos, $dataPagamentos, $valorPagamentos, $observacao);

        echo json_encode(array('result'=>  true));
    }

    public function pagarParcelaPorSelecao() {
        $parcelaId = $this->input->post('idParcelaPagamentoSelecao');
        $observacao = $this->input->post('observacaoPagamentoSelecao');

        $totalCreditoParcelaSelecao = $this->input->post('totalCreditoParcelaSelecao');
        $totalDebitoParcelaSelecao = $this->input->post('totalDebitoParcelaSelecao');

        $totalPagamento = $this->input->post('totalPagamentoParcelaSelecao');

        $formaPagamentos = $this->input->post('formaPgtoSelecao');
        $dataPagamentos = $this->input->post('dataPagamentoSelecao');
        $valorPagamentos = $this->input->post('valorPagamentoSelecao');

        $reciboId = $this->financeiro_model->pagarPorSelecao($parcelaId, $formaPagamentos, $dataPagamentos, $valorPagamentos, $totalCreditoParcelaSelecao,$totalDebitoParcelaSelecao ,$totalPagamento, $observacao);

        echo json_encode(array('reciboId'=>  $reciboId));

    }

    public function estornarParcela () {

	    $idParcela = $this->input->post('idParcela');
	    $vencimento =  $this->input->post('vencimento');

        if($vencimento == null) $vencimento = date('d/m/Y');

        $data = array('dtVencimento'  => $vencimento);

        $fatura = $this->financeiro_model->getFaturaByParcela($idParcela);

        $this->financeiro_model->edit('parcela',$data,'idParcela', $idParcela);
        $this->financeiro_model->edit('fatura',$data,'idFatura', $fatura->fatura_id);

        $this->financeiro_model->estornarParcela($idParcela);
	}

	public function buscarBoletosBaixa() {
	    $boletos = $this->financeiro_model->getAllBoletosAberto();

	    foreach ($boletos as $boleto) {
	        $this->analisaBoletoBaixa($boleto->transactions_integration_id);
        }
    }

	public function analisaBoletoBaixa($transaction_id) {

	    if ($transaction_id != null) {

            $jsonBoleto = json_decode($this->zoop_model->getTransactionsById($transaction_id));

            foreach ($jsonBoleto->history as $history) {

                if ($this->isBoletoPago($history)) {

                    $fees = $jsonBoleto->fees;
                    $this->baixarBoleto($transaction_id, $history->amount, $history->created_at, $fees);

                    sleep(5); // Espera 5 segundos
                }
            }
	    }
    }

    public function isBoletoPago($history) {
	    return $history->operation_type == 'paid' && $history->status == 'succeeded';
    }

    public function baixarBoleto($transaction_id, $valor, $dataPagamento, $fees=0) {
        $this->financeiro_model->baixarBoleto($transaction_id, $valor, $dataPagamento, $fees);
    }

    function gerarLinkPagamentoParcela() {

        $parcela = $this->financeiro_model->getParcelaById($this->input->post('parcelaId'));
        $lancamento =   $this->financeiro_model->getById($parcela->lancamentos_id);
        $receita =  $this->receita_model->getById($lancamento->receita_id);
        $token = $this->site_model->geraToken();

        $data = array(
            'token'             => $token,
            'url'               => base_url().'pag?token='.$token.'&cnpj='.$this->session->userdata('cnpjempresa'),
            'descricao'         => $receita->nome,
            'modo_parcelamento' => $this->input->post('modo_parcelamento'),
            'forma_pagamento'   => $this->input->post('forma_pagamento'),
            'numero_parcelas'   => $this->input->post('numero_parcelas'),
            'valor'             => $parcela->valorPagar,
            'validade'          => date('Y-m-d'),
            'status'            => 'ATIVO',
            'parcela_id'        => $this->input->post('parcelaId'),
        );

        $linkpagamentoId = $this->linkpagamento_model->add('link_pagamento', $data);

        echo json_encode( $this->linkpagamento_model->getById($linkpagamentoId));
    }

    function estornarPagamento() {

	    $primeiroPagamento = $this->financeiro_model->getPagamentoById($this->input->post('pagamentoId'));
        $primeirParcela = $this->financeiro_model->getParcelaById($this->input->post('parcelaId'));
        $lancamento = $this->financeiro_model->getById($primeirParcela->lancamentos_id);
        $parcelas = $this->financeiro_model->getParcelasByLancamento($lancamento->idLancamentos);

        $transaction_id = $primeiroPagamento->transaction_id;
        $retornoApiEstorno = null;
        $statusTransacao = null;

        if ($transaction_id != null)  $retornoApiEstorno = json_decode($this->zoop_model->estornarUmaTransacaoDeUmCartaoNaoPresente($transaction_id , $lancamento));

        if (array_key_exists('status', $retornoApiEstorno) )  $statusTransacao = $retornoApiEstorno->status;

        if ($retornoApiEstorno == null || $statusTransacao == 'canceled') {

            foreach ($parcelas as $parcela) {

                $faturaParcela = $this->financeiro_model->getFaturaByParcela($parcela->idParcela);
                $pagamento = $this->financeiro_model->getPagamentoByFatura($faturaParcela->fatura_id);

                $dadosAtualizarParcela = array(
                    'dtUltimoPagamento' => null,
                    'valorPago' => 0,
                    'valorPagar' => $parcela->valorVencimento,
                    'taxas' => 0,
                    'status' => 'FATURADA'
                );

                $dadosAtualizarFatura = array(
                    'dtUltimoPagamento' => null,
                    'valorPago' => 0,
                    'valorPagar' => $parcela->valorVencimento,
                    'status' => 'FATURADA'
                );

                $this->financeiro_model->edit('parcela', $dadosAtualizarParcela, 'idParcela', $parcela->idParcela);
                $this->financeiro_model->edit('fatura', $dadosAtualizarFatura, 'idFatura', $faturaParcela->fatura_id);
                $this->financeiro_model->edit('pagamento', array('status' => 'ESTORNADO'), 'idPagamento', $pagamento->idPagamento);
            }

            $retorno = $this->retornoDoEstornoDePagamento('Pagamento estornado com sucesso.', false);

        } else if ($retornoApiEstorno != null) {
            $retorno = $this->retornoDoEstornoDePagamento($retornoApiEstorno->error->message, true);
        } else {
            $retorno = $this->retornoDoEstornoDePagamento('Cancelamento efetuado com sucesso.', true);
        }

        echo json_encode($retorno);
    }

    public function retornoDoEstornoDePagamento($message, $error) {
        $retornoPagamentoError = array(
            'error'     => $error,
            'message'   => $message,
        );
        return $retornoPagamentoError;
    }

    public function excluirLancamentoV1(){

        $lancamentos_id = $this->input->post('id');

        $result = $this->financeiro_model->delete('lancamentos', 'idLancamentos', $lancamentos_id);

        if($result) echo json_encode(array('result'=>  true));
        else echo json_encode(array('result'=>  false));

    }

    public function cancelarParcelas() {
        $parcelasId = $this->input->post('parcelasId');
        foreach( $parcelasId as $parcelaId) {
            $this->financeiro_model->cancelarParcela($parcelaId);
        }
        echo json_encode(array('result'=>  true));
    }

    public function cancelarParcela() {
        $parcelaId = $this->input->post('idParcela');
        $this->financeiro_model->cancelarParcela($parcelaId);
        echo json_encode(array('result'=>  true));
    }

    public function excluirLancamento($idParcela){

    	$parcela = $this->financeiro_model->getParcelaById($idParcela);
        $lancamentos_id = $parcela->lancamentos_id;
    	$parcelasFatura = $this->financeiro_model->getFaturasParcelaByLancamento($parcela->idParcela);

    	foreach ($parcelasFatura as $pf) {
            $this->financeiro_model->delete('pagamento','fatura_id',$pf->fatura_id);
            $this->financeiro_model->delete('boleto','fatura_id',$pf->fatura_id);
            $this->financeiro_model->delete('fatura_parcela', 'fatura_id', $pf->fatura_id);
            $this->financeiro_model->delete('fatura','idFatura',$pf->fatura_id);
        }

        $this->financeiro_model->delete('parcela', 'idParcela', $parcela->idParcela);
    	$this->financeiro_model->delete('lancamentos', 'idLancamentos', $lancamentos_id);
    }

    function buscarPagamentos() {
        $result = $this->financeiro_model->buscarPagamentos($this->input->post('idParcela'));
        echo json_encode($result);
    }

    function buscarParcelas() {

        $valorConta = $this->input->post('valor');
        $dataPrimeiroVencimento = $this->input->post('dataPrimeiroVencimento');

        $condicao = $this->condicao_pagamento_model->getById($this->input->post('condicao'));
	    $parcelas = $condicao->parcelas;
	    $vencimento = $valorConta/$parcelas;

	    if ($dataPrimeiroVencimento !== null)  $dataVencimento = $dataPrimeiroVencimento;
	    else $dataVencimento = date('Y-m-d');

	    $html = '';
	    for ($i=0;$i<$parcelas;$i++) {
            $readonly = '';

            if ($parcelas == ($i+1) ) $readonly = 'readonly';

            $html .= '<tr>
                          <td>'.($i+1).'X</td>
                          <td><input id="parcela'.($i+1).'" onkeyup="atualizarValorVencimentoAoEditarParcela(this, 0);" '.$readonly.' vencimento="'.$vencimento.'" name="valorVencimento[]" style="line-height: 20px;text-align: right;" type="text" value="'.$vencimento.'" class="pa form-control kb-pad1 amount money" required="required"/></td>
                          <td><input required="required" class="form-control" name="vencimentoParcelas[]" type="date" value="'.$dataVencimento.'" class="span6"/></td>
                      </tr>';
            $dataVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dataVencimento)));
        }
        echo $html;
    }

	protected function getThisYear() {

        $dias = date("z");
        $primeiro = date("Y-m-d", strtotime("-".($dias)." day"));
        $ultimo = date("Y-m-d", strtotime("+".( 364 - $dias)." day"));
        return array($primeiro,$ultimo);

    }

    protected function getThisWeek(){
        return array(date("Y/m/d", strtotime("last sunday", strtotime("now"))),date("Y/m/d", strtotime("next saturday", strtotime("now"))));
    }

    protected function getLastSevenDays() {
        return array(date("Y-m-d", strtotime("-7 day", strtotime("now"))), date("Y-m-d", strtotime("now")));
    }

    protected function getThisMonth(){

        $mes = date('m');
        $ano = date('Y'); 
        $qtdDiasMes = date('t');
        $inicia = $ano."-".$mes."-01";

        $ate = $ano."-".$mes."-".$qtdDiasMes;
        return array($inicia, $ate);
    }
}

