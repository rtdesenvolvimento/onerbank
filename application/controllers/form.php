<?php

class form extends CI_Controller {

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
    }

    function index(){
        $this->data['view'] = 'form/form';
        $this->load->view('theme/topo',$this->data);
    }
}

