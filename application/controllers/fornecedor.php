<?php

class fornecedor extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }
        $this->load->helper(array('codegen_helper'));
        $this->load->model('fornecedor_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->data['menuFornecedor'] = 'fornecedor';
        $this->data['menuCadastro'] = 'Cadastro';
    }

    function index()
    {
        $this->gerenciar();
    }

    function buscar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vFornecedor')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar fornecedores.');
            //redirect(base_url());
        }
        $this->load->library('table');
        $this->load->library('pagination');


        $config['base_url'] = base_url() . 'index.php/fornecedor/gerenciar/';
        $config['total_rows'] = $this->fornecedor_model->count('fornecedor');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->data['results'] = $this->fornecedor_model->get('fornecedor', 'idFornecedor, documento, nomeFornecedor,nomeFornecedor,telefone,celular,email,rua,numero,bairro,cidade,estado,cep', '', $config['per_page'], '');
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['view'] = 'fornecedor/fornecedor';
        $this->load->view('tema/topo', $this->data);
    }

    function gerenciar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vFornecedor')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar fornecedores.');
           // redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/fornecedor/gerenciar/';
        $config['total_rows'] = $this->fornecedor_model->count('fornecedor');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->fornecedor_model->get('fornecedor', 'idFornecedor, documento, nomeFornecedor,nomeFornecedor,telefone,celular,email,rua,numero,bairro,cidade,estado,cep', '', $config['per_page'], '');
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['view'] = 'fornecedor/fornecedor';
        $this->load->view('tema/topo', $this->data);
    }

    function adicionar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aFornecedor')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para adicionar fornecedores.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('fornecedor') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeFornecedor'        => $this->input->post('nomeFornecedor'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeFornecedor' => $this->input->post('contatoNomeFornecedor'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular'),
                'dataCadastro'      => date('Y-m-d')
            );

            $idFornecedor = $this->fornecedor_model->add('fornecedor', $data, TRUE);
            if ($idFornecedor) {
                $this->session->set_flashdata('success', 'Fornecedor adicionado com sucesso!');
                redirect(base_url() . 'index.php/fornecedor/editar/'.$idFornecedor);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'fornecedor/adicionarFornecedor';
        $this->load->view('tema/topo', $this->data);

    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eFornecedor')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para editar fornecedor.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('fornecedor') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'tipoPessoa'            => $this->input->post('tipoPessoa'),
                'nomeFornecedor'        => $this->input->post('nomeFornecedor'),
                'sexo'                  => $this->input->post('sexo'),
                'nomeFantasiaApelido'   => $this->input->post('nomeFantasiaApelido'),
                'documento'             => $this->input->post('documento'),
                'rg'                    => $this->input->post('rg'),
                'orgaoEmissor'          => $this->input->post('orgaoEmissor'),
                'estadoOrgaoEmissor'    => $this->input->post('estadoOrgaoEmissor'),
                'dataOrgaoEmissor'      => $this->input->post('dataOrgaoEmissor'),
                'data_nascimento'       => $this->input->post('data_nascimento'),
                'telefone'              => $this->input->post('telefone'),
                'celular'               => $this->input->post('celular'),
                'email'                 => $this->input->post('email'),
                'rua'                   => $this->input->post('rua'),
                'numero'                => $this->input->post('numero'),
                'bairro'                => $this->input->post('bairro'),
                'cidade'                => $this->input->post('cidade'),
                'estado'                => $this->input->post('estado'),
                'cep'                   => $this->input->post('cep'),
                'complemento'           => $this->input->post('complemento'),
                'observacao'            => $this->input->post('observacao'),
                'contatoNomeFornecedor' => $this->input->post('contatoNomeFornecedor'),
                'contatoSexo'           => $this->input->post('contatoSexo'),
                'contatoCpf'            => $this->input->post('contatoCpf'),
                'contatoEmail'          => $this->input->post('contatoEmail'),
                'contatoDataNascimento' => $this->input->post('contatoDataNascimento'),
                'contatoTelefone'       => $this->input->post('contatoTelefone'),
                'contatoCelular'        => $this->input->post('contatoCelular')
            );

            if ($this->fornecedor_model->edit('fornecedor', $data, 'idFornecedor', $this->input->post('idFornecedor')) == TRUE) {
                $this->session->set_flashdata('success', 'Fornecedor editado com sucesso!');
                redirect(base_url() . 'index.php/fornecedor/editar/' . $this->input->post('idFornecedor'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }


        $this->data['result'] = $this->fornecedor_model->getById($this->uri->segment(3));
        $this->data['view'] = 'fornecedor/editarFornecedor';
        $this->load->view('tema/topo', $this->data);
    }

    public function visualizar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vFornecedor')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar fornecedores.');
            //redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->data['result'] = $this->fornecedor_model->getById($this->uri->segment(3));
        $this->data['view'] = 'fornecedor/visualizar';
        $this->load->view('tema/topo', $this->data);
    }

    public function excluir()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dFornecedor')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para excluir fornecedores.');
            //redirect(base_url());
        }

        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir fornecedor.');
            redirect(base_url() . 'index.php/fornecedor/gerenciar/');
        }



        $this->fornecedor_model->delete('fornecedor', 'idFornecedor', $id);

        $this->session->set_flashdata('success', 'Cliente excluido com sucesso!');
        redirect(base_url() . 'index.php/fornecedor/gerenciar/');
    }

}

