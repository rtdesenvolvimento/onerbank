<?php

class grupoproduto extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->data['menuCadastro'] = 'Cadastro';
        $this->data['menuGrupoProduto'] = 'Grupo de Produto';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vGrupoProduto')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar Grupo de Produtos.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/grupoproduto/gerenciar/';
        $config['total_rows'] = $this->grupoproduto_model->count('grupoproduto');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->grupoproduto_model->get('grupoproduto','idGrupoProduto,nome,descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'grupoproduto/grupoproduto';
        $this->load->view('theme/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aGrupoProduto')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar grupo de produtos.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('grupoproduto') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao')
            );

            if ($this->grupoproduto_model->add('grupoproduto', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Grupo de Produto adicionado com sucesso!');
                redirect(base_url() . 'index.php/grupoproduto/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'grupoproduto/adicionarGrupoProduto';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eGrupoProduto')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar grupo de produtos.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('grupoproduto') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->grupoproduto_model->edit('grupoproduto', $data, 'idGrupoProduto', $this->input->post('idGrupoProduto')) == TRUE) {
                $this->session->set_flashdata('success', 'Aparelho editado com sucesso!');
                redirect(base_url() . 'index.php/grupoproduto/editar/'.$this->input->post('idGrupoProduto'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->grupoproduto_model->getById($this->uri->segment(3));
        $this->data['view'] = 'grupoproduto/editarGrupoProduto';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dGrupoProduto')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir grupo de produtos.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir grupo de produto.');
            redirect(base_url().'index.php/grupoproduto/gerenciar/');
        }

        $this->grupoproduto_model->delete('grupoproduto','idGrupoProduto',$id);

        $this->session->set_flashdata('success','Grupo de Produto excluido com sucesso!');
        redirect(base_url().'index.php/grupoproduto/gerenciar/');
    }
}

