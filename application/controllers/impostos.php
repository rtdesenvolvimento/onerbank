<?php

class impostos extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->model('ncm_model', '', TRUE);
        $this->load->model('cest_model', '', TRUE);
        $this->load->model('cfop_model', '', TRUE);

        $this->load->helper(array('form', 'codegen_helper'));
    }

    function getICMS() {
        $CST = $this->input->post('CST');
        $loadView = 'icms_'.$CST.'_form';

        $this->data['view']     = 'impostos/icms/'.$loadView;
        $this->load->view('tema/blank', $this->data);
    }

    function getIPI() {
        $CSTIPI     = $this->input->post('CSTIPI');
        $loadView   = 'ipi_'.$CSTIPI.'_form';

        $this->data['view']     = 'impostos/ipi/'.$loadView;
        $this->load->view('tema/blank', $this->data);
    }

    function getPIS01() {
        $this->data['view']     = 'impostos/pis/pis_01_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getPIS03() {
        $this->data['view']     = 'impostos/pis/pis_03_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getPIS05() {
        $this->data['view']     = 'impostos/pis/pis_05_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getPIS49() {
        $this->data['view']     = 'impostos/pis/pis_49_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getCOFINS01() {
        $this->data['view']     = 'impostos/cofins/cofins_01_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getCOFINS03() {
        $this->data['view']     = 'impostos/cofins/cofins_03_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getCOFINS05() {
        $this->data['view']     = 'impostos/cofins/cofins_05_form';
        $this->load->view('tema/blank', $this->data);
    }

    function getCOFINS49() {
        $this->data['view']     = 'impostos/cofins/cofins_49_form';
        $this->load->view('tema/blank', $this->data);
    }

    function searchNCM() {


        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $termo = $this->input->post('termo');;

        $config['base_url'] = base_url() . 'index.php/impostos/searchNCM/';
        $config['total_rows'] = $this->ncm_model->count('ncm');

        if (!$termo) {
            $config['per_page'] = 20;
        } else {
            $config['per_page'] = 10000;
        }

        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->ncm_model->search($termo, $config['per_page'], $this->uri->segment(3));
        $this->data['termo']    = $termo;
        $this->data['view']     = 'impostos/ncm/searchNCM';
        $this->load->view('tema/blank', $this->data);
    }

    function searchCEST() {


        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $termo = $this->input->post('termo');;

        $config['base_url'] = base_url() . 'index.php/impostos/searchCEST/';
        $config['total_rows'] = $this->cest_model->count('tabelacest');

        if (!$termo) {
            $config['per_page'] = 20;
        } else {
            $config['per_page'] = 10000;
        }

        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->cest_model->search($termo, $config['per_page'], $this->uri->segment(3));
        $this->data['termo']    = $termo;
        $this->data['view']     = 'impostos/cest/searchCEST';
        $this->load->view('tema/blank', $this->data);
    }

    function searchCFOP() {


        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $termo = $this->input->post('termo');;

        $config['base_url'] = base_url() . 'index.php/impostos/searchCFOP/';
        $config['total_rows'] = $this->cfop_model->count('cfop');

        if (!$termo) {
            $config['per_page'] = 20;
        } else {
            $config['per_page'] = 10000;
        }

        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->cfop_model->search($termo, $config['per_page'], $this->uri->segment(3));
        $this->data['termo']    = $termo;
        $this->data['view']     = 'impostos/cfop/searchCFOP';
        $this->load->view('tema/blank', $this->data);
    }
}

