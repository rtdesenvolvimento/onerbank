<?php
error_reporting(E_ALL ^ E_DEPRECATED);

class inventario extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('inventario_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('filial_model', '', TRUE);
        $this->load->model('historico_estoque_model', '', TRUE);
        $this->load->model('estoque_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);

        $this->data['menuInventario'] = 'Inventário de compra';
        $this->data['menuEstoque'] = 'Estoque';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar inventário.');
            redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/inventario/gerenciar/';
        $config['total_rows'] = $this->inventario_model->count('inventario');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->inventario_model->get('inventario', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'inventario/inventario';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar inventário.');
             redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('inventario') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $dataInventario = $this->input->post('dataInventario');

            $data = array(
                'dataInventario'    => $dataInventario,
                'usuarios_id'       => $this->input->post('usuarios_id')
            );

            if (is_numeric($id = $this->inventario_model->add('inventario', $data, true))) {
                $this->session->set_flashdata('success', 'Inventário iniciada com sucesso, adicione os produtos.');
                redirect('inventario/editar/' . $id);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['origem']       = $this->filial_model->getFilialCurrent();
        $this->data['view']         = 'inventario/adicionarInventario';
        $this->load->view('theme/topo', $this->data);
    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar inventário');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('inventario') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $dataInventario             = $this->input->post('dataInventario');

            $data = array(
                'numeroLivro' => $this->input->post('numeroLivro'),
                'usuarios_id' => $this->input->post('usuarios_id'),
                'liberarContador' => $this->input->post('liberarContador'),
                'zerarEstoqueProdutoForaInventario' => $this->input->post('zerarEstoqueProdutoForaInventario'),
                'dataInventario' => $dataInventario,
            );

            if ($this->inventario_model->edit('inventario', $data, 'idInventario', $this->input->post('idInventario')) == TRUE) {
                $this->session->set_flashdata('success', 'Invenatário editado com sucesso!');
                redirect(base_url() . 'index.php/inventario/editar/' . $this->input->post('idInventario'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['origem']               = $this->filial_model->getFilialCurrent();
        $this->data['usuarios']             = $this->usuarios_model->getAll();
        $this->data['produtosList']         = $this->produtos_model->getAll();
        $this->data['result']               = $this->inventario_model->getById($this->uri->segment(3));
        $this->data['produtos']             = $this->inventario_model->getProdutos($this->uri->segment(3));
        $this->data['view']                 = 'inventario/editarInventario';
        $this->load->view('theme/topo', $this->data);

    }

    public function relatorioEstoqueHtmlHeader($data) {
        $strHora = $this->site_model->dataDeHojePorExtensoRetorno( strtotime($data) );
        $html = '
                    <br/><br/><br/>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%;"></td>
                            <td style="width: 50%;text-align: right;">
                                Estoque existente em: '.$strHora.'<br/>
                                * * * * * Registro de Inventário * * * * *
                            </td>
                        </tr>
                    </table>
                    
                    <table style="width: 100%;font-size: 12px;">
                        <thead>
                        <tr>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Código</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">Descrição do Artigo</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Quantidade</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">Unidade</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Unitário</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Total</th>
                        </tr>
                        </thead>
                        <tbody>';

        return $html;
    }

    public function relatorioEstoqueHtmlFooter() {
        $html = '</table>';

        return $html;
    }

    public function relatorioEstoqueTableHtmlFooter($total) {
        $html = '<tfoot>
                <tr>
                    <td colspan="6" style="border-top: 1px solid #2c2f3b;text-align: right;">Valor. . . . . . . . . . . . : '.$this->site_model->formatarValorMonetario($total).'</td>
                </tr>
                </tfoot> ';

        return $html;
    }

    public function relatorioEstoqueSubTotalTableHtmlFooter($total) {
        $html = '<tfoot>
                <tr>
                    <td colspan="6" style="text-align: right;">Total. . . . . . . . . . . . : '.$this->site_model->formatarValorMonetario($total).'</td>
                </tr>
                </tfoot> ';

        return $html;
    }

    public function relatorioEstoque() {

        $this->load->helper('mpdf');

        $filial = $this->db->get_where('filial', array('idFilial'=>$this->session->userdata('filial_id')))->row();
        $emitente = $this->db->get_where('emitente', array('filial_id'=>$this->session->userdata('filial_id')))->row();

        $stream = TRUE;
        $orientation='A4';
        $filename = 'relatorio_inventario';
        $client = $filial->nome.' CNPJ: '.$emitente->cnpj.' I.E.: '.$emitente->ie;

        define('_MPDF_PATH','application/lib/mpdf/');
        require('application/lib/mpdf/mpdf.php');

        $pdf_c = 'c';

        $mpdf=new mPDF($pdf_c,$orientation,'','',5,5,5,5,9,9);
        $mpdf->debug = false;
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;

        $mpdf->SetProtection(array('copy','print'), '', '123456');
        $mpdf->SetTitle($client);
        $mpdf->SetAuthor($client);
        $mpdf->SetCreator($client);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetAutoPageBreak(TRUE);

        $produtos = $this->inventario_model->getProdutos($this->uri->segment(3));
        $result = $this->inventario_model->getById($this->uri->segment(3));

        $mpdf->SetHeader($client.'|| Livro No. '.$result->numeroLivro.' Folha: {PAGENO}', '', TRUE); // For simple text header

        $subTotal = 0;
        $total = 0;
        $contadorItens = 1;
        $contadorTotal = 0;
        $exibirHeader = true;
        $QUANTIDADE_ITENS_EXIBIR = 47;

        $html = '';

        foreach ($produtos as $produto) {

            $estoque = $this->produtos_model->getProdutoFilialById($produto->produtos_id);

            $precoDeCompra = $estoque->precoCompra;
            $estoqueContado = $produto->quantidade;

            $valorEmEstoque = $precoDeCompra * $estoqueContado;

            $total = $total + $valorEmEstoque;
            $subTotal = $subTotal + $valorEmEstoque;

            if ($exibirHeader) {
                $html .= $this->relatorioEstoqueHtmlHeader($result->dataInventario);

                $mpdf->WriteHTML($this->relatorioEstoqueHtmlHeader($result->dataInventario));
                $exibirHeader = false;
            }

            $htmlProdutos = ' 
                    <tr>
                        <td style="text-align: center;">'.$produto->cProd.'</td>
                        <td style="text-align: left;">'.$produto->descricao.'</td>
                        <td style="text-align: right;">'.$this->site_model->formatarValorMonetario($estoqueContado, 3).'</td>
                        <td style="text-align: left;">'.$produto->unidade.'</td>
                        <td style="text-align: right;">'.$this->site_model->formatarValorMonetario($precoDeCompra, 6).'</td>
                        <td style="text-align: right;">'.$this->site_model->formatarValorMonetario($precoDeCompra* $estoqueContado).'</td>
                    </tr>';

            $html .= $htmlProdutos;

            $mpdf->WriteHTML($htmlProdutos);

            if ($contadorItens == $QUANTIDADE_ITENS_EXIBIR || $contadorTotal == count($produtos)-1) {

                $html .= $this->relatorioEstoqueTableHtmlFooter($total);

                $mpdf->WriteHTML($this->relatorioEstoqueTableHtmlFooter($total));

                if ($contadorTotal == count($produtos)-1) {

                    $html .= $this->relatorioEstoqueSubTotalTableHtmlFooter($subTotal);
                    $mpdf->WriteHTML($this->relatorioEstoqueSubTotalTableHtmlFooter($subTotal));
                }

                $html .= $this->relatorioEstoqueHtmlFooter();

                $mpdf->WriteHTML($this->relatorioEstoqueHtmlFooter());

                if ($contadorTotal < count($produtos)-1) {
                    $mpdf->SetFooter('{PAGENO}/{nbpg}', '', TRUE); // For simple text header
                    $mpdf->AddPage();
                }

                $contadorItens = 1;
                $total = 0;
                $exibirHeader = true;
            }

            $contadorItens++;
            $contadorTotal++;
        }

        $mpdf->SetFooter('{PAGENO}/{nbpg}', '', TRUE); // For simple text header

        if ($stream) {
            $mpdf->Output($filename . '.pdf', 'D');
        } else  {
            $mpdf->Output('assets/uploads/' . $filename . '.pdf', 'F');
            return 'assets/uploads/' . $filename . '.pdf';
        }

    }

    public function gerarInventarioEstoqueAtual() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar invantário.');
            redirect(base_url());
        }

        $inventarioId    =  $this->uri->segment(3);

        $inventario = $this->inventario_model->getById($this->uri->segment(3));

        $this->deleteAllItensInventario($inventarioId);

        $allProducts = $this->produtos_model->getProdutosFiliais();

        foreach ($allProducts as $produto) {

            if ($produto->filial_id == $inventario->filial_id) {

                $quantidade = $produto->estoque;
                $produtoId = $produto->produto_id;
                $custo = $produto->precoCompra;
                $venda = $produto->precoVenda;
                $tipo = 'SUBSTITUIR';

                $data_product = array(
                    'quantidade' => $quantidade,
                    'produtos_id' => $produtoId,
                    'custo' => $custo,
                    'venda' => $venda,
                    'tipo' => $tipo,
                    'inventario_id' => $inventarioId,
                );

                $this->inventario_model->add('itensInventario', $data_product);
            }
        }

        $this->session->set_flashdata('success', 'Itens adicionados ao inventário!');
        redirect(base_url() . 'index.php/inventario/editar/'.$inventarioId);

    }

    public function editarObservacao() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar invantário.');
            redirect(base_url());
        }

        $idInventario   = $this->input->post('idInventario');
        $observacao = $this->input->post('observacao');

        $data = array(
            'observacao' => $observacao,
        );

        if ($this->inventario_model->edit('inventario', $data, 'idInventario',$idInventario) == TRUE) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function confirmar() {

        $InventarioId    =  $this->uri->segment(3);
        $itens           = $this->inventario_model->getProdutos($InventarioId);
        $result          = $this->inventario_model->getById($this->uri->segment(3));

        if (count($itens) === 0)  {
            $this->session->set_flashdata('success', 'Adicione pelo menos 1 produto!');
            redirect('inventario/editar/'.$InventarioId);
        }

        foreach ($itens as $r) {

            $idItens            = $r->idItens;
            $quantidade         = $r->quantidade;
            $produto            = $r->produtos_id;
            $filial             = $r->filial_id;
            $inventario_id      = $r->inventario_id;
            $custo              = $r->custo;
            $venda              = $r->venda;

            if ($custo > 0 ) $this->estoque_model->alterarPrecoCompra($custo, $produto, $filial);
            if ($venda > 0) $this->estoque_model->alterarPrecoVenda($venda, $produto, $filial);

            $tipo = $r->tipo;

            if ('ENTRADA' === $tipo) {
                $this->estoque_model->entradaEstoque($quantidade, $produto, 'Inventário', $inventario_id, $idItens, $filial);
            } else if ('SAIDA' === $tipo) {
                $this->estoque_model->saidaEstoque($quantidade, $produto, 'Inventário', $inventario_id, $idItens, $filial);
            } else if('SUBSTITUIR' === $tipo) {

                $produtoFilial = $this->produtos_model->getProdutoFilialById($produto);
                $quantidadeEstoqueAtual = $produtoFilial->estoque;

                if ($quantidadeEstoqueAtual > $quantidade) {
                    $quantidade = $quantidadeEstoqueAtual - $quantidade;
                    $this->estoque_model->saidaEstoque($quantidade, $produto, 'Inventário', $inventario_id, $idItens, $filial);
                } else {
                    $quantidade = $quantidade - $quantidadeEstoqueAtual;
                    $this->estoque_model->entradaEstoque($quantidade, $produto, 'Inventário', $inventario_id, $idItens, $filial);
                }

            }
        }

        $this->zerarEstoqueProdutosForaDoInventario($itens, $result);

        $this->inventario_model->edit('inventario', array('status' => 'Confirmado'), 'idInventario', $InventarioId);
        $this->session->set_flashdata('success', 'Inventário entregue com sucesso!');

        redirect(base_url() . 'index.php/inventario/editar/'.$InventarioId);
    }

    public function zerarEstoqueProdutosForaDoInventario($itens, $result) {

        if ($result->zerarEstoqueProdutoForaInventario == 'S') {
            $allProducts = $this->produtos_model->getAll();

            foreach ($allProducts as $produto) {

                if (!$this->verificarSeProdutoSeEncontraNoInventario($itens, $produto)) {
                    $estoqueAtual = $produto->estoque;
                    $filial = $produto->filial_id;
                    $produto_id = $produto->idProdutos;

                    if ($estoqueAtual > 0) $this->estoque_model->saidaEstoque($estoqueAtual, $produto_id, 'Inventário', null, null, $filial);
                    else $this->estoque_model->entradaEstoque($estoqueAtual * -1, $produto_id, 'Inventário', null, null, $filial);
                }
            }
        }
    }

    public function verificarSeProdutoSeEncontraNoInventario($itens, $produto) {
        $isEncontrouProdutoNoInventario = false;

        foreach ($itens as $item) {
            $produtoInventario_id = $item->produtos_id;

            if ($produtoInventario_id == $produto->idProdutos) $isEncontrouProdutoNoInventario = true;
        }

        return $isEncontrouProdutoNoInventario;
    }

    public function visualizar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar inventário.');
            //redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->inventario_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->inventario_model->getProdutos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();

        $this->data['view'] = 'inventario/visualizarInventario';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para excluir inventário');
            //redirect(base_url());
        }

        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir inventário.');
            redirect(base_url() . 'index.php/inventario/gerenciar/');
        }

        $this->deleteAllItensInventario($id);

        $this->db->where('idInventario', $id);
        $this->db->delete('inventario');

        $this->session->set_flashdata('success', 'Inventário excluído com sucesso!');
        redirect(base_url() . 'index.php/inventario/gerenciar/');
    }

    private function deleteAllItensInventario($InventarioId) {
        $this->db->where('inventario_id', $InventarioId);
        $this->db->delete('itensInventario');
    }

    public function adicionarProduto()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar Inventário.');
            // redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idProduto', 'Produto', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idInventario', 'Inventario', 'trim|required|xss_clean');

        if ($this->form_validation->run() == false) {
            echo json_encode(array('result' => false));
        } else {

            $quantidade = $this->input->post('quantidade');
            $produto    = $this->input->post('product_id');
            $custo      = $this->input->post('custo');
            $venda      = $this->input->post('venda');
            $tipo       = $this->input->post('tipo');

            $data = array(
                'quantidade' => $quantidade,
                'produtos_id' => $produto,
                'custo' => $custo,
                'venda' => $venda,
                'tipo' => $tipo,
                'inventario_id' => $this->input->post('idInventario'),
            );

            if ($this->inventario_model->add('itensInventario', $data) == true) echo json_encode(array('result' => true));
            else echo json_encode(array('result' => false));
        }

    }

    function excluirProduto()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eInventario')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar inventário');
            //redirect(base_url());
        }

        $ID = $this->input->post('idProduto');
        if ($this->inventario_model->delete('itensInventario', 'idItens', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
}

