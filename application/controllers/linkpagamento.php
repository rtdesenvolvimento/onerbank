<?php

class linkpagamento extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('linkpagamento_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);

        $this->data['menuLinkDePagamento'] = 'Link de pagamento';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/linkpagamento/gerenciar/';
        $config['total_rows'] = $this->linkpagamento_model->count('link_pagamento');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->linkpagamento_model->get('link_pagamento','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'linkpagamento/linkpagamento';
        $this->load->view('theme/topo',$this->data);
    }

    function adicionar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('linkpagamento') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $token = $this->site_model->geraToken();

            $data = array(
                'token'             => $token,
                'url'               => base_url().'pag?token='.$token.'&cnpj='.$this->session->userdata('cnpjempresa'),
                'descricao'         => $this->input->post('descricao'),
                'modo_parcelamento' => $this->input->post('modo_parcelamento'),
                'status'            => $this->input->post('status'),
                'forma_pagamento'   => $this->input->post('forma_pagamento'),
                'valor'             => $this->input->post('valor'),
                'validade'          => $this->input->post('validade'),
                'numero_parcelas'   => $this->input->post('numero_parcelas'),
            );

            if ($this->linkpagamento_model->add('link_pagamento', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Link de pagamento adicionado com sucesso!');
                redirect(base_url() . 'index.php/linkpagamento/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['view'] = 'linkpagamento/adicionarLinkPagamento';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('linkpagamento') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'descricao'         => $this->input->post('descricao'),
                'status'            => $this->input->post('status'),
                'modo_parcelamento' => $this->input->post('modo_parcelamento'),
                'forma_pagamento'   => $this->input->post('forma_pagamento'),
                'valor'             => $this->input->post('valor'),
                'validade'          => $this->input->post('validade'),
                'numero_parcelas'   => $this->input->post('numero_parcelas'),
            );

            if ($this->linkpagamento_model->edit('link_pagamento', $data, 'idLinkPagamento', $this->input->post('idLinkPagamento')) == TRUE) {
                $this->session->set_flashdata('success', 'Link de pagamento editado com sucesso!');
                redirect(base_url() . 'index.php/linkpagamento/editar/'.$this->input->post('idLinkPagamento'));
            } else {
                $this->data['cusSatom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->linkpagamento_model->getById($this->uri->segment(3));
        $this->data['view'] = 'linkpagamento/editarLinkPagamento';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){

        $id =  $this->input->post('id');

        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir tipo de avaria.');
            redirect(base_url().'index.php/linkpagamento/gerenciar/');
        }

        $this->linkpagamento_model->delete('link_pagamento','idLinkPagamento',$id);
        $this->session->set_flashdata('success','Tipo de avaria excluido com sucesso!');

        redirect(base_url().'index.php/linkpagamento/gerenciar/');
    }
}

