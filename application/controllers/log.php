<?php

class log extends CI_Controller {

    function __construct() {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->model('setor_model', '', TRUE);
        $this->load->model('zoop_model', '', TRUE);

    }

    function index(){

        $dataArquivo = $this->input->post('data-arquivo');
        $arquivo = $this->input->post('arquivo');
        $leitura = '';
        $emitente = $this->zoop_model->getEmitente();

        if ($dataArquivo == null) $dataArquivo = date('Y-m-d');

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/setor/gerenciar/';
        $config['total_rows'] = $this->setor_model->count('setor');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Ultima';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($arquivo != null) {
            $destino = './assets/log/'.$emitente->cnpj.'/'.$dataArquivo.'/'.$arquivo;;
            $handle = fopen($destino, 'r');
            while($line = fgets($handle)) $leitura .= $line;
        }

        $this->data['emitente'] = $emitente;
        $this->data['arquivo'] = $arquivo;
        $this->data['leitura'] = $leitura;
        $this->data['data'] = $dataArquivo;

        $this->data['view'] = 'log/log';
        $this->load->view('theme/topo',$this->data);
    }
}

