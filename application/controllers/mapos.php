<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mapos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mapos_model', '', '');
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('configuracao_model', '', TRUE);
        $this->load->model('configuracaotecnospeed_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);
    }

    public function index()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->data['emitente'] = $this->mapos_model->getEmitente();

        $this->data['ordens']   = $this->mapos_model->getOsAbertas();
        $this->data['produtos'] = $this->mapos_model->getProdutosMinimo();
        $this->data['os']       = $this->mapos_model->getOsEstatisticas();
        $this->data['vistoria'] = $this->mapos_model->getVistoriaEstatisticas();

        $this->data['estatisticas_financeiro'] = $this->mapos_model->getEstatisticasFinanceiro();

        $this->data['contasReceberHoje'] = $this->mapos_model->contasReceberHoje();
        $this->data['contasReceberSemana'] = $this->mapos_model->contasReceberSemana();
        $this->data['contasReceberMes'] = $this->mapos_model->contasReceberMes();

        $this->data['contasPagarHoje'] = $this->mapos_model->contasPagarHoje();
        $this->data['contasPagarSemana'] = $this->mapos_model->contasPagarSemana();
        $this->data['contasPagarMes']   = $this->mapos_model->contasPagarMes();

        $this->data['contasReceberVencidas'] = $this->mapos_model->contasReceberVencidasMes();
        $this->data['contasPagarVencidas'] = $this->mapos_model->contasPagarVencidasMes();


        $this->data['qtdBoletosGeradosMes'] = $this->mapos_model->getBoletosGeradosNoMes();
        $this->data['qtdBoletosPagosMes'] = $this->mapos_model->getBoletosPagosNoMes();
        $this->data['qtdBoletosVencidosMes'] = $this->mapos_model->getBoletosVencidosNoMes();


        $this->data['menuPainel'] = 'Painel';
        $this->data['view'] = 'mapos/painel';
        $this->load->view('theme/topo', $this->data);
    }

    public function cadClienteOnline()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) redirect('mapos/login');

        redirect('clientes/cadPessoaOnlineIntegracao');
    }

    public function onerApp() {
        $this->data['menuPainel'] = 'Painel';
        $this->data['view'] = 'mapos/onerapp';
        $this->load->view('tema/topoonerapp', $this->data);
    }

    public function minhaConta()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->data['usuario'] = $this->mapos_model->getById($this->session->userdata('id'));
        $this->data['view'] = 'mapos/minhaConta';
        $this->load->view('tema/topo', $this->data);

    }

    public function alterarFilialSession() {
        $this->load->library('session');
        $this->session->sess_destroy();
        $dados = $this->session->all_userdata();
        $this->session->set_userdata($dados);
        print_r($this->session->all_userdata());
        die();
    }

    public function alterarSenha()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $oldSenha   = $this->input->post('oldSenha');
        $senha      = $this->input->post('novaSenha');
        $result     = $this->mapos_model->alterarSenha($senha, $oldSenha, $this->session->userdata('id'));

        if ($result) {
            $this->session->set_flashdata('success', 'Senha Alterada com sucesso!');
            redirect(base_url() . 'index.php/mapos/minhaConta');
        } else {
            $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar alterar a senha!');
            redirect(base_url() . 'index.php/mapos/minhaConta');

        }
    }

    public function pesquisar()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $termo                  = $this->input->get('termo');

        $data['results']        = $this->mapos_model->pesquisar($termo);

        $this->data['veiculos'] = $data['results']['veiculos'];
        $this->data['produtos'] = $data['results']['produtos'];
        $this->data['servicos'] = $data['results']['servicos'];
        $this->data['os']       = $data['results']['os'];
        $this->data['vistoria'] = $data['results']['vistoria'];
        $this->data['clientes'] = $data['results']['clientes'];
        $this->data['termo']    = $termo;

        $this->data['view']     = 'mapos/pesquisa';
        $this->load->view('tema/topo', $this->data);
    }

    public function login()
    {
        $this->load->view('mapos/login');
    }

    public function sair()
    {
        $cnpjempresa = $this->session->userdata('cnpjempresa');
        $this->session->sess_destroy();
        redirect('mapos/login/'.$cnpjempresa);
    }

    public function atribuirCNPJEmpresa() {
        $cnpjempresa    = $this->input->post('cnpjempresa');

        $file_path = APPPATH.'config/clientes/'.$cnpjempresa.'.txt';

        if ( ! file_exists($file_path))
        {
            $this->session->set_flashdata('error', 'Não foi possível identificar a empresa.');
            $json = array('result' => false);
            echo json_encode($json);
            exit();
        }

        $json = array('result' => true);
        echo json_encode($json);
    }

    public function verificarLogin() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('usuariologin', 'Usuário', 'required|xss_clean|trim');
        $this->form_validation->set_rules('senha', 'Senha', 'required|xss_clean|trim');
        $ajax = $this->input->get('ajax');

        if ($this->form_validation->run() == false) {
            if ($ajax == true) {
                $json = array('result' => false);
                echo json_encode($json);
            } else {
                $this->session->set_flashdata('error', 'Os dados de acesso estão incorretos.');
                redirect($this->login);
            }
        } else {

            $usuariologin   = $this->input->post('usuariologin');
            $senha          = $this->input->post('senha');
            $cnpjempresa    = $this->input->post('cnpjempresa');
            $filial         = $this->input->post('filial');

            if ($filial == '') {
                $this->session->set_flashdata('error', 'Não foi possível identificar a empresa.');
                $json = array('result' => false);
                echo json_encode($json);
                exit();
            }

            $this->load->library('encrypt');
            $senha = $this->encrypt->sha1($senha);

            $otherdb = $this->load->database($cnpjempresa, TRUE);

            $otherdb->where('usuariologin', $usuariologin);
            $otherdb->where('senha', $senha);
            $otherdb->where('situacao', 1);
            $otherdb->limit(1);
            $usuario = $otherdb->get('usuarios')->row();

            if (count($usuario) > 0) {

                $dados = array(
                    'nome'          => $usuario->nome,
                    'id'            => $usuario->idUsuarios,
                    'setor_id'      => $usuario->setor_id,
                    'permissao'     => $usuario->permissoes_id,
                    'filial_id'     => $filial,
                    'cnpjempresa'   => $cnpjempresa,
                    'logado'        => TRUE);

                if (!isset($_SESSION)):
                    session_start();
                endif;

                $this->session->set_userdata($dados);
                $_SESSION['BD'] = $cnpjempresa;

                if ($ajax == true) {
                    $json = array('result' => true, 'setor_id' => $usuario->setor_id);
                    echo json_encode($json);
                } else {
                    redirect(base_url() . 'mapos');
                }
            } else {
                if ($ajax == true) {
                    $json = array('result' => false);
                    echo json_encode($json);
                } else {
                    $this->session->set_flashdata('error', 'Os dados de acesso estão incorretos.');
                    redirect(base_url() . 'mapos/login/'.$this->input->post('cnpjempresa') );
                }
            }
        }
    }

    public function  buscar_filial($email = '', $cnpjempresa = '') {

        $email = str_replace("arroba","@",$email);
        $otherdb = $this->load->database($cnpjempresa, TRUE);

        $usuarios = $otherdb->get_where('usuarios' , array(
            'usuariologin' => $email
        ))->result_array();

        if (count($usuarios) > 0) {
            foreach ($usuarios as $row) {

                echo '  <select class="combostyle" name="filial" required="required" style="color: #aaa;width: 100%;" id="filial">';
                            $this->get_filiais($row['idUsuarios'], $cnpjempresa);
                echo '  </select>';
            }
        } else {
            echo '
                <select class="combostyle" name="filial" required="required" style="color: #aaa;width: 100%;" id="filial">
                    <option value="">Selecione uma filial</option>
                </select>';
        }
        return;
    }

    function get_filiais($client_id = null,$cnpjempresa='')
    {
        $otherdb = $this->load->database($cnpjempresa, TRUE);
        $filiais = $otherdb->get_where('usuarios_filial' , array(
            'usuario_id' => $client_id,
            'status' => 1,
        ))->result_array();

        foreach ($filiais as $row) {
            $filial = $otherdb->get_where('filial' , array('idFilial' => $row['filial'] ))->row();
            $nome = $filial->nome;
            $status = $filial->status;

            if ($status == 1) {
                echo '<option value="' . $row['filial'] . '">' . $nome . '</option>';
            }
        }
    }

    public function backup()
    {

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cBackup')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para efetuar backup.');
            redirect(base_url());
        }

        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip',
            'filename' => 'backup' . date('d-m-Y') . '.sql'
        );

        $backup =& $this->dbutil->backup($prefs);

        $this->load->helper('file');
        write_file(base_url() . 'backup/backup.zip', $backup);

        $this->load->helper('download');
        force_download('backup' . date('d-m-Y H:m:s') . '.zip', $backup);
    }

    public function emitente()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para configurar emitente.');
            redirect(base_url());
        }

        $data['menuConfiguracoes'] = 'Configuracoes';

        $data['configuracoes'] = $this->configuracao_model->getAll();
        $data['configuracoesTecnnoSpeed'] = $this->configuracaotecnospeed_model->getAll();
        $data['dados'] = $this->mapos_model->getEmitente();
        $data['view'] = 'mapos/emitente';

        $this->load->view('theme/topo', $data);
    }

    function do_upload()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para configurar emitente.');
            redirect(base_url());
        }

        $this->load->library('upload');
        $image_upload_folder = FCPATH . 'assets/uploads';

        if (!file_exists($image_upload_folder)) {
            mkdir($image_upload_folder, DIR_WRITE_MODE, true);
        }

        $this->upload_config = array(
            'upload_path' => $image_upload_folder,
            'allowed_types' => 'png|jpg|jpeg|bmp',
            'max_size' => 2048,
            'remove_space' => TRUE,
            'encrypt_name' => TRUE,
        );

        $this->upload->initialize($this->upload_config);

        if (!$this->upload->do_upload()) {
            $upload_error = $this->upload->display_errors();
            print_r($upload_error);
            exit();
        } else {
            $file_info = array($this->upload->data());
            return $file_info[0]['file_name'];
        }
    }

    public function cadastrarEmitente()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('index.php/mapos/login');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para configurar emitente.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome', 'Razão Social', 'required|xss_clean|trim');
        $this->form_validation->set_rules('cnpj', 'CNPJ', 'required|xss_clean|trim');
        $this->form_validation->set_rules('ie', 'IE', 'required|xss_clean|trim');
        $this->form_validation->set_rules('logradouro', 'Logradouro', 'required|xss_clean|trim');
        $this->form_validation->set_rules('numero', 'Número', 'required|xss_clean|trim');
        $this->form_validation->set_rules('bairro', 'Bairro', 'required|xss_clean|trim');
        $this->form_validation->set_rules('cidade', 'Cidade', 'required|xss_clean|trim');
        $this->form_validation->set_rules('uf', 'UF', 'required|xss_clean|trim');
        $this->form_validation->set_rules('telefone', 'Telefone', 'required|xss_clean|trim');
        $this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean|trim');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Campos obrigatórios não foram preenchidos.');
            redirect(base_url() . 'index.php/mapos/emitente');

        } else {

            $nome = $this->input->post('nome');
            $cnpj = $this->input->post('cnpj');
            $ie = $this->input->post('ie');
            $logradouro = $this->input->post('logradouro');
            $numero = $this->input->post('numero');
            $bairro = $this->input->post('bairro');
            $cidade = $this->input->post('cidade');
            $uf = $this->input->post('uf');
            $telefone = $this->input->post('telefone');
            $ramo_atividade = $this->input->post('ramo_atividade');
            $email = $this->input->post('email');
            $image = $this->do_upload();
            $logo = base_url() . 'assets/uploads/' . $image;

            $retorno = $this->mapos_model->addEmitente($nome, $cnpj, $ie, $logradouro, $numero, $bairro, $cidade, $uf, $telefone, $ramo_atividade, $email, $logo);

            if ($retorno) {
                $this->session->set_flashdata('success', 'As informações foram inseridas com sucesso.');
                redirect(base_url() . 'index.php/mapos/emitente');
            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar inserir as informações.');
                redirect(base_url() . 'index.php/mapos/emitente');
            }
        }
    }

    public function editarEmitente()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('index.php/mapos/login');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para configurar emitente.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome', 'Razão Social', 'required|xss_clean|trim');
        $this->form_validation->set_rules('cnpj', 'CNPJ', 'required|xss_clean|trim');
        $this->form_validation->set_rules('ie', 'IE', 'required|xss_clean|trim');
        $this->form_validation->set_rules('logradouro', 'Logradouro', 'required|xss_clean|trim');
        $this->form_validation->set_rules('numero', 'Número', 'required|xss_clean|trim');
        $this->form_validation->set_rules('bairro', 'Bairro', 'required|xss_clean|trim');
        $this->form_validation->set_rules('cidade', 'Cidade', 'required|xss_clean|trim');
        $this->form_validation->set_rules('uf', 'UF', 'required|xss_clean|trim');
        $this->form_validation->set_rules('telefone', 'Telefone', 'required|xss_clean|trim');
        $this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean|trim');

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('error', 'Campos obrigatórios não foram preenchidos.');
            redirect(base_url() . 'index.php/mapos/emitente');

        } else {

            $nome           = $this->input->post('nome');
            $cnpj           = $this->input->post('cnpj');
            $ie             = $this->input->post('ie');
            $logradouro     = $this->input->post('logradouro');
            $numero         = $this->input->post('numero');
            $bairro         = $this->input->post('bairro');
            $cidade         = $this->input->post('cidade');
            $uf             = $this->input->post('uf');
            $telefone       = $this->input->post('telefone');
            $email          = $this->input->post('email');
            $id             = $this->input->post('id');
            $ramo_atividade = $this->input->post('ramo_atividade');
            $certsenha      =  $this->input->post('certsenha');

            $this->mapos_model->criacaoPastasArquivos($this->input->post('cnpj'), $_FILES);
            $cert   = $this->mapos_model->gravarCertificado($this->input->post('cnpj'), $_FILES);

            $retorno = $this->mapos_model->editEmitente($id, $nome, $cnpj, $ie, $logradouro, $numero, $bairro, $cidade, $uf, $telefone, $ramo_atividade, $email, $cert, $certsenha);
            if ($retorno) {
                $this->session->set_flashdata('success', 'As informações foram alteradas com sucesso.');
                redirect(base_url() . 'index.php/mapos/emitente');
            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar alterar as informações.');
                redirect(base_url() . 'index.php/mapos/emitente');
            }
        }
    }

    public function editarLogo()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('index.php/mapos/login');
        }

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para configurar emitente.');
            redirect(base_url());
        }

        $id = $this->input->post('id');
        if ($id == null || !is_numeric($id)) {
            $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar alterar a logomarca.');
            redirect(base_url() . 'index.php/mapos/emitente');
        }
        $this->load->helper('file');
        delete_files(FCPATH . 'assets/uploads/');

        $image = $this->do_upload();
        $logo = base_url() . 'assets/uploads/' . $image;

        $retorno = $this->mapos_model->editLogo($id, $logo);
        if ($retorno) {

            $this->session->set_flashdata('success', 'As informações foram alteradas com sucesso.');
            redirect(base_url() . 'index.php/mapos/emitente');
        } else {
            $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar alterar as informações.');
            redirect(base_url() . 'index.php/mapos/emitente');
        }
    }
}
