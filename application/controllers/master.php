<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class master extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_model');
    }


    public function index()
    {
        $this->load->view('master/login');
    }


    public function sair()
    {
        $this->session->sess_destroy();
        redirect('master');
    }

    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('masterlogin', 'Usuário', 'required|xss_clean|trim');
        $this->form_validation->set_rules('senha', 'Senha', 'required|xss_clean|trim');
        $ajax = $this->input->get('ajax');

        if ($this->form_validation->run() == false) {
            if ($ajax == true) {
                $json = array('result' => false);
                echo json_encode($json);
            } else {
                $this->session->set_flashdata('error', 'Os dados de acesso estão incorretos.');
                redirect($this->login);
            }
        } else {

            $usuariologin   = $this->input->post('masterlogin');
            $senha          = $this->input->post('senha');

            $this->load->library('encrypt');
            $senha = $this->encrypt->sha1($senha);

            $this->db->where('masterlogin', $usuariologin);
            $this->db->where('senha', $senha);
            $this->db->where('status', 1);
            $this->db->limit(1);
            $master = $this->db->get('master')->row();

            if (count($master) > 0) {

                $dados = array(
                    'nome'          => $master->nome,
                    'id'            => $master->idMaster,
                    'logado'        => TRUE);
                $this->session->set_userdata($dados);

                if ($ajax == true) {
                    $json = array('result' => true);
                    redirect(base_url() . 'master/painel');
                } else {
                    redirect(base_url() . 'master/painel');
                }

            } else {
                if ($ajax == true) {
                    $json = array('result' => false);
                    echo json_encode($json);
                } else {
                    $this->session->set_flashdata('error', 'Os dados de acesso estão incorretos.');
                    redirect($this->login);
                }
            }
        }
    }

    public function painel()
    {
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('master');
        }

        $data['menuPainel'] = 'painel';
        $data['output'] = 'master/painel';
        $this->load->view('master/template', $data);

    }



}

/* End of file conecte.php */
/* Location: ./application/controllers/conecte.php */