<?php

class naturezaoperacao extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('naturezaoperacao_model', '', TRUE);

        $this->data['menuNaturezaOperacao'] = 'Natureza de operação';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vNaturezaoperacao')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar natureza de operacao.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/naturezaoperacao/gerenciar/';
        $config['total_rows'] = $this->naturezaoperacao_model->count('naturezaoperacao');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->naturezaoperacao_model->get('naturezaoperacao','*','',$config['per_page'],$this->uri->segment(3));
        $this->data['view'] = 'naturezaoperacao/naturezaoperacao';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aNaturezaoperacao')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar natureza de operacao.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('naturezaoperacao') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'tipoEstoque' => $this->input->post('tipoEstoque'),
                'ICMS' => $this->input->post('ICMS'),
                'IPI' => $this->input->post('IPI'),
                'PIS' => $this->input->post('PIS'),
                'COFINS' => $this->input->post('COFINS'),
                'CFOP' => $this->input->post('CFOP'),
            );

            if ($this->naturezaoperacao_model->add('naturezaoperacao', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Natureza de operacao adicionado com sucesso!');
                redirect(base_url() . 'index.php/naturezaoperacao/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'naturezaoperacao/adicionarNaturezaoperacao';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eNaturezaoperacao')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar natureza de operacao.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('naturezaoperacao') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'tipoEstoque' => $this->input->post('tipoEstoque'),
                'ICMS' => $this->input->post('ICMS'),
                'IPI' => $this->input->post('IPI'),
                'PIS' => $this->input->post('PIS'),
                'COFINS' => $this->input->post('COFINS'),
                'CFOP' => $this->input->post('CFOP'),
            );

            if ($this->naturezaoperacao_model->edit('naturezaoperacao', $data, 'idNaturezaoperacao', $this->input->post('idNaturezaoperacao')) == TRUE) {
                $this->session->set_flashdata('success', 'Natureza de operação editado com sucesso!');
                redirect(base_url() . 'index.php/naturezaoperacao/editar/'.$this->input->post('idNaturezaoperacao'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->naturezaoperacao_model->getById($this->uri->segment(3));
        $this->data['view'] = 'naturezaoperacao/editarNaturezaoperacao';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dNaturezaoperacao')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir natureza de operação.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir natureza de operação.');
            redirect(base_url().'index.php/naturezaoperacao/gerenciar/');
        }

        $this->naturezaoperacao_model->delete('naturezaoperacao','idNaturezaoperacao',$id);

        $this->session->set_flashdata('success','Natureza de operação excluida com sucesso!');
        redirect(base_url().'index.php/naturezaoperacao/gerenciar/');
    }
}

