<?php

class nfce extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('nfce_model', '', TRUE);
        $this->load->model('tecnospeednotafiscalservico_model', '', TRUE);

        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('configuracao_model', '', TRUE);
        $this->load->model('naturezaoperacao_model', '', TRUE);
        $this->load->model('estoque_model', '', '');
        $this->load->model('site_model', '', '');

        $this->data['menuNFCE'] = 'Nota Fiscal';
        $this->data['menuNFE'] = 'Nota Fiscal';

    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vNFCE')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar nota fiscal de serviço.');
            //redirect(base_url());
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfce/gerenciar/';
        $config['total_rows'] = $this->nfce_model->count('nfce');
        $config['per_page'] = 100000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);


        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        if ($dataInicial == '') $dataInicial = date('Y-m-d');
        if ($dataFinal == '') $dataFinal = date('Y-m-d');

        $where = 'nfce.dhEmi >= "'.$dataInicial.'" and nfce.dhEmi <= "'.$dataFinal.'"';

        $this->data['results'] = $this->nfce_model->get('nfce', '*', $where, $config['per_page'], $this->uri->segment(3));

        $this->data['dataInicial'] = $dataInicial;
        $this->data['dataFinal'] = $dataFinal;
        $this->data['view'] = 'nfce/nfce';
        $this->load->view('theme/topo', $this->data);
    }

    function uploadXMLNF() {
        $xmlNF          = $this->do_upload();

        if ($xmlNF['file_name'] != '') $this->lendoXML($xmlNF['file_name']);

        $this->nfcePendente();
    }

    function forcarBaixarXML() {

        $this->data['view'] = 'nfce/forcarBaixarXML';
        $this->load->view('theme/topo', $this->data);
    }

    function lendoXML($nomeArquivo) {
        // Transformando arquivo XML em Objeto
        $origem = './assets/arquivos/xml/temporario/'.$nomeArquivo;

        $xml    = simplexml_load_file($origem);
        $nNF    = '';
        $CNPJ   = '';
        $chave  = '';
        $pasta  = '';
        //print_r($xml);

        foreach($xml->NFe  as $NFe):
            foreach($NFe->infNFe  as $infNFe):

                foreach($infNFe->ide  as $ide):

                    $nNF    = $ide->nNF;
                    $dhEmiT = $ide->dhEmi;
                    $chave  = str_replace ('NFe','',$infNFe['Id'][0]);

                    $dhEmi      = date('Y-m-d', strtotime($dhEmiT));
                    $hEmi       = date('H:i:s', strtotime($dhEmiT));
                    $pasta      = date('Ym', strtotime($dhEmiT));

                    $data = array(
                        'dhEmi'     => $dhEmi,
                        'hEmi'      => $hEmi,
                        'chavenfe'  => $chave,
                    );

                    $this->nfce_model->edit('nfce', $data, 'nNF', $nNF);

                endforeach;

                foreach($infNFe->emit  as $emit):
                    $CNPJ = $emit->CNPJ;
                endforeach;

            endforeach;
        endforeach;

        foreach($xml->protNFe  as $protNFe):
            foreach($protNFe  as $infProt):

                    $nProt = $infProt->nProt;
                    $cStat  = $infProt->cStat;

                    $data = array(
                        'nProt'     => $nProt,
                        'status'    => $cStat
                    );

                    $this->nfce_model->edit('nfce', $data, 'nNF', $nNF);
            endforeach;
        endforeach;

        $diretorio = './emissores/v4/nfephp-master/XML/'.$CNPJ.'/NF-e/producao/enviadas/aprovadas/'.$pasta.'/';
        $destino = $diretorio.$chave.'-nfce.xml';

        if (!file_exists($diretorio)) mkdir($diretorio, 0700);
        if (copy($origem, $destino));

    }

    public function do_upload(){
        $config['upload_path']      = './assets/arquivos/xml/temporario/';
        $config['allowed_types']    = 0;
        $config['max_size']         = 0;
        $config['max_width']        = '3000';
        $config['max_height']       = '2000';
        $config['encrypt_name']     = false;

        if (!is_dir('./assets/arquivos/xml/temporario')) mkdir('./assets/arquivos/xml/temporario', 0777, TRUE);
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('xmlNF')) return FALSE;
        else return $this->upload->data();
    }

    function nfcePendente()
    {

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfce/nfcePendente/';
        $config['total_rows'] = $this->nfce_model->count('nfce');
        $config['per_page'] = 100000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->nfce_model->get('nfce', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'nfce/nfcePendente';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aNFCE')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar nota fiscal de serviço.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $configuracao = $this->configuracao_model->getById(1);

        if ($this->form_validation->run('nfce') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $destCNPJ   = $this->formatarNumeroDocumento($this->input->post('destCNPJ'));
            $destCEP    = $this->formatarCEP($this->input->post('destCEP'));

            $data = array(
                'modelo'   => $this->input->post('modelo'),
                'serie'   => $this->input->post('serie'),
                'nNF'   => $this->input->post('nNF'),
                'natOp'   => $this->input->post('natOp'),
                'dhEmi'   => $this->input->post('dhEmi'),
                'hEmi'   => $this->input->post('hEmi'),
                'indPag'   => $this->input->post('indPag'),
                'tpEmis'   => $this->input->post('tpEmis'),
                'finNFe'   => $this->input->post('finNFe'),
                'indPres'   => $this->input->post('indPres'),
                'clientes_id'   => $this->input->post('clientes_id'),
                'destxNome'   => $this->input->post('destxNome'),
                'destCNPJ'   => $destCNPJ,
                'destIE'   => $this->input->post('destIE'),
                'destISUF'   => $this->input->post('destISUF'),
                'destxLgr'   => $this->input->post('destxLgr'),
                'destnro'   => $this->input->post('destnro'),
                'destxCpl'   => $this->input->post('destxCpl'),
                'destCEP'   => $destCEP,
                'destxBairro'   => $this->input->post('destxBairro'),
                'destxMun'   => $this->input->post('destxMun'),
                'destcMun'   => $this->input->post('destcMun'),
                'destUF'   => $this->input->post('destUF'),
                'destfone'   => $this->input->post('destfone'),
                'destemail'   => $this->input->post('destemail'),
                'usuarios_id'   => $this->input->post('usuarios_id'),
                'naturezaoperacao_id'   => $configuracao->naturezaoperacaodentroestado_id,
            );

            if (is_numeric($id = $this->nfce_model->add('nfce', $data, true))) {
                $this->session->set_flashdata('success', 'Nota Fiscal de serviço cadastrada com sucesso, adicione as informações do serviço.');
                redirect('nfce/editar/' . $this->input->post('nNF'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['nNF']              = $this->nfce_model->getProximoNumeroNFCE();
        $this->data['nSerie']           = $configuracao->numeroDeSerieNFCE;
        $this->data['emitente']         = $this->mapos_model->getEmitente();
        $this->data['clientes']         = $this->clientes_model->getAll();
        $this->data['usuarios']         = $this->usuarios_model->getAll();
        $this->data['view']             = 'nfce/adicionarNfce';
        $this->load->view('theme/topo', $this->data);
    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eNfce')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar nota fiscal de serviço');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('nfce') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $destCNPJ   = $this->formatarNumeroDocumento($this->input->post('destCNPJ'));
            $destCEP    = $this->formatarCEP($this->input->post('destCEP'));

            $data = array(
                'modelo'   => $this->input->post('modelo'),
                'serie'   => $this->input->post('serie'),
                'nNF'   => $this->input->post('nNF'),
                'natOp'   => $this->input->post('natOp'),
                'dhEmi'   => $this->input->post('dhEmi'),
                'hEmi'   => $this->input->post('hEmi'),
                'indPag'   => $this->input->post('indPag'),
                'tpEmis'   => $this->input->post('tpEmis'),
                'finNFe'   => $this->input->post('finNFe'),
                'indPres'   => $this->input->post('indPres'),
                'destxNome'   => $this->input->post('destxNome'),
                'destCNPJ'   => $destCNPJ,
                'destIE'   => $this->input->post('destIE'),
                'destISUF'   => $this->input->post('destISUF'),
                'destxLgr'   => $this->input->post('destxLgr'),
                'destnro'   => $this->input->post('destnro'),
                'destxCpl'   => $this->input->post('destxCpl'),
                'destCEP'   => $destCEP,
                'destxBairro'   => $this->input->post('destxBairro'),
                'destxMun'   => $this->input->post('destxMun'),
                'destcMun'   => $this->input->post('destcMun'),
                'destUF'   => $this->input->post('destUF'),
                'destfone'   => $this->input->post('destfone'),
                'destemail'   => $this->input->post('destemail'),
                'vBCnf'   => $this->input->post('vBCnf'),
                'vICMSnf'   => $this->input->post('vICMSnf'),
                'vBCSTnf'   => $this->input->post('vBCSTnf'),
                'vFretenf'   => $this->input->post('vFretenf'),
                'vOutronf'   => $this->input->post('vOutronf'),
                'vIInf'   => $this->input->post('vIInf'),
                'vIPInf'   => $this->input->post('vIPInf'),
                'vPISnf'   => $this->input->post('vPISnf'),
                'vCOFINSnf'   => $this->input->post('vCOFINSnf'),
                'vSegnf'   => $this->input->post('vSegnf'),
                'vDescnf'   => $this->input->post('vDescnf'),
                'vICMSDesonnf'   => $this->input->post('vICMSDesonnf'),
                'qVol'   => $this->input->post('qVol'),
                'esp'   => $this->input->post('esp'),
                'infCpl'   => $this->input->post('infCpl'),
                'tpag'   => $this->input->post('tpag'),
                'vPag'   => $this->input->post('vPag'),
                'vTroco'   => $this->input->post('vTroco'),
                'dinheiro'   => $this->input->post('dinheiro'),
                'cartao_debito'   => $this->input->post('cartao_debito'),
                'cartao_credito'   => $this->input->post('cartao_credito'),
                'valorParcelado'   => $this->input->post('valorParcelado'),
                'usuarios_id'   => $this->input->post('usuarios_id'),
                'clientes_id'   => $this->input->post('clientes_id'),
            );

            $acrescimo = $this->input->post('vOutronf');

            if ($acrescimo > 0) {
                $itens = $this->nfce_model->getProdutos($this->uri->segment(3));
                $acrescimo = $acrescimo / count($itens);

                foreach ($itens as $item) {
                    $this->nfce_model->edit('produtonfe',  array('vOutro' => $acrescimo) , 'prodnfeid', $item->prodnfeid);
                }
            }

            if ($this->nfce_model->edit('nfce', $data, 'nNF', $this->input->post('nNF')) == TRUE) {
                $this->atualizarTotalizadorNFCE(false);
                $this->session->set_flashdata('success', 'Venda editada com sucesso!');
                redirect(base_url() . 'index.php/nfce');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['emitente']     = $this->mapos_model->getEmitente();
        $this->data['clientes']     = $this->clientes_model->getAll();
        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['result']       = $this->nfce_model->getById($this->uri->segment(3));
        $this->data['produtos']     = $this->nfce_model->getProdutos($this->uri->segment(3));

        $this->data['view']         = 'nfce/editarNfce';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionarProduto() {

        $prodnfeid = $this->input->post('prodnfeid');

        //calcular ibpt
        $data = array(
            'NFCe'   => $this->input->post('nNF'),
            'cProd'   => $this->input->post('cProd'),
            'xProd'   => $this->input->post('xProd'),
            'uCom'   => $this->input->post('uCom'),
            'qtd'   => $this->input->post('qtd'),
            'vProd'   => $this->input->post('vProd'),
            'vlrtotal'   => $this->input->post('vlrtotal'),
            'valortotal'   => $this->input->post('valortotal'),
            'NCM'   => $this->input->post('NCM'),
            'CEST'   => $this->input->post('CEST'),
            'CFOP'   => $this->input->post('CFOP'),
            'prodEspe'   => $this->input->post('prodEspe'),
            'infAdProd'   => $this->input->post('infAdProd'),
            'orig' =>  $this->input->post('orig'),
            'CST' =>  $this->input->post('CST'),
            'vBCSTRet' => $this->input->post('vBCSTRet'),
            'pST' => $this->input->post('pST'),
            'vICMSSTRet' => $this->input->post('vICMSSTRet'),
            'vBC' => $this->input->post('vBC'),
            'pICMS' => $this->input->post('pICMS'),
            'vICMS' => $this->input->post('vICMS'),
            'modBC' => $this->input->post('modBC'),
            'vBCST' => $this->input->post('vBCST'),
            'vICMSST' => $this->input->post('vICMSST'),
            'cProdANP' => $this->input->post('cProdANP'),
            'descANP' => $this->input->post('descANP'),
            'UFCons' => $this->input->post('UFCons'),
            'CSTIPI' => $this->input->post('CSTIPI'),
            'pIPI' => $this->input->post('pIPI'),
            'vBCIPI' => $this->input->post('vBCIPI'),
            'vIPI' => $this->input->post('vIPI'),
            'CSTPIS' => $this->input->post('CSTPIS'),
            'pPIS' => $this->input->post('pPIS'),
            'vBCPIS' => $this->input->post('vBCPIS'),
            'vPIS' => $this->input->post('vPIS'),
            'CSTCOFINS' => $this->input->post('CSTCOFINS'),
            'pCOFINS' => $this->input->post('pCOFINS'),
            'vBCCOFINS' => $this->input->post('vBCCOFINS'),
            'vCOFINS' => $this->input->post('vCOFINS'),
            'pICMSST' => $this->input->post('pICMSST'),
            'produtoid' => $this->input->post('produtoid')

        );

        if ($prodnfeid) {
            $this->nfce_model->edit('produtonfe', $data, 'prodnfeid', $prodnfeid);
            echo json_encode(array('resultado' => true));
        } else {
            if (is_numeric($id = $this->nfce_model->add('produtonfe', $data, true))) {
                echo json_encode(array('resultado' => true));
            } else {
                echo json_encode(array('resultado' => false));
            }
        }
    }

    function atualizarTotalizadorNFCE($retorno = true) {

        $nNF        = $this->input->post('nNF');
        $produtos   = $this->nfce_model->getProdutos($nNF);

        $vFretenf   = $this->input->post('vFretenf');
        $vOutronf   = $this->input->post('vOutronf');
        $vIPInf     = $this->input->post('vIPInf');
        $vSegnf     = $this->input->post('vSegnf');
        $vDescnf    = $this->input->post('vDescnf');

        $total      = 0;
        $vTotTrib   = 0;

        $vBC        = 0;
        $vBCST      = 0;
        $vICMS      = 0;
        $vIPI       = 0;
        $vPIS       = 0;
        $vCOFINS    = 0;
        $vICMSST    = 0;

        foreach ($produtos as $produto) {
            $total        = $total + $produto->vlrtotal;
            $vTotTrib     = $vTotTrib + $produto->vTotTrib;

            //aliquotas
            $vBC        += $produto->vBC;
            $vBCST      += $produto->vBCST;
            $vICMS      += $produto->vICMS;
            $vIPI       += $produto->vIPI;
            $vPIS       += $produto->vPIS;
            $vCOFINS    += $produto->vCOFINS;
            $vICMSST    += $produto->vICMSST;
        }

        $valortotalnf = $total + $vFretenf + $vOutronf + $vSegnf + $vIPInf - $vDescnf;

        $data = array(

            'vBCnf'          => $vBC,
            'vBCSTnf'        => $vBCST,
            'vICMSSTnf'      => $vICMSST,
            'vICMSnf'        => $vICMS,
            'vIPInf'         => $vIPI,
            'vPISnf'         => $vPIS,
            'vCOFINSnf'      => $vCOFINS,

            'vFretenf'      => $vFretenf,
            'vOutronf'      => $vOutronf,
            'vSegnf'        => $vSegnf,
            'vDescnf'       => $vDescnf,

            'vNF'            => $valortotalnf,
            'vProdnf'        => $total
        );
        $this->nfce_model->edit('nfce', $data, 'nNF', $nNF);

        if ($retorno)  echo json_encode($this->nfce_model->getById($nNF));

    }

    function getNotaFiscalById() {
        $nNF = $this->input->post('nNF');
        echo json_encode($this->nfce_model->getById($nNF));
    }

    function excluirProduto() {
        $prodnfeid = $this->input->post('prodnfeid');
        $this->db->where('prodnfeid', $prodnfeid);
        $this->db->delete('produtonfe');
    }

    function getProdutosById() {
        $prodnfeid = $this->input->post('prodnfeid');
        echo json_encode($this->nfce_model->getProdutosById($prodnfeid));
    }

    function iframeNFEOS($os_id) {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vNFCE')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar nota fiscal de serviço.');
            //redirect(base_url());
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfce/gerenciar/';
        $config['total_rows'] = $this->nfe_model->count('nfce');
        $config['per_page'] = 1000000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->nfce_model->getNFEByOS($os_id);
        $this->data['view'] = 'nfce/nfce';
        $this->load->view('theme/blank', $this->data);
    }

    function excluir($nNF) {
        $this->nfce_model->delete('produtonfe', 'NFCe', $nNF);
        $this->nfce_model->delete('nfce', 'nNF', $nNF);
        $this->session->set_flashdata('success', 'NF excluída com sucesso!');
        redirect(base_url() . 'index.php/nfse/nfse');
    }

    function controleEstoqueCancelamento()  {

        $nNF                = $this->input->post('nNF');
        $nfe                = $this->nfce_model->getById($nNF);
        $produtosnfe        = $this->nfce_model->getProdutos($nNF);
        $naturezaOperacao   = $this->naturezaoperacao_model->getById($nfe->naturezaoperacao_id);
        $tipoEstoque        = $naturezaOperacao->tipoEstoque;

        foreach ($produtosnfe as $produto) {

            $quantidade = $produto->qtd;
            $prodnfeid  = $produto->prodnfeid;
            $produto_id = $produto->produtoid;

            if ('ENTRADA' === $tipoEstoque) {
                $this->estoque_model->saidaEstoque($quantidade, $produto_id, 'NFC-e (Cancelamento)', $nNF, $prodnfeid, $this->session->userdata('filial_id'));
            } else if ('SAIDA') {
                $this->estoque_model->entradaEstoque($quantidade, $produto_id, 'NFC-e (Cancelamento)', $nNF, $prodnfeid, $this->session->userdata('filial_id'));
            }
        }
    }

    function formatarNumeroDocumento($destCNPJ) {
        $destCNPJ   = str_replace('.','', $destCNPJ);
        $destCNPJ   = str_replace('-','', $destCNPJ);
        $destCNPJ   = str_replace('/','', $destCNPJ);

        return $destCNPJ;
    }

    function formatarCEP($destCEP) {
        $destCEP    = str_replace('.','', $destCEP);
        $destCEP    = str_replace('-','', $destCEP);
        $destCEP    = str_replace('/','', $destCEP);
        return $destCEP;
    }
}

