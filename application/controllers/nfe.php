<?php

class nfe extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('nfe_model', '', TRUE);
        $this->load->model('tecnospeednotafiscalservico_model', '', TRUE);

        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('contador_model', '', TRUE);
        $this->load->model('naturezaoperacao_model', '', TRUE);
        $this->load->model('configuracao_model', '', TRUE);
        $this->load->model('mapos_model', '', '');
        $this->load->model('estoque_model', '', '');

        $this->data['menuNFE'] = 'Nota Fiscal';
        $this->data['menuNFENFE'] = 'Nota Fiscal';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar nota fiscal de serviço.');
            redirect(base_url());
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfe/gerenciar/';
        $config['total_rows'] = $this->nfe_model->count('nfe');
        $config['per_page'] = 1000000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->nfe_model->get('nfe', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'nfe/nfe';
        $this->load->view('theme/topo', $this->data);
    }

    function formatarCPFCNPJ($destCNPJ) {
        $destCNPJ = str_replace('.','', $destCNPJ);
        $destCNPJ = str_replace('-','', $destCNPJ);
        $destCNPJ = str_replace('/','', $destCNPJ);
        return $destCNPJ;
    }

    function formatarCEP($destCEP) {
        $destCEP = str_replace('.','', $destCEP);
        $destCEP = str_replace('-','', $destCEP);
        $destCEP = str_replace('/','', $destCEP);
        return $destCEP;
    }

    function adicionar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aNfe')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar nota fiscal de serviço.');
            redirect(base_url());
        }

        $configuracao = $this->configuracao_model->getById(1);
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('nfe') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $destCNPJ = $this->formatarCPFCNPJ( $this->input->post('destCNPJ'));
            $destCEP =  $this->formatarCEP($this->input->post('destCEP'));
            $autXML = '';

            $contador = $this->contador_model->getById(1);

            if (count($contador) > 0 ) $autXML = $contador->cnpjCpf;

            $naturezaoperacao_id =  $this->input->post('naturezaoperacao_id');
            $naturezaOperacao = $this->naturezaoperacao_model->getById($naturezaoperacao_id);
            $natOp = $naturezaOperacao->nome;

            $data = array(
                'modelo'   => $this->input->post('modelo'),
                'serie'   => $this->input->post('serie'),
                'nNF'   => $this->input->post('nNF'),
                'natOp'   => $natOp,
                'dhEmi'   => $this->input->post('dhEmi'),
                'hEmi'   => $this->input->post('hEmi'),
                'dhSaiEnt'   => $this->input->post('dhSaiEnt'),
                'hSaiEnt'   => $this->input->post('hSaiEnt'),
                'indPag'   => $this->input->post('indPag'),
                'tpNF'   => $this->input->post('tpNF'),
                'tpEmis'   => $this->input->post('tpEmis'),
                'finNFe'   => $this->input->post('finNFe'),
                'indPres'   => $this->input->post('indPres'),
                'clientes_id'   => $this->input->post('clientes_id'),
                'destxNome'   => $this->input->post('destxNome'),
                'destCNPJ'   => $destCNPJ,
                'destIE'   => $this->input->post('destIE'),
                'destISUF'   => $this->input->post('destISUF'),
                'destxLgr'   => $this->input->post('destxLgr'),
                'destnro'   => $this->input->post('destnro'),
                'destxCpl'   => $this->input->post('destxCpl'),
                'destCEP'   => $destCEP,
                'autXML'   => $autXML,
                'destxBairro'   => $this->input->post('destxBairro'),
                'destxMun'   => $this->input->post('destxMun'),
                'destcMun'   => $this->input->post('destcMun'),
                'destUF'   => $this->input->post('destUF'),
                'destfone'   => $this->input->post('destfone'),
                'destemail'   => $this->input->post('destemail'),
                'usuarios_id'   => $this->input->post('usuarios_id'),
                'percentualICMS'   => $this->input->post('percentualICMS'),
                'cPais'   => $this->input->post('cPais'),
                'xPais'   => $this->input->post('xPais'),
                'naturezaoperacao_id'   => $naturezaoperacao_id,
            );

            if (is_numeric($id = $this->nfe_model->add('nfe', $data, true))) {
                $this->session->set_flashdata('success', 'Nota Fiscal de serviço cadastrada com sucesso, adicione as informações do serviço.');
                redirect('nfe/editar/' . $this->input->post('nNF'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }


        $this->data['nNF']              = $this->nfe_model->getProximoNumeroNFE();
        $this->data['nSerie']           = $configuracao->numeroDeSerieNFE;
        $this->data['emitente']         = $this->mapos_model->getEmitente();
        $this->data['clientes']         = $this->clientes_model->getAll();
        $this->data['usuarios']         = $this->usuarios_model->getAll();
        $this->data['naturezasoperacao']= $this->naturezaoperacao_model->getAll();
        $this->data['view']             = 'nfe/adicionarNfe';
        $this->load->view('theme/topo', $this->data);
    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eNfe')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar nota fiscal de serviço');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('nfe') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $destCNPJ = $this->formatarCPFCNPJ( $this->input->post('destCNPJ'));
            $destCEP =  $this->formatarCEP($this->input->post('destCEP'));
            $autXML = $this->input->post('$autXML');

            $contador = $this->contador_model->getById(1);
            if (count($contador) > 0 ) $autXML = $contador->cnpjCpf;

            $naturezaoperacao_id =  $this->input->post('naturezaoperacao_id');
            $naturezaOperacao = $this->naturezaoperacao_model->getById($naturezaoperacao_id);
            $natOp = $naturezaOperacao->nome;

            $data = array(
                'modelo'   => $this->input->post('modelo'),
                'serie'   => $this->input->post('serie'),
                'nNF'   => $this->input->post('nNF'),
                'natOp'   =>  $natOp,
                'dhEmi'   => $this->input->post('dhEmi'),
                'hEmi'   => $this->input->post('hEmi'),
                'dhSaiEnt'   => $this->input->post('dhSaiEnt'),
                'hSaiEnt'   => $this->input->post('hSaiEnt'),
                'indPag'   => $this->input->post('indPag'),
                'tpNF'   => $this->input->post('tpNF'),
                'tpEmis'   => $this->input->post('tpEmis'),
                'finNFe'   => $this->input->post('finNFe'),
                'indPres'   => $this->input->post('indPres'),
                'destxNome'   => $this->input->post('destxNome'),
                'destCNPJ'   => $destCNPJ,
                'destIE'   => $this->input->post('destIE'),
                'destISUF'   => $this->input->post('destISUF'),
                'destxLgr'   => $this->input->post('destxLgr'),
                'destnro'   => $this->input->post('destnro'),
                'destxCpl'   => $this->input->post('destxCpl'),
                'destCEP'   => $destCEP,
                'destxBairro'   => $this->input->post('destxBairro'),
                'destxMun'   => $this->input->post('destxMun'),
                'destcMun'   => $this->input->post('destcMun'),
                'destUF'   => $this->input->post('destUF'),
                'destfone'   => $this->input->post('destfone'),
                'destemail'   => $this->input->post('destemail'),
                'vBCnf'   => $this->input->post('vBCnf'),
                'vICMSnf'   => $this->input->post('vICMSnf'),
                'vBCSTnf'   => $this->input->post('vBCSTnf'),
                'vICMSSTnf'   => $this->input->post('vICMSSTnf'),
                'valortotalprod'   => $this->input->post('valortotalprod'),
                'vFretenf'   => $this->input->post('vFretenf'),
                'vTotTrib'   => $this->input->post('vTotTrib'),
                'vOutronf'   => $this->input->post('vOutronf'),
                'vIInf'   => $this->input->post('vIInf'),
                'vIPInf'   => $this->input->post('vIPInf'),
                'vPISnf'   => $this->input->post('vPISnf'),
                'vCOFINSnf'   => $this->input->post('vCOFINSnf'),
                'vSegnf'   => $this->input->post('vSegnf'),
                'vDescnf'   => $this->input->post('vDescnf'),
                'vICMSDesonnf'   => $this->input->post('vICMSDesonnf'),
                'valortotalnf'   => $this->input->post('valortotalnf'),
                'nFatcob'   => $this->input->post('nFatcob'),
                'vOrigcob'   => $this->input->post('vOrigcob'),
                'vDesccob'   => $this->input->post('vDesccob'),
                'vLiqcob'   => $this->input->post('vLiqcob'),
                'modFrete'   => $this->input->post('modFrete'),
                'transpCNPJ'   => $this->input->post('transpCNPJ'),
                'transpIE'   => $this->input->post('transpIE'),
                'transpxNome'   => $this->input->post('transpxNome'),
                'transpCEP'   => $this->input->post('transpCEP'),
                'transpxLgr'   => $this->input->post('transpxLgr'),
                'transpnro'   => $this->input->post('transpnro'),
                'transpxCpl'   => $this->input->post('transpxCpl'),
                'transpxBairro'   => $this->input->post('transpxBairro'),
                'transpxMun'   => $this->input->post('transpxMun'),
                'transpUF'   => $this->input->post('transpUF'),
                'qVol'   => $this->input->post('qVol'),
                'esp'   => $this->input->post('esp'),
                'marca'   => $this->input->post('marca'),
                'placa'   => $this->input->post('placa'),
                'VeicUF'   => $this->input->post('VeicUF'),
                'RNTC'   => $this->input->post('RNTC'),
                'nVol'   => $this->input->post('nVol'),
                'pesoL'   => $this->input->post('pesoL'),
                'pesoB'   => $this->input->post('pesoB'),
                'infCpl'   => $this->input->post('infCpl'),
                'autXML'   => '13937073000156',//TODO NAO ESQUECA DE TIRAR DAQUI
                'usuarios_id'   => $this->input->post('usuarios_id'),
                'clientes_id'   => $this->input->post('clientes_id'),
                'percentualICMS'   => $this->input->post('percentualICMS'),
                'cPais'   => $this->input->post('cPais'),
                'xPais'   => $this->input->post('xPais'),
                'naturezaoperacao_id'   => $naturezaoperacao_id,
            );

            if ($this->nfe_model->edit('nfe', $data, 'nNF', $this->input->post('nNF')) == TRUE) {
                $this->atualizarTotalizadorNFE(false);
                $this->atualizarCFOPProdutos($naturezaOperacao);
                $this->adicionarNotaFiscalReferenciada($this->input->post('nNF'));
                $this->adicionarCupomFiscal($this->input->post('nNF'));
                $this->session->set_flashdata('success', 'Venda editada com sucesso!');
                redirect(base_url() . 'index.php/nfe');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }


        $this->data['nfenfes']      = $this->nfe_model->getAllNotasFiscaisReferenciada($this->uri->segment(3));
        $this->data['cfnfes']       = $this->nfe_model->getAllCuponsFiscais($this->uri->segment(3));
        $this->data['emitente']     = $this->mapos_model->getEmitente();
        $this->data['clientes']     = $this->clientes_model->getAll();
        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['result']       = $this->nfe_model->getById($this->uri->segment(3));
        $this->data['produtos']     = $this->nfe_model->getProdutos($this->uri->segment(3));
        $this->data['naturezasoperacao']= $this->naturezaoperacao_model->getAll();

        $this->data['view']         = 'nfe/editarNfe';
        $this->load->view('theme/topo', $this->data);
    }

    function controleEstoqueCancelamento()  {
        $nNF                = $this->input->post('nNF');
        $nfe                = $this->nfe_model->getById($nNF);
        $produtosnfe        = $this->nfe_model->getProdutos($nNF);
        $naturezaOperacao   = $this->naturezaoperacao_model->getById($nfe->naturezaoperacao_id);
        $tipoEstoque        = $naturezaOperacao->tipoEstoque;

        foreach ($produtosnfe as $produto) {

            $quantidade = $produto->qtd;
            $prodnfeid  = $produto->prodnfeid;
            $produto_id = $produto->produtoid;

            if ('ENTRADA' === $tipoEstoque) {
                $this->estoque_model->saidaEstoque($quantidade, $produto_id, 'NF-e (Cancelamento)', $nNF, $prodnfeid, $this->session->userdata('filial_id'));
            } else if ('SAIDA') {
                $this->estoque_model->entradaEstoque($quantidade, $produto_id, 'NF-e (Cancelamento)', $nNF, $prodnfeid, $this->session->userdata('filial_id'));
            }
        }
    }

    function controleEstoque() {
        $nNF                = $this->input->post('nNF');
        $nfe                = $this->nfe_model->getById($nNF);
        $produtosnfe        = $this->nfe_model->getProdutos($nNF);
        $naturezaOperacao   = $this->naturezaoperacao_model->getById($nfe->naturezaoperacao_id);
        $tipoEstoque        = $naturezaOperacao->tipoEstoque;

        foreach ($produtosnfe as $produto) {

            $quantidade = $produto->qtd;
            $prodnfeid  = $produto->prodnfeid;
            $produto_id = $produto->produtoid;

            if ('ENTRADA' === $tipoEstoque) {
                $this->estoque_model->entradaEstoque($quantidade, $produto_id, 'NF-e', $nNF, $prodnfeid, $this->session->userdata('filial_id'));
            } else if ('SAIDA') {
                $this->estoque_model->saidaEstoque($quantidade, $produto_id, 'NF-e', $nNF, $prodnfeid, $this->session->userdata('filial_id'));
            }
        }

        $this->gerarFinanceiroNF($nfe);
    }

    function gerarFinanceiroNF($nfe) {
        $data = array(
            'descricao'         => 'Venda NF-e',
            'valor'             => $nfe->valortotalnf,
            'custo'             => 0,
            'clientes_id'       => $nfe->clientes_id,
            'data_vencimento'   => date('Y-m-d'),
            'data_pagamento'    => date('Y-m-d'),
            'baixado'           => 1,
            'forma_pgto'        => 'DINHEIRO',
            'nfe'               => $nfe->nNF,
            'tipo'              => 'receita',
            'cliente_fornecedor'=>  $nfe->destxNome,
        );
        $this->nfe_model->add('lancamentos', $data, [$nfe->valortotalnf], [date('Y-m-d')], 0);
    }

    function adicionarNotaFiscalReferenciada($nNF) {
        $chavenfes =  $this->input->post('chavenfe');
        if ( is_array ($chavenfes)) {
            $this->nfe_model->delete('nfenfe', 'nNF', $nNF);

            foreach ($chavenfes as $chavenfe) {
                if ($chavenfe) {
                    $chaves_data = array(
                        'chavenfe' => $chavenfe,
                        'nNF' => $nNF,
                    );
                    $this->nfe_model->addNotFilial('nfenfe', $chaves_data);
                }
            }
        }
    }

    function adicionarCupomFiscal($nNF) {
        $dados =  $this->input->post('ncf');

        if ( is_array ($dados)) {

            $this->nfe_model->delete('cfnfe', 'nNF', $nNF);
            $contador = 0;
            foreach ($dados as $dado) {
                $ncoo = $this->input->post('ncoo');
                $ncf = $this->input->post('ncf');
                $modelocf = $this->input->post('modelocf');

                if ($ncoo[$contador]) {
                    $cupom_data = array(
                        'ncf' => $ncf[$contador],
                        'ncoo' => $ncoo[$contador],
                        'modelo' => $modelocf[$contador],
                        'nNF' => $nNF,
                    );
                    $contador = $contador + 1;
                    $this->nfe_model->addNotFilial('cfnfe', $cupom_data);
                }
            }
        }
    }

    function atualizarCFOPProdutos($naturezaOperacao) {
        $produtosnfe = $this->nfe_model->getProdutos($this->uri->segment(3));
        foreach ($produtosnfe as $produtos) {
            $produtosnfe_data = array(
                'CFOP' => $naturezaOperacao->CFOP,
            );
            $this->nfe_model->edit('produtonfe', $produtosnfe_data, 'prodnfeid', $produtos->prodnfeid);
        }
    }

    function adicionarProduto() {

        $prodnfeid = $this->input->post('prodnfeid');

        $vICMS =  $this->input->post('vICMS');
        $vBCST = $this->input->post('vBCST');
        $vIPI = $this->input->post('vIPI');
        $vPIS = $this->input->post('vPIS');
        $vCOFINS =  $this->input->post('vCOFINS');
        $vICMSST = $this->input->post('vICMSST');

        $vTotTrib = ($vICMS + $vICMSST + $vIPI + $vPIS + $vCOFINS);

        //calcular ibpt
        $data = array(

            'NFe'   => $this->input->post('nNF'),
            'produtoid' =>$this->input->post('produtoid'),
            'cProd'   => $this->input->post('cProd'),
            'xProd'   => $this->input->post('xProd'),
            'uCom'   => $this->input->post('uCom'),
            'qtd'   => $this->input->post('qtd'),
            'vProd'   => $this->input->post('vProd'),
            'vlrtotal'   => $this->input->post('vlrtotal'),
            'valortotal'   => $this->input->post('valortotal'),
            'NCM'   => $this->input->post('NCM'),
            'CEST'   => $this->input->post('CEST'),
            'CFOP'   => $this->input->post('CFOP'),
            'prodEspe'   => $this->input->post('prodEspe'),
            'infAdProd'   => $this->input->post('infAdProd'),
            'orig' =>  $this->input->post('orig'),
            'CST' =>  $this->input->post('CST'),

            //aliquotas
            'vTotTrib' => $vTotTrib,

            'vBCSTRet' => $this->input->post('vBCSTRet'),
            'pST' => $this->input->post('pST'),

            //ICMS
            'vBC' => $this->input->post('vBC'),
            'pICMS' => $this->input->post('pICMS'),
            'vICMS' => $vICMS,
            'vICMSSTRet' => $vICMSST,//TODO VERIFICAR!!

            //ICMS ST
            'pICMSST' => $this->input->post('pICMSST'),
            'vICMSST' => $vICMSST,
            'vBCST' =>  $vBCST,

            'modBC' => $this->input->post('modBC'),
            'modBCST' => $this->input->post('modBCST'),

            //combustivel
            'cProdANP' => $this->input->post('cProdANP'),
            'descANP' => $this->input->post('descANP'),
            'UFCons' => $this->input->post('UFCons'),

            //IPI
            'pIPI' => $this->input->post('pIPI'),
            'CSTIPI' => $this->input->post('CSTIPI'),
            'vBCIPI' => $this->input->post('vBCIPI'),
            'vIPI' =>  $vIPI,

            //PIS
            'CSTPIS' => $this->input->post('CSTPIS'),
            'pPIS' => $this->input->post('pPIS'),
            'vBCPIS' => $this->input->post('vBCPIS'),
            'vPIS' =>  $vPIS,

            //COFINS
            'CSTCOFINS' => $this->input->post('CSTCOFINS'),
            'pCOFINS' => $this->input->post('pCOFINS'),
            'vBCCOFINS' => $this->input->post('vBCCOFINS'),
            'vCOFINS' => $vCOFINS,
        );

        if ($prodnfeid) {
            $this->nfe_model->edit('produtonfe', $data, 'prodnfeid', $prodnfeid);
            echo json_encode(array('resultado' => true));
        } else {
            if (is_numeric($id = $this->nfe_model->add('produtonfe', $data, true))) {
                echo json_encode(array('resultado' => true));
            } else {
                echo json_encode(array('resultado' => false));
            }
        }
    }

    function atualizarTotalizadorNFE($retorno = true) {

        $nNF        = $this->input->post('nNF');
        $produtos   = $this->nfe_model->getProdutos($nNF);

        $vFretenf   = $this->input->post('vFretenf');
        $vOutronf   = $this->input->post('vOutronf');
        $vIPInf     = $this->input->post('vIPInf');
        $vSegnf     = $this->input->post('vSegnf');
        $vDescnf    = $this->input->post('vDescnf');
        $vIInf      = $this->input->post('vIInf');
        $vPISnf     = $this->input->post('vPISnf');
        $vCOFINSnf  = $this->input->post('vCOFINSnf');


        $vBC        = 0;
        $vBCST      = 0;
        $vICMS      = 0;
        $vICMSST    = 0;
        $vIPI       = 0;
        $vPIS       = 0;
        $vCOFINS    = 0;
        $valortotalprod = 0;


        //        this.setDouble("vTotTrib", (totalICMS + totalICMSST + totalIPI + totalPIS + totalCOFINS));
        foreach ($produtos as $produto) {
            $valortotalprod  = $valortotalprod + $produto->vlrtotal;

            $vBC        = $vBC + $produto->vBC;
            $vBCST      = $vBCST + $produto->vBCST;

            $vICMS      = $vICMS + $produto->vICMS;
            $vICMSST    = $vICMSST + $produto->vICMSST;
            $vIPI       = $vIPI + $produto->vIPI;
            $vPIS       = $vPIS + $produto->vPIS;
            $vCOFINS    = $vCOFINS + $produto->vCOFINS;


        }

        $vTotTrib       = ($vICMS + $vICMSST + $vIPI + $vPIS + $vCOFINS);
        $vNF            = $valortotalprod - $vDescnf + $vFretenf + $vOutronf + $vIInf + $vIPInf + $vPISnf + $vCOFINSnf + $vSegnf +$vICMSST;

        $data = array(
            'vTotTrib'       => $vTotTrib,

            'vBCnf'          => $vBC,
            'vBCSTnf'        => $vBCST,
            'vICMSnf'        => $vICMS,
            'vICMSSTnf'      => $vICMSST,
            'vIPInf'         => $vIPI,
            'vPISnf'         => $vPIS,
            'vCOFINSnf'      => $vCOFINS,

            'vFretenf'      => $vFretenf,
            'vOutronf'      => $vOutronf,
            'vSegnf'        => $vSegnf,
            'vDescnf'       => $vDescnf,

            'vSTnf'         => $vICMSST,
            'vNF'           => $vNF + $vICMS,

            'valortotalprod' => $valortotalprod,
            'vProdnf'        => $valortotalprod
        );
        $this->nfe_model->edit('nfe', $data, 'nNF', $nNF);

        if ($retorno) {
            echo json_encode($this->nfe_model->getById($nNF));
        }
    }

    function getNotaFiscalById() {
        $nNF = $this->input->post('nNF');
        echo json_encode($this->nfe_model->getById($nNF));
    }

    function searchPesquisaProdutos($nNF) {
        $this->data['nNF']      = $nNF;
        $this->data['view']     = 'nfe/searchPesquisaProdutos';
        $this->load->view('theme/blank', $this->data);
    }

    function excluirProduto() {
        $prodnfeid = $this->input->post('prodnfeid');
        $this->db->where('prodnfeid', $prodnfeid);
        $this->db->delete('produtonfe');
    }

    function getProdutosById() {
        $prodnfeid = $this->input->post('prodnfeid');
        echo json_encode($this->nfe_model->getProdutosById($prodnfeid));
    }

    function iframeNFEOS($os_id) {

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfe/gerenciar/';
        $config['total_rows'] = $this->nfe_model->count('nfe');
        $config['per_page'] = 1000000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->nfe_model->getNFEByOS($os_id);
        $this->data['view'] = 'nfe/nfe';
        $this->load->view('theme/blank', $this->data);
    }

    function excluir($nNF) {
        $this->nfe_model->delete('produtonfe', 'NFe', $nNF);
        $this->nfe_model->delete('nfe', 'nNF', $nNF);
        $this->session->set_flashdata('success', 'NF excluída com sucesso!');
        redirect(base_url() . 'index.php/nfse/nfse');
    }

    function buscarNaturezaOperacao() {

        $nNF = $this->input->post('nNF');
        $nf = $this->nfe_model->getById($nNF);

        $configuracao = $this->configuracao_model->getById(1);

        if (count($nf) > 0 ) $naturezaOperacao = $this->naturezaoperacao_model->getById($nf->naturezaoperacao_id);
        else $naturezaOperacao = $this->naturezaoperacao_model->getById($configuracao->naturezaoperacaodentroestado_id );

        echo json_encode(array('configuracao' => $configuracao, 'naturezaOperacao' =>$naturezaOperacao, 'nf' =>$nf));
    }

    function buscarNaturezaOperacaoByCliente() {
        $cliente_id = $this->input->post('cliente');

        $configuracao = $this->configuracao_model->getById(1);
        $cliente = $this->clientes_model->getById($cliente_id);
        $emitente = $this->mapos_model->getEmitente();

        $ufEmitente = $emitente->uf;
        $ufCliente = $cliente->estado;

        $icms =  $this->nfe_model->getAliquotaICMS($ufEmitente, $ufCliente);

        if ($ufCliente == $ufEmitente) {
            echo json_encode(array('naturezaoperacao' => $configuracao->naturezaoperacaodentroestado_id, 'icms' =>$icms ));
        } else {
            echo json_encode(array('naturezaoperacao' => $configuracao->naturezaoperacaoforaestado_id , 'icms' => $icms));
        }


    }
}

