<?php

class nfefornecedor extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('nfefornecedor_model', '', TRUE);
        $this->load->model('tecnospeednotafiscalservico_model', '', TRUE);

        $this->load->model('fornecedor_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('contador_model', '', TRUE);
        $this->load->model('naturezaoperacao_model', '', TRUE);
        $this->load->model('configuracao_model', '', TRUE);
        $this->load->model('mapos_model', '', '');
        $this->load->model('estoque_model', '', '');
        $this->load->model('produtos_model', '', '');
        $this->load->model('filial_model', '', '');
        $this->load->model('unidade_model', '', '');

        $this->data['menuColetorXML'] = 'Coletor de XML';
    }

    function index() {}

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eNfe')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar nota fiscal de serviço');
            redirect(base_url());
        }

        $result                         = $this->nfefornecedor_model->getByChave($this->uri->segment(3));
        $this->data['emitente']         = $this->mapos_model->getEmitente();
        $this->data['clientes']         = $this->clientes_model->getAll();
        $this->data['usuarios']         = $this->usuarios_model->getAll();
        $this->data['result']           = $result;
        $this->data['produtos']         = $this->nfefornecedor_model->getProdutos($result->nNF);
        $this->data['naturezasoperacao']= $this->naturezaoperacao_model->getAll();

        $this->data['view']             = 'nfefornecedor/editarNfefornecedor';
        $this->load->view('tema/topo', $this->data);
    }

    function cadastrarProduto() {

        $elementos      = $this->input->post('elementos');
        $unidadesVenda  = $this->input->post('unidadesVenda');
        $contador       = 0;

        foreach ($elementos as $elemento) {
            $produtonfe                         = $this->nfefornecedor_model->getProdutosById($elemento);
            $verificaProdutoByCProdByFornecedor = $this->produtos_model->getByCProdByFornecedor($produtonfe->cProd);

            if (count($verificaProdutoByCProdByFornecedor) == 0) {
                $this->cadastroProdutoInner($produtonfe, $unidadesVenda[$contador]);
            } else if (count($verificaProdutoByCProdByFornecedor) > 0) {
                $this->produtoUnidade($verificaProdutoByCProdByFornecedor->idProduto, $unidadesVenda[$contador]);
            }

            $contador = $contador + 1;
        }
    }

    function produtoUnidade($idProduto, $unid) {

        //atulizar a unidade de venda do produto

        $filiais            = $this->filial_model->getAll();
        $qtdsVenda          = $this->input->post('qtdsVenda');
        $contador           = 0;

        $produto_array = array(
            'unidade' => $unid,
        );
        $this->produtos_model->edit('produtos', $produto_array, 'idProdutos', $idProduto);
        $produto            = $this->produtos_model->getById($idProduto);

        foreach ($filiais as $filial) {

            $unidade_origem     = $produto->unidade;
            $verificaExistencia = $this->unidade_model->getUnidadeProdutoByIdProduto($idProduto, $unidade_origem, $unid, $filial->idFilial);

            $produto_unidade = array(
                'unidade_origem' => $unidade_origem,
                'unidade' => $unid,
                'quantidade' => $qtdsVenda[$contador],
                'filial_id' => $filial->idFilial,
                'produto_id' => $idProduto
            );
            if (count($verificaExistencia) > 0) {
                $this->produtos_model->edit('unidade_produto', $produto_unidade, 'idUnidadeProduto', $verificaExistencia->idUnidadeProduto);
            } else {
                $this->produtos_model->addProdutoFilial('unidade_produto', $produto_unidade, TRUE);
            }
            $contador = $contador + 1;
        }

    }

    function atualizarEstoqueValor() {

        $elementos = $this->input->post('elementos');
        $valores   = $this->input->post('valores');
        $qtdsVenda = $this->input->post('qtdsVenda');
        $contador  = 0;

        foreach ($elementos as $elemento) {

            $produtonfe                         = $this->nfefornecedor_model->getProdutosById($elemento);
            $verificaProdutoByCProdByFornecedor = $this->produtos_model->getByCProdByFornecedor($produtonfe->cProd);

            if (count($verificaProdutoByCProdByFornecedor) > 0) {
                $this->atualizarEstoqueValorInner($verificaProdutoByCProdByFornecedor->idProduto, $produtonfe, $valores[$contador], $qtdsVenda[$contador]);
            }
            $contador = $contador + 1;
        }
    }

    function vincularProdutoFilial($idProduto, $unid) {
        $filiais  = $this->filial_model->getAll();

        foreach ($filiais as $filial) {
            $isExisteProdutoFilial = $this->produtos_model->getProdutoFilial($idProduto, $filial->idFilial);
            $produto_filial = array(
                'filial_id'     => $filial->idFilial,
                'produto_id'    => $idProduto
            );

            if (count($isExisteProdutoFilial) > 0) {
                $this->produtos_model->edit('produto_filial', $produto_filial, 'idProdutoFilial', $isExisteProdutoFilial->idProdutoFilial);
            } else {
                $this->produtos_model->addProdutoFilial('produto_filial', $produto_filial, TRUE);
            }

            $this->produtoUnidade($idProduto, $unid);
        }
    }

    function atualizarEstoqueValorInner($idProduto, $produtonfe, $valor=0, $indice) {

        $filiais  = $this->filial_model->getAll();

        foreach ($filiais as $filial) {
            $isExisteProdutoFilial = $this->produtos_model->getProdutoFilial($idProduto, $filial->idFilial);
            $produto_filial = array(
                'filial_id'     => $filial->idFilial,
                'produto_id'    => $idProduto,
                'precoCompra'   => $produtonfe->vProd/$indice,
                'precoVenda'    => $valor
            );

            if (count($isExisteProdutoFilial) > 0) {
                $this->produtos_model->edit('produto_filial', $produto_filial, 'idProdutoFilial', $isExisteProdutoFilial->idProdutoFilial);
            } else {
                $this->produtos_model->addProdutoFilial('produto_filial', $produto_filial, TRUE);
            }

            if ($produtonfe->estoque_atualizado != 'S') {
                $this->estoque_model->entradaEstoque($produtonfe->qtd * $indice, $idProduto, 'NF-e Fornecedor', $produtonfe->NFe, $produtonfe->prodnfeid, $filial->idFilial);
            }
        }

        $dados = array('estoque_atualizado' => 'S');
        $this->produtos_model->edit('produtonfe_fornecedor', $dados, 'prodnfeid', $produtonfe->prodnfeid);
    }

    function vincularProdutoFornecedor($idProduto, $produtonfe) {

        $cnpj       = $this->input->post('destCNPJ');
        $fornecedor = $this->fornecedor_model->getByCNPJ($cnpj);

        if (count($fornecedor) == 0) {
            $nfe        = $this->nfefornecedor_model->getById($produtonfe->NFe);
            $fornecedor = $this->cadastrarFornecedor($nfe->chavenfe, $cnpj);
        }

        if (count($fornecedor) > 0) {
            $data = array(
                'idFornecedor' => $fornecedor->idFornecedor,
                'idProduto' => $idProduto,
                'codigo' => $produtonfe->cProd,
                'observacao' => '',
            );
            $this->produtos_model->add('fornecedorproduto', $data, TRUE);
        }
    }

    function cadastroProdutoInner($produtonfe, $unidadesVenda) {

        if ($unidadesVenda == '') $unidadesVenda =  $produtonfe->uCom;

        $CST = $produtonfe->CST;

        if ($CST === '60') {
            $CFOP = '5405';
            $CST = '500';
        } else {
            $CFOP = '5102';
            $CST = '102';
        }

        $data = array(
            'descricao' => $produtonfe->xProd,
            'unidade' => $unidadesVenda,
            'ean' => $produtonfe->cEan,
            'ncm' => $produtonfe->NCM,
            'CFOP' => $CFOP,
            'CST' => $CST,
            'cProd' => $produtonfe->cProd,
            'cEAN' => $produtonfe->cEan,
            'CEST' => $produtonfe->CEST,
            'origem' => $produtonfe->orig,
            'observacao' => '',
            'grupoProduto_id' => 1,//TODO
            'precoCompra' => 0,
            'precoVenda' => 0,
            'comissao' => 0,
            'estoque' => 0,
            'estoqueMinimo' => 0,
            'percentualIBPT' => 0,
            'infoNota' => '',
            'pIPI' => 0,
            'pPIS' => 0,
            'pCOFINS' => 0,
            'pICMSST' => 0,
            'CSTIPI' => $produtonfe->CSTIPI,
            'CSTPIS' => $produtonfe->CSTPIS,
            'CSTCOFINS' => $produtonfe->CSTCOFINS,
            'modBCST' => $produtonfe->modBCST,
            'cProdANP' => '',
            'descANP' => '',
            'UFCons' => '',
        );

        $idProduto = $this->produtos_model->add('produtos', $data, TRUE);

        $this->vincularProdutoFornecedor($idProduto, $produtonfe);
        $this->vincularProdutoFilial($idProduto, $unidadesVenda);
    }

    function associarProduto() {

        $idProduto  = $this->input->post('produtoOrigem');
        $cnpj       = $this->input->post('destCNPJ');
        $cProd      = $this->input->post('cProd');
        $chave      = $this->input->post('chave');

        $produto     = $this->produtos_model->getById($idProduto);
        $fornecedor = $this->fornecedor_model->getByCNPJ($cnpj);

        if (count($fornecedor) == 0) $fornecedor = $this->cadastrarFornecedor($chave, $cnpj);

        $data = array(
            'idFornecedor'  => $fornecedor->idFornecedor,
            'idProduto'     => $produto->idProdutos,
            'codigo'        => $cProd,
            'observacao'    => '',
        );
        $this->produtos_model->add('fornecedorproduto', $data, TRUE);
    }

    function desassociarProduto() {

        $idFornecedorproduto  = $this->input->post('idFornecedorproduto');

        $this->db->where('idFornecedorproduto',$idFornecedorproduto);
        $this->db->delete('fornecedorproduto');
    }

    function cadastrarFornecedor ($chave, $cnpj) {

        $nfe  = $this->nfefornecedor_model->getByChave($chave);
        $dados_fornecedor = array(
            'documento' => $cnpj,
            'tipoPessoa' => 'PJ',
            'IE' => $nfe->emitIE,
            'nomeFornecedor' => $nfe->destxNome,
            'nomeFantasiaApelido' => $nfe->destxFant.' ',
            'cep' => $nfe->destCEP,
            'rua' => $nfe->destxLgr,
            'numero' => $nfe->destnro,
            'bairro' => $nfe->destxBairro,
            'cidade' => $nfe->destxMun,
            'estado' => $nfe->destUF,
            'codIBGECidade' => $nfe->destcMun,
            'telefone' => $nfe->destfone,
            'complemento' => $nfe->destxCpl,
            'email' => $nfe->destemail,
            'dataCadastro'      => date('Y-m-d')
        );
        $this->fornecedor_model->add('fornecedor', $dados_fornecedor, TRUE);
        return $this->fornecedor_model->getByCNPJ($cnpj);
    }
}

