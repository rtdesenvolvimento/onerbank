<?php

class nfse extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('nfse_model', '', TRUE);
        $this->load->model('tecnospeednotafiscalservico_model', '', TRUE);

        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);

        $this->data['menuNFE'] = 'Nota Fiscal';
        $this->data['menuNFENFSE'] = 'Nota Fiscal Serviço';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vNFSE')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar nota fiscal de serviço.');
            //redirect(base_url());
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfse/gerenciar/';
        $config['total_rows'] = $this->nfse_model->count('nfse');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->nfse_model->get('nfse', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'nfse/nfse';
        $this->load->view('tema/topo', $this->data);
    }

    function adicionar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aNFSE')) {
           $this->session->set_flashdata('error', 'Você não tem permissão para adicionar nota fiscal de serviço.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('nfse') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $proximoNumeroRPS = $this->getProximoNumeroRPS();

            $data = array(
                'dataEmissao'   => $this->input->post('dataEmissao'),
                'clientes_id'   => $this->input->post('clientes_id'),
                'usuarios_id'   => $this->input->post('usuarios_id'),
                'numeroRps'     => $proximoNumeroRPS,
                'serieRps'      => $this->input->post('serieRps'),
                'nomeClienteNota'   => $this->input->post('nomeClienteNota'),
                'cpfCnpjCliente'   => $this->input->post('cpfCnpjCliente'),
                'inscricaoEstadualCliente'   => $this->input->post('inscricaoEstadualCliente'),
                'inscricaoMunicialCliente'   => $this->input->post('inscricaoMunicialCliente'),
                'enderecoCliente'   => $this->input->post('enderecoCliente'),
                'numeroCliente'   => $this->input->post('numeroCliente'),
                'complementoCliente'   => $this->input->post('complementoCliente'),
                'cepCliente'   => $this->input->post('cepCliente'),
                'bairroCliente'   => $this->input->post('bairroCliente'),
                'cidadeCliente'   => $this->input->post('cidadeCliente'),
                'ufCliente'   => $this->input->post('ufCliente'),
                'telefoneCliente'   => $this->input->post('telefoneCliente'),
                'emailCliente'   => $this->input->post('emailCliente'),
                'comissaoVendedor'   => $this->input->post('comissaoVendedor'),
                'valorComissaoVendedor'   => $this->input->post('valorComissaoVendedor'),
                'codIBGECidade'   => $this->input->post('codIBGECidade'),
                'faturado'      => 0,
                'situacao'      => 1
            );

            if (is_numeric($id = $this->nfse_model->add('nfse', $data, true))) {
                $this->session->set_flashdata('success', 'Nota Fiscal de serviço cadastrada com sucesso, adicione as informações do serviço.');
                redirect('nfse/editar/' . $id);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['emitente']         = $this->mapos_model->getEmitente();
        $this->data['proximoNumeroRPS'] = $this->getProximoNumeroRPS();
        $this->data['proximoNumeroNFSE']= $this->getProximoNumeroNFSE();
        $this->data['clientes']         = $this->clientes_model->getAll();
        $this->data['usuarios']         = $this->usuarios_model->getAll();
        $this->data['view']             = 'nfse/adicionarNFSE';
        $this->load->view('tema/topo', $this->data);
    }


    private function getProximoNumeroRPS() {
        return $this->nfse_model->getProximoNumeroRPS();
    }

    private function getProximoNumeroLote() {
        return $this->nfse_model->getProximoNumeroLote();
    }

    private function getProximoNumeroNFSE() {
        return $this->nfse_model->getProximoNumeroNFSE();
    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eNFSE')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para editar nota fiscal de serviço');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('nfse') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'dataEmissao'   => $this->input->post('dataEmissao'),
                'clientes_id'   => $this->input->post('clientes_id'),
                'usuarios_id'   => $this->input->post('usuarios_id'),
                'nomeClienteNota'   => $this->input->post('nomeClienteNota'),
                'cpfCnpjCliente'   => $this->input->post('cpfCnpjCliente'),
                'inscricaoEstadualCliente'   => $this->input->post('inscricaoEstadualCliente'),
                'inscricaoMunicialCliente'   => $this->input->post('inscricaoMunicialCliente'),
                'enderecoCliente'   => $this->input->post('enderecoCliente'),
                'numeroCliente'   => $this->input->post('numeroCliente'),
                'complementoCliente'   => $this->input->post('complementoCliente'),
                'cepCliente'   => $this->input->post('cepCliente'),
                'bairroCliente'   => $this->input->post('bairroCliente'),
                'cidadeCliente'   => $this->input->post('cidadeCliente'),
                'ufCliente'   => $this->input->post('ufCliente'),
                'telefoneCliente'   => $this->input->post('telefoneCliente'),
                'emailCliente'   => $this->input->post('emailCliente'),
                'comissaoVendedor'   => $this->input->post('comissaoVendedor'),
                'valorComissaoVendedor'   => $this->input->post('valorComissaoVendedor'),
                'descricaoServico'   => $this->input->post('descricaoServico'),
                'valorServico'   => $this->input->post('valorServico'),
                'servicoTributado'   => $this->input->post('servicoTributado'),
                'codigoServicoTributado'   => $this->input->post('codigoServicoTributado'),
                'cnaeServicoTributado'   => $this->input->post('cnaeServicoTributado'),
                'percentualIR'   => $this->input->post('percentualIR'),
                'valorIR'   => $this->input->post('valorIR'),
                'percentualISS'   => $this->input->post('percentualISS'),
                'valorISS'   => $this->input->post('valorISS'),
                'reterISS'   => $this->input->post('reterISS'),
                'descISSTotalNota'   => $this->input->post('descISSTotalNota'),
                'reterINSS'   => $this->input->post('reterINSS'),
                'reterCsllPisCofins'   => $this->input->post('reterCsllPisCofins'),
                'valorINSS'   => $this->input->post('valorINSS'),
                'percentualCofins'   => $this->input->post('percentualCofins'),
                'valorCofins'   => $this->input->post('valorCofins'),
                'percentualPIS'   => $this->input->post('percentualPIS'),
                'valorPIS'   => $this->input->post('valorPIS'),
                'percentualContribuicaoSocial'   => $this->input->post('percentualContribuicaoSocial'),
                'valorContribuicaoSocial'   => $this->input->post('valorContribuicaoSocial'),
                'valorServicos'   => $this->input->post('valorServicos'),
                'baseCalculo'   => $this->input->post('baseCalculo'),
                'desconto'   => $this->input->post('desconto'),
                'valorNota'   => $this->input->post('valorNota'),
                'textoIR'   => $this->input->post('textoIR'),
                'condicoes'   => $this->input->post('condicoes'),
                'codIBGECidade'   => $this->input->post('codIBGECidade'),
            );

            if ($this->nfse_model->edit('nfse', $data, 'idNFSE', $this->input->post('idNFSE')) == TRUE) {
                $this->session->set_flashdata('success', 'Venda editada com sucesso!');
                redirect(base_url() . 'index.php/nfse');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['emitente']     = $this->mapos_model->getEmitente();
        $this->data['clientes']     = $this->clientes_model->getAll();
        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['result']       = $this->nfse_model->getById($this->uri->segment(3));
        $this->data['servicosISS']  = $this->nfse_model->getServicosISS();
        $this->data['view']         = 'nfse/editarNFSE';
        $this->load->view('tema/topo', $this->data);
    }

    function transmitir()
    {
        $numeroRps  = $this->getProximoNumeroRPS();
        $numeroLote = $this->getProximoNumeroLote();
        $nfse_id    = $this->input->post('nfse_id');

        $data = array(
            'numeroRps'  => $numeroRps,
            'numeroLote' => $numeroLote
        );
        $this->nfse_model->edit('nfse', $data, 'idNFSE',$nfse_id);
        $nfse = $this->nfse_model->getById($nfse_id);

        if ($nfse->situacao == 3) {
             $this->descarta($nfse->numeroRps, $nfse->serieRps);
        } else {
            $nfseRejeitada = $this->nfse_model->getByNumeroRps($nfse->numeroRps);
            if ($nfseRejeitada->situacao == 3) {
                $this->descarta($nfse->numeroRps, $nfse->serieRps);
            }
        }

        $this->tecnospeednotafiscalservico_model->trasmitir($nfse_id);
        echo json_encode($this->nfse_model->getById($nfse_id));
    }

    function excluir($idNFSE) {
        $this->nfse_model->delete('nfse', 'idNFSE', $idNFSE);
        $this->session->set_flashdata('success', 'OS excluída com sucesso!');
        redirect(base_url() . 'index.php/nfse/nfse');
    }

    function descarta($numeroRps, $serie) {
        $this->tecnospeednotafiscalservico_model->descarta($numeroRps, $serie);
    }

    function imprimir($numeroNFSe) {
        echo $this->tecnospeednotafiscalservico_model->imprimir($numeroNFSe);
    }

    function cancelar() {
        $numeroNFSe = $this->input->post('numeroNFSe');
        $idNFSE     = $this->input->post('idNFSE');
        $this->tecnospeednotafiscalservico_model->cancelar($numeroNFSe, $idNFSE);
        echo json_encode($this->nfse_model->getById($idNFSE));
    }

    function gerarTxt()
    {
        $this->tecnospeednotafiscalservico_model->gerarTxt(2);
    }

    function exporta()
    {
        $this->tecnospeednotafiscalservico_model->exporta();
    }


    function calcularTotais() {

        $emitente           = $this->mapos_model->getEmitente();

        $valorServico       = $this->input->post('valorServico');
        $percentualISS      = $this->input->post('percentualISS');
        $descISSTotalNota   = $this->input->post('descISSTotalNota');
        $reterINSS          = $this->input->post('reterINSS');
        $desconto           = $this->input->post('desconto');
        $reterCsllPisCofins = $this->input->post('reterCsllPisCofins');

        //calcular valorServicos
        $valorServicos = $valorServico;
        $baseCalculo   = $valorServico - $desconto;

        //caluclar valorNota
        $valorNota = $baseCalculo;

        //calcular ISS
        $valorISS       = $baseCalculo * ($percentualISS/100);

        if ($descISSTotalNota == 'S') {
            $valorNota = ($valorNota - $valorISS);
        }

        //calcular IR
        $descontarIRValor       = $emitente->descontarIRValor;
        $valorMinimoCalculoIR   = $emitente->valorMinimoCalculoIR;
        $percentualIR           = $emitente->aliquotaIR;
        $valorIR                = $baseCalculo * ($percentualIR/100);

        if ($descontarIRValor == 1 && $baseCalculo >= $valorMinimoCalculoIR) {
            $valorNota = ($valorNota - $valorIR);
        } else {
            $percentualIR = 0;
            $valorIR = 0;
        }

        //calcular inss
        $aliquotaINSS   = 0;
        $valorINSS      = 0;
        if ($reterINSS == 'S') {
            $aliquotaINSS   = $emitente->aliquotaINSS;
            $valorINSS      = $baseCalculo * ($aliquotaINSS/100);
            $valorNota      = ($valorNota-$valorINSS);
        }

        //calcular CONFINS PIS
        $reterCSLLPISCONFINSNotaAcima5000       = $emitente->reterCSLLPISCONFINSNotaAcima5000;
        $reterCSLLPISCONFINSSomaImpostosAcima   = $emitente->reterCSLLPISCONFINSSomaImpostosAcima;
        $valorCofins = 0;
        $valorPIS = 0;
        $valorContribuicaoSocial = 0;
        $isReterCsllPisCofins = 0;

        if($reterCsllPisCofins == 'S' ||
            ( $reterCSLLPISCONFINSNotaAcima5000 == 1  && $baseCalculo >= 5000) ||
            $baseCalculo >= $reterCSLLPISCONFINSSomaImpostosAcima) {
            $percentualCofins = $emitente->aliquotaCofins;
            $valorCofins = $baseCalculo * ($percentualCofins / 100);
            $valorNota = ($valorNota - $valorCofins);

            $percentualPIS = $emitente->aliquotaPIS;
            $valorPIS = $baseCalculo * ($percentualPIS / 100);
            $valorNota = ($valorNota - $valorPIS);

            $percentualContribuicaoSocial = $emitente->aliquotaContribuicaoSocial;
            $valorContribuicaoSocial = $baseCalculo * ($percentualContribuicaoSocial/100);
            $valorNota = ($valorNota - $valorContribuicaoSocial);

            $isReterCsllPisCofins = 1;
        } else {
            $percentualPIS = 0;
            $percentualCofins = 0;
            $percentualContribuicaoSocial = 0;
        }

        echo json_encode(
            array(
                'valorServicos' => $valorServicos,

                'valorISS'      =>$valorISS,

                'percentualINSS'=> $aliquotaINSS,
                'valorINSS'     => $valorINSS,

                'percentualIR'  => $percentualIR,
                'valorIR'       =>  $valorIR,

                'percentualCofins' => $percentualCofins,
                'valorCofins' => $valorCofins,

                'percentualPIS' => $percentualPIS,
                'valorPIS' => $valorPIS,

                'percentualContribuicaoSocial' => $percentualContribuicaoSocial,
                'valorContribuicaoSocial' => $valorContribuicaoSocial,

                'baseCalculo'   => $baseCalculo,
                'valorNota'     => $valorNota,

                'isReterCsllPisCofins' => $isReterCsllPisCofins
            )
        );
    }

    function iframeNFSOS($os_id) {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vNFE')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar nota fiscal de serviço.');
            //redirect(base_url());
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/nfe/gerenciar/';
        $config['total_rows'] = $this->nfse_model->count('nfe');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->nfse_model->getNFEByOS($os_id);
        $this->data['view'] = 'nfse/nfse';
        $this->load->view('tema/blank', $this->data);
    }
}

