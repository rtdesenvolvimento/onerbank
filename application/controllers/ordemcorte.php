<?php

class ordemcorte extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'codegen_helper'));
    }

    function index(){}

    function venda($idOrcamento, $base) {

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('
        orcamento.numeropedidocliente,
        orcamento.codigo as codigoorcamento,
        orcamento.contato as contato,
        orcamento.dataorcamento as dataorcamento,
        orcamento.codigo as codigoorcameto,
        orcamento.observacao,
        orcamento.percenticms,
        orcamento.prazoentrega,
        orcamento.valordesconto,
        orcamento.frete,
        orcamento.percentipi,
        orcamento.valortotal,
        pessoa.descricao as nomecliente,
        pessoa.telefone as telefone,
        pessoa.cnpjcpf,
        pessoa.inscricaoestadual,
        orcamentoproduto.codigo as codigo,
        orcamentoproduto.largura, 
        orcamentoproduto.altura,
        orcamentoproduto.quantidade,
        orcamentoproduto.posicao,
        orcamentoproduto.os,
        orcamentoproduto.eletrodo,
        orcamentoproduto.pesokg,
        orcamentoproduto.profundidade,
        orcamentoproduto.precounitario,
        orcamentoproduto.total as subtotal,
        produto.codigo as cdproduto,
        produto.descricao as descricaoproduto,
        condicaopagamento.descricao as condicaopagamento');
        $otherdb->where('orcamento.id', $idOrcamento);
        $otherdb->from('orcamentoproduto');
        $otherdb->join('orcamento', 'orcamento.id = orcamentoproduto.orcamento', 'left');
        $otherdb->join('produto', 'produto.id = orcamentoproduto.produto', 'left');
        $otherdb->join('pessoa', 'pessoa.id = orcamento.cliente', 'left');
        $otherdb->join('condicaopagamento', 'condicaopagamento.id = orcamento.condicaopagamento', 'left');
        $otherdb->order_by('orcamentoproduto.id', 'asc');

        $data['dados'] =  $otherdb->get()->result();;
        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/orcamentovenda', $data, true);

        echo pdf_create($html, 'orcamento_venda' . date('d-m-y').'_'.$idOrcamento, true, 'A4-L');
    }

    function corte($idOrcamento, $base) {

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('
        orcamento.numeropedidocliente,
        orcamento.codigo as codigoorcamento,
        orcamento.contato as contato,
        orcamento.dataorcamento as dataorcamento,
        orcamento.codigo as codigoorcameto,
        pessoa.descricao as nomecliente,
        pessoa.telefone as telefone,
        orcamentoproduto.codigo as codigo,
        orcamentoproduto.largura, 
        orcamentoproduto.altura,
        orcamentoproduto.quantidade,
        orcamentoproduto.posicao,
        orcamentoproduto.eletrodo,
        orcamentoproduto.os,
        orcamentoproduto.pesokg,
        orcamentoproduto.profundidade,produto.codigo as cdproduto ');
        $otherdb->where('orcamento.id', $idOrcamento);
        $otherdb->from('orcamentoproduto');
        $otherdb->join('orcamento', 'orcamento.id = orcamentoproduto.orcamento');
        $otherdb->join('produto', 'produto.id = orcamentoproduto.produto');
        $otherdb->join('pessoa', 'pessoa.id = orcamento.cliente');
        $otherdb->order_by('orcamentoproduto.id', 'asc');

        $data['dados'] =  $otherdb->get()->result();;
        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/ordemcorte', $data, true);

        echo pdf_create($html, 'ordem_corte' . date('d-m-y').'_'.$idOrcamento, true, 'A4-L');

    }

}

