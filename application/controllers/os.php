<?php

class Os extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('os_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('setor_model', '', TRUE);
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('equipamentoveiculo_model', '', TRUE);
        $this->load->model('acessorio_model', '', TRUE);
        $this->load->model('tipoavaria_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('forma_pagamento_model', '', TRUE);
        $this->load->model('tipoveiculo_model', '', TRUE);
        $this->load->model('nfe_model', '', TRUE);
        $this->load->model('nfse_model', '', TRUE);

        $this->data['menuOs'] = 'OS';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {
        $this->load->library('pagination');

        $where_array = array();

        $id_os = $this->input->get('id_os');
        $pesquisa = $this->input->get('pesquisa');
        $status = $this->input->get('status');
        $de = $this->input->get('data_inicio');
        $ate = $this->input->get('data_final');

        $this->data['pesquisa'] = $pesquisa;
        $this->data['status'] = $status;
        $this->data['data_inicio'] = $de;
        $this->data['data_final'] = $ate;
        $this->data['id_os'] = $id_os;

        if ($id_os) {
            $where_array['id_os'] = $id_os;
        }

        if ($pesquisa) {
            $where_array['pesquisa'] = $pesquisa;
        }

        if ($status) {
            $where_array['status'] = $status;
        }

        if ($de) {
            $de = explode('/', $de);
            $de = $de[2] . '-' . $de[1] . '-' . $de[0];
            $where_array['de'] = $de;
        }

        if ($ate) {
            $ate = explode('/', $ate);
            $ate = $ate[2] . '-' . $ate[1] . '-' . $ate[0];
            $where_array['ate'] = $ate;
        }

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->os_model->getOs('os', 'idOs,dataInicial,veiculo_id,dataFinal,valorTotal, os.marca, os.modelo, numero_serie ,garantia,valorTotal,descricaoProduto,defeito,status,observacoes,laudoTecnico', $where_array, $config['per_page'], $this->uri->segment(3));
        $this->data['view'] = 'os/os';
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('tema/topo', $this->data);
    }


    function osProducao()
    {
        $this->load->library('pagination');

        $where_array = array();

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $where_array['produzir'] = 1;
        $where_array['bloqueadasetor'] = 0;
        $where_array['producaopausada'] = 0;

        $this->pagination->initialize($config);

        $this->data['results'] = $this->os_model->getOs('os', 'idOs,dataInicial,veiculo_id,dataFinal,valorTotal, os.marca, os.modelo, numero_serie ,garantia,valorTotal,descricaoProduto,defeito,status,observacoes,laudoTecnico', $where_array, $config['per_page'], $this->uri->segment(3));
        $this->data['view'] = 'os/osProducao';
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('tema/topo', $this->data);
    }

    function osSetor()
    {
        $this->load->library('pagination');
        $where_array = array();

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $setor_id = $this->session->userdata('setor_id');

        $where_array['produzir'] = 1;
        $where_array['bloqueadasetor'] = 1;
        $where_array['setor'] = $setor_id;

        $nome_setor = '';
        if ($setor_id) {
            $setor = $this->db->get_where('setor', array('idSetor' => $setor_id))->row();
            if (count($setor)) {
                $nome_setor = $setor->nome;
            }
        }

        $this->data['results'] = $this->os_model->getOs('os', 'idOs,dataInicial,dataFinal,producaopausada,valorTotal,veiculo_id, os.marca, os.modelo, numero_serie ,garantia,valorTotal,descricaoProduto,defeito,status,observacoes,laudoTecnico', $where_array, $config['per_page'], $this->uri->segment(3));
        $this->data['view'] = 'os/osSetor';
        $this->data['nome_setor'] = $nome_setor;
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('tema/topo', $this->data);
    }

    function osSetorEncaminhar()
    {

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $setor_id = $this->session->userdata('setor_id');

        $nome_setor = '';
        if ($setor_id) {
            $setor = $this->db->get_where('setor', array('idSetor' => $setor_id))->row();
            if (count($setor)) {
                $nome_setor = $setor->nome;
            }
        }

        $this->pagination->initialize($config);
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['nome_setor'] = $nome_setor;
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['produtos'] = $this->os_model->getProdutos($this->uri->segment(3), null, $setor_id);
        $this->data['servicos'] = $this->os_model->getServicos($this->uri->segment(3), null, $setor_id);

        $this->data['view'] = 'os/osSetorEncaminhar';
        $this->load->view('tema/topo', $this->data);
    }

    function buscar()
    {
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $parametro = $this->uri->segment(3);
        $parametro = str_replace("%20", "%", $parametro);

        $where = '';
        if ($parametro) {
            $where = 'clientes_id in ( select idClientes from clientes 
										where 
											nomeCliente LIKE "%' . $parametro . '%" 
											OR telefone LIKE "%' . $parametro . '%" 
											OR celular LIKE "%' . $parametro . '%" 
											OR email LIKE "%' . $parametro . '%" 
									)';
            $where = $where . ' OR status LIKE "%' . $parametro . '%" ';
        }

        $this->data['results'] = $this->os_model->get('os', 'idOs,dataInicial,valorTotal,marca, modelo, numero_serie,dataFinal,garantia,descricaoProduto,defeito,status,observacoes,laudoTecnico', $where, $config['per_page'], '');

        $this->data['view'] = 'os/os';
        $this->load->view('tema/topo', $this->data);
    }

    function adicionar()
    {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('os') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $dataInicial    = $this->input->post('dataInicial');
            $dataFinal      = $this->input->post('dataFinal');

            $kilometragementrada = 0;
            $kilometragemsaida = 0;

            if ($this->input->post('kilometragementrada')) {
                $kilometragementrada = $this->input->post('kilometragementrada');
            }

            if ($this->input->post('kilometragemsaida')) {
                $kilometragemsaida = $this->input->post('kilometragemsaida');
            }

            $aparelho_id = null;
            if ($this->input->post('aparelho_id')) {
                $aparelho_id = $this->input->post('aparelho_id');
            }

            $data = array(
                'dataInicial' => $dataInicial,
                'dataFinal' => $dataFinal,
                'gnv'                   => $this->input->post('gnv'),
                'tanque'                => $this->input->post('tanque'),
                'clientes_id' => $this->input->post('clientes_id'),
                'usuarios_id' => $this->input->post('usuarios_id'),
                'garantia' => set_value('garantia'),
                'kilometragementrada' => $kilometragementrada,
                'kilometragemsaida' => $kilometragemsaida,
                'veiculo_id' => $this->input->post('veiculo_id'),
                'descricaoProduto' => set_value('descricaoProduto'),
                'defeito' => set_value('defeito'),
                'status' => set_value('status'),
                'aparelho_id' => $aparelho_id,
                'marca' => $this->input->post('marca'),
                'modelo' => $this->input->post('modelo'),
                'numero_serie' => $this->input->post('numero_serie'),
                'acessorios' => $this->input->post('acessorios'),
                'observacoes' => set_value('observacoes'),
                'laudoTecnico' => set_value('laudoTecnico'),
                'faturado' => 0
            );

            if (is_numeric($id = $this->os_model->add('os', $data, true))) {

                $setores = $this->setor_model->getSetorAll();
                $contador = 0;

                foreach ($setores as $setor) {
                    $ordemProducao = array(
                        'os_id' => $id,
                        'setor_id' => $setor->idSetor,
                        'ordem' => $contador,
                    );
                    $idOrdemProducao = $this->os_model->add('ordemproducao', $ordemProducao, true);
                    $contador = $contador + 1;

                    //FUNCAO PARA MULTIPLICAR A PECA POR SETOR - FUTURAS ATUALIZACOES
                    // $this->adicionarServicoAoAdicionar($id, $setor->idSetor, $idOrdemProducao);
                    // $this->adicionarProdutoAoAdicionar($id, $setor->idSetor, $idOrdemProducao);
                }

                $produtos = $this->os_model->getProdutos($id);
                $servicos = $this->os_model->getServicos($id);

                $totalSubTotal = 0;
                $totalCusto = 0;

                foreach ($produtos as $p) {
                    $totalSubTotal = $totalSubTotal + $p->subTotal;
                    $totalCusto = $totalCusto + $p->precoCusto;
                }

                foreach ($servicos as $s) {
                    $totalSubTotal = $totalSubTotal + $s->subTotal;;
                    $totalCusto = $totalCusto + $s->precoCustoServico;
                }

                $this->data['totalSubTotal'] = $totalSubTotal;
                $this->data['totalCusto'] = $totalCusto;
                $this->data['totalLucro'] = ($totalSubTotal - $totalCusto);

                $data_os = array(
                    'valorTotal' => $totalSubTotal
                );

                //atualizar o valor da os ao editar
                $this->os_model->edit('os', $data_os, 'idOs', $id);

                $this->session->set_flashdata('success', 'OS adicionada com sucesso, você pode adicionar produtos ou serviços a essa OS nas abas de "Produtos" e "Serviços"!');
                redirect('os/editar/' . $id);

            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['clientes']     = $this->clientes_model->getAll();
        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['emitente']     = $this->mapos_model->getEmitente();
        $this->data['aparelhos']    = $this->os_model->getAparelhos();
        $this->data['tiposveiculo'] = $this->tipoveiculo_model->getAll();

        $this->data['view'] = 'os/adicionarOs';
        $this->load->view('tema/topo', $this->data);
    }

    public function adicionarAjax()
    {
        $this->load->library('form_validation');

        if ($this->form_validation->run('os') == false) {
            $json = array("result" => false);
            echo json_encode($json);
        } else {

            $aparelho_id = null;
            if ($this->input->post('aparelho_id')) {
                $aparelho_id = $this->input->post('aparelho_id');
            }

            $data = array(
                'dataInicial' => set_value('dataInicial'),
                'clientes_id' => $this->input->post('clientes_id'),//set_value('idCliente'),
                'usuarios_id' => $this->input->post('usuarios_id'),//set_value('idUsuario'),
                'dataFinal' => set_value('dataFinal'),
                'garantia' => set_value('garantia'),
                'gnv'                   => $this->input->post('gnv'),
                'tanque'                => $this->input->post('tanque'),
                'kilometragementrada' => set_value('kilometragementrada'),
                'kilometragemsaida' => set_value('kilometragemsaida'),
                'descricaoProduto' => set_value('descricaoProduto'),
                'veiculo_id' => $this->input->post('veiculo_id'),
                'aparelho_id' => $aparelho_id,
                'marca' => $this->input->post('marca'),
                'modelo' => $this->input->post('modelo'),
                'numero_serie' => $this->input->post('numero_serie'),
                'acessorios' => $this->input->post('acessorios'),
                'defeito' => set_value('defeito'),
                'status' => set_value('status'),
                'observacoes' => set_value('observacoes'),
                'laudoTecnico' => set_value('laudoTecnico')
            );

            if (is_numeric($id = $this->os_model->add('os', $data, true))) {
                $json = array("result" => true, "id" => $id);
                echo json_encode($json);

            } else {
                $json = array("result" => false);
                echo json_encode($json);
            }
        }
    }

    function editar()
    {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('os') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $dataInicial = $this->input->post('dataInicial');
            $dataFinal = $this->input->post('dataFinal');

            if ($this->input->post('status') == 'Retirado') {
                if ($this->input->post('status')) {
                    $dataFinal = date('Y/m/d');
                }
            }

            $kilometragementrada = 0;
            $kilometragemsaida = 0;

            if ($this->input->post('kilometragementrada')) {
                $kilometragementrada = $this->input->post('kilometragementrada');
            }

            if ($this->input->post('kilometragemsaida')) {
                $kilometragemsaida = $this->input->post('kilometragemsaida');
            }

            $veiculo = 0;
            if ($this->input->post('veiculo_id')) {
                $veiculo = $this->input->post('veiculo_id');
            }

            $aparelho_id = null;
            if ($this->input->post('aparelho_id')) {
                $aparelho_id = $this->input->post('aparelho_id');
            }

            $data = array(
                'dataInicial' => $dataInicial,
                'dataFinal' => $dataFinal,
                'garantia' => $this->input->post('garantia'),
                'descricaoProduto' => $this->input->post('descricaoProduto'),
                'defeito' => $this->input->post('defeito'),
                'status' => $this->input->post('status'),
                'gnv'                   => $this->input->post('gnv'),
                'tanque'                => $this->input->post('tanque'),
                'observacoes' => $this->input->post('observacoes'),
                'kilometragementrada' => $kilometragementrada,
                'kilometragemsaida' => $kilometragemsaida,
                'veiculo_id' => $veiculo,
                'aparelho_id' => $aparelho_id,
                'marca' => $this->input->post('marca'),
                'modelo' => $this->input->post('modelo'),
                'numero_serie' => $this->input->post('numero_serie'),
                'acessorios' => $this->input->post('acessorios'),
                'laudoTecnico' => $this->input->post('laudoTecnico'),
                'usuarios_id' => $this->input->post('usuarios_id'),
                'clientes_id' => $this->input->post('clientes_id')
            );

            if ($this->os_model->edit('os', $data, 'idOs', $this->input->post('idOs')) == TRUE) {
                $this->session->set_flashdata('success', 'Os editada com sucesso!');
                redirect(base_url() . 'index.php/os/editar/' . $this->input->post('idOs'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['visaotopo1']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(1);
        $this->data['visaotopo2']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(2);
        $this->data['visaotopo3']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(3);
        $this->data['visaotopo4']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(4);
        $this->data['visaotopo5']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(5);

        $this->data['acessorios1']       = $this->acessorio_model->getAllByLimit(1, 7);
        $this->data['acessorios2']       = $this->acessorio_model->getAllByLimit(8, 7);
        $this->data['acessorios3']       = $this->acessorio_model->getAllByLimit(15, 7);
        $this->data['acessorios4']       = $this->acessorio_model->getAllByLimit(26, 10);

        $this->data['tipoavarias']      = $this->tipoavaria_model->getAll();

        $this->data['formasPagamento']  = $this->forma_pagamento_model->getAll();
        $this->data['clientes']         = $this->clientes_model->getAll();
        $this->data['usuarios']         = $this->usuarios_model->getAll();
        $this->data['produtosList']     = $this->produtos_model->getAll();
        $this->data['servicosList']     = $this->servicos_model->getAll();
        $this->data['emitente']         = $this->mapos_model->getEmitente();
        $this->data['aparelhos']        = $this->os_model->getAparelhos();
        $this->data['result']           = $this->os_model->getById($this->uri->segment(3));
        $this->data['produtos']         = $this->os_model->getProdutos($this->uri->segment(3));
        $this->data['servicos']         = $this->os_model->getServicos($this->uri->segment(3));
        $this->data['setores']          = $this->setor_model->getSetorAll();
        $this->data['tiposveiculo']     = $this->tipoveiculo_model->getAll();

        $totalSubTotal  = 0;
        $totalCusto     = 0;

        foreach ($this->data['produtos'] as $p) {
            $totalSubTotal = $totalSubTotal + $p->subTotal;
            $totalCusto = $totalCusto + $p->precoCusto;
        }

        foreach ($this->data['servicos'] as $s) {
            $totalSubTotal = $totalSubTotal + $s->subTotal;;
            $totalCusto = $totalCusto + $s->precoCustoServico;
        }

        $this->data['totalSubTotal'] = $totalSubTotal;
        $this->data['totalCusto'] = $totalCusto;
        $this->data['totalLucro'] = ($totalSubTotal - $totalCusto);

        $data_os = array(
            'valorTotal' => $totalSubTotal
        );

        //atualizar o valor da os ao editar
        $this->os_model->edit('os', $data_os, 'idOs', $this->uri->segment(3));

        $this->data['anexos']   = $this->os_model->getAnexos($this->uri->segment(3));
        $this->data['veiculos'] = $this->os_model->getVeiculos($this->data['result']->clientes_id);
        $this->data['vistoria'] = $this->os_model->getVistoriaOS($this->uri->segment(3));
        $this->data['view']     = 'os/editarOs';
        $this->load->view('tema/topo', $this->data);
    }

    public function visualizar()
    {
        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->os_model->getProdutosGroupByProdutos($this->uri->segment(3));
        $this->data['servicos'] = $this->os_model->getServicosGroupByServicos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo_id);

        $this->data['view'] = 'os/visualizarOs';
        $this->load->view('tema/topo', $this->data);
    }

    public function visualizarOSImpressao()
    {

        $this->load->helper('mpdf');
        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->os_model->getProdutosGroupByProdutos($this->uri->segment(3));
        $this->data['servicos'] = $this->os_model->getServicosGroupByServicos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo_id);

        $this->data['view'] = 'os/visualizarOSImpressao';


        $html = $this->load->view('os/visualizarOSImpressao', $this->data, true);

        pdf_create($html, 'impressao_os_' . $this->uri->segment(3) . date('d/m/y'), TRUE);

    }

    public function visualizarSetor()
    {

        $setor_id = $this->session->userdata('setor_id');

        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->os_model->getProdutos($this->uri->segment(3), null, $setor_id);
        $this->data['servicos'] = $this->os_model->getServicos($this->uri->segment(3), null, $setor_id);
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo);

        $this->data['view'] = 'os/visualizarSetor';
        $this->load->view('tema/topo', $this->data);
    }

    public function produzirOS()
    {

        $data_os = array(
            'status' => 'Em Produção',
            'produzir' => TRUE,
            'producaopausada' => FALSE,
        );

        //atualizar o valor da os ao editar
        $this->os_model->edit('os', $data_os, 'idOs', $this->uri->segment(3));
        redirect(base_url() . 'index.php/os/editar/' . $this->uri->segment(3));
    }

    public function pausarProducaoOS()
    {
        $data_os = array(
            'status' => 'Produção Pausada',
            'produzir' => TRUE,
            'producaopausada' => TRUE,
        );

        //atualizar o valor da os ao editar
        $this->os_model->edit('os', $data_os, 'idOs', $this->uri->segment(3));

        redirect(base_url() . 'index.php/os/editar/' . $this->uri->segment(3));
    }

    public function aceitar_setor()
    {

        $setor_id = $this->session->userdata('setor_id');

        $data_ordemproducao = array(
            'produzido' => TRUE,
            'data_produzido' => NULL,
            'hora_produzido' => NULL,
        );
        $this->os_model->edit_ordemproducao($data_ordemproducao, $this->uri->segment(3), $setor_id);

        $data_os = array(
            'setorAtual_id' => $setor_id,
            'bloqueadasetor' => TRUE,
        );

        //atualizar o valor da os ao editar
        $this->os_model->edit('os', $data_os, 'idOs', $this->uri->segment(3));

        redirect(base_url() . 'index.php/os/osSetor');
    }


    public function encaminhar_proximo_setor()
    {

        date_default_timezone_set('America/Sao_Paulo');
        $setor_id = $this->session->userdata('setor_id');

        $data = date('Y-m-d');
        $hora = date('H:i:s');

        $data_ordemproducao = array(
            'produzido' => TRUE,
            'data_produzido' => $data,
            'hora_produzido' => $hora,
        );
        $this->os_model->edit_ordemproducao($data_ordemproducao, $this->uri->segment(3), $setor_id);

        $data_os = array(
            'bloqueadasetor' => FALSE,
        );

        //atualizar o valor da os ao editar
        $this->os_model->edit('os', $data_os, 'idOs', $this->uri->segment(3));
        redirect(base_url() . 'index.php/os/osSetor');
    }

    public function iniciar_produzir($idProdutoServicoOS,$tipo, $idOs) {

        $usuario_id = $this->session->userdata('id');
        date_default_timezone_set('America/Sao_Paulo');
        $usuario    = $this->usuarios_model->getById($usuario_id);

        $data = date('Y-m-d');
        $hora = date('H:i:s');


        if ($tipo == 'produto') {

            $produtos_os    = $this->os_model->getProdutosOSById($idProdutoServicoOS);
            $comissao       = $usuario->comissao;
            $preco          = $produtos_os->precoVendaOS;
            $quantidade     = $produtos_os->quantidade;
            $valorComissao  =  (($preco * $quantidade) * $comissao / 100);

            $data_os = array(
                'produzido'                     => FALSE,
                'data_produzido'                => $data,
                'hora_produzido'                => $hora,
                'responsavel_id'                => $usuario_id,
                'comissao'                      => $comissao,
                'valorComissao'                 => $valorComissao,
                'data_produzido_conclusao'      => NULL,
                'hora_produzido_conclusao'      => NULL,
            );

            //atualizar o valor da os ao editar
            $this->os_model->edit('produtos_os', $data_os, 'idProdutos_os', $idProdutoServicoOS);
        } else {

            $servicos_os    = $this->os_model->getServicosOSById($idProdutoServicoOS);
            $comissao       = $usuario->comissao;
            $preco          = $servicos_os->precoVendaServico;
            $quantidade     = $servicos_os->quantidade;
            $valorComissao  =  (($preco * $quantidade) * $comissao / 100);

            $data_os = array(
                'produzido'         => FALSE,
                'data_produzido'    => $data,
                'hora_produzido'    => $hora,
                'responsavel_id'    => $usuario_id,
                'comissao'          => $comissao,
                'valorComissao'     => $valorComissao,
                'data_produzido_conclusao'      => NULL,
                'hora_produzido_conclusao'      => NULL,
            );

            //atualizar o valor da os ao editar
            $this->os_model->edit('servicos_os', $data_os, 'idServicos_os', $idProdutoServicoOS);
        }

        redirect(base_url() . 'index.php/os/osSetorEncaminhar/' . $idOs);

    }

    public function finaliza_produzir($idProdutoServicoOS,$tipo, $idOs) {

        date_default_timezone_set('America/Sao_Paulo');

        $data = date('Y-m-d');
        $hora = date('H:i:s');

        if ($tipo == 'produto') {

            $data_os = array(
                'produzido'                     => TRUE,
                'data_produzido_conclusao'      => $data,
                'hora_produzido_conclusao'      => $hora,
            );

            //atualizar o valor da os ao editar
            $this->os_model->edit('produtos_os', $data_os, 'idProdutos_os', $idProdutoServicoOS);
        } else {
            $data_os = array(
                'produzido'                     => TRUE,
                'data_produzido_conclusao'    => $data,
                'hora_produzido_conclusao'    => $hora,
            );

            //atualizar o valor da os ao editar
            $this->os_model->edit('servicos_os', $data_os, 'idServicos_os', $idProdutoServicoOS);
        }

        redirect(base_url() . 'index.php/os/osSetorEncaminhar/' . $idOs);
    }

    public function reiniciar_producao($idProdutoServicoOS,$tipo, $idOs) {

        if ($tipo == 'produto') {

            $data_os = array(
                'produzido'                     => FALSE,
                'data_produzido_conclusao'      => NULL,
                'hora_produzido_conclusao'      => NULL,
                'data_produzido'                => NULL,
                'hora_produzido'                => NULL,
                'responsavel_id'                => NULL,
                'comissao'                      => 0,
                'valorComissao'                 => 0,
            );

            //atualizar o valor da os ao editar
            $this->os_model->edit('produtos_os', $data_os, 'idProdutos_os', $idProdutoServicoOS);
        } else {
            $data_os = array(
                'produzido'                     => FALSE,
                'data_produzido_conclusao'      => NULL,
                'hora_produzido_conclusao'      => NULL,
                'data_produzido'                => NULL,
                'hora_produzido'                => NULL,
                'responsavel_id'                => NULL,
                'comissao'                      => 0,
                'valorComissao'                 => 0,
            );

            //atualizar o valor da os ao editar
            $this->os_model->edit('servicos_os', $data_os, 'idServicos_os', $idProdutoServicoOS);
        }

        redirect(base_url() . 'index.php/os/osSetorEncaminhar/' . $idOs);
    }

    public function produzir_pecas()
    {

        $produzir = count($_POST['produzir']);
        $usuario_id = $this->session->userdata('id');
        $idOs = $this->input->post('idOs');

        $usuario    = $this->usuarios_model->getById($usuario_id);
        $produtos   = $this->os_model->getProdutos($idOs, $usuario_id);
        $servicos   = $this->os_model->getServicos($idOs, $usuario_id);

        date_default_timezone_set('America/Sao_Paulo');

        $data = date('Y-m-d');
        $hora = date('H:i:s');

        foreach ($produtos as $produto) {
            $data_os = array(
                'produzido'         => FALSE,
                'comissao'          => 0,
                'valorComissao'     => 0,
                'responsavel_id'    => NULL
            );
            $this->os_model->edit('produtos_os', $data_os, 'idProdutos_os', $produto->idProdutos_os);
        }

        foreach ($servicos as $servico) {
            $data_os = array(
                'produzido'         => FALSE,
                'responsavel_id'    => NULL,
                'comissao'          => 0,
                'valorComissao'     => 0
            );
            $this->os_model->edit('servicos_os', $data_os, 'idServicos_os', $servico->idServicos_os);
        }

        for ($i = 0; $i < $produzir; $i++) {
            if ($_POST['produzir'][$i]) {

                $idProdutoServicoOS = $_POST['produzir'][$i];
                $idProdutoServicoOSArray = explode("_", $idProdutoServicoOS);

                $tipo               = $idProdutoServicoOSArray[0];
                $idProdutoServicoOS = $idProdutoServicoOSArray[1];

                if ($tipo == 'produto') {

                    $produtos_os    = $this->os_model->getProdutosOSById($idProdutoServicoOS);
                    $comissao       = $usuario->comissao;
                    $preco          = $produtos_os->precoVendaOS;
                    $quantidade     = $produtos_os->quantidade;
                    $valorComissao  =  (($preco * $quantidade) * $comissao / 100);

                    $data_os = array(
                        'produzido'         => TRUE,
                        'data_produzido'    => $data,
                        'hora_produzido'    => $hora,
                        'responsavel_id'    => $usuario_id,
                        'comissao'          => $comissao,
                        'valorComissao'     => $valorComissao,
                    );

                    //atualizar o valor da os ao editar
                    $this->os_model->edit('produtos_os', $data_os, 'idProdutos_os', $idProdutoServicoOS);
                } else {

                    $servicos_os    = $this->os_model->getServicosOSById($idProdutoServicoOS);
                    $comissao       = $usuario->comissao;
                    $preco          = $servicos_os->precoVendaServico;
                    $quantidade     = $servicos_os->quantidade;
                    $valorComissao  =  (($preco * $quantidade) * $comissao / 100);

                    $data_os = array(
                        'produzido'         => TRUE,
                        'data_produzido'    => $data,
                        'hora_produzido'    => $hora,
                        'responsavel_id'    => $usuario_id,
                        'comissao'          => $comissao,
                        'valorComissao'     => $valorComissao,
                    );

                    //atualizar o valor da os ao editar
                    $this->os_model->edit('servicos_os', $data_os, 'idServicos_os', $idProdutoServicoOS);
                }
            }
        }
        redirect(base_url() . 'index.php/os/osSetorEncaminhar/' . $idOs);
    }

    public function imprimir_os()
    {

        $this->load->model('mapos_model');
        $this->load->helper('mpdf');

        $data['custom_error'] = '';
        $data['result'] = $this->os_model->getById($this->uri->segment(3));
        $data['produtos'] = $this->os_model->getProdutos($this->uri->segment(3));
        $data['servicos'] = $this->os_model->getServicos($this->uri->segment(3));
        $data['emitente'] = $this->mapos_model->getEmitente();
        $data['veiculos'] = $this->os_model->getVeiculosOS($data['result']->veiculo_id);

        $html = $this->load->view('os/impressaoOS', $data, true);

        pdf_create($html, 'impressao_os_' . $this->uri->segment(3) . date('d/m/y'), TRUE, 'A4-L');
    }

    public function visualizarRecibo()
    {
        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->os_model->getProdutos($this->uri->segment(3));
        $this->data['servicos'] = $this->os_model->getServicos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo_id);

        $this->data['view'] = 'os/recibo';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir()
    {

        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir OS.');
            redirect(base_url() . 'index.php/os/gerenciar/');
        }

        $this->db->where('os_id', $id);
        $this->db->delete('servicos_os');

        $this->db->where('os_id', $id);
        $this->db->delete('produtos_os');

        $this->db->where('os_id', $id);
        $this->db->delete('anexos');

        $this->os_model->delete('os', 'idOs', $id);


        $this->session->set_flashdata('success', 'OS excluída com sucesso!');
        redirect(base_url() . 'index.php/os/gerenciar/');
    }

    public function autoCompleteProduto()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteProduto($q);
        }
    }

    public function autoCompleteProdutoFilterSelect()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->os_model->autoCompleteProdutoFilterSelect($q);
            $this->send_json($rows);
        }
    }

    public function autoCompleteProdutoById($idProduto)
    {
        if ($idProduto) {
            $rows['results'] = $this->os_model->autoCompleteProdutoById($idProduto);
            $this->send_json($rows);
        }
    }

    public function autoCompleteClienteFilterSelect()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->os_model->autoCompleteClienteFilterSelect($q);
            $this->send_json($rows);
        }
    }

    public function autoCompleteClienteById($idCliente)
    {
        if ($idCliente) {
            $rows['results'] = $this->os_model->autoCompleteClienteById($idCliente);
            $this->send_json($rows);
        }
    }

    public function autoCompleteUsuarioById($idUsuario)
    {
        if ($idUsuario) {
            $rows['results'] = $this->os_model->autoCompleteUsuarioById($idUsuario);
            $this->send_json($rows);
        }
    }

    public function autoCompleteUsuarioFilterSelect()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->os_model->autoCompleteUsuarioFilterSelect($q);
            $this->send_json($rows);
        }
    }

    public function send_json($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    public function autoCompleteCliente()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->os_model->autoCompleteCliente($q);
            $this->sma->send_json($rows);
        }
    }

    public function autoCompleteUsuario()
    {

        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteUsuario($q);
        }

    }

    public function autoCompleteServico()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->os_model->autoCompleteServico($q);
        }
    }

    public function autoCompleteServicoFilterSelect()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->os_model->autoCompleteServicoFilterSelect($q);
            $this->send_json($rows);
        }
    }

    public function autoCompleteServicoById($idServico)
    {
        $rows['results'] = $this->os_model->autoCompleteServicoById($idServico);
        $this->send_json($rows);
    }

    public function consultaVeiculo()
    {
        $idCliente = $this->input->post('idCliente');
        echo json_encode($this->os_model->getVeiculos($idCliente));
    }

    public function adicionarProdutoAoAdicionar($idOsProduto = null, $setor_id = null, $idOrdemProducao = null)
    {
        $produtos = $this->produtos_model->getAllProdutos($setor_id);

        foreach ($produtos as $produto) {

            $preco = $produto->precoVenda;
            $precoCompra = $produto->precoCompra;
            $setor_id = $produto->setor_id;
            $grupoProduto_id = $produto->grupoProduto_id;
            $comissao = $produto->comissao;
            $produto_id = $produto->idProdutos;
            $quantidade = 1;

            $data = array(
                'quantidade' => $quantidade,
                'precoCusto' => $precoCompra,
                'precoVendaOS' => $preco,
                'subTotal' => $preco,
                'produtos_id' => $produto_id,
                'os_id' => $idOsProduto,
                'ordemproducao_id' => $idOrdemProducao,
                'setor_id' => $setor_id,
                'grupoProduto_id' => $grupoProduto_id,
                'comissao' => $comissao,
                'valorComissao' => (($preco * $quantidade) * $comissao / 100),
            );

            if ($this->os_model->add('produtos_os', $data) == true) {
                $sql = "UPDATE produtos set estoque = estoque - ? WHERE idProdutos = ?";
                $this->db->query($sql, array($quantidade, $produto_id));
            }
        }
    }

    public function adicionarProdutoUmAUm()
    {

        $quantidade = $this->input->post('quantidade');
        $precoCusto = $this->input->post('precoCusto');
        $precoVendaOS = $this->input->post('precoVendaOS');

        $subtotal = $precoVendaOS * $quantidade;
        $produto = $this->input->post('idProduto');
        $setor_id = null;
        $comissao = 0;
        $grupoProduto_id = null;

        if ($produto) {
            $produtoObj = $this->db->get_where('produtos', array('idProdutos' => $produto))->row();
            if (count($produtoObj) > 0) {
                $setor_id = $produtoObj->setor_id;
                $grupoProduto_id = $produtoObj->grupoProduto_id;
                $comissao = $produtoObj->comissao;
            }
        }

        $data = array(
            'quantidade' => $quantidade,
            'precoCusto' => $precoCusto,
            'precoVendaOS' => $precoVendaOS,
            'subTotal' => $subtotal,
            'produtos_id' => $produto,
            'os_id' => $this->input->post('idOsProduto'),
            'setor_id' => $setor_id,
            'grupoProduto_id' => $grupoProduto_id,
            'comissao' => $comissao,
            'valorComissao' => (($precoVendaOS * $quantidade) * $comissao / 100),
        );

        if ($this->os_model->add('produtos_os', $data) == true) {
            $sql = "UPDATE produtos set estoque = estoque - ? WHERE idProdutos = ?";
            $this->db->query($sql, array($quantidade, $produto));
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }


    public function adicionarProduto()
    {

        $setores = $this->setor_model->getSetorAll();

        $quantidade = $this->input->post('quantidade');
        $precoCusto = $this->input->post('precoCusto');
        $precoVendaOS = $this->input->post('precoVendaOS');
        $sel_comissao = $this->input->post('sel_comissao');
        $comissao = $this->input->post('comissaoProduto');
        $filial = $this->input->post('filial_customizada');

        if ($filial == '') {
            $filial = $this->session->userdata('filial_id');
        }

        if ($sel_comissao == 'R$') {
            $valorComissao = $comissao;
        } else {
            $valorComissao = (($precoVendaOS * $quantidade) * $comissao / 100);
        }

        $subtotal = $precoVendaOS * $quantidade;
        $produto = $this->input->post('idProduto');
        $baixarApenasUmEstoque = 1;

        foreach ($setores as $setor) {

            $data = array(
                'os_id' => $this->input->post('idOsProduto'),
                'quantidade' => $quantidade,
                'precoCusto' => $precoCusto,
                'precoVendaOS' => $precoVendaOS,
                'subTotal' => $subtotal,
                'produtos_id' => $produto,
                'setor_id' => $setor->idSetor,
                'comissao' => $comissao,
                'valorComissao' => $valorComissao,
            );

            $idItemOS = $this->os_model->add('produtos_os', $data, TRUE);
            if ($idItemOS) {

                if ($baixarApenasUmEstoque == 1) {
                    $sql = "UPDATE produto_filial set estoque = estoque - ? WHERE produto_id = ? and filial_id = ? ";
                    $this->db->query($sql, array($quantidade, $produto, $filial));

                    //historico_estoque
                    $historico_estoque = array(
                        'os_id' => $this->input->post('idOsProduto'),
                        'origem' => 'Ordem de Serviço',
                        'tipo' => 'SAÍDA',
                        'estoque' => $quantidade,
                        'precoVenda' => $precoVendaOS,
                        'precoCompra' => $precoCusto,
                        'filial_id' => $filial,
                        'produto_id' => $produto,
                        'item_os_id' => $idItemOS,
                        'data' => date('Y-m-d'),
                        'hora' => date('H:i'),

                    );
                    $this->os_model->addHistoricoEstoque('historico_estoque', $historico_estoque);
                }
                $baixarApenasUmEstoque = $baixarApenasUmEstoque + 1;
            }
        }
        echo json_encode(array('result' => true));
    }

    function excluirProduto()
    {
        $ID = $this->input->post('idProduto');
        if ($this->os_model->delete('produtos_os', 'idProdutos_os', $ID) == true) {

            $idOsProduto = $this->input->post('idOsProduto');
            $quantidade = $this->input->post('quantidade');
            $produto = $this->input->post('produto');
            $filial =  $this->session->userdata('filial_id');

            $sql = "UPDATE produto_filial set estoque = estoque + ? WHERE produto_id = ? and filial_id = ? ";
            $this->db->query($sql, array($quantidade, $produto, $filial));

            //historico_estoque
            $historico_estoque = array(
                'os_id' => $idOsProduto,
                'origem' => 'Ordem de Serviço (Item Excluído)',
                'tipo' => 'ENTRADA',
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto,
                'item_os_id' => $ID,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->os_model->add('historico_estoque', $historico_estoque);
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function adicionarServicoAoAdicionar($idOsServico = null, $setor_id = null, $idOrdemProducao = null)
    {

        $servicos = $this->servicos_model->getAllServico($setor_id);

        foreach ($servicos as $servico) {

            $preco = $servico->preco;
            $setor_id = $servico->setor_id;
            $grupoProduto_id = $servico->grupoProduto_id;
            $comissao = $servico->comissao;
            $servicos_id = $servico->idServicos;

            $data = array(
                'servicos_id' => $servicos_id,
                'setor_id' => $setor_id,
                'grupoProduto_id' => $grupoProduto_id,
                'comissao' => $comissao,
                'valorComissao' => ($preco * $comissao / 100),
                'os_id' => $idOsServico,
                'ordemproducao_id' => $idOrdemProducao,
                'subTotal' => $preco,
                'precoVendaServico' => $preco,
                'precoCustoServico' => $preco,
            );
            $this->os_model->add('servicos_os', $data);
        }
    }

    public function adicionarServicoUmAUm()
    {
        $precoVendaServico = $this->input->post('precoVendaServico');
        $precoServico = $this->input->post('precoServico');
        $precoCustoServico = $this->input->post('precoCustoServico');
        $sutTotal = $precoServico;
        $idServico = $this->input->post('idServico');
        $setor_id = null;
        $grupoProduto_id = null;
        $comissao = 0;

        if ($precoVendaServico) {
            $sutTotal = $precoVendaServico;
        }

        if ($idServico) {
            $Servicoobj = $this->db->get_where('servicos', array('idServicos' => $idServico))->row();
            if (count($Servicoobj) > 0) {
                $setor_id = $Servicoobj->setor_id;
                $grupoProduto_id = $Servicoobj->grupoProduto_id;
                $comissao = $Servicoobj->comissao;
            }
        }

        $data = array(
            'servicos_id' => $idServico,
            'setor_id' => $setor_id,
            'grupoProduto_id' => $grupoProduto_id,
            'comissao' => $comissao,
            'valorComissao' => ($precoVendaServico * $comissao / 100),
            'os_id' => $this->input->post('idOsServico'),
            'subTotal' => $sutTotal,
            'precoVendaServico' => $precoVendaServico,
            'precoCustoServico' => $precoCustoServico,
        );

        if ($this->os_model->add('servicos_os', $data) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function adicionarServico()
    {
        $setores = $this->setor_model->getSetorAll();

        $precoVendaServico = $this->input->post('precoVendaServico');
        $precoServico = $this->input->post('precoServico');
        $precoCustoServico = $this->input->post('precoCustoServico');
        $idServico = $this->input->post('idServico');
        $sel_comissao = $this->input->post('sel_comissao');
        $comissao = $this->input->post('comissaoServico');
        $sutTotal = $precoServico;
        $valorComissao = 0;

        if ($sel_comissao == 'R$') {
            $valorComissao = $comissao;
        } else {
            $valorComissao = ($precoVendaServico * $comissao / 100);
        }

        if ($precoVendaServico) {
            $sutTotal = $precoVendaServico;
        }

        foreach ($setores as $setor) {

            $data = array(
                'os_id' => $this->input->post('idOsServico'),
                'servicos_id' => $idServico,
                'setor_id' => $setor->idSetor,
                'comissao' => $comissao,
                'valorComissao' => $valorComissao,
                'subTotal' => $sutTotal,
                'precoVendaServico' => $precoVendaServico,
                'precoCustoServico' => $precoCustoServico,
            );
            $this->os_model->add('servicos_os', $data);
        }

        echo json_encode(array('result' => true));
    }

    function excluirServico()
    {
        $ID = $this->input->post('idServico');
        if ($this->os_model->delete('servicos_os', 'idServicos_os', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function anexarSetor() {
        $this->load->library('upload');
        $this->load->library('image_lib');

        $upload_conf = array(
            'upload_path' => realpath('./assets/anexos'),
            'encrypt_name' => TRUE,
            'allowed_types' => 'jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG|pdf|PDF|cdr|CDR|docx|DOCX|txt', // formatos permitidos para anexos de os
            'max_size' => 0,
        );

        $this->upload->initialize($upload_conf);

        // Change $_FILES to new vars and loop them
        foreach ($_FILES['userfile'] as $key => $val) {
            $i = 1;
            foreach ($val as $v) {
                $field_name = "file_" . $i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }
        // Unset the useless one ;)
        unset($_FILES['userfile']);

        // Put each errors and upload data to an array
        $error = array();
        $success = array();

        // main action to upload each file
        foreach ($_FILES as $field_name => $file) {
            if (!$this->upload->do_upload($field_name)) {
                // if upload fail, grab error
                $error['upload'][] = $this->upload->display_errors();
            } else {
                // otherwise, put the upload datas here.
                // if you want to use database, put insert query in this loop
                $upload_data = $this->upload->data();

                if ($upload_data['is_image'] == 1) {

                    // set the resize config
                    $resize_conf = array(
                        // it's something like "/full/path/to/the/image.jpg" maybe
                        'source_image' => $upload_data['full_path'],
                        // and it's "/full/path/to/the/" + "thumb_" + "image.jpg
                        // or you can use 'create_thumbs' => true option instead
                        'new_image' => $upload_data['file_path'] . 'thumbs/thumb_' . $upload_data['file_name'],
                        'width' => 200,
                        'height' => 125
                    );

                    // initializing
                    $this->image_lib->initialize($resize_conf);

                    // do it!
                    if (!$this->image_lib->resize()) {
                        // if got fail.
                        $error['resize'][] = $this->image_lib->display_errors();
                    } else {
                        // otherwise, put each upload data to an array.
                        $success[] = $upload_data;
                        $this->load->model('Os_model');

                        if($this->input->post('tipo') == 'Produto') {
                            $this->Os_model->anexarProduto($this->input->post('idOsServico'),$this->input->post('produtos_id'),$this->input->post('idprodutoservico'),$this->input->post('localfoto'),$this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./assets/anexos/'));
                        } else {
                            $this->Os_model->anexarServico($this->input->post('idOsServico'),$this->input->post('servicos_id'),$this->input->post('idprodutoservico'),$this->input->post('localfoto'),$this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./assets/anexos/'));
                        }

                    }
                } else {
                    $success[] = $upload_data;
                    $this->load->model('Os_model');

                    if($this->input->post('tipo') == 'Produto') {
                        $this->Os_model->anexarProduto($this->input->post('idOsServico'),$this->input->post('produtos_id'), $this->input->post('idprodutoservico'), $this->input->post('localfoto'), $this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', '', realpath('./assets/anexos/'));
                    } else {
                        $this->Os_model->anexarServico($this->input->post('idOsServico'),$this->input->post('servicos_id'), $this->input->post('idprodutoservico'), $this->input->post('localfoto'), $this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', '', realpath('./assets/anexos/'));
                    }
                }

            }
        }

        redirect(base_url() . 'index.php/os/osSetorEncaminhar/' . $this->input->post('idOsServico'));

    }

    public function anexar()
    {

        $this->load->library('upload');
        $this->load->library('image_lib');

        $upload_conf = array(
            'upload_path' => realpath('./assets/anexos'),
            'encrypt_name' => TRUE,
            'allowed_types' => 'jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG|pdf|PDF|cdr|CDR|docx|DOCX|txt', // formatos permitidos para anexos de os
            'max_size' => 0,
        );

        $this->upload->initialize($upload_conf);

        // Change $_FILES to new vars and loop them
        foreach ($_FILES['userfile'] as $key => $val) {
            $i = 1;
            foreach ($val as $v) {
                $field_name = "file_" . $i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }
        // Unset the useless one ;)
        unset($_FILES['userfile']);

        // Put each errors and upload data to an array
        $error = array();
        $success = array();

        // main action to upload each file
        foreach ($_FILES as $field_name => $file) {
            if (!$this->upload->do_upload($field_name)) {
                // if upload fail, grab error 
                $error['upload'][] = $this->upload->display_errors();
            } else {
                // otherwise, put the upload datas here.
                // if you want to use database, put insert query in this loop
                $upload_data = $this->upload->data();

                if ($upload_data['is_image'] == 1) {

                    // set the resize config
                    $resize_conf = array(
                        // it's something like "/full/path/to/the/image.jpg" maybe
                        'source_image' => $upload_data['full_path'],
                        // and it's "/full/path/to/the/" + "thumb_" + "image.jpg
                        // or you can use 'create_thumbs' => true option instead
                        'new_image' => $upload_data['file_path'] . 'thumbs/thumb_' . $upload_data['file_name'],
                        'width' => 200,
                        'height' => 125
                    );

                    // initializing
                    $this->image_lib->initialize($resize_conf);

                    // do it!
                    if (!$this->image_lib->resize()) {
                        // if got fail.
                        $error['resize'][] = $this->image_lib->display_errors();
                    } else {
                        // otherwise, put each upload data to an array.
                        $success[] = $upload_data;
                        $this->load->model('Os_model');
                        $this->Os_model->anexar($this->input->post('idOsServico'),$this->input->post('localfoto'),$this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./assets/anexos/'));

                    }
                } else {
                    $success[] = $upload_data;
                    $this->load->model('Os_model');
                    $this->Os_model->anexar($this->input->post('idOsServico'),$this->input->post('localfoto'),$this->input->post('observacaoimg'), $upload_data['file_name'], base_url() . 'assets/anexos/', '', realpath('./assets/anexos/'));
                }

            }
        }

        // see what we get
        if (count($error) > 0) {
            //print_r($data['error'] = $error);
            echo json_encode(array('result' => false, 'mensagem' => 'Nenhum arquivo foi anexado.'));
        } else {
            //print_r($data['success'] = $upload_data);
            echo json_encode(array('result' => true, 'mensagem' => 'Arquivo(s) anexado(s) com sucesso .'));
        }
    }

    public function excluirAnexo($id = null)
    {
        if ($id == null || !is_numeric($id)) {
            echo json_encode(array('result' => false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
        } else {

            $this->db->where('idAnexos', $id);
            $file = $this->db->get('anexos', 1)->row();

            unlink($file->path . '/' . $file->anexo);

            if ($file->thumb != null) {
                unlink($file->path . '/thumbs/' . $file->thumb);
            }

            if ($this->os_model->delete('anexos', 'idAnexos', $id) == true) {

                echo json_encode(array('result' => true, 'mensagem' => 'Anexo excluído com sucesso.'));
            } else {
                echo json_encode(array('result' => false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
            }
        }
    }


    public function downloadanexo($id = null)
    {

        if ($id != null && is_numeric($id)) {

            $this->db->where('idAnexos', $id);
            $file = $this->db->get('anexos', 1)->row();

            $this->load->library('zip');

            $path = $file->path;

            $this->zip->read_file($path . '/' . $file->anexo);

            $this->zip->download('file' . date('d-m-Y-H.i.s') . '.zip');
        }

    }

    public function faturar()
    {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';


        if ($this->form_validation->run('receita') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $vencimento         = $this->input->post('vencimento');
            $recebimento        = $this->input->post('recebimento');
            $previsaoPagamento  = $this->input->post('previsaoPagamento');

            if($vencimento == null){
                $vencimento = date('Y-m-d');
            }

            $data = array(
                'descricao'         => set_value('descricao'),
                'valor'             => $this->input->post('valor'),
                'custo'             => $this->input->post('custoFatura'),
                'clientes_id'       => $this->input->post('clientes_id'),
                'data_vencimento'   => $vencimento,
                'data_pagamento'    => $recebimento,
                'previsaoPagamento' => $previsaoPagamento,
                'baixado'           => $this->input->post('recebido'),
                'cliente_fornecedor' => set_value('cliente'),
                'forma_pgto'        => $this->input->post('formaPgto'),
                'os'                => $os = $this->input->post('os_id'),
                'tipo'              => $this->input->post('tipo')
            );

            if ($this->os_model->add('lancamentos', $data, [$this->input->post('valor')], [$vencimento], 0) == TRUE) {
                $os = $this->input->post('os_id');
                $this->db->set('faturado', 1);
                $this->db->set('valorTotal', $this->input->post('valor'));
                $this->db->where('idOs', $os);
                $this->db->update('os');
                $this->session->set_flashdata('success', 'OS faturada com sucesso!');
                $json = array('result' => true);
                echo json_encode($json);
                die();
            } else {
                $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar faturar OS.');
                $json = array('result' => false);
                echo json_encode($json);
                die();
            }
        }

        $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar faturar OS.');
        $json = array('result' => false);
        echo json_encode($json);
    }

    public function enviarEmail()
    {
        $this->load->library('email');

        $idOs = $this->input->post('idOs');
        $assunto = $this->input->post('assunto');
        $mensagem = $this->input->post('mensagem');
        $email = $this->input->post('para');

        $config = array(
            'smtp_host' => 'q7ky3e8xvr.undercloud.net',
            'smtp_port' => '465',
            'smtp_user' => 'contato@sysoner.com.br',
            'smtp_pass' => '2&it]Z)s5Nu}',
            'crlf' => "\r\n",
            'protocol' => 'smtp',
            'validation' => TRUE,
            'smtp_crypto' => 'ssl',
            'starttls' => false,
        );

        // Send email
        $config['useragent'] = 'SysOner';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from('contato@sysoner.com.br', 'SysOner');

        if ($email) {
            $this->email->to($email);
        }


        $this->load->helper('mpdf');
        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($idOs);
        $this->data['produtos'] = $this->os_model->getProdutosGroupByProdutos($idOs);
        $this->data['servicos'] = $this->os_model->getServicosGroupByServicos($idOs);
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo_id);

        $this->data['view'] = 'os/visualizarOSImpressao';

        $html = $this->load->view('os/visualizarOSImpressao', $this->data, true);
        $attachment = pdf_create($html, 'impressao_os_' . $idOs.'_' . date('dmy'), FALSE);

        $this->email->attach($attachment);
        $this->email->subject($assunto);
        $this->email->message($mensagem);

        $this->email->send();
        //echo $this->email->print_debugger();

        $this->session->set_flashdata('success', 'E-mail enviado com sucesso!');
        redirect(base_url() . 'index.php/os/editar/' . $idOs);
    }

    public function enviarEmailFotoSetor() {
        $this->load->library('email');

        $idOs = $this->input->post('idOs');
        $assunto = $this->input->post('assunto');
        $mensagem = $this->input->post('mensagem');
        $email = $this->input->post('para');

        $config = array(
            'smtp_host' => 'q7ky3e8xvr.undercloud.net',
            'smtp_port' => '465',
            'smtp_user' => 'contato@sysoner.com.br',
            'smtp_pass' => '2&it]Z)s5Nu}',
            'crlf' => "\r\n",
            'protocol' => 'smtp',
            'validation' => TRUE,
            'smtp_crypto' => 'ssl',
            'starttls' => false,
        );

        // Send email
        $config['useragent'] = 'SysOner';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from('contato@sysoner.com.br', 'SysOner');

        if ($email) {
            $this->email->to($email);
        }

        $this->load->helper('mpdf');
        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($idOs);
        $this->data['produtos'] = $this->os_model->getProdutosGroupByProdutos($idOs);
        $this->data['servicos'] = $this->os_model->getServicosGroupByServicos($idOs);
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo_id);

        $this->email->subject($assunto);
        $this->email->message($mensagem);

        $this->email->send();
        //echo $this->email->print_debugger();

        $this->session->set_flashdata('success', 'E-mail enviado com sucesso!');
        redirect(base_url() . 'index.php/os/osSetorEncaminhar/' . $idOs);
    }

    public function  enviarEmailFoto() {
        $this->load->library('email');

        $idOs = $this->input->post('idOs');
        $assunto = $this->input->post('assunto');
        $mensagem = $this->input->post('mensagem');
        $email = $this->input->post('para');

        $config = array(
            'smtp_host' => 'q7ky3e8xvr.undercloud.net',
            'smtp_port' => '465',
            'smtp_user' => 'contato@sysoner.com.br',
            'smtp_pass' => '2&it]Z)s5Nu}',
            'crlf' => "\r\n",
            'protocol' => 'smtp',
            'validation' => TRUE,
            'smtp_crypto' => 'ssl',
            'starttls' => false,
        );

        // Send email
        $config['useragent'] = 'SysOner';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from('contato@sysoner.com.br', 'SysOner');

        if ($email) {
            $this->email->to($email);
        }

        $this->load->helper('mpdf');
        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->os_model->getById($idOs);
        $this->data['produtos'] = $this->os_model->getProdutosGroupByProdutos($idOs);
        $this->data['servicos'] = $this->os_model->getServicosGroupByServicos($idOs);
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['veiculos'] = $this->os_model->getVeiculosOS($this->data['result']->veiculo_id);

        $this->email->subject($assunto);
        $this->email->message($mensagem);

        $this->email->send();
        //echo $this->email->print_debugger();

        $this->session->set_flashdata('success', 'E-mail enviado com sucesso!');
        redirect(base_url() . 'index.php/os/editar/' . $idOs);
    }

    function gerarNFS() {
        $os                 = $this->os_model->getById($this->uri->segment(3));
        $clientes           = $this->clientes_model->getById($os->clientes_id);
        $servicos           = $this->os_model->getServicos($this->uri->segment(3));
        $emiente            = $this->mapos_model->getEmitente();

        if (count($servicos) == 0) {
            redirect(base_url() . 'index.php/os/editar/'.$this->uri->segment(3));
        }

        $proximoNumeroRPS   = $this->nfse_model->getProximoNumeroRPS();
        $serie              = $emiente->serieRps;

        $data_nfse = array(
            'dataEmissao'       => date('Y-m-d'),
            'numeroRps'         => $proximoNumeroRPS,
            //'proximoNumeroNFSE' => $proximoNumeroNFSE,
            'serieRps'          => $serie,
            'naturezaOperacao'  => 'Venda de serviço',

            'nomeClienteNota'   => $clientes->nomeCliente,
            'cpfCnpjCliente'   => $clientes->documento,
            'inscricaoEstadualCliente'   => $clientes->IE,
            'inscricaoMunicialCliente'   => $clientes->IE,//TODO
            'enderecoCliente'   => $clientes->rua,
            'numeroCliente'   => $clientes->numero,
            'complementoCliente'   => $clientes->complemento,
            'cepCliente'   => $clientes->cep,
            'bairroCliente'   => $clientes->bairro,
            'cidadeCliente'   => $clientes->cidade,
            'ufCliente'   => $clientes->estado,
            'telefoneCliente'   => $clientes->telefone,
            'emailCliente'   => $clientes->email,

            'os_id'         => $this->uri->segment(3),
            'clientes_id'   => $os->clientes_id,
            'usuarios_id'   => $os->usuarios_id,
            'faturado'      => 0,
            'situacao'      => 1
        );
        $idNFSE = $this->nfse_model->add('nfse', $data_nfse, true);

        $servicos_id        = null;
        $codigoServicoTributado = null;
        $cnaeServicoTributado   = null;

        $precoVendaServico  = 0;
        $aliquotaISS        = 0;
        $descricaoServico   = '';
        foreach ($servicos as $se)  {
            $servicos_id            = $se->servicos_id;
            $servico                = $this->servicos_model->getById($servicos_id);

            $precoVendaServico      = $precoVendaServico + $se->precoVendaServico;
            $codigoServicoTributado = $servico->codigoServico;
            $cnaeServicoTributado   = $servico->cnae;
            $aliquotaISS            = $servico->aliquotaISS;
            $descricaoServico       = $descricaoServico.'-'.$servico->nome;
        }

        $data_nfse = array(
            'servicoTributado' => $servicos_id,
            'valorServico' => $precoVendaServico,
            'codigoServicoTributado' => $codigoServicoTributado,
            'cnaeServicoTributado' =>$cnaeServicoTributado,
            'descricaoServico' => $descricaoServico,
            'percentualISS' => $aliquotaISS

        );
        $this->nfse_model->edit('nfse', $data_nfse, 'idNFSE',$idNFSE);


        $data_os = array(
            'nfse_gerada' => 1,
            'nNFSE'       => $this->uri->segment(3)
        );
        $this->os_model->edit('os', $data_os, 'idOs', $this->uri->segment(3));

        //redirect(base_url() . 'index.php/nfse/editar/' . $idNFSE);
        redirect(base_url() . 'index.php/os/editar/'.$this->uri->segment(3));
     }

    function gerarNF() {

        $os_id      = $this->uri->segment(3);

        $os         = $this->os_model->getById($os_id);
        $clientes   = $this->clientes_model->getById($os->clientes_id);
        $produtos   = $this->os_model->getProdutos($os_id);

        if (count($produtos) == 0) {
            redirect(base_url() . 'index.php/os/editar/'.$this->uri->segment(3));
        }

        $destCNPJ   = $clientes->documento;
        $destCNPJ   = str_replace('.','', $destCNPJ);
        $destCNPJ   = str_replace('-','', $destCNPJ);
        $destCNPJ   = str_replace('/','', $destCNPJ);

        $destCEP    = $clientes->cep;
        $destCEP    = str_replace('.','', $destCEP);
        $destCEP    = str_replace('-','', $destCEP);
        $destCEP    = str_replace('/','', $destCEP);

        $nNF = $this->nfe_model->getProximoNumeroNFE();

        //gravarnf
        $nfe = array(
            'modelo'   => '55',
            'serie'   => '1',
            'nNF'   => $nNF,
            'natOp'   => 'VENDA DE MERCADORIA',
            'dhEmi'   => date('Y-m-d'),
            'hEmi'   =>  date('H:i'),
            'dhSaiEnt'   => date('Y-m-d'),
            'hSaiEnt'   => date('H:i'),
            'indPag'   => 0,//FORMA DE PAGAMENTO
            'tpNF'   => 1,//Operaçao
            'tpEmis'   => 1,//Forma Emi.*
            'finNFe'   => 1,//Finalidade
            'indPres'   => 1,//Tipo Atend.
            'modFrete'   => 0,//Tipo Frete*

            'destxNome'   => $clientes->nomeCliente,
            'destCNPJ'   => $destCNPJ,
            'destIE'   => $clientes->IE,
            'destISUF'   => $clientes->IESUF,
            'destxLgr'   =>  $clientes->rua,
            'destnro'   =>  $clientes->numero,
            'destxCpl'   => $clientes->complemento,
            'destCEP'   => $destCEP,
            'destxBairro'   => $clientes->bairro,
            'destxMun'   => $clientes->cidade,
            'destcMun'   => $clientes->codIBGECidade,
            'destUF'   => $clientes->estado,
            'destfone'   => $clientes->telefone,
            'destemail'   => $clientes->email,

            'clientes_id'   => $os->clientes_id,
            'usuarios_id'   => $os->usuarios_id,
            'os_id'   => $os_id,

        );

        $this->nfe_model->add('nfe', $nfe, true);

        foreach ($produtos as $pd) {

            $produto = $this->produtos_model->getById($pd->produtos_id);
            $quantidade = $pd->quantidade;
            $precoVendaOS = $pd->precoVendaOS;
            $subTotal = $pd->subTotal;

            $data_produto = array(
                'NFe'   => $nNF,
                'cProd'   => $produto->cProd,
                'xProd'   => $produto->descricao,
                'uCom'   => $produto->unidade,

                'qtd'   => $quantidade,
                'vProd'   => $precoVendaOS,
                'vlrtotal'   => $subTotal,
                'valortotal'   => $subTotal,

                'NCM'   =>  $produto->ncm,
                'CEST'   => $produto->CEST,
                'CFOP'   => $produto->CFOP,

                'orig' => 0,//ORIGEM
                'CST' => 102,//CST

            );

            $this->nfe_model->add('produtonfe', $data_produto, true);
        }

        $this->atualizarTotalizadorNFE($nNF);

        $data_os = array(
            'nfe_gerada' => 1,
            'nNFE'       => $nNF
        );
        $this->os_model->edit('os', $data_os, 'idOs', $os_id);

        //redirect(base_url() . 'index.php/nfe/editar/' . $nNF);
        redirect(base_url() . 'index.php/os/editar/'.$this->uri->segment(3));
    }

    function atualizarTotalizadorNFE($nNF) {
        $produtos   = $this->nfe_model->getProdutos($nNF);

        $vFretenf   = 0;
        $vOutronf   = 0;
        $vIPInf     = 0;
        $vSegnf     = 0;
        $vDescnf    = 0;

        $total      = 0;
        $vTotTrib   = 0;

        $vBC        = 0;
        $vBCST      = 0;
        $vICMS      = 0;
        $vICMSST    = 0;
        $vIPI       = 0;
        $vPIS       = 0;
        $vCOFINS    = 0;

        foreach ($produtos as $produto) {
            $total        = $total + $produto->vlrtotal;
            $vTotTrib     = $vTotTrib + $produto->vTotTrib;

            //aliquotas
            $vBC        = $produto->vBC;
            $vBCST      = $produto->vICMS;
            $vICMS      = $produto->vBCST;
            $vICMSST    = $produto->vICMSST;
            $vIPI       = $produto->vIPI;
            $vPIS       = $produto->vPIS;
            $vCOFINS    = $produto->vCOFINS;
        }

        $valortotalnf = $total + $vFretenf + $vOutronf + $vSegnf + $vIPInf - $vDescnf;

        $data = array(
            'valortotalprod' => $total,
            'valortotalnf'   => $valortotalnf,
            'vTotTrib'       => $vTotTrib,

            'vBCnf'          => $vBC,
            'vBCSTnf'        => $vBCST,
            'vICMSnf'        => $vICMS,
            'vICMSSTnf'      => $vICMSST,
            'vIPInf'         => $vIPI,
            'vPISnf'         => $vPIS,
            'vCOFINSnf'      => $vCOFINS,

            'vFretenf'      => $vFretenf,
            'vOutronf'      => $vOutronf,
            'vSegnf'        => $vSegnf,
            'vDescnf'       => $vDescnf,

            'vNF'            => $valortotalnf,
            'vProdnf'        => $total
        );
        $this->nfe_model->edit('nfe', $data, 'nNF', $nNF);
    }

}

