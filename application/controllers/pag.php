<?php

class pag extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('linkpagamento_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->load->model('zoop_model','',TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('financeiro_model','',TRUE);
        $this->load->model('mapos_model','',TRUE);
        $this->load->model('taxas_model','',TRUE);
        $this->load->model('condicao_pagamento_model', '', TRUE);
        $this->load->model('receita_model', '', TRUE);
        $this->load->model('vendas_model','',TRUE);
        $this->load->model('estoque_model', '', TRUE);

        $this->data['menuLinkDePagamento'] = 'Link de pagamento';
        $this->data['menuFinanceiro'] = 'Financeiro';
    }

    function index(){

        if (!$this->input->get('login')) $this->desconectar();

        if ($this->input->get('login') == 'true') $this->newLogin();
        else redirect(base_url()."pag/security?token=".$this->input->get('token').'&cnpj='.$this->input->get('cnpj'));
    }

    public function security() {

        if ($this->naoLogado() || $this->logadoEmOutraEmpresa())  $this->index();

        $this->data['taxas'] = $this->taxas_model->getById(1);
        $this->data['result'] = $this->linkpagamento_model->getByToken($this->input->get('token'));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['view'] = 'pag/pag';
        $this->load->view('tema/blank', $this->data);
    }

    public function recibo($transacaoId) {

        $pagamento = $this->financeiro_model->getPagamentoByTransacao($transacaoId);
        $faturaParcela = $this->financeiro_model->getParcelaByFatura($pagamento->fatura_id);
        $parcela    = $this->financeiro_model->getParcelaById($faturaParcela->parcela_id);
        $lancamento = $this->financeiro_model->getById($parcela->lancamentos_id);
        $receita = $this->receita_model->getById($lancamento->receita_id);
        $cliente    =  $this->clientes_model->getById($lancamento->clientes_id);
        $emitente   = $this->mapos_model->getEmitente();
        $condicaoPagamento = $this->condicao_pagamento_model->getById($lancamento->condicao_pagamento_id);

        $this->data['pagamento'] = $pagamento;
        $this->data['emitente'] = $emitente;
        $this->data['cliente'] = $cliente;
        $this->data['parcela'] = $parcela;
        $this->data['receita'] = $receita;
        $this->data['condicaoPagamento'] = $condicaoPagamento;

        $this->data['view'] = 'pag/recibo';

        $this->load->helper('mpdf');
        $html =$this->load->view('tema/blank', $this->data, true);
        pdf_create($html, 'recibo_de_pagamento' . date('d/m/y'), TRUE);
    }

    function desconectar() {
        $this->session->sess_destroy();
        redirect(base_url()."pag?token=".$this->input->get('token').'&cnpj='.$this->input->get('cnpj').'&login=true');
    }

    function newLogin() {

        $cnpjempresa = $this->input->get('cnpj');

        $dados = array(
            'filial_id'     => 1,
            'cnpjempresa'   => $cnpjempresa,
            'logado'        => TRUE
        );

        $this->session->userdata['cnpjempresa'] = $cnpjempresa;
        $this->session->userdata['filial_id'] = 1;
        $this->session->userdata['id'] = 1;

        if (!isset($_SESSION)):
            session_start();
        endif;

        $_SESSION['BD'] = $cnpjempresa;
        $this->session->set_userdata($dados);
        $this->session->set_userdata('cnpjempresa', $cnpjempresa);

        redirect(base_url()."pag/security?token=".$this->input->get('token').'&cnpj='.$this->input->get('cnpj').'&login=false');
    }

    function logadoEmOutraEmpresa() {
        $logado = $this->session->userdata('cnpjempresa');
        $logar = $this->input->get('cnpj');
        return $logado != $logar;
    }

    function naoLogado() {
        return !$this->session->userdata('cnpjempresa');
    }

    function pagSend() {
        $token = $this->input->post('token');
        $formaPagamento = $this->input->post('forma');
        $linkPagamento = $this->linkpagamento_model->getByToken($token);
        $parcelas = $this->input->post('parcelas');

        if ($formaPagamento == 'cc') $this->autorizarPagamentoComCartaoDeCredito($linkPagamento, $parcelas);
        else $this->cadastroDeClienteParaEmissaoDeBoletos($linkPagamento);

    }

    function autorizarPagamentoComCartaoDeCredito($linkPagamento, $parcelas) {

        $card = $this->preencherDadosCartaoDeCredito();

        $retornoApi = json_decode($this->zoop_model->realizarTransacaoComCartaoCreditoDiretaNaoPresente($linkPagamento, $card, $parcelas));
        $statusTransacao = '';

        if (array_key_exists('status', $retornoApi) )  $statusTransacao = $retornoApi->status;

        if ($statusTransacao != null && $statusTransacao == 'succeeded') {

            $valor = $retornoApi->amount;
            $transaction_number = $retornoApi->id;

            if ($linkPagamento->parcela_id != null) $this->pagarParcelaDeUmLinkDePagamentoEmCartaoDeCredito($retornoApi, $linkPagamento);
            else if ($linkPagamento->venda_id) $this->lancarReceitaDeLinkDePagamentoEmCartaoDeCredito($retornoApi , $linkPagamento, $linkPagamento->venda_id);
            else $this->lancarReceitaDeLinkDePagamentoEmCartaoDeCredito($retornoApi, $linkPagamento);

            $retornoPagamentoError = array(
                'error' => FALSE,
                'boleto' => FALSE,
                'message_display' => '',
                'message' => '',
                'data_pagamento'=> date('Y/m/d'),
                'numero_transacao' => $transaction_number,
                'valor' => $this->site_model->formatarValorMonetario($valor)
            );

            if ($linkPagamento->parcela_id != null){
                $this->linkpagamento_model->inativar($linkPagamento->idLinkPagamento);
            }

            echo json_encode($retornoPagamentoError);

        } else if ($statusTransacao == 'failed') {

            $message_display = $retornoApi->error->message_display;
            $message = $retornoApi->error->message;

            $retornoPagamentoError = array(
                'error' => true,
                'message_display' => $message_display,
                'boleto' => FALSE,
                'message' => $message,
            );

            echo json_encode($retornoPagamentoError);
        } else {

            $retornoPagamentoError = array(
                'error' => true,
                'boleto' => FALSE,
                'message_display' => 'Erro ao processar o pagamento, verifique os dados do seu cartão',
                'message' => '',
            );
            echo json_encode($retornoPagamentoError);
        }
    }

    function pagarParcelaDeUmLinkDePagamentoEmCartaoDeCredito($retornoApi, $linkPagamento) {

        $parcela = $this->financeiro_model->getParcelaById($linkPagamento->parcela_id);
        $numeroParcela = $this->input->post('parcelas');

        $transaction_id = $retornoApi->id;
        $authorizer_id = $retornoApi->payment_authorization->authorizer_id;
        $authorization_code = $retornoApi->payment_authorization->authorization_code;
        $authorization_nsu = $retornoApi->payment_authorization->authorization_nsu;
        $fees = $retornoApi->fees/$numeroParcela;

        $observacao = 'PAGAMENTO REF:.' . $linkPagamento->descricao.' CÓDIGO DA TRANSAÇÃO :: '.$transaction_id;//TODO VERIFICAR
        $valorPagamentos =  $retornoApi->amount;
        $dataPagamentos = date('Y-m-d');

        $formaPagamentos = 5;//TODO VERIFICAR CARTAO DE CREDITO

        $this->financeiro_model->pagar($parcela->idParcela, [$formaPagamentos], [$dataPagamentos], [$valorPagamentos], $observacao, $transaction_id, $authorizer_id, $authorization_code, $authorization_nsu, $fees, $linkPagamento);

    }

    function buscarCondicaoPagamento($numeroParcela) {
        $condicaoPagamento = $this->condicao_pagamento_model->getByNumeroDeParcelas($numeroParcela);

        if (count($condicaoPagamento) > 0) return $condicaoPagamento;
        else {
            $this->condicao_pagamento_model->add('condicao_pagamento', array('parcelas' => $numeroParcela, 'nome' => $numeroParcela.' X'));
            return $this->condicao_pagamento_model->getByNumeroDeParcelas($numeroParcela);
        }
    }

    function lancarReceitaDeLinkDePagamentoEmCartaoDeCredito($retornoApi, $linkPagamento, $venda_id=null) {

        $emitente = $this->zoop_model->getEmitente();

        $valor = $retornoApi->amount;
        $transaction_id = $retornoApi->id;
        $authorizer_id = $retornoApi->payment_authorization->authorizer_id;
        $authorization_code = $retornoApi->payment_authorization->authorization_code;
        $authorization_nsu = $retornoApi->payment_authorization->authorization_nsu;
        $numeroParcelas = $this->input->post('parcelas');
        $fees = $retornoApi->fees/$numeroParcelas;

        $condicaoPagamento = $this->buscarCondicaoPagamento($numeroParcelas);

        $clienteId = $this->preencherCliente();

        $contaReceber = array(
            'descricao' => $linkPagamento->descricao,
            'valor' => $valor,
            'data_vencimento' => date('Y-m-d'),
            'condicao_pagamento_id' => $condicaoPagamento->idCondicaoPagamento,
            'clientes_id' => $clienteId,
            'venda' => $venda_id,

            'tipo' => 'receita',
            'forma_pgto' => 'PAGAMENTO DIGITAL',
            'receita_id' => 1,//TODO VIA CONFIGURACAO
        );

        $idLancamento = $this->financeiro_model->add('lancamentos', $contaReceber, [$valor] , [date('Y-m-d')], 0);

        if ($emitente->adiantamento_parcela == 1) {

            $parcelas = $this->financeiro_model->getParcelasByLancamento($idLancamento);

            foreach ($parcelas as $parcela) {

                $observacao = 'PAGAMENTO REF:.' . $linkPagamento->descricao . ' CÓDIGO DA TRANSAÇÃO :: ' . $transaction_id;//TODO VERIFICAR
                $dataPagamento = date('Y-m-d');
                $valorPagamento = $valor / $numeroParcelas;

                $formaPagamentos = 5;//TODO VERIFICAR CARTAO DE CREDITO

                $this->financeiro_model->pagar($parcela->idParcela, [$formaPagamentos], [$dataPagamento], [$valorPagamento], $observacao, $transaction_id, $authorizer_id, $authorization_code, $authorization_nsu, $fees, $linkPagamento);
            }
        }

        if ($venda_id != null) $this->faturarVenda($venda_id, 0);

    }

    function faturarVenda($vendas_id, $desconto) {
        $this->vendas_model->faturarVenda($vendas_id, $desconto);
        $this->atualizarEstoqueDosProdutos($vendas_id);
    }

    function atualizarEstoqueDosProdutos($vendas_id) {

        $produtos   = $this->vendas_model->getProdutos($vendas_id);

        foreach ($produtos as $produto) {

            $idItens        = $produto->idItens;
            $quantidade     = $produto->quantidade;
            $produtos_id    = $produto->produtos_id;
            $vendas_id      = $produto->vendas_id;
            $filial         = $produto->filial_id;

            if ($quantidade > 0) $this->estoque_model->saidaEstoque($quantidade, $produtos_id, 'Venda PDV', $vendas_id, $idItens, $filial);
        }
    }

    function cadastroDeClienteParaEmissaoDeBoletos($linkPagamento) {

        $tipoPessoa = 'pj';
        $cpf = $this->input->post('documento');
        $nome = $this->input->post('nome');
        $telefone = $this->input->post('telefone');
        $email = $this->input->post('email');
        $cep = $this->input->post('cep');
        $endereco =  $this->input->post('endereco');
        $numero = $this->input->post('numero');
        $complemento = $this->input->post('complemento');
        $bairro = $this->input->post('bairro');
        $cidade = $this->input->post('cidade');
        $estado = $this->input->post('estado');

        if ($tipoPessoa == 'pf') $tipoPessoa = 'PF';
        else $tipoPessoa = 'PJ';

        $data = array(
            'tipoPessoa'            => $tipoPessoa,
            'nomeCliente'           => $nome,
            'documento'             => $cpf,
            'celular'               => $telefone,
            'email'                 => $email,
            'rua'                   => $endereco,
            'numero'                => $numero,
            'bairro'                => $bairro,
            'cidade'                => $cidade,
            'estado'                => $estado,
            'cep'                   => $cep,
            'complemento'           => $complemento,
            'dataCadastro'          => date('Y-m-d')
        );

        $cliente  =  $this->clientes_model->getByDocumento($cpf);

        if (count($cliente) > 0) {
           $this->clientes_model->edit('clientes', $data, 'idClientes',$cliente->idClientes);
           $idCliente = $cliente->idClientes;
        } else  $idCliente = $this->clientes_model->add('clientes', $data, TRUE);

        $cliente = $this->clientes_model->getById($idCliente);
        $this->cadClienteZoop($cliente);

        $this->cadastroDoFinaceiroEhEmissaoDeBoleto($linkPagamento, $idCliente);
    }

    function cadastroDoFinaceiroEhEmissaoDeBoleto($linkPagamento, $clienteId) {

        $valorVencimento    = $linkPagamento->valor;
        $dataVencimento     = date('Y-m-d');
        $descricaoBoleto    = 'PAG REF:. '.$linkPagamento->descricao;
        $cliente_id         = $clienteId;
        $dataVencimento     = date("Y-m-d", strtotime($dataVencimento));

        $limitePagamento    = 3;//TODO VER DA CONFIGURACAO
        $receita_id         = 1;//TODO VIR DA CONFIGURACAO
        $condicao_pagamento_id = 1;//TODO VIR DA CONFIGURACAO

        $data = array(
            'descricao'             => $descricaoBoleto,
            'valor'                 => $valorVencimento,
            'receita_id'            => $receita_id,
            'clientes_id'           => $cliente_id,
            'data_vencimento'       => $dataVencimento,
            'tipo'                  => 'receita',
            'condicao_pagamento_id' => $condicao_pagamento_id
        );

        $idLancamento = $this->financeiro_model->add('lancamentos',$data, [$valorVencimento], [$dataVencimento], 0);

        $this->gerarBoletoZoop($idLancamento, $limitePagamento, $descricaoBoleto);
    }

    function gerarBoletoZoop($idLancamento, $carencia, $descricaoBoleto) {

        $parcelas = $this->financeiro_model->getParcelasByLancamento($idLancamento);
        $boleto = null;

        foreach ($parcelas as $parcela) {

            $faturas = $this->financeiro_model->getFaturasParcelaByLancamento($parcela->idParcela);

            foreach ($faturas as $fat) {

                $fatura     =  $this->financeiro_model->getFaturaById($fat->fatura_id);
                $cliente    =  $this->clientes_model->getById($fatura->clientes_id);

                if ($cliente->client_integration_id != '') {
                    $jsonRetorno = json_decode($this->zoop_model->postCadTransactionsBoleto($fatura, $cliente, $carencia, $descricaoBoleto));

                    $boleto = array(
                        'cliente_id' => $cliente->idClientes,
                        'fatura_id' => $fatura->idFatura,
                        'parcela_id' => $parcela->idParcela,
                        'transactions_integration_id' => $jsonRetorno->id,
                        'url_boleto' => $jsonRetorno->payment_method->url,
                        'transactions_status' => $jsonRetorno->status,
                        'transactions_status_boleto' => $jsonRetorno->payment_method->status,
                        'barcode' => $jsonRetorno->payment_method->barcode,
                    );
                    $boletoId = $this->clientes_model->add('boleto', $boleto, TRUE);
                }
            }
        }

        $boleto = $this->financeiro_model->getBoletoById($boletoId);

        if ($boleto->transactions_integration_id) {
            $retornoPagamento = array(
                'error' => FALSE,
                'boleto' => TRUE,
                'link'=> $boleto->transactions_integration_id
            );

            echo json_encode($retornoPagamento);
        } else  {
            $retornoPagamentoErro = array(
                'error' => TRUE,
                'boleto' => 'Correu um erro ao gerar o boleto, por vafor verique seus dados e tente outra vez.',
                'link'=> $boleto->link
            );
            echo json_encode($retornoPagamentoErro);
        }
    }

    function cadClienteZoop($cliente) {
        if ($cliente->client_integration_id == '') {
            $jsonRetorno = json_decode($this->zoop_model->postCadBuyers($cliente));

            if ($jsonRetorno->id != null) $this->clientes_model->edit('clientes', array('client_integration_id' => $jsonRetorno->id), 'idClientes', $cliente->idClientes);
        } else {
            $jsonRetorno = json_decode($this->zoop_model->putGetCadBuyers($cliente));

            if ($jsonRetorno->id != null) $this->clientes_model->edit('clientes', array('client_integration_id' => $jsonRetorno->id), 'idClientes', $cliente->idClientes);
        }
    }

    function preencherCliente() {

        $nomeComprador = $this->input->post('nomeComprador');
        $cpfComprador = $this->input->post('cpfComprador');

        $dadosCliente = array(
            'tipoPessoa'            => 'PF',
            'nomeCliente'           => $nomeComprador,
            'documento'             => $cpfComprador,
            'dataCadastro'          => date('Y-m-d')
        );

        $cliente  =  $this->clientes_model->getByDocumento($cpfComprador);

        if (count($cliente) > 0) {
            $this->clientes_model->edit('clientes', $dadosCliente, 'idClientes',$cliente->idClientes);
            $idCliente = $cliente->idClientes;
        } else {
            $idCliente = $this->clientes_model->add('clientes', $dadosCliente, TRUE);
        }

        return $idCliente;
    }

    function preencherDadosCartaoDeCredito() {

        $card_number1 = $this->input->post('cardnumber');
        $card_number2 = $this->input->post('card-number-1');
        $card_numbe3 = $this->input->post('card-number-2');
        $card_numbe4 = $this->input->post('card-number-3');
        $cvv = $this->input->post('card-cvv');
        $donoCartao = $this->input->post('name');
        $mes = $this->input->post('card-month');
        $ano = $this->input->post('card-year');

        $card = array(
            "card_number" => $card_number1 . $card_number2 . $card_numbe3 . $card_numbe4,
            "holder_name" => $donoCartao,
            "expiration_month" => $mes,
            "expiration_year" => $ano,
            "security_code" => $cvv,
        );

        return $card;
    }
}