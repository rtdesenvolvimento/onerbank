<?php

class patrimonio extends CI_Controller {

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('patrimonio_model', '', TRUE);
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);

        $this->load->model('filial_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->load->model('centrocusto_model', '', TRUE);

        $this->data['menuPatrimonio'] = 'Patrimonio';
        $this->data['menuEstoque'] = 'Estoque';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/patrimonio/gerenciar/';
        $config['total_rows']   = $this->patrimonio_model->count('produtos');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->patrimonio_model->getProdutos('',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'patrimonio/patrimonio';
        $this->load->view('theme/topo',$this->data);
    }

    function buscar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/patrimonio/gerenciar/';
        $config['total_rows'] = $this->patrimonio_model->count('produtos');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $parametro = $this->uri->segment(3);
        $parametro = str_replace("%20", "%" , $parametro);

        $where = '';
        if ($parametro) {
            $where = 'p.descricao like "%'.$parametro.'%" or s.nome like "%'.$parametro.'%" or g.nome like "%'.$parametro.'%" ';
        }

        $this->data['results'] = $this->patrimonio_model->getProdutos($where,$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'patrimonio/patrimonio';
        $this->load->view('theme/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para adicionar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('patrimonio') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'descricao'         => $this->input->post('descricao'),
                'cProd'             => $this->input->post('codigo'),
                'cEAN'              => $this->input->post('cEAN'),
                'grupoProduto_id'   => $this->input->post('grupoProduto_id'),
                'centro_custo_id'   => $this->input->post('centro_custo_id'),
                'origemAquisicao'   => $this->input->post('origemAquisicao'),
                'marca'             => $this->input->post('marca'),
                'modelo'            => $this->input->post('modelo'),
                'numeroSerie'       => $this->input->post('numeroSerie'),
                'localizacao'       => $this->input->post('localizacao'),
                'dataAquisicao'     => $this->input->post('dataAquisicao'),
                'numeroNFCompra'    => $this->input->post('numeroNFCompra'),
                'valorAquisicao'    => $this->input->post('valorAquisicao'),
                'observacao'        => $this->input->post('observacao'),
                'valorResidual'     => $this->input->post('valorResidual'),
                'valorDepreciar'    => $this->input->post('valorDepreciar'),
                'vidaUtil'          => $this->input->post('vidaUtil'),
                'valorDepreciacao'  => $this->input->post('valorDepreciacao'),
                'valorDepreciado'   => $this->input->post('valorDepreciado'),
                'tipoDepreciacao'   => $this->input->post('tipoDepreciacao'),
                'depreciacaoPercentual'   => $this->input->post('depreciacaoPercentual'),

                'unidade'           => 'UN',
                'patrimonio'        => 1,
            );
            $idPatriomonio = $this->patrimonio_model->add('produtos', $data ,TRUE);

            if ($idPatriomonio) {
                for($i=1;$i<=6;$i++) {
                    $manutencao = array(
                        'indice'        => $i,
                        'produto_id'    =>$idPatriomonio,
                    );
                    $this->patrimonio_model->add('manutencao_preventiva', $manutencao);
                }
                $this->session->set_flashdata('success','Patrimonio adicionado com sucesso!');
                redirect(base_url() . 'index.php/patrimonio/editar/'.$idPatriomonio);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }

        $contador = $this->patrimonio_model->count('produtos');
        $this->data['numeroPatrimonio']     = str_pad($contador, 6, '0', STR_PAD_LEFT).'/'.date('Y');
        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['centroCustos']         = $this->centrocusto_model->getAll();
        $this->data['view'] = 'patrimonio/adicionarPatrimonio';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'ePatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para editar patrimonio.');
            redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $filiais  = $this->filial_model->getAll();

        if ($this->form_validation->run('patrimonio') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'descricao'         => $this->input->post('descricao'),
                'cProd'             => $this->input->post('codigo'),
                'cEAN'              => $this->input->post('cEAN'),
                'grupoProduto_id'   => $this->input->post('grupoProduto_id'),
                'centro_custo_id'   => $this->input->post('centro_custo_id'),
                'origemAquisicao'   => $this->input->post('origemAquisicao'),
                'marca'             => $this->input->post('marca'),
                'modelo'            => $this->input->post('modelo'),
                'numeroSerie'       => $this->input->post('numeroSerie'),
                'localizacao'       => $this->input->post('localizacao'),
                'dataAquisicao'     => $this->input->post('dataAquisicao'),
                'numeroNFCompra'    => $this->input->post('numeroNFCompra'),
                'valorAquisicao'    => $this->input->post('valorAquisicao'),
                'observacao'        => $this->input->post('observacao'),
                'valorResidual'     => $this->input->post('valorResidual'),
                'valorDepreciar'    => $this->input->post('valorDepreciar'),
                'vidaUtil'          => $this->input->post('vidaUtil'),
                'valorDepreciacao'  => $this->input->post('valorDepreciacao'),
                'valorDepreciado'   => $this->input->post('valorDepreciado'),
                'tipoDepreciacao'   => $this->input->post('tipoDepreciacao'),
                'depreciacaoPercentual'   => $this->input->post('depreciacaoPercentual'),
                'unidade'           => 'UN',
                'patrimonio'        => 1,
            );

            for($i=0;$i<6;$i++) {

                $servico_id = $this->input->post('servico_id')[$i];
                $peca_id = $this->input->post('peca_id')[$i];
                $tipoManutencao = $this->input->post('tipoManutencao')[$i];
                $periodo = $this->input->post('periodo')[$i];
                $dataInicialPreventiva =  $this->input->post('dataInicialPreventiva')[$i];
                $idProdutos = $this->input->post('idProdutos');

                $indice = $i + 1;

                $manutencao = array(
                    'servico_id' => $servico_id,
                    'peca_id' => $peca_id,
                    'tipoManutencao' => $tipoManutencao,
                    'periodo' => $periodo,
                    'dataInicialPreventiva' => $dataInicialPreventiva,
                    'produto_id' =>$idProdutos,
                );

                $this->patrimonio_model->editManutencao($manutencao, $indice, $idProdutos);
            }

            if ($this->patrimonio_model->edit('produtos', $data, 'idProdutos', $this->input->post('idProdutos')) == TRUE) {
                $this->calcularDepreciacao( $this->input->post('idProdutos'));
                $this->session->set_flashdata('success','Patrimonio editado com sucesso!');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['manutencoes']          = $this->patrimonio_model->getAllManutencoes($this->uri->segment(3));
        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['centroCustos']         = $this->centrocusto_model->getAll();
        $this->data['servicos']             = $this->servicos_model->getAll();
        $this->data['pecas']                = $this->produtos_model->getAll();
        $this->data['result']               = $this->patrimonio_model->getById($this->uri->segment(3));
        $this->data['filiais']              = $filiais;

        $this->data['view'] = 'patrimonio/editarPatrimonio';
        $this->load->view('theme/topo', $this->data);
    }

    function atualizarDepreciacao() {
        $produtos = $this->patrimonio_model->getAll();
        foreach ($produtos as $produto) {
            $this->calcularDepreciacao($produto->idProdutos);
        }
    }

    function calcularDepreciacao($id) {

        $patrimonio = $this->patrimonio_model->getById($id);
        $tipoDepreciacao = $patrimonio->tipoDepreciacao;

        if ($tipoDepreciacao == 2) {
            $this->depreciacaoPercentualMes($patrimonio, $id);
        }  else if ($tipoDepreciacao == 3) {
            $this->depreciacaoGerencial($patrimonio, $id);
        }
    }

    function depreciacaoPercentualMes($patrimonio, $id) {

        $dataAquisicao = $patrimonio->dataAquisicao;
        $valorAquisicao = $patrimonio->valorAquisicao;
        $valorDepreciar = $patrimonio->valorDepreciar;
        $depreciacaoPercentual = $patrimonio->depreciacaoPercentual;

        if ($depreciacaoPercentual > 0) {
            $vidaUtil = 100 / $depreciacaoPercentual;
        } else {
            $vidaUtil = 0;
        }

        $dataHoje = date('Y-m-d');

        $intervalo = $this->diff($dataAquisicao, $dataHoje);

        $tempoUsoAno = $intervalo->y;
        if ($tempoUsoAno > 0) {
            $tempoUsoMes = ($tempoUsoAno * 12) + $intervalo->m;
        } else {
            $tempoUsoMes =  $intervalo->m;
        }

        if ($vidaUtil > 0) {
            $valorDepreciacao = $valorDepreciar / $vidaUtil;
        } else {
            $valorDepreciacao = 0;
        }

        $data = array(
            'tempoUso'          => $tempoUsoMes,
            'valorResidual'     => $valorAquisicao -  $valorDepreciar,
            'valorDepreciacao'  => $valorDepreciacao,
            'valorDepreciado'   => ($valorDepreciacao * $tempoUsoMes),
            'vidaUtil'          => $vidaUtil,
            'valorAtualPosDepreciacao' => $valorAquisicao - ($valorDepreciacao * $tempoUsoMes),
        );

        $this->patrimonio_model->edit('produtos', $data, 'idProdutos', $id);
    }

    function depreciacaoGerencial($patrimonio, $id) {

        $dataAquisicao = $patrimonio->dataAquisicao;
        $valorAquisicao = $patrimonio->valorAquisicao;
        $valorDepreciar = $patrimonio->valorDepreciar;
        $vidaUtil       = $patrimonio->vidaUtil;

        $dataHoje = date('Y-m-d');

        $intervalo = $this->diff($dataAquisicao, $dataHoje);

        $tempoUsoAno = $intervalo->y;
        if ($tempoUsoAno > 0) {
            $tempoUsoMes = ($tempoUsoAno * 12) + $intervalo->m;
        } else {
            $tempoUsoMes =  $intervalo->m;
        }

        if ($vidaUtil > 0) {
            $valorDepreciacao = $valorDepreciar / $vidaUtil;
            $depreciacaoPercentual = 100/$vidaUtil;
        } else {
            $valorDepreciacao = 0;
            $depreciacaoPercentual = 0;
        }

        $data = array(
            'tempoUso'          => $tempoUsoMes,
            'valorResidual'     => $valorAquisicao -  $valorDepreciar,
            'valorDepreciacao'  => $valorDepreciacao,
            'valorDepreciado'   => ($valorDepreciacao * $tempoUsoMes),
            'valorAtualPosDepreciacao' => $valorAquisicao - ($valorDepreciacao * $tempoUsoMes),
            'depreciacaoPercentual' => $depreciacaoPercentual,
        );

        $this->patrimonio_model->edit('produtos', $data, 'idProdutos', $id);
    }

    function diff($dataInicial , $dataFinal) {
        $data1 = new DateTime( $dataInicial);
        $data2 = new DateTime( $dataFinal);
        return $data1->diff( $data2 );
    }


    function visualizar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->data['result'] = $this->patrimonio_model->getById($this->uri->segment(3));

        if($this->data['result'] == null){
            $this->session->set_flashdata('error','Patrimonio não encontrado.');
            redirect(base_url() . 'index.php/patrimonio/editar/'.$this->input->post('idProdutos'));
        }

        $this->data['fornecedoresproduto']  = $this->produtos_model->getFornecedoresProduto($this->uri->segment(3));
        $this->data['view']                 = 'produtos/visualizarProduto';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para excluir patrimonio.');
            redirect(base_url());
        }


        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir patrimonio.');
            redirect(base_url().'index.php/patrimonio/gerenciar/');
        }

        $this->db->where('produtos_id', $id);
        $this->db->delete('produtos_os');


        $this->db->where('produtos_id', $id);
        $this->db->delete('itens_de_vendas');

        $this->produtos_model->delete('produtos','idProdutos',$id);

        $this->session->set_flashdata('success','Produto excluido com sucesso!');
        redirect(base_url().'index.php/produtos/gerenciar/');
    }

    public function autoCompleteFornecedorFilterSelect() {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->produtos_model->autoCompleteFornecedorFilterSelect($q);
            $this->send_json($rows);
        }
    }


    public function autoCompleteProdutoByIdNotFilterFilial($idProduto)
    {
        if ($idProduto) {
            $rows['results'] = $this->os_model->autoCompleteProdutoById($idProduto);
            $this->send_json($rows);
        }
    }

    public function autoCompleteFornecedorById($idFornecedor) {
        if ($idFornecedor) {
            $rows['results'] = $this->produtos_model->autoCompleteFornecedorById($idFornecedor);
            $this->send_json($rows);
        }
    }

    public function send_json($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    public function salvarFornecedorProduto()
    {
        $idFornecedorproduto    =  $this->input->post('idFornecedorproduto');
        $idFornecedor           = $this->input->post('idFornecedor');
        $idProduto              = $this->input->post('idProduto');
        $codigo                 = $this->input->post('codigo');
        $observacao             = $this->input->post('observacao');

        $data = array(
            'idFornecedor'  => $idFornecedor,
            'idProduto'     => $idProduto,
            'codigo'        => $codigo,
            'observacao'    => $observacao,
        );

        if ($idFornecedorproduto) {
            if ($this->produtos_model->edit('fornecedorproduto', $data, 'idFornecedorproduto', $idFornecedorproduto) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            if ($this->produtos_model->add('fornecedorproduto', $data) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        }
    }

    function excluirFornecedorProduto()
    {
        $ID = $this->input->post('idFornecedorproduto');
        if ($this->produtos_model->delete('fornecedorproduto', 'idFornecedorproduto', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    function consultaProduto() {
        $produto_id = $this->input->post('produtos_id');
        if ($produto_id) {
            echo json_encode($this->produtos_model->getProdutoFilialById($produto_id));
        }
    }

}

