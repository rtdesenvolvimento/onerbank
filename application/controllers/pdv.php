<?php

class pdv extends CI_Controller {

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aPDV')){
            //$this->session->set_flashdata('error','Você não tem permissão usar o PDV verifique as permissões no sistema.');
            //redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('pdv_model', '', TRUE);
        $this->load->model('filial_model', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('vendas_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('nfce_model', '', TRUE);
        $this->load->model('usuarios_model', TRUE);
        $this->load->model('caixa_model', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->load->model('estoque_model', '', TRUE);
        $this->load->model('naturezaoperacao_model', '', TRUE);
        $this->load->model('configuracao_model', '', TRUE);
        $this->load->model('financeiro_model', '', TRUE);
        $this->load->model('condicao_pagamento_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->load->model('linkpagamento_model', '', TRUE);
        $this->load->model('tipo_cobranca_model', '', TRUE);

        $this->data['menuOperacao'] = 'Operacao';
        $this->data['menuVendasPDV'] = 'Vendas no PDV';
    }

    function index(){
        $this->edit();
    }

    function nova() {
        $this->edit(null, null, true);
    }

    function edit($vendas_id=NULL,$grupo_id=null,$nova=null) {

        if ($grupo_id == null) $grupo_id = 1;
        if ($vendas_id == null) $vendas_id = $this->buscarVendaAberta($nova);

        $this->data['view'] = 'pdv/pdv';
        $this->data['gruposproduto']    = $this->grupoproduto_model->getGruposProdutoAll();

        $this->data['usuario']          = $this->vendas_model->getUsuarioById($this->session->userdata('id'));;
        $this->data['filial']           = $this->vendas_model->getFilialById($this->session->userdata('filial_id'));
        $this->data['result']           = $this->vendas_model->getById($vendas_id);
        $this->data['produtospdv']      = $this->vendas_model->getProdutos($vendas_id);
        $this->data['condicoes']        = $this->condicao_pagamento_model->getAll();
        $this->data['tiposCobranca']    = $this->tipo_cobranca_model->getAll();

        $this->data['configuracao']     = $this->configuracao_model->getConfiguracao();
        $this->data['grupo_id']         = $grupo_id;
        $this->data['produtos']         = $this->produtos_model->getProdutosFiliais('',null,null, $grupo_id);
        $this->data['esperas']          = $this->vendas_model->getAllPedidosEmEspera();

        $this->load->view('tema/blank', $this->data);
    }

    function vendas() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vVenda')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar vendas.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/vendas/gerenciar/';
        $config['total_rows'] = $this->vendas_model->count('os');
        $config['per_page'] = 100000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        if ($dataInicial == '') $dataInicial = date('Y-m-d');
        if ($dataFinal == '') $dataFinal = date('Y-m-d');


        $where = 'vendas.dataVenda >= "'.$dataInicial.'" and vendas.dataVenda <= "'.$dataFinal.'"';

        $this->data['results'] = $this->vendas_model->getVendasPDV('vendas', '*', $where, $config['per_page'], $this->uri->segment(3));

        $this->data['dataInicial'] = $dataInicial;
        $this->data['dataFinal'] = $dataFinal;
        $this->data['view'] = 'pdv/vendaspdv';

        $this->load->view('theme/topo', $this->data);
    }

    function gerarUmaNovaVendaPDV() {

        $configuracao = $this->configuracao_model->getConfiguracao();

        $data = array(
            'dataVenda' => date('Y-m-d'),
            'clientes_id' => $configuracao->cliente_padrao_id,
            'faturado' => 0,
            'espera' => 1,
            'vendapdv' => 1,
            'usuarios_id' => $this->session->userdata('id'),
            'fiscal' => $configuracao->usarNFCE
        );

        return $this->vendas_model->add('vendas', $data, true);
    }

    function getTabelaDeParcelamento() {

        $condicao = $this->condicao_pagamento_model->getById($this->input->post('condicao'));
        $parcelas = $condicao->parcelas;

        $valorConta = $this->input->post('faltaPagar');
        $dataVencimento = date('Y-m-d');
        $vencimento = number_format(   $valorConta/$parcelas , 2, '.', '');

        $html = '';
        for ($i=0;$i<$parcelas;$i++) {

            $dataVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dataVencimento)));
            $readonly = '';

            if ($parcelas == ($i+1) ) $readonly = 'readonly';

            $html .= '<tr>
                          <td>'.($i+1).'</td>
                          <td><div class="form-group"> <div class="input-group"><div class="input-group-addon" style="padding: 2px 8px;">R$</div><input id="parcela'.($i+1).'" onkeyup="atualizarValorVencimentoAoEditarParcela(this, 0);" '.$readonly.' vencimento="'.$vencimento.'" name="valorVencimento[]" style="line-height: 20px;text-align: right;" type="text" value="'.$vencimento.'" class="pa form-control kb-pad1 amount money" required="required"/> </div></div></td>
                          <td><input name="vencimentoParcelas[]" style="line-height: 20px;" type="date" value="'.$dataVencimento.'" class="form-control" required="required" /></td>
                      </tr>';
        }

        echo $html;
    }

    function buscarVendaAberta($nova=null) {

        if ($nova==null) $vendaAberta = $this->vendas_model->getVendaAberta();
        else $vendaAberta = null;

        if ($vendaAberta != null) return $vendaAberta->idVendas;
        else return $this->gerarUmaNovaVendaPDV();
    }

    function pagar() {

        $desconto       = $this->input->post('desconto');
        $acrescimo      = $this->input->post('acrescimo');
        $troco          = $this->input->post('troco');

        $dinheiro       = $this->input->post('amount');
        $cartaoCredito  = $this->input->post('cartaoCredito');
        $cartaoDebito   = $this->input->post('cartaoDebito');

        $totalPago      = $dinheiro + $cartaoCredito + $cartaoDebito - $troco;

        $venda          = $this->vendas_model->getById($this->input->post('vendas_id'));
        $valorParcelado = ($venda->valorTotal - $desconto + $acrescimo) - $totalPago;

        $lancamento = null;
        $lancamentoParcelado = null;

        if ($totalPago > 0) $lancamento = $this->pagarAVista($totalPago);
        if ($valorParcelado > 0) $lancamentoParcelado = $this->pagarParcelado($valorParcelado);

        echo json_encode($this->faturarVenda($desconto, $acrescimo, $totalPago, $valorParcelado,$lancamento, $lancamentoParcelado));
    }

    function faturarVenda($desconto,
                          $acrescimo,
                          $totalPago,
                          $valorParcelado,
                          $lancamentoId,
                          $lancamentoParceladoId) {

        $vendas_id      = $this->input->post('vendas_id');
        $dinheiro       = $this->input->post('amount');
        $cartaoCredito  = $this->input->post('cartaoCredito');
        $cartaoDebito   = $this->input->post('cartaoDebito');
        $troco          = $this->input->post('troco');
        $condicaoPagamentoParcelado   = $this->input->post('parcelas');

        if ($valorParcelado < 0) $valorParcelado = 0;

        $data = array(
            'faturado' => 1,
            'status' => 1,
            'espera' => 0,
            'condicaoPagamentoParcelado' => $condicaoPagamentoParcelado,
            'acrescimo' => $acrescimo,
            'desconto' => $desconto,
            'valorParcelado' => $valorParcelado,
            'totalPago' => $totalPago,
            'lancamentoId' => $lancamentoId,
            'lancamentoParceladoId' => $lancamentoParceladoId,
            'dinheiro' => $dinheiro,
            'cartao_credito' => $cartaoCredito,
            'cartao_debito' => $cartaoDebito,
            'troco' => $troco
        );

        $this->vendas_model->edit('vendas', $data, 'idVendas', $vendas_id);

        $this->atualizarEstoqueDosProdutos($vendas_id);

        return $this->vendas_model->getById($vendas_id);
    }

    function pagarParcelado($valorParcelado) {

        $vendas_id      = $this->input->post('vendas_id');
        $clientes_id    = $this->input->post('clientes_id');
        $forma_pgto     = $this->input->post('formaPgto');
        $payment_note   = $this->input->post('payment_note');
        $dtVencimentos  = $this->input->post('vencimentoParcelas');
        $valorVencimentos = $this->input->post('valorVencimento');

        $condicaoPagamento = $this->input->post('parcelas');
        $tipoCobranca   = $this->input->post('tipoCobranca');

        $desconto       = $this->input->post('desconto');
        $acrescimo      = $this->input->post('acrescimo');

        $cliente       = $this->clientes_model->getById($clientes_id);

        $data = array(
            'descricao'             => $payment_note,
            'valor'                 => $valorParcelado,
            'clientes_id'           => $clientes_id,
            'forma_pgto'            => $forma_pgto,
            'venda'                 => $vendas_id,
            'cliente_fornecedor'    => $cliente->nomeCliente,
            'condicao_pagamento_id' => $condicaoPagamento,
            'tipo_cobranca_id'      => $tipoCobranca,
            'data_pagamento'        => date('Y-m-d'),
            'data_vencimento'       => date('Y-m-d'),

            'baixado'               => 1,
            'custo'                 => 0,
            'tipo'                  => 'receita',
            'receita_id'            => 1,//TODO RECEITA DE VENDAS NO PDV
        );

        return $this->financeiro_model->add('lancamentos', $data, $valorVencimentos, $dtVencimentos, $desconto, $acrescimo);
    }

    function pagarAVista($totalPago) {

        $vendas_id      = $this->input->post('vendas_id');
        $clientes_id    = $this->input->post('clientes_id');
        $forma_pgto     = $this->input->post('formaPgto');
        $payment_note   = $this->input->post('payment_note');
        $condicaoAVista = 1;//TODO CONFIGURACAO A VISTA DA CONDICAO DE PAGAMENTO

        $desconto       = $this->input->post('desconto');
        $acrescimo      = $this->input->post('acrescimo');

        $dinheiro       = $this->input->post('amount');
        $cartaoCredito  = $this->input->post('cartaoCredito');
        $cartaoDebito   = $this->input->post('cartaoDebito');
        $troco          = $this->input->post('troco');

        $cliente       = $this->clientes_model->getById($clientes_id);

        $data = array(
            'descricao'             => $payment_note,
            'valor'                 => $totalPago,
            'clientes_id'           => $clientes_id,
            'forma_pgto'            => $forma_pgto,
            'venda'                 => $vendas_id,
            'cliente_fornecedor'    => $cliente->nomeCliente,
            'condicao_pagamento_id' => $condicaoAVista,
            'data_pagamento'        => date('Y-m-d'),
            'data_vencimento'       => date('Y-m-d'),

            'baixado'               => 1,
            'custo'                 => 0,
            'tipo'                  => 'receita',
            'receita_id'            => 1,//TODO RECEITA DE VENDAS NO PDV
        );

        $lancamentoAVistaId = $this->financeiro_model->add('lancamentos', $data, [$totalPago] , [date('Y-m-d')], $desconto, $acrescimo) ;
        $parcelas = $this->financeiro_model->getParcelasByLancamento($lancamentoAVistaId);

        foreach ($parcelas as $parcela) {
            if ($dinheiro > 0) $this->adicionarPagamento($parcela->idParcela, $dinheiro - $troco, 1);
            if ($cartaoDebito > 0) $this->adicionarPagamento($parcela->idParcela, $cartaoDebito, 4);
            if ($cartaoCredito > 0) $this->adicionarPagamento($parcela->idParcela, $cartaoCredito, 5);
        }

        return $lancamentoAVistaId;
    }

    public function adicionarPagamento($parcelaId, $valorPagamentos, $formaPagamento) {
        $this->financeiro_model->pagar($parcelaId, [$formaPagamento], [date('Y-m-d')], [$valorPagamentos], '');
    }

    function atualizarEstoqueDosProdutos($vendas_id) {

        $produtos   = $this->vendas_model->getProdutos($vendas_id);

        foreach ($produtos as $produto) {

            $idItens        = $produto->idItens;
            $quantidade     = $produto->quantidade;
            $produtos_id    = $produto->produtos_id;
            $vendas_id      = $produto->vendas_id;
            $filial         = $produto->filial_id;

            if ($quantidade > 0) $this->estoque_model->saidaEstoque($quantidade, $produtos_id, 'Venda PDV', $vendas_id, $idItens, $filial);
        }
    }

    function consultarPagamentoVendaPDV() {
        $venda_id = $this->input->post('vendas_id');
        echo json_encode( $this->vendas_model->getById($venda_id));
    }

    function gerarLinkPagamentoPDV() {

        $venda_id = $this->input->post('venda_id');
        $parcelas = $this->input->post('parcelas');
        $faltaPagar = $this->input->post('faltaPagar');
        $token = $this->site_model->geraToken();

        $data = array(
            'token' => $token,
            'url' => base_url() . 'pag?token=' . $token . '&cnpj=' . $this->session->userdata('cnpjempresa'),
            'modo_parcelamento' => 'interest_free',
            'forma_pagamento' => 'credito',
            'valor' => $faltaPagar,
            'validade' => date('Y-m-d'),
            'venda_id' =>  $venda_id,
            'condicao_pagamento' =>  $parcelas,

            'descricao' => 'VENDA NO PDV',//TODO LISTAGEM DOS ITENS.
            'status' => 'ATIVO',
            'numero_parcelas' => 1,
        );

        $linkpagamentoId = $this->linkpagamento_model->add('link_pagamento', $data);

        echo json_encode( $this->linkpagamento_model->getById($linkpagamentoId));
    }

    function visualizar() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vVenda')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para visualizar vendas.');
            //redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->vendas_model->getById($this->uri->segment(3));
        $this->data['cliente'] = $this->vendas_model->getClienteVendaById($this->uri->segment(3));
        $this->data['produtos'] = $this->vendas_model->getProdutos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();

        $this->data['view'] = 'pdv/visualizarVendaPDV';
        $this->load->view('tema/topo', $this->data);
    }

    function impressao() {

        $venda =  $this->vendas_model->getById($this->uri->segment(3));
        $this->data['cliente'] = $this->vendas_model->getClienteVendaById($this->uri->segment(3));
        $this->data['produtos'] = $this->vendas_model->getProdutos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->data['pagamentos'] = $this->financeiro_model->getPagamentoByVenda($this->uri->segment(3));
        $this->data['result'] = $venda;

        $this->data['view'] = 'pdv/impressao';
        $this->load->view('tema/blank', $this->data);
    }

    function orcamento() {

        $this->data['result'] = $this->vendas_model->getById($this->uri->segment(3));
        $this->data['cliente'] = $this->vendas_model->getClienteVendaById($this->uri->segment(3));
        $this->data['produtos'] = $this->vendas_model->getProdutos($this->uri->segment(3));

        $this->db->limit(1);
        $this->db->where('emitente.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id', 'left');
        $this->data['emitente'] = $this->db->get('emitente')->row();

        $this->data['pagamentos'] = $this->financeiro_model->getPagamentoByVenda($this->uri->segment(3));

        $this->data['view'] = 'pdv/orcamento';
        $this->load->view('tema/blank', $this->data);
    }

    function gerarNFCE() {

        $cpf = $this->input->get('cpf');

        $venda              = $this->vendas_model->getById($this->uri->segment(3));
        $produtos           = $this->vendas_model->getProdutos($this->uri->segment(3));
        $clientes           = $this->clientes_model->getById($venda->clientes_id);
        $configuracao       = $this->configuracao_model->getById(1);

        $naturezaOperacao   = $this->naturezaoperacao_model->getById($configuracao->naturezaoperacaodentroestado_id);
        $CFOP               = $naturezaOperacao->CFOP;
        $ICMS               = $naturezaOperacao->ICMS;
        $desconto           = $venda->desconto;
        $acrescimo          = $venda->acrescimo;
        $numeroDeSerieNFCE = $configuracao->numeroDeSerieNFCE;

        $infCpl = 'Venda: '.$venda->idVendas;

        if ($acrescimo > 0) $infCpl .= ' ACRESCIMO FINANCEIRO DE R$ '.number_format($acrescimo, 2, ',', ' ');

        if (count($produtos) == 0)  redirect(base_url() . 'index.php/pdv/visualizar/'.$this->uri->segment(3));

        if ($cpf==null)  $destCNPJ   = $this->formatarNumeroDocumento($clientes->documento);
        else $destCNPJ = $this->formatarNumeroDocumento($cpf);

        $destCEP    = $this->formatarCEP($clientes->cep);
        $nNF        = $this->nfce_model->getProximoNumeroNFCE();

        $nfe = array(
            'modelo'   => '65',
            'serie'   => $numeroDeSerieNFCE,
            'natOp'   => 'VENDA DE MERCADORIA',
            'dhEmi'   => date('Y-m-d'),
            'hEmi'   =>  date('H:i'),
            'nNF'   => $nNF,
            'vDescnf' => $desconto,
            'vOutronf' => $acrescimo,
            'vFretenf' => 0,
            'vTroco'   => $venda->troco,
            'indPag'   => 0,//FORMA DE PAGAMENTO
            'tpEmis'   => 1,//Forma Emi.*
            'finNFe'   => 1,//Finalidade
            'indPres'   => 1,//Tipo Atend.
            'modFrete'   => 0,//Tipo Frete*

            'destxNome'   => $clientes->nomeCliente,
            'destCNPJ'   => $destCNPJ,
            'destIE'   => $clientes->IE,
            'destISUF'   => $clientes->IESUF,
            'destxLgr'   =>  $clientes->rua,
            'destnro'   =>  $clientes->numero,
            'destxCpl'   => $clientes->complemento,
            'destCEP'   => $destCEP,
            'destxBairro'   => $clientes->bairro,
            'destxMun'   => $clientes->cidade,
            'destcMun'   => $clientes->codIBGECidade,
            'destUF'   => $clientes->estado,
            'destfone'   => $clientes->telefone,
            'destemail'   => $clientes->email,
            'clientes_id'   => $venda->clientes_id,
            'usuarios_id'   => $venda->usuarios_id,
            'naturezaoperacao_id'   => $configuracao->naturezaoperacaodentroestado_id,
            'infCpl' => $infCpl
        );

        $this->nfce_model->add('nfce', $nfe, true);

        $acrescimoProduto = 0;

        if ($acrescimo > 0) {
            $acrescimoProduto = ($acrescimo/count($produtos));
        }

        foreach ($produtos as $pd) {

            $produto        = $this->produtos_model->getById($pd->produtos_id);

            $quantidade     = $pd->quantidade;
            $valorVenda     = $pd->valorVenda;
            $subTotal       = $pd->subTotal;

            $vICMS          = 0;
            $vICMSST        = 0;
            $vBCST          = 0;
            $vBC            = 0;

            $pICMS          = 0;
            $pICMSST        = $produto->pICMSST;

            if ($pICMS > 0) {
                $vICMS = ($subTotal*$pICMS)/100;
                $vBC = $subTotal;
            }

             if ($pICMSST > 0 ) {
                $vICMSST = ($subTotal*$pICMSST)/100;
                $vBCST   = $subTotal;
            }

            //IPI
            $CSTIPI = $produto->CSTIPI;
            $pIPI   = $produto->pIPI;
            $vIPI   = 0;
            $vBCIPI = 0;

            if ($pIPI > 0 ) {
                $vIPI = ($subTotal*$pIPI)/100;
                $vBCIPI   = $subTotal;
            }

            //PIS
            $CSTPIS = $produto->CSTPIS;
            $pPIS   = $produto->pPIS;
            $vPIS   = 0;
            $vBCPIS = 0;

            if ($pPIS > 0 ) {
                $vPIS = ($subTotal*$pPIS)/100;
                $vBCPIS   = $subTotal;
            }

            //COFINS
            $CSTCOFINS = $produto->CSTCOFINS;
            $pCOFINS   = $produto->pCOFINS;
            $vCOFINS   = 0;
            $vBCCOFINS = 0;

            if ($pCOFINS > 0 ) {
                $vCOFINS = ($subTotal*$pCOFINS)/100;
                $vBCCOFINS   = $subTotal;
            }

            if ($produto->CFOP != '') $CFOP = $produto->CFOP;
            if ($produto->CST != '') $ICMS = $produto->CST;

            $data_produto = array(

                'NFCe'   => $nNF,
                'produtoid' => $pd->produtos_id,
                'cProd'   => $produto->cProd,
                'xProd'   => $produto->descricao,
                'uCom'   => $produto->unidade,

                'qtd'   => $quantidade,
                'vProd'   => $valorVenda,
                'vlrtotal'   => $subTotal,
                'valortotal'   => $subTotal,
                'vOutro' => $acrescimoProduto,

                'NCM'   =>  $produto->ncm,
                'CEST'   => $produto->CEST,
                'CFOP'   => $CFOP,

                'cProdANP'  => $produto->cProdANP,
                'descANP'   => $produto->descANP,
                'UFCons'   => $produto->UFCons,

                //ICMS
                'orig'      => $produto->origem,
                'CST'       => $ICMS,

                //IMPOSTOS
                'vTotTrib' => ($vICMS + $vBCST + $vIPI + $vPIS + $vCOFINS),

                //ICMS
                'pICMS'     => $pICMS,
                'vICMS'     => $vICMS,
                'vBC'       => $vBC,

                //ICMS ST
                'modBCST'   => $produto->modBCST,
                'pICMSST'   => $pICMSST,
                'vICMSST'   => $vICMSST,
                'vBCST'     => $vBCST,


                //IPI
                'CSTIPI'    => $CSTIPI,
                'pIPI'      => $pIPI,
                'vIPI'     => $vIPI,
                'vBCIPI'    => $vBCIPI,

                //PIS
                'CSTPIS'    => $CSTPIS,
                'pPIS'      => $pPIS,
                'vPIS'     => $vPIS,
                'vBCPIS'    => $vBCPIS,

                //COFINS
                'CSTCOFINS'    => $CSTCOFINS,
                'pCOFINS'      => $pCOFINS,
                'vCOFINS'      => $vCOFINS,
                'vBCCOFINS'    => $vBCCOFINS,

            );
            $this->nfce_model->add('produtonfe', $data_produto, true);
        }

        $this->atualizarTotalizadorNFE($nNF, $venda);

        $this->db->set('nfcegerada', 1);
        $this->db->set('nNF', $nNF);
        $this->db->where('idVendas', $this->uri->segment(3));
        $this->db->update('vendas');

        echo json_encode($this->nfce_model->getById($nNF));
    }

    function ibpt() {

        $url = 'http://localhost/rt-php-sos.net-1.0/emissores/v4/sped-ibpt-master/geracao/busca_ibpt.php?ncm=02032900&xProd=Teste&xUN=UN&valor=78&BD='.$this->session->userdata('cnpjempresa');

        $headers = array(
            "Content-Type:application/json"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
        $obj = json_decode($response);

        echo  ($obj->Codigo);
    }

    function atualizarTotalizadorNFE($nNF, $venda) {

        $produtos   = $this->nfce_model->getProdutos($nNF);
        $vDescnf    = $venda->desconto;
        $acrescimo = $venda->acrescimo;

        $dinheiro = $venda->dinheiro;
        $cartaoCredito = $venda->cartao_credito;
        $cartaoDebito = $venda->cartao_debito;
        $valorParcelado = $venda->valorParcelado;

        $vIPInf     = 0;
        $vSegnf     = 0;
        $vFretenf   = 0;

        $total      = 0;
        $vTotTrib   = 0;

        $vBC        = 0;
        $vBCST      = 0;
        $vICMSST    = 0;
        $vICMS      = 0;
        $vIPI       = 0;
        $vPIS       = 0;
        $vCOFINS    = 0;

        foreach ($produtos as $produto) {

            $total        = $total + $produto->vlrtotal;
            $vTotTrib     = $vTotTrib + $produto->vTotTrib;

            //aliquotas
            $vBC        = $vBC + $produto->vBC;
            $vICMS      = $vICMS + $produto->vICMS;

            $vBCST      = $vBCST + $produto->vBCST;
            $vICMSST    = $vICMSST + $produto->vICMSST;

            $vIPI       = $vIPI + $produto->vIPI;
            $vPIS       = $vPIS + $produto->vPIS;
            $vCOFINS    = $vCOFINS + $produto->vCOFINS;
        }

        $valortotalnf = $total + $vFretenf + $vSegnf + $vIPInf + $acrescimo - $vDescnf;

        $tpag = '01';//TODO AJUSTAR!!

        $data = array(

            'vBCnf'          => $vBC,
            'vICMSnf'        => $vICMS,

            'vBCSTnf'        => $vBCST,
            'vSTnf'          => $vICMSST,

            'vIPInf'         => $vIPI,
            'vPISnf'         => $vPIS,
            'vCOFINSnf'      => $vCOFINS,

            'vSegnf'        => $vSegnf,
            'vDescnf'       => $vDescnf,

            'vNF'            => $valortotalnf,
            'vProdnf'        => $total,

            'tpag'          => $tpag,
            'vPag'          => $valortotalnf,

            'dinheiro' => $dinheiro,
            'cartao_credito' => $cartaoCredito,
            'cartao_debito' => $cartaoDebito,
            'valorParcelado' => $valorParcelado
        );
        $this->nfce_model->edit('nfce', $data, 'nNF', $nNF);
    }

    function excluirVenda()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dVenda')) {
            //$this->session->set_flashdata('error', 'Você não tem permissão para excluir vendas');
            //redirect(base_url());
        }

        $id = $this->uri->segment(3);

        if ($id == null) {
            $this->session->set_flashdata('error', 'Erro ao tentar excluir venda.');
            redirect(base_url() . 'index.php/vendas/gerenciar/');
        }

        $this->db->where('vendas_id', $id);
        $this->db->delete('itens_de_vendas');

        $this->db->where('idVendas', $id);
        $this->db->delete('vendas');

        $this->session->set_flashdata('success', 'Venda excluída com sucesso!');
        redirect(base_url() . 'index.php/pdv');
    }

    public function adicionarProdutoPDV()
    {

        $preco      = $this->input->post('preco');
        $custo      = $this->input->post('custo');
        $valorVenda = $this->input->post('valorVenda');
        $quantidade = $this->input->post('quantidade');
        $produto    = $this->input->post('idProduto');

        if ($valorVenda) $subtotal = $valorVenda * $quantidade;
        else $subtotal = $preco * $quantidade;

        $data = array(
            'quantidade'    => $quantidade,
            'custo'         => $custo,
            'valorVenda'    => $valorVenda,
            'subTotal'      => $subtotal,
            'produtos_id'   => $produto,
            'vendas_id'     => $this->input->post('idVendasProduto'),
        );

        $idItens = $this->vendas_model->add('itens_de_vendas', $data, TRUE);

        if ($idItens) {
            $item = $this->vendas_model->getVendaItemByVenda($idItens);
            $sql = "UPDATE vendas set valorTotal = valorTotal + ? WHERE idVendas = ? ";
            $this->db->query($sql, array($subtotal, $item->vendas_id) );

            echo json_encode($this->vendas_model->getItemVendaById($idItens));
        }
    }

    function montarTabelaDeProdutos() {

        $venda_id      = $this->input->post('venda_id');
        $grupo_id      = $this->input->post('grupo_id');
        $configuracao = $this->configuracao_model->getConfiguracao();
        $permiteEstoqueNegativo = $configuracao->permiteEstoqueNegativo;

        if ($grupo_id==null) $grupo_id = 1;

        $produtos = $this->produtos_model->getProdutosFiliais('',null,null, $grupo_id);

        $html = '<div id="item-list" style="height: 473px; min-height: 515px;">';

        foreach ($produtos as $produto){

            $reservado = $this->produtos_model->getProdutosPendenteVenda($produto->idProdutos)->quantidade;
            $estoque = $produto->estoque-$reservado;

            if (!$permiteEstoqueNegativo && $estoque <= 0) continue;

            $html .= '<button id="'.$produto->idProdutos.'" vendas_id="'.$venda_id.'" custo="'.$produto->precoCompra.'" preco="'.$produto->precoVenda.'" type="button" value="'.$produto->idProdutos.'" title="" class="btn-prni btn-default product pos-tip">';

            if ($produto->imagem) {
                $html .= '<img src="'.base_url().'/assets/arquivos/produto/'.$produto->idProdutos.'/'.$produto->imagem.'" alt="'.$produto->produto.'" style="width:60px;height:60px;" class="img-rounded">';
            } else {
                $html .='<img src="'.base_url().'/assets/arquivos/sem_foto.png" alt="'.$produto->produto.'" style="width:60px;height:60px;" class="img-rounded">';
            }

            $html .= '<div class="estoque">'.$estoque.' '.$produto->unidade.'</div>';
            $html .= '<span>'.$produto->produto.'</span>';
            $html .='<div class="nome_preco">R$' .number_format($produto->precoVenda, 2, ',', '.').' '.$produto->unidade.'</div>';
            $html .='</button>';
        }
        $html .='</div>';

        echo $html;
    }

    function montarTabelaDePedidosEmEspera() {
        $esperas = $this->vendas_model->getAllPedidosEmEspera();

        $html = '<div id="item-list" style="height: 473px; min-height: 515px;">';

        foreach ($esperas as $pedido) {

            $background = '';
            if ($pedido->faturado == '1') $background = 'background: url('.base_url().'assets/js/images/sucesso.gif);background-size: 160px;';
            else if ($pedido->status == '2') $background = 'background: url(https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSvXRlixZn7Q_F33fU42HSbrpjPukw8oU9KLwSQ-331wBXvHiVu);background-size: 180px;';

            $html .= '<button style="'.$background.'" onclick="abrirVenda('.$pedido->idVendas.')" type="button" value="<?php echo $pedido->idVendas;?>" title="" class="btn-prni btn-default product pos-tip">';
            $html .= '<span>'.$pedido->nomeCliente.'</span>';
            $html .= '  <p> #'.$pedido->idVendas.' - '.$pedido->referencia_espera.'</p>';
            $html .= '  <p>'.date(('d/m/Y'), strtotime( $pedido->dataVenda)).'</p>';
            $html .= '  <div class="nome_preco">R$'.number_format($pedido->valorTotal, 2, ",", ".").'</div>';
            $html .= '</button>';
        }
        $html .='</div>';
        echo $html;
    }

    function atualizarProdutoPDV() {

        $idItens    = $this->input->post('idItens');
        $quantidade = $this->input->post('quantidade');
        $valorVenda = $this->input->post('valorVenda');
        $subTotal = ($quantidade*$valorVenda);

        $data = array(
            'quantidade'    => $quantidade,
            'valorVenda'    => $valorVenda,
            'subTotal'      => $subTotal,
        );

        $item = $this->vendas_model->getVendaItemByVenda($idItens);
        $sql = "UPDATE vendas set valorTotal = valorTotal - ? WHERE idVendas = ? ";
        $this->db->query($sql, array($item->subTotal, $item->vendas_id) );

        $item = $this->vendas_model->getVendaItemByVenda($idItens);
        $sql = "UPDATE vendas set valorTotal = valorTotal + ? WHERE idVendas = ? ";
        $this->db->query($sql, array($subTotal, $item->vendas_id) );

        $this->vendas_model->edit('itens_de_vendas', $data, 'idItens', $idItens);
        echo json_encode(array('result' => true));
    }

    function atualizarClientePDV() {

        $idVenda    = $this->input->post('idVenda');
        $cliente_id = $this->input->post('clientes_id');

        $data = array('clientes_id' => $cliente_id);

        $this->vendas_model->edit('vendas', $data, 'idVendas', $idVenda);
    }

    function vendaNaoFiscal() {
        $vendaId    = $this->input->post('vendaId');
        $this->vendas_model->edit('vendas', array('fiscal' => 0) , 'idVendas', $vendaId);
        echo json_encode(array('result' => true));
    }

    function vendaFiscal() {
        $vendaId    = $this->input->post('vendaId');
        $this->vendas_model->edit('vendas', array('fiscal' => 1) , 'idVendas', $vendaId);
        echo json_encode(array('result' => true));
    }

    function porVendaEmEspera() {
        $vendas_id = $this->input->post('vendas_id');
        $referencia_espera = $this->input->post('referencia_espera');

        $this->vendas_model->porVendaEmEspera($vendas_id, $referencia_espera);

        echo json_encode(array('id' =>  $this->gerarUmaNovaVendaPDV()));
    }

    function finalizarVendaEmEspera() {
        $vendas_id = $this->input->post('vendas_id');

        $this->vendas_model->finalizarVendaEmEspera($vendas_id);
    }

    function cancelar() {
        $vendas_id = $this->input->post('vendas_id');
        $link_pagamento = $this->input->post('link_pagamento');

        $this->vendas_model->cancelar($vendas_id);
        $this->linkpagamento_model->inativar($link_pagamento);
    }

    function adicionarClientePDV() {

        $vendaId = $this->input->post('vendaId');
        $clienteId = $this->input->post('clienteId');

        $data = array(
            'origem'                => 'Particular',
            'tipoPessoa'            => $this->input->post('tipoPessoa'),
            'nomeCliente'           => $this->input->post('nomeCliente'),
            'documento'             => $this->input->post('documento'),
            'telefone'              => $this->input->post('telefone'),
            'celular'               => $this->input->post('celular'),
            'email'                 => $this->input->post('email'),
            'dataCadastro'          => date('Y-m-d'),
        );

        if ($clienteId != '') {
            $this->clientes_model->edit('clientes', $data, 'idClientes', $clienteId);
        } else {
            $clienteId = $this->clientes_model->add('clientes', $data, TRUE);
        }

        $this->vendas_model->edit('vendas', array('clientes_id' => $clienteId), 'idVendas', $vendaId);

        echo json_encode($this->clientes_model->getById($clienteId));
    }

    function excluirProdutoPDV() {

        $idItens = $this->input->post('idItens');
        $item = $this->vendas_model->getVendaItemByVenda($idItens);
        $sql = "UPDATE vendas set valorTotal = valorTotal - ? WHERE idVendas = ? ";
        $this->db->query($sql, array($item->subTotal, $item->vendas_id) );

        if ($this->vendas_model->delete('itens_de_vendas', 'idItens', $idItens) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    function cadCliente() {
        $this->data['view'] = 'pdv/cadCliente';
        $this->load->view('tema/topo', $this->data);
    }

    function formatarNumeroDocumento($destCNPJ) {
        $destCNPJ   = str_replace('.','', $destCNPJ);
        $destCNPJ   = str_replace('-','', $destCNPJ);
        $destCNPJ   = str_replace('/','', $destCNPJ);

        return $destCNPJ;
    }

    function formatarCEP($destCEP) {
        $destCEP    = str_replace('.','', $destCEP);
        $destCEP    = str_replace('-','', $destCEP);
        $destCEP    = str_replace('/','', $destCEP);
        return $destCEP;
    }
}

