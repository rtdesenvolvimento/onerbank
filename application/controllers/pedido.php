<?php

class pedido extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('pedido_model', '', TRUE);
        $this->load->model('fornecedor_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('forma_pagamento_model', '', TRUE);
        $this->load->model('condicao_pagamento_model', '', TRUE);
        $this->load->model('historico_estoque_model', '', TRUE);
        $this->load->model('estoque_model', '', TRUE);

        $this->data['menuPedido'] = 'Pedido de compra';
        $this->data['menuCompra'] = 'Compras';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar pedido.');
            redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/pedido/gerenciar/';
        $config['total_rows'] = $this->pedido_model->count('pedido');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->pedido_model->get('pedido', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'pedido/pedido';
        $this->load->view('theme/topo', $this->data);

    }

    function adicionar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aPedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar pedido.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('pedido') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $dataPedido = $this->input->post('dataPedido');

            $data = array(
                'dataPedido' => $dataPedido,
                'fornecedor_id' => $this->input->post('fornecedor_id'),
                'usuarios_id' => $this->input->post('usuarios_id'),
                'faturado' => 0
            );

            if (is_numeric($id = $this->pedido_model->add('pedido', $data, true))) {
                $this->session->set_flashdata('success', 'Pedido iniciada com sucesso, adicione os produtos.');
                redirect('pedido/editar/' . $id);

            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['fornecedores'] = $this->fornecedor_model->getAll();
        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['view']         = 'pedido/adicionarPedido';
        $this->load->view('theme/topo', $this->data);
    }


    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'ePedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar pedido');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('pedido') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $dataPedido             = $this->input->post('dataPedido');
            $forma_pagamento_id     = $this->input->post('forma_pagamento_id');
            $condicao_pagamento_id  = $this->input->post('condicao_pagamento_id');
            $data_forma = array();
            $data_condicao = array();

            $data = array(
                'dataPedido'        => $dataPedido,
                'usuarios_id'       => $this->input->post('usuarios_id'),
                'fornecedor_id'     => $this->input->post('fornecedor_id'),
                'previsaoEntrega'   => $this->input->post('previsaoEntrega'),
                'valorFrete'        => $this->input->post('valorFrete'),
                'desconto'          => $this->input->post('desconto'),
            );

            if ($forma_pagamento_id) {
                $data_forma = array(
                    'forma_pagamento_id' => $forma_pagamento_id
                );
            }

            if ($condicao_pagamento_id) {
                $data_condicao = array(
                    'condicao_pagamento_id' => $condicao_pagamento_id
                );
            }

            $dataToo = array_merge ( $data,  $data_forma, $data_condicao);

            if ($this->pedido_model->edit('pedido', $dataToo, 'idPedido', $this->input->post('idPedido')) == TRUE) {
                $this->session->set_flashdata('success', 'Pedido editado com sucesso!');
                redirect(base_url() . 'index.php/pedido/editar/' . $this->input->post('idPedido'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['fornecedores']         = $this->fornecedor_model->getAll();
        $this->data['formasPagamento']      = $this->forma_pagamento_model->getAll();
        $this->data['condicoesPagamento']   = $this->condicao_pagamento_model->getAll();
        $this->data['usuarios']             = $this->usuarios_model->getAll();
        $this->data['produtosList']         = $this->produtos_model->getAll();
        $this->data['result']               = $this->pedido_model->getById($this->uri->segment(3));
        $this->data['produtos']             = $this->pedido_model->getProdutos($this->uri->segment(3));
        $this->data['view']                 = 'pedido/editarPedido';
        $this->load->view('theme/topo', $this->data);

    }

    public function editarEnderecoEntrega() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'ePedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar pedido.');
            redirect(base_url());
        }

        $idPedido   = $this->input->post('idPedido');
        $cepEntrega = $this->input->post('cepEntrega');
        $ruaEntrega = $this->input->post('ruaEntrega');
        $numeroEntrega = $this->input->post('numeroEntrega');
        $complementoEntrega = $this->input->post('complementoEntrega');
        $bairroEntrega = $this->input->post('bairroEntrega');
        $cidadeEntrega = $this->input->post('cidadeEntrega');
        $estadoEntrega = $this->input->post('estadoEntrega');

        $data = array(
            'cepEntrega' => $cepEntrega,
            'ruaEntrega' => $ruaEntrega,
            'numeroEntrega' => $numeroEntrega,
            'complementoEntrega' => $complementoEntrega,
            'bairroEntrega' => $bairroEntrega,
            'cidadeEntrega' => $cidadeEntrega,
            'estadoEntrega' => $estadoEntrega,
        );

        if ($this->pedido_model->edit('pedido', $data, 'idPedido',$idPedido) == TRUE) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function editarObservacaoPedido() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'ePedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar pedido.');
            redirect(base_url());
        }

        $idPedido   = $this->input->post('idPedido');
        $observacao = $this->input->post('observacao');

        $data = array(
            'observacao' => $observacao,
        );

        if ($this->pedido_model->edit('pedido', $data, 'idPedido',$idPedido) == TRUE) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function emOrcamentoFornecedor() {
        $pedidoId   =  $this->uri->segment(3);
        $data = array(
            'status' => 'Em Orçamento Fornecedor',
        );
        $this->pedido_model->edit('pedido', $data, 'idPedido',$pedidoId);
        $this->session->set_flashdata('success', 'Pedido entregue com sucesso!');

        $this->editar();
    }

    public function orcamentoConfirmado() {
        $pedidoId   =  $this->uri->segment(3);
        $data = array(
            'status' => 'Pendente de entrega',
        );
        $this->pedido_model->edit('pedido', $data, 'idPedido',$pedidoId);
        $this->session->set_flashdata('success', 'Pedido entregue com sucesso!');

        redirect('pedido/editar/' . $pedidoId);
    }

    public function entregar() {

        $pedidoId   = $this->uri->segment(3);
        $itens      = $this->pedido_model->getProdutos($pedidoId);

        foreach ($itens as $r) {

            $idItens    = $r->idItens;
            $quantidade = $r->quantidade;
            $produto    = $r->produtos_id;
            $filial     = $r->filial_id;
            $custo      = $r->custo;
            $pedido_id  = $r->pedido_id;

            if ($custo > 0) $this->estoque_model->alterarPrecoCompra($custo, $produto, $filial);
            if ($quantidade > 0) $this->estoque_model->entradaEstoque($quantidade, $produto, 'Pedido de compra', $pedido_id, $idItens, $filial);
        }

        $data = array(
            'status'    => 'Entregue',
            'faturado'  => 1
        );

        $this->pedido_model->edit('pedido', $data, 'idPedido',$pedidoId);

        $this->faturar();
    }


    public function faturar()
    {

        $vencimento     = $this->input->post('vencimento');
        $recebimento    = $this->input->post('recebimento');
        $pedidoId       = $this->uri->segment(3);

        if (!$vencimento) {
            $vencimento = date('Y/m/d');
        }

        $data = array(
            'descricao'             => $this->input->post('descricao'),
            'custo'                 => 0,
            'valor'                 => $this->input->post('valor'),
            'fornecedor_id'         => $this->input->post('fornecedor_id'),
            'data_vencimento'       => $vencimento,
            'data_pagamento'        => $recebimento,
            'baixado'               => $this->input->post('recebido'),
            'cliente_fornecedor'    => set_value('fornecedor'),
            'forma_pgto'            => $this->input->post('formaPgto'),
            'pedido'                => $pedidoId,
            'tipo'                  => $this->input->post('tipo')
        );

        if ($this->pedido_model->add('lancamentos', $data, [$this->input->post('valor')], [$vencimento], 0) == TRUE) {
            $json = array('result' => true);
            echo json_encode($json);
            die();
        } else {
            $this->session->set_flashdata('error', 'Ocorreu um erro ao tentar faturar venda.');
            $json = array('result' => false);
            echo json_encode($json);
            die();
        }
    }

    public function visualizar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar pedido.');
            redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->pedido_model->getById($this->uri->segment(3));
        $this->data['cliente'] = $this->pedido_model->getFornecedorPedidoById($this->uri->segment(3));
        $this->data['produtos'] = $this->pedido_model->getProdutos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();

        $this->data['view'] = 'pedido/visualizarPedido';
        $this->load->view('theme/topo', $this->data);

    }

    function cancelar() {
        $pedidoId   =  $this->uri->segment(3);
        $itens      = $this->pedido_model->getProdutos($pedidoId);

        foreach ($itens as $r) {

            $idItens    = $r->idItens;
            $quantidade = $r->quantidade;
            $produto    = $r->produtos_id;
            $filial     = $r->filial_id;
            $custo      = $r->custo;
            $pedido_id  = $r->pedido_id;

            if ($custo > 0) $this->estoque_model->alterarPrecoCompra($custo, $produto, $filial);
            if ($quantidade > 0) $this->estoque_model->saidaEstoque($quantidade, $produto, 'Pedido de compra (cancelamento)', $pedido_id, $idItens, $filial);
        }

        $this->db->where('pedido', $pedidoId);
        $this->db->delete('lancamentos');

        $data = array(
            'status' => 'Cancelado',
        );
        $this->pedido_model->edit('pedido', $data, 'idPedido',$pedidoId);
        $this->session->set_flashdata('success', 'Pedido Cancelado com sucesso!');

        redirect('pedido/editar/' . $pedidoId);
    }

    function excluir()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dPedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para excluir pedido');
            redirect(base_url());
        }

        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir pedido.');
            redirect(base_url() . 'index.php/pedido/gerenciar/');
        }

        $this->db->where('pedido_id', $id);
        $this->db->delete('itensPedido');

        $this->db->where('idPedido', $id);
        $this->db->delete('pedido');

        $this->session->set_flashdata('success', 'Pedido excluído com sucesso!');
        redirect(base_url() . 'index.php/pedido/gerenciar/');

    }

    public function autoCompleteProduto()
    {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->pedido_model->autoCompleteProduto($q);
        }

    }


    public function adicionarProduto()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'ePedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar pedido.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idProduto', 'Produto', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idPedido', 'Pedido', 'trim|required|xss_clean');

        if ($this->form_validation->run() == false) {
            echo json_encode(array('result' => false));
        } else {

            $custo = $this->input->post('custo');
            $quantidade = $this->input->post('quantidade');
            $produto = $this->input->post('idProduto');
            $subtotal = $custo * $quantidade;
            $pedido_id = $this->input->post('idPedido');

            $data = array(
                'quantidade'    => $quantidade,
                'custo'         => $custo,
                'subTotal'      => $subtotal,
                'produtos_id'   => $produto,
                'pedido_id'     => $pedido_id,
            );

            if ($this->pedido_model->add('itensPedido', $data) == true) {
                $sql = "UPDATE pedido set valorTotal = valorTotal + ? WHERE idPedido = ? ";
                $this->db->query($sql, array($subtotal, $pedido_id) );
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        }
    }

    function excluirProduto()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'ePedido')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar pedido');
            redirect(base_url());
        }

        $ID = $this->input->post('idProduto');
        $item = $this->pedido_model->getPedidoItemByPedido($ID);

        if ($this->pedido_model->delete('itensPedido', 'idItens', $ID) == true) {
            $sql = "UPDATE pedido set valorTotal = valorTotal - ? WHERE idPedido = ? ";
            $this->db->query($sql, array($item->subTotal, $item->pedido_id) );
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
}

