<?php

class plano extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('plano_model', '', TRUE);
        $this->data['menuPlano'] = 'Plano';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPlano')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar plano.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/plano/gerenciar/';
        $config['total_rows'] = $this->plano_model->count('plano');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->data['results'] = $this->plano_model->get('plano','*','',$config['per_page'],$this->uri->segment(3));
        $this->data['output'] = 'plano/plano';
        $this->load->view('master/template', $this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aPlano')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar planos.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('plano') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao')
            );

            if ($this->plano_model->add('plano', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Plano adicionado com sucesso!');
                redirect(base_url() . 'index.php/plano/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['output'] = 'plano/adicionarPlano';
        $this->load->view('master/template', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'ePlano')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar planos.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('plano') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->plano_model->edit('plano', $data, 'idPlano', $this->input->post('idPlano')) == TRUE) {
                $this->session->set_flashdata('success', 'Setor editado com sucesso!');
                redirect(base_url() . 'index.php/plano/editar/'.$this->input->post('idPlano'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->plano_model->getById($this->uri->segment(3));
        $this->data['output'] = 'plano/editarPlano';
        $this->load->view('master/template', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dPlano')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir planos.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir plano.');
            redirect(base_url().'index.php/plano/gerenciar/');
        }

        $this->plano_model->delete('plano','idPlano',$id);

        $this->session->set_flashdata('success','Plano excluido com sucesso!');
        redirect(base_url().'index.php/plano/gerenciar/');
    }
}

