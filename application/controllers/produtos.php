<?php

class Produtos extends CI_Controller {
    
    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('setor_model', '', TRUE);
        $this->load->model('filial_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->load->model('icms_model', '', TRUE);
        $this->load->model('estoque_model', '', TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('fornecedor_model', '', TRUE);

        $this->data['menuProdutos'] = 'Produtos';
        $this->data['menuEstoque'] = 'Estoque';
    }

    function index(){
	   $this->gerenciar();
    }

    function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');
        
        $config['base_url']     = base_url().'index.php/produtos/gerenciar/';
        $config['total_rows']   = $this->produtos_model->count('produtos');
        $config['per_page']     = 100;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);

        $this->data['results']  = $this->produtos_model->getProdutos('',$config['per_page'],$this->uri->segment(3));
        $this->data['fornecedores'] = $this->fornecedor_model->getAll();

        $this->data['termo']    = '';

        $this->data['view']     = 'produtos/produtos';
       	$this->load->view('theme/topo',$this->data);
    }
	
	function buscar() {
	
		if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');
        
        $config['base_url'] = base_url().'index.php/produtos/gerenciar/';
        $config['total_rows'] = $this->produtos_model->count('produtos');
        $config['per_page']     = 100;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $this->pagination->initialize($config); 	
		$parametro = $termo = $this->input->post('termo');
        $parametro = str_replace("%20", "%" , $parametro);
        $parametro = str_replace(" ", "%" , $parametro);

        $where = '';
		if ($parametro) {
			$where = 'p.cEAN = "'.$parametro.'" or p.cProd = "'.$parametro.'" or p.descricao like "%'.$parametro.'%" or s.nome like "%'.$parametro.'%" or g.nome like "%'.$parametro.'%" ';
		}

        $this->data['results']  = $this->produtos_model->getProdutos($where,$config['per_page'],$this->uri->segment(3));
        $this->data['termo']    = $parametro;
        $this->data['fornecedores'] = $this->fornecedor_model->getAll();

        $this->data['view']     = 'produtos/produtos';
       	$this->load->view('theme/topo',$this->data);
	}

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('produtos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $precoCompra = $this->input->post('precoCompra');
            $precoCompra = str_replace(",","", $precoCompra);

            $precoVenda = $this->input->post('precoVenda');
            $precoVenda = str_replace(",", "", $precoVenda);

            $comissao = $this->input->post('comissao');
            $comissao = str_replace(",", "", $comissao);
			
            $data = array(
                'descricao'         => $this->input->post('descricao'),
                'cProd'               => $this->input->post('cProd'),
                'cEAN'               => $this->input->post('cEAN'),
                'unidade'           => $this->input->post('unidade'),
                'observacao'        => $this->input->post('observacao'),
                //'grupoProduto_id'   => $this->input->post('grupoProduto_id'),
                //'setor_id'          => $this->input->post('setor_id'),
                'precoCompra'       => $precoCompra,
                'precoVenda'        => $precoVenda,
                'comissao'          => $comissao,
                'estoque'           => $this->input->post('estoque'),
                'estoqueMinimo'     => $this->input->post('estoqueMinimo')
            );

            $idProduto = $this->produtos_model->add('produtos', $data ,TRUE);
            if ($idProduto) {
                $this->session->set_flashdata('success','Produto adicionado com sucesso!');
                redirect(base_url() . 'index.php/produtos/editar/'.$idProduto);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }

        $this->data['setores']          = $this->setor_model->getSetorAll();
        $this->data['gruposProduto']    = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['view'] = 'produtos/adicionarProduto';
        $this->load->view('theme/topo', $this->data);
    }

    function importacaoProdutoCSV() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
            $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
            redirect(base_url());
        }
        $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        $this->data['view'] = 'produtos/importacaoProdutoCSV';
        $this->load->view('theme/topo', $this->data);
    }

    function atualizacaoEstoqueProdutoCSV() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
            $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
            redirect(base_url());
        }
        $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        $this->data['view'] = 'produtos/atualizarEstoqueProdutoCSV';
        $this->load->view('theme/topo', $this->data);
    }

    function atualizacaoPrecoVendaProdutoCSV() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
            $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
            redirect(base_url());
        }
        $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        $this->data['view'] = 'produtos/atualizarPrecoVendaProdutoCSV';
        $this->load->view('theme/topo', $this->data);
    }

    function atualizarEstoque() {

        ini_set('max_execution_time', 3000); //3000 seconds = 50 minutes
        ini_set('memory_limit', '-1');

        $csv    = $this->do_upload_csv($this->input->post('idProdutos'));
        $arrResult = array();
        $handle = fopen($csv, "r");

        if ($handle) {
            while (($row = fgetcsv($handle, 10000, ";")) !== FALSE) $arrResult[] = $row;
            fclose($handle);
        }

        $keys = array('Codigo', 'Estoque', 'Filial');
        $final = array();

        foreach ($arrResult as $key => $value)  $final[] = array_combine($keys, $value);

        $rw = 1;
        foreach ($final as $csv_pr) {

            if ($rw == 1) {
                $rw++;
                continue;
            }

            $codigo = trim($csv_pr['Codigo']);
            $estoque = trim($csv_pr['Estoque']);
            $filial = trim($csv_pr['Filial']);

            if ($filial == '') $filial = 1;

            $produtosId = 0;

            if ($codigo != '') {
                $produtoExiste = $this->produtos_model->getByCProd($codigo);
                if (count($produtoExiste) > 0) $produtosId = $produtoExiste->idProdutos;
            }

            $pr_codigo[] = $codigo;
            $pr_estoque[] = $estoque != ''  ? str_replace( ',', '.', $estoque) : 0;
            $pr_filial[] =  $filial;
            $pr_produtos_id[] = $produtosId;

            $rw++;
        }

        $items = array();
        $keysis = array('cProd', 'Estoque', 'filial_id', 'idProdutos');

        foreach (array_map(null, $pr_codigo,  $pr_estoque, $pr_filial, $pr_produtos_id) as $ikey => $value) {
            $items[] = array_combine($keysis, $value);
        }

        //$this->print_arrays($items);

        if ($this->produtos_model->atualizarEstoqueImportacaoCSV($items)) {
            redirect('produtos');
        } else {
            redirect('produtos/importacaoProdutoCSV');
        }
    }

    function atualizarPrecoVenda() {

        ini_set('max_execution_time', 3000); //3000 seconds = 50 minutes
        ini_set('memory_limit', '-1');

        $csv    = $this->do_upload_csv($this->input->post('idProdutos'));
        $arrResult = array();
        $handle = fopen($csv, "r");

        if ($handle) {
            while (($row = fgetcsv($handle, 10000, ";")) !== FALSE) $arrResult[] = $row;
            fclose($handle);
        }

        $keys = array('Codigo', 'Preco venda', 'Filial');
        $final = array();

        foreach ($arrResult as $key => $value)  $final[] = array_combine($keys, $value);

        $rw = 1;
        foreach ($final as $csv_pr) {

            if ($rw == 1) {
                $rw++;
                continue;
            }

            $codigo = trim($csv_pr['Codigo']);
            $precoVenda = trim($csv_pr['Preco venda']);
            $filial = trim($csv_pr['Filial']);

            if ($filial == '') $filial = 1;

            $produtosId = 0;

            if ($codigo != '') {
                $produtoExiste = $this->produtos_model->getByCProd($codigo);
                if (count($produtoExiste) > 0) $produtosId = $produtoExiste->idProdutos;
            }

            $pr_codigo[] = $codigo;
            $pr_preco_venda[] = $precoVenda != '' ? str_replace(  ',', '.', $precoVenda)  : 0;
            $pr_filial[] =  $filial;
            $pr_produtos_id[] = $produtosId;

            $rw++;
        }

        $items = array();
        $keysis = array('cProd', 'Venda', 'filial_id', 'idProdutos');

        foreach (array_map(null, $pr_codigo, $pr_preco_venda, $pr_filial, $pr_produtos_id) as $ikey => $value) {
            $items[] = array_combine($keysis, $value);
        }

        //$this->print_arrays($items);

        if ($this->produtos_model->atualizarPrecoVendaImportacaoCSV($items)) {
            redirect('produtos');
        } else {
            redirect('produtos/importacaoProdutoCSV');
        }
    }

    function importar() {

        ini_set('max_execution_time', 3000); //3000 seconds = 50 minutes
        ini_set('memory_limit', '-1');

        $csv    = $this->do_upload_csv($this->input->post('idProdutos'));
        $arrResult = array();
        $handle = fopen($csv, "r");

        if ($handle) {
            while (($row = fgetcsv($handle, 10000, ";")) !== FALSE) {
                $arrResult[] = $row;
            }
            fclose($handle);
        }

        $keys = array('Codigo', 'Cod. Barras', 'Descricao', 'Unidade', 'CFOP', 'CST', 'CEST', 'NCM', 'Estoque', 'Grupo', 'Preco custo', 'Preco venda', 'Filial');
        $final = array();

        foreach ($arrResult as $key => $value) {
            $final[] = array_combine($keys, $value);
        }

        $rw = 1;
        foreach ($final as $csv_pr) {

            if ($rw == 1) {
                $rw++;
                continue;
            }

            $codigo = trim($csv_pr['Codigo']);
            $codigoBarras = trim($csv_pr['Cod. Barras']);
            $filial = trim($csv_pr['Filial']);
            $estoque = trim($csv_pr['Estoque']);
            $grupo = utf8_encode(trim($csv_pr['Grupo']));
            $descricao = utf8_encode(trim($csv_pr['Descricao']));
            $unidade = utf8_encode(trim($csv_pr['Unidade']));
            $precoVenda = trim($csv_pr['Preco venda']);
            $precoCompra = trim($csv_pr['Preco custo']);
            $produtosId = 0;

            if ($filial == '') $filial = 1;
            if ($grupo == '') $grupo = 'OUTROS';

            $bjGrupo = $this->grupoproduto_model->getByName($grupo, $filial);
            $codigo =  $codigo != '' ? $codigo : $codigoBarras;

            if (count($bjGrupo) > 0) $grupo = $bjGrupo->idGrupoProduto;
            else $grupo = $this->grupoproduto_model->add('grupoproduto', array('nome' =>  $grupo ), true);

            if ($codigo != '') {
                $produtoExiste = $this->produtos_model->getByCProd($codigo);
                if (count($produtoExiste) > 0) $produtosId = $produtoExiste->idProdutos;
            }

            $pr_codigo[] = $codigo;
            $pr_cod_barras[] = $codigoBarras;
            $pr_descricao[] =  $descricao;
            $pr_unidade[] = $unidade;
            $pr_cfop[] = trim($csv_pr['CFOP']);
            $pr_cst[] = trim($csv_pr['CST']);
            $pr_cest[] = trim($csv_pr['CEST']);
            $pr_ncm[] = trim($csv_pr['NCM']);
            $pr_estoque[] = $estoque != ''  ? str_replace( ',', '.', $estoque) : 0;
            $pr_grupo[] = $grupo;
            $pr_preco_custo[] = $precoCompra != '' ? str_replace( ',', '.', $precoCompra) : 0;
            $pr_preco_venda[] = $precoVenda != '' ? str_replace(  ',', '.', $precoVenda)  : 0;
            $pr_filial[] =  $filial;
            $pr_produtos_id[] = $produtosId;

            $rw++;
        }

        $items = array();
        $keysis = array('cProd', 'cEAN', 'descricao', 'unidade', 'CFOP', 'CST', 'CEST', 'ncm', 'Estoque', 'grupoProduto_id', 'Custo', 'Venda', 'filial_id', 'idProdutos');

        foreach (array_map(null, $pr_codigo, $pr_cod_barras, $pr_descricao, $pr_unidade, $pr_cfop, $pr_cst, $pr_cest, $pr_ncm, $pr_estoque, $pr_grupo, $pr_preco_custo, $pr_preco_venda, $pr_filial, $pr_produtos_id) as $ikey => $value) {
            $items[] = array_combine($keysis, $value);
        }

        //$this->print_arrays($items);

        if ($this->produtos_model->addProdutoImportacaoCSV($items)) {
            redirect('produtos');
        } else {
            redirect('produtos/importacaoProdutoCSV');
        }
    }

    public function print_arrays()
    {
        $args = func_get_args();
        echo "<pre>";
        foreach ($args as $arg) {
            print_r($arg);
        }
        echo "</pre>";
        die();
    }

    function editar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar produtos.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $filiais  = $this->filial_model->getAll();

        if ($this->form_validation->run('produtos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $precoCompra = $this->input->post('precoCompra');
            $precoCompra = str_replace(",","", $precoCompra);
            $precoVenda = $this->input->post('precoVenda');
            $precoVenda = str_replace(",", "", $precoVenda);
            $comissao = $this->input->post('comissao');
            $comissao = str_replace(",", "", $comissao);
            $arquivo    = $this->do_upload($this->input->post('idProdutos'));

            if ($arquivo['file_name'] != ''){
                $nomeArquivo = $arquivo['file_name'];
            } else {
                $nomeArquivo = $this->input->post('imagem');
            }

            $data = array(
                'descricao'         => $this->input->post('descricao'),
                'unidade'           => $this->input->post('unidade'),
                'ean'               => $this->input->post('ean'),
                'ncm'               => $this->input->post('ncm'),
                'CFOP'              => $this->input->post('CFOP'),
                'cProd'             => $this->input->post('cProd'),
                'cEAN'              => $this->input->post('cEAN'),
                'CEST'              => $this->input->post('CEST'),
                'origem'            => $this->input->post('origem'),
                'observacao'        => $this->input->post('observacao'),
                'grupoProduto_id'   => $this->input->post('grupoProduto_id'),
                'precoCompra'       => $precoCompra,
                'precoVenda'        => $precoVenda,
                'comissao'          => $comissao,
                'estoque'           => $this->input->post('estoque'),
                'CST'               => $this->input->post('CST'),
                'estoqueMinimo'     => $this->input->post('estoqueMinimo'),
                'imagem'            => $nomeArquivo,
                'percentualIBPT'    => $this->input->post('percentualIBPT'),
                'infoNota'          => $this->input->post('infoNota'),
                'pIPI'              => $this->input->post('pIPI'),
                'CSTIPI'            => $this->input->post('CSTIPI'),
                'pPIS'              => $this->input->post('pPIS'),
                'CSTPIS'            => $this->input->post('CSTPIS'),
                'pCOFINS'           => $this->input->post('pCOFINS'),
                'CSTCOFINS'         => $this->input->post('CSTCOFINS'),
                'pICMSST'           => $this->input->post('pICMSST'),
                'modBCST'           => $this->input->post('modBCST'),
                'cProdANP'          => $this->input->post('cProdANP'),
                'descANP'           => $this->input->post('descANP'),
                'UFCons'            => $this->input->post('UFCons'),
                'permiteEstoqueNegativo'            => $this->input->post('permiteEstoqueNegativo'),
                'usarInformacoesFiscaisProduto'     => $this->input->post('usarInformacoesFiscaisProduto'),
            );


            foreach ($filiais as $filial) {

                $usarFilial     = $this->input->post('usarFilial'.$filial->idFilial);
                $estoque_original = $this->input->post('estoque_original' . $filial->idFilial);
                $estoque = $this->input->post('estoque' . $filial->idFilial);
                $produto_id = $this->input->post('idProdutos');

                if ($usarFilial == 1) {

                    $isExisteProdutoFilial = $this->produtos_model->getProdutoFilial($this->input->post('idProdutos'), $filial->idFilial);

                    $reservado = $this->input->post('reservado' . $filial->idFilial);
                    $bloqueado = $this->input->post('bloqueado' . $filial->idFilial);
                    $precoCompra = $this->input->post('precoCompra' . $filial->idFilial);
                    $precoVenda = $this->input->post('precoVenda' . $filial->idFilial);
                    $localizacao = $this->input->post('localizacao' . $filial->idFilial);
                    $estoqueMinimo = $this->input->post('estoqueMinimo' . $filial->idFilial);

                    $produto_filial = array(
                        'filial_id' => $filial->idFilial,
                        'produto_id' => $produto_id,
                        'localizacao' => $localizacao,
                        'reservado' => $reservado,
                        'bloqueado' => $bloqueado,
                        'estoqueMinimo' => $estoqueMinimo,
                        'precoVenda' => $precoVenda,
                        'precoCompra' => $precoCompra
                    );

                    if (count($isExisteProdutoFilial) > 0) {
                        $this->produtos_model->edit('produto_filial', $produto_filial, 'idProdutoFilial', $isExisteProdutoFilial->idProdutoFilial);
                    } else {
                        $this->produtos_model->addProdutoFilial('produto_filial', $produto_filial, TRUE);
                    }

                    if ($estoque > $estoque_original) {
                        $this->estoque_model->entradaEstoque(($estoque - $estoque_original), $produto_id, 'Ajuste Manual', null, null, $filial->idFilial);
                    } else if($estoque < $estoque_original){
                        $this->estoque_model->saidaEstoque(($estoque_original - $estoque), $produto_id, 'Ajuste Manual', null, null, $filial->idFilial);
                    }

                } else {
                    $this->estoque_model->saidaEstoque($estoque, $produto_id, 'Ajuste Manual', null, null, $filial->idFilial);
                    $this->produtos_model->deletarProdutoFilial($this->input->post('idProdutos'), $filial->idFilial);
                }
            }

            if ($this->produtos_model->edit('produtos', $data, 'idProdutos', $this->input->post('idProdutos')) == TRUE) {
                $this->session->set_flashdata('success','Produto editado com sucesso!');
                //redirect(base_url() . 'index.php/produtos/editar/'.$this->input->post('idProdutos'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['icmsAll']              = $this->icms_model->getAll();
        $this->data['setores']              = $this->setor_model->getSetorAll();
        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['result']               = $this->produtos_model->getById($this->uri->segment(3));
        $this->data['fornecedoresveiculos'] = $this->produtos_model->getFornecedoresProduto($this->uri->segment(3));
        $this->data['filiais']              = $filiais;

        $this->data['view'] = 'produtos/editarProduto';
        $this->load->view('theme/topo', $this->data);
    }

    public function getUnicProdutoFilial()
    {
        $idProdutoFilial= $this->input->post('idProdutoFilial');
        echo json_encode($this->produtos_model->getUnicProdutoFilial($idProdutoFilial));
    }

    public function getProdutoFiliais()
    {
        $produto = $this->input->post('produto');
        echo json_encode($this->produtos_model->getProdutoFiliais($produto));
    }

    function visualizar() {
      
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }

        $this->data['result'] = $this->produtos_model->getById($this->uri->segment(3));
        $this->data['estoque'] = $this->produtos_model->getProdutoFilialById($this->uri->segment(3));

        if($this->data['result'] == null){
            $this->session->set_flashdata('error','Produto não encontrado.');
            redirect(base_url() . 'index.php/produtos/editar/'.$this->input->post('idProdutos'));
        }

        $this->data['emitente']         = $this->mapos_model->getEmitente();
        $this->data['fornecedoresproduto'] = $this->produtos_model->getFornecedoresProduto($this->uri->segment(3));
        $this->data['view'] = 'produtos/visualizarProduto';
        $this->load->view('theme/topo', $this->data);
    }
	
    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir produtos.');
           redirect(base_url());
        }

        
        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir produto.');            
            redirect(base_url().'index.php/produtos/gerenciar/');
        }

        $this->db->where('produtos_id', $id);
        $this->db->delete('produtos_os');


        $this->db->where('produtos_id', $id);
        $this->db->delete('itens_de_vendas');
        
        $this->produtos_model->delete('produtos','idProdutos',$id);             

        $this->session->set_flashdata('success','Produto excluido com sucesso!');            
        redirect(base_url().'index.php/produtos/gerenciar/');
    }

    public function suggestions() {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->send_json($this->produtos_model->suggestions($q));
        }
    }

    public function autoCompleteFornecedorFilterSelect() {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $rows['results'] = $this->produtos_model->autoCompleteFornecedorFilterSelect($q);
            $this->send_json($rows);
        }
    }

    public function autoCompleteProdutoByIdNotFilterFilial($idProduto)
    {
        if ($idProduto) {
            $rows['results'] = $this->os_model->autoCompleteProdutoById($idProduto);
            $this->send_json($rows);
        }
    }

    public function autoCompleteFornecedorById($idFornecedor) {
        if ($idFornecedor) {
            $rows['results'] = $this->produtos_model->autoCompleteFornecedorById($idFornecedor);
            $this->send_json($rows);
        }
    }

    public function send_json($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    public function salvarFornecedorProduto()
    {
        $idFornecedorproduto    =  $this->input->post('idFornecedorproduto');
        $idFornecedor           = $this->input->post('idFornecedor');
        $idProduto              = $this->input->post('idProduto');
        $codigo                 = $this->input->post('codigo');
        $observacao             = $this->input->post('observacao');

        $data = array(
            'idFornecedor'  => $idFornecedor,
            'idProduto'     => $idProduto,
            'codigo'        => $codigo,
            'observacao'    => $observacao,
        );

        if ($idFornecedorproduto) {
            if ($this->produtos_model->edit('fornecedorproduto', $data, 'idFornecedorproduto', $idFornecedorproduto) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            if ($this->produtos_model->add('fornecedorproduto', $data) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        }
    }

    function excluirFornecedorProduto()
    {
        $ID = $this->input->post('idFornecedorproduto');
        if ($this->produtos_model->delete('fornecedorproduto', 'idFornecedorproduto', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    function consultaProduto() {
        $produto_id = $this->input->post('produtos_id');
        if ($produto_id) {
            echo json_encode($this->produtos_model->getProdutoFilialById($produto_id));
        }
    }

    function searchPesquisaProdutos($nNF) {
        $this->data['nNF']      = $nNF;
        $this->data['view']     = 'produtos/searchPesquisaProdutos';
        $this->load->view('tema/blank', $this->data);
    }

    function search() {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar produtos.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $termo = $this->input->post('termo');;

        $config['base_url'] = base_url() . 'index.php/produtos/search/';
        $config['total_rows'] = $this->produtos_model->count('produtos');

        if (!$termo) {
            $config['per_page'] = 20;
        } else {
            $config['per_page'] = 100;
        }

        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->produtos_model->search($termo, $config['per_page'], $this->uri->segment(3));
        $this->data['termo']    = $termo;
        $this->data['view']     = 'produtos/search';
        $this->load->view('tema/blank', $this->data);
    }

    public function do_upload_csv($produto_id){

        $config['upload_path']      = './assets/arquivos/produto/'.$produto_id;
        $config['allowed_types']    = '*';
        $config['max_width']        = '3000';
        $config['max_height']       = '2000';
        $config['encrypt_name']     = true;
        $config['max_size']         = 16384;

        if (!is_dir('./assets/arquivos/produto/'.$produto_id)) {
            mkdir('./assets/arquivos/produto/'. $produto_id, 0777, TRUE);
        }

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('arquivoproduto'))  {
            echo $this->upload->display_errors();
            return FALSE;
        } else  {
            return './assets/arquivos/produto/'.$produto_id.'/'.$this->upload->file_name;
        }
    }

    public function do_upload($produto_id){

        $config['upload_path']      = './assets/arquivos/produto/'.$produto_id;
        $config['allowed_types']    = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
        $config['max_size']         = 0;
        $config['max_width']        = '3000';
        $config['max_height']       = '2000';
        $config['encrypt_name']     = true;

        if (!is_dir('./assets/arquivos/produto/'.$produto_id)) {
            mkdir('./assets/arquivos/produto/'. $produto_id, 0777, TRUE);
        }

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('arquivoproduto'))  {
            return FALSE;
        }
        else  {
            return $this->upload->data();
        }
    }

}

