<?php

class programacaomanutencaopreventiva extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('programacaomanutencaopreventiva_model', '', TRUE);
        $this->load->model('fornecedor_model', '', TRUE);

        $this->data['menuProgramacaoManutencao']    = 'Programação Manutenção Preventiva';
        $this->data['menuManutencao']               = 'Manutenção';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/programacaomanutencaopreventiva/gerenciar/';
        $config['total_rows']   = $this->programacaomanutencaopreventiva_model->count('programacao_manutencao_preventiva');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->programacaomanutencaopreventiva_model->getProgramacao('',$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventiva/programacaomanutencaopreventiva';
        $this->load->view('tema/topo',$this->data);
    }


    function iframeManutencaoPreventivaAgendada() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/programacaomanutencaopreventiva/gerenciar/';
        $config['total_rows']   = $this->programacaomanutencaopreventiva_model->count('programacao_manutencao_preventiva');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->programacaomanutencaopreventiva_model->getProgramacaoByProdutoAgendada($this->uri->segment(3),$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventiva/programacaomanutencaopreventiva';
        $this->load->view('tema/blank', $this->data);
    }


    function iframeManutencaoPreventivaHistorico() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/programacaomanutencaopreventiva/gerenciar/';
        $config['total_rows']   = $this->programacaomanutencaopreventiva_model->count('programacao_manutencao_preventiva');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->programacaomanutencaopreventiva_model->getProgramacaoByProdutoHistorico($this->uri->segment(3),$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventiva/programacaomanutencaopreventiva';
        $this->load->view('tema/blank', $this->data);
    }

    function buscar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/programacaomanutencaopreventiva_model/gerenciar/';
        $config['total_rows'] = $this->programacaomanutencaopreventiva_model->count('programacao_manutencao_preventiva');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $where = '';

        $this->data['results']  = $this->programacaomanutencaopreventiva_model->getProgramacao($where,$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventiva/programacaomanutencaopreventiva';
        $this->load->view('tema/topo',$this->data);
    }

    function editar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('programacao_manutencao_preventiva') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'fornecedor_id'     => $this->input->post('fornecedor_id'),
                'dataAgendamento'   => $this->input->post('dataAgendamento'),
                'prazoEntrega'      => $this->input->post('prazoEntrega'),
                'numeroNotaFiscal'  => $this->input->post('numeroNotaFiscal'),
                'valorManutencao'   => $this->input->post('valorManutencao'),
                'observacao'        => $this->input->post('observacao'),
            );
            if ($this->programacaomanutencaopreventiva_model->edit('programacao_manutencao_preventiva', $data, 'idProgramacaomanutencao', $this->input->post('idProgramacaomanutencao')) == TRUE) {
                $this->session->set_flashdata('success','Manutenção editado com sucesso!');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['fornecedores']         = $this->fornecedor_model->getAll();
        $this->data['servicos']             = $this->servicos_model->getAll();
        $this->data['servicosManutencao']   = $this->programacaomanutencaopreventiva_model->getServicosManutencaoPreventiva($this->uri->segment(3));
        $this->data['result']               = $this->programacaomanutencaopreventiva_model->getById($this->uri->segment(3));
        $this->data['view']                 = 'programacaomanutencaopreventiva/editarProgramacaomanutencaopreventiva';
        $this->load->view('tema/topo', $this->data);
    }

    public function confirmarManutencao() {
        $programacao = $this->programacaomanutencaopreventiva_model->getById($this->uri->segment(3));
        $data = array(
            'situacao'  => 2
        );
        $this->programacaomanutencaopreventiva_model->edit('programacao_manutencao_preventiva', $data, 'idProgramacaomanutencao',$programacao->idProgramacaomanutencao);

        $manutencao_preventiva = array(
            'situacao' => 1,
            'dataInicialPreventiva' => date('Y-m-d'),
        );
        $this->programacaomanutencaopreventiva_model->edit('manutencao_preventiva', $manutencao_preventiva, 'produto_id',$programacao->produto_id);
        redirect('programacaomanutencaopreventiva');
    }

    public function converterOS() {

        $idVistoria                 = $this->input->post('idVistoria');
        $vistoria                   = $this->vistoria_model->getById($idVistoria);
        $descricaoprodutoservico    = $this->input->post('descricaoprodutoservico');

        $data_os = array(
            'dataInicial'           => date('Y-m-d'),
            'clientes_id'           => $vistoria->clientes_id,
            'usuarios_id'           => $vistoria->usuarios_id,
            'kilometragementrada'   => $vistoria->kmentrada,
            'kilometragemsaida'     => $vistoria->kmsaida,
            'veiculo'               => $vistoria->veiculo_id,
            'descricaoProduto'      => $descricaoprodutoservico,
            'status'                => 'Orçamento',
            'faturado'              => 0
        );

        $idOs = $this->vistoria_model->add('os', $data_os, true);

        $data_vistoria = array(
            'os_id'           => $idOs,
        );
        $this->vistoria_model->edit('vistoria', $data_vistoria, 'idVistoria', $idVistoria);

        redirect('os/editar/' . $idOs);
    }

    function verificarManutencaoPeriodo(){
        $this->programacaomanutencaopreventiva_model->verificarManutencaoPeriodo();
    }

}

