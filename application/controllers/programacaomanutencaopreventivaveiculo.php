<?php

class programacaomanutencaopreventivaveiculo extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('programacaomanutencaopreventivaveiculo_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('veiculos_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('servicos_model', '', TRUE);

        $this->data['menuProgramacaoManutencaoVeiculo']    = 'Programação Manutenção Preventiva';
        $this->data['menuManutencao']               = 'Manutenção';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/programacaomanutencaopreventivaveiculo/gerenciar/';
        $config['total_rows']   = $this->programacaomanutencaopreventivaveiculo_model->count('programacao_manutencao_preventiva_veiculo');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->programacaomanutencaopreventivaveiculo_model->getProgramacao('',$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventivaveiculo/programacaomanutencaopreventivaveiculo';
        $this->load->view('tema/topo',$this->data);
    }


    function iframeManutencaoPreventivaAgendada() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/programacaomanutencaopreventivaveiculo/gerenciar/';
        $config['total_rows']   = $this->programacaomanutencaopreventivaveiculo_model->count('programacao_manutencao_preventiva_veiculo');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->programacaomanutencaopreventivaveiculo_model->getProgramacaoByProdutoAgendada($this->uri->segment(3),$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventivaveiculo/programacaomanutencaopreventivaveiculo';
        $this->load->view('tema/blank', $this->data);
    }


    function iframeManutencaoPreventivaHistorico() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url']     = base_url().'index.php/programacaomanutencaopreventivaveiculo/gerenciar/';
        $config['total_rows']   = $this->programacaomanutencaopreventivaveiculo_model->count('programacao_manutencao_preventiva_veiculo');
        $config['per_page']     = 20;
        $config['next_link']    = 'Próxima';
        $config['prev_link']    = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results']  = $this->programacaomanutencaopreventivaveiculo_model->getProgramacaoByProdutoHistorico($this->uri->segment(3),$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventivaveiculo/programacaomanutencaopreventivaveiculo';
        $this->load->view('tema/blank', $this->data);
    }

    function buscar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vPatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para visualizar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/programacaomanutencaopreventivaveiculo_model/gerenciar/';
        $config['total_rows'] = $this->programacaomanutencaopreventivaveiculo_model->count('programacao_manutencao_preventiva_veiculo');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $where = '';

        $this->data['results']  = $this->programacaomanutencaopreventivaveiculo_model->getProgramacao($where,$config['per_page'],$this->uri->segment(3));
        $this->data['view']     = 'programacaomanutencaopreventivaveiculo/programacaomanutencaopreventivaveiculo';
        $this->load->view('tema/topo',$this->data);
    }

    function editar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('programacao_manutencao_preventiva_veiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'dataAgendamento'   => $this->input->post('dataAgendamento'),
                'prazoEntrega'      => $this->input->post('prazoEntrega'),
                'numeroNotaFiscal'  => $this->input->post('numeroNotaFiscal'),
                'valorManutencao'   => $this->input->post('valorManutencao'),
                'observacao'        => $this->input->post('observacao'),
            );
            if ($this->programacaomanutencaopreventivaveiculo_model->edit('programacao_manutencao_preventiva_veiculo', $data, 'idProgramacaomanutencaoveiculo', $this->input->post('idProgramacaomanutencaoveiculo')) == TRUE) {
                $this->session->set_flashdata('success','Manutenção editado com sucesso!');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['clientes']             = $this->clientes_model->getAll();
        $this->data['servicos']             = $this->servicos_model->getAll();
        $this->data['servicosManutencao']   = $this->programacaomanutencaopreventivaveiculo_model->getServicosManutencaoPreventiva($this->uri->segment(3));
        $this->data['result']               = $this->programacaomanutencaopreventivaveiculo_model->getById($this->uri->segment(3));
        $this->data['view']                 = 'programacaomanutencaopreventivaveiculo/editarProgramacaomanutencaopreventivaveiculo';
        $this->load->view('tema/topo', $this->data);
    }

    public function confirmarManutencao() {
        $this->execConfirmarManutencao();
        redirect('programacaomanutencaopreventivaveiculo');
    }

    function execConfirmarManutencao() {
        $programacao = $this->programacaomanutencaopreventivaveiculo_model->getById($this->uri->segment(3));
        $data = array(
            'situacao'  => 2
        );
        $this->programacaomanutencaopreventivaveiculo_model->edit('programacao_manutencao_preventiva_veiculo', $data, 'idProgramacaomanutencaoveiculo',$programacao->idProgramacaomanutencaoveiculo);

        $manutencao_preventiva = array(
            'situacao' => 1,
            'dataInicialPreventiva' => date('Y-m-d'),
        );
        $this->programacaomanutencaopreventivaveiculo_model->edit('manutencao_preventiva_veiculo', $manutencao_preventiva, 'veiculo_id',$programacao->veiculo_id);
    }

    function verificarManutencaoPeriodo(){
        $this->programacaomanutencaopreventivaveiculo_model->verificarManutencaoPeriodo();
    }

    public function converteros() {

        $programacao                    = $this->programacaomanutencaopreventivaveiculo_model->getById($this->uri->segment(3));
        $veiculo                        = $this->veiculos_model->getById($programacao->veiculo_id);

        $data_os = array(
            'dataInicial'           => date('Y-m-d'),
            'clientes_id'           => $veiculo->cliente_id,
            'usuarios_id'           => 1,
            'veiculo_id'            => $veiculo->idVeiculo,
            'descricaoProduto'      => $programacao->observacao,
            'status'                => 'Orçamento',
            'faturado'              => 0,
            'manutencao_id'         => $this->uri->segment(3)
        );
        $idOs = $this->programacaomanutencaopreventivaveiculo_model->add('os', $data_os, true);

        $servicos = $this->programacaomanutencaopreventivaveiculo_model->getServicosManutencaoPreventiva($this->uri->segment(3));

        foreach ($servicos as $servico) {

            if ($servico->peca_id) {

                $produto = $this->produtos_model->getProdutoFilialById($servico->peca_id);

                $data_produto = array(
                    'quantidade'    => 1,
                    'os_id'         => $idOs,
                    'produtos_id'   => $produto->idProdutos,
                    'precoCusto'    => $produto->precoCompra,
                    'precoVendaOS'  => $produto->precoVenda,
                    'subTotal'      => $produto->precoVenda,
                    'setor_id'      => 1,
                );
                $this->programacaomanutencaopreventivaveiculo_model->add('produtos_os', $data_produto, true);
            }

            if ($servico->servico_id) {

                $serv = $this->servicos_model->getById($servico->servico_id);

                $data_produto = array(
                    'os_id'             => $idOs,
                    'servicos_id'       => $serv->idServicos,
                    'precoVendaServico' => $serv->preco,
                    'subTotal'          => $serv->preco,
                    'setor_id'          => 1,
                );
                $this->programacaomanutencaopreventivaveiculo_model->add('servicos_os', $data_produto, true);

            }
        }

        $this->execConfirmarManutencao();

        redirect('os/editar/' . $idOs);
    }

}

