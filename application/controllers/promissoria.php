<?php

class Promissoria extends CI_Controller {

    var $promissoriaService;

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));

        $this->load->model('promissoriaservice_model', '', TRUE);
        $this->load->model('mapos_model','',TRUE);
        $this->load->model('site_model', '', TRUE);
        $this->load->model('financeiro_model', '', TRUE);

        $this->promissoriaService = $this->promissoriaservice_model;
    }

    public function gerar() {
        try {
            $this->data['promissoria'] = $this->promissoriaService->gerar($this->uri->segment(3));
            $this->data['emitente'] = $this->mapos_model->getEmitente();

            $this->data['view'] = 'promissoria/promissoria';
            $this->load->view('tema/blank', $this->data);

        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function gerarPromissoriaContaReceber() {

        $parcelas = $this->financeiro_model->getParcelasByLancamentoId($this->uri->segment(3));
        $promissorias = [];
        $contador = 0;

        foreach ($parcelas as $parcela) {
            $promissorias[$contador] = $this->promissoriaService->gerar($parcela->idParcela);
            $contador++;
        }

        $this->data['promissorias'] = $promissorias;
        $this->data['emitente'] = $this->mapos_model->getEmitente();

        $this->data['view'] = 'promissoria/promissorias';
        $this->load->view('tema/blank', $this->data);
    }

}

