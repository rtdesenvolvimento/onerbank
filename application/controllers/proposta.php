<?php

class proposta extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'codegen_helper'));
    }

    function index(){}

    function gestorLDN21($idProposta, $base, $pdf) {

        $base = '30831687000178';

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('pp.*, p.* , c.*, p.descricao as nome');
        $otherdb->from('proposta pp');
        $otherdb->join('pessoa p', 'pp.cliente = p.id', 'left');
        $otherdb->join('cliente c', 'c.pessoa = p.id', 'left');
        $otherdb->where('pp.id', $idProposta);

        $data['proposta'] =  $otherdb->get()->row();

        $this->load->helper('mpdf');

        if ($pdf == 1) {
            $html = $this->load->view('relatorios/proposta/gestorLDN21', $data, true);
            echo pdf_create($html, 'gestor_ldn21' . date('d-m-y') . '_' . $idProposta, true);
        } else {
            $html = $this->load->view('relatorios/proposta/gestorLDN21', $data, false);
        }
    }

    function individual($idProposta, $base, $pdf) {

        $base = '30831687000178';

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('pp.* , pp.valorplano as valorplanoservico, p.* , c.*, p.descricao as nome, r.descricao as nomeresponsavel ');
        $otherdb->from('proposta pp');
        $otherdb->join('pessoa p', 'pp.cliente = p.id', 'left');
        $otherdb->join('pessoa r', 'pp.responsavel = r.id', 'left');
        $otherdb->join('cliente c', 'c.pessoa = p.id', 'left');
        $otherdb->where('pp.id', $idProposta);

        $data['proposta'] =  $otherdb->get()->row();


        //portabilidade
        $otherdb->select('pp.*, o.descricao as operadora');
        $otherdb->from('propostaportabilidade pp');
        $otherdb->join('operadora o', 'pp.operadora = o.id', 'left');
        $otherdb->where('pp.proposta', $idProposta);
        $data['propostaportabilidades'] =  $otherdb->get()->result();

        //franquia indivudual
        $otherdb->select('pf.*, r.descricao as regional, tsp.descricao as tiposolicitacaoproposta, ft.franquia as franquia, pss.descricao as passaporte, pssa.descricao as passaporteavulso, anatel.descricao as anatel ');
        $otherdb->join('regiao r', 'pf.regiao = r.id', 'left');
        $otherdb->join('tiposolicitacaoproposta tsp', 'pf.tiposolicitacaoproposta = tsp.id', 'left');
        $otherdb->join('franquiatelefonica ft', 'pf.franquia = ft.id', 'left');
        $otherdb->join('passaporte pss', 'pf.passaporte = pss.id', 'left');
        $otherdb->join('passaporte pssa', 'pf.passaporteavulso = pssa.id', 'left');
        $otherdb->join('codigoanatel anatel', 'pf.codanatel = anatel.id', 'left');
        $otherdb->join('planotelefonico pt', 'pf.plano = pt.id', 'left');
        $otherdb->from('propostafranquia pf');
        $otherdb->where('pt.tipo', 'Plano Claro Total Individual - Claro Life (Anatel N 190)');
        $otherdb->where('pf.proposta', $idProposta);
        $data['propostafranquiasIndividual'] =  $otherdb->get()->result();

        //franquia compartilhada
        $otherdb->select('pf.*, r.descricao as regional, tsp.descricao as tiposolicitacaoproposta, ft.franquia as franquia, pss.descricao as passaporte, pssa.descricao as passaporteavulso, anatel.descricao as anatel ');
        $otherdb->join('regiao r', 'pf.regiao = r.id', 'left');
        $otherdb->join('tiposolicitacaoproposta tsp', 'pf.tiposolicitacaoproposta = tsp.id', 'left');
        $otherdb->join('franquiatelefonica ft', 'pf.franquia = ft.id', 'left');
        $otherdb->join('passaporte pss', 'pf.passaporte = pss.id', 'left');
        $otherdb->join('passaporte pssa', 'pf.passaporteavulso = pssa.id', 'left');
        $otherdb->join('codigoanatel anatel', 'pf.codanatel = anatel.id', 'left');
        $otherdb->join('planotelefonico pt', 'pf.plano = pt.id', 'left');
        $otherdb->from('propostafranquia pf');
        $otherdb->where('pt.tipo', 'Plano Internet Movel Individual (Anatel N 29 / 41 / 45 / 47 )');
        $otherdb->where('pf.proposta', $idProposta);
        $data['propostafranquiasCompartilhado'] =  $otherdb->get()->result();

        //equipamento individual
        $otherdb->select('pa.*, pd.descricao as produto');
        $otherdb->from('propostaaparelho pa');
        $otherdb->join('planotelefonico pt', 'pa.plano = pt.id', 'left');
        $otherdb->join('aparelhotelefonico at', 'pa.aparelho = at.id', 'left');
        $otherdb->join('produtofilial pf', 'pf.id = at.produtofilial', 'left');
        $otherdb->join('produto pd', 'pd.id = pf.produto', 'left');
        $otherdb->where('pa.proposta', $idProposta);
        $otherdb->where('pf.filial', '12');
        $otherdb->where('pt.tipo', 'Plano Claro Total Individual - Claro Life (Anatel N 190)');
        $data['propostaaparelhosIndividual'] =  $otherdb->get()->result();

        //equipamento individual
        $otherdb->select('pa.*, pd.descricao as produto');
        $otherdb->from('propostaaparelho pa');
        $otherdb->join('planotelefonico pt', 'pa.plano = pt.id', 'left');
        $otherdb->join('aparelhotelefonico at', 'pa.aparelho = at.id', 'left');
        $otherdb->join('produtofilial pf', 'pf.id = at.produtofilial', 'left');
        $otherdb->join('produto pd', 'pd.id = pf.produto', 'left');
        $otherdb->where('pa.proposta', $idProposta);
        $otherdb->where('pf.filial', '12');
        $otherdb->where('pt.tipo', 'Plano Internet Movel Individual (Anatel N 29 / 41 / 45 / 47 )');
        $data['propostaaparelhosCompartilhado'] =  $otherdb->get()->result();

        $this->load->helper('mpdf');

        if ($pdf == 1) {
            $html = $this->load->view('relatorios/proposta/individual', $data, true);
            echo pdf_create($html, 'proposta' . date('d-m-y') . '_' . $idProposta, true, 'A4-L');
        } else {
            $html = $this->load->view('relatorios/proposta/individual', $data, false);
        }
    }

    function gestorOnline($idProposta, $base, $pdf) {

        $base = '30831687000178';

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('pp.*, p.* , c.*, p.descricao as nome');
        $otherdb->from('proposta pp');
        $otherdb->join('pessoa p', 'pp.cliente = p.id', 'left');
        $otherdb->join('cliente c', 'c.pessoa = p.id', 'left');
        $otherdb->where('pp.id', $idProposta);

        $data['proposta'] =  $otherdb->get()->row();

        $this->load->helper('mpdf');

        if ($pdf == 1) {
            $html = $this->load->view('relatorios/proposta/gestorOnline', $data, true);
            echo pdf_create($html, 'gestor_online_3_0' . date('d-m-y') . '_' . $idProposta, true);
        } else {
            $html = $this->load->view('relatorios/proposta/gestorOnline', $data, false);
        }
    }

    function propostaOnline($idProposta, $pdf) {

        $base = '30831687000178';

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('pp.*, p.* , c.*, p.descricao as nome, r.descricao as nomeresponsavel ');
        $otherdb->from('proposta pp');
        $otherdb->join('pessoa p', 'pp.cliente = p.id', 'left');
        $otherdb->join('pessoa r', 'pp.responsavel = r.id', 'left');
        $otherdb->join('cliente c', 'c.pessoa = p.id', 'left');
        $otherdb->where('pp.id', $idProposta);

        $data['proposta'] =  $otherdb->get()->row();

        $this->load->helper('mpdf');

        if ($pdf == 1) {
            $html = $this->load->view('relatorios/proposta/proposta', $data, true);
            echo pdf_create($html, 'proposta_3_0' . date('d-m-y') . '_' . $idProposta, true);
        } else {
            $html = $this->load->view('relatorios/proposta/proposta', $data, false);
        }
    }

    function compartilhado($idProposta, $base, $pdf) {

        $base = '30831687000178';

        $otherdb = $this->load->database($base, TRUE);

        $otherdb->select('pp.* , pp.valorplano as valorplanoservico, p.* , c.*, p.descricao as nome, r.descricao as nomeresponsavel ');
        $otherdb->from('proposta pp');
        $otherdb->join('pessoa p', 'pp.cliente = p.id', 'left');
        $otherdb->join('pessoa r', 'pp.responsavel = r.id', 'left');
        $otherdb->join('cliente c', 'c.pessoa = p.id', 'left');
        $otherdb->where('pp.id', $idProposta);

        $data['proposta'] =  $otherdb->get()->row();


        //portabilidade
        $otherdb->select('pp.*, o.descricao as operadora');
        $otherdb->from('propostaportabilidade pp');
        $otherdb->join('operadora o', 'pp.operadora = o.id', 'left');
        $otherdb->where('pp.proposta', $idProposta);
        $data['propostaportabilidades'] =  $otherdb->get()->result();

        //franquia indivudual
        $otherdb->select('pf.*, r.descricao as regional, tsp.descricao as tiposolicitacaoproposta, ft.franquia as franquia, pss.descricao as passaporte, pssa.descricao as passaporteavulso, anatel.descricao as anatel ');
        $otherdb->join('regiao r', 'pf.regiao = r.id', 'left');
        $otherdb->join('tiposolicitacaoproposta tsp', 'pf.tiposolicitacaoproposta = tsp.id', 'left');
        $otherdb->join('franquiatelefonica ft', 'pf.franquia = ft.id', 'left');
        $otherdb->join('passaporte pss', 'pf.passaporte = pss.id', 'left');
        $otherdb->join('passaporte pssa', 'pf.passaporteavulso = pssa.id', 'left');
        $otherdb->join('codigoanatel anatel', 'pf.codanatel = anatel.id', 'left');
        $otherdb->join('planotelefonico pt', 'pf.plano = pt.id', 'left');
        $otherdb->from('propostafranquia pf');
        $otherdb->where('pt.tipo', 'Plano Claro Total Individual - Claro Life (Anatel N 190)');
        $otherdb->where('pf.proposta', $idProposta);
        $data['propostafranquiasIndividual'] =  $otherdb->get()->result();

        //franquia compartilhada
        $otherdb->select('pf.*, r.descricao as regional, tsp.descricao as tiposolicitacaoproposta, ft.franquia as franquia, pss.descricao as passaporte, pssa.descricao as passaporteavulso, anatel.descricao as anatel ');
        $otherdb->join('regiao r', 'pf.regiao = r.id', 'left');
        $otherdb->join('tiposolicitacaoproposta tsp', 'pf.tiposolicitacaoproposta = tsp.id', 'left');
        $otherdb->join('franquiatelefonica ft', 'pf.franquia = ft.id', 'left');
        $otherdb->join('passaporte pss', 'pf.passaporte = pss.id', 'left');
        $otherdb->join('passaporte pssa', 'pf.passaporteavulso = pssa.id', 'left');
        $otherdb->join('codigoanatel anatel', 'pf.codanatel = anatel.id', 'left');
        $otherdb->join('planotelefonico pt', 'pf.plano = pt.id', 'left');
        $otherdb->from('propostafranquia pf');
        $otherdb->where('pt.tipo', 'Plano Internet Movel Individual (Anatel N 29 / 41 / 45 / 47 )');
        $otherdb->where('pf.proposta', $idProposta);
        $data['propostafranquiasCompartilhado'] =  $otherdb->get()->result();

        //equipamento individual
        $otherdb->select('pa.*, pd.descricao as produto');
        $otherdb->from('propostaaparelho pa');
        $otherdb->join('planotelefonico pt', 'pa.plano = pt.id', 'left');
        $otherdb->join('aparelhotelefonico at', 'pa.aparelho = at.id', 'left');
        $otherdb->join('produtofilial pf', 'pf.id = at.produtofilial', 'left');
        $otherdb->join('produto pd', 'pd.id = pf.produto', 'left');
        $otherdb->where('pa.proposta', $idProposta);
        $otherdb->where('pf.filial', '12');
        $otherdb->where('pt.tipo', 'Plano Claro Total Individual - Claro Life (Anatel N 190)');
        $data['propostaaparelhosIndividual'] =  $otherdb->get()->result();

        //equipamento individual
        $otherdb->select('pa.*, pd.descricao as produto');
        $otherdb->from('propostaaparelho pa');
        $otherdb->join('planotelefonico pt', 'pa.plano = pt.id', 'left');
        $otherdb->join('aparelhotelefonico at', 'pa.aparelho = at.id', 'left');
        $otherdb->join('produtofilial pf', 'pf.id = at.produtofilial', 'left');
        $otherdb->join('produto pd', 'pd.id = pf.produto', 'left');
        $otherdb->where('pa.proposta', $idProposta);
        $otherdb->where('pf.filial', '12');
        $otherdb->where('pt.tipo', 'Plano Internet Movel Individual (Anatel N 29 / 41 / 45 / 47 )');
        $data['propostaaparelhosCompartilhado'] =  $otherdb->get()->result();

        $this->load->helper('mpdf');

        if ($pdf == 1) {
            $html = $this->load->view('relatorios/proposta/individual', $data, true);
            echo pdf_create($html, 'proposta' . date('d-m-y') . '_' . $idProposta, true, 'A4-L');
        } else {
            $html = $this->load->view('relatorios/proposta/individual', $data, false);
        }
    }
}

