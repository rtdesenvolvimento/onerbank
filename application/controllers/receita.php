<?php

class receita extends CI_Controller {

    function __construct() {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('receita_model', '', TRUE);
        $this->data['menuReceita'] = 'Plano de contas - Receita';
        $this->data['menuApoio'] = 'Apoio';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/receita/gerenciar/';
        $config['total_rows'] = $this->receita_model->count('receita');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->receita_model->get('',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'receita/receita';
        $this->load->view('theme/topo',$this->data);
    }

    function adicionar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('receita') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'codigo' => $this->input->post('codigo'),
                'nome' => $this->input->post('nome'),
                'receitaSuperior_id' => $this->input->post('receitaSuperior_id'),
            );

            if ($this->receita_model->add('receita', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Receita adicionada com sucesso!');
                redirect(base_url() . 'index.php/receita/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['receitas'] = $this->receita_model->getAll();
        $this->data['view'] = 'receita/adicionarReceita';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('receita') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'codigo' => $this->input->post('codigo'),
                'nome' => $this->input->post('nome'),
                'receitaSuperior_id' => $this->input->post('receitaSuperior_id'),
            );

            if ($this->receita_model->edit('receita', $data, 'idReceita', $this->input->post('idReceita')) == TRUE) {
                $this->session->set_flashdata('success', 'Receita editada com sucesso!');
                redirect(base_url() . 'index.php/receita/editar/'.$this->input->post('idReceita'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['receitas'] = $this->receita_model->getAll();
        $this->data['result'] = $this->receita_model->getById($this->uri->segment(3));
        $this->data['view'] = 'receita/editarReceita';
        $this->load->view('theme/topo', $this->data);
    }

    function excluir(){
        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir tipo de receita.');
            redirect(base_url().'index.php/receita/gerenciar/');
        }

        $this->receita_model->delete('receita','idReceita',$id);

        $this->session->set_flashdata('success','Receita excluida com sucesso!');
        redirect(base_url().'index.php/receita/gerenciar/');
    }
}

