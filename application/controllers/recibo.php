<?php

class Recibo extends CI_Controller {

    var $reciboService;

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));

        $this->load->model('reciboservice_model', '', TRUE);
        $this->load->model('reciborepository_model', '', TRUE);

        $this->load->model('mapos_model','',TRUE);
        $this->load->model('site_model', '', TRUE);

        $this->reciboService = $this->reciboservice_model;
    }

    public function gerar($reciboId) {
        try {
            $this->data['recibo'] = $this->reciboService->gerar($reciboId);
            $this->data['emitente'] = $this->mapos_model->getEmitente();

            $this->data['view'] = 'recibo/recibo';
            $this->load->view('tema/blank', $this->data);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function gerarReciboDePagamento($pagamentoId) {
        $recibo = $this->reciborepository_model->buscarReciboDePagamento($pagamentoId);
        $this->gerar($recibo->idRecibo);
    }
}

