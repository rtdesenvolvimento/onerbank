<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(E_ALL ^ E_DEPRECATED);

class Relatorios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->model('Relatorios_model', '', TRUE);
        $this->load->model('mapos_model', '', TRUE);
        $this->load->model('os_model', '', TRUE);
        $this->load->model('setor_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->load->model('centrocusto_model', '', TRUE);

        $this->data['menuRelatorios'] = 'Relatórios';
    }


    function producao()
    {
        $this->load->library('pagination');

        $where_array = array();

        $pesquisa   = $this->input->get('pesquisa');
        $status     = $this->input->get('status');
        $de         = $this->input->get('data_inicio');
        $ate        = $this->input->get('data_final');
        $setor          = $this->input->get('setor');
        $funcionario    = $this->input->get('funcionario');

        $this->data['pesquisa']     = $pesquisa;
        $this->data['status']       = $status;
        $this->data['data_inicio']  = $de;
        $this->data['data_final']   = $ate;
        $this->data['setor']        = $setor;
        $this->data['funcionario']  = $funcionario;

        if ($pesquisa) {
            $where_array['pesquisa'] = $pesquisa;
        }

        if ($status) {
            $where_array['status'] = $status;
        }

        if ($de) {
            $de = explode('/', $de);
            $de = $de[2] . '-' . $de[1] . '-' . $de[0];
            $where_array['de'] = $de;
        }

        if ($ate) {
            $ate = explode('/', $ate);
            $ate = $ate[2] . '-' . $ate[1] . '-' . $ate[0];
            $where_array['ate'] = $ate;
        }

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 10000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['produtos']  = array();
        $this->data['servicos']  = array();

        $this->data['view']     = 'relatorios/rel_producao';
        $this->data['setores']  = $this->setor_model->getSetorAll();
        $this->data['usuarios'] = $this->usuarios_model->getAll();
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('tema/topo', $this->data);
    }

    function producao_consulta()
    {
        $this->load->library('pagination');

        $where_array = array();

        $pesquisa       = $this->input->get('pesquisa');
        $status         = $this->input->get('status');
        $de             = $this->input->get('data_inicio');
        $ate            = $this->input->get('data_final');
        $setor          = $this->input->get('setor');
        $funcionario    = $this->input->get('funcionario');

        $this->data['pesquisa']     = $pesquisa;
        $this->data['status']       = $status;
        $this->data['data_inicio']  = $de;
        $this->data['data_final']   = $ate;
        $this->data['setor']        = $setor;
        $this->data['funcionario']  = $funcionario;

        if ($setor) {
            $where_array['setor'] = $setor;
        }

        if ($funcionario) {
            $where_array['funcionario'] = $funcionario;
        }

        if ($pesquisa) {
            $where_array['pesquisa'] = $pesquisa;
        }

        if ($status) {
            $where_array['status'] = $status;
        }

        if ($de) {
            $de = explode('/', $de);
            $de = $de[2] . '-' . $de[1] . '-' . $de[0];
            $where_array['de'] = $de;
        }

        if ($ate) {
            $ate = explode('/', $ate);
            $ate = $ate[2] . '-' . $ate[1] . '-' . $ate[0];
            $where_array['ate'] = $ate;
        }

        $config['base_url'] = base_url() . 'index.php/os/gerenciar/';
        $config['total_rows'] = $this->os_model->count('os');
        $config['per_page'] = 10000;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['produtos']  = $this->os_model->getOsAllProdutos( $where_array, $config['per_page'], $this->uri->segment(3));
        $this->data['servicos']  = $this->os_model->getOsAllServicos( $where_array, $config['per_page'], $this->uri->segment(3));


        $this->data['view']     = 'relatorios/rel_producao';
        $this->data['setores']  = $this->setor_model->getSetorAll();
        $this->data['usuarios'] = $this->usuarios_model->getAll();
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('tema/topo', $this->data);
    }

    public function clientes()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de clientes.');
            redirect(base_url());
        }
        $this->data['view'] = 'relatorios/rel_clientes';
        $this->load->view('theme/topo', $this->data);
    }

    public function produtos()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }
        $this->data['view'] = 'relatorios/rel_produtos';
        $this->load->view('theme/topo', $this->data);

    }

    public function clientesCustom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de clientes.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        $data['clientes'] = $this->Relatorios_model->clientesCustom($dataInicial, $dataFinal);

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirClientes', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirClientes', $data, true);
        pdf_create($html, 'relatorio_clientes' . date('d/m/y'), TRUE);

    }

    public function clientesCustomExcel() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de clientes.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        $clientes = $this->Relatorios_model->clientesCustom($dataInicial, $dataFinal);

        $this->load->library('excel');
        $this->excel->criarExcel($clientes, 'relatorio_clientes');

    }


    public function clientesCustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de clientes.');
            redirect(base_url());
        }

        $dataInicial    = $this->input->get('dataInicial');
        $dataFinal      = $this->input->get('dataFinal');

        $data['clientes'] = $this->Relatorios_model->clientesCustom($dataInicial, $dataFinal);

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirClientes', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirClientes', $data, true);
        echo pdf_create($html, 'relatorio_clientes' . date('d-m-y'), FALSE);

    }

    public function clientesRapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de clientes.');
            redirect(base_url());
        }

        $data['clientes'] = $this->Relatorios_model->clientesRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirClientes', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirClientes', $data, true);
        echo pdf_create($html, 'relatorio_clientes' . date('d-m-y'), FALSE);
    }

    public function produtosRapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $data['produtos'] = $this->Relatorios_model->produtosRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirProdutos', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);
        echo pdf_create($html, 'relatorio_produtos' . date('d-m-y'), FALSE);
    }

    public function produtosRapidMin()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $data['produtos'] = $this->Relatorios_model->produtosRapidMin();

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);
        echo pdf_create($html, 'relatorio_produtos' . date('d-m-y'), FALSE);
    }

    public function produtosCustom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $estoqueInicial = $this->input->get('estoqueInicial');
        $estoqueFinal = $this->input->get('estoqueFinal');

        $data['produtos'] = $this->Relatorios_model->produtosCustom($precoInicial, $precoFinal, $estoqueInicial, $estoqueFinal);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);
        pdf_create($html, 'relatorio_produtos' . date('d/m/y'), TRUE);
    }

    public function produtosCustomExcel()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $estoqueInicial = $this->input->get('estoqueInicial');
        $estoqueFinal = $this->input->get('estoqueFinal');

        $produtos = $this->Relatorios_model->produtosCustom($precoInicial, $precoFinal, $estoqueInicial, $estoqueFinal);

        $this->load->library('excel');
        $this->excel->criarExcel($produtos, 'relatorio_produtos');
    }

    public function produtosCustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $estoqueInicial = $this->input->get('estoqueInicial');
        $estoqueFinal = $this->input->get('estoqueFinal');

        $data['produtos'] = $this->Relatorios_model->produtosCustom($precoInicial, $precoFinal, $estoqueInicial, $estoqueFinal);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirProdutos', $data, true);
        echo pdf_create($html, 'relatorio_produtos' . date('d-m-y'), FALSE);
    }

    public function servicos()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de serviços.');
            redirect(base_url());
        }
        $this->data['view'] = 'relatorios/rel_servicos';
        $this->load->view('theme/topo', $this->data);

    }

    public function servicosCustom()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de serviços.');
            redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $data['servicos'] = $this->Relatorios_model->servicosCustom($precoInicial, $precoFinal);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirServicos', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirServicos', $data, true);
        pdf_create($html, 'relatorio_servicos' . date('d/m/y'), TRUE);
    }

    public function servicosCustomExcel()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de serviços.');
            redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $servicos = $this->Relatorios_model->servicosCustom($precoInicial, $precoFinal);

        $this->load->library('excel');
        $this->excel->criarExcel($servicos, 'relatorio_servicos');
    }

    public function servicosCustomPreview()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de serviços.');
            redirect(base_url());
        }

        $precoInicial = $this->input->get('precoInicial');
        $precoFinal = $this->input->get('precoFinal');
        $data['servicos'] = $this->Relatorios_model->servicosCustom($precoInicial, $precoFinal);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirServicos', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirServicos', $data, true);
        echo pdf_create($html, 'relatorio_servicos' . date('d-m-y'), FALSE);
    }

    public function servicosRapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de serviços.');
            redirect(base_url());
        }

        $data['servicos'] = $this->Relatorios_model->servicosRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirServicos', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirServicos', $data, true);
        echo pdf_create($html, 'relatorio_servicos' . date('d-m-y'), FALSE);
    }

    public function os()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }
        $this->data['view'] = 'relatorios/rel_os';
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('theme/topo', $this->data);
    }

    public function aniversario()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }
        $this->data['view'] = 'relatorios/rel_aniversario';
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        $this->load->view('theme/topo', $this->data);
    }

    public function aniversarioCustomPreview() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $mes = $this->input->get('mes');

        $data['clientes'] = $this->Relatorios_model->aniversarioCustom($mes);
        $data['mes'] = $mes;

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirAniversario', $data, true);
        echo pdf_create($html, 'relatorio_os' . date('d-m-y'), FALSE);
    }

    public function aniversarioCustom() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $mes = $this->input->get('mes');

        $data['clientes'] = $this->Relatorios_model->aniversarioCustom($mes);
        $data['mes'] = $mes;

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirAniversario', $data, true);
        pdf_create($html, 'relatorio_os' . date('d-m-y'), TRUE);
    }

    public function aniversarioCustomExcel() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $mes = $this->input->get('mes');

        $clientes = $this->Relatorios_model->aniversarioCustom($mes);

        $this->load->library('excel');
        $this->excel->criarExcel($clientes, 'relatorio_aniversariantes_mes');
    }

    public function osRapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $data['os'] = $this->Relatorios_model->osRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirOs', $data, true);
        $this->data['emitente'] = $this->mapos_model->getEmitente();
        echo pdf_create($html, 'relatorio_os' . date('d-m-y'), FALSE);
    }

    public function osCustom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');
        $status = $this->input->get('status');
        $data['os'] = $this->Relatorios_model->osCustom($dataInicial, $dataFinal, $cliente, $responsavel, $status);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirOs', $data, true);
        pdf_create($html, 'relatorio_os' . date('d/m/y'), TRUE);
    }

    public function osCustomExcel()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');
        $status = $this->input->get('status');
        $os = $this->Relatorios_model->osCustom($dataInicial, $dataFinal, $cliente, $responsavel, $status);

        $this->load->library('excel');
        $this->excel->criarExcel($os, 'relatorio_os');
    }

    public function osCustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de OS.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');
        $status = $this->input->get('status');
        $data['os'] = $this->Relatorios_model->osCustom($dataInicial, $dataFinal, $cliente, $responsavel, $status);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirOs', $data, true);
        echo pdf_create($html, 'relatorio_os' . date('d-m-y'), FALSE);
    }

    public function financeiro()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios financeiros.');
            redirect(base_url());
        }

        $this->data['view'] = 'relatorios/rel_financeiro';
        $this->load->view('theme/topo', $this->data);
    }


    public function financeiroRapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios financeiros.');
            redirect(base_url());
        }

        $data['lancamentos'] = $this->Relatorios_model->financeiroRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirFinanceiro', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirFinanceiro', $data, true);
        echo pdf_create($html, 'relatorio_os' . date('d-m-y'), FALSE);
    }

    public function financeiroCustom()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios financeiros.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        $previsaoPagamentoInicial = $this->input->get('previsaoPagamentoInicial');
        $previsaoPagamentoFinal = $this->input->get('previsaoPagamentoFinal');

        $tipo = $this->input->get('tipo');
        $situacao = $this->input->get('situacao');

        $data['lancamentos'] = $this->Relatorios_model->financeiroCustom($dataInicial, $dataFinal, $previsaoPagamentoInicial, $previsaoPagamentoFinal, $tipo, $situacao);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirFinanceiro', $data, true);

        pdf_create($html, 'relatorio_financeiro' . date('d/m/y'), TRUE);
    }

    public function financeiroCustomExcel()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios financeiros.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        $previsaoPagamentoInicial = $this->input->get('previsaoPagamentoInicial');
        $previsaoPagamentoFinal = $this->input->get('previsaoPagamentoFinal');

        $tipo = $this->input->get('tipo');
        $situacao = $this->input->get('situacao');

        $lancamentos = $this->Relatorios_model->financeiroCustom($dataInicial, $dataFinal, $previsaoPagamentoInicial, $previsaoPagamentoFinal, $tipo, $situacao);

        $this->load->library('excel');
        $this->excel->criarExcel($lancamentos, 'relatorio_financeiro');
    }

    public function financeiroCustomPreview()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios financeiros.');
            redirect(base_url());
        }

        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');

        $previsaoPagamentoInicial = $this->input->get('previsaoPagamentoInicial');
        $previsaoPagamentoFinal = $this->input->get('previsaoPagamentoFinal');

        $tipo = $this->input->get('tipo');
        $situacao = $this->input->get('situacao');

        $data['lancamentos'] = $this->Relatorios_model->financeiroCustom($dataInicial, $dataFinal, $previsaoPagamentoInicial, $previsaoPagamentoFinal, $tipo, $situacao);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirFinanceiro', $data, true);

        echo pdf_create($html, 'relatorio_financeiro' . date('d-m-y'), FALSE);
    }

    public function vendas_pdv()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }

        $this->data['view'] = 'relatorios/rel_vendas_pdv';
        $this->load->view('theme/topo', $this->data);
    }
    public function vendas()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }

        $this->data['view'] = 'relatorios/rel_vendas';
        $this->load->view('theme/topo', $this->data);
    }

    public function vendasRapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $data['vendas'] = $this->Relatorios_model->vendasRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);
        echo pdf_create($html, 'relatorio_vendas' . date('d-m-y'), FALSE);
    }

    public function vendas_pdv_Rapid()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $data['vendas'] = $this->Relatorios_model->vendasPdvRapid();

        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);
        echo pdf_create($html, 'relatorio_vendas' . date('d-m-y'), FALSE);
    }

    public function vendasCustom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $data['vendas'] = $this->Relatorios_model->vendasCustom($dataInicial, $dataFinal, $cliente, $responsavel);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);
        pdf_create($html, 'relatorio_vendas' . date('d/m/y'), TRUE);
    }

    public function vendas_pdv_Custom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $data['vendas'] = $this->Relatorios_model->vendasPdvCustom($dataInicial, $dataFinal, $cliente, $responsavel);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);
        pdf_create($html, 'relatorio_vendas' . date('d/m/y'), TRUE);
    }

    public function vendasCustomExcel()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $vendas = $this->Relatorios_model->vendasCustom($dataInicial, $dataFinal, $cliente, $responsavel);

        $this->load->library('excel');
        $this->excel->criarExcel($vendas, 'relatorio_vendas');
    }

    public function vendas_pdv_CustomExcel()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $vendas = $this->Relatorios_model->vendasPdvCustom($dataInicial, $dataFinal, $cliente, $responsavel);

        $this->load->library('excel');
        $this->excel->criarExcel($vendas, 'relatorio_vendas');
    }

    public function vendasCustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $data['vendas'] = $this->Relatorios_model->vendasCustom($dataInicial, $dataFinal, $cliente, $responsavel);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);
        echo pdf_create($html, 'relatorio_vendas' . date('d-m-y'), FALSE);
    }

    public function vendas_pdv_CustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de vendas.');
            redirect(base_url());
        }
        $dataInicial = $this->input->get('dataInicial');
        $dataFinal = $this->input->get('dataFinal');
        $cliente = $this->input->get('cliente');
        $responsavel = $this->input->get('responsavel');

        $data['vendas'] = $this->Relatorios_model->vendasPdvCustom($dataInicial, $dataFinal, $cliente, $responsavel);
        $this->load->helper('mpdf');
        //$this->load->view('relatorios/imprimir/imprimirOs', $data);
        $html = $this->load->view('relatorios/imprimir/imprimirVendas', $data, true);
        echo pdf_create($html, 'relatorio_vendas' . date('d-m-y'), FALSE);
    }


    /*
     * PATRIMONIO
     */

    public function patrimonio()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }
        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['centroCustos']         = $this->centrocusto_model->getAll();
        $this->data['view'] = 'relatorios/rel_patrimonio';
        $this->load->view('theme/topo', $this->data);

    }

    public function patrimonioCustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $descricao       = $this->input->get('descricao');
        $grupoProduto_id = $this->input->get('grupoProduto_id');
        $centro_custo_id = $this->input->get('centro_custo_id');
        $origemAquisicao = $this->input->get('origemAquisicao');

        $data['produtos'] = $this->Relatorios_model->patrimonioCustomizado($descricao, $grupoProduto_id, $centro_custo_id, $origemAquisicao);
        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirPatrimonio', $data, true);
        echo pdf_create($html, 'relatorio_produtos' . date('d-m-y'), FALSE);
    }

    public function patrimonioCustom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $descricao       = $this->input->get('descricao');
        $grupoProduto_id = $this->input->get('grupoProduto_id');
        $centro_custo_id = $this->input->get('centro_custo_id');
        $origemAquisicao = $this->input->get('origemAquisicao');

        $data['produtos']= $this->Relatorios_model->patrimonioCustomizado($descricao, $grupoProduto_id, $centro_custo_id, $origemAquisicao);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirPatrimonio', $data, true);
        pdf_create($html, 'relatorio_patrimonio' . date('d/m/y'), TRUE);
    }

    public function patrimonioCustomExcel()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $descricao       = $this->input->get('descricao');
        $grupoProduto_id = $this->input->get('grupoProduto_id');
        $centro_custo_id = $this->input->get('centro_custo_id');
        $origemAquisicao = $this->input->get('origemAquisicao');

        $produtos = $this->Relatorios_model->patrimonioCustomizado($descricao, $grupoProduto_id, $centro_custo_id, $origemAquisicao);

        $this->load->library('excel');
        $this->excel->criarExcel($produtos, 'relatorio_patrimonio');
    }

    /*
     * PGDAS RELATORIO DE PAGAMENTO DA DAS
     */
    public function pgdas()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }
        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['centroCustos']         = $this->centrocusto_model->getAll();
        $this->data['view'] = 'relatorios/rel_pgdas';
        $this->load->view('theme/topo', $this->data);

    }

    public function pgdasCustomPreview()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $dataInicio = $this->input->get('dataInicio');
        $dataFinal = $this->input->get('dataFinal');

        $data['nfe'] = $this->Relatorios_model->pgdasNFeCustom($dataInicio, $dataFinal);
        $data['nfce'] = $this->Relatorios_model->pgdasNFCeCustom($dataInicio, $dataFinal);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirPgdas', $data, true);
        echo pdf_create($html, 'relatorio_produtos' . date('d-m-y'), FALSE, 'A4-L');
    }

    public function pgdasCustom()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para gerar relatórios de produtos.');
            redirect(base_url());
        }

        $dataInicio = $this->input->get('dataInicio');
        $dataFinal = $this->input->get('dataFinal');

        $data['nfe'] = $this->Relatorios_model->pgdasNFeCustom($dataInicio, $dataFinal);
        $data['nfce'] = $this->Relatorios_model->pgdasNFCeCustom($dataInicio, $dataFinal);

        $this->load->helper('mpdf');
        $html = $this->load->view('relatorios/imprimir/imprimirPgdas', $data, true);
        pdf_create($html, 'relatorio_pgdas' . date('d/m/y'), TRUE);
    }

}
