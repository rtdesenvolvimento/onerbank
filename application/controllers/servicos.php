<?php

class Servicos extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('setor_model', '', TRUE);
        $this->load->model('grupoproduto_model', '', TRUE);
        $this->data['menuServicos'] = 'Serviços';
        $this->data['menuCadastro'] = 'Cadastro';
    }
	
	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vServico')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar serviços.');
           redirect(base_url());
        }

        $this->load->library('pagination');
        
        
        $config['base_url'] = base_url().'index.php/servicos/gerenciar/';
        $config['total_rows'] = $this->servicos_model->count('servicos');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config); 	

        $this->data['results'] = $this->servicos_model->getServicos('',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'servicos/servicos';
       	$this->load->view('theme/topo',$this->data);
    }
	
	function buscar() {
		if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vServico')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar serviços.');
           redirect(base_url());
        }

        $this->load->library('pagination');
        
        $config['base_url'] = base_url().'index.php/servicos/gerenciar/';
        $config['total_rows'] = $this->servicos_model->count('servicos');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config); 		
 		$parametro = $this->uri->segment(3);
		$parametro = str_replace("%20", "%" , $parametro);
	
        $where = '';
		if ($parametro) {
            $where = 'p.nome like "%'.$parametro.'%" or  p.descricao like "%'.$parametro.'%" or s.nome like "%'.$parametro.'%" or g.nome like "%'.$parametro.'%" ';

        }
		
        $this->data['results'] = $this->servicos_model->getServicos($where,$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'servicos/servicos';
       	$this->load->view('theme/topo',$this->data);
	}
	
    function adicionar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aServico')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar serviços.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('servicos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $preco = $this->input->post('preco');
            $preco = str_replace(",","", $preco);

            $comissao = $this->input->post('comissao');
            $comissao = str_replace(",","", $comissao);
            $isISS      =  $this->input->post('isISS');

            if ($isISS == 'on') {
                $isISS = 1;
            } else {
                $isISS = 0;
            }

            $data = array(
                'nome'              => set_value('nome'),
               // 'grupoProduto_id'   => $this->input->post('grupoProduto_id'),
               // 'setor_id'          => $this->input->post('setor_id'),
                'descricao'         => set_value('descricao'),
                'preco'             => $preco,
                'comissao'          => $comissao,
                'isISS'             => $isISS,
                'codigoServico'     => $this->input->post('codigoServico'),
                'aliquotaISS'       => $this->input->post('aliquotaISS'),
                'cnae'              => $this->input->post('cnae'),
            );

            $idServico = $this->servicos_model->add('servicos', $data , TRUE);

            if ($idServico) {
                $this->session->set_flashdata('success', 'Serviço adicionado com sucesso!');
                redirect(base_url() . 'index.php/servicos/editar/'.$idServico);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['setores']          = $this->setor_model->getSetorAll();
        $this->data['gruposProduto']    = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['view'] = 'servicos/adicionarServico';
        $this->load->view('theme/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eServico')){
           $this->session->set_flashdata('error','Você não tem permissão para editar serviços.');
           redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('servicos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $preco = $this->input->post('preco');
            $preco = str_replace(",","", $preco);

            $comissao   = $this->input->post('comissao');
            $comissao   = str_replace(",","", $comissao);
            $isISS      =  $this->input->post('isISS');

            if ($isISS == 'on') {
                $isISS = 1;
            } else {
                $isISS = 0;
            }

            $data = array(
                'nome'              => $this->input->post('nome'),
                'descricao'         => $this->input->post('descricao'),
               // 'grupoProduto_id'   => $this->input->post('grupoProduto_id'),
               // 'setor_id'          => $this->input->post('setor_id'),
                'preco'             => $preco,
                'comissao'          => $comissao,
                'isISS'             => $isISS,
                'codigoServico'     => $this->input->post('codigoServico'),
                'aliquotaISS'       => $this->input->post('aliquotaISS'),
                'cnae'              => $this->input->post('cnae'),
            );

            if ($this->servicos_model->edit('servicos', $data, 'idServicos', $this->input->post('idServicos')) == TRUE) {
                $this->session->set_flashdata('success', 'Serviço editado com sucesso!');
                redirect(base_url() . 'index.php/servicos/editar/'.$this->input->post('idServicos'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['setores']              = $this->setor_model->getSetorAll();
        $this->data['gruposProduto']        = $this->grupoproduto_model->getGruposProdutoAll();
        $this->data['result']               = $this->servicos_model->getById($this->uri->segment(3));
        $this->data['fornecedoresservico']  = $this->servicos_model->getFornecedoresServico($this->uri->segment(3));
        $this->data['view'] = 'servicos/editarServico';
        $this->load->view('theme/topo', $this->data);

    }
	
    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dServico')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir serviços.');
           redirect(base_url());
        }
        
        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir serviço.');            
            redirect(base_url().'index.php/servicos/gerenciar/');
        }

        $this->db->where('servicos_id', $id);
        $this->db->delete('servicos_os');

        $this->servicos_model->delete('servicos','idServicos',$id);

        $this->session->set_flashdata('success','Serviço excluido com sucesso!');            
        redirect(base_url().'index.php/servicos/gerenciar/');
    }

    public function salvarFornecedorServico()
    {
        $idFornecedorservico    =  $this->input->post('idFornecedorservico');
        $idFornecedor           = $this->input->post('idFornecedor');
        $idServico              = $this->input->post('idServico');
        $codigo                 = $this->input->post('codigo');
        $observacao             = $this->input->post('observacao');

        $data = array(
            'idFornecedor'  => $idFornecedor,
            'idServico'     => $idServico,
            'codigo'        => $codigo,
            'observacao'    => $observacao,
        );

        if ($idFornecedorservico) {
            if ($this->servicos_model->edit('fornecedorservico', $data, 'idFornecedorservico', $idFornecedorservico) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            if ($this->servicos_model->add('fornecedorservico', $data) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        }
    }

    function excluirFornecedorServico()
    {
        $ID = $this->input->post('idFornecedorservico');
        if ($this->servicos_model->delete('fornecedorservico', 'idFornecedorservico', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    function getServicoJson() {
        $servicos_id = $this->input->post('servicos_id');
        echo json_encode($this->servicos_model->getById($servicos_id));
    }

}

