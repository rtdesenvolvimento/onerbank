<?php

class setor extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('setor_model', '', TRUE);
        $this->data['menuSetor'] = 'Setor';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vSetor')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar setores.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/setor/gerenciar/';
        $config['total_rows'] = $this->setor_model->count('setor');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->setor_model->get('setor','idSetor,nome,descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'setor/setor';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aSetor')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar setores.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('setor') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao')
            );

            if ($this->setor_model->add('setor', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Setor adicionado com sucesso!');
                redirect(base_url() . 'index.php/setor/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'setor/adicionarSetor';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eSetor')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar setores.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('setor') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->setor_model->edit('setor', $data, 'idSetor', $this->input->post('idSetor')) == TRUE) {
                $this->session->set_flashdata('success', 'Setor editado com sucesso!');
                redirect(base_url() . 'index.php/setor/editar/'.$this->input->post('idSetor'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->setor_model->getById($this->uri->segment(3));
        $this->data['view'] = 'setor/editarSetor';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dSetor')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir setores.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir setor.');
            redirect(base_url().'index.php/setor/gerenciar/');
        }

        $this->setor_model->delete('setor','idSetor',$id);

        $this->session->set_flashdata('success','Setor excluido com sucesso!');
        redirect(base_url().'index.php/setor/gerenciar/');
    }
}

