<?php

class sincronizacao extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('sincronizacao_model', '', TRUE);
    }

    function index(){}

    function donwloadPermissoes() {
        echo "apiStatusDownloadPermissoes(".json_encode($this->sincronizacao_model->getAllPermissoes() ).");";
    }

    function uploadPermissoes() {
        $cargo = $this->input->get('cargos');

        if ( !is_array($cargo) ) return;

        $id                 = $cargo['id'];
        $telefoneUsuario    =  $cargo['telefoneUsuario'].'_'.$id;

        $data = array(
            'idPermissao'       => $id,
            'nome'              => $cargo['descricao'],
            'data'              =>  date('Y-m-d'),
            'permissoes'        => null,
            'situacao'          => 1,
            'telefoneUsuario'   => $telefoneUsuario,
        );

        if ($this->verificaExistenciaById('permissoes', 'idPermissao',$id)) {
            $this->sincronizacao_model->edit('permissoes', $data,'idPermissao', $id);
        } else {
            $this->sincronizacao_model->add('permissoes', $data);
        }


        echo "apiStatusUploadPermissoes(".json_encode(array("Status" => 'Ok')).");";
    }

    function donwloadUsuarios() {
        echo "apiStatusDownloadUsuarios(".json_encode($this->sincronizacao_model->getAllUsuarios() ).");";
    }

    function uploadUsuarios() {
        $usuario = $this->input->get('usuarios');

        if ( !is_array($usuario) ) return;

        $id                 =  $usuario['id'];
        $telefoneUsuario    =  $usuario['telefoneUsuario'].'_'.$id;

        $data = array(
            'idUsuarios'        => $id,
            'nome'              => $usuario['nomeCompleto'],
            'email'             => $usuario['email'],
            'usuariologin'             => $usuario['email'],
            'celular'           => $usuario['telefoneUsuario'],
            'telefone'          => $usuario['telefoneUsuario'],
            'situacao'          => $usuario['ativo'],
            'senha'          => $usuario['password'],
            'photo'             => $usuario['foto'],
            'permissoes_id'     => $usuario['cargo'],
            'telefoneUsuario'   => $telefoneUsuario,
        );

        if ($this->verificaExistenciaById('usuarios', 'idUsuarios', $id)) {
            $this->sincronizacao_model->edit('usuarios', $data,'idUsuarios', $id);
        } else {
            $this->sincronizacao_model->addUsuario('usuarios', $data);
        }


        echo "apiStatusUploadUsuarios(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadTipoTarefa() {
        echo "apiStatusDonwloadTipoTarefa(".json_encode($this->sincronizacao_model->getAllTiposTarefa() ).");";
    }

    function uploadTipoTarefa() {
        $tipoTarefa = $this->input->get('tiposTarefa');

        if ( !is_array($tipoTarefa) ) return;

        $id                 = $tipoTarefa['id'];
        $telefoneUsuario    = $tipoTarefa['telefoneUsuario'].'_'.$id;

        $data = array(
            'idTipoTarefa'      => $id,
            'descricao'         => $tipoTarefa['descricao'],
            'telefoneUsuario'   => $telefoneUsuario,
        );

        if ($this->verificaExistenciaById('tipotarefa', 'idTipoTarefa', $id)) {
            $this->sincronizacao_model->edit('tipotarefa', $data,'idTipoTarefa', $id);
        } else {
            $this->sincronizacao_model->add('tipotarefa', $data);
        }

        echo "apiStatusUploadTipoTarefa(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadFormulario() {
        echo "apiStatusDonwloadFormulario(".json_encode($this->sincronizacao_model->getAllFormulario() ).");";
    }

    function uploadFormulario() {

        $formulario = $this->input->get('formularios');

        if ( !is_array($formulario) ) return;

        $id                 = $formulario['id'];
        $telefoneUsuario    = $formulario['telefoneUsuario'].'_'.$id;

        $data = array(
            'idFormulario'      => $id,
            'descricao'         => $formulario['descricao'],
            'telefoneUsuario'   => $telefoneUsuario,
        );

        if ($this->verificaExistenciaById('formulario', 'idFormulario', $id)) {
            $this->sincronizacao_model->edit('formulario', $data,'idFormulario', $id);
        } else {
            $this->sincronizacao_model->add('formulario', $data);
        }

        echo "apiStatusUploadFormulario(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadPerguntasFormulario() {
        echo "apiStatusDonwloadPerguntaFormulario(".json_encode($this->sincronizacao_model->getAllPerguntasFormulario() ).");";
    }

    function uploadPerguntasFormulario() {
        $perguntas = $this->input->get('perguntasFormulario');

        if ( !is_array($perguntas) ) return;

        $id                 = $perguntas['id'];
        $telefoneUsuario    = $perguntas['telefoneUsuario'].'_'.$id;

        $data = array(
            'idPerguntaFormulario'  => $id,
            'formulario'            => $perguntas['formulario'],
            'pergunta'              => $perguntas['pergunta'],
            'tipoCampo'             => $perguntas['tipoCampo'],
            'descricao'             => $perguntas['descricao'],
            'peso'                  => $perguntas['peso'],
            'obrigatorio'           => $perguntas['obrigatorio'],
            'ativo'                 => $perguntas['ativo'],
            'anterior'              => $perguntas['anterior'],
            'proxima'               => $perguntas['proxima'],
            'alias'                 => $perguntas['alias'],
            'pagina'                => $perguntas['pagina'],
            'ordem'                 => $perguntas['ordem'],
            'telefoneUsuario'       => $telefoneUsuario,
        );
        if ($this->verificaExistenciaById('perguntaformulario', 'idPerguntaFormulario', $id)) {
            $this->sincronizacao_model->edit('perguntaformulario', $data,'idPerguntaFormulario', $id);
        } else {
            $this->sincronizacao_model->add('perguntaformulario', $data);
        }

        echo "apiStatusUploadPerguntasFormulario(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadPerguntasFormularioOpcao() {
        echo "apiStatusDonwloadPerguntaFormularioOpcao(".json_encode($this->sincronizacao_model->getAllPerguntasFormularioOpcao() ).");";
    }

    function uploadPerguntasFormularioOpcao() {
        $perguntas = $this->input->get('perguntasFormularioOpcao');

        if ( !is_array($perguntas) ) return;

        $id                 = $perguntas['id'];
        $telefoneUsuario    = $perguntas['telefoneUsuario'].'_'.$id;

        $data = array(
            'idPerguntaFormularioOpcao' => $id,
            'formulario'                => $perguntas['formulario'],
            'pergunta'                  => $perguntas['pergunta'],
            'peso'                      => $perguntas['peso'],
            'anterior'                  => $perguntas['anterior'],
            'proxima'                   => $perguntas['proxima'],
            'opcao'                     => $perguntas['opcao'],
            'telefoneUsuario'           => $telefoneUsuario,
        );

        if ($this->verificaExistenciaById('perguntaformularioopcao', 'idPerguntaFormularioOpcao', $id)) {
            $this->sincronizacao_model->edit('perguntaformularioopcao', $data,'idPerguntaFormularioOpcao', $id);
        } else {
            $this->sincronizacao_model->add('perguntaformularioopcao', $data);
        }

        echo "apiStatusUploadPerguntaFormulariOpcao(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadTarefa() {
        echo "apiStatusDonwloadTarefa(".json_encode($this->sincronizacao_model->getAllTarefa() ).");";
    }

    function uploadTarefa() {
        $tarefa = $this->input->get('tarefas');

        if ( !is_array($tarefa) ) return;

        $id                 = $tarefa['id'];
        $telefoneUsuario    = $tarefa['telefoneUsuario'].'_'.$id;

        $data = array(
            'responsavel'       => $tarefa['responsavel'],
            'tipoTarefa'        => $tarefa['tipoTarefa'],
            'atribuir'          => $tarefa['atribuir'],
            'status'            => $tarefa['status'],
            'descricao'         => $tarefa['descricao'],
            'titulo'            => $tarefa['titulo'],
            'data'              => $tarefa['data'],
            'horaInicio'        => $tarefa['horaInicio'],
            'horaFinal'         => $tarefa['horaFinal'],
            'app_id'            => $id,
            'telefoneUsuario'   => $telefoneUsuario,
        );

        if ($this->verificaExistencia('tarefa', $telefoneUsuario) ) {
            $this->sincronizacao_model->edit('tarefa', $data,'telefoneUsuario', $telefoneUsuario);
        } else {
            $this->sincronizacao_model->add('tarefa', $data);
        }

        echo "apiStatusUploadTarefa(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadProposta() {
        echo "apiStatusDonloadProposta(".json_encode($this->sincronizacao_model->getAllPropostas() ).");";
    }

    function uploadProposta() {
        $proposta = $this->input->get('propostas');

        if ( !is_array($proposta) ) return;

        $id                 = $proposta['id'];
        $telefoneUsuario    = $proposta['telefoneUsuario'].'_'.$id;

        $data = array(
            'descricao'         => $proposta['descricao'],
            'status'            => $proposta['status'],
            'formulario'        => $proposta['formulario'],
            'cliente'           => $proposta['cliente'],
            'observacao'        => $proposta['observacao'],
            'dataProposta'      => $proposta['dataProposta'],
            'dataAberta'        => $proposta['dataAberta'],
            'dataAguardando'    => $proposta['dataAguardando'],
            'dataFechamento'    => $proposta['dataFechamento'],
            'latitude'          => $proposta['latitude'],
            'longitude'         => $proposta['longitude'],
            'altitude'          => $proposta['altitude'],
            'visitaConfirmada'  => $proposta['visitaConfirmada'],
            'responsavel'       => $proposta['responsavel'],
            'app_id'            => $id,
            'telefoneUsuario'   => $telefoneUsuario,

        );

        if ($this->verificaExistencia('proposta', $telefoneUsuario) ) {
            $this->sincronizacao_model->edit('proposta', $data,'telefoneUsuario', $telefoneUsuario);
        } else {
            $this->sincronizacao_model->add('proposta', $data);
        }

        echo "apiStatusUploadProposta(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadRespostasProposta() {
        echo "apiStatusDonloadResponstasProposta(".json_encode($this->sincronizacao_model->getAllRespostasProposta() ).");";
    }

    function uploadRespostasProposta() {
        $resposta = $this->input->get('respostasPropostas');

        if ( !is_array($resposta) ) return;

        $id                 = $resposta['id'];
        $telefoneUsuario    = $resposta['telefoneUsuario'].'_'.$id;

        $data = array(
            'proposta'          => $resposta['proposta'],
            'formulario'        => $resposta['formulario'],
            'pergunta'          => $resposta['pergunta'],
            'resposta'          => $resposta['resposta'],
            'tipoCampo'         => $resposta['tipoCampo'],
            'descricao'         => $resposta['descricao'],
            'peso'              => $resposta['peso'],
            'obrigatorio'       => $resposta['obrigatorio'],
            'anterior'          => $resposta['anterior'],
            'proxima'           => $resposta['proxima'],
            'alias'             => $resposta['alias'],
            'app_id'            => $id,
            'telefoneUsuario'   => $telefoneUsuario,
        );

        if ($this->verificaExistencia('respostaproposta', $telefoneUsuario) ) {
            $this->sincronizacao_model->edit('respostaproposta', $data,'telefoneUsuario', $telefoneUsuario);
        } else {
            $this->sincronizacao_model->add('respostaproposta', $data);
        }

        echo "apiStatusUploadResponstaPropostas(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadRespostasPropostaOpcao() {
        echo "apiStatusDonwloadResponstasPropostasOpcao(".json_encode($this->sincronizacao_model->getAllRespostasPropostaOpcao() ).");";
    }

    function uploadRespostasPropostaOpcao() {
        $resposta = $this->input->get('respostasPropostasOpcao');

        if ( !is_array($resposta) ) return;

        $id                 = $resposta['id'];
        $telefoneUsuario    = $resposta['telefoneUsuario'].'_'.$id;

        $data = array(
            'pergunta'              => $resposta['pergunta'],
            'formulario'            => $resposta['formulario'],
            'proposta'              => $resposta['proposta'],
            'formularioOpcao'       => $resposta['formularioOpcao'],
            'opcao'                 => $resposta['opcao'],
            'resposta'              => $resposta['resposta'],
            'peso'                  => $resposta['peso'],
            'anterior'              => $resposta['anterior'],
            'proxima'               => $resposta['proxima'],
            'app_id'                => $id,
            'telefoneUsuario'       => $telefoneUsuario,
        );

        if ($this->verificaExistencia('respostapropostaopcao', $telefoneUsuario) ) {
            $this->sincronizacao_model->edit('respostapropostaopcao', $data,'telefoneUsuario', $telefoneUsuario);
        } else {
            $this->sincronizacao_model->add('respostapropostaopcao', $data);
        }

        echo "apiStatusUploadRespostasPropostasOpcao(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadClientes() {
        echo "apiStatusDonwloadClientes(".json_encode($this->sincronizacao_model->getAllClientes() ).");";
    }

    function uploadClientes() {
        $cliente = $this->input->get('clientes');

        if ( !is_array($cliente) ) return;

        $id                 = $cliente['id'];
        $telefoneUsuario    = $cliente['telefoneUsuario'].'_'.$id;

        $data = array(
            'tipoPessoa'            => $cliente['tipoPessoa'],
            'nomeCliente'           => $cliente['nome'],
            'documento'             => $cliente['cpfCnpj'],
            'telefone'              => $cliente['telefone'],
            'celular'               => $cliente['celular'],
            'ie'                    => $cliente['ie'],
            'sexo'                  => $cliente['sexo'],
            'rg'                    => $cliente['rg'],
            'orgaoEmissor'          => $cliente['orgaoEmissor'],
            'estadoOrgaoEmissor'    => $cliente['estadoEmissor'],
            'dataOrgaoEmissor'      => $cliente['dataEmissao'],
            'cep'                   => $cliente['cep'],
            'rua'                   => $cliente['rua'],
            'numero'                => $cliente['numero'],
            'complemento'           => $cliente['complemento'],
            'bairro'                => $cliente['bairro'],
            'cidade'                => $cliente['cidade'],
            'estado'                => $cliente['estado'],
            'contatoNomeCliente'    => $cliente['nomeContato'],
            'contatoTelefone'       => $cliente['telefoneContato'],
            'contatoEmail'          => $cliente['emailContato'],
            'cargoContato'          => $cliente['cargoContato'],
            'observacao'            => $cliente['observacao'],
            'inativo'               => $cliente['ativo'],
            'responsavel'           => $cliente['responsavel'],
            'app_id'                => $id,
            'telefoneUsuario'       => $telefoneUsuario,

        );

        if ($this->verificaExistencia('clientes', $telefoneUsuario) ) {
            $this->sincronizacao_model->edit('clientes', $data,'telefoneUsuario', $telefoneUsuario);
        } else {
            $this->sincronizacao_model->add('clientes', $data);
        }

        echo "apiStatusUploadClientes(".json_encode(array("Status" => 'Ok')).");";

    }

    function donwloadDigitalizacao() {
        echo "apiStatusDonwloadDigitalizacao(".json_encode($this->sincronizacao_model->getAllDigitalizacao() ).");";
    }

    function uploadDigitalizacao() {
        $digitalizacao = $this->input->get('digitalizacao');

        if ( !is_array($digitalizacao) ) return;

        $id                 = $digitalizacao['id'];
        $telefoneUsuario    = $digitalizacao['telefoneUsuario'].'_'.$id;

        $data = array(
            'documento'         => $digitalizacao['documento'],
            'observacao'        => $digitalizacao['observacao'],
            'cliente'           => $digitalizacao['cliente'],
            'app_id'            => $id,
            'telefoneUsuario'   => $digitalizacao['telefoneUsuario'].'_'.$id,
        );

        if ($this->verificaExistencia('digitalizacao', $telefoneUsuario) ) {
            $this->sincronizacao_model->edit('digitalizacao', $data,'telefoneUsuario', $telefoneUsuario);
        } else {
            $this->sincronizacao_model->add('digitalizacao', $data);
        }

        echo "apiStatusUploadDigitalizacao(".json_encode(array("Status" => 'Ok')).");";

    }

    function verificaExistencia($tabela, $telefoneUsuario_id) {

        $this->db->from($tabela);
        $this->db->where('telefoneUsuario',$telefoneUsuario_id);
        $this->db->limit(1);
        $resultado = $this->db->get()->row();

        if (count($resultado) > 0) {
            return true;
        } else {
            return false;
        }
    }

    function verificaExistenciaById($tabela, $coluna, $id) {

        $this->db->from($tabela);
        $this->db->where($coluna,$id);
        $this->db->limit(1);
        $resultado = $this->db->get()->row();

        if (count($resultado) > 0) {
            return true;
        } else {
            return false;
        }
    }
}

