<?php

class tabelacest extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('unidade_model', '', TRUE);
        $this->data['menuUnidade'] = 'Unidade';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vUnidade')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar unidades.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/unidade/gerenciar/';
        $config['total_rows'] = $this->unidade_model->count('unidade');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->unidade_model->get('unidade','idUnidade,nome, sigla, descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'unidade/unidade';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aUnidade')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar unidades.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('unidade') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome'      => set_value('nome'),
                'sigla'     => $this->input->post('sigla'),
                'descricao' => set_value('descricao')
            );

            if ($this->unidade_model->add('unidade', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Unidade adicionada com sucesso!');
                redirect(base_url() . 'index.php/unidade/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'unidade/adicionarUnidade';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eUnidade')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar uniades.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('unidade') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome'      => $this->input->post('nome'),
                'sigla'     => $this->input->post('sigla'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->unidade_model->edit('unidade', $data, 'idUnidade', $this->input->post('idUnidade')) == TRUE) {
                $this->session->set_flashdata('success', 'Aparelho editado com sucesso!');
                redirect(base_url() . 'index.php/unidade/editar/'.$this->input->post('idUnidade'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->unidade_model->getById($this->uri->segment(3));
        $this->data['view'] = 'unidade/editarUnidade';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dUnidade')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir unidades.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir a unidade.');
            redirect(base_url().'index.php/unidade/gerenciar/');
        }

        $this->unidade_model->delete('unidade','idUnidade',$id);

        $this->session->set_flashdata('success','Unidade excluida com sucesso!');
        redirect(base_url().'index.php/unidade/gerenciar/');
    }
}

