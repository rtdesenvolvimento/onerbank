<?php

class taxas extends CI_Controller {

    function __construct() {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('taxas_model', '', TRUE);
        $this->data['menuApoio'] = 'Apoio';
        $this->data['menuTaxas'] = 'Taxas do cartão';
    }

    function index(){
        $this->editar();
    }

    function editar() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('taxas') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'tarifaFixa'            => $this->input->post('tarifaFixa'),
                'taxaIntermediacao'     => $this->input->post('taxaIntermediacao'),
                'taxaParcelamento1x'    => $this->input->post('taxaParcelamento1x'),
                'taxaParcelamento2x'    => $this->input->post('taxaParcelamento2x'),
                'taxaParcelamento3x'    => $this->input->post('taxaParcelamento3x'),
                'taxaParcelamento4x'    => $this->input->post('taxaParcelamento4x'),
                'taxaParcelamento5x'    => $this->input->post('taxaParcelamento5x'),
                'taxaParcelamento6x'    => $this->input->post('taxaParcelamento6x'),
                'taxaParcelamento7x'    => $this->input->post('taxaParcelamento7x'),
                'taxaParcelamento8x'    => $this->input->post('taxaParcelamento8x'),
                'taxaParcelamento9x'    => $this->input->post('taxaParcelamento9x'),
                'taxaParcelamento10x'    => $this->input->post('taxaParcelamento10x'),
                'taxaParcelamento11x'    => $this->input->post('taxaParcelamento11x'),
                'taxaParcelamento12x'    => $this->input->post('taxaParcelamento12x'),
                'taxaParcelamentoVendedor1x'    => $this->input->post('taxaParcelamentoVendedor1x'),
                'taxaParcelamentoVendedor2x'    => $this->input->post('taxaParcelamentoVendedor2x'),
                'taxaParcelamentoVendedor3x'    => $this->input->post('taxaParcelamentoVendedor3x'),
                'taxaParcelamentoVendedor4x'    => $this->input->post('taxaParcelamentoVendedor4x'),
                'taxaParcelamentoVendedor5x'    => $this->input->post('taxaParcelamentoVendedor5x'),
                'taxaParcelamentoVendedor6x'    => $this->input->post('taxaParcelamentoVendedor6x'),
                'taxaParcelamentoVendedor7x'    => $this->input->post('taxaParcelamentoVendedor7x'),
                'taxaParcelamentoVendedor8x'    => $this->input->post('taxaParcelamentoVendedor8x'),
                'taxaParcelamentoVendedor9x'    => $this->input->post('taxaParcelamentoVendedor9x'),
                'taxaParcelamentoVendedor10x'    => $this->input->post('taxaParcelamentoVendedor10x'),
                'taxaParcelamentoVendedor11x'    => $this->input->post('taxaParcelamentoVendedor11x'),
                'taxaParcelamentoVendedor12x'    => $this->input->post('taxaParcelamentoVendedor12x'),
            );
            if ($this->taxas_model->edit('taxas', $data, 'idTaxas', $this->uri->segment(3)) == TRUE) {
                $this->session->set_flashdata('success', 'Taxa atualizada com sucesso!!');
                redirect(base_url() . 'index.php/taxas/editar/'.$this->uri->segment(3));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->taxas_model->getById($this->uri->segment(3));
        $this->data['view'] = 'taxas/editarTaxas';
        $this->load->view('theme/topo', $this->data);
    }
}

