<?php

class tecnospeed extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('tecnospeed_model', '', TRUE);
        $this->load->model('empresa_model', '', TRUE);
    }

    function cadastrarEmpresa($id) {
        $empresa = $this->empresa_model->getById($id);
        echo $this->tecnospeed_model->cadastrarEmpresa($empresa, '9816');

    }

    function consultaHandlerEmpresa($id) {
        $empresa = $this->empresa_model->getById($id);
        echo $this->tecnospeed_model->consultaHandlerEmpresa($empresa, '9816');
    }

    function geraToken() {
        echo $this->tecnospeed_model->geraToken();
    }

}