<?php

class tipoavaria extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('tipoavaria_model', '', TRUE);
        $this->data['menuTipoavaria'] = 'Tipo de Avaria';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vTipoavaria')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar tipos de avaria.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/tipoavaria/gerenciar/';
        $config['total_rows'] = $this->tipoavaria_model->count('tipoavaria');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->tipoavaria_model->get('tipoavaria','idTipoavaria,nome,descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'tipoavaria/tipoavaria';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aTipoavaria')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar tipos de avaria.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('tipoavaria') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao')
            );

            if ($this->tipoavaria_model->add('tipoavaria', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Tipo de avaria adicionado com sucesso!');
                redirect(base_url() . 'index.php/tipoavaria/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'tipoavaria/adicionarTipoavaria';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eTipoavaria')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar tipos de avaria.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('tipoavaria') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->tipoavaria_model->edit('tipoavaria', $data, 'idTipoavaria', $this->input->post('idTipoavaria')) == TRUE) {
                $this->session->set_flashdata('success', 'Tipo avaria editado com sucesso!');
                redirect(base_url() . 'index.php/tipoavaria/editar/'.$this->input->post('idTipoavaria'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->tipoavaria_model->getById($this->uri->segment(3));
        $this->data['view'] = 'tipoavaria/editarTipoavaria';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dTipoavaria')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir tipos de avarias.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir tipo de avaria.');
            redirect(base_url().'index.php/tipoavaria/gerenciar/');
        }

        $this->tipoavaria_model->delete('tipoavaria','idTipoavaria',$id);

        $this->session->set_flashdata('success','Tipo de avaria excluido com sucesso!');
        redirect(base_url().'index.php/tipoavaria/gerenciar/');
    }
}

