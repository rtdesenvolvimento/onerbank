<?php

class tipoveiculo extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('tipoveiculo_model', '', TRUE);
        $this->data['menuTipoveiculo'] = 'Tipo de Veículo';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vTipoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar tipos de veículo.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/tipoveiculo/gerenciar/';
        $config['total_rows'] = $this->tipoveiculo_model->count('tipoveiculo');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->tipoveiculo_model->get('tipoveiculo','idTipoveiculo,nome,descricao,numero_eixo,anexo','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'tipoveiculo/tipoveiculo';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aTipoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar tipos de veículo.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('tipoveiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $arquivo = $this->do_upload();
            $file = $arquivo['file_name'];

            $data = array(
                'nome' => set_value('nome'),
                'numero_eixo' => set_value('numero_eixo'),
                'numero_unidade_tracao' => set_value('numero_unidade_tracao'),
                'numero_unidade_acoplada' => set_value('numero_unidade_acoplada'),
                'anexo' => $file,
                'descricao' => set_value('descricao')
            );

            if ($this->tipoveiculo_model->add('tipoveiculo', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Tipo de veículo adicionado com sucesso!');
                redirect(base_url() . 'index.php/tipoveiculo/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'tipoveiculo/adicionarTipoveiculo';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eTipoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar tipos de veículo.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('tipoveiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $arquivo = $this->do_upload();
            $file = $arquivo['file_name'];

            if ($file) {
                $data = array(
                    'nome' => $this->input->post('nome'),
                    'numero_eixo' => $this->input->post('numero_eixo'),
                    'numero_unidade_tracao' => $this->input->post('numero_unidade_tracao'),
                    'numero_unidade_acoplada' => $this->input->post('numero_unidade_acoplada'),
                    'anexo' => $file,
                    'descricao' => $this->input->post('descricao')
                );
            } else {
                $data = array(
                    'nome' => $this->input->post('nome'),
                    'numero_eixo' => $this->input->post('numero_eixo'),
                    'numero_unidade_tracao' => $this->input->post('numero_unidade_tracao'),
                    'numero_unidade_acoplada' => $this->input->post('numero_unidade_acoplada'),
                    'descricao' => $this->input->post('descricao')
                );
            }

            if ($this->tipoveiculo_model->edit('tipoveiculo', $data, 'idTipoveiculo', $this->input->post('idTipoveiculo')) == TRUE) {
                $this->session->set_flashdata('success', 'Tipo veículo editado com sucesso!');
                redirect(base_url() . 'index.php/tipoveiculo/editar/'.$this->input->post('idTipoveiculo'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->tipoveiculo_model->getById($this->uri->segment(3));
        $this->data['view'] = 'tipoveiculo/editarTipoveiculo';
        $this->load->view('tema/topo', $this->data);
    }

    public function iframePontosAvariaTipoveiculo($idTipoveiculo)
    {
        $this->data['result']   = $this->tipoveiculo_model->getById($idTipoveiculo);
        $this->data['view']     = 'tipoveiculo/iframePontosAvariaTipoveiculo';
        $this->load->view('tema/blank', $this->data);
    }


    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dTipoveiculo')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir tipos de veículos.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir tipo de veículo.');
            redirect(base_url().'index.php/tipoveiculo/gerenciar/');
        }

        $this->tipoveiculo_model->delete('tipoveiculo','idTipoveiculo',$id);

        $this->session->set_flashdata('success','Tipo de veículo excluido com sucesso!');
        redirect(base_url().'index.php/tipoveiculo/gerenciar/');
    }

    public function do_upload() {

        $config['upload_path'] = './assets/arquivos/tipoveiculo';
        $config['allowed_types'] = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
        $config['max_size']     = 0;
        $config['max_width']  = '3000';
        $config['max_height']  = '2000';
        $config['encrypt_name'] = true;

        if (!is_dir('./assets/arquivos/tipoveiculo')) {
            mkdir('./assets/arquivos/tipoveiculo', 0777, TRUE);

        }
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload())  {
        }  else  {
            return $this->upload->data();
        }
    }
}

