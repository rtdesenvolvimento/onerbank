<?php

class tipoveiculopneu extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('tipoveiculopneu_model', '', TRUE);
        $this->load->model('tipoveiculo_model', '', TRUE);

        $this->data['menuTipoveiculopneu'] = 'Tipo veículo pneu';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vTipoveiculopneu')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar tipos pneu do veículo.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/tipoveiculopneu/gerenciar/';
        $config['total_rows'] = $this->tipoveiculopneu_model->count('tipoveiculopneu');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->tipoveiculopneu_model->get('tipoveiculopneu','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'tipoveiculopneu/tipoveiculopneu';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aTipoveiculopneu')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar tipos de veiculo pneu.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('tipoveiculopneu') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'idTipoveiculo' => set_value('idTipoveiculo'),
                'posicao' => set_value('posicao')
            );

            if ($this->tipoveiculopneu_model->add('tipoveiculopneu', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Tipos de pneu do veículo adicionado com sucesso!');
                redirect(base_url() . 'index.php/tipoveiculopneu/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['tiposveiculo']  = $this->tipoveiculo_model->getAll();
        $this->data['view'] = 'tipoveiculopneu/adicionarTipoveiculopneu';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eTipoveiculopneu')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar tipos de veiculo pne.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('tipoveiculopneu') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'idTipoveiculo' => $this->input->post('idTipoveiculo'),
                'posicao' => $this->input->post('posicao')
            );

            if ($this->tipoveiculopneu_model->edit('tipoveiculopneu', $data, 'idTipoveiculopneu', $this->input->post('idTipoveiculopneu')) == TRUE) {
                $this->session->set_flashdata('success', 'Tipos de pneu do veículo editado com sucesso!');
                redirect(base_url() . 'index.php/tipoveiculopneu/editar/'.$this->input->post('idTipoveiculopneu'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['tiposveiculo']  = $this->tipoveiculo_model->getAll();
        $this->data['result'] = $this->tipoveiculopneu_model->getById($this->uri->segment(3));
        $this->data['view'] = 'tipoveiculopneu/editarTipoveiculopneu';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dTipoveiculopneu')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir tipos de veiculos pneus.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir tipos de veiculo pneu.');
            redirect(base_url().'index.php/tipoveiculopneu/gerenciar/');
        }

        $this->tipoveiculopneu_model->delete('tipoveiculopneu','idTipoveiculopneu',$id);

        $this->session->set_flashdata('success','Tipos de pneu do veículo excluido com sucesso!');
        redirect(base_url().'index.php/tipoveiculopneu/gerenciar/');
    }
}

