<?php

class transferencia extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('transferencia_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('filial_model', '', TRUE);
        $this->load->model('historico_estoque_model', '', TRUE);

        $this->data['menuTransferencia'] = 'Transferência de compra';
        $this->data['menuEstoque'] = 'Estoque';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar transferência.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/transferencia/gerenciar/';
        $config['total_rows'] = $this->transferencia_model->count('transferencia');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->transferencia_model->get('transferencia', '*', '', $config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'transferencia/transferencia';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionar()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'aTranferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para adicionar transferência.');
            // redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('transferencia') == false) {
            $this->data['custom_error'] = (validation_errors() ? true : false);
        } else {

            $dataTransferencia = $this->input->post('dataTransferencia');

            $data = array(
                'dataTransferencia' => $dataTransferencia,
                'filial_destino_id' => $this->input->post('filial_destino_id'),
                'usuarios_id' => $this->input->post('usuarios_id')
             );

            if (is_numeric($id = $this->transferencia_model->add('transferencia', $data, true))) {
                $this->session->set_flashdata('success', 'Transfereência iniciada com sucesso, adicione os produtos.');
                redirect('transferencia/editar/' . $id);
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['origem']       = $this->filial_model->getFilialCurrent();
        $this->data['filiais']      = $this->filial_model->getAllNotCurrent();
        $this->data['view']         = 'transferencia/adicionarTransferencia';
        $this->load->view('theme/topo', $this->data);
    }

    function editar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar transferência');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('transferencia') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $dataTransferencia             = $this->input->post('dataTransferencia');

            $data = array(
                'dataTransferencia'     => $dataTransferencia,
                'previsaoEntrega'       => $this->input->post('previsaoEntrega'),
                'usuarios_id'           => $this->input->post('usuarios_id'),
                'filial_destino_id'     => $this->input->post('filial_destino_id'),
            );


            if ($this->transferencia_model->edit('transferencia', $data, 'idTransferencia', $this->input->post('idTransferencia')) == TRUE) {
                $this->session->set_flashdata('success', 'Transferência editado com sucesso!');
                redirect(base_url() . 'index.php/transferencia/editar/' . $this->input->post('idTransferencia'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['origem']       = $this->filial_model->getFilialCurrent();
        $this->data['filiais']      = $this->filial_model->getAllNotCurrent();
        $this->data['usuarios']             = $this->usuarios_model->getAll();
        $this->data['produtosList']         = $this->produtos_model->getAll();
        $this->data['result']               = $this->transferencia_model->getById($this->uri->segment(3));
        $this->data['produtos']             = $this->transferencia_model->getProdutos($this->uri->segment(3));
        $this->data['view']                 = 'transferencia/editarTransferencia';
        $this->load->view('theme/topo', $this->data);

    }

    public function editarEnderecoEntrega() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar transferência.');
            // redirect(base_url());
        }

        $idTransferencia   = $this->input->post('idTransferencia');
        $cepEntrega = $this->input->post('cepEntrega');
        $ruaEntrega = $this->input->post('ruaEntrega');
        $numeroEntrega = $this->input->post('numeroEntrega');
        $complementoEntrega = $this->input->post('complementoEntrega');
        $bairroEntrega = $this->input->post('bairroEntrega');
        $cidadeEntrega = $this->input->post('cidadeEntrega');
        $estadoEntrega = $this->input->post('estadoEntrega');

        $data = array(
            'cepEntrega' => $cepEntrega,
            'ruaEntrega' => $ruaEntrega,
            'numeroEntrega' => $numeroEntrega,
            'complementoEntrega' => $complementoEntrega,
            'bairroEntrega' => $bairroEntrega,
            'cidadeEntrega' => $cidadeEntrega,
            'estadoEntrega' => $estadoEntrega,
        );

        if ($this->transferencia_model->edit('transferencia', $data, 'idTransferencia',$idTransferencia) == TRUE) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function editarObservacao() {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar transferência.');
            // redirect(base_url());
        }

        $idTransferencia   = $this->input->post('idTransferencia');
        $observacao = $this->input->post('observacao');

        $data = array(
            'observacao' => $observacao,
        );

        if ($this->transferencia_model->edit('transferencia', $data, 'idTransferencia',$idTransferencia) == TRUE) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function confirmarTransferencia() {
        $TransferenciaId   =  $this->uri->segment(3);
        $data = array(
            'status' => 'Confirmado',
        );
        $this->transferencia_model->edit('transferencia', $data, 'idTransferencia',$TransferenciaId);
        $this->session->set_flashdata('success', 'Transferência confirmada com sucesso!');

        $this->editar();
    }

    public function emTransito() {
        $TransferenciaId   =  $this->uri->segment(3);
        $data = array(
            'status' => 'Em Trânsito',
        );
        $this->transferencia_model->edit('transferencia', $data, 'idTransferencia',$TransferenciaId);
        $this->session->set_flashdata('success', 'Transferência em trânsito com sucesso!');

        $this->editar();
    }

    public function entregar() {

        $TransferenciaId    =  $this->uri->segment(3);
        $destino            =  $this->uri->segment(4);
        $itens              = $this->transferencia_model->getProdutos($TransferenciaId);

        foreach ($itens as $r) {

            $idItens    = $r->idItens;
            $quantidade = $r->quantidade;
            $produto    = $r->produtos_id;
            $filial     = $r->filial_id;
            $transferencia_id  = $r->transferencia_id;

            $sql = "UPDATE produto_filial set estoque = estoque - ? WHERE produto_id = ? and filial_id = ? ";
            $this->db->query($sql, array($quantidade, $produto, $filial));

            //historico_estoque saida
            $historico_estoque = array(
                'transferencia_id' => $transferencia_id,
                'origem' => 'Transferência',
                'tipo' => 'SAIDA',
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto,
                'item_transferencia_id' => $idItens,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->historico_estoque_model->addHistoricoEstoque('historico_estoque', $historico_estoque);

            $sql = "UPDATE produto_filial set estoque = estoque + ? WHERE produto_id = ? and filial_id = ? ";
            $this->db->query($sql, array($quantidade, $produto, $destino));
            //historico_estoque entrada
            $historico_estoque = array(
                'transferencia_id' => $transferencia_id,
                'origem' => 'Transferência',
                'tipo' => 'ENTRADA',
                'estoque' => $quantidade,
                'filial_id' => $destino,
                'produto_id' => $produto,
                'item_transferencia_id' => $idItens,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->historico_estoque_model->addHistoricoEstoque('historico_estoque', $historico_estoque);
        }

        $data = array(
            'status' => 'Entregue',
        );
        $this->transferencia_model->edit('transferencia', $data, 'idTransferencia',$TransferenciaId);
        $this->session->set_flashdata('success', 'Transferência entregue com sucesso!');

        $this->editar();
    }

    public function visualizar()
    {
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'vTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para visualizar transferência.');
            //redirect(base_url());
        }

        $this->data['custom_error'] = '';
        $this->load->model('mapos_model');
        $this->data['result'] = $this->transferencia_model->getById($this->uri->segment(3));
        $this->data['produtos'] = $this->transferencia_model->getProdutos($this->uri->segment(3));
        $this->data['emitente'] = $this->mapos_model->getEmitente();

        $this->data['view'] = 'transferencia/visualizarTransferencia';
        $this->load->view('theme/topo', $this->data);

    }

    function excluir()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'dTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para excluir transferencia');
            //redirect(base_url());
        }

        $id = $this->input->post('id');
        if ($id == null) {

            $this->session->set_flashdata('error', 'Erro ao tentar excluir transferencia.');
            redirect(base_url() . 'index.php/transferencia/gerenciar/');
        }

        $this->db->where('transferencia_id', $id);
        $this->db->delete('itensTransferencia');

        $this->db->where('idTransferencia', $id);
        $this->db->delete('transferencia');

        $this->session->set_flashdata('success', 'Transferência excluído com sucesso!');
        redirect(base_url() . 'index.php/transferencia/gerenciar/');

    }

    public function autoCompleteProduto()
    {

        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->transferencia_model->autoCompleteProduto($q);
        }

    }


    public function adicionarProduto()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar Transferência.');
            // redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idProduto', 'Produto', 'trim|required|xss_clean');
        $this->form_validation->set_rules('idTransferencia', 'Transferência', 'trim|required|xss_clean');

        if ($this->form_validation->run() == false) {
            echo json_encode(array('result' => false));
        } else {

            $custo = $this->input->post('custo');
            $quantidade = $this->input->post('quantidade');
            $produto = $this->input->post('idProduto');

            $subtotal = $custo * $quantidade;

            $data = array(
                'quantidade' => $quantidade,
                'custo' => $custo,
                'subTotal' => $subtotal,
                'produtos_id' => $produto,
                'transferencia_id' => $this->input->post('idTransferencia'),
            );

            if ($this->transferencia_model->add('itensTransferencia', $data) == true) {
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false));
            }
        }
    }

    function excluirProduto()
    {

        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'eTransferencia')) {
            $this->session->set_flashdata('error', 'Você não tem permissão para editar transferência');
            //redirect(base_url());
        }

        $ID = $this->input->post('idProduto');
        if ($this->transferencia_model->delete('itensTransferencia', 'idItens', $ID) == true) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
}

