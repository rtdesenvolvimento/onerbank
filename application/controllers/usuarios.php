<?php

class Usuarios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }
        if (!$this->permission->checkPermission($this->session->userdata('permissao'), 'cUsuario')) {
           // $this->session->set_flashdata('error', 'Você não tem permissão para configurar os usuários.');
           // redirect(base_url());
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('usuarios_model', '', TRUE);
        $this->load->model('setor_model', '', TRUE);
        $this->load->model('filial_model', '', TRUE);

        $this->load->model('permissoes_model');

        $this->data['menuUsuarios'] = 'Usuários';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index()
    {
        $this->gerenciar();
    }

    function gerenciar()
    {
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'index.php/usuarios/gerenciar/';
        $config['total_rows'] = $this->usuarios_model->count('usuarios');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->usuarios_model->get($config['per_page'], $this->uri->segment(3));

        $this->data['view'] = 'usuarios/usuarios';
        $this->load->view('theme/topo', $this->data);
    }

    function adicionar()
    {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('usuarios') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : false);

        } else {
            $setor_id     = $this->input->post('setor_id');

            $this->load->library('encrypt');
            if($setor_id) {
                $data = array(
                    'nome' => set_value('nome'),
                    'rg' => set_value('rg'),
                    'cpf' => set_value('cpf'),
                    'rua' => set_value('rua'),
                    'numero' => set_value('numero'),
                    'bairro' => set_value('bairro'),
                    'cidade' => set_value('cidade'),
                    'estado' => set_value('estado'),
                    'setor_id' => $this->input->post('setor_id'),
                    'comissao' => $this->input->post('comissao'),
                    'usuariologin' => $this->input->post('usuariologin'),
                    'email' => set_value('email'),
                    'senha' => $this->encrypt->sha1($this->input->post('senha')),
                    'telefone' => set_value('telefone'),
                    'celular' => set_value('celular'),
                    'situacao' => set_value('situacao'),
                    'permissoes_id' => $this->input->post('permissoes_id'),
                    'dataCadastro' => date('Y-m-d')
                );
            } else {
                $data = array(
                    'nome' => set_value('nome'),
                    'rg' => set_value('rg'),
                    'cpf' => set_value('cpf'),
                    'rua' => set_value('rua'),
                    'numero' => set_value('numero'),
                    'bairro' => set_value('bairro'),
                    'cidade' => set_value('cidade'),
                    'estado' => set_value('estado'),
                    'setor_id' => NULL,
                    'comissao' => $this->input->post('comissao'),
                    'usuariologin' => $this->input->post('usuariologin'),
                    'email' => set_value('email'),
                    'senha' => $this->encrypt->sha1($this->input->post('senha')),
                    'telefone' => set_value('telefone'),
                    'celular' => set_value('celular'),
                    'situacao' => set_value('situacao'),
                    'permissoes_id' => $this->input->post('permissoes_id'),
                    'dataCadastro' => date('Y-m-d')
                );
            }

            if ($this->usuarios_model->add('usuarios', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Usuário cadastrado com sucesso!');
                redirect(base_url() . 'index.php/usuarios/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';

            }
        }
        $this->data['permissoes'] = $this->permissoes_model->getActive('permissoes', 'permissoes.idPermissao,permissoes.nome');
        $this->data['setores'] = $this->setor_model->getSetorAll();

        $this->data['view'] = 'usuarios/adicionarUsuario';
        $this->load->view('theme/topo', $this->data);
    }

    function editar()
    {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|xss_clean');
        $this->form_validation->set_rules('usuariologin', 'Usuário', 'trim|required|xss_clean');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required|xss_clean');
        $this->form_validation->set_rules('situacao', 'Situação', 'trim|required|xss_clean');
        $this->form_validation->set_rules('permissoes_id', 'Permissão', 'trim|required|xss_clean');

        $this->form_validation->set_rules('rg', 'RG', 'trim|xss_clean');
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|xss_clean');
        $this->form_validation->set_rules('rua', 'Rua', 'trim|xss_clean');
        $this->form_validation->set_rules('numero', 'Número', 'trim|xss_clean');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|xss_clean');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|xss_clean');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');

        if ($this->form_validation->run() == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);

        } else {

            if ($this->input->post('idUsuarios') == 1 && $this->input->post('situacao') == 0) {
                $this->session->set_flashdata('error', 'O usuário super admin não pode ser desativado!');
                redirect(base_url() . 'index.php/usuarios/editar/' . $this->input->post('idUsuarios'));
            }

            $senha = $this->input->post('senha');
            if ($senha != null) {
                $this->load->library('encrypt');
                $senha = $this->encrypt->sha1($senha);
                $setor_id     = $this->input->post('setor_id');

                if ($setor_id) {
                    $data = array(
                        'nome' => $this->input->post('nome'),
                        'usuariologin' => $this->input->post('usuariologin'),
                        'rg' => $this->input->post('rg'),
                        'cpf' => $this->input->post('cpf'),
                        'rua' => $this->input->post('rua'),
                        'numero' => $this->input->post('numero'),
                        'bairro' => $this->input->post('bairro'),
                        'cidade' => $this->input->post('cidade'),
                        'setor_id' => $this->input->post('setor_id'),
                        'comissao' => $this->input->post('comissao'),
                        'estado' => $this->input->post('estado'),
                        'email' => $this->input->post('email'),
                        'senha' => $senha,
                        'telefone' => $this->input->post('telefone'),
                        'celular' => $this->input->post('celular'),
                        'situacao' => $this->input->post('situacao'),
                        'permissoes_id' => $this->input->post('permissoes_id')
                    );
                } else {
                    $data = array(
                        'nome' => $this->input->post('nome'),
                        'usuariologin' => $this->input->post('usuariologin'),
                        'rg' => $this->input->post('rg'),
                        'cpf' => $this->input->post('cpf'),
                        'rua' => $this->input->post('rua'),
                        'numero' => $this->input->post('numero'),
                        'bairro' => $this->input->post('bairro'),
                        'cidade' => $this->input->post('cidade'),
                        'setor_id' => NULL,
                        'comissao' => $this->input->post('comissao'),
                        'estado' => $this->input->post('estado'),
                        'email' => $this->input->post('email'),
                        'senha' => $senha,
                        'telefone' => $this->input->post('telefone'),
                        'celular' => $this->input->post('celular'),
                        'situacao' => $this->input->post('situacao'),
                        'permissoes_id' => $this->input->post('permissoes_id')
                    );
                }
            } else {

                $data = array(
                    'nome'      => $this->input->post('nome'),
                    'rg'        => $this->input->post('rg'),
                    'cpf'       => $this->input->post('cpf'),
                    'rua'       => $this->input->post('rua'),
                    'numero'    => $this->input->post('numero'),
                    'bairro'    => $this->input->post('bairro'),
                    'setor_id'  => $this->input->post('setor_id'),
                    'cidade'    => $this->input->post('cidade'),
                    'estado'    => $this->input->post('estado'),
                    'email'     => $this->input->post('email'),
                    'telefone'  => $this->input->post('telefone'),
                    'celular'   => $this->input->post('celular'),
                    'situacao'  => $this->input->post('situacao'),
                    'comissao' => $this->input->post('comissao'),
                    'permissoes_id' => $this->input->post('permissoes_id')
                );
            }

            if ($this->usuarios_model->edit('usuarios', $data, 'idUsuarios', $this->input->post('idUsuarios')) == TRUE) {

                $filiais = count($_POST['filiais']);
                for ($i = 0; $i < $filiais; $i++) {
                    if ($_POST['filiais'][$i]) {

                        $dados_produto_filial = array (
                            'filial' 		    => $_POST['filiais'][$i],
                            'usuario_id' 		=> $this->input->post('idUsuarios'),
                            'status'            => 1
                        );
                        $this->usuarios_model->add('usuarios_filial', $dados_produto_filial);
                    }
                }

                $this->session->set_flashdata('success', 'Usuário editado com sucesso!');
                redirect(base_url() . 'index.php/usuarios/editar/' . $this->input->post('idUsuarios'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro</p></div>';
            }
        }

        $this->data['filiais']      = $this->filial_model->getAll();
        $this->data['result']       = $this->usuarios_model->getById($this->uri->segment(3));
        $this->data['permissoes']   = $this->permissoes_model->getActive('permissoes', 'permissoes.idPermissao,permissoes.nome');
        $this->data['setores']      = $this->setor_model->getSetorAll();

        $this->data['view'] = 'usuarios/editarUsuario';
        $this->load->view('theme/topo', $this->data);
    }


    public function remover_usuario_filial(){
        $id	= $this->uri->segment(3);
        $this->usuarios_model->delete('usuarios_filial', 'idUsuariosFilial', $id);

        redirect('usuarios/editar/'.$this->uri->segment(4));
    }

    public function consultaUsuarioSetor()
    {
        $setor = $this->input->post('setor');
        echo json_encode($this->usuarios_model->getUsuariosSetor($setor));
    }

    function excluir()
    {
        $ID = $this->uri->segment(3);
        $this->usuarios_model->delete('usuarios', 'idUsuarios', $ID);
        redirect(base_url() . 'index.php/usuarios/gerenciar/');
    }
}

