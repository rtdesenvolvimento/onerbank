<?php

class Veiculos extends CI_Controller {
    
    function __construct() {
	
        parent::__construct();
		
		$this->load->helper(array('form','codegen_helper'));
		$this->load->model('veiculos_model','',TRUE);
        $this->load->model('clientes_model','',TRUE);
        $this->load->model('tipoveiculo_model', '', TRUE);
        $this->load->model('servicos_model', '', TRUE);
        $this->load->model('produtos_model', '', TRUE);

        $this->data['menuEstoque'] = 'Estoque';
        $this->data['menuVeiculo'] = 'Veículos';
    }
	
	function index(){
		$this->gerenciar();
	}

	function gerenciar(){
        
        $this->load->library('pagination');
            
        $config['base_url'] = base_url().'index.php/veiculos/gerenciar/';
        $config['total_rows'] = $this->veiculos_model->count('veiculos');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        	
        $this->pagination->initialize($config); 	
		$this->data['results'] = $this->veiculos_model->get('veiculos','*','',$config['per_page'],$this->uri->segment(3));
       
	    $this->data['view'] = 'veiculos/veiculos';
       	$this->load->view('theme/topo',$this->data);
    }
	
	function buscar(){
        
        $this->load->library('pagination');
        
        $config['base_url'] = base_url().'index.php/veiculos/gerenciar/';
        $config['total_rows'] = $this->veiculos_model->count('veiculos');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        	
        $this->pagination->initialize($config); 
		$parametro = $this->uri->segment(3);
		$parametro = str_replace("%20", "%" , $parametro);
		
		$where = '';
		if ($parametro) {
			$where = 'cliente_id in ( select idClientes from clientes 
										where 
											nomeCliente LIKE "%'.$parametro.'%" 
											OR telefone LIKE "%'.$parametro.'%" 
											OR celular LIKE "%'.$parametro.'%" 
											OR email LIKE "%'.$parametro.'%" 
									)';
			$where = $where.' OR placa LIKE "%'.$parametro.'%" ';
 		}

		$this->data['results'] = $this->veiculos_model->get('veiculos','*', $where,$config['per_page'],'');
       
	    $this->data['view'] = 'veiculos/veiculos';
       	$this->load->view('theme/topo',$this->data);
    }

    function editar()
    {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'ePatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para editar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('veiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'modelo'        => $this->input->post('modelo'),
                'marca'         => $this->input->post('marca'),
                'tipoVeiculo'   => $this->input->post('tipoVeiculo'),
                'cor'           => $this->input->post('cor'),
                'municipio'     => $this->input->post('municipio'),
                'uf'            => $this->input->post('uf'),
                'situacao'      => $this->input->post('situacao'),
                'chassi'        => $this->input->post('chassi'),
                'ano'           => $this->input->post('ano'),
                'gasolina'      => $this->input->post('gasolina'),
                'etanol'        => $this->input->post('etanol'),
                'diesel'        => $this->input->post('diesel'),
                'gnv'           => $this->input->post('gnv'),
                'observacao'    => $this->input->post('observacao'),
                'tipoveiculo_id'=> $this->input->post('tipoveiculo_id'),
            );

            for($i=0;$i<10;$i++) {

                $servico_id     = $this->input->post('servico_id')[$i];
                $tipoManutencao = $this->input->post('tipoManutencao')[$i];
                $periodo        = $this->input->post('periodo')[$i];
                $veiculo_id     = $this->input->post('veiculo_id');
                $peca_id        = $this->input->post('peca_id')[$i];
                $dataInicialPreventiva =  $this->input->post('dataInicialPreventiva')[$i];

                $indice = $i + 1;

                $manutencao = array(
                    'peca_id'   => $peca_id,
                    'servico_id' => $servico_id,
                    'tipoManutencao' => $tipoManutencao,
                    'periodo' => $periodo,
                    'dataInicialPreventiva' => $dataInicialPreventiva,
                    'veiculo_id' =>$veiculo_id,
                );
                $this->veiculos_model->editManutencao($manutencao,$indice, $veiculo_id);
            }

            if ($this->veiculos_model->edit('veiculos', $data, 'idVeiculo', $this->input->post('veiculo_id')) == TRUE) {
                $this->session->set_flashdata('success','Patrimonio editado com sucesso!');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['manutencoes']      = $this->veiculos_model->getManutencoes($this->uri->segment(4));
        $this->data['tiposveiculo']     = $this->tipoveiculo_model->getAll();
        $this->data['veiculos']         = $this->clientes_model->getVeiculos($this->uri->segment(3));
        $this->data['result']           = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['veiculo']          = $this->veiculos_model->getById($this->uri->segment(4));
        $this->data['servicos']         = $this->servicos_model->getAll();
        $this->data['pecas']            = $this->produtos_model->getAll();
        $this->data['view']             = 'veiculos/editarVeiculo';
        $this->load->view('theme/topo', $this->data);
    }

    public function visualizar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vCliente')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar clientes.');
           redirect(base_url());
        }
		
	    $this->data['custom_error'] = '';
        $this->data['veiculos']     = $this->veiculos_model->getVeiculos($this->uri->segment(3));
        $this->data['result'] 	    = $this->veiculos_model->getClienteById($this->uri->segment(3));
        $this->data['results'] 	    = $this->veiculos_model->getOsByVeiculos($this->uri->segment(4));
        $this->data['view'] 	    = 'veiculos/visualizar';
        $this->load->view('theme/topo', $this->data);
    }

    public function consulta_veiculo_sinesp($placa) {

        $ch = curl_init("http://resultatec-com-br.umbler.net/".$placa);
        $retorno = curl_exec($ch);
        $retorno = json_encode( $retorno );

        //require 'application/helpers/consulta_veiculo.php';
        //consulta_veiculo($placa);
    }

    public function getListaVeiculos() {
        $clientes_id                =  $this->input->post('clientes_id');
        $this->data['veiculos']     = $this->clientes_model->getVeiculos($clientes_id);
        $this->data['view']         = 'veiculos/cadVeiculoModalLista';
        $this->load->view('theme/blank', $this->data);
    }

    public function iFrameVeiculo() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'ePatrimonio')){
            $this->session->set_flashdata('error','Você não tem permissão para editar patrimonio.');
            redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('veiculo') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {

            $data = array(
                'modelo'        => $this->input->post('modelo'),
                'marca'         => $this->input->post('marca'),
                'tipoVeiculo'   => $this->input->post('tipoVeiculo'),
                'cor'           => $this->input->post('cor'),
                'municipio'     => $this->input->post('municipio'),
                'uf'            => $this->input->post('uf'),
                'situacao'      => $this->input->post('situacao'),
                'chassi'        => $this->input->post('chassi'),
                'ano'           => $this->input->post('ano'),
                'gasolina'      => $this->input->post('gasolina'),
                'etanol'        => $this->input->post('etanol'),
                'diesel'        => $this->input->post('diesel'),
                'gnv'           => $this->input->post('gnv'),
                'observacao'    => $this->input->post('observacao'),
                'tipoveiculo_id'=> $this->input->post('tipoveiculo_id'),
            );

            for($i=0;$i<10;$i++) {

                $servico_id     = $this->input->post('servico_id')[$i];
                $tipoManutencao = $this->input->post('tipoManutencao')[$i];
                $periodo        = $this->input->post('periodo')[$i];
                $veiculo_id     = $this->input->post('veiculo_id');
                $peca_id        = $this->input->post('peca_id')[$i];
                $dataInicialPreventiva =  $this->input->post('dataInicialPreventiva')[$i];

                $indice = $i + 1;

                $manutencao = array(
                    'peca_id'   => $peca_id,
                    'servico_id' => $servico_id,
                    'tipoManutencao' => $tipoManutencao,
                    'periodo' => $periodo,
                    'dataInicialPreventiva' => $dataInicialPreventiva,
                    'veiculo_id' =>$veiculo_id,
                );
                $this->veiculos_model->editManutencao($manutencao,$indice, $veiculo_id);
            }

            if ($this->veiculos_model->edit('veiculos', $data, 'idVeiculo', $this->input->post('veiculo_id')) == TRUE) {
                $this->session->set_flashdata('success','Patrimonio editado com sucesso!');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['manutencoes']      = $this->veiculos_model->getManutencoes($this->uri->segment(4));
        $this->data['tiposveiculo']     = $this->tipoveiculo_model->getAll();
        $this->data['veiculos']         = $this->clientes_model->getVeiculos($this->uri->segment(3));
        $this->data['result']           = $this->clientes_model->getById($this->uri->segment(3));
        $this->data['veiculo']          = $this->veiculos_model->getById($this->uri->segment(4));
        $this->data['servicos']         = $this->servicos_model->getAll();
        $this->data['pecas']            = $this->produtos_model->getAll();
        $this->data['view']             = 'veiculos/editarVeiculoManutencao';
        $this->load->view('theme/blank_script', $this->data);
    }
}

