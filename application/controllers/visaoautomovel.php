<?php

class visaoautomovel extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('visaoautomovel_model', '', TRUE);
        $this->data['menuVisaoautomovel'] = 'Visão do automovel';
        $this->data['menuConfiguracoes'] = 'Configurações';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vVisaoautomovel')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar visão do automovel.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/visaoautomovel/gerenciar/';
        $config['total_rows'] = $this->visaoautomovel_model->count('visaoautomovel');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->visaoautomovel_model->get('visaoautomovel','idVisaoautomovel,nome,descricao','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'visaoautomovel/visaoautomovel';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aVisaoautomovel')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar visão automovel.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('visaoautomovel') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => set_value('nome'),
                'descricao' => set_value('descricao')
            );

            if ($this->visaoautomovel_model->add('visaoautomovel', $data) == TRUE) {
                $this->session->set_flashdata('success', 'Visão automovel adicionado com sucesso!');
                redirect(base_url() . 'index.php/visaoautomovel/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }
        $this->data['view'] = 'visaoautomovel/adicionarVisaoautomovel';
        $this->load->view('tema/topo', $this->data);
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eVisaoautomovel')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar visão automovel.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('visaoautomovel') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'nome' => $this->input->post('nome'),
                'descricao' => $this->input->post('descricao')
            );

            if ($this->visaoautomovel_model->edit('visaoautomovel', $data, 'idVisaoautomovel', $this->input->post('idVisaoautomovel')) == TRUE) {
                $this->session->set_flashdata('success', 'Visão automovel editado com sucesso!');
                redirect(base_url() . 'index.php/visaoautomovel/editar/'.$this->input->post('idVisaoautomovel'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['result'] = $this->visaoautomovel_model->getById($this->uri->segment(3));
        $this->data['view'] = 'visaoautomovel/editarVisaoautomovel';
        $this->load->view('tema/topo', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dVisaoautomovel')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir visão automovel.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir visão do automovel.');
            redirect(base_url().'index.php/visaoautomovel/gerenciar/');
        }

        $this->visaoautomovel_model->delete('visaoautomovel','idVisaoautomovel',$id);

        $this->session->set_flashdata('success','Visão do automovel excluido com sucesso!');
        redirect(base_url().'index.php/visaoautomovel/gerenciar/');
    }
}

