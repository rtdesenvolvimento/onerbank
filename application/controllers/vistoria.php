<?php

class vistoria extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) {
            redirect('mapos/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('acessorio_model', '', TRUE);
        $this->load->model('vistoria_model', '', TRUE);
        $this->load->model('vistoriaacessorio_model', '', TRUE);
        $this->load->model('tipoveiculopneu_model', '', TRUE);
        $this->load->model('vistoriapneu_model', '', TRUE);
        $this->load->model('equipamentoveiculo_model', '', TRUE);
        $this->load->model('tipoavaria_model', '', TRUE);
        $this->load->model('tipoveiculo_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('usuarios_model', '', TRUE);

        $this->data['menuVistoria'] = 'Vistoria';
    }

    function index(){
        $this->gerenciar();
    }

    function gerenciar(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vVistoria')){
            //$this->session->set_flashdata('error','Você não tem permissão para visualizar vistorias.');
            //redirect(base_url());
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'index.php/vistoria/gerenciar/';
        $config['total_rows'] = $this->vistoria_model->count('vistoria');
        $config['per_page'] = 20;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['results'] = $this->vistoria_model->get('vistoria','*','',$config['per_page'],$this->uri->segment(3));

        $this->data['view'] = 'vistoria/vistoria';
        $this->load->view('tema/topo',$this->data);
    }

    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aVistoria')){
            //$this->session->set_flashdata('error','Você não tem permissão para adicionar vistorias.');
            //redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('vistoria') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'status'                => set_value('status'),
                'datavistoria'          => set_value('datavistoria'),
                'horavistoria'          => set_value('horavistoria'),
                'gnv'                   => set_value('gnv'),
                'tanque'                => set_value('tanque'),
                'kmentrada'             => set_value('kmentrada'),
                'kmsaida'               => set_value('kmsaida'),
                'observacao'            => set_value('observacao'),
                'clientes_id'           => set_value('clientes_id'),
                'usuarios_id'           => set_value('usuarios_id'),
                'veiculo_id'            => set_value('veiculo_id'),
                'pertencesPortaLuva'    => set_value('pertencesPortaLuva'),
                'datahoracadastro'      => date('Y-m-d h:i')
            );

            $idVistoria = $this->vistoria_model->add('vistoria', $data, true);
            $veiculo = $this->clientes_model->getVeiculoById($this->input->post('veiculo_id'));
            $tipoveiculo_id = $veiculo->tipoveiculo_id;

            if ($idVistoria != '') {
                $this->adicionarAcessorio($idVistoria);
                $this->adicionarPneus($idVistoria, $tipoveiculo_id);
                $this->adicionarAvaria($idVistoria,$tipoveiculo_id);
                $this->session->set_flashdata('success', 'Vistoria adicionada com sucesso!');
                redirect(base_url() . 'index.php/vistoria/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
            }
        }

        $this->data['clientes']     = $this->clientes_model->getAll();
        $this->data['usuarios']     = $this->usuarios_model->getAll();
        $this->data['tiposveiculo'] = $this->tipoveiculo_model->getAll();
        $this->data['acessorios1']  = $this->acessorio_model->getAllByLimit(1, 7);
        $this->data['acessorios2']  = $this->acessorio_model->getAllByLimit(8, 7);
        $this->data['acessorios3']  = $this->acessorio_model->getAllByLimit(15, 7);
        $this->data['acessorios4']  = $this->acessorio_model->getAllByLimit(26, 10);
        $this->data['view']         = 'vistoria/adicionarVistoria';
        $this->load->view('tema/topo', $this->data);
    }

    function adicionarAcessorio($idVistoria) {

        $this->vistoriaacessorio_model->delete('vistoriaacessorio','idVistoria',$idVistoria);
        $acessorios = $this->acessorio_model->getAll();

        foreach ($acessorios as $acessorio) {
            $acessorioPost = $this->input->post('acessorio_'.$acessorio->idAcessorio);
            if ($acessorioPost == 'on') {
                $data = array(
                    'idVistoria'  => $idVistoria,
                    'acesssorio_id'  => $acessorio->idAcessorio,
                );
                $this->vistoria_model->add('vistoriaacessorio', $data);
            }
         }
    }

    public function adicionarPneus($idVistoria, $tipoVeiculo_id) {

        $pneus          = $this->tipoveiculopneu_model->getAllTipoVeiculo($tipoVeiculo_id);

        foreach ($pneus as $pneu) {
            $estado     = $this->input->post('estado_'.$pneu->idTipoveiculopneu);
            $marca      = $this->input->post('marca_'.$pneu->idTipoveiculopneu);
            $posicao    = $this->input->post('posicao_'.$pneu->idTipoveiculopneu);
            $verificaVistoriapneu         = $this->vistoria_model->getVistoriapneuByIdTipoVeiculopenuVistoria($pneu->idTipoveiculopneu, $idVistoria);

            $data = array(
                'idVistoria'        => $idVistoria,
                'estado'            => $estado,
                'marca'             => $marca,
                'posicao'           => $posicao,
                'tipoVeiculo_id'    => $tipoVeiculo_id,
                'idTipoveiculopneu' => $pneu->idTipoveiculopneu
            );

            if ( count($verificaVistoriapneu) > 0) {
                 $this->vistoria_model->edit('vistoriapneu', $data, 'idVistoriapneu', $verificaVistoriapneu->idVistoriapneu);
            } else {
                $this->vistoria_model->add('vistoriapneu', $data);
            }
        }
    }

    function adicionarAvaria($idVistoria, $tipoVeiculo_id) {

        $visaotopo1 = $this->equipamentoveiculo_model->getAllByVisaveiculo(1, $tipoVeiculo_id);
        $visaotopo2 = $this->equipamentoveiculo_model->getAllByVisaveiculo(2, $tipoVeiculo_id);
        $visaotopo3 = $this->equipamentoveiculo_model->getAllByVisaveiculo(3, $tipoVeiculo_id);
        $visaotopo4 = $this->equipamentoveiculo_model->getAllByVisaveiculo(4, $tipoVeiculo_id);
        $visaotopo5 = $this->equipamentoveiculo_model->getAllByVisaveiculo(5, $tipoVeiculo_id);

        $this->adicionarAvariaVisao($visaotopo1,$idVistoria, $tipoVeiculo_id);
        $this->adicionarAvariaVisao($visaotopo2,$idVistoria, $tipoVeiculo_id);
        $this->adicionarAvariaVisao($visaotopo3,$idVistoria, $tipoVeiculo_id);
        $this->adicionarAvariaVisao($visaotopo4,$idVistoria, $tipoVeiculo_id);
        $this->adicionarAvariaVisao($visaotopo5,$idVistoria, $tipoVeiculo_id);

    }

    public function adicionarAvariaVisao($visaotopo,$idVistoria, $tipoVeiculo_id) {
        foreach ($visaotopo as $v1) {

            $equipamento    = $this->input->post('equipamento_'.$v1->idEquipamentoveiculo);
            $tipoAvaria     = $this->input->post('tipoavaria_id_'.$v1->idEquipamentoveiculo);

            $client_x     = $this->input->post('client_x'.$v1->idEquipamentoveiculo);
            $client_y     = $this->input->post('client_y'.$v1->idEquipamentoveiculo);

            if ($equipamento == 'on') {
                $data = array(
                    'idVistoria'            => $idVistoria,
                    'tipoavaria_id'         => $tipoAvaria,
                    'equipamentoveiculo_id' => $v1->idEquipamentoveiculo,
                    'visaoautomovel_id'     => $v1->idVisaoautomovel,
                    'client_x'              => $client_x,
                    'client_y'              => $client_y
                );

                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria,$v1->idVisaoautomovel,$v1->idEquipamentoveiculo);

                if (count($avariasVistoria) > 0) {
                    $this->vistoria_model->edit('vistoriaavaria', $data, 'idVistoriaavaria', $avariasVistoria->idVistoriaavaria);
                    $idVistoriaAvaria = $avariasVistoria->idVistoriaavaria;
                } else {
                    $idVistoriaAvaria = $this->vistoria_model->add('vistoriaavaria', $data, true);
                }

                $arquivo    = $this->do_upload($idVistoria, $v1->idEquipamentoveiculo);
                if ($arquivo) {

                    $file = $arquivo['file_name'];
                    $path = $arquivo['full_path'];
                    $url = base_url() . 'assets/arquivos/vistoria/' . $idVistoria . '/' . $file;
                    $tamanho = $arquivo['file_size'];
                    $tipo = $arquivo['file_ext'];
                    $orig_name = $arquivo['orig_name'];

                    $data = array(
                        'idVistoriaavaria'  => $idVistoriaAvaria,
                        'idVistoria'        => $idVistoria,
                        'documento'         => $file,
                        'descricao'         => $file,
                        'orig_name'         => $orig_name,
                        'file'              => $file,
                        'path'              => $path,
                        'url'               => $url,
                        'cadastro'          => date('Y-m-d'),
                        'tamanho'           => $tamanho,
                        'tipo'              => $tipo
                    );

                    $this->vistoria_model->add('documentoavaria', $data);
                }
            } else {
                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria,$v1->idVisaoautomovel,$v1->idEquipamentoveiculo);
                if (count($avariasVistoria) > 0) {
                    $this->vistoria_model->delete('vistoriaavaria','idVistoriaavaria',$avariasVistoria->idVistoriaavaria);
                }
            }
        }
    }

    public function converteros() {

        $idVistoria                 = $this->input->post('idVistoria');
        $vistoria                   = $this->vistoria_model->getById($idVistoria);
        $descricaoprodutoservico    = $this->input->post('descricaoprodutoservico');

        $data_os = array(
            'dataInicial'           => date('Y-m-d'),
            'clientes_id'           => $vistoria->clientes_id,
            'usuarios_id'           => $vistoria->usuarios_id,
            'kilometragementrada'   => $vistoria->kmentrada,
            'kilometragemsaida'     => $vistoria->kmsaida,
            'veiculo_id'            => $vistoria->veiculo_id,
            'descricaoProduto'      => $descricaoprodutoservico,
            'status'                => 'Orçamento',
            'faturado'              => 0
        );

        $idOs = $this->vistoria_model->add('os', $data_os, true);

        $data_vistoria = array(
            'os_id'           => $idOs,
        );
        $this->vistoria_model->edit('vistoria', $data_vistoria, 'idVistoria', $idVistoria);

        redirect('os/editar/' . $idOs);
    }

    public function getAnexosAvarias() {
        $idVistoria             = $this->input->post('idVistoria');
        $idVistoriaavaria       = $this->input->post('idVistoriaavaria');

        $this->data['anexos']   = $this->vistoria_model->getAnexosAvarias($idVistoria, $idVistoriaavaria);
        $this->data['view']             = 'vistoria/getAnexosAvarias';
        $this->load->view('tema/blank', $this->data);
    }

    public function do_upload($idVistoria,$idEquipamentoveiculo){

        $config['upload_path']      = './assets/arquivos/vistoria/'.$idVistoria;
        $config['allowed_types']    = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG';
        $config['max_size']         = 0;
        $config['max_width']        = '3000';
        $config['max_height']       = '2000';
        $config['encrypt_name']     = true;

        if (!is_dir('./assets/arquivos/vistoria/'.$idVistoria)) {
            mkdir('./assets/arquivos/vistoria/' . $idVistoria, 0777, TRUE);
        }

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file_'.$idEquipamentoveiculo))  {
           return FALSE;
        }
        else  {
            return $this->upload->data();
        }
    }

    function editar() {
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eVistoria')){
            //$this->session->set_flashdata('error','Você não tem permissão para editar vistorias.');
            //redirect(base_url());
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';

        if ($this->form_validation->run('vistoria') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $data = array(
                'status'                => $this->input->post('status'),
                'datavistoria'          => $this->input->post('datavistoria'),
                'horavistoria'          => $this->input->post('horavistoria'),
                'gnv'                   => $this->input->post('gnv'),
                'tanque'                => $this->input->post('tanque'),
                'kmentrada'             => $this->input->post('kmentrada'),
                'kmsaida'               =>  $this->input->post('kmsaida'),
                'observacao'            =>  $this->input->post('observacao'),
                'clientes_id'           =>  $this->input->post('clientes_id'),
                'usuarios_id'           =>  $this->input->post('usuarios_id'),
                'veiculo_id'            =>  $this->input->post('veiculo_id'),
                'pertencesPortaLuva'    =>  $this->input->post('pertencesPortaLuva')
            );

            if ($this->vistoria_model->edit('vistoria', $data, 'idVistoria', $this->input->post('idVistoria')) == TRUE) {

                $veiculo = $this->clientes_model->getVeiculoById($this->input->post('veiculo_id'));
                $tipoveiculo_id = $veiculo->tipoveiculo_id;

                $this->adicionarAcessorio($this->input->post('idVistoria'));
                $this->adicionarPneus($this->input->post('idVistoria'), $tipoveiculo_id);
                $this->adicionarAvaria($this->input->post('idVistoria'),$tipoveiculo_id);
                $this->session->set_flashdata('success', 'Vistoria editada com sucesso!');
                redirect(base_url() . 'index.php/vistoria/editar/'.$this->input->post('idVistoria'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um errro.</p></div>';
            }
        }

        $this->data['clientes']             = $this->clientes_model->getAll();
        $this->data['usuarios']             = $this->usuarios_model->getAll();

        $this->data['acessorios1']          = $this->acessorio_model->getAllByLimit(1, 7);
        $this->data['acessorios2']          = $this->acessorio_model->getAllByLimit(8, 7);
        $this->data['acessorios3']          = $this->acessorio_model->getAllByLimit(15, 7);
        $this->data['acessorios4']          = $this->acessorio_model->getAllByLimit(26, 10);

        $this->data['tipoavarias']          = $this->tipoavaria_model->getAll();
        $this->data['result']               = $this->vistoria_model->getById($this->uri->segment(3));
        $this->data['veiculos']             = $this->vistoria_model->getVeiculos($this->data['result']->clientes_id);
        $this->data['acessoriosvistoria']   = $this->vistoriaacessorio_model->getAcessoriosVistoria($this->uri->segment(3));
        $this->data['tiposveiculo']         = $this->tipoveiculo_model->getAll();

        $this->data['view'] = 'vistoria/editarVistoria';
        $this->load->view('tema/topo', $this->data);
    }

    function getTemplatePneu() {
        $tipoVeiculo = $this->input->post('idTipoVeiculo');
        $idVistoria  = $this->input->post('idVistoria');

        $this->data['vistoriapneus']    = $this->vistoria_model->getVistoriapneuByVistoria($idVistoria);
        $this->data['pneus']            = $this->tipoveiculopneu_model->getAllTipoVeiculo($tipoVeiculo);
        $this->data['pneusvistoria']    = $this->vistoriapneu_model->getAllByVistoria($idVistoria);
        $this->data['view']             = 'vistoria/getTemplatePneu';

        $this->load->view('tema/blank', $this->data);
    }

    function  getTemplateAvaria() {
        $tipoVeiculo = $this->input->post('idTipoVeiculo');
        $idVistoria  = $this->input->post('idVistoria');

        $this->data['tipoavarias']      = $this->tipoavaria_model->getAll();
        $this->data['visaotopo1']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(1, $tipoVeiculo);
        $this->data['visaotopo2']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(2, $tipoVeiculo);
        $this->data['visaotopo3']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(3, $tipoVeiculo);
        $this->data['visaotopo4']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(4, $tipoVeiculo);
        $this->data['visaotopo5']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(5, $tipoVeiculo);

        $this->data['idVistoria']      = $idVistoria;
        $this->data['tipoVeiculo']      = $tipoVeiculo;
        $this->data['view']             = 'vistoria/getTemplateAvaria';
        $this->load->view('tema/blank', $this->data);
    }

    public function iframePontosAvaria($tipoVeiculo,$idVistoria = NULL)
    {
        $this->data['tipoVeiculo']   = $this->tipoveiculo_model->getById($tipoVeiculo);

        $this->data['visaotopo1']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(1, $tipoVeiculo);
        $this->data['visaotopo2']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(2, $tipoVeiculo);
        $this->data['visaotopo3']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(3, $tipoVeiculo);
        $this->data['visaotopo4']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(4, $tipoVeiculo);
        $this->data['visaotopo5']       = $this->equipamentoveiculo_model->getAllByVisaveiculo(5, $tipoVeiculo);

        $this->data['avarias'] = $this->vistoria_model->getAllAvariasVistoria($idVistoria);

        $this->data['view'] = 'vistoria/iframePontosAvaria';
        $this->load->view('tema/blank', $this->data);
    }

    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dVistoria')){
            //$this->session->set_flashdata('error','Você não tem permissão para excluir vistorias.');
            //redirect(base_url());
        }

        $id =  $this->input->post('id');
        if ($id == null){
            $this->session->set_flashdata('error','Erro ao tentar excluir vistoria.');
            redirect(base_url().'index.php/vistoria/gerenciar/');
        }

        $this->vistoria_model->delete('vistoria','idVistoria',$id);
        $this->vistoriaacessorio_model->delete('vistoriaacessorio','idVistoria',$id);

        $this->session->set_flashdata('success','Vistoria excluida com sucesso!');
        redirect(base_url().'index.php/vistoria/gerenciar/');
    }
}

