<?php

class zoop extends CI_Controller {

    function __construct() {

        parent::__construct();

        if ((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))) redirect('mapos/login');

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('zoop_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
    }

    function index(){}

    function getMarketplaces() {
        $this->zoop_model->getMarketplaces();
    }

    function getPlans() {
        $this->zoop_model->getPlans();
    }

    function getSellers() {
        $this->zoop_model->getSellers();
    }

    function atualizarCadastroEstabelecimento() {
        echo $this->zoop_model->atualizarCadastroEstabelecimento();
    }

    function getBuyers() {
        $this->zoop_model->getBuyers();
    }

    function postCadTransactionsBoleto() {
        $this->zoop_model->postCadTransactionsBoleto();
    }

    function postCadBuyers() {
        $this->zoop_model->postCadBuyers();
    }

    function getBoletoById($boleto_id) {
        $this->zoop_model->getBoletoById($boleto_id);
    }

    function getTransactionsById($transaction_id) {
        $this->zoop_model->getTransactionsById($transaction_id);
    }

    function getBuyersById($buyer_id) {
        $this->zoop_model->getBuyersById($buyer_id);
    }

    function postCadSellers() {
        $this->zoop_model->postCadSellers();
    }

    function putGetCadBuyers($idClientes) {
        $cliente = $this->clientes_model->getById($idClientes);
        $this->zoop_model->putGetCadBuyers($cliente);
    }

    function getSellersById() {
        echo $this->zoop_model->getSellersById();
    }

    function gerarTokenCartao() {
        echo $this->zoop_model->gerarTokenCartao();
    }
}

