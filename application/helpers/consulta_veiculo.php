<?php

require 'vendor/autoload.php';

use Sinesp\Sinesp;

function consulta_veiculo($placa)
{
    $veiculo = new Sinesp;

    try {
        $veiculo->buscar($placa);
        if ($veiculo->existe()) {
            echo json_encode($veiculo->dados());
        }
    } catch (\Exception $e) {
        echo $e->getMessage();
    }
}