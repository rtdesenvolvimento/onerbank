<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * FusionInvoice
 * 
 * A free and open source web based invoicing system
 *
 * @package		FusionInvoice
 * @author		Jesse Terry
 * @copyright	Copyright (c) 2012 - 2013, Jesse Terry
 * @license		http://www.fusioninvoice.com/support/page/license-agreement
 * @link		http://www.fusioninvoice.com
 * 
 */

function pdf_create($html, $filename, $stream = TRUE, $orientation='A4')
{
 	define('_MPDF_PATH','application/lib/mpdf/');
	require('application/lib/mpdf/mpdf.php');
 
	$pdf_c = 'c';
	$ib_w_font = 'dejavusanscondensed';

	$mpdf=new mPDF($pdf_c,$orientation,'','',2,2,2,2,5,5);
    $mpdf->SetProtection(array('copy','print'), '', '123456');
    $mpdf->SetTitle('Orcamento');
	$mpdf->SetAuthor(NOME_SISTEMA);
 	$mpdf->watermark_font = $ib_w_font;
	$mpdf->watermarkTextAlpha = 0.1;
	$mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);

    if ($stream) {
        $mpdf->Output($filename . '.pdf', 'D');
    } else  {
        $mpdf->Output('assets/uploads/' . $filename . '.pdf', 'F');
        return 'assets/uploads/' . $filename . '.pdf';
    }
}

?>