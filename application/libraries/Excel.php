<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 *  ==============================================================================
 *  Author	: Mian Saleem
 *  Email	: saleem@tecdiary.com
 *  For		: PHPExcel
 *  Web		: https://github.com/PHPOffice/PHPExcels
 *  License	: LGPL (GNU LESSER GENERAL PUBLIC LICENSE)
 *		: https://github.com/PHPOffice/PHPExcel/blob/master/license.md
 *  ==============================================================================
 */
require_once APPPATH . "/third_party/PHPExcel/PHPExcel.php";

class Excel extends PHPExcel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function criarExcel($dados, $filename) {

        $this->setActiveSheetIndex(0);
        $this->getActiveSheet()->setTitle($filename);

        $alfabeto =  $this->getAlfabeto();

        $contador   = 1;
        $count      = 0;
        foreach($dados as $key => $val) {
            foreach($val as $key2 => $newVal) {
                $letra  = $alfabeto[$count];
                $celula = strtoupper($letra).$contador;
                $this->getActiveSheet()->SetCellValue($celula, $key2);
                $this->getActiveSheet()->getStyle($celula)->getFont()->setBold(true);
                $this->getActiveSheet()->getColumnDimension($letra)->setWidth(30);

                $this->getActiveSheet()
                    ->getStyle($celula)
                    ->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('FF969696');

                $this->getActiveSheet()->getStyle(
                    $celula
                )->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $count = $count + 1;
            }
            break;
        }

        $row        = 2;
        foreach($dados as $key => $val) {
            $contador   = 1;
            foreach($val as $key2 => $newVal) {
                $letra  = $alfabeto[$contador-1];
                $celula = strtoupper($letra). $row;

                $this->getActiveSheet()->SetCellValue($celula, $newVal);

                $this->getActiveSheet()->getStyle(
                    $celula
                )->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $contador = $contador   + 1;
            }
            $row++;
        }

        $this->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        if (ob_get_length()) ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');
        if (ob_get_length()) ob_end_clean();
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel5');
        $objWriter->save('php://output');
    }

    function getAlfabeto() {
        $alfabeto = array();
        $count = 0;
        for($letra = 97; $letra <= 122; $letra++) {
            $alfabeto[$count] = chr($letra);
            $count = $count + 1;
        }
        return $alfabeto;
    }
}
