<?php

class cest_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function getAll()
    {
        $this->db->from('tabelacest');
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->where('codigo_tbc', $id);
        $this->db->limit(1);
        return $this->db->get('tabelacest')->row();
    }

    function search($termo='',$perpage=0,$start=0){

        $this->db->from('tabelacest');
        $this->db->order_by('legal_tbc');
        $this->db->limit($perpage,$start);

        if($termo){
            $this->db->or_like('legal_tbc', $termo);
            $this->db->or_like('ncm_tbc', $termo);
            $this->db->or_like('descricao_tbc', $termo);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }
}