<?php

class cfop_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function getAll()
    {
        $this->db->from('cfop');
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->where('codigo_cfop', $id);
        $this->db->limit(1);
        return $this->db->get('cfop')->row();
    }

    function search($termo='',$perpage=0,$start=0){

        $this->db->from('cfop');
        $this->db->order_by('codigo_cfop');
        $this->db->limit($perpage,$start);

        if($termo){
            $this->db->or_like('codigo_cfop', $termo);
            $this->db->or_like('desc_cfop', $termo);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }
}