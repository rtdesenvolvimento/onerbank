<?php
class Clientes_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idClientes','desc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }


    function search($termo='',$perpage=0,$start=0){

        $this->db->select('*');
        $this->db->from('clientes');
        $this->db->join('veiculos', 'veiculos.cliente_id = clientes.idClientes', 'left');
        $this->db->order_by('nomeCliente');
        $this->db->group_by('clientes.idClientes');
        $this->db->limit($perpage,$start);

        if($termo){
            $this->db->or_like('nomeCliente', $termo);
            $this->db->or_like('nomeCliente', $termo);
            $this->db->or_like('telefone', $termo);
            $this->db->or_like('celular', $termo);
            $this->db->or_like('email', $termo);
            $this->db->or_like('documento', $termo);
            $this->db->or_like('modelo', $termo);
            $this->db->or_like('marca', $termo);
            $this->db->or_like('placa', $termo);
            $this->db->or_like('tipoVeiculo', $termo);
            $this->db->or_like('cor', $termo);
            $this->db->or_like('ano', $termo);
            $this->db->or_like('nomeFantasiaApelido', $termo);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function getById($id){
        $this->db->where('idClientes',$id);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }

    function getByDocumento($documento){
        $this->db->where('documento',$documento);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }

    function getVeiculoById($id){
        $this->db->where('idVeiculo',$id);
        $this->db->limit(1);
        return $this->db->get('veiculos')->row();
    }

	public function getVeiculos($id = null){
         $this->db->from('veiculos');
        $this->db->where('cliente_id',$id);
        return $this->db->get()->result();
    }

    public function suggestions($termo){
        $this->db->select('clientes.idClientes id, clientes.nomeCliente as text, clientes.inativo, clientes.observacaoInativo bloqueio');
        $this->db->from('clientes');
        $this->db->or_like('nomeCliente', $termo);
        $this->db->or_like('nomeFantasiaApelido', $termo);
        $this->db->or_like('documento', $termo);
        $this->db->or_like('contatoCelular', $termo);
        $this->db->or_like('celular', $termo);
        $this->db->or_like('email', $termo);
        $this->db->or_like('telefone', $termo);
        return $this->db->get()->result();
    }


    function getCliente($id){
        $this->db->select('clientes.idClientes id, clientes.nomeCliente as text');
        $this->db->where('idClientes',$id);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }

    public function getVendas($id = null){
         $this->db->from('vendas');
        $this->db->where('clientes_id',$id);
        return $this->db->get()->result();
    }

    public function getAll()
    {
        $this->db->order_by('nomeCliente','desc');
        $this->db->where('inativo',0);
        $this->db->from('clientes');
        return $this->db->get()->result();
    }

    function add($table,$data, $returnId = false){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }

    function count($table) {
        return $this->db->count_all($table);
    }
    
    public function getOsByCliente($id){
        $this->db->where('clientes_id',$id);
        $this->db->order_by('idOs','desc');
         return $this->db->get('os')->result();
    }

    public function anexarArquivoVeiculo($idVeiculo, $observacaoimg, $anexo, $url, $thumb, $path)
    {

        $this->db->set('anexo', $anexo);
        $this->db->set('url', $url);
        $this->db->set('thumb', $thumb);
        $this->db->set('path', $path);
        $this->db->set('veiculos_id', $idVeiculo);
        $this->db->set('observacaoimg', $observacaoimg);
        $this->db->set('data', date('Y-m-d'));
        $this->db->set('hora', date('h:i'));
        $this->db->set('filial_id', $this->session->userdata('filial_id'));

        return $this->db->insert('arquivos_veiculo');
    }

    public function getAnexosByVeiculo($idVeiculos)
    {
        $this->db->where('veiculos_id', $idVeiculos);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('arquivos_veiculo')->result();
    }

}