<?php

class coletorxml_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('manifestaid', 'desc');
        $this->db->limit($perpage, $start);
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getAllByGraficsManifestada()
    {
        $query = $this->db->query('SELECT sum(valor) as valor, data FROM manifesta WHERE manifestada = "S" GROUP BY MONTH(STR_TO_DATE(data, \'%d/%m/%Y\')) ');
        return $query->result();
    }

    function getAllByGraficsNaoManifestada()
    {
        $query = $this->db->query('SELECT sum(valor) as valor, data FROM manifesta WHERE manifestada = "N" GROUP BY MONTH(STR_TO_DATE(data, \'%d/%m/%Y\')) ');
        return $query->result();
    }

    function getProdutosNotaFiscalFornecedorAtualizados($chave)
    {
        $query = $this->db->query(' select * from nfe_fornecedor n,	produtonfe_fornecedor p where p.estoque_atualizado = "S" and n.nNF = p.NFe and n.chavenfe = "'.$chave.'"');
        return $query->result();
    }


    function getById($id)
    {
        $this->db->where('manifestaid', $id);
        $this->db->limit(1);
        return $this->db->get('manifesta')->row();
    }

    function getByChave($chave)
    {
        $this->db->where('chave', $chave);
        $this->db->limit(1);
        return $this->db->get('manifesta')->row();
    }

    function add($table, $data)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    public function getAll()
    {
        $this->db->from('manifesta');
        return $this->db->get()->result();
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }
}