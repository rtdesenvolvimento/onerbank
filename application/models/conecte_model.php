<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conecte_model extends CI_Model {

	public function getLastOs($cliente){
		
		$this->db->where('clientes_id',$cliente);
        $this->db->limit(5);
		return $this->db->get('os')->result();
	}	

	public function getLastCompras($cliente){
		
		$this->db->select('vendas.*,usuarios.nome');
		$this->db->from('vendas');
		$this->db->join('usuarios', 'usuarios.idUsuarios = vendas.usuarios_id');
		$this->db->where('clientes_id',$cliente);
        $this->db->limit(5);

		return $this->db->get()->result();
	}

	public function getCompras($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array',$cliente){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->join('usuarios', 'vendas.usuarios_id = usuarios.idUsuarios', 'left');
        $this->db->where('clientes_id', $cliente);
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    public function getOs($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array',$cliente){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->join('usuarios', 'os.usuarios_id = usuarios.idUsuarios', 'left');
        $this->db->where('clientes_id', $cliente);
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    public function count($table,$cliente ){
    	$this->db->where('clientes_id', $cliente);
        return $this->db->count_all($table);
	}

    public function getDados(){
        
        $this->db->where('idclientes',$this->session->userdata('id'));
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }


    function getOSById($id)
    {
        $this->db->select('os.*, clientes.*, clientes.email emailCliente, clientes.telefone telefone_cliente, clientes.celular celular_cliente, usuarios.telefone, usuarios.email,usuarios.nome, aparelho.nome aparelho');
        $this->db->from('os');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->where('os.idOs', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getEmitente()
    {
        $this->db->limit(1);
        return $this->db->get('emitente')->row();
    }


    function edit($table,$data,$fieldID,$ID) {
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        
        return FALSE;       
    }

    public function getNFCEAutorizadas($mes) {
	    $ano = date('Y');
        $this->db->select(' count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('status',100);
        $this->db->where('nfce.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfce.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->from('nfce');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFEAutorizadas($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('status',100);
        $this->db->where('nfe.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfe.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->from('nfe');
        $this->db->limit(1);
        return $this->db->get()->row();
    }


    public function getNFCECanceladas($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfce.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfce.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status',101);
        $this->db->from('nfce');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFECanceladas($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfe.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfe.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status',101);
        $this->db->from('nfe');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFCEInutilizadas($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfce.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfce.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status',999);
        $this->db->from('nfce');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFEInutilizadas($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfe.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfe.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status',999);
        $this->db->from('nfe');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFCEPendentesVazio($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfce.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfce.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status','');
        $this->db->from('nfce');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFEPendentesVazio($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd,COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfe.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfe.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status','');
        $this->db->from('nfe');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFCEPendentesNull($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfce.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfce.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status',null);
        $this->db->from('nfce');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFEPendentesNull($mes) {
        $ano = date('Y');
        $this->db->select('count(*) qtd, COALESCE(sum(vNF),0) as totalvNF', false);
        $this->db->where('nfe.dhEmi >= ',$ano.'-'.$mes.'-01');
        $this->db->where('nfe.dhEmi <= ',$ano.'-'.$mes.'-31');
        $this->db->where('status',null);
        $this->db->from('nfe');
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getNFCEGrafico() {
        $this->db->select(' COALESCE(sum(vNF),0) as totalvNF, dhEmi', false);
        $this->db->where('(nfce.status=100 OR nfce.status=101 OR nfce.status=999)');
        $this->db->where('YEAR(nfce.dhEmi)', date('Y'));
        $this->db->from('nfce');
        $this->db->group_by('MONTH(nfce.dhEmi)');
        return $this->db->get()->result();
    }

    public function getNFEGrafico() {
        $this->db->select(' COALESCE(sum(vNF),0) as totalvNF, dhEmi', false);
        $this->db->where('(nfe.status=100 OR nfe.status=101 OR nfe.status=999)');
        $this->db->where('YEAR(nfe.dhEmi)', date('Y'));
        $this->db->from('nfe');
        $this->db->group_by('MONTH(nfe.dhEmi)');
        return $this->db->get()->result();
    }

}

/* End of file conecte_model.php */
/* Location: ./application/models/conecte_model.php */