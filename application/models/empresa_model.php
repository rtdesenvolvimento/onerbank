<?php
class empresa_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idEmpresa','desc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getById($id){
        $this->db->where('idEmpresa',$id);
        $this->db->limit(1);
        return $this->db->get('empresa')->row();
    }

    function getByFilialId($id){
        $this->db->where('idFilial',$id);
        $this->db->limit(1);
        return $this->db->get('filial')->row();
    }

    function add($table, $data, $returnId = false)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }
        return FALSE;
    }

    function addSelectBD($table,$data,$bd, $returnId = false){
        $otherdb = $this->load->database($bd, TRUE);
        $otherdb->insert($table, $data);
        if ($otherdb->affected_rows() == '1')
        {
            if ($returnId == true) {
                return $otherdb->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function getConfiguracaoTecnoSpeedByFilialBD($filial_id,$bd) {
        $otherdb = $this->load->database($bd, TRUE);
        $otherdb->where('filial_id',$filial_id);
        $otherdb->limit(1);
        return $otherdb->get('configuracao_tecnospeed')->row();
    }

    public function getAll()
    {
        $this->db->from('empresa');
        return $this->db->get()->result();
    }


    public function getFiliaisEmpresa($idFilial)
    {
        $this->db->order_by('idEmpresa','asc');
        $this->db->where('idEmpresa',$idFilial);
        $this->db->from('filial');
        return $this->db->get()->result();
    }

    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    function editSelectBD($table,$data,$fieldID,$ID, $bd){

        $otherdb = $this->load->database($bd, TRUE);

        $otherdb->where($fieldID,$ID);
        $otherdb->update($table, $data);

        if ($otherdb->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        return FALSE;
    }

    function count($table) {
        return $this->db->count_all($table);
    }

    function getContadorById($id,$bd) {
        $otherdb = $this->load->database($bd, TRUE);
        $otherdb->where('idContador',$id);
        $otherdb->limit(1);
        return $otherdb->get('contador')->row();
    }

}