<?php

class estoque_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function entradaEstoque($quantidade, $produto_id, $origem, $movimentador_id, $item_movimentador_id, $filial) {
        $sql = "UPDATE produto_filial set estoque = estoque + ? WHERE produto_id = ? and filial_id = ? ";
        $this->db->query($sql, array($quantidade, $produto_id, $filial));
        $this->historicoEntrada('ENTRADA', $quantidade, $produto_id, $filial, $origem, $movimentador_id, $item_movimentador_id);
    }

    function saidaEstoque($quantidade, $produto_id, $origem, $movimentador_id, $item_movimentador_id, $filial) {
        $sql = "UPDATE produto_filial set estoque = estoque - ? WHERE produto_id = ? and filial_id = ? ";
        $this->db->query($sql, array($quantidade, $produto_id, $filial));
        $this->historicoEntrada('SAIDA', $quantidade, $produto_id, $filial, $origem, $movimentador_id, $item_movimentador_id);
    }

    function historicoEntrada($tipo, $quantidade, $produto_id, $filial, $origem, $movimentador_id, $item_movimentador_id) {

        if ($origem == 'Inventário') {
            $historico_estoque = array(
                'tipo'              => $tipo,
                'inventario_id'     => $movimentador_id,
                'origem'            => $origem,
                'estoque'           => $quantidade,
                'filial_id'         => $filial,
                'produto_id'        => $produto_id,
                'item_iventario_id' => $item_movimentador_id,
                'data'              => date('Y-m-d'),
                'hora'              => date('H:i'),
            );
            $this->add('historico_estoque', $historico_estoque);
        }

        if ($origem == 'Venda' || $origem == 'Venda PDV') {
            $historico_estoque = array(
                'tipo' => $tipo,
                'venda_id' => $movimentador_id,
                'origem' => $origem,
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto_id,
                'item_venda_id' => $item_movimentador_id,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->add('historico_estoque', $historico_estoque);
        }

        if ($origem == 'Pedido de compra' ||
            $origem == 'Pedido de compra (cancelamento)') {
            $historico_estoque = array(
                'tipo' => $tipo,
                'pedido_id' => $movimentador_id,
                'origem' => $origem,
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto_id,
                'item_pedido_id' => $item_movimentador_id,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->add('historico_estoque', $historico_estoque);
        }

        if ($origem === 'Ajuste Manual') {
            $historico_estoque = array(
                'tipo' => $tipo,
                'origem' => $origem,
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto_id,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->add('historico_estoque', $historico_estoque);
        }

        if ($origem === 'NF-e' ||
            $origem == 'NF-e (Cancelamento)' ||
            $origem == 'NFC-e (Cancelamento)') {
            $historico_estoque = array(
                'tipo' => $tipo,
                'nfe_id' => $movimentador_id,
                'origem' => $origem,
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto_id,
                'nfe_item_id' => $item_movimentador_id,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->add('historico_estoque', $historico_estoque);
        }

        if ($origem === 'NF-e Fornecedor') {
            $historico_estoque = array(
                'tipo' => $tipo,
                'nfe_id' => $movimentador_id,
                'origem' => $origem,
                'estoque' => $quantidade,
                'filial_id' => $filial,
                'produto_id' => $produto_id,
                'nfe_item_id' => $item_movimentador_id,
                'data' => date('Y-m-d'),
                'hora' => date('H:i'),
            );
            $this->add('historico_estoque', $historico_estoque);
        }
    }

    function alterarPrecoCompra($custo, $produto_id, $filial) {
        $sql = "UPDATE produto_filial set precoCompra = ? WHERE produto_id = ? and filial_id = ? ";
        $this->db->query($sql, array($custo, $produto_id, $filial));
    }

    function alterarPrecoVenda($venda, $produto_id, $filial) {
        $sql = "UPDATE produto_filial set precoVenda = ? WHERE produto_id = ? and filial_id = ? ";
        $this->db->query($sql, array($venda, $produto_id, $filial));
    }

    function add($table, $data, $returnId = false)
    {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }
}