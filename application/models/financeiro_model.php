<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financeiro_model extends CI_Model {
	
	function __construct() {
        parent::__construct();

        $this->load->model('forma_pagamento_model', TRUE);
        $this->load->model('parcela_model', '', TRUE);
        $this->load->model('fatura_model', '', TRUE);
        $this->load->model('clientes_model', '', TRUE);
        $this->load->model('reciboservice_model', '', TRUE);
        $this->load->model('Vendas_model', '', TRUE);
    }

    function getv1($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $this->db->select($fields.', clientes.nomeCliente, fornecedor.nomeFornecedor, tipo_cobranca.descricao tipocobranca, tipo_cobranca.tipo tipocobrancatipo');
        $this->db->from($table);
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('clientes', 'clientes.idClientes = lancamentos.clientes_id', 'left');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = lancamentos.fornecedor_id', 'left');
        $this->db->join('tipo_cobranca', 'lancamentos.tipo_cobranca_id = tipo_cobranca.idTipoCobranca', 'left');

        $this->db->order_by('lancamentos.data_vencimento', 'asc');

        if($where) $this->db->where($where);

        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();

        return $result;
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields.', 
            parcela.idParcela,
            lancamentos.taxa,
            parcela.*,
            link_pagamento.descricao as linkpagamento,
            receita.nome as receita,
            despesa.nome as despesa,
            clientes.documento as cnpjCliente,  
            clientes.nomeCliente,
            clientes.idClientes, 
            clientes.email as emailCliente,
            clientes.celular as celularCliente,
            fornecedor.documento as cnpjFornecedor,
            fornecedor.nomeFornecedor, 
            fornecedor.email as emailFornecedor,
            tipo_cobranca.descricao tipocobranca,
            tipo_cobranca.tipo tipocobrancatipo');

        $this->db->from($table);
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('link_pagamento', 'link_pagamento.idLinkPagamento = parcela.linkpagamento_id', 'left');
        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->join('clientes', 'clientes.idClientes = lancamentos.clientes_id', 'left');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = lancamentos.fornecedor_id', 'left');
        $this->db->join('receita', 'receita.idReceita = lancamentos.receita_id', 'left');
        $this->db->join('despesa', 'despesa.idDespesa = lancamentos.despesa_id', 'left');
        $this->db->join('tipo_cobranca', 'lancamentos.tipo_cobranca_id = tipo_cobranca.idTipoCobranca', 'left');

        if($where) $this->db->where($where);

        $this->db->order_by('parcela.dtVencimento, parcela.lancamentopai_id,  parcela.idParcela, parcela.numeroParcela', 'asc');
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getById($id){
        $this->db->where('idLancamentos',$id);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('lancamentos')->row();
    }

    function getCondicaoPagamentoById($id)
    {
        $this->db->where('idCondicaoPagamento', $id);
        $this->db->limit(1);
        return $this->db->get('condicao_pagamento')->row();
    }

    function getBoletoById($id){
        $this->db->where('idBoleto',$id);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('boleto')->row();
    }

    function getPagamentoById($id){
        $this->db->where('idPagamento',$id);
        $this->db->limit(1);
        return $this->db->get('pagamento')->row();
    }

    function getPagamentoByTransacao($transacaoId){
        $this->db->where('transaction_id',$transacaoId);
        $this->db->limit(1);
        $this->db->order_by('pagamento.idPagamento', 'desc');
        return $this->db->get('pagamento')->row();
    }

    function getPagamentoByFatura($fatura_id){
        $this->db->where('fatura_id',$fatura_id);
        $this->db->limit(1);
        return $this->db->get('pagamento')->row();
    }

    function getPagamentoByVenda($venda){
        $this->db->select('forma_pagamento.nome as formaPagamento, pagamento.* ');

        $this->db->join('forma_pagamento', 'forma_pagamento.idFormaPagamento = pagamento.formaPagamento_id', 'left');
        $this->db->join('fatura_parcela', 'fatura_parcela.fatura_id = pagamento.fatura_id', 'left');
        $this->db->join('parcela', 'parcela.idParcela = fatura_parcela.parcela_id', 'left');
        $this->db->join('lancamentos', 'parcela.lancamentos_id = lancamentos.idLancamentos', 'left');
        $this->db->join('vendas', 'vendas.idVendas = lancamentos.venda', 'left');
        $this->db->where('venda',$venda);

        return $this->db->get('pagamento')->result();
    }

    function getBoletoByParcela($parcela_id) {
	    $fatura = $this->getFaturaByParcela($parcela_id);
        $this->db->where('fatura_id', $fatura->fatura_id);
        $this->db->where('url_boleto is not null');
        $this->db->limit(1);
        return $this->db->get('boleto')->row();
    }

    function getFaturaById($id){
        $this->db->where('idFatura',$id);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('fatura')->row();
    }

    function getFaturaByParcela($idParcela){
        $this->db->where('parcela_id',$idParcela);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get('fatura_parcela')->row();
    }

    function getParcelaByFatura($idFatura){
        $this->db->where('fatura_id',$idFatura);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get('fatura_parcela')->row();
    }

    function getParcelasByLancamento($idLancamento){
        $this->db->where('lancamentos_id',$idLancamento);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('parcela.dtVencimento', 'asc');
        return $this->db->get('parcela')->result();
    }

    function getBoletoByIdTransacao($transaction_id){
        $this->db->where('transactions_integration_id',$transaction_id);
        $this->db->limit(1);
        return $this->db->get('boleto')->row();
    }

    function getBoletoByBarCode($barcode){
        $this->db->where('barcode',$barcode);
        $this->db->limit(1);
        return $this->db->get('boleto')->row();
    }

    function getFaturasParcelaByLancamento($idParcela){
        $this->db->where('parcela_id',$idParcela);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('fatura_parcela')->result();
    }

    function getParcelasByLancamentoId($lancamentos_id){
        $this->db->where('lancamentos_id',$lancamentos_id);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('parcela')->result();
    }

    function getParcelaById($id){
        $this->db->where('idParcela',$id);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('parcela')->row();
    }

    function add($table,$data, $valores=[], $vencimentos=[], $desconto=0, $acrescimo=0){

	    $data['filial_id'] = $this->session->userdata('filial_id');

	    $data['totalDesconto'] = $desconto;
	    $data['totalAcrescimo'] = $acrescimo;

        $this->db->insert($table, $data);

        if ($this->db->affected_rows() == '1') {

            $idLancamento = $this->db->insert_id($table);
            $this->gerarParcelamento($idLancamento, $valores, $vencimentos, $desconto, $acrescimo);

            return $idLancamento;
		}

		return FALSE;       
    }

    function gerarParcelamento($idConta, $valores, $vencimentos, $desconto=0, $acrescimo=0) {

        $conta              = $this->getById($idConta);
        $condicaoPagamento  = $this->getCondicaoPagamentoById($conta->condicao_pagamento_id);
        $totalParcelas      = $condicaoPagamento->parcelas;
        $descontoParcela    = $desconto/$totalParcelas;
        $acrescimoParcela   = $acrescimo/$totalParcelas;
        $dataVencimento     = null;

        for($i=0;$i<$totalParcelas;$i++) {

            $dataDigitada = $vencimentos[$i];
            $vencimento = $valores[$i];

            if ($dataDigitada == null) {
                if ($dataVencimento == null) $dataVencimento = date('Y-m-d');
                else $dataVencimento = date("Y-m-d", strtotime($dataVencimento. " +1 month"));
            } else {
                $dataVencimento = $dataDigitada;
            }

            $parcela_id = $this->getDadosParcela($conta, $i, $totalParcelas ,$vencimento, $dataVencimento, $descontoParcela, $acrescimoParcela);

            $this->faturar($parcela_id);
        }
    }

    function getDadosParcela($conta, $i, $totalParcelas, $vencimento, $dataVencimento, $desconto=0, $acrescimo=0) {

        $dataParcela = array(
            'numeroParcela'     => ($i + 1),
            'totalParcelas'     => $totalParcelas,
            'valorVencimento'   => $vencimento,
            'valorPagar'        => $vencimento,
            'lancamentos_id'    => $conta->idLancamentos,
            'lancamentopai_id'  => $conta->idLancamentos,
            'dtVencimento'      => $dataVencimento,
            'desconto'          => $desconto,
            'acrescimo'         => $acrescimo
        );
        return $this->addParcela($dataParcela);
    }

    function faturar($parcela_id) {

	    $parcela = $this->getParcelaById($parcela_id);
	    $conta   = $this->getById($parcela->lancamentos_id);

	    $tipo           = $conta->tipo;
        $totalCredito   = 0;
        $totalDebito    = 0;
        $valorFatura    = $parcela->valorVencimento;
        $clientes_id    = null;
        $fornecedor_id  = null;

	    if ($tipo == 'receita') {
	        $totalCredito = $parcela->valorVencimento;
	        $credito = 1;
            $clientes_id = $conta->clientes_id;
        } else {
	        $totalDebito = $parcela->valorVencimento;
	        $credito = 0;
            $clientes_id = $conta->clientes_id;
        }

        $dataFatura = array(
            'totalCredito'      => $totalCredito,
            'totalDebito'       => $totalDebito,
            'valorFatura'       => $valorFatura,
            'valorPagar'        => $valorFatura,
            'dtFaturamento'     => date('Y-m-d'),
            'dtVencimento'      => $parcela->dtVencimento,
            'credito'           => $credito,
            'clientes_id'       => $clientes_id
        );

	    $fatura_id = $this->addFatura($dataFatura);

        $dataFaturaParcela = array(
            'parcela_id'=> $parcela_id,
            'fatura_id' => $fatura_id
        );

        $this->addFaturaParcela($dataFaturaParcela);

        $editParcela = array('status' => 'FATURADA');

        $this->edit('parcela', $editParcela,'idParcela', $parcela_id);
    }

    function estornarParcela($idParcela) {
        $this->delete('boleto', 'parcela_id', $idParcela);
    }

    function pagarPorSelecao($parcelasId, $formaPagamentos, $dataPagamentos, $valorPagamentos, $totalCreditoParcelaSelecao,$totalDebitoParcelaSelecao, $totalPagamento, $observacao) {

        $percentualDePagamento = $this->calcularPercentualDePagamento($totalCreditoParcelaSelecao, $totalDebitoParcelaSelecao, $totalPagamento);
        $cliente = null;
        $tipo_cobranca_id = null;

        $recibo = new recibo_model();

        foreach ($parcelas = explode(',', $parcelasId) as $parcelaId) {

            $parcela = $this->getParcelaById($parcelaId);
            $fatura = $this->getFaturaByParcela($parcelaId);
            $lancamento = $this->getById($parcela->lancamentos_id);

            $cliente =  $this->clientes_model->getById($lancamento->clientes_id);
            $tipo_cobranca_id = $lancamento->tipo_cobranca_id;

            $valorPagamentosParcela = $this->calcularPagarParcelaPorSelecao($parcela, $valorPagamentos, $percentualDePagamento);

            $this->pagar($parcelaId, $formaPagamentos, $dataPagamentos, $valorPagamentosParcela, $observacao);

            $parcela = $this->getParcelaById($parcelaId);

            $this->edit('parcela',array('status'=> parcela_model::STATUS_QUITADA, 'valorPagar' => 0, 'valorVencimento'=> $parcela->valorPago ),'idParcela', $parcela->idParcela);
            $this->edit('fatura', array('status'=> fatura_model::STATUS_QUITADA,  'valorPagar' => 0, 'valorFatura'=> $parcela->valorPago ),'idFatura', $fatura->fatura_id);


            $this->adicionarParcelaRecibo($recibo, $lancamento, $parcela);

        }

        if ($this->isLancarContaReceberParcial($totalCreditoParcelaSelecao, $totalPagamento)) {
            $this->adicionarContaReceberPorSaldoParcialPagamentoSelecao($cliente->idClientes, $tipo_cobranca_id, $totalCreditoParcelaSelecao, $totalPagamento, $observacao);
        }

        if ($this->isLancarContaPagarParcial($totalDebitoParcelaSelecao, $totalPagamento)) {
            $this->adicionarContaPagarPorSaldoParcialPagamentoSelecao($cliente->idClientes, $tipo_cobranca_id, $totalDebitoParcelaSelecao, $totalPagamento, $observacao);
        }

        return $this->gerarRecibo($recibo, $totalPagamento, $this->isCredito($totalCreditoParcelaSelecao), $cliente->idClientes, true);
    }

    private function adicionarParcelaRecibo($recibo, $lancamento, $parcela) {

        $vendaId = '';
        $nNF = '';

        if ($lancamento->venda != '') {
            $venda = $this->Vendas_model->getById($lancamento->venda);
            $vendaId = $venda->idVendas;
            $nNF = $venda->nNF;
        }

        $reciboParcela = new reciboparcela_model();
        $reciboParcela->setParcelaId($parcela->idParcela);
        $reciboParcela->setNParcela($parcela->numeroParcela . '/' . $parcela->totalParcelas);
        $reciboParcela->setDataVencimento($parcela->dtVencimento);
        $reciboParcela->setRefNF($nNF);
        $reciboParcela->setRefVenda($vendaId);

        $recibo->adicionarParcela($reciboParcela);

    }

    private function gerarRecibo($recibo, $valor, $credito, $clienteId, $grupo, $formaPagamento='', $pagamentoId=null) {
        $cliente =  $this->clientes_model->getById($clienteId);

        $recibo->setNomePessoa($cliente->nomeCliente);
        $recibo->setCpfCnpjPessoa($cliente->documento);
        $recibo->setValor($valor);
        $recibo->setCredito($credito);
        $recibo->setGrupo($grupo);
        $recibo->setDataPagamento(date('Y-m-d'));
        $recibo->setPagamentoId($pagamentoId);
        $recibo->setFormaPagamento($formaPagamento);

        return $this->reciboservice_model->salvar($recibo);
    }

    private function isCredito($totalCreditoParcelaSelecao) {
        return $totalCreditoParcelaSelecao > 0;
    }

    private function isLancarContaPagarParcial($totalDebitoParcelaSelecao, $totalPagamento) {
        return $totalDebitoParcelaSelecao > 0 && $totalPagamento < $totalDebitoParcelaSelecao;
    }

    private function isLancarContaReceberParcial($totalCreditoParcelaSelecao, $totalPagamento) {
	    return $totalCreditoParcelaSelecao > 0 && $totalPagamento < $totalCreditoParcelaSelecao;
    }

    function adicionarContaReceberPorSaldoParcialPagamentoSelecao($cliente, $tipo_cobranca_id, $totalCreditoParcelaSelecao, $totalPagamento, $observacao) {
        $dtVencimento = date('Y-m-d');
        $valor = $totalCreditoParcelaSelecao - $totalPagamento;

        $data = array(
            'tipo' => 'receita',
            'receita_id' => 1,//TODO VIA CONFIGURACAO
            'condicao_pagamento_id' => 1,//TODO A VISTA POR CONFIGURACAO
            'descricao' => $observacao,
            'valor' =>  $valor,
            'clientes_id' => $cliente,
            'tipo_cobranca_id' => $tipo_cobranca_id,
            'data_vencimento' => $dtVencimento,
            'previsaoPagamento' => $dtVencimento
        );

        $this->add('lancamentos', $data, [$valor],  [$dtVencimento], 0);
    }

    function adicionarContaPagarPorSaldoParcialPagamentoSelecao($cliente, $tipo_cobranca_id, $totalDebitoParcelaSelecao, $totalPagamento, $observacao) {

	    $dtVencimento = date('Y-m-d');
        $valor = $totalDebitoParcelaSelecao - $totalPagamento;

        $data = array(
            'tipo' => 'despesa',
            'despesa_id' => 1,//TODO VIA CONFIGURACAO
            'condicao_pagamento_id' => 1,//TODO A VISTA POR CONFIGURACAO
            'descricao' => $observacao,
            'valor' =>  $valor ,
            'clientes_id' => $cliente,
            'tipo_cobranca_id' => $tipo_cobranca_id,
            'data_vencimento' => $dtVencimento,
            'previsaoPagamento' => $dtVencimento
        );

        $this->add('lancamentos', $data, [$valor], [$dtVencimento], 0);
    }

    function calcularPagarParcelaPorSelecao($parcela, $valorPagamentos, $percentualDePagamento) {
        $valorPagamentosParcela = [];

        for ($i=0;$i<count($valorPagamentos);$i++) {
            $valorPagamentosParcela[] = ($parcela->valorPagar/count($valorPagamentos)) * $percentualDePagamento;
        }

        return $valorPagamentosParcela;
    }

    function calcularPercentualDePagamento($totalCreditoParcelaSelecao,$totalDebitoParcelaSelecao, $totalPagamento) {
        $totalVencimento = $totalCreditoParcelaSelecao - $totalDebitoParcelaSelecao;

        if ($totalVencimento <= 0) $totalVencimento = $totalVencimento*-1;

        return ($totalPagamento * 100 / $totalVencimento) / 100;
    }

    function isPagamentoDeCredito($lancamento) {
	    return $lancamento->tipo == 'receita';
    }

    public function getFormaDePagamentoById($id)
    {
        $this->db->where('idFormaPagamento', $id);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get('forma_pagamento')->row();
    }

    function pagar($parcelaId, $formaPagamentos, $dataPagamentos, $valorPagamentos, $observacao, $transaction_id=null, $authorizer_id=null,$authorization_code=null, $authorization_nsu=null, $taxas=null, $linkpagamento=null) {

        $parcela = $this->getParcelaById($parcelaId);
        $faturaParcela = $this->getFaturaByParcela($parcelaId);
        $lancamento = $this->getById($parcela->lancamentos_id);

        $valorPagar = $parcela->valorPagar;
        $valorPago = $parcela->valorPago;
        $totalPagamentos = 0;
        $dataUltimoPagamento = date('Y-m-d');

	    for ($i=0; $i <count($formaPagamentos); $i++) {

	        $valorPagamento = $valorPagamentos[$i];
	        $valorPagamento = str_replace(',', '.', $valorPagamento);
	        $formaPagamento = $this->getFormaDePagamentoById($formaPagamentos[$i]);
            $dataUltimoPagamento = $dataPagamentos[$i];

            $dadosPagamento = array(
                'status'            => 'PAGO',
                'observacao'        => $observacao,
                'valor'             => $valorPagamento,
                'dtPagamento'       => $dataUltimoPagamento,
                'hrPagamento'       => date('H:i'),
                'credito'           => TRUE,
                'conciliado'        => FALSE,
                'fatura_id'         => $faturaParcela->fatura_id,
                'clientes_id'       => $lancamento->clientes_id,
                'fornecedor_id'     => $lancamento->fornecedor_id,
                'formaPagamento_id' => $formaPagamento->idFormaPagamento,
                'transaction_id'    => $transaction_id,
                'authorizer_id'     => $authorizer_id,
                'authorization_code'=> $authorization_code,
                'authorization_nsu' => $authorization_nsu,
                'dtConciliacao'     => $dataPagamentos[$i],
            );

            $totalPagamentos = $totalPagamentos + $valorPagamento;
            $pagamentoId = $this->addPagamento($dadosPagamento);

            $recibo = new recibo_model();

            $this->adicionarParcelaRecibo($recibo, $lancamento, $parcela);
            $this->gerarRecibo($recibo, $valorPagamento,  $this->isPagamentoDeCredito($lancamento) , $lancamento->clientes_id, false, $formaPagamento->nome, $pagamentoId);
        }

        $saldo = ($valorPagar - $totalPagamentos);
	    $status = 'FATURADA';

	    if ($saldo == 0) $status = 'PAGO';

        if ($linkpagamento != null) {
            $atualizarParcela = array (
                'dtUltimoPagamento' => $dataUltimoPagamento,
                'valorPago'         => $valorPago + $totalPagamentos,
                'valorPagar'        => $saldo,
                'status'            => $status,
                'taxas'             => $taxas + $parcela->taxas,
                'linkpagamento_id'  => $linkpagamento->idLinkPagamento,
            );
        } else {
            $atualizarParcela = array (
                'dtUltimoPagamento' => $dataUltimoPagamento,
                'valorPago'         => $valorPago + $totalPagamentos,
                'valorPagar'        => $saldo,
                'status'            => $status,
                'taxas'             => $taxas + $parcela->taxas
            );
        }

        $atualizarFatura = array (
            'dtUltimoPagamento' => $dataUltimoPagamento,
            'valorPago'         => $valorPago + $totalPagamentos,
            'valorPagar'        => $saldo,
            'status'            => $status
        );

        $this->edit('parcela',$atualizarParcela,'idParcela', $parcelaId);
        $this->edit('fatura',$atualizarFatura,'idFatura', $faturaParcela->fatura_id);

        if ($taxas != null) $this->adicionarNovoDescontoParcela($parcela, $lancamento, $taxas, $transaction_id);

        //atualizar saldo do lancamento
        //atualizar saldo do movimentador
        //gerar recibo de pagamento
    }

    function adicionarNovoDescontoParcela($parcela, $lancamento, $desconto, $transaction_id) {

        $dados = array (
            'data'          => date('Y-m-d'),
            'valor'         => $desconto,
            'parcela_id'    => $parcela->idParcela
        );
        $this->addDescontoParcela($dados);

        $lancamento_taxa = array(
            'condicao_pagamento_id' => 1,//TODO VIA CONFIGURCAO
            'despesa_id' => 2,//TODO VIA CONFIGURCAO
            'tipo_cobranca_id' => $lancamento->tipo_cobranca_id,

            'taxa' => 1,
            'parcelapai_id' => $parcela->idParcela,

            'valor' => $desconto,
            'tipo' => 'despesa',
            'custo' => 0,
            'descricao' => $lancamento->descricao,
            'clientes_id' => $lancamento->clientes_id,
            'baixado' =>  $lancamento->baixado,
            'forma_pgto' => $lancamento->forma_pgto,
            'data_vencimento' => $parcela->dtVencimento,
            'previsaoPagamento' => $lancamento->previsaoPagamento,
            'data_pagamento' => NULL,
        );

        $lancamentoId = $this->add('lancamentos',$lancamento_taxa,[$desconto], [$parcela->dtVencimento], 0);

        $parcelas = $this->getParcelasByLancamento($lancamentoId);

        $formaPagamentos = 5;//TODO VERIFICAR CARTAO DE CREDITO

        foreach ($parcelas as $par) {
            $this->edit('parcela',array ('lancamentopai_id' => $lancamento->idLancamentos),'idParcela', $par->idParcela);
            $this->pagar($par->idParcela, [$formaPagamentos], [$parcela->dtVencimento], [$desconto], '');
        }
    }

    function baixarBoleto($transaction_id, $valor, $dataPagamento, $fees=0) {

        $boleto = $this->getBoletoByIdTransacao($transaction_id);
        
        if ($boleto->transactions_status == 'pending') {

            $fatura = $this->getParcelaByFatura($boleto->fatura_id);
            $parcela = $this->getParcelaById($fatura->parcela_id);
            $lancamento = $this->getById($parcela->lancamentos_id);

            $data = array('status' => 'PAGO', 'dtUltimoPagamento' => $dataPagamento, 'valorPago' => $valor, 'valorPagar' => '0');

            $data_boleto = array(
                'transactions_status' => 'succeeded',
                'transactions_status_boleto' => 'paid'
            );

            $this->edit('parcela', $data, 'idParcela', $fatura->parcela_id);
            $this->edit('fatura', $data, 'idFatura', $fatura->fatura_id);
            $this->edit('boleto', $data_boleto, 'idBoleto', $boleto->idBoleto);

            $dadosPagamento = array(
                'status' => 'PAGO',
                'observacao' => '',
                'valor' => $valor,
                'dtPagamento' => $dataPagamento,
                'hrPagamento' => date('H:i'),
                'credito' => TRUE,
                'conciliado' => FALSE,
                'fatura_id' => $fatura->fatura_id,
                'clientes_id' => $lancamento->clientes_id,
                'fornecedor_id' => $lancamento->fornecedor_id,
                'formaPagamento_id' => 1,
                'transaction_id' => $transaction_id,
                'dtConciliacao' => $dataPagamento,
            );
            $this->addPagamento($dadosPagamento);

            if ($fees > 0) $this->adicionarNovoDescontoParcela($parcela, $lancamento, $fees, $transaction_id);
        }
    }

    function cancelarParcela($parcelaId) {
        $dados_parcela = array ('status' => 'cancelada');
        $this->edit('parcela',$dados_parcela,'idParcela', $parcelaId);
    }

    function addFatura($data) {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert('fatura', $data);
        if ($this->db->affected_rows() == '1') {
            return $idLancamento = $this->db->insert_id('fatura');
        }
        return FALSE;
    }

    function addDescontoParcela($data) {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert('desconto_parcela', $data);
        if ($this->db->affected_rows() == '1') {
            return $idLancamento = $this->db->insert_id('desconto_parcela');
        }
        return FALSE;
    }

    function addPagamento($data) {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert('pagamento', $data);

        if ($this->db->affected_rows() == '1') return $idPagamento = $this->db->insert_id('pagamento');
        return FALSE;
    }

    function buscarPagamentos($idParela) {
        $this->db->select('pagamento.idPagamento, forma_pagamento.nome as nome, pagamento.valor as valor, parcela.dtVencimento, pagamento.dtPagamento as dtUltimoPagamento, pagamento.transaction_id');
        $this->db->join('forma_pagamento', 'forma_pagamento.idFormaPagamento = pagamento.formaPagamento_id');
        $this->db->join('fatura', 'fatura.idFatura = pagamento.fatura_id');
        $this->db->join('fatura_parcela', 'fatura_parcela.fatura_id = fatura.idFatura');
        $this->db->join('parcela', 'parcela.idParcela = fatura_parcela.parcela_id');
        $this->db->where('fatura_parcela.parcela_id', $idParela);
        $this->db->where('pagamento.status', 'PAGO');
        return $this->db->get('pagamento')->result();
    }

    function addFaturaParcela($data) {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert('fatura_parcela', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function addParcela($data) {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert('parcela', $data);

        if ($this->db->affected_rows() == '1') return $idLancamento = $this->db->insert_id('fatura');

        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) return TRUE;
		return FALSE;
    }
    
    function delete($table,$fieldID,$ID) {

        $this->db->where($fieldID,$ID);
        $this->db->delete($table);

        if ($this->db->affected_rows() == '1') return TRUE;

		return FALSE;        
    }

    function count($table) {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    public function getAllBoletosAberto(){
        $this->db->where('transactions_status','pending');
        $this->db->from('boleto');
        return $this->db->get()->result();
    }

    function getEmitente()
    {
        $this->db->limit(1);
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id', 'left');
        return $this->db->get('emitente')->row();
    }
}

