<?php
class fornecedor_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idFornecedor','desc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getById($id){
        $this->db->where('idFornecedor',$id);
        $this->db->limit(1);
        return $this->db->get('fornecedor')->row();
    }

    function getByCNPJ($documento){
        $this->db->where('documento',$documento);
        $this->db->limit(1);
        return $this->db->get('fornecedor')->row();
    }

    function add($table,$data, $returnId = false){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function getAll()
    {
        $this->db->order_by('nomeFornecedor','desc');
        $this->db->from('fornecedor');
        return $this->db->get()->result();
    }

    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        return FALSE;
    }

    function count($table) {
        return $this->db->count_all($table);
    }

}