<?php

class historico_estoque_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function addHistoricoEstoque($table, $data, $returnId = false)
    {

        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

}