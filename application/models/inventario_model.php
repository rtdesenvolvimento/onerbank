<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class inventario_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields.', filial.nome as origem, inventario.status as status');
        $this->db->from($table);
        $this->db->limit($perpage, $start);
        $this->db->join('filial', 'filial.idFilial = inventario.filial_id','left');
        $this->db->where($table.'.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idInventario', 'desc');
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getContador($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields.', filial.nome as origem, inventario.status as status');
        $this->db->from($table);
        $this->db->limit($perpage, $start);
        $this->db->join('filial', 'filial.idFilial = inventario.filial_id','left');
        $this->db->where('liberarContador','S');
        $this->db->order_by('idInventario', 'desc');
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->select('inventario.*, usuarios.*, o.nome origem');
        $this->db->from('inventario');
        $this->db->join('usuarios', 'usuarios.idUsuarios = inventario.usuarios_id');
        $this->db->join('filial o', 'o.idFilial = inventario.filial_id','left');
        $this->db->where('inventario.idInventario', $id);
        $this->db->where('inventario.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getProdutos($id = null)
    {

        $this->db->select('itensInventario.*, produtos.*');
        $this->db->from('itensInventario');
        $this->db->join('produtos', 'produtos.idProdutos = itensInventario.produtos_id');
        $this->db->where('inventario_id', $id);
        $this->db->where('itensInventario.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('produtos.descricao', 'asc');
        return $this->db->get()->result();
    }


    function add($table, $data, $returnId = false)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    function getEmitente()
    {
        $this->db->limit(1);
        $this->db->where('emitente.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('configuracao_tecnospeed', 'configuracao_tecnospeed.idConfiguracaotecnospeed = emitente.configuracao_tecnospeed_id');
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id');
        return $this->db->get('emitente')->row();
    }

}
