<?php

class Mapos_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit($perpage, $start);
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->from('usuarios');
        $this->db->select('usuarios.*, permissoes.nome as permissao');
        $this->db->join('permissoes', 'permissoes.idPermissao = usuarios.permissoes_id', 'left');
        $this->db->where('idUsuarios', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function alterarSenha($senha, $oldSenha, $id)
    {

        $this->db->where('idUsuarios', $id);
        $this->db->limit(1);
        $usuario = $this->db->get('usuarios')->row();

        if ($usuario->senha != $oldSenha) {
            return false;
        } else {
            $this->db->set('senha', $senha);
            $this->db->where('idUsuarios', $id);
            return $this->db->update('usuarios');
        }
    }

    function pesquisar($termo)
    {
        $data = array();
        // buscando clientes
        $this->db->or_like('nomeCliente', $termo);
        $this->db->or_like('telefone', $termo);
        $this->db->or_like('celular', $termo);
        $this->db->or_like('email', $termo);
        $this->db->or_like('documento', $termo);
        $this->db->or_like('modelo', $termo);
        $this->db->or_like('marca', $termo);
        $this->db->or_like('placa', $termo);
        $this->db->or_like('tipoVeiculo', $termo);
        $this->db->or_like('cor', $termo);
        $this->db->or_like('ano', $termo);
        $this->db->join('veiculos', 'veiculos.cliente_id = clientes.idClientes', 'left');
        $this->db->group_by('clientes.idClientes');
        $this->db->limit(20);
        $data['clientes'] = $this->db->get('clientes')->result();

        // buscando os
        $this->db->select('os.*, clientes.*, usuarios.nome nomeResponsavel ');
        $this->db->or_like('idOs', $termo);
        $this->db->or_like('clientes.nomeCliente', $termo);
        $this->db->or_like('clientes.telefone', $termo);
        $this->db->or_like('clientes.celular', $termo);
        $this->db->or_like('clientes.email', $termo);
        $this->db->or_like('clientes.documento', $termo);
        $this->db->or_like('veiculos.modelo', $termo);
        $this->db->or_like('veiculos.marca', $termo);
        $this->db->or_like('veiculos.placa', $termo);
        $this->db->or_like('veiculos.tipoVeiculo', $termo);
        $this->db->or_like('veiculos.cor', $termo);
        $this->db->or_like('veiculos.ano', $termo);
        $this->db->or_like('status', $termo);
        $this->db->join('veiculos', 'veiculos.idVeiculo = os.veiculo_id', 'left');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id', 'left');
        $this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id','left');
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(20);
        $data['os'] = $this->db->get('os')->result();

        // buscando veistoria
        $this->db->select('vistoria.*, clientes.*, usuarios.nome nomeResponsavel ');
        $this->db->or_like('idVistoria', $termo);
        $this->db->or_like('clientes.nomeCliente', $termo);
        $this->db->or_like('clientes.telefone', $termo);
        $this->db->or_like('clientes.celular', $termo);
        $this->db->or_like('clientes.email', $termo);
        $this->db->or_like('clientes.documento', $termo);
        $this->db->or_like('veiculos.modelo', $termo);
        $this->db->or_like('veiculos.marca', $termo);
        $this->db->or_like('veiculos.placa', $termo);
        $this->db->or_like('veiculos.tipoVeiculo', $termo);
        $this->db->or_like('veiculos.cor', $termo);
        $this->db->or_like('veiculos.ano', $termo);
        $this->db->or_like('status', $termo);
        $this->db->join('veiculos', 'veiculos.idVeiculo = vistoria.veiculo_id', 'left');
        $this->db->join('clientes', 'clientes.idClientes = vistoria.clientes_id', 'left');
        $this->db->join('usuarios', 'usuarios.idUsuarios = vistoria.usuarios_id','left');
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(20);
        $data['vistoria'] = $this->db->get('vistoria')->result();

        //buscando veiculos
        $this->db->or_like('modelo', $termo);
        $this->db->or_like('marca', $termo);
        $this->db->or_like('placa', $termo);
        $this->db->or_like('tipoVeiculo', $termo);
        $this->db->or_like('cor', $termo);
        $this->db->or_like('ano', $termo);
        $this->db->or_like('nomeCliente', $termo);
        $this->db->or_like('documento', $termo);
        $this->db->or_like('telefone', $termo);
        $this->db->or_like('celular', $termo);
        $this->db->or_like('email', $termo);
        $this->db->join('clientes', 'clientes.idClientes = veiculos.cliente_id', 'left');
        $this->db->limit(20);
        $data['veiculos'] = $this->db->get('veiculos')->result();

        // buscando produtos
        $this->db->like('descricao', $termo);
        $this->db->limit(20);
        $data['produtos'] = $this->db->get('produtos')->result();

        //buscando serviços
        $this->db->like('nome', $termo);
        $this->db->limit(20);
        $data['servicos'] = $this->db->get('servicos')->result();
        return $data;
    }

    function add($table, $data)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }

    function getOsAbertas()
    {
        $this->db->select('os.*, veiculos.situacao, clientes.nomeCliente, clientes.inativo, clientes.observacaoInativo , clientes.telefone, clientes.celular, usuarios.nome nomeResponsavel');
        $this->db->from('os');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = os.usuarios_id','left');
        $this->db->join('veiculos','veiculos.idVeiculo = os.veiculo_id','left');
        $this->db->where('os.status', 'Aberto');
        $this->db->where('os.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(10);
        return $this->db->get()->result();
    }

    function getProdutosMinimo()
    {
        $sql = "SELECT  p.idProdutos, p.descricao, pf.precoVenda, pf.precoCompra, pf.estoque, pf.estoqueMinimo 
        FROM produtos p, produto_filial pf WHERE pf.produto_id = p.idProdutos and pf.estoque <= pf.estoqueMinimo and pf.filial_id=".$this->session->userdata('filial_id')." LIMIT 10";
        return $this->db->query($sql)->result();
    }

    function getOsEstatisticas()
    {
        $sql = "SELECT status, COUNT(status) as total FROM os WHERE os.filial_id = ".$this->session->userdata('filial_id')." GROUP BY status ORDER BY status";
        return $this->db->query($sql)->result();
    }

    function getVistoriaEstatisticas()
    {
        $sql = "SELECT status, COUNT(status) as total FROM vistoria WHERE vistoria.filial_id = ".$this->session->userdata('filial_id')." GROUP BY status ORDER BY status";
        return $this->db->query($sql)->result();
    }

    public function getEstatisticasFinanceiro()
    {
        $sql = "SELECT SUM(CASE WHEN baixado = 1 AND tipo = 'receita' THEN valor END) as total_receita, 
                       SUM(CASE WHEN baixado = 1 AND tipo = 'despesa' THEN valor END) as total_despesa,
                       SUM(CASE WHEN baixado = 0 AND tipo = 'receita' THEN valor END) as total_receita_pendente,
                       SUM(CASE WHEN baixado = 0 AND tipo = 'despesa' THEN valor END) as total_despesa_pendente FROM lancamentos
                 WHERE filial_id=".$this->session->userdata('filial_id');
        return $this->db->query($sql)->row();
    }

    function getEmitente()
    {
        $this->db->limit(1);
        $this->db->where('emitente.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id', 'left');
        return $this->db->get('emitente')->row();
    }

    public function addEmitente($nome, $cnpj, $ie, $logradouro, $numero, $bairro, $cidade, $uf, $telefone, $ramo_atividade,$email, $logo)
    {
        $this->db->set('nome', $nome);
        $this->db->set('cnpj', $cnpj);
        $this->db->set('ie', $ie);
        $this->db->set('rua', $logradouro);
        $this->db->set('numero', $numero);
        $this->db->set('bairro', $bairro);
        $this->db->set('cidade', $cidade);
        $this->db->set('uf', $uf);
        $this->db->set('telefone', $telefone);
        $this->db->set('email', $email);
        $this->db->set('ramo_atividade', $ramo_atividade);
        $this->db->set('filial_id', $this->session->userdata('filial_id'));

        return $this->db->insert('emitente');
    }

    public function editEmitente($id, $nome, $cnpj, $ie, $logradouro, $numero, $bairro, $cidade, $uf, $telefone, $ramo_atividade, $email, $cert, $certsenha)
    {
        $this->db->set('nome', $nome);
        $this->db->set('cnpj', $cnpj);
        $this->db->set('ie', $ie);
        $this->db->set('rua', $logradouro);
        $this->db->set('numero', $numero);
        $this->db->set('bairro', $bairro);
        $this->db->set('cidade', $cidade);
        $this->db->set('uf', $uf);
        $this->db->set('telefone', $telefone);
        $this->db->set('email', $email);
        $this->db->set('ramo_atividade', $ramo_atividade);
        $this->db->set('cert', $cert);
        $this->db->set('certsenha', $certsenha);

        $this->db->where('id', $id);
        return $this->db->update('emitente');
    }

    public function editLogo($id, $logo)
    {
        $this->db->set('url_logo', $logo);
        $this->db->where('id', $id);
        return $this->db->update('emitente');
    }

    public function criacaoPastasArquivos($cnpj) {

        $base_path = FCPATH.'emissores/v4/';

        //verifica se ha o diretorio do cert
        $dircert = $base_path.'nfephp-master/certs/'.$cnpj.'/';
        
        if (!file_exists ($dircert)) :
            mkdir($dircert, 0700);
        endif;

        //verifica se ha o diretorio do XML
        $dirxml =  $base_path.'nfephp-master/XML/'.$cnpj.'/';
        if (!file_exists ($dirxml)) :
            mkdir($dirxml , 0700);
            $origem     = $base_path.'nfephp-master/XML/NF-e.zip';
            $destino    = $base_path.'nfephp-master/XML/'.$cnpj.'/NF-e.zip';

            if (@copy($origem, $destino)) :
                $zip = new ZipArchive();
                $zip->open($destino);
                $zip->extractTo( $base_path.'nfephp-master/XML/'.$cnpj.'');
                $zip->close();
                unlink($destino);
            endif;
        endif;

        //verifica se ha o diretorio do XML cte
        $dirxml =  $base_path.'sped-cte/XML/'.$cnpj.'/';
        if (!file_exists ($dirxml)) :
            mkdir($dirxml , 0700);
            $origem =  $base_path.'sped-cte/XML/CTe.zip';
            $destino = $base_path.'sped-cte/XML/'.$cnpj.'/CTe.zip';

            if (@copy($origem, $destino)) :
                $zip = new ZipArchive();
                $zip->open($destino);
                $zip->extractTo( $base_path.'sped-cte/XML/'.$cnpj.'');
                $zip->close();
                unlink($destino);
            endif;
        endif;

        //verifica se ha o diretorio do XML mdfe
        $dirxml =  $base_path.'sped-mdfe/XML/'.$cnpj.'/';
        if (!file_exists($dirxml)) :
            mkdir($dirxml , 0700);
            $origem     =  $base_path.'sped-mdfe/XML/MDFe.zip';
            $destino    = $base_path.'sped-mdfe/XML/'.$cnpj.'/MDFe.zip';
            if (@copy($origem, $destino)) :
                $zip = new ZipArchive();
                $zip->open($destino);
                $zip->extractTo( $base_path.'sped-mdfe/XML/'.$cnpj.'');
                $zip->close();
                unlink($destino);
            endif;
        endif;

        $dircert = $base_path.'nfephp-master/config/'.$cnpj.'/';
        if (!file_exists($dircert)) :
            mkdir($dircert, 0700);
        endif;

        //include (base_url().'emissores/v4/nfephp-master/install/saveconfig.php');

    }

    function gravarCertificado($cnpj, $files) {

        $base_path = FCPATH.'emissores/v4/';

        $uploaddircert  = $base_path.'nfephp-master/certs/'.$cnpj.'/';
        $uploadfilecert = $uploaddircert . basename($files['cert']['name']);

        if (!file_exists ($uploaddircert)) :
            mkdir($uploaddircert, 0700);
        endif;

        if ($files['cert']) :
            if (move_uploaded_file($files['cert']['tmp_name'], $uploadfilecert)) {
                $cert = $files['cert']['name'];
                $_POST['cert'] = $files['cert']['name'];
            } else {
                unset($_POST['cert']);
                $cert = '';
            }
        endif;
        unset($files['cert']);

        if($cert == '') :
            unset($_POST['cert']);
        endif;

        return $cert;
    }

    function contasReceberHoje() {
        return $this->contasDeHoje('receita');
    }
    function contasPagarHoje() {
        return $this->contasDeHoje('despesa');
    }

    function contasDeHoje($tipo) {
        $this->db->select('sum(valorPagar) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo',$tipo);
        $this->db->where('parcela.dtVencimento',date('Y-m-d'));

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "PAGO")');

        $query = $this->db->get('parcela');
        return  $query->row();;
    }

    function contasReceberSemana() {
        return $this->contasSemana('receita');
    }

    function contasPagarSemana() {
        return $this->contasSemana('despesa');
    }

    function contasSemana($tipo) {
        $semana = $this->getThisWeek();

        $this->db->select('sum(valorPagar) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->join('clientes', 'clientes.idClientes = lancamentos.clientes_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo',$tipo);
        $this->db->where('parcela.dtVencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'"');

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "PAGO")');

        $query = $this->db->get('parcela');
        return  $query->row();
    }

    function contasReceberMes() {
        return $this->contaMes('receita');
    }

    function contasPagarMes() {
        return $this->contaMes('despesa');
    }

    function contaMes($tipo) {
        $this->db->select('sum(valorPagar) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo',$tipo);
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"');

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "PAGO")');

        $query = $this->db->get('parcela');
        return  $query->row();
    }

    function contasReceberAno($mes) {
        return $this->contaAno('receita',$mes)->vencimento;
    }

    function contasPagarAno($mes) {
        return $this->contaAno('despesa',$mes)->vencimento;
    }

    function contaAno($tipo,$mes) {
        $this->db->select('sum(valorPagar) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo',$tipo);
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%m") = "'.$mes.'"');
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%Y") = "'.date('Y').'"');

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "PAGO")');

        $query = $this->db->get('parcela');
        return  $query->row();
    }

    function contasReceberVencidasMes() {
        return $this->contasVencidas('receita');
    }

    function contasPagarVencidasMes() {
        return $this->contasVencidas('despesa');
    }

    function contasVencidas($tipo) {
        $this->db->select('sum(valorPagar) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo',$tipo);
        $this->db->where('parcela.dtVencimento < "'.date('Y-m-d').'"');
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"');

        $this->db->where('parcela.status','FATURADA');

        $query = $this->db->get('parcela');
        return  $query->row();
    }

    function contasReceberAnoGraficoAno() {
        $this->db->select('receita.nome as receita, sum(valorVencimento) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->join('receita', 'receita.idReceita = lancamentos.receita_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo','receita');
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%Y") = "'.date('Y').'"');

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "PAGO")');

        $this->db->group_by('receita.idReceita');

        $query = $this->db->get('parcela');
        return  $query->result();
    }

    function contasPagarAnoGraficoAno() {
        $this->db->select('despesa.nome as despesa, sum(valorVencimento) as vencimento');

        $this->db->join('lancamentos', 'lancamentos.idLancamentos = parcela.lancamentos_id', 'left');
        $this->db->join('despesa', 'despesa.idDespesa = lancamentos.despesa_id', 'left');
        $this->db->where('lancamentos.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('lancamentos.tipo','despesa');
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%Y") = "'.date('Y').'"');

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "PAGO")');

        $this->db->group_by('despesa.idDespesa');

        $query = $this->db->get('parcela');
        return  $query->result();
    }

    protected function getThisWeek(){
        return array(date("Y/m/d", strtotime("last sunday", strtotime("now"))),date("Y/m/d", strtotime("next saturday", strtotime("now"))));
    }

    function getBoletosGeradosNoMes() {
        $this->db->select('count(idBoleto) as qtdBoletos');

        $this->db->join('parcela', 'parcela.idParcela = boleto.parcela_id');

        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"');
        $this->db->where('transactions_integration_id is NOT NULL', NULL, FALSE);

        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "ABERTA")');

        $query = $this->db->get('boleto');
        return  $query->row()->qtdBoletos;
    }

    function getBoletosPagosNoMes() {
        $this->db->select('count(idBoleto) as qtdBoletos');

        $this->db->join('parcela', 'parcela.idParcela = boleto.parcela_id');

        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"');
        $this->db->where('transactions_integration_id is NOT NULL', NULL, FALSE);

        $this->db->where('(parcela.status="PAGO")');

        $query = $this->db->get('boleto');
        return  $query->row()->qtdBoletos;
    }

    function getBoletosVencidosNoMes() {
        $this->db->select('count(idBoleto) as qtdBoletos');

        $this->db->join('parcela', 'parcela.idParcela = boleto.parcela_id');

        $this->db->where('parcela.dtVencimento < "'.date('Y-m-d').'"');
        $this->db->where('DATE_FORMAT(parcela.dtVencimento,"%m") = "'.date('m').'"');
        $this->db->where('transactions_integration_id is NOT NULL', NULL, FALSE);
        
        $this->db->where('(parcela.status="FATURADA" OR parcela.status = "ABERTA")');

        $query = $this->db->get('boleto');
        return  $query->row()->qtdBoletos;
    }

}