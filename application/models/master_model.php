<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class master_model extends CI_Model {

    public function count($table,$cliente){
        $this->db->where('clientes_id', $cliente);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }
}

/* End of file conecte_model.php */
/* Location: ./application/models/conecte_model.php */