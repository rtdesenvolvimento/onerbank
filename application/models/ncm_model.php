<?php

class ncm_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function getAll()
    {
        $this->db->from('ncm');
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->where('id_ncm', $id);
        $this->db->limit(1);
        return $this->db->get('ncm')->row();
    }

    function search($termo='',$perpage=0,$start=0){

        $this->db->from('ncm');
        $this->db->order_by('nome_ncm');
        $this->db->limit($perpage,$start);

        if($termo){
            $this->db->or_like('cod_ncm', $termo);
            $this->db->or_like('nome_ncm', $termo);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }
}