<?php

class nfefornecedor_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $this->db->select($fields);
        $this->db->where($table.'.filial_id', $this->session->userdata('filial_id'));
        $this->db->from($table);
        $this->db->order_by('nNF', 'desc');
        $this->db->limit($perpage, $start);
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getNFEByOS($os_id) {
        $this->db->where('os_id', $os_id);
        return $this->db->get('nfe_fornecedor')->result();
    }

    public function getAll()
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->from('nfe_fornecedor');
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->where('nNF', $id);
        $this->db->limit(1);
        return $this->db->get('nfe_fornecedor')->row();
    }

    function getByChave($chave)
    {
        $this->db->where('chavenfe', $chave);
        $this->db->limit(1);
        return $this->db->get('nfe_fornecedor')->row();
    }

    function getProdutos($id) {
        $this->db->where('NFe', $id);
        return $this->db->get('produtonfe_fornecedor')->result();
    }

    function getProdutosById($prodnfeid) {
        $this->db->where('prodnfeid', $prodnfeid);
        $this->db->limit(1);
        return $this->db->get('produtonfe_fornecedor')->row();
    }

    function add($table, $data, $returnId=FALSE)
    {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }
        return FALSE;
    }

    function addNotFilial($table, $data, $returnId=FALSE)
    {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    function getEmitente()
    {
        $this->db->limit(1);
        $this->db->where('emitente.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('configuracao_tecnospeed', 'configuracao_tecnospeed.idConfiguracaotecnospeed = emitente.configuracao_tecnospeed_id','left');
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id','left');
        return $this->db->get('emitente')->row();
    }

}