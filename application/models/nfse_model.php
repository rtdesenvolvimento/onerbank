<?php

class nfse_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where($table.'.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idNFSE', 'desc');
        $this->db->limit($perpage, $start);
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }


    function getNFEByOS($os_id) {
        $this->db->where('os_id', $os_id);
        return $this->db->get('nfse')->result();
    }

    public function getAll()
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->from('nfse');
        return $this->db->get()->result();
    }

    public function getProximoNumeroRPS() {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->where('(situacao=2 or situacao = 4)');
        $this->db->select('max(numeroRps) numeroRps');
        $this->db->from('nfse');
        $this->db->limit(1);

        $proximo = $this->db->get()->row()->numeroRps;

        if ($proximo > 0) {
            return $proximo + 1;
        } else {
            $emitente = $this->getEmitente();
            return $emitente->numeroProximoNumeroRps;
        }
    }

    public function getProximoNumeroLote() {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->where('(situacao=2 or situacao = 4)');
        $this->db->select('max(numeroLote) numeroLote');
        $this->db->from('nfse');
        $this->db->limit(1);

        $proximo = $this->db->get()->row()->numeroLote;

        if ($proximo > 0) {
            return $proximo + 1;
        } else {
            $emitente = $this->getEmitente();
            return $emitente->numeroProximoNumeroLote;
        }
    }

    public function getProximoNumeroNFSE() {
        $this->db->select('max(numeroNFSe) numeroNFSe');
        $this->db->from('nfse');
        $this->db->limit(1);
        return $this->db->get()->row()->numeroNFSe + 1;
    }

    function getById($id)
    {
        $this->db->where('idNFSE', $id);
        $this->db->limit(1);
        return $this->db->get('nfse')->row();
    }

    function getByNumeroRps($numeroRps)
    {
        $this->db->where('numeroRps', $numeroRps);
        $this->db->limit(1);
        return $this->db->get('nfse')->row();
    }

    function add($table, $data, $returnId=FALSE)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }


    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    function getServicosISS()
    {
        $this->db->where('isISS', 1);
        return $this->db->get('servicos')->result();
    }

    function getEmitente()
    {
        $this->db->limit(1);
        $this->db->where('emitente.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('configuracao_tecnospeed', 'configuracao_tecnospeed.idConfiguracaotecnospeed = emitente.configuracao_tecnospeed_id','left');
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id','left');
        return $this->db->get('emitente')->row();
    }

}