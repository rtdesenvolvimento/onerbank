<?php

class Os_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $this->db->select($fields . ', veiculos.situacao, clientes.nomeCliente, clientes.inativo, clientes.observacaoInativo , clientes.telefone, clientes.celular, usuarios.nome nomeResponsavel, aparelho.nome aparelho');
        $this->db->from($table);
        $this->db->where($table.'filial_id', $this->session->userdata('filial_id'));
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = os.usuarios_id','left');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->join('veiculos','veiculos.idVeiculo = os.veiculo_id','left');
        $this->db->limit($perpage, $start);
        $this->db->order_by('idOs', 'desc');
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getOs($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

        $lista_clientes = array();

        if($where){
            if(array_key_exists('pesquisa',$where)){
                $this->db->select('idClientes');
                $this->db->like('nomeCliente',$where['pesquisa']);
                $this->db->limit(5);
                $clientes = $this->db->get('clientes')->result();

                foreach ($clientes as $c) {
                    array_push($lista_clientes,$c->idClientes);
                }
            }
        }

        $this->db->select($fields . ',veiculos.situacao,clientes.nomeCliente, clientes.telefone, clientes.celular, usuarios.nome nomeResponsavel, aparelho.nome aparelho');
        $this->db->from($table);
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios','usuarios.idUsuarios = os.usuarios_id','left');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->join('veiculos','veiculos.idVeiculo = os.veiculo_id','left');

        // condicional do id da os
        if(array_key_exists('id_os',$where)){
            $this->db->where('os.idOs',$where['id_os']);
        }

        // condicional de status
        if(array_key_exists('status',$where)){
            $this->db->where('status',$where['status']);
        }

        // condicional de clientes
        if(array_key_exists('pesquisa',$where)){
            if($lista_clientes != null){
                $this->db->where_in('os.clientes_id',$lista_clientes);
            }
        }

        // condicional data inicial
        if(array_key_exists('de',$where)){
            $this->db->where('dataInicial >=' ,$where['de']);
        }

        // condicional data final
        if(array_key_exists('ate',$where)){
            $this->db->where('dataFinal <=', $where['ate']);
        }

        if(array_key_exists('produzir',$where)){
            $this->db->where('os.produzir =', $where['produzir']);
        }

        if(array_key_exists('producaopausada',$where)){
            $this->db->where('os.producaopausada =', $where['producaopausada']);
        }

        if(array_key_exists('bloqueadasetor',$where)){
            $this->db->where('os.bloqueadasetor =', $where['bloqueadasetor']);
        }

        if(array_key_exists('setor',$where)){
            $this->db->where('os.setorAtual_id =', $where['setor']);
        }

        $this->db->where('os.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit($perpage,$start);
        $this->db->order_by('os.idOs','desc');
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }


    function getOsAllProdutos($where='',$perpage=0,$start=0,$one=false,$array='array'){

        $lista_clientes = array();

        if($where){
            if(array_key_exists('pesquisa',$where)){
                $this->db->select('idClientes');
                $this->db->like('nomeCliente',$where['pesquisa']);
                $this->db->limit(5);
                $clientes = $this->db->get('clientes')->result();

                foreach ($clientes as $c) {
                    array_push($lista_clientes,$c->idClientes);
                }
            }
        }

        $this->db->select(' os.*,
                            produtos_os.valorComissao,
                            produtos_os.data_produzido,
                            produtos_os.hora_produzido,
                            produtos_os.data_produzido_conclusao,
                            produtos_os.hora_produzido_conclusao,
                            produtos_os.subTotal as subTotalPeca,
                            clientes.nomeCliente, 
                            clientes.telefone,
                            clientes.celular,
                            usuarios.nome nomeResponsavel, 
                            aparelho.nome aparelho, 
                            produtos.descricao produto');
        $this->db->from('produtos_os');
        $this->db->join('os','produtos_os.os_id = os.idOs','left');
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->join('usuarios','usuarios.idUsuarios = produtos_os.responsavel_id','left');
        $this->db->join('produtos','produtos.idProdutos = produtos_os.produtos_id','left');

        // condicional do id da os
        if(array_key_exists('id_os',$where)){
            $this->db->where('os.idOs',$where['id_os']);
        }

        // condicional de status
        if(array_key_exists('status',$where)){
            $this->db->where('os.status',$where['status']);
        }

        // condicional de clientes
        if(array_key_exists('pesquisa',$where)){
            if($lista_clientes != null){
                $this->db->where_in('os.clientes_id',$lista_clientes);
            }
        }

        // condicional data inicial
        if(array_key_exists('de',$where)){
            $this->db->where('produtos_os.data_produzido >=' ,$where['de']);
        }

        // condicional data final
        if(array_key_exists('ate',$where)){
            $this->db->where('produtos_os.data_produzido <=', $where['ate']);
        }

        if(array_key_exists('produzir',$where)){
            $this->db->where('os.produzir =', $where['produzir']);
        }

        if(array_key_exists('producaopausada',$where)){
            $this->db->where('os.producaopausada =', $where['producaopausada']);
        }

        if(array_key_exists('bloqueadasetor',$where)){
            $this->db->where('os.bloqueadasetor =', $where['bloqueadasetor']);
        }

        if(array_key_exists('setor',$where)){
            $this->db->where('produtos_os.setor_id =', $where['setor']);
        }

        if(array_key_exists('funcionario',$where)){
            $this->db->where('produtos_os.responsavel_id =', $where['funcionario']);
        }

        $this->db->limit($perpage,$start);
        $this->db->order_by('os.idOs','desc');
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getOsAllServicos($where='',$perpage=0,$start=0,$one=false,$array='array'){

        $lista_clientes = array();

        if($where){
            if(array_key_exists('pesquisa',$where)){
                $this->db->select('idClientes');
                $this->db->like('nomeCliente',$where['pesquisa']);
                $this->db->limit(5);
                $clientes = $this->db->get('clientes')->result();

                foreach ($clientes as $c) {
                    array_push($lista_clientes,$c->idClientes);
                }
            }
        }

        $this->db->select(' os.*,
                            servicos_os.valorComissao,
                            servicos_os.data_produzido,
                            servicos_os.hora_produzido,
                            servicos_os.data_produzido_conclusao,
                            servicos_os.hora_produzido_conclusao,
                            servicos_os.subTotal as subTotalPeca,
                            clientes.nomeCliente, 
                            clientes.telefone,
                            clientes.celular,
                            usuarios.nome nomeResponsavel, 
                            aparelho.nome aparelho, 
                            servicos.nome produto');
        $this->db->from('servicos_os');
        $this->db->join('os','servicos_os.os_id = os.idOs','left');
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->join('usuarios','usuarios.idUsuarios = servicos_os.responsavel_id','left');
        $this->db->join('servicos','servicos.idServicos = servicos_os.servicos_id','left');

        // condicional do id da os
        if(array_key_exists('id_os',$where)){
            $this->db->where('os.idOs',$where['id_os']);
        }

        // condicional de status
        if(array_key_exists('status',$where)){
            $this->db->where('os.status',$where['status']);
        }

        // condicional de clientes
        if(array_key_exists('pesquisa',$where)){
            if($lista_clientes != null){
                $this->db->where_in('os.clientes_id',$lista_clientes);
            }
        }

        // condicional data inicial
        if(array_key_exists('de',$where)){
            $this->db->where('servicos_os.data_produzido >=' ,$where['de']);
        }

        // condicional data final
        if(array_key_exists('ate',$where)){
            $this->db->where('servicos_os.data_produzido <=', $where['ate']);
        }

        if(array_key_exists('produzir',$where)){
            $this->db->where('os.produzir =', $where['produzir']);
        }

        if(array_key_exists('producaopausada',$where)){
            $this->db->where('os.producaopausada =', $where['producaopausada']);
        }

        if(array_key_exists('bloqueadasetor',$where)){
            $this->db->where('os.bloqueadasetor =', $where['bloqueadasetor']);
        }

        if(array_key_exists('setor',$where)){
            $this->db->where('servicos_os.setor_id =', $where['setor']);
        }

        if(array_key_exists('funcionario',$where)){
            $this->db->where('servicos_os.responsavel_id =', $where['funcionario']);
        }

        $this->db->limit($perpage,$start);
        $this->db->order_by('os.idOs','desc');
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }


    function getById($id)
    {
        $this->db->select('os.*, veiculos.situacao, clientes.*, clientes.email emailCliente, clientes.telefone telefone_cliente, clientes.celular celular_cliente, usuarios.telefone, usuarios.email,usuarios.nome, aparelho.nome aparelho');
        $this->db->from('os');
        $this->db->join('clientes', 'clientes.idClientes = os.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = os.usuarios_id');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->join('veiculos','veiculos.idVeiculo = os.veiculo_id','left');
        $this->db->where('os.idOs', $id);
        $this->db->where('os.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getProdutosOSById($id){
        $this->db->where('idProdutos_os',$id);
        $this->db->limit(1);
        return $this->db->get('produtos_os')->row();
    }

    function getServicosOSById($id){
        $this->db->where('idServicos_os',$id);
        $this->db->limit(1);
        return $this->db->get('servicos_os')->row();
    }

    public function getProdutos($id = null,$usuario_id = null, $setor_id=null)
    {
        $this->db->select('produtos_os.*, produtos.*, produtos_os.setor_id,  produtos_os.produzido produzido_os');
        $this->db->from('produtos_os');
        $this->db->join('produtos', 'produtos.idProdutos = produtos_os.produtos_id');
        $this->db->where('os_id', $id);
        if ($usuario_id) {
            $this->db->where('responsavel_id', $usuario_id);
        }

        if ($setor_id) {
            $this->db->where('produtos_os.setor_id', $setor_id);
        }
        return $this->db->get()->result();
    }

    public function getProdutosGroupByProdutos($id = null)
    {
        $this->db->select('produtos_os.*, sum(produtos_os.quantidade) quantidade, sum(produtos_os.subTotal) subTotal, produtos.*, produtos_os.setor_id,  produtos_os.produzido produzido_os');
        $this->db->from('produtos_os');
        $this->db->join('produtos', 'produtos.idProdutos = produtos_os.produtos_id');
        $this->db->where('os_id', $id);
        $this->db->group_by('produtos.setor_id');
        return $this->db->get()->result();
    }

    public function getVeiculos($idCliente = null)
    {
        $this->db->from('veiculos');
        $this->db->where('cliente_id', $idCliente);
        return $this->db->get()->result();
    }

    public function getVeiculosOS($idVeiculo = null)
    {
        $this->db->from('veiculos');
        $this->db->where('idVeiculo', $idVeiculo);
        return $this->db->get()->result();
    }

    public function getServicos($id = null,$usuario_id = null, $setor_id=null)
    {
        $this->db->select('servicos_os.*, servicos.*, servicos_os.setor_id, servicos_os.produzido produzido_os');
        $this->db->from('servicos_os');
        $this->db->join('servicos', 'servicos.idServicos = servicos_os.servicos_id');
        $this->db->where('os_id', $id);

        if ($usuario_id) {
            $this->db->where('responsavel_id', $usuario_id);
        }

        if ($setor_id) {
            $this->db->where('servicos_os.setor_id', $setor_id);
        }
        return $this->db->get()->result();
    }

    public function getServicosGroupByServicos($id = null)
    {
        $this->db->select('servicos_os.*, sum(servicos_os.subTotal) subTotal, servicos.*, servicos_os.setor_id, servicos_os.produzido produzido_os');
        $this->db->from('servicos_os');
        $this->db->join('servicos', 'servicos.idServicos = servicos_os.servicos_id');
        $this->db->where('os_id', $id);
        $this->db->where('servicos_os.filial_id', $this->session->userdata('filial_id'));
        $this->db->group_by('servicos_os.setor_id');
        return $this->db->get()->result();
    }

    public function getAparelhos()
    {
        $this->db->from('aparelho');
        return $this->db->get()->result();
    }

    function add($table, $data, $returnId = false)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function addHistoricoEstoque($table, $data, $returnId = false)
    {

        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function edit_ordemproducao($data, $os_id, $setor_id)
    {
        $this->db->where('os_id', $os_id);
        $this->db->where('setor_id', $setor_id);
        $this->db->update('ordemproducao', $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    public function autoCompleteProduto($q)
    {
        $this->db->select('*');
        $this->db->limit(50);
        $this->db->like('descricao', $q);
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'label'         => $row['descricao'] . ' | Preço sugerido: R$ ' . $row['precoVenda'] . ' | Estoque: ' . $row['estoque'],
                    'estoque'       => $row['estoque'],
                    'id'            => $row['idProdutos'],
                    'preco'         => $row['precoVenda'],
                    'precoCompra'   => $row['precoCompra'],
                    'idSetorProduto' => $row['setor_id']
                );
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteProdutoFilterSelect($q) {
        $this->db->select("produtos.*, idProdutos id, concat(descricao, \" | Preço sugerido: R$  \", precoVenda , \" | Estoque \" ,  estoque ) as text", FALSE);
        $this->db->limit(50);
        $this->db->like('descricao', $q);
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'text'         => $row['descricao'] . ' | Preço sugerido: R$ ' . $row['precoVenda'] . ' | Estoque: ' . $row['estoque'],
                    'estoque'       => $row['estoque'],
                    'id'            => $row['idProdutos'],
                    'preco'         => $row['precoVenda'],
                    'precoCompra'   => $row['precoCompra'],
                    'idSetorProduto' => $row['setor_id']
                );
            }
            return $row_set;
         }
    }

    public function autoCompleteProdutoById($idProduto) {
        $this->db->select(" p.idProdutos idProdutos, p.descricao, produto_filial.estoque, produto_filial.precoVenda, produto_filial.precoCompra, ", FALSE);

        $this->db->join('produtos p', 'p.idProdutos = produto_filial.produto_id', 'left');
        $this->db->join('filial f', 'f.idFilial = produto_filial.filial_id', 'left');

        $this->db->limit(50);
        $this->db->like('idProdutos', $idProduto);
        $this->db->where('f.idFilial',  $this->session->userdata('filial_id') );

        $query = $this->db->get('produto_filial');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'text'         => $row['descricao'] . ' | Preço sugerido: R$ ' . $row['precoVenda'] . ' | Estoque: ' . $row['estoque'],
                    'estoque'       => $row['estoque'],
                    'id'            => $row['idProdutos'],
                    'preco'         => $row['precoVenda'],
                    'custo'         => $row['precoCompra'],
                    'precoCompra'   => $row['precoCompra']
                );
            }
            return $row_set;
        }
    }

    public function autoCompleteClienteById($cliente_id) {
        $this->db->select("idClientes id, concat(nomeCliente, \" - \", telefone) as text", FALSE);
        $this->db->where('idClientes', $cliente_id);
        $q = $this->db->get_where('clientes');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function autoCompleteClienteFilterSelect($q) {
        $this->db->select("idClientes id, concat(nomeCliente, \" - \", telefone, \" / \", celular) as text", FALSE);
        $this->db->or_like('nomeCliente', $q);
        $this->db->or_like('nomeFantasiaApelido', $q);
        $this->db->or_like('telefone', $q);
        $this->db->or_like('celular', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('modelo', $q);
        $this->db->or_like('marca', $q);
        $this->db->or_like('placa', $q);
        $this->db->or_like('tipoVeiculo', $q);
        $this->db->or_like('cor', $q);
        $this->db->or_like('ano', $q);
        $this->db->or_like('documento', $q);
        $this->db->join('veiculos', 'clientes.idClientes = veiculos.cliente_id', 'left');
        $this->db->distinct();
        $this->db->order_by('nomeCliente', 'asc');
        $q = $this->db->get_where('clientes');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function autoCompleteCliente($q)
    {

        $this->db->select('clientes.nomeCliente, clientes.telefone, clientes.idClientes');
        $this->db->limit(50);
        $this->db->or_like('nomeCliente', $q);
        $this->db->or_like('nomeFantasiaApelido', $q);
        $this->db->or_like('telefone', $q);
        $this->db->or_like('celular', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('modelo', $q);
        $this->db->or_like('marca', $q);
        $this->db->or_like('placa', $q);
        $this->db->or_like('tipoVeiculo', $q);
        $this->db->or_like('cor', $q);
        $this->db->or_like('ano', $q);
        $this->db->or_like('documento', $q);
        $this->db->join('veiculos', 'clientes.idClientes = veiculos.cliente_id', 'left');
        $this->db->distinct();
        $query = $this->db->get('clientes');

        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nomeCliente'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idClientes']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteUsuario($q)
    {
        $this->db->select('*');
        $this->db->limit(5);
        $this->db->like('nome', $q);
        $this->db->where('situacao', 1);
        $query = $this->db->get('usuarios');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nome'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idUsuarios']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteUsuarioFilterSelect($q) {
        $this->db->select("idUsuarios id, concat(nome, \" - \", telefone) as text", FALSE);
        $this->db->like('nome', $q);
        $this->db->where('situacao', 1);
        $q = $this->db->get_where('usuarios');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function autoCompleteUsuarioById($idUsuarios) {
        $this->db->select("idUsuarios id, concat(nome, \" - \", telefone) as text", FALSE);
        $this->db->like('idUsuarios', $idUsuarios);
        $q = $this->db->get_where('usuarios');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function autoCompleteServico($q)
    {

        $this->db->select('*');
        $this->db->limit(50);
        $this->db->like('nome', $q);
        $query = $this->db->get('servicos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'label'         => $row['nome'] . ' | Preço sugerido: R$ ' . $row['preco'],
                    'id'            => $row['idServicos'],
                    'preco'         => $row['preco'],
                    'idSetorServico' => $row['setor_id']
                );
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteServicoFilterSelect($q){

        $this->db->select('*');
        $this->db->limit(50);
        $this->db->like('nome', $q);
        $query = $this->db->get('servicos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'text'         => $row['nome'] . ' | Preço sugerido: R$ ' . $row['preco'],
                    'id'            => $row['idServicos'],
                    'preco'         => $row['preco'],
                    'idSetorServico' => $row['setor_id']
                );
            }

            return $row_set;
        }
    }

    public function autoCompleteServicoById($idServico) {
        $this->db->select('*');
        $this->db->limit(50);
        $this->db->like('idServicos', $idServico);
        $query = $this->db->get('servicos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'text'         => $row['nome'] . ' | Preço sugerido: R$ ' . $row['preco'],
                    'id'            => $row['idServicos'],
                    'preco'         => $row['preco'],
                    'idSetorServico' => $row['setor_id']
                );
            }
            return $row_set;
        }
    }

    public function anexar($os,$localfoto, $observacaoimg, $anexo, $url, $thumb, $path)
    {

        $this->db->set('anexo', $anexo);
        $this->db->set('url', $url);
        $this->db->set('thumb', $thumb);
        $this->db->set('path', $path);
        $this->db->set('os_id', $os);
        $this->db->set('observacaoimg', $observacaoimg);
        $this->db->set('localfoto', $localfoto);
        $this->db->set('data', date('Y-m-d'));
        $this->db->set('hora', date('h:i'));
        $this->db->set('filial_id', $this->session->userdata('filial_id'));

        return $this->db->insert('anexos');
    }

    public function anexarProduto($os,$produtos_id,$idProdutos_os,$localfoto, $observacaoimg, $anexo, $url, $thumb, $path)
    {

        $this->db->set('anexo', $anexo);
        $this->db->set('url', $url);
        $this->db->set('thumb', $thumb);
        $this->db->set('path', $path);
        $this->db->set('os_id', $os);
        $this->db->set('observacaoimg', $observacaoimg);
        $this->db->set('localfoto', $localfoto);
        $this->db->set('data', date('Y-m-d'));
        $this->db->set('hora', date('h:i'));
        $this->db->set('produtos_os_id', $idProdutos_os);
        $this->db->set('produtos_id', $produtos_id);
        $this->db->set('filial_id', $this->session->userdata('filial_id'));

        return $this->db->insert('anexos');
    }

    public function anexarServico($os,$servicos_id,$idServicos_os,$localfoto, $observacaoimg, $anexo, $url, $thumb, $path)
    {

        $this->db->set('anexo', $anexo);
        $this->db->set('url', $url);
        $this->db->set('thumb', $thumb);
        $this->db->set('path', $path);
        $this->db->set('os_id', $os);
        $this->db->set('observacaoimg', $observacaoimg);
        $this->db->set('localfoto', $localfoto);
        $this->db->set('data', date('Y-m-d'));
        $this->db->set('hora', date('h:i'));
        $this->db->set('servico_os_id', $idServicos_os);
        $this->db->set('servicos_id', $servicos_id);
        $this->db->set('filial_id', $this->session->userdata('filial_id'));

        return $this->db->insert('anexos');
    }

    public function getAnexosByProdutosOs($os, $idProdutoOs)
    {
        $this->db->where('produtos_os_id',$idProdutoOs);
        $this->db->where('os_id', $os);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('anexos')->result();
    }

    public function getAnexosByProdutos($os, $idProduto)
    {
        $this->db->where('produtos_id',$idProduto);
        $this->db->where('os_id', $os);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('anexos')->result();
    }

    public function getAnexosByServicoOs($os, $idServicoOs)
    {
        $this->db->where('servico_os_id',$idServicoOs);
        $this->db->where('os_id', $os);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('anexos')->result();
    }

    public function getAnexosByServico($os, $idServico)
    {
        $this->db->where('servicos_id',$idServico);
        $this->db->where('os_id', $os);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('anexos')->result();
    }

    public function getAnexosAvulsos($os)
    {
        $this->db->where('os_id', $os);
        $this->db->where('servicos_id is null');
        $this->db->where('produtos_id is null');
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('anexos')->result();
    }

    public function getAnexos($os)
    {
        $this->db->where('os_id', $os);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('anexos')->result();
    }



    function getVistoriaOS($idOS)
    {
        $this->db->where('os_id', $idOS);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('vistoria')->row();
    }
}