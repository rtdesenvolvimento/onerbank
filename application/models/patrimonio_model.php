<?php
class patrimonio_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idProdutos','desc');
        $this->db->limit($perpage,$start);

        if($where){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }


    function getProdutos($where='',$perpage=0,$start=0,$one=false,$array='array'){
        $this->db->select('p.*, s.nome setor, g.nome grupoproduto , cc.nome centrocusto');
        $this->db->from('produtos p');
        $this->db->join('setor s', 's.idSetor = p.setor_id', 'left');
        $this->db->join('grupoproduto g', 'g.idGrupoProduto = p.grupoProduto_id', 'left');
        $this->db->join('centro_custo cc', 'cc.idCentrocusto = p.centro_custo_id', 'left');
        $this->db->order_by('idProdutos','desc');
        $this->db->where('patrimonio', 1);
        $this->db->limit($perpage,$start);

        if($where){
            $this->db->where($where);
        }
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getById($id){
        $this->db->select('p.*, s.nome setor, g.nome grupoproduto ');
        $this->db->join('setor s', 's.idSetor = p.setor_id', 'left');
        $this->db->join('grupoproduto g', 'g.idGrupoProduto = p.grupoProduto_id', 'left');
        $this->db->where('p.idProdutos',$id);
        $this->db->limit(1);
        return $this->db->get('produtos p')->row();
    }

    function getAll() {
        $this->db->join('produto_filial', 'produto_filial.produto_id = produtos.idProdutos');
        $this->db->where('produto_filial.filial_id',$this->session->userdata('filial_id'));
        $this->db->group_by('produtos.idProdutos');
        return $this->db->get('produtos')->result();
    }

    function getAllManutencoes($id) {
        $this->db->where('produto_id', $id);
        return $this->db->get('manutencao_preventiva')->result();
    }

    function getProdutoFiliais($produto) {
        $this->db->select('produto_filial.*, f.nome as filial, p.descricao produto, p.unidade');
        $this->db->join('produtos p', 'p.idProdutos = produto_filial.produto_id', 'left');
        $this->db->join('filial f', 'f.idFilial = produto_filial.filial_id', 'left');
        $this->db->where('produto_id',$produto);
        return $this->db->get('produto_filial')->result();
    }

    function add($table,$data, $returnId = false){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function addProdutoFilial($table,$data, $returnId = false){
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    function editManutencao($data,$indice, $produto_id) {
        $this->db->where('indice',$indice);
        $this->db->where('produto_id',$produto_id);
        $this->db->update('manutencao_preventiva', $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        return FALSE;
    }

    function deletarProdutoFilial($idProduto, $idFilial) {
        $this->db->where('produto_id',$idProduto);
        $this->db->where('filial_id',$idFilial);
        $this->db->delete('produto_filial');
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        return FALSE;
    }

    function count($table){
        return $this->db->count_all($table);
    }

    function autoCompleteFornecedorFilterSelect($q) {
        $this->db->select("idFornecedor id, concat(nomeFornecedor, \" - \", telefone, \" / \", celular) as text", FALSE);
        $this->db->or_like('nomeFornecedor', $q);
        $this->db->or_like('nomeFantasiaApelido', $q);
        $this->db->or_like('telefone', $q);
        $this->db->or_like('celular', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('documento', $q);
        $this->db->distinct();
        $this->db->order_by('nomeFornecedor', 'asc');
        $q = $this->db->get_where('fornecedor');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function autoCompleteFornecedorById($cliente_id) {
        $this->db->select("idFornecedor id, concat(nomeFornecedor, \" - \", telefone) as text", FALSE);
        $this->db->where('idFornecedor', $cliente_id);
        $q = $this->db->get_where('fornecedor');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getFornecedoresProduto($idProduto = null){
        $this->db->select("fornecedorproduto.*, fornecedor.* , fornecedorproduto.observacao observacao");
        $this->db->where('idProduto',$idProduto);
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = fornecedorproduto.idFornecedor', 'left');
        $this->db->from('fornecedorproduto');
        return $this->db->get()->result();
    }

    public function autoCompleteProdutoByIdNotFilterFilial($idProduto) {
        $this->db->where('idProdutos', $idProduto);
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'text'  => $row['descricao'],
                    'id'    => $row['idProdutos']
                );
            }
            return $row_set;
        }
    }

    function search($termo='',$perpage=0,$start=0){

        $this->db->select('produtos.idProdutos, produtos.descricao, produtos.unidade, produtos.ncm, produtos.cEAN, produtos.cProd, produtos.CST, produtos.CEST, produtos.CFOP, produto_filial.precoVenda, produto_filial.estoque, produto_filial.precoCompra ');
        $this->db->from('produtos');
        $this->db->where('produto_filial.filial_id',$this->session->userdata('filial_id'));
        $this->db->join('produto_filial', 'produto_filial.produto_id = produtos.idProdutos');
        $this->db->order_by('descricao');
        $this->db->limit($perpage,$start);

        if($termo){
            $this->db->or_like('descricao', $termo);
            $this->db->or_like('unidade', $termo);
            $this->db->or_like('ncm', $termo);
            $this->db->or_like('cEAN', $termo);
            $this->db->or_like('cProd', $termo);
        }
        $query = $this->db->get();
        return $query->result();
    }
}