<?php

class pdv_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }
}