<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class pedido_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields . ', forma_pagamento.nome formaPagamento, condicao_pagamento.nome condicaoPagamento, fornecedor.nomeFornecedor, fornecedor.idFornecedor');
        $this->db->from($table);
        $this->db->limit($perpage, $start);
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = ' . $table . '.fornecedor_id');
        $this->db->join('condicao_pagamento', 'condicao_pagamento.idCondicaoPagamento = ' . $table . '.condicao_pagamento_id','left');
        $this->db->join('forma_pagamento', 'forma_pagamento.idFormaPagamento = ' . $table . '.forma_pagamento_id','left');
        $this->db->where($table.'.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idPedido', 'desc');
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->select('pedido.*,forma_pagamento.nome formaPagamento, condicao_pagamento.nome condicaoPagamento, pedido.observacao observacaoPedido, fornecedor.*, usuarios.*');
        $this->db->from('pedido');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = pedido.fornecedor_id');
        $this->db->join('condicao_pagamento', 'condicao_pagamento.idCondicaoPagamento = pedido.condicao_pagamento_id','left');
        $this->db->join('forma_pagamento', 'forma_pagamento.idFormaPagamento = pedido.forma_pagamento_id','left');
        $this->db->join('usuarios', 'usuarios.idUsuarios = pedido.usuarios_id');
        $this->db->where('pedido.idPedido', $id);
        $this->db->where('pedido.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getFornecedorPedidoById($id)
    {
        $this->db->select('pedido.*, fornecedor.*');
        $this->db->from('pedido');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = pedido.fornecedor_id');
        $this->db->where('pedido.idPedido', $id);
        $this->db->where('pedido.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getProdutos($id = null)
    {
        $this->db->select('itensPedido.*, produtos.*');
        $this->db->from('itensPedido');
        $this->db->join('produtos', 'produtos.idProdutos = itensPedido.produtos_id');
        $this->db->where('pedido_id', $id);
        $this->db->where('itensPedido.filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }


    function add($table, $data, $returnId = false)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    public function autoCompleteProduto($q)
    {

        $this->db->select('*');
        $this->db->limit(50);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->like('descricao', $q);
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['descricao'] . ' | Preço: R$ ' . $row['precoVenda'] . ' | Estoque: ' . $row['estoque'], 'estoque' => $row['estoque'], 'id' => $row['idProdutos'], 'preco' => $row['precoVenda'], 'custo' => $row['precoCompra']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteCliente($q)
    {

        $this->db->select('clientes.nomeCliente, clientes.telefone, clientes.idClientes');
        $this->db->limit(50);
        $this->db->like('nomeCliente', $q);
        $this->db->or_like('telefone', $q);
        $this->db->or_like('celular', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('modelo', $q);
        $this->db->or_like('marca', $q);
        $this->db->or_like('placa', $q);
        $this->db->or_like('tipoVeiculo', $q);
        $this->db->or_like('cor', $q);
        $this->db->or_like('ano', $q);
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('veiculos', 'clientes.idClientes = veiculos.cliente_id', 'left');
        $this->db->distinct();
        $query = $this->db->get('clientes');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nomeCliente'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idClientes']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteUsuario($q)
    {

        $this->db->select('*');
        $this->db->limit(50);
        $this->db->like('nome', $q);
        $this->db->where('situacao', 1);
        $query = $this->db->get('usuarios');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nome'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idUsuarios']);
            }
            echo json_encode($row_set);
        }
    }

    function getPedidoItemByPedido($pedidoItem_id)
    {
        $this->db->where('idItens', $pedidoItem_id);
        $this->db->limit(1);
        return $this->db->get('itensPedido')->row();
    }

}
