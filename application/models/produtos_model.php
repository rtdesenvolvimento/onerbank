<?php
class Produtos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('descricao','asc');
        $this->db->limit($perpage,$start);

        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getProdutos($where='',$perpage=0,$start=0,$one=false,$array='array'){
        $this->db->select('p.*, s.nome setor, g.nome grupoproduto ');
        $this->db->from('produtos p');
        $this->db->join('setor s', 's.idSetor = p.setor_id', 'left');
        $this->db->join('grupoproduto g', 'g.idGrupoProduto = p.grupoProduto_id', 'left');
        $this->db->order_by('descricao','asc');
        $this->db->limit($perpage,$start);
        $this->db->where('patrimonio', 0);

        if($where){
            $this->db->where($where);
        }
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getProdutosFiliais($where='', $descricaoProduto=NULL,$tipoConsultaEstoque=NULL, $grupo_id=NULL)
    {

        $this->db->select('produto_filial.*, f.nome as filial, p.imagem, p.descricao produto, p.imagem, p.unidade, p.idProdutos');
        $this->db->from('produto_filial');
        $this->db->where('p.patrimonio', 0);
        $this->db->where('produto_filial.filial_id',$this->session->userdata('filial_id'));
        $this->db->join('produtos p', 'p.idProdutos = produto_filial.produto_id');
        $this->db->join('filial f', 'f.idFilial = produto_filial.filial_id');
        $this->db->order_by('p.descricao','desc');

        if($where) $this->db->where($where);
        if($grupo_id) $this->db->where('p.grupoProduto_id', $grupo_id);

        if($descricaoProduto) {
            $this->db->or_like('p.descricao',$descricaoProduto);
            $this->db->or_like('p.cProd',$descricaoProduto);
            $this->db->or_like('p.cEAN',$descricaoProduto);
        }

        if ($tipoConsultaEstoque) {
            if ($tipoConsultaEstoque=='COMSALDO') {
                $this->db->where('produto_filial.estoque > 0');
            } else if ($tipoConsultaEstoque == 'SEMSALDO') {
                $this->db->where('produto_filial.estoque <= 0');
            }
        }

        $query = $this->db->get();
        $result =  $query->result();

        return $result;
    }

    function getHistoricoEstoque($produto, $filial){
        $this->db->select('historico_estoque.*, f.nome as filial, p.descricao produto, p.unidade, produto_filial.estoque destoqueAtual');
        $this->db->from('historico_estoque');
        $this->db->join('produtos p', 'p.idProdutos = historico_estoque.produto_id', 'left');
        $this->db->join('produto_filial', 'p.idProdutos = produto_filial.produto_id', 'left');
        $this->db->join('filial f', 'f.idFilial = historico_estoque.filial_id', 'left');
        $this->db->where('historico_estoque.produto_id', $produto);
        $this->db->where('historico_estoque.filial_id', $filial);

        $this->db->order_by('historico_estoque.data','desc');
        $this->db->order_by('historico_estoque.hora','desc');

        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }

    public function getProdutosPendenteVenda($produto_id) {
        $this->db->select('sum(quantidade) as quantidade');
        $this->db->from('itens_de_vendas');
        $this->db->join('vendas', 'vendas.idVendas = itens_de_vendas.vendas_id', 'left');

        $this->db->where('vendas.faturado', 0);
        $this->db->where('vendas.status', 0);
        $this->db->where('itens_de_vendas.produtos_id', $produto_id);
        $this->db->where('itens_de_vendas.filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->row();
    }

    function suggestions($term) {

        $this->db->select("
            p.idProdutos id, p.descricao as label ,      
            pf.estoque - (SELECT COALESCE(SUM(quantidade),0) as quantidade 
            FROM itens_de_vendas LEFT JOIN vendas ON vendas.idVendas = itens_de_vendas.vendas_id 
            WHERE   vendas.faturado = 0 
                AND vendas.status = 0 
                AND itens_de_vendas.produtos_id = p.idProdutos AND itens_de_vendas.filial_id = ".$this->session->userdata('filial_id')."            
            ) as estoque,
            p.descricao as descricao,
            pf.precoVenda,
            p.unidade as unidade,
            pf.precoCompra ", FALSE);

        $this->db->from('produto_filial pf');
        $this->db->join('produtos p', 'p.idProdutos = pf.produto_id', 'left');
        $this->db->join('filial f', 'f.idFilial = pf.filial_id', 'left');

        $this->db->or_like('p.cProd',$term);
        $this->db->or_like('p.cEAN',$term);
        $this->db->or_like('p.descricao',$term);

        $this->db->where('p.patrimonio', 0);
        $this->db->where('f.idFilial', $this->session->userdata('filial_id'));

        $this->db->group_by('p.idProdutos');
        $this->db->order_by('p.descricao','desc');

        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }

    function getById($id){
        $this->db->select('p.*, s.nome setor, g.nome grupoproduto ');
        $this->db->join('setor s', 's.idSetor = p.setor_id', 'left');
        $this->db->join('grupoproduto g', 'g.idGrupoProduto = p.grupoProduto_id', 'left');
        $this->db->where('p.idProdutos',$id);
        $this->db->limit(1);
        return $this->db->get('produtos p')->row();
    }

    function getByCProd($cProd) {
        $this->db->where('cProd', $cProd);
        $this->db->limit(1);
        return $this->db->get('produtos')->row();
    }

    function getByCProdByFornecedor($cProd) {
        $this->db->where('codigo', $cProd);
        $this->db->limit(1);
        return $this->db->get('fornecedorproduto')->row();
    }

    function getProdutoFilialById($id) {
        $this->db->select('produtos.idProdutos, produtos.*, produto_filial.precoVenda, produto_filial.estoque, produto_filial.precoCompra ');
        $this->db->join('produto_filial', 'produto_filial.produto_id = produtos.idProdutos');
        $this->db->where('produto_filial.filial_id',$this->session->userdata('filial_id'));
        $this->db->where('idProdutos',$id);
        $this->db->limit(1);
        return $this->db->get('produtos')->row();
    }

    function getProdutoFilialByIdAndFilial($id, $filial) {
        $this->db->select('produtos.idProdutos, produtos.*, produto_filial.precoVenda, produto_filial.estoque, produto_filial.precoCompra ');
        $this->db->join('produto_filial', 'produto_filial.produto_id = produtos.idProdutos');
        $this->db->where('produto_filial.filial_id',$filial);
        $this->db->where('idProdutos',$id);
        $this->db->limit(1);
        return $this->db->get('produtos')->row();
    }

    function getProdutoFilial($idProduto, $idFilial) {
        $this->db->limit(1);
        $this->db->where('produto_id',$idProduto);
        $this->db->where('filial_id',$idFilial);
        return $this->db->get('produto_filial')->row();
    }

    function getAllProdutos($setor_id) {
        $this->db->where('setor_id',$setor_id);
        return $this->db->get('produtos')->result();
    }

    function getAll() {
        $this->db->join('produto_filial', 'produto_filial.produto_id = produtos.idProdutos');
        $this->db->where('produto_filial.filial_id',$this->session->userdata('filial_id'));
        $this->db->group_by('produtos.idProdutos');
        return $this->db->get('produtos')->result();
    }

    function getProdutoFiliais($produto) {
        $this->db->select('produto_filial.*, f.nome as filial, p.descricao produto, p.unidade');
        $this->db->join('produtos p', 'p.idProdutos = produto_filial.produto_id', 'left');
        $this->db->join('filial f', 'f.idFilial = produto_filial.filial_id', 'left');
        $this->db->where('produto_id',$produto);
        return $this->db->get('produto_filial')->result();
    }


    function getUnicProdutoFilial($idProdutoFilial) {
        $this->db->where('idProdutoFilial',$idProdutoFilial);
        $this->db->limit(1);
        return $this->db->get('produto_filial')->row();
    }

    function atualizarPrecoVendaImportacaoCSV($products) {
        if (empty($products)) return false;

        foreach ($products as $product) {

            $idProdutos = $product['idProdutos'];
            $venda = $product['Venda'];
            $filialId = $product['filial_id'];

            if ($idProdutos > 0 ) {
                $isExisteProdutoFilial = $this->getProdutoFilial($idProdutos, $filialId);
                $this->edit('produto_filial', array('precoVenda' => $venda), 'idProdutoFilial', $isExisteProdutoFilial->idProdutoFilial);
            }
        }
        return true;
    }

    function atualizarEstoqueImportacaoCSV($products) {
        if (empty($products)) return false;

        foreach ($products as $product) {

            $idProdutos = $product['idProdutos'];
            $estoque = $product['Estoque'];
            $filialId = $product['filial_id'];

            if ($idProdutos > 0 ) {

                $isExisteProdutoFilial = $this->getProdutoFilial($idProdutos, $filialId);
                $estoque_original = $isExisteProdutoFilial->estoque;

                if ($estoque > $estoque_original) {
                    $this->estoque_model->entradaEstoque(($estoque - $estoque_original), $idProdutos, 'Ajuste Manual', null, null, $filialId);
                } else if($estoque < $estoque_original){
                    $this->estoque_model->saidaEstoque(($estoque_original - $estoque), $idProdutos, 'Ajuste Manual', null, null, $filialId);
                }

            }
        }
        return true;
    }

    function addProdutoImportacaoCSV($products) {
        if (empty($products)) return false;

        foreach ($products as $product) {

            $idProdutos = $product['idProdutos'];
            $estoque = $product['Estoque'];
            $custo = $product['Custo'];
            $venda = $product['Venda'];
            $filial_id = $product['filial_id'];

            unset($product['Estoque']);
            unset($product['Custo']);
            unset($product['Venda']);
            unset($product['idProdutos']);

            if ($idProdutos > 0 ) {
                $this->edit('produtos', $product, 'idProdutos', $idProdutos);
                $isExisteProdutoFilial = $this->getProdutoFilial($idProdutos, $filial_id);

                if (count($isExisteProdutoFilial) > 0) {
                    $this->editarProdutoImportacaoEstoqueCSV($idProdutos, $filial_id, $isExisteProdutoFilial->estoque, $estoque, $custo, $venda, $isExisteProdutoFilial->idProdutoFilial);
                } else {
                    $this->addProdutoImportacaoEstoqueCSV($idProdutos, $filial_id, $estoque, $custo, $venda);
                }

            } else if ($this->db->insert('produtos', $product)) {
                $this->addProdutoImportacaoEstoqueCSV($this->db->insert_id(), $filial_id, $estoque, $custo, $venda);
            }
        }

        return true;
    }

    function editarProdutoImportacaoEstoqueCSV($produto_id, $filialId, $estoque_original, $estoque, $precoCompra, $precoVenda, $idProdutoFilial) {

        $produto_filial = array(
            'filial_id' => $filialId,
            'produto_id' => $produto_id,
            'estoqueMinimo' => 0,
            'precoVenda' => $precoVenda,
            'precoCompra' => $precoCompra
        );

        $this->edit('produto_filial', $produto_filial, 'idProdutoFilial', $idProdutoFilial);

        if ($estoque > $estoque_original) {
            $this->estoque_model->entradaEstoque(($estoque - $estoque_original), $produto_id, 'Ajuste Manual', null, null, $filialId);
        } else if($estoque < $estoque_original){
            $this->estoque_model->saidaEstoque(($estoque_original - $estoque), $produto_id, 'Ajuste Manual', null, null, $filialId);
        }
    }

    function addProdutoImportacaoEstoqueCSV($produto_id, $filialId, $estoque, $precoCompra, $precoVenda) {

        $produto_filial = array(
            'filial_id' => $filialId,
            'produto_id' => $produto_id,
            'estoqueMinimo' => 0,
            'precoVenda' => $precoVenda,
            'precoCompra' => $precoCompra
        );

        $this->produtos_model->addProdutoFilial('produto_filial', $produto_filial, TRUE);
        $this->estoque_model->entradaEstoque($estoque, $produto_id, 'Ajuste Manual', null, null, $filialId);
    }

    function add($table,$data, $returnId = false){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function addProdutoFilial($table,$data, $returnId = false){
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		return FALSE;        
    }

    function deletarProdutoFilial($idProduto, $idFilial) {
        $this->db->where('produto_id',$idProduto);
        $this->db->where('filial_id',$idFilial);
        $this->db->delete('produto_filial');
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        return FALSE;
    }
	
	function count($table){
		return $this->db->count_all($table);
	}

    function autoCompleteFornecedorFilterSelect($q) {
        $this->db->select("idFornecedor id, concat(nomeFornecedor, \" - \", telefone, \" / \", celular) as text", FALSE);
        $this->db->or_like('nomeFornecedor', $q);
        $this->db->or_like('nomeFantasiaApelido', $q);
        $this->db->or_like('telefone', $q);
        $this->db->or_like('celular', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('documento', $q);
        $this->db->distinct();
        $this->db->order_by('nomeFornecedor', 'asc');
        $q = $this->db->get_where('fornecedor');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function autoCompleteFornecedorById($cliente_id) {
        $this->db->select("idFornecedor id, concat(nomeFornecedor, \" - \", telefone) as text", FALSE);
        $this->db->where('idFornecedor', $cliente_id);
        $q = $this->db->get_where('fornecedor');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getFornecedoresProduto($idProduto = null){
        $this->db->select("fornecedorproduto.*, fornecedor.* , fornecedorproduto.observacao observacao");
        $this->db->where('idProduto',$idProduto);
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = fornecedorproduto.idFornecedor', 'left');
        $this->db->from('fornecedorproduto');
        return $this->db->get()->result();
    }

    public function autoCompleteProdutoByIdNotFilterFilial($idProduto) {
        $this->db->where('idProdutos', $idProduto);
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array(
                    'text'  => $row['descricao'],
                    'id'    => $row['idProdutos']
                );
            }
            return $row_set;
        }
    }

    function search($termo='',$perpage=0,$start=0){

        $this->db->select('produtos.idProdutos, produtos.descricao, produtos.unidade, produtos.ncm, produtos.cEAN, produtos.cProd, produtos.CST, produtos.CEST, produtos.CFOP, produto_filial.precoVenda, produto_filial.estoque, produto_filial.precoCompra ');
        $this->db->from('produtos');
        $this->db->where('produto_filial.filial_id',$this->session->userdata('filial_id'));
        $this->db->join('produto_filial', 'produto_filial.produto_id = produtos.idProdutos');
        $this->db->order_by('descricao');
        $this->db->limit($perpage,$start);

        if($termo){
            $this->db->or_like('descricao', $termo);
            $this->db->or_like('unidade', $termo);
            $this->db->or_like('ncm', $termo);
            $this->db->or_like('cEAN', $termo);
            $this->db->or_like('cProd', $termo);
        }
        $query = $this->db->get();
        return $query->result();
    }
}