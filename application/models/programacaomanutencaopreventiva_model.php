<?php
class programacaomanutencaopreventiva_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getProgramacao($where='',$perpage=0,$start=0,$one=false,$array='array'){

        $this->db->select('pmp.idProgramacaomanutencao, pmp.dataAgendamento, pmp.numeroProgramacao, produtos.descricao produto, peca.descricao peca , servicos.nome servico, pmp.situacao, pmp.prazoEntrega, pmp.valorManutencao, fornecedor.nomeFornecedor ');
        $this->db->from('programacao_manutencao_preventiva_servico pmps');
        $this->db->join('programacao_manutencao_preventiva pmp', 'pmp.idProgramacaomanutencao = pmps.programacao_id', 'left');
        $this->db->join('produtos', 'produtos.idProdutos = pmp.produto_id', 'left');
        $this->db->join('produtos peca', 'peca.idProdutos = pmps.peca_id', 'left');
        $this->db->join('servicos', 'servicos.idServicos = pmps.servico_id', 'left');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = pmp.fornecedor_id', 'left');
        $this->db->order_by('idProgramacaomanutencao','desc');
        $this->db->limit($perpage,$start);

        if($where){
            $this->db->where($where);
        }
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getProgramacaoByProdutoHistorico($produto_id,$perpage=0,$start=0,$one=false,$array='array'){

        $this->db->select('pmp.idProgramacaomanutencao, pmp.dataAgendamento, pmp.numeroProgramacao, produtos.descricao produto, peca.descricao peca, servicos.nome servico, pmp.situacao, pmp.prazoEntrega, pmp.valorManutencao, fornecedor.nomeFornecedor ');
        $this->db->from('programacao_manutencao_preventiva_servico pmps');
        $this->db->join('programacao_manutencao_preventiva pmp', 'pmp.idProgramacaomanutencao = pmps.programacao_id', 'left');
        $this->db->join('produtos', 'produtos.idProdutos = pmp.produto_id', 'left');
        $this->db->join('servicos', 'servicos.idServicos = pmps.servico_id', 'left');
        $this->db->join('produtos peca', 'peca.idProdutos = pmps.peca_id', 'left');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = pmp.fornecedor_id', 'left');
        $this->db->order_by('idProgramacaomanutencao','desc');
        $this->db->where('pmp.produto_id',$produto_id);
        $this->db->where('pmp.situacao',2);

        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getProgramacaoByProdutoAgendada($produto_id,$perpage=0,$start=0,$one=false,$array='array'){

        $this->db->select('pmp.idProgramacaomanutencao, pmp.dataAgendamento, pmp.numeroProgramacao, produtos.descricao produto, peca.descricao peca, servicos.nome servico, pmp.situacao, pmp.prazoEntrega, pmp.valorManutencao, fornecedor.nomeFornecedor ');
        $this->db->from('programacao_manutencao_preventiva_servico pmps');
        $this->db->join('programacao_manutencao_preventiva pmp', 'pmp.idProgramacaomanutencao = pmps.programacao_id', 'left');
        $this->db->join('produtos', 'produtos.idProdutos = pmp.produto_id', 'left');
        $this->db->join('servicos', 'servicos.idServicos = pmps.servico_id', 'left');
        $this->db->join('produtos peca', 'peca.idProdutos = pmps.peca_id', 'left');
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = pmp.fornecedor_id', 'left');
        $this->db->order_by('idProgramacaomanutencao','desc');
        $this->db->where('pmp.produto_id',$produto_id);
        $this->db->where('pmp.situacao',1);

        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function verificarManutencaoPeriodo() {

        $manutencoes = $this->getManutencaoPeriodo();

        foreach ($manutencoes as $manutencao) {

            $emManuntecao   = FALSE;
            $dataHoje       = date('Y-m-d');
            $dataManutencao = $manutencao->dataInicialPreventiva;
            $periodo        = $manutencao->periodo;
            $intervaldo     = $this->diff($dataHoje, $dataManutencao);
            $tipoManutencao = $manutencao->tipoManutencao;

            if ($tipoManutencao == 1) {//dias
                if ($intervaldo->d >= $periodo) {
                    $this->gerarProgramacaoManutencaoPreventiva($manutencao);
                    $emManuntecao = TRUE;
                }
            }

            if ($tipoManutencao == 2) {//meses
                if ($intervaldo->m >= $periodo) {
                    $this->gerarProgramacaoManutencaoPreventiva($manutencao);
                    $emManuntecao = TRUE;
                }
            }

            if ($tipoManutencao == 3) {//anos
                if ($intervaldo->y >= $periodo) {
                    $this->gerarProgramacaoManutencaoPreventiva($manutencao);
                    $emManuntecao = TRUE;
                }
            }

            if ($tipoManutencao == 4) {//depreciacao
                $this->gerarProgramacaoManutencaoPreventiva($manutencao);
                $emManuntecao = TRUE;
            }

            if($emManuntecao) {
                $this->atualizarSituacaoManutencao($manutencao);
            }
        }
    }

    function gerarProgramacaoManutencaoPreventiva($manutencao) {

        $numeroProgramacao =  str_pad( $this->count('programacao_manutencao_preventiva'), 6, '0', STR_PAD_LEFT).'/'.date('Y');

        $programacao = array(
            'produto_id'                => $manutencao->produto_id,
            'manutencao_preventiva_id'  => $manutencao->idManutencaopreventiva,
            'dataAgendamento'           => $manutencao->dataInicialPreventiva,
            'numeroProgramacao'         => $numeroProgramacao,
            'situacao'                  => 1,
            'valorManutencao'           => 0,//TODO
         );

        $openProgramacao = $this->isVerificaProgramacaoManutencaoPreventivaPendente($manutencao->produto_id);
        if ($openProgramacao) {
            $programacao_id =  $openProgramacao->idProgramacaomanutencao;
            $this->edit('programacao_manutencao_preventiva', $programacao, 'idProgramacaomanutencao', $programacao_id);
        } else {
            $programacao_id = $this->add('programacao_manutencao_preventiva', $programacao, TRUE);
        }

        $this->gerarProgramacaoPreventivaServico($manutencao, $programacao_id);
    }

    function gerarProgramacaoPreventivaServico($manutencao, $programacao_id) {

        $servico = $this->getServicoById($manutencao->servico_id);
        $programacao = array(
            'servico_id'                => $servico->idServicos,
            'peca_id'                   => $manutencao->peca_id,
            'programacao_id'            => $programacao_id,
            'valor'                     => $servico->preco,
        );
        $this->add('programacao_manutencao_preventiva_servico', $programacao);
    }

    function getManutencaoPeriodo(){
        $this->db->from('manutencao_preventiva');
        $this->db->where("servico_id  <> ''");
        $this->db->where('situacao',1);
        $query = $this->db->get();
        return $query->result();
    }

    function diff($dataInicial , $dataFinal) {
        $data1 = new DateTime( $dataInicial);
        $data2 = new DateTime( $dataFinal);
        return $data1->diff( $data2 );
    }

    function add($table,$data, $returnId = false){
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }

    function getById($id)
    {
        $this->db->where('idProgramacaomanutencao', $id);
        $this->db->join('produtos', 'produtos.idProdutos = programacao_manutencao_preventiva.produto_id', 'left');
        $this->db->limit(1);
        return $this->db->get('programacao_manutencao_preventiva')->row();
    }

    function getServicosManutencaoPreventiva($programacao_id) {
        $this->db->from('programacao_manutencao_preventiva_servico pmps');
        $this->db->join('servicos', 'servicos.idServicos = pmps.servico_id', 'left');
        $this->db->join('produtos', 'produtos.idProdutos = pmps.peca_id', 'left');
        $this->db->where('pmps.programacao_id', $programacao_id);
        $query = $this->db->get();
        return $query->result();
    }

    function isVerificaProgramacaoManutencaoPreventivaPendente($produto_id) {
        $this->db->where('produto_id', $produto_id);
        $this->db->where('situacao', 1);
        $this->db->limit(1);
        return $this->db->get('programacao_manutencao_preventiva')->row();
    }

    function getServicoById($id)
    {
        $this->db->where('idServicos', $id);
        $this->db->limit(1);
        return $this->db->get('servicos')->row();
    }

    function atualizarSituacaoManutencao($manutencao) {
        $programacao = array(
            'situacao' => 2
        );
        $this->edit('manutencao_preventiva', $programacao, 'idManutencaopreventiva', $manutencao->idManutencaopreventiva);
    }


}