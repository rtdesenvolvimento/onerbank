<?php

class promissoria_model extends CI_Model
{
    public $valor;
    public $nomePessoa;
    public $cpfCnpjPessoa;
    public $dataVencimento;
    public $refParcela;
    public $refVenda;
    public $refNF;
    public $refPedido;
    public $tipoCobranca;

    function __construct()
    {
        parent::__construct();
        $this->load->model('site_model', '', TRUE);
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getNomePessoa()
    {
        return $this->nomePessoa;
    }

    /**
     * @param mixed $nomePessoa
     */
    public function setNomePessoa($nomePessoa)
    {
        $this->nomePessoa = $nomePessoa;
    }

    /**
     * @return mixed
     */
    public function getCpfCnpjPessoa()
    {
        return $this->cpfCnpjPessoa;
    }

    /**
     * @param mixed $cpfCnpjPessoa
     */
    public function setCpfCnpjPessoa($cpfCnpjPessoa)
    {
        $this->cpfCnpjPessoa = $cpfCnpjPessoa;
    }

    /**
     * @return mixed
     */
    public function getDataVencimento()
    {
        return $this->site_model->formatarData($this->dataVencimento);
    }

    /**
     * @param mixed $dataVencimento
     */
    public function setDataVencimento($dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;
    }

    /**
     * @return mixed
     */
    public function getRefParcela()
    {
        $ref = $this->refParcela;

        if ($this->getRefPedido() != '') $ref .= ' Pedido ° '.$this->getRefPedido();
        if ($this->getRefVenda() != '') $ref .= ' Venda nº '.$this->getRefVenda();
        if ($this->getRefNF() != '') $ref .= ' NF '.$this->getRefNF();

        $ref .= ' com vencimento dia '.$this->getDataVencimento().'.';
        return $ref;
    }

    /**
     * @param mixed $refParcela
     */
    public function setRefParcela($refParcela)
    {
        $this->refParcela = $refParcela;
    }

    /**
     * @return mixed
     */
    public function getRefVenda()
    {
        return $this->refVenda;
    }

    /**
     * @param mixed $refVenda
     */
    public function setRefVenda($refVenda)
    {
        $this->refVenda = $refVenda;
    }

    /**
     * @return mixed
     */
    public function getRefNF()
    {
        return $this->refNF;
    }

    /**
     * @param mixed $refNF
     */
    public function setRefNF($refNF)
    {
        $this->refNF = $refNF;
    }

    /**
     * @return mixed
     */
    public function getRefPedido()
    {
        return $this->refPedido;
    }

    /**
     * @param mixed $refPedido
     */
    public function setRefPedido($refPedido)
    {
        $this->refPedido = $refPedido;
    }

    /**
     * @return mixed
     */
    public function getTipoCobranca()
    {
        return $this->tipoCobranca;
    }

    /**
     * @param mixed $tipoCobranca
     */
    public function setTipoCobranca($tipoCobranca)
    {
        $this->tipoCobranca = $tipoCobranca;
    }



}