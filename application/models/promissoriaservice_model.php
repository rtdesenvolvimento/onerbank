<?php

class promissoriaservice_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('promissoria_model', '', TRUE);
        $this->load->model('financeiro_model','', TRUE);
        $this->load->model('Clientes_model','',TRUE);
        $this->load->model('tipo_cobranca_model','', TRUE);
        $this->load->model('Vendas_model', '', TRUE);
    }

    public function gerar($parcelaId) {

        $parcela = $this->financeiro_model->getParcelaById($parcelaId);
        $lancamento = $this->financeiro_model->getById($parcela->lancamentos_id);
        $cliente = $this->Clientes_model->getById($lancamento->clientes_id);
        $tipoCobranca = $this->tipo_cobranca_model->getById($lancamento->tipo_cobranca_id);
        $vendaId = '';
        $nNF = '';

        if ($lancamento->venda != '') {
            $venda = $this->Vendas_model->getById($lancamento->venda);
            $vendaId = $venda->idVendas;
            $nNF = $venda->nNF;
        }

        $promissoria = new promissoria_model();

        $promissoria->setNomePessoa($cliente->nomeCliente);
        $promissoria->setCpfCnpjPessoa($cliente->documento);
        $promissoria->setRefParcela($parcela->numeroParcela . '/' . $parcela->totalParcelas);
        $promissoria->setValor($parcela->valorPagar);
        $promissoria->setDataVencimento($parcela->dtVencimento);
        $promissoria->setTipoCobranca($tipoCobranca->descricao);
        $promissoria->setRefVenda($vendaId);
        $promissoria->setRefNF($nNF);

        return $promissoria;
    }


}