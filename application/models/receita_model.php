<?php

class receita_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $this->db->select('r.idReceita, r.codigo, r.nome, s.nome as superior ');
        $this->db->from('receita r');
        $this->db->join('receita s', 'r.receitaSuperior_id = s.idReceita', 'left');
        $this->db->where('r.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('r.receitaSuperior_id', 'asc');
        $this->db->limit($perpage, $start);

        if ($where) $this->db->where($where);

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    public function getAll()
    {
        $this->db->from('receita');
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->where('idReceita', $id);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get('receita')->row();
    }

    function add($table, $data)
    {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }
}