<?php

class recibo_model extends CI_Model
{
    public $valor;
    public $nomePessoa;
    public $cpfCnpjPessoa;
    public $credito;
    public $grupo;
    public $dataPagamento;
    public $pagamentoId;
    public $formaPagamento;
    private $parcelas = [];

    function __construct()
    {
        parent::__construct();
        $this->load->model('reciboparcela_model', '', TRUE);
    }

    function adicionarParcela($parcela) {
        $this->parcelas[count($this->parcelas) + 1] = $parcela;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }


    /**
     * @return mixed
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * @param mixed $credito
     */
    public function setCredito($credito)
    {
        $this->credito = $credito;
    }

    /**
     * @return mixed
     */
    public function getReferencia()
    {
        $referencia = 'referente ao pagamento da(s) parcela(s):<br/>';

        foreach ($this->getParcelas() as $parcela) {
            $referencia .= ' Ref. Parcela '.$parcela->getNParcela().' com vencimento dia '.$parcela->getDataVencimento();

            if ($parcela->getRefPedido() != '') $referencia .= ' Pedido '.$parcela->getRefPedido();
            if ($parcela->getRefVenda() != '') $referencia .= ' Venda '.$parcela->getRefVenda();
            if ($parcela->getRefNF() != '') $referencia .= ' NF '.$parcela->getRefNF();

            $referencia .= '<br/>';
        }

        return $referencia;
    }


    /**
     * @return mixed
     */
    public function getNomePessoa()
    {
        return $this->nomePessoa;
    }

    /**
     * @param mixed $nomePessoa
     */
    public function setNomePessoa($nomePessoa)
    {
        $this->nomePessoa = $nomePessoa;
    }

    /**
     * @return mixed
     */
    public function getCpfCnpjPessoa()
    {
        return $this->cpfCnpjPessoa;
    }

    /**
     * @param mixed $cpfCnpjPessoa
     */
    public function setCpfCnpjPessoa($cpfCnpjPessoa)
    {
        $this->cpfCnpjPessoa = $cpfCnpjPessoa;
    }

    /**
     * @return mixed
     */
    public function getDataPagamento()
    {
        return $this->dataPagamento;
    }

    /**
     * @param mixed $dataPagamento
     */
    public function setDataPagamento($dataPagamento)
    {
        $this->dataPagamento = $dataPagamento;
    }

    public function getParcelas() {
        return $this->parcelas;
    }

    public function getId() {
        return $this->idRecibo;
    }

    /**
     * @return mixed
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param mixed $grupo
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;
    }

    /**
     * @return mixed
     */
    public function getPagamentoId()
    {
        return $this->pagamentoId;
    }

    /**
     * @param mixed $pagamentoId
     */
    public function setPagamentoId($pagamentoId)
    {
        $this->pagamentoId = $pagamentoId;
    }

    /**
     * @return mixed
     */
    public function getFormaPagamento()
    {
        if ($this->getPagamentoId() == '') return '';

        return  ' em '.$this->formaPagamento;
    }

    /**
     * @param mixed $formaPagamento
     */
    public function setFormaPagamento($formaPagamento)
    {
        $this->formaPagamento = $formaPagamento;
    }
}