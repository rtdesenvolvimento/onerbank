<?php

class reciboparcela_model extends CI_Model
{
    public $recibo_id;
    public $parcela_id;
    public $nParcela;
    public $refPedido;
    public $refVenda;
    public $refNF;
    public $dataVencimento;

    function __construct()
    {
        parent::__construct();

        $this->load->model('site_model', '', TRUE);

    }

    /**
     * @return mixed
     */
    public function getReciboId()
    {
        return $this->recibo_id;
    }

    /**
     * @param mixed $recibo_id
     */
    public function setReciboId($recibo_id)
    {
        $this->recibo_id = $recibo_id;
    }

    /**
     * @return mixed
     */
    public function getParcelaId()
    {
        return $this->parcela_id;
    }

    /**
     * @param mixed $parcela_id
     */
    public function setParcelaId($parcela_id)
    {
        $this->parcela_id = $parcela_id;
    }

    /**
     * @return mixed
     */
    public function getNParcela()
    {
        return $this->nParcela;
    }

    /**
     * @param mixed $nParcela
     */
    public function setNParcela($nParcela)
    {
        $this->nParcela = $nParcela;
    }

    /**
     * @return mixed
     */
    public function getRefPedido()
    {
        return $this->refPedido;
    }

    /**
     * @param mixed $refPedido
     */
    public function setRefPedido($refPedido)
    {
        $this->refPedido = $refPedido;
    }

    /**
     * @return mixed
     */
    public function getRefVenda()
    {
        return $this->refVenda;
    }

    /**
     * @param mixed $refVenda
     */
    public function setRefVenda($refVenda)
    {
        $this->refVenda = $refVenda;
    }

    /**
     * @return mixed
     */
    public function getRefNF()
    {
        return $this->refNF;
    }

    /**
     * @param mixed $refNF
     */
    public function setRefNF($refNF)
    {
        $this->refNF = $refNF;
    }

    /**
     * @return mixed
     */
    public function getDataVencimento()
    {
        return $this->site_model->formatarData($this->dataVencimento);
    }

    /**
     * @param mixed $dataVencimento
     */
    public function setDataVencimento($dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;
    }





}