<?php

class reciborepository_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function buscarReciboParcelaGrupo($parcelaId) {
        $this->db->from('recibo_parcela');
        $this->db->join('recibo', 'recibo_parcela.recibo_id = recibo.idRecibo');
        $this->db->where('recibo.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('parcela_id', $parcelaId);
        $this->db->where('grupo', true);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function buscarReciboDePagamento($pagamentoId) {
        $this->db->from('recibo');
        $this->db->where('pagamentoId', $pagamentoId);
        $this->db->where('grupo', false);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

}