<?php

class reciboservice_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('recibo_model', '', TRUE);
    }

    function salvar($recibo) {
        $reciboId = $this->__save('recibo',  $recibo->__toArray());

        $this->salvarParcelas($recibo, $reciboId);

        return $reciboId;
    }

    private function salvarParcelas($recibo, $reciboId) {
        foreach($recibo->getParcelas() as $parcela){
            $parcela->setReciboId($reciboId);
            $this->__save('recibo_parcela', $parcela->__toArray());
        }
    }

    public function excluir($id) {
        $this->__delete('recibo','idRecibo', $id);
        $this->__delete('recibo_parcela','recibo_id', $id);
    }

    function gerar($id) {
        return $this->montar($id);
    }

    private function montar($id) {
        $recibo   = $this->buscarOuFalhar($id);
        $parcelas = $this->__getAllItens('recibo_parcela', 'recibo_id', $id);

        foreach ($parcelas as $parcela) {
            $recibo->adicionarParcela($this->__toModel($parcela, new reciboparcela_model()));
        }

        return  $recibo;
    }

    function buscarOuFalhar($id) {

        if(!$this->__exist('recibo', $id)) throw new Exception('Recibo não encontrado.');

        return $this->__getByIdToModel('recibo', $id, new Recibo_model());
    }
}