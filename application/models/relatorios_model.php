<?php
class Relatorios_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    
    function add($table,$data){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }

    function count($table) {
        return $this->db->count_all($table);
    }
    
    public function clientesCustom($dataInicial = null,$dataFinal = null){

        $this->db->select('nomeCliente, documento, telefone, email, dataCadastro');
        $this->db->order_by('nomeCliente','asc');
        $this->db->where('filial_id', $this->session->userdata('filial_id'));

        if ($dataInicial) {
            $this->db->where("dataCadastro >='".$dataInicial."'");
        }

        if ($dataFinal) {
            $this->db->where("dataCadastro <='".$dataFinal."'");
        }

        return $this->db->get('clientes')->result();
    }

    public function clientesRapid(){
        $this->db->order_by('nomeCliente','asc');
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('clientes')->result();
    }

    public function produtosRapid(){
        $this->db->select('descricao, unidade, produto_filial.precoCompra, produto_filial.precoVenda,produto_filial.estoque, produto_filial.localizacao ');
        $this->db->where('produto_filial.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('produto_filial','produto_filial.produto_id = produtos.idProdutos');
        $this->db->order_by('descricao','asc');
        return $this->db->get('produtos')->result();
    }

    public function servicosRapid(){
        $this->db->order_by('nome','asc');
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('servicos')->result();
    }

    public function osRapid(){
        $this->db->select('os.*, clientes.*, clientes.telefone telefone_cliente, clientes.celular celular_cliente, aparelho.nome aparelho');
        $this->db->from('os');
        $this->db->join('aparelho','aparelho.idAparelho = os.aparelho_id','left');
        $this->db->join('clientes','clientes.idClientes = os.clientes_id');
        $this->db->where('os.filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }

    public function produtosRapidMin(){
        $this->db->select('descricao, unidade, produto_filial.precoVenda, produto_filial.precoCompra, produto_filial.estoque, produto_filial.estoqueMinimo, produto_filial.localizacao');
        $this->db->where('produto_filial.estoque <= produto_filial.estoqueMinimo');
        $this->db->where('produto_filial.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('produto_filial','produto_filial.produto_id = produtos.idProdutos');
        $this->db->order_by('descricao','asc');
        return $this->db->get('produtos')->result();
    }

    public function produtosCustom($precoInicial = null,$precoFinal = null,$estoqueInicial = null,$estoqueFinal = null){
        $wherePreco = "";
        $whereEstoque = "";
        if($precoInicial != null){
            $wherePreco = " AND precoVenda BETWEEN ".$this->db->escape($precoInicial)." AND ".$this->db->escape($precoFinal);
        }
        if($estoqueInicial != null){
            $whereEstoque = " AND estoque BETWEEN ".$this->db->escape($estoqueInicial)." AND ".$this->db->escape($estoqueFinal);
        }
        $query = "
            SELECT p.descricao,
                 p.unidade, pd.precoCompra, pd.precoVenda, pd.estoque , pd.localizacao
                 FROM produtos p, produto_filial pd WHERE  pd.produto_id = p.idProdutos and pd.filial_id = ".$this->session->userdata('filial_id').$wherePreco.$whereEstoque;
        return $this->db->query($query)->result();
    }

    public function servicosCustom($precoInicial = null,$precoFinal = null){

        $this->db->select('nome, descricao, preco');
        $this->db->from('servicos');
        $this->db->where('filial_id',$this->session->userdata('filial_id'));
        if ($precoInicial){
            $this->db->where('preco>='.$precoInicial,NULL, false);
        }

        if ($precoFinal) {
            $this->db->where('preco<='.$precoFinal,NULL, false);
        }
        return $this->db->get()->result();
    }

    public function osCustom($dataInicial = null,$dataFinal = null,$cliente = null,$responsavel = null,$status = null){
        $whereData = "";
        $whereCliente = "";
        $whereResponsavel = "";
        $whereStatus = "";
        if($dataInicial != null){
            $whereData = "AND dataInicial BETWEEN ".$this->db->escape($dataInicial)." AND ".$this->db->escape($dataFinal);
        }
        if($cliente != null){
            $whereCliente = "AND clientes_id = ".$this->db->escape($cliente);
        }
        if($responsavel != null){
            $whereResponsavel = "AND usuarios_id = ".$this->db->escape($responsavel);
        }
        if($status != null){
            $whereStatus = "AND status = ".$this->db->escape($status);
        }
        $query = "SELECT os.idOs, clientes.nomeCliente, os.status, os.dataInicial, os.defeito, os.descricaoProduto FROM os LEFT JOIN clientes ON os.clientes_id = clientes.idClientes WHERE idOs != 0 $whereData $whereCliente $whereResponsavel $whereStatus";
        $query = $query.' and os.filial_id='. $this->session->userdata('filial_id');
        return $this->db->query($query)->result();
    }

    public function aniversarioCustom($mes = null){

        if($mes != null){
            $this->db->where("MONTH(data_nascimento) = ".$mes);
        }
        $this->db->select('nomeCliente,email, telefone, celular, data_nascimento');
        $this->db->from('clientes');
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }

    public function financeiroRapid(){

        $dataInicial = date('Y-m-01');
        $dataFinal = date("Y-m-d");

        return $this->financeiroCustom($dataInicial, $dataFinal);
    }

    public function financeiroCustom($dataInicial, $dataFinal, $previsaoPagamentoInicial = null, $previsaoPagamentoFinal = null, $tipo = null, $situacao = null){
        
        $whereTipo = "";
        $whereSituacao = "";
		$whereData = "";
		
		if ($dataInicial != null)  $whereData .= " AND parc.dtVencimento >= '".$dataInicial."'";
		if ($dataFinal != null)  $whereData .= " AND parc.dtVencimento <= '".$dataFinal."'";
        if($tipo == 'receita') $whereTipo = "AND l.tipo = 'receita'";
        if($tipo == 'despesa') $whereTipo = "AND l.tipo = 'despesa'";
        if($situacao == 'pendente') $whereSituacao = "AND l.baixado = 0";
        if($situacao == 'pago') $whereSituacao = "AND l.baixado = 1";

		$whereData .= ' and l.filial_id='.$this->session->userdata('filial_id');

        $query = "SELECT 
                c.nomeCliente, 
                f.nomeFornecedor, 
                l.tipo,
                l.forma_pgto, 
                l.baixado,
                parc.dtVencimento,
                parc.valorVencimento, 
                parc.valorPagar, 
                parc.valorPago, 
                parc.acrescimo,
                parc.desconto,
                parc.dtUltimoPagamento,
                parc.numeroParcela,
                parc.totalParcelas
              FROM lancamentos l left outer join clientes c on (c.idClientes =  l.clientes_id ) 
                                 left outer join fornecedor f on (f.idFornecedor = l.fornecedor_id ),
                                 parcela parc  
                                 WHERE l.idLancamentos = parc.lancamentos_id $whereData $whereTipo $whereSituacao ORDER BY parc.dtVencimento ";
		$resultados = $this->db->query($query)->result();

        return  $resultados;
    }

    public function vendasRapid(){
        $this->db->select('vendas.*,clientes.nomeCliente, usuarios.nome');
        $this->db->from('vendas');
        $this->db->join('clientes','clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = vendas.usuarios_id');
        $this->db->where('vendas.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('vendapdv', 0);
        return $this->db->get()->result();
    }

    public function vendasPdvRapid(){
        $this->db->select('vendas.*,clientes.nomeCliente, usuarios.nome');
        $this->db->from('vendas');
        $this->db->join('clientes','clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = vendas.usuarios_id');
        $this->db->where('vendas.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('vendapdv', 1);
        return $this->db->get()->result();
    }

    public function vendasCustom($dataInicial = null,$dataFinal = null,$cliente = null,$responsavel = null){
        $whereData = "";
        $whereCliente = "";
        $whereResponsavel = "";
        $whereStatus = "";
        if($dataInicial != null){
            $whereData = "AND dataVenda BETWEEN ".$this->db->escape($dataInicial)." AND ".$this->db->escape($dataFinal);
        }
        if($cliente != null){
            $whereCliente = "AND clientes_id = ".$this->db->escape($cliente);
        }
        if($responsavel != null){
            $whereResponsavel = "AND usuarios_id = ".$this->db->escape($responsavel);
        }

        $whereData .= ' and vendas.filial_id='.$this->session->userdata('filial_id');
        $query = "SELECT  clientes.nomeCliente,usuarios.nome as responsavel, valorTotal, dataVenda  FROM vendas LEFT JOIN clientes ON vendas.clientes_id = clientes.idClientes LEFT JOIN usuarios ON vendas.usuarios_id = usuarios.idUsuarios WHERE  vendapdv = 0 $whereData $whereCliente $whereResponsavel";
        return $this->db->query($query)->result();
    }

    public function vendasPdvCustom($dataInicial = null,$dataFinal = null,$cliente = null,$responsavel = null){
        $whereData = "";
        $whereCliente = "";
        $whereResponsavel = "";
        $whereStatus = "";
        if($dataInicial != null){
            $whereData = "AND dataVenda BETWEEN ".$this->db->escape($dataInicial)." AND ".$this->db->escape($dataFinal);
        }
        if($cliente != null){
            $whereCliente = "AND clientes_id = ".$this->db->escape($cliente);
        }
        if($responsavel != null){
            $whereResponsavel = "AND usuarios_id = ".$this->db->escape($responsavel);
        }

        $whereData .= ' and vendas.filial_id='.$this->session->userdata('filial_id');
        $query = "SELECT  clientes.nomeCliente,usuarios.nome as responsavel, valorTotal, dataVenda  FROM vendas LEFT JOIN clientes ON vendas.clientes_id = clientes.idClientes LEFT JOIN usuarios ON vendas.usuarios_id = usuarios.idUsuarios WHERE  vendapdv = 1 $whereData $whereCliente $whereResponsavel";
        return $this->db->query($query)->result();
    }

    /*
     * PATRIMONIO
     */
    public function patrimonioCustomizado($descricao=NULL, $grupoProduto_id=NULL, $centro_custo_id=NULL, $origemAquisicao=NULL){

        $this->db->select('p.cProd, p.descricao, p.origemAquisicao, p.dataAquisicao, p.valorAquisicao, p.valorDepreciado, p.valorAtualPosDepreciacao, p.marca, p.modelo, p.numeroSerie, p.localizacao, s.nome setor, g.nome grupoproduto , cc.nome centrocusto');
        $this->db->from('produtos p');
        $this->db->join('setor s', 's.idSetor = p.setor_id', 'left');
        $this->db->join('grupoproduto g', 'g.idGrupoProduto = p.grupoProduto_id', 'left');
        $this->db->join('centro_custo cc', 'cc.idCentrocusto = p.centro_custo_id', 'left');
        $this->db->order_by('idProdutos','desc');
        $this->db->where('patrimonio', 1);

        if ($descricao) {
            $this->db->or_like('p.descricao', $descricao);
        }

        if ($grupoProduto_id) {
            $this->db->where('p.grupoProduto_id', $grupoProduto_id);
        }

        if ($centro_custo_id) {
            $this->db->where('p.centro_custo_id', $centro_custo_id);
        }

        if ($origemAquisicao) {
            $this->db->where('p.origemAquisicao', $origemAquisicao);
        }
        return $this->db->get()->result();

    }

    /*
     * PGDAS
     */
    function pgdasNFeCustom($dataInicio=NULL, $dataFinal=NULL) {
        $this->db->select('nf.dhEmi, concat("NF-e") documento, sum(nf.vICMSnf) vICMSnf, sum(nf.vICMSSTnf) vICMSSTnf, sum(nf.valortotalprod) valortotalprod, sum(nf.valortotalnf) as valortotalnf , max(nNF) final, min(nNF) inicio, sum(nf.vDescnf) vDescnf');
        $this->db->from('nfe nf');
        $this->db->group_by('dhEmi');
        $this->db->order_by('dhEmi asc');
        $this->db->where('nf.status in (101, 100)');

        if ($dataInicio) {
            $this->db->where("nf.dhEmi >= '".$dataInicio."'", NULL, false);
        }

        if ($dataFinal) {
            $this->db->where("nf.dhEmi <= '".$dataFinal."'", NULL, false);
        }
        return $this->db->get()->result();
    }

    function pgdasNFeCanceladas($data) {

        $this->db->select('sum(nf.valortotalnf) as valor');
        $this->db->from('nfe nf');
        $this->db->group_by('dhEmi');
        $this->db->order_by('dhEmi desc');
        $this->db->limit(1);
        $this->db->where('nf.status', 101);
        if ($data) {
            $this->db->where("nf.dhEmi", $data);
        }
        return $this->db->get()->row();
    }

    function pgdasNFCeCustom($dataInicio=NULL, $dataFinal=NULL) {
        $this->db->select('
        nf.dhEmi, concat("NFC-e") documento,
        sum(nf.vICMSnf) vICMSnf,
        sum(nf.vBCSTnf) vBCSTnf, 
        sum(nf.vNF) vNF, 
        max(nNF) final,
        min(nNF) inicio, 
        sum(nf.vDescnf) vDescnf, 
        sum(nf.vOutronf) as vOutronf ');

        $this->db->from('nfce nf');
        $this->db->group_by('dhEmi');
        $this->db->order_by('dhEmi asc');
        $this->db->where('nf.status in (101, 100)');

        if ($dataInicio) {
            $this->db->where("nf.dhEmi >= '".$dataInicio."'", NULL, false);
        }

        if ($dataFinal) {
            $this->db->where("nf.dhEmi <= '".$dataFinal."'", NULL, false);
        }
        return $this->db->get()->result();
    }

    function pgdasNFCeCanceladas($data) {

        $this->db->select('sum(nf.vNF) valor');
        $this->db->from('nfce nf');
        $this->db->group_by('dhEmi');
        $this->db->order_by('dhEmi asc');
        $this->db->where('nf.status', 101);
        $this->db->limit(1);

        if ($data) {
            $this->db->where("nf.dhEmi", $data);
        }

        return $this->db->get()->row();
    }
}