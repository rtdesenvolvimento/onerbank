<?php

class Servicos_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idServicos', 'desc');
        $this->db->limit($perpage, $start);
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getServicos($where='',$perpage=0,$start=0,$one=false,$array='array'){
        $this->db->select('p.*, s.nome setor, g.nome grupoproduto ');
        $this->db->from('servicos p');
        $this->db->join('setor s', 's.idSetor = p.setor_id', 'left');
        $this->db->join('grupoproduto g', 'g.idGrupoProduto = p.grupoProduto_id', 'left');
        $this->db->order_by('idServicos','desc');
        $this->db->limit($perpage,$start);

        if($where){
            $this->db->where($where);
        }
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->where('idServicos', $id);
        $this->db->limit(1);
        return $this->db->get('servicos')->row();
    }


    function getAllServico($setor_id)
    {
        $this->db->where('setor_id', $setor_id);
        return $this->db->get('servicos')->result();
    }

    function getAll()
    {
        return $this->db->get('servicos')->result();
    }

    function add($table,$data, $returnId = false){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        return $this->db->count_all($table);
    }

    public function getFornecedoresServico($idServico = null){
        $this->db->select("fornecedorservico.*, fornecedor.* , fornecedorservico.observacao observacao");
        $this->db->where('idServico',$idServico);
        $this->db->join('fornecedor', 'fornecedor.idFornecedor = fornecedorservico.idFornecedor', 'left');
        $this->db->from('fornecedorservico');
        return $this->db->get()->result();
    }
}