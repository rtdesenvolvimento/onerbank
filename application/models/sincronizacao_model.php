<?php

class sincronizacao_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getAllPermissoes()
    {
        $this->db->select('permissoes.idPermissao as id, permissoes.nome descricao');
        $this->db->from('permissoes');
        return $this->db->get()->result();
    }

    public function getAllTiposTarefa()
    {
        $this->db->select('tipotarefa.idTipoTarefa as id, tipotarefa.descricao descricao');
        $this->db->from('tipotarefa');
        return $this->db->get()->result();
    }

    public function getAllFormulario()
    {
        $this->db->select('formulario.idFormulario as id, formulario.descricao descricao');
        $this->db->from('formulario');
        return $this->db->get()->result();
    }

    public function getAllPropostas()
    {
        $this->db->select('proposta.app_id as id, proposta.* ');
        $this->db->from('proposta');
        return $this->db->get()->result();
    }

    public function getAllRespostasProposta()
    {
        $this->db->select('respostaproposta.app_id as id, respostaproposta.* ');
        $this->db->from('respostaproposta');
        return $this->db->get()->result();
    }

    public function getAllRespostasPropostaOpcao()
    {
        $this->db->select('respostapropostaopcao.app_id as id, respostapropostaopcao.* ');
        $this->db->from('respostapropostaopcao');
        return $this->db->get()->result();
    }


    public function getAllUsuarios()
    {
        $this->db->select('usuarios.idUsuarios as id,usuarios.permissoes_id as cargo, usuarios.nome nomeCompleto, usuarios.email, usuarios.celular, usuarios.usuariologin as login, usuarios.senha as  password , usuarios.situacao as ativo, usuarios.photo as photo ');
        $this->db->from('usuarios');
        return $this->db->get()->result();
    }

    public function getAllPerguntasFormulario()
    {
        $this->db->select('perguntaformulario.idPerguntaFormulario as id, perguntaformulario.* ');
        $this->db->from('perguntaformulario');
        return $this->db->get()->result();
    }

    public function getAllDigitalizacao()
    {
        $this->db->select('digitalizacao.app_id as id, digitalizacao.* ');
        $this->db->from('digitalizacao');
        return $this->db->get()->result();
    }

    public function getAllPerguntasFormularioOpcao()
    {
        $this->db->select('perguntaformularioopcao.idPerguntaFormularioOpcao as id, perguntaformularioopcao.* ');
        $this->db->from('perguntaformularioopcao');
        return $this->db->get()->result();
    }

    public function getAllTarefa()
    {
        $this->db->select('tarefa.app_id as id, tarefa.* ');
        $this->db->from('tarefa');
        return $this->db->get()->result();
    }


    public function getAllClientes()
    {
        $this->db->select('
            c.app_id as id, 
            c.tipoPessoa, 
            c.nomeCliente as nome, 
            c.documento as cpfCnpj, 
            c.telefone  as telefone, 
            c.celular as celular, 
            c.IE as ie, 
            c.sexo as sexo, 
            c.rg as rg, 
            c.orgaoEmissor as orgaoEmissor, 
            c.estadoOrgaoEmissor as   estadoEmissor, 
            c.dataOrgaoEmissor as dataEmissao, 
            c.cep as cep, 
            c.rua as rua, 
            c.numero, 
            c.complemento, 
            c.bairro, 
            c.cidade, 
            c.estado, 
            c.contatoNomeCliente as nomeContato, 
            c.contatoTelefone as  telefoneContato, 
            c.contatoEmail as emailContato, 
            c.cargoContato, 
            c.observacao as observacao, 
            c.inativo as ativo, 
            c.responsavel,
            c.telefoneUsuario ');
        $this->db->from('clientes c');
        return $this->db->get()->result();
    }

    function add($table,$data){
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function addUsuario($table,$data){
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

}