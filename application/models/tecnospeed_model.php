<?php

class tecnospeed_model extends CI_Model
{

    private $URL_POST_GERA_TOKEN        = 'https://managersaas.tecnospeed.com.br:1337/api/v1/software-house/token';
    private $URL_POST_CADASTRAR_EMPRESA = 'https://managersaas.tecnospeed.com.br:1337/api/v1/empresa';
    private $URL_POST_LISTA_UF          = 'https://managersaas.tecnospeed.com.br:1337/api/v1/uf';
    private $URL_POST_LISTA_CIDADE      = 'https://managersaas.tecnospeed.com.br:1337/api/v1/cidade';

    function __construct()
    {
        parent::__construct();
    }

    function geraToken() {

        $licenca =  fopen('tecnospeed_licenca.txt', 'r');
        $email = '';
        $senha = '';

        $index = 0;
        while (!feof($licenca)) :
            $linha = fgets($licenca, 1024);

            if ($index == 0) {
                $email = $linha;
            }

            if ($index == 1) {
                $senha = $linha;
            }

            $index = $index + 1;
        endwhile;
        fclose($licenca);

        $url = $this->URL_POST_GERA_TOKEN;
        $headers = array(
            "Content-Type:application/x-www-form-urlencoded"
        );

        $dados = 'email='.trim($email).'&senha='.$senha.'&gerar=1';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        curl_close($ch);

        $token = explode( 'token', $this->response)[1];
        $token = str_replace('":"','', $token);
        $token = str_replace('","','', $token);

        return $token;
    }

    function getUF($uf, $token) {

        $url = $this->URL_POST_LISTA_UF.'?token='.$token;
        $headers = array(
            "Content-Type:application/x-www-form-urlencoded"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        $this->response = str_replace('{"mensagem":"Registros retornados com sucesso","dados":[','', $this->response );
        $this->response = str_replace('],"total":27}','', $this->response );
        curl_close($ch);

        foreach (json_decode("[".$this->response."]") as $a) {
            $sigla = $a->sigla;
            $handle = $a->handle;
            if ($sigla == $uf) {
                return $handle;
            }
        }
    }

    function getCidade($codigoIBGE, $token) {
        $url = $this->URL_POST_LISTA_CIDADE.'?token='.$token;
        $headers = array(
            "Content-Type:application/x-www-form-urlencoded"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        $this->response = str_replace('{"mensagem":"Registros retornados com sucesso","dados":[','', $this->response );
        $this->response = str_replace('],"total":5574}','', $this->response );
        curl_close($ch);

        foreach (json_decode("[".$this->response."]") as $a) {
            $codigoibge     = $a->codigoibge;
            $handle         = $a->handle;
            if ($codigoibge == $codigoIBGE) {
                return $handle;
            }
        }
    }

    function cadastrarEmpresa($empresa, $cod_grupo) {

        $token  = $this->geraToken();
        $url    = $this->URL_POST_CADASTRAR_EMPRESA.'?token='.$token;

        $headers = array(
            "Content-Type:application/x-www-form-urlencoded"
        );

        $cert = $empresa->cert;
        $certificadobinario = '';
        if ($cert) {
            $base_path = FCPATH.'emissores/v4/';
            $uploaddircert  = $base_path.'nfephp-master/certs/'.$empresa->documento.'/';
            $uploadfilecert = $uploaddircert.$cert;
            $certificadobinario = file_get_contents($uploadfilecert);
        }
        $telefone = $empresa->telefone;

        $telefone = str_replace('(','', $telefone);
        $telefone = str_replace(')','', $telefone);
        $telefone = str_replace('-','', $telefone);
        $telefone = str_replace(' ','', $telefone);
        $telefone = trim($telefone);

        $endereco = $empresa->rua.' '.$empresa->numero.' '.$empresa->bairro.' '.$empresa->cidade.' '.$empresa->estado;
        $endereco = $this->removeAcentos($endereco);

        $dados  = 'idgrupo='.$cod_grupo;
        $dados .= '&iduf='.$this->getUF($empresa->estado, $token);
        $dados .= '&idcidade='.$this->getCidade($empresa->codIBGECidade, $token);
        $dados .= '&cnpj='.$empresa->documento;
        $dados .= '&razaosocial='.$empresa->nomeEmpresa;
        $dados .= '&identificacao='.$empresa->documento;
        $dados .= '&descricao='.$empresa->nomeEmpresa;
        $dados .= '&inscricaoestadual='.$empresa->IE;
        $dados .= '&inscricaomunicipal='.$empresa->insmun;
        $dados .= '&endereco='.$endereco;
        $dados .= '&telefone='.$telefone;

        $plano  = '&nfse[situacao]=0&nfse[tipocontrato]=0';

        $plano .= '&nfe[situacao]=-1&nfe[tipocontrato]=1';
        $plano .= '&nfce[situacao]=-1&nfce[tipocontrato]=1';

        $plano .= '&mdfe[situacao]=-1&mdfe[tipocontrato]=1';
        $plano .= '&cte[situacao]=-1&cte[tipocontrato]=1';
        $plano .= '&cfesat[situacao]=-1&cfesat[tipocontrato]=1';
        $plano .= '&gnre[situacao]=-1&gnre[tipocontrato]=1';

        $plano .= '&situacao=0';//ativa
        $plano .= '&usingCertificadoMatriz=false';
        $plano .= '&certificadobinario='.$certificadobinario;
        $plano .= '&tipocertificado=CURRENT_USER_STORE';
        $plano .= '&senhacertificado='.$empresa->certsenha;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dados.$plano);

        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        curl_close($ch);
        return $this->response;
    }

    function consultaHandlerEmpresa($empresa, $cod_grupo) {

        $token  = $this->geraToken();
        $url = $this->URL_POST_CADASTRAR_EMPRESA.'?token='.$token.'&idgrupo='.$cod_grupo;
        $headers = array(
            "Content-Type:application/x-www-form-urlencoded"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        curl_close($ch);

        foreach (json_decode( "[".$this->response."]" ) as $a) {
            for ($i=0;$i<10000;$i++) {
                if ($a->dados[$i]->identificacao == $empresa->documento) {
                    $handeler = $a->dados[$i]->handle;
                    return $handeler;
                }
            }
        }
    }

    function removeAcentos($texto) {
        $de = array('Á', 'Í', 'Ó', 'Ú', 'É', 'Ä', 'Ï', 'Ö', 'Ü', 'Ë', 'À', 'Ì', 'Ò', 'Ù', 'È', 'Ã', 'Õ', 'Â', 'Î', 'Ô', 'Û', 'Ê', 'á', 'í', 'ó', 'ú', 'é', 'ä', 'ï', 'ö', 'ü', 'ë', 'à', 'ì', 'ò', 'ù', 'è', 'ã', 'õ', 'â', 'î', 'ô', 'û', 'ê', 'Ç', 'ç', ' ');
        $para = array('A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'O', 'A', 'I', 'O', 'U', 'E', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'o', 'a', 'i', 'o', 'u', 'e', 'C', 'c', ' ');
        return preg_replace("/[^a-zA-Z0-9_-]/", " ", str_replace($de, $para, $texto));
    }
}