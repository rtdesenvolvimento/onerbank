<?php

class tecnospeednotafiscalservico_model extends CI_Model
{

    //enviar informacoes ao servidor
    private $URL_PRODUCAO_POST_ENVIA_NFSE           = 'https://managersaas.tecnospeed.com.br:8081/ManagerAPIWeb/nfse/envia';
    private $URL_HOMOLOGACAO_POST_ENVIA_NFSE        = 'https://managersaashom.tecnospeed.com.br:7071/ManagerAPIWeb/nfse/envia';

    //retorna pdf da nota fiscal
    private $URL_PRODUCAO_GET_IMPRIME_NFSE          = 'https://managersaas.tecnospeed.com.br:8081/ManagerAPIWeb/nfse/imprime';
    private $URL_HOMOLOGACAO_GET_IMPRIME_NFSE       = 'https://managersaashom.tecnospeed.com.br:7071/ManagerAPIWeb/nfse/imprime';

    //cancelar nota
    private $URL_PRODUCAO_POST_CANCELA_NFSE         = 'https://managersaas.tecnospeed.com.br:8081/ManagerAPIWeb/nfse/cancela';
    private $URL_HOMOLOGACAO_POST_CANCELA_NFSE      = 'https://managersaashom.tecnospeed.com.br:7071/ManagerAPIWeb/nfse/cancela';

    //descarta nota
    private $URL_PRODUCAO_POST_DESCARTA_NFSE         = 'https://managersaas.tecnospeed.com.br:8081/ManagerAPIWeb/nfse/descarta';
    private $URL_HOMOLOGACAO_POST_DESCARTA_NFSE      = 'https://managersaashom.tecnospeed.com.br:7071/ManagerAPIWeb/nfse/descarta';

    //donwload do xml
    private $URL_PRODUCAO_POST_EXPORTA_NFSE          = 'https://managersaas.tecnospeed.com.br:8081/ManagerAPIWeb/nfse/exportaxml';
    private $URL_HOMOLOGACAO_POST_EXPORTA_NFSE       = 'https://managersaashom.tecnospeed.com.br:7071/ManagerAPIWeb/nfse/exportaxml';

    private $formato          = 'tx2';
    private $padrao           = 'TecnoNFSe';
    private $transacao        = 1;
    private $quantidadeRps    = 1;
    private $versao           = '3.10';
    private $idRps            = 'R1';
    private $tipoRps          = '1';
    private $situacaoNota     = '1';

    //dados do remente
    private $emitente                    = null;
    private $usuario                     = null;
    private $senha                       = null;
    private $grupo                       = null;
    private $cidadeRemetente             = null;
    private $inscricaoMunicipalRemetente = null;
    private $razaoSocialRemetente        = null;
    private $codigoCidadeRemetente       = null;
    private $telefoneRemetente           = null;
    private $DDDTelefoneRemetente        = null;
    private $regimeTributario            = null;
    private $incentivadorCultural        = null;
    private $incentivoFiscal             = null;
    private $tipoTributacao              = null;
    private $naturezaTributacao          = null;
    private $isReterISS                  = null;
    private $ambiente                    = null;

    //respostas do servidor
    private $response = '';

    //dados da nota autorizada
    public $handler;
    public $numeroLote;
    public $numeroNota;
    public $mensagem;
    public $numeroProtocoloCancelamento;

    public $error;

    function __construct()
    {
        parent::__construct();
        $this->emitente           = $this->getEmitente();

        $this->usuario                          = $this->emitente->usuario;
        $this->senha                            = $this->emitente->senha;
        $this->grupo                            = $this->emitente->grupo;
        $this->cidadeRemetente                  = $this->removeAcentos($this->emitente->cidade);
        $this->cnpjRemetente                    = $this->emitente->cnpj;
        $this->inscricaoMunicipalRemetente      = $this->emitente->insmun;
        $this->razaoSocialRemetente             = $this->emitente->nome;
        $this->codigoCidadeRemetente            = $this->emitente->codigoCidade;
        $this->telefoneRemetente                = $this->emitente->telefone;
        $this->DDDTelefoneRemetente             = $this->emitente->dddTelefone;
        $this->regimeTributario                 = $this->emitente->regimeTributario;
        $this->incentivadorCultural             = $this->emitente->incentivadorCultural;
        $this->incentivoFiscal                  = $this->emitente->incentivoFiscal;
        $this->tipoTributacao                   = $this->emitente->tipoTributacao;
        $this->naturezaTributacao               = $this->emitente->naturezaTributacao;
        $this->regimeEspecialTributacao         = $this->emitente->regimeEspecialTributacao;
        $this->isReterISS                       = $this->emitente->isReterISS;
        $this->ambiente                         = $this->emitente->ambiente;
    }

    function gerarTxt($idNFSE) {

        $nfse  = $this->getNotaFiscalById($idNFSE);

        //dados da nfse
        $numeroRps              = $nfse->numeroRps;
        $serieRps               = $nfse->serieRps;

        $dataEmissao            = $nfse->dataEmissao;
        $descricaoServico       = $nfse->descricaoServico;
        $codigoServicoTributado = $nfse->codigoServicoTributado;
        $cnaeServicoTributado   = $nfse->cnaeServicoTributado;
        $numeroLote             = $nfse->numeroLote;

        if ($this->incentivoFiscal == 'S') {
            $this->incentivoFiscal = 1;
        } else {
            $this->incentivoFiscal = 2;
        }


        //cabecalho
        $txt  = $this->formatarText('formato='.$this->formato);
        $txt .= $this->formatarText('padrao='.$this->padrao);
        $txt .= $this->formatarText('Cidade='.$this->cidadeRemetente);
        $txt .= $this->formatarText('INCLUIR');

        $txt .= $this->formatarText('IdLote=');
        $txt .= $this->formatarText('NumeroLote='.$numeroLote);

        //dados do remetente
        $txt .= $this->formatarText('CpfCnpjRemetente='.$this->cnpjRemetente);
        $txt .= $this->formatarText('InscricaoMunicipalRemetente='.$this->inscricaoMunicipalRemetente);
        $txt .= $this->formatarText('RazaoSocialRemetente='.$this->razaoSocialRemetente);
        $txt .= $this->formatarText('QuantidadeRps='.$this->quantidadeRps);
        $txt .= $this->formatarText('CodigoCidadeRemetente='.$this->codigoCidadeRemetente);
        $txt .= $this->formatarText('Transacao='.$this->transacao);
        $txt .= $this->formatarText('DataInicio='.date('Y-m-d'));
        $txt .= $this->formatarText('Versao='.$this->versao);

        //inicio do rps
        $txt .= $this->formatarText('ValorTotalServicos='.$nfse->valorServicos);
        $txt .= $this->formatarText('ValorTotalDeducoes=0');
        $txt .= $this->formatarText('ValorTotalBaseCalculo='.$nfse->baseCalculo);
        $txt .= $this->formatarText('SALVAR');

        //dados da rps
        $txt .= $this->formatarText('INCLUIRRPS');
        $txt .= $this->formatarText('DataEmissao='.$dataEmissao);
        $txt .= $this->formatarText('IdRps='.$this->idRps);
        $txt .= $this->formatarText('NumeroRps='.$numeroRps);
        $txt .= $this->formatarText('SerieRps='.$serieRps);
        $txt .= $this->formatarText('TipoRps='.$this->tipoRps);

        $txt .= $this->formatarText('OptanteSimplesNacional='.$this->regimeTributario);
        $txt .= $this->formatarText('IncentivadorCultural='.$this->incentivadorCultural);
        $txt .= $this->formatarText('IncentivoFiscal='.$this->incentivoFiscal);
        $txt .= $this->formatarText('ExigibilidadeISS='.$this->isReterISS);
        $txt .= $this->formatarText('SituacaoNota='.$this->situacaoNota);
        $txt .= $this->formatarText('TipoTributacao='.$this->tipoTributacao);
        $txt .= $this->formatarText('NaturezaTributacao='.$this->naturezaTributacao);
        $txt .= $this->formatarText('RegimeEspecialTributacao='.$this->regimeEspecialTributacao);

        //totalizadores
        $reterISS = $nfse->reterISS;
        if ($reterISS == 'S') {
            $reterISS = 1;
        } else {
            $reterISS = 2;
        }

        $txt .= $this->formatarText('ValorServicos='.$nfse->valorServico);
        $txt .= $this->formatarText('ValorDeducoes=0');
        $txt .= $this->formatarText('ValorPis='.$nfse->valorPIS);
        $txt .= $this->formatarText('ValorCofins='.$nfse->valorCofins);
        $txt .= $this->formatarText('ValorInss='.$nfse->valorINSS);
        $txt .= $this->formatarText('ValorIr='.$nfse->valorIR);
        $txt .= $this->formatarText('ValorCsll='.$nfse->valorContribuicaoSocial);
        $txt .= $this->formatarText('IssRetido='.$reterISS);
        $txt .= $this->formatarText('ValorIss='.$nfse->valorISS);
        $txt .= $this->formatarText('ValorIssRetido='.$nfse->valorISS);
        $txt .= $this->formatarText('BaseCalculo='.$nfse->baseCalculo);
        $txt .= $this->formatarText('ValorLiquidoNfse='.$nfse->valorNota);
        $txt .= $this->formatarText('DescontoIncondicionado='.$nfse->desconto);
        $txt .= $this->formatarText('DescontoCondicionado=0.00');

        $txt .= $this->formatarText('AliquotaISS='.$nfse->percentualISS);
        $txt .= $this->formatarText('AliquotaPIS='.$nfse->percentualPIS);
        $txt .= $this->formatarText('AliquotaCOFINS='.$nfse->percentualCofins);
        $txt .= $this->formatarText('AliquotaINSS='.$nfse->percentualINSS);
        $txt .= $this->formatarText('AliquotaIR='.$nfse->percentualIR);
        $txt .= $this->formatarText('AliquotaCSLL='.$nfse->percentualContribuicaoSocial);

        //dados do servico
        $txt .= $this->formatarText('CodigoItemListaServico='.$codigoServicoTributado);
        $txt .= $this->formatarText('CodigoCnae='.$cnaeServicoTributado);
        $txt .= $this->formatarText('CodigoTributacaoMunicipio='.$this->codigoCidadeRemetente);
        $txt .= $this->formatarText('DiscriminacaoServico='.$descricaoServico);

        //dados do prestador
        $txt .= $this->formatarText('CodigoCidadePrestacao='.$this->codigoCidadeRemetente);
        $txt .= $this->formatarText('DescricaoCidadePrestacao='.$this->cidadeRemetente);
        $txt .= $this->formatarText('CpfCnpjPrestador='.$this->cnpjRemetente);
        $txt .= $this->formatarText('InscricaoMunicipalPrestador='.$this->inscricaoMunicipalRemetente);
        $txt .= $this->formatarText('RazaoSocialPrestador='.$this->razaoSocialRemetente);
        $txt .= $this->formatarText('DDDPrestador='.$this->DDDTelefoneRemetente);
        $txt .= $this->formatarText('TelefonePrestador='.$this->telefoneRemetente);

        $nomeClienteNota = $this->removeAcentos($nfse->nomeClienteNota);
        $enderecoCliente = $this->removeAcentos($nfse->enderecoCliente);
        $complementoCliente = $this->removeAcentos($nfse->complementoCliente);
        $bairroCliente = $this->removeAcentos($nfse->bairroCliente);
        $cidadeCliente = $this->removeAcentos($nfse->cidadeCliente);

        $cepCliente = $nfse->cepCliente;
        $cepCliente = str_replace('.','', $cepCliente);
        $cepCliente = str_replace('-','', $cepCliente);

        //dados do tomador
        $txt .= $this->formatarText('CpfCnpjTomador='.$nfse->cpfCnpjCliente);
        $txt .= $this->formatarText('RazaoSocialTomador='. $nomeClienteNota);
        $txt .= $this->formatarText('InscricaoMunicipalTomador='.$nfse->inscricaoMunicialCliente);
        $txt .= $this->formatarText('InscricaoEstadualTomador='.$nfse->inscricaoEstadualCliente);
        $txt .= $this->formatarText('TipoLogradouroTomador=');
        $txt .= $this->formatarText('EnderecoTomador='.$enderecoCliente);
        $txt .= $this->formatarText('NumeroTomador='.$nfse->numeroCliente);
        $txt .= $this->formatarText('ComplementoTomador='.$complementoCliente);
        $txt .= $this->formatarText('TipoBairroTomador=');
        $txt .= $this->formatarText('BairroTomador='.$bairroCliente);
        $txt .= $this->formatarText('CodigoCidadeTomador=4115200');//.$nfse->codIBGECidade//TODO
        $txt .= $this->formatarText('DescricaoCidadeTomador='.$cidadeCliente);
        $txt .= $this->formatarText('UfTomador='.$nfse->ufCliente);
        $txt .= $this->formatarText('CepTomador='.$cepCliente);
        $txt .= $this->formatarText('DDDTomador=');
        $txt .= $this->formatarText('TelefoneTomador='.$nfse->telefoneCliente);
        $txt .= $this->formatarText('EmailTomador='.$nfse->emailCliente);

        //rodape
        $txt .= $this->formatarText('PercentualDeduzir=0');
        $txt .= $this->formatarText('QuantidadeServicos=1');
        $txt .= $this->formatarText('ValorUnitarioServico='.$nfse->valorServico);
        $txt .= $this->formatarText('SALVARRPS');

        //echo $txt;
        return urlencode($txt);
    }

    function trasmitir($idNFSE) {

        $grupo      = $this->grupo;
        $cnpj       = $this->cnpjRemetente;
        $nomeCidade = $this->cidadeRemetente;
        $parametros = '?grupo='.$grupo.'&cnpj='.$cnpj.'&nomecidade='.$nomeCidade.'&arquivo='.$this->gerarTxt($idNFSE);

        if ($this->ambiente == 1) {
            $url = $this->URL_PRODUCAO_POST_ENVIA_NFSE . $parametros;
        } else {
            $url = $this->URL_HOMOLOGACAO_POST_ENVIA_NFSE . $parametros;
        }

        $headers = array(
            "Authorization:".$this->getAuthorization(),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        curl_close($ch);

        $this->response = utf8_encode($this->response);
        $resultado = explode(',',$this->response);

        if (strpos($this->response , 'Autorizada') !== false) {
            $this->handler      = $resultado[0];
            $this->numeroLote   = $resultado[1];
            $this->numeroNota   = $resultado[2];
            $this->mensagem     = $resultado[3];
            $this->atualizarDadosAutorizacaoNFSe($idNFSE);
        } else if (count($resultado) > 3) {

            $this->handler      = $resultado[0];
            $this->numeroLote   = $resultado[1];
            $this->numeroNota   = $resultado[2];
            $this->mensagem     = $resultado[3];
            $this->error        = $this->mensagem;

            $this->atualizarRejeicaoAutorizacaoMotivo($idNFSE);
        } else {
            $this->response = str_replace('EXCEPTION,EspdManNFSeJaExisteException,','', $this->response);
            $this->response = str_replace('\delimiter',' ', $this->response);
            $this->error    = $this->response;
            $this->atualizarRejeicaoAutorizacaoMotivo($idNFSE);
        }
    }

    function imprimir($numeroNotaConsulta, $isRetornaURL = 0) {

        $grupo          = $this->grupo;
        $cnpj           = $this->cnpjRemetente;
        $nomeCidade     = $this->cidadeRemetente;

        $parametros =  '?grupo='.$grupo.'&cnpj='.$cnpj.'&numnfse='.$numeroNotaConsulta.'&nomecidade='.$nomeCidade.'&url='.$isRetornaURL;

        if ($this->ambiente == 1) {
            $url = $this->URL_PRODUCAO_GET_IMPRIME_NFSE . $parametros;
        } else {
            $url = $this->URL_HOMOLOGACAO_GET_IMPRIME_NFSE . $parametros;
        }

        header('Content-type: application/pdf');
        //header('Content-Disposition: attachment; filename="nfse_'.$numeroNotaConsulta.'.pdf"');
        $headers = array(
            "Authorization:".$this->getAuthorization(),
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        curl_close($ch);
        echo $this->response;
    }

    function exporta($isRetornaURL = 1) {

        $grupo          = $this->grupo;
        $cnpj           = $this->cnpjRemetente;
        $nomeCidade     = $this->cidadeRemetente;

        $parametros =  '?grupo='.$grupo.'&cnpj='.$cnpj.'&nomecidade='.$nomeCidade.'&url='.$isRetornaURL.'&tipo=AUTORIZACAO';

        if ($this->ambiente == 1) {
            $url = $this->URL_PRODUCAO_POST_EXPORTA_NFSE.$parametros;
        } else {
            $url = $this->URL_HOMOLOGACAO_POST_EXPORTA_NFSE.$parametros;
        }

        $headers = array(
            "Authorization:".$this->getAuthorization(),
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        curl_close($ch);
        echo $this->response;
    }

    function cancelar($numeroNotaConsulta, $idNFSE) {
        $grupo                  = $this->grupo;
        $cnpj                   = $this->cnpjRemetente;
        $nomeCidade             = $this->cidadeRemetente;

        $parametros =  '?grupo='.$grupo.'&cnpj='.$cnpj.'&numnfse='.$numeroNotaConsulta.'&nomecidade='.$nomeCidade;

        if ($this->ambiente == 1) {
            $url = $this->URL_PRODUCAO_POST_CANCELA_NFSE . $parametros;
        } else {
            $url = $this->URL_HOMOLOGACAO_POST_CANCELA_NFSE . $parametros;
        }

        $headers = array(
            "Authorization:".$this->getAuthorization(),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        $this->response = str_replace('EXCEPTION,EspdCheckParamsException,','',$this->response);
        curl_close($ch);

        $resultado = explode(',',$this->response);

        if (strpos($this->response , 'Cancelamento') !== false) {
            $this->handler      = $resultado[0];
            $this->numeroNFSE   = $resultado[1];
            $this->numeroProtocoloCancelamento   = $resultado[2];
            $this->mensagem     = $resultado[3];
            $this->atualizarCancelamento($idNFSE);
        } else {
            $this->error    = $this->response;
            $this->atualizarRejeicaoCancelamentoMotivo($idNFSE);
        }
    }

    private function atualizarCancelamento($id) {
        $data = array(
            'situacao'  => 4
        );
        $this->db->where('idNFSE', $id);
        $this->db->update('nfse', $data);
    }

    private function atualizarRejeicaoCancelamentoMotivo($id) {
        $data = array(
            'rejeicaoMotivo'    => $this->error,
        );
        $this->db->where('idNFSE', $id);
        $this->db->update('nfse', $data);
    }

    private function atualizarDadosAutorizacaoNFSe($id) {
        $data = array(
            'situacao'          => 2,
            'numeroLote'        => $this->numeroLote,
            'numeroNFSe'        => $this->numeroNota ,
            'handleTecnospeed'  => $this->handler,
            'rejeicaoMotivo'    => ''
        );
        $this->db->where('idNFSE', $id);
        $this->db->update('nfse', $data);
    }

    private function atualizarRejeicaoAutorizacaoMotivo($id) {
        $data = array(
            'rejeicaoMotivo'    => $this->error,
            'situacao'          => 3,
            'numeroLote'        => $this->numeroLote,
            'numeroNFSe'        => $this->numeroNota ,
            'handleTecnospeed'  => $this->handler,
        );
        $this->db->where('idNFSE', $id);
        $this->db->update('nfse', $data);
    }

    function descarta($numRPS,$serieRPS) {

        $grupo      = $this->grupo;
        $cnpj       = $this->cnpjRemetente;
        $nomeCidade = $this->cidadeRemetente;

        $parametros =  '?grupo='.$grupo.'&cnpj='.$cnpj.'&nomecidade='.$nomeCidade.'PR'.'&numRPS='.$numRPS.'&serieRPS='.$serieRPS.'&tipoRPS=1';

        if ($this->ambiente == 1) {
            $url = $this->URL_PRODUCAO_POST_DESCARTA_NFSE.$parametros;
        } else {
            $url = $this->URL_HOMOLOGACAO_POST_DESCARTA_NFSE.$parametros;
        }

        $headers = array(
            "Content-Type:application/x-www-form-urlencoded",
            "Authorization:".$this->getAuthorization(),
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->response = curl_exec($ch);
        $this->response = utf8_encode($this->response);
        curl_close($ch);

        if (strpos($this->response , 'OK') !== false) {
            return true;
        }  else {
            $this->error    = $this->response;
            return false;
        }
    }

    function consulta() {

    }

    private function getAuthorization() {
        $usuario    = $this->usuario;
        $senha      = $this->senha;
        return 'Basic '.base64_encode ( $usuario.':'.$senha);
    }

    private function getEmitente()
    {
        $this->db->limit(1);
        $this->db->where('emitente.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('configuracao_tecnospeed', 'configuracao_tecnospeed.idConfiguracaotecnospeed = emitente.configuracao_tecnospeed_id', 'left');
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id', 'left');
        return $this->db->get('emitente')->row();
    }

    private function formatarText($variavel) {
        return $variavel.chr(10);
    }

    private function getNotaFiscalById($id)
    {
        $this->db->where('idNFSE', $id);
        $this->db->limit(1);
        return $this->db->get('nfse')->row();
    }

    function removeAcentos($texto) {
        $de = array('Á', 'Í', 'Ó', 'Ú', 'É', 'Ä', 'Ï', 'Ö', 'Ü', 'Ë', 'À', 'Ì', 'Ò', 'Ù', 'È', 'Ã', 'Õ', 'Â', 'Î', 'Ô', 'Û', 'Ê', 'á', 'í', 'ó', 'ú', 'é', 'ä', 'ï', 'ö', 'ü', 'ë', 'à', 'ì', 'ò', 'ù', 'è', 'ã', 'õ', 'â', 'î', 'ô', 'û', 'ê', 'Ç', 'ç', ' ');
        $para = array('A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'O', 'A', 'I', 'O', 'U', 'E', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'o', 'a', 'i', 'o', 'u', 'e', 'C', 'c', ' ');
        return preg_replace("/[^a-zA-Z0-9_-]/", " ", str_replace($de, $para, $texto));
    }

}