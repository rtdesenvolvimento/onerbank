<?php

class tipo_cobranca_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->from('tipo_cobranca');
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->where('idTipocobranca', $id);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('tipo_cobranca')->row();
    }

    function add($table, $data)
    {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }
}