<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class transferencia_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields.', transferencia.status statusTransferencia, o.nome origem, d.nome destino ');
        $this->db->from($table);
        $this->db->join('filial o', 'o.idFilial = transferencia.filial_id','left');
        $this->db->join('filial d', 'd.idFilial = transferencia.filial_destino_id','left');
        $this->db->limit($perpage, $start);
        $this->db->where($table.'.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idTransferencia', 'desc');
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->select('transferencia.*, usuarios.*, o.nome origem, d.nome destino');
        $this->db->from('transferencia');
        $this->db->join('usuarios', 'usuarios.idUsuarios = transferencia.usuarios_id');
        $this->db->where('transferencia.idTransferencia', $id);
        $this->db->join('filial o', 'o.idFilial = transferencia.filial_id','left');
        $this->db->join('filial d', 'd.idFilial = transferencia.filial_destino_id','left');
        $this->db->where('transferencia.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getProdutos($id = null)
    {
        $this->db->select('itensTransferencia.*, produtos.*');
        $this->db->from('itensTransferencia');
        $this->db->join('produtos', 'produtos.idProdutos = itensTransferencia.produtos_id');
        $this->db->where('transferencia_id', $id);
        $this->db->where('itensTransferencia.filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }


    function add($table, $data, $returnId = false)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }


}
