<?php
class Veiculos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields.',clientes.nomeCliente');
        $this->db->from($table);
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('clientes','clientes.idClientes = veiculos.cliente_id');
        $this->db->limit($perpage,$start);
        $this->db->order_by('idVeiculo','desc');
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
	
	function count($table){
		return $this->db->count_all($table);
    }

    function getById($id){
        $this->db->where('idVeiculo',$id);
        $this->db->limit(1);
        return $this->db->get('veiculos')->row();
    }

    function getManutencoes($id) {
        $this->db->where('veiculo_id', $id);
        return $this->db->get('manutencao_preventiva_veiculo')->result();
    }

    public function getOsByVeiculos($veiculo){
        $this->db->where('veiculo_id',$veiculo);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idOs','desc');
        return $this->db->get('os')->result();
    }
	
	public function getVeiculos($id = null){
         $this->db->from('veiculos');
         $this->db->where('cliente_id',$id);
        return $this->db->get()->result();
    }
	
	function getClienteById($id){
        $this->db->where('idClientes',$id);
        $this->db->limit(1);
        return $this->db->get('clientes')->row();
    }
	
	public function getVendas($id = null){
         $this->db->from('vendas');
         $this->db->where('clientes_id',$id);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }

    function add($table,$data, $returnId = false){

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    function editManutencao($data,$indice, $veiculo_id) {
        $this->db->where('indice',$indice);
        $this->db->where('veiculo_id',$veiculo_id);
        $this->db->update('manutencao_preventiva_veiculo', $data);

        if ($this->db->affected_rows() >= 0)
        {
            return TRUE;
        }
        return FALSE;
    }
}