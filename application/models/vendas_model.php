<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vendas_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields . ', clientes.nomeCliente, clientes.idClientes');
        $this->db->from($table);
        $this->db->limit($perpage, $start);
        $this->db->join('clientes', 'clientes.idClientes = ' . $table . '.clientes_id');
        $this->db->where('vendapdv', 0);
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idVendas', 'desc');
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getVendasPDV($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {

        $this->db->select($fields . ', clientes.nomeCliente, clientes.idClientes');
        $this->db->from($table);
        $this->db->limit($perpage, $start);
        $this->db->join('clientes', 'clientes.idClientes = ' . $table . '.clientes_id');
        $this->db->where($table.'.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('vendapdv', 1);
        $this->db->order_by('idVendas', 'desc');

        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();

        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    function getById($id)
    {
        $this->db->select('vendas.*, clientes.*, usuarios.*');
        $this->db->from('vendas');
        $this->db->join('clientes', 'clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = vendas.usuarios_id');
        $this->db->where('vendas.idVendas', $id);
        $this->db->where('vendas.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getItemVendaById($id)
    {
        $this->db->from('itens_de_vendas');
        $this->db->join('produtos', 'produtos.idProdutos = itens_de_vendas.produtos_id');
        $this->db->where('idItens', $id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getVendaAberta() {
        $this->db->select('vendas.*, clientes.*, usuarios.*');
        $this->db->from('vendas');
        $this->db->join('clientes', 'clientes.idClientes = vendas.clientes_id');
        $this->db->join('usuarios', 'usuarios.idUsuarios = vendas.usuarios_id');
        $this->db->where('vendas.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('vendas.usuarios_id', $this->session->userdata('id'));
        $this->db->where('vendas.status', 0);
        $this->db->where('vendas.vendapdv', 1);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function getClienteVendaById($id)
    {
        $this->db->select('vendas.*, clientes.*');
        $this->db->from('vendas');
        $this->db->join('clientes', 'clientes.idClientes = vendas.clientes_id');
        $this->db->where('vendas.idVendas', $id);
        $this->db->where('vendas.filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function getProdutos($id = null,$grupo_id=NULL)
    {
        $this->db->select('itens_de_vendas.*, produtos.*');
        $this->db->from('itens_de_vendas');
        $this->db->join('produtos', 'produtos.idProdutos = itens_de_vendas.produtos_id');
        $this->db->where('vendas_id', $id);
        $this->db->where('itens_de_vendas.filial_id', $this->session->userdata('filial_id'));

        if($grupo_id) {
            $this->db->where('produtos.grupoProduto_id', $grupo_id);
        }

        $this->db->order_by('itens_de_vendas.idItens', 'desc');
        return $this->db->get()->result();
    }

    function add($table, $data, $returnId = false)
    {

        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);

        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) return $this->db->insert_id($table);
            return TRUE;
        }
        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

    public function autoCompleteProduto($q)
    {

        $this->db->select('*');
        $this->db->limit(50);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->like('descricao', $q);
        $query = $this->db->get('produtos');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['descricao'] . ' | Preço: R$ ' . $row['precoVenda'] . ' | Estoque: ' . $row['estoque'], 'estoque' => $row['estoque'], 'id' => $row['idProdutos'], 'preco' => $row['precoVenda'], 'custo' => $row['precoCompra']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteCliente($q)
    {

        $this->db->select('clientes.nomeCliente, clientes.telefone, clientes.idClientes');
        $this->db->limit(50);
        $this->db->like('nomeCliente', $q);
        $this->db->or_like('telefone', $q);
        $this->db->or_like('celular', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('modelo', $q);
        $this->db->or_like('marca', $q);
        $this->db->or_like('placa', $q);
        $this->db->or_like('tipoVeiculo', $q);
        $this->db->or_like('cor', $q);
        $this->db->or_like('ano', $q);
        $this->db->where('clientes.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('veiculos', 'clientes.idClientes = veiculos.cliente_id', 'left');
        $this->db->distinct();
        $query = $this->db->get('clientes');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nomeCliente'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idClientes']);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteUsuario($q)
    {

        $this->db->select('*');
        $this->db->limit(50);
        $this->db->like('nome', $q);
        $this->db->where('situacao', 1);
        $query = $this->db->get('usuarios');
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = array('label' => $row['nome'] . ' | Telefone: ' . $row['telefone'], 'id' => $row['idUsuarios']);
            }
            echo json_encode($row_set);
        }
    }

    function getUsuarioById($id){
        $this->db->where('idUsuarios',$id);
        $this->db->limit(1);
        return $this->db->get('usuarios')->row();
    }

    function getFilialById($id)
    {
        $this->db->where('idFilial', $id);
        $this->db->limit(1);
        return $this->db->get('filial')->row();
    }

    function getVendaItemByVenda($item_venda_id)
    {
        $this->db->where('idItens', $item_venda_id);
        $this->db->limit(1);
        return $this->db->get('itens_de_vendas')->row();
    }

    function faturarVenda($vendas_id, $desconto) {
        $this->db->set('faturado', 1);
        $this->db->set('status', 1);
        $this->db->set('desconto', $desconto);
        $this->db->where('idVendas', $vendas_id);
        $this->db->update('vendas');
    }

    function porVendaEmEspera($vendas_id, $referencia_espera) {
        $this->db->set('espera', 1);
        $this->db->set('referencia_espera', $referencia_espera);
        $this->db->where('idVendas', $vendas_id);
        $this->db->update('vendas');
    }

    function finalizarVendaEmEspera($vendas_id) {
        $this->db->set('espera', 0);
        $this->db->where('idVendas', $vendas_id);
        $this->db->update('vendas');
    }

    function cancelar($vendas_id) {
        $this->db->set('espera', 1);
        $this->db->set('faturado', 0);
        $this->db->set('status',  2);
        $this->db->where('idVendas', $vendas_id);
        $this->db->update('vendas');
    }

    public function getAllPedidosEmEspera()
    {
        $this->db->select('vendas.*, clientes.nomeCliente ');
        $this->db->from('vendas');
        $this->db->join('clientes', 'clientes.idClientes = vendas.clientes_id');
        $this->db->where('vendas.filial_id', $this->session->userdata('filial_id'));
        $this->db->where('vendas.espera', 1);
        $this->db->order_by('dataVenda', 'desc');
        $this->db->order_by('idVendas', 'desc');
        return $this->db->get()->result();
    }

}

/* End of file vendas_model.php */
/* Location: ./application/models/vendas_model.php */