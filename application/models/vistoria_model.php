<?php

class vistoria_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get($table, $fields, $where = '', $perpage = 0, $start = 0, $one = false, $array = 'array')
    {
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->order_by('idVistoria', 'desc');
        $this->db->limit($perpage, $start);
        if ($where) {
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = !$one ? $query->result() : $query->row();
        return $result;
    }

    public function getSetorAll()
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->from('vistoria');
        return $this->db->get()->result();
    }

    function getById($id)
    {
        $this->db->select('vistoria.*, veiculos.situacao, clientes.inativo, clientes.nomeCliente, clientes.observacaoInativo ');
        $this->db->where('vistoria.filial_id', $this->session->userdata('filial_id'));
        $this->db->join('clientes', 'clientes.idClientes = vistoria.clientes_id');
        $this->db->join('veiculos','veiculos.idVeiculo = vistoria.veiculo_id','left');
        $this->db->where('idVistoria', $id);
        $this->db->limit(1);
        return $this->db->get('vistoria')->row();
    }

    function getVistoriapneuByIdTipoVeiculopenuVistoria($idTipoveiculopneu, $idVistoria)
    {
        $this->db->where('idTipoveiculopneu', $idTipoveiculopneu);
        $this->db->where('idVistoria', $idVistoria);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get('vistoriapneu')->row();
    }

    function getAllAvariasVistoria($idVistoria)
    {
        $this->db->where('idVistoria', $idVistoria);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('vistoriaavaria')->result();
    }

    function getAllAvariasVistoriaVisaoEquipamento($idVistoria, $visaoautomovel_id,$idEquipamentoveiculo)
    {
        $this->db->where('idVistoria', $idVistoria);
        $this->db->where('visaoautomovel_id', $visaoautomovel_id);
        $this->db->where('equipamentoveiculo_id', $idEquipamentoveiculo);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->limit(1);
        return $this->db->get('vistoriaavaria')->row();
    }

    function getVistoriapneuByVistoria($idVistoria)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        $this->db->where('idVistoria', $idVistoria);
        return $this->db->get('vistoriapneu')->result();
    }

    function add($table, $data, $returnId = false)
    {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            if ($returnId == true) {
                return $this->db->insert_id($table);
            }
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getVeiculos($idCliente = null)
    {
        $this->db->from('veiculos');
        $this->db->where('cliente_id', $idCliente);
        return $this->db->get()->result();
    }

    public function getAnexosAvarias($idVistoria = null,$idVistoriaavaria)
    {
        $this->db->from('documentoavaria');
        $this->db->where('idVistoria', $idVistoria);
        $this->db->where('idVistoriaavaria', $idVistoriaavaria);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }

    function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }
}