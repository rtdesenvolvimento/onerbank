<?php

class zoop_model extends CI_Model
{

    const INSTRUCAO_EMISSAO_ONERBANK = 'Boleto emitido via plataforma Onerbank - Zoop Brasil';

    var $API = '';
    function __construct()
    {
        parent::__construct();
    }

    function getMarketplaces() {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = "https://api.zoop.ws/v1/marketplaces/".$marketplace_id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function getPlans() {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/plans';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function getSellers() {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/sellers';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPTf_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function getSellersById() {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;
        $on_behalf_of   = $emitente->on_behalf_of;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/sellers/'.$on_behalf_of;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function postCadTransactionsBoleto($fatura, $cliente, $carencia, $descricao) {

        $FORMA_PAGAMENTO = 'boleto';

        $emitente       = $this->getEmitente();
        $configuracao   = $this->getConfiguracaoFinanceira();
        $multa          = $configuracao->multaAtrasoBoleto;
        $juros          = $configuracao->jurosAtrasoBoleto;
        $diasPosVencimento = $configuracao->prazoMaximoPagamentoAposVencimento;

        $marketplace_id = $emitente->marketplace_id;
        $on_behalf_of   = $emitente->on_behalf_of;

        if ($carencia == NULL) {
            if ($diasPosVencimento == null) $carencia = 3;
            else $carencia = $diasPosVencimento;
        }

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/transactions';
        $valorFatura = number_format($fatura->valorPagar,2,'.','')  ;

        $limenteDePagamento = date('Y-m-d', strtotime("+" . $carencia . " days", strtotime($fatura->dtVencimento)));
        $descricao = zoop_model::INSTRUCAO_EMISSAO_ONERBANK.' Não receber após '. date('d/m/Y', strtotime($limenteDePagamento) ).' '.$descricao;

        if ($multa > 0  && $juros > 0) {
            $payload =
                array(
                    "amount" => ($valorFatura * 100),
                    "currency" => 'BRL',
                    "on_behalf_of" => $on_behalf_of,
                    "customer" => $cliente->client_integration_id,
                    "description" => $descricao,
                    "payment_type" => $FORMA_PAGAMENTO,
                    "logo" => $emitente->url_logo,
                    "reference_id" => $fatura->idFatura,
                    "payment_method" => array(
                        "expiration_date" => $fatura->dtVencimento,
                        "payment_limit_date" => $limenteDePagamento,
                        "body_instructions" => $descricao,
                        "billing_instructions"=> array(
                            "late_fee" => array(
                                "mode"=> "PERCENTAGE",
                                "percentage"=> $multa
                            ),
                            "interest" => array(
                                "mode"=> "DAILY_PERCENTAGE",
                                "percentage"=> $juros
                            )
                        )
                    )
                );
        } else  if ($multa > 0 ) {
            $payload =
                array(
                    "amount" => ($valorFatura * 100),
                    "currency" => 'BRL',
                    "on_behalf_of" => $on_behalf_of,
                    "customer" => $cliente->client_integration_id,
                    "description" => $descricao,
                    "payment_type" => $FORMA_PAGAMENTO,
                    "logo" => $emitente->url_logo,
                    "reference_id" => $fatura->idFatura,
                    "payment_method" => array(
                        "expiration_date" => $fatura->dtVencimento,
                        "payment_limit_date" => $limenteDePagamento,
                        "body_instructions" => $descricao,
                        "billing_instructions"=> array(
                                "late_fee" => array(
                                    "mode"=> "PERCENTAGE",
                                    "percentage"=> $multa
                                )
                        )
                    )
                );
        } else if ($juros > 0) {
            $payload =
                array(
                    "amount" => ($valorFatura * 100),
                    "currency" => 'BRL',
                    "on_behalf_of" => $on_behalf_of,
                    "customer" => $cliente->client_integration_id,
                    "description" => $descricao,
                    "payment_type" => $FORMA_PAGAMENTO,
                    "logo" => $emitente->url_logo,
                    "reference_id" => $fatura->idFatura,
                    "payment_method" => array(
                        "expiration_date" => $fatura->dtVencimento,
                        "payment_limit_date" => $limenteDePagamento,
                        "body_instructions" => $descricao,
                        "billing_instructions"=> array(
                            "interest" => array(
                                "mode"=> "DAILY_PERCENTAGE",
                                "percentage"=> $juros
                            )
                        )
                    )
                );
        } else {
            $payload =
                array(
                    "amount" => ($valorFatura * 100),
                    "currency" => 'BRL',
                    "on_behalf_of" => $on_behalf_of,
                    "customer" => $cliente->client_integration_id,
                    "description" => $descricao,
                    "payment_type" => $FORMA_PAGAMENTO,
                    "logo" => $emitente->url_logo,
                    "reference_id" => $fatura->idFatura,
                    "payment_method" => array(
                        "expiration_date" => $fatura->dtVencimento,
                        "payment_limit_date" => date('Y-m-d', strtotime("+" . $carencia . " days", strtotime($fatura->dtVencimento))),
                        "body_instructions" => $descricao,
                    )
                );
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: */*',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);

        $this->gravarLog('[GERACAO DE NOVA TRANSAÇÃO]'.$response, 'transacao');

        return $response;
    }

    function getBuyers() {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/buyers';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function getBuyersById($buyer_id) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/buyers/'.$buyer_id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function postCadBuyers($cliente) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/buyers';

        $documento = $cliente->documento;
        $documento = str_replace('-','', $documento);
        $documento = str_replace('/','', $documento);
        $documento = str_replace('.','', $documento);
        $documento = str_replace('.','', $documento);

        $cep = $cliente->cep;
        $cep = str_replace('.','', $cep);
        $cep = str_replace('-','', $cep);

        $tBody =  '{"first_name":"'.$cliente->nomeCliente.'","last_name":"","email":"'.$cliente->email.'","phone_number":"'.$cliente->celular.'","taxpayer_id":"'.$documento.'","address":{"line1":"'.$cliente->rua.'","line2":"'.$cliente->numero.' '.$cliente->complemento.'","city":"'.$cliente->cidade.'","neighborhood":"'.$cliente->bairro.'","state":"'.$cliente->estado.'","postal_code":"'.$cep.'","country_code":"BR"}}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  ($tBody));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: application/json',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);

        $this->gravarLog('[GRAVAR NOVO CLIENTE]'.$response,'cad');

        return $response;
    }

    function gerarTokenCartao() {
        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/cards/tokens';

        $dadosDoCartao =
            array(
                "holder_name"=> 'Andre J Velho',
                "expiration_month" => '04',
                "expiration_year" => '2025',
                "card_number" => '4539003370725497',//TODO TESTE
                "security_code" => '123',
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dadosDoCartao));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: application/json',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function associarTokenDoCartoAoCliente($tokenGerado, $cliente) {
        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;
        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/cards/tokens';

        $dadosDaAssociacao =
            array(
                "token"     => $tokenGerado,
                "customer"  =>  $cliente->client_integration_id
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dadosDaAssociacao));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: application/json',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function getTaxas()
    {
        $this->db->where('idTaxas', 1);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('taxas')->row();
    }


    function buscarValorDaParcelaComTaxasParcelada($taxas, $numeroParcela, $valorParcela) {

        if ($numeroParcela==1) {
            $taxaParcela = ($taxas->taxaParcelamento1x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==2) {
            $taxaParcela = ($taxas->taxaParcelamento2x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==3) {
            $taxaParcela = ($taxas->taxaParcelamento3x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==4) {
            $taxaParcela = ($taxas->taxaParcelamento4x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==5) {
            $taxaParcela = ($taxas->taxaParcelamento5x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==6) {
            $taxaParcela = ($taxas->taxaParcelamento6x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==7) {
            $taxaParcela = ($taxas->taxaParcelamento7x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==8) {
            $taxaParcela = ($taxas->taxaParcelamento8x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==9) {
            $taxaParcela = ($taxas->taxaParcelamento9x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==10) {
            $taxaParcela = ($taxas->taxaParcelamento10x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==11) {
            $taxaParcela = ($taxas->taxaParcelamento11x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        if ($numeroParcela==12) {
            $taxaParcela = ($taxas->taxaParcelamento12x/100);
            $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
        }

        return $valorParcela;
    }

    function calcularValorPagamentoComTaxas($valor, $modo_parcelamento, $parcelas) {
        $taxas = $this->getTaxas();

        $tarifaFixa = $taxas->tarifaFixa;
        $taxaIntermediacao = $taxas->taxaIntermediacao;

        if ($modo_parcelamento == 'with_interest') {

            if ($taxaIntermediacao > 0) $taxaIntermediacao = $taxaIntermediacao/100;

            $valor = $valor + ($valor * $taxaIntermediacao);
            $valor = $valor + $tarifaFixa;

            if ($parcelas >= 1) {
                $valorParcela = $valor/$parcelas;
                $valor = $this->buscarValorDaParcelaComTaxasParcelada($taxas, $parcelas, $valorParcela)*$parcelas;
            }
        }

        return $valor;
    }

    function getParcelaById($id){
        $this->db->where('idParcela',$id);
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('parcela')->row();
    }

    function realizarTransacaoComCartaoCreditoDiretaNaoPresente($linkPagamento, $card, $parcelas) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;
        $on_behalf_of   = $emitente->on_behalf_of;
        $descricaoEstabelecimento = $emitente->nome;
        $modoParcelamento = $linkPagamento->modo_parcelamento;

        if ($linkPagamento->parcela_id != null)  $valor = $this->getParcelaById($linkPagamento->parcela_id)->valorPagar;
        else  $valor = $this->calcularValorPagamentoComTaxas($linkPagamento->valor, $modoParcelamento, $parcelas);

        $produto = $linkPagamento->descricao;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/transactions';
        $valorFatura = number_format($valor,2,'.','')  ;

        $payload =
            array(
                "currency" => 'BRL',
                "payment_type" => 'credit',
                "amount"=> ($valorFatura*100),
                "description" => $produto,
                "reference_id" => $linkPagamento->idLinkPagamento,
                "statement_descriptor"  => $descricaoEstabelecimento,
                "on_behalf_of" => $on_behalf_of,
                "source" => array(
                    "usage" => 'single_use',
                    "currency" => 'BRL',
                    "type" => 'card',
                    "amount" =>  ($valorFatura*100),
                    "card" => array(
                        "card_number" => $card['card_number'],
                        "holder_name" => $card['holder_name'],
                        "expiration_month" => $card['expiration_month'],
                        "expiration_year" => $card['expiration_year'],
                        "security_code" => $card['security_code'],
                    ),
                ),
                "installment_plan" => array(
                    "mode" => $modoParcelamento,
                    "number_installments" => $parcelas
                )
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: */*',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function estornarUmaTransacaoDeUmCartaoNaoPresente($transaction_id, $lancamento) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;
        $on_behalf_of   = $emitente->on_behalf_of;
        $valor = number_format($lancamento->valor,2,'.','')  ;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/transactions/'.$transaction_id.'/void';

        $payload =
            array(
                "amount"=> ($valor*100),
                "on_behalf_of" => $on_behalf_of,
                "payment_type" => 'credit',
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: */*',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function realizarTransacaoCartaoCredito($fatura, $cliente, $descricao, $tokenGerado) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;
        $on_behalf_of   = $emitente->on_behalf_of;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/transactions';
        $valorFatura = number_format($fatura->valorPagar,2,'.','')  ;

        $payload =
            array(
                "amount"=> ($valorFatura*100),
                "currency" => 'BRL',
                "description" => $descricao,
                "on_behalf_of" => $on_behalf_of,
                "payment_type" => 'credit',
                "customer" => $cliente->client_integration_id,
                "token"     => $tokenGerado,
                "installment_plan" => array(
                    "mode" => 'interest_free',
                    "number_installments" => '1'
                )
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: */*',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function putGetCadBuyers($cliente) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/buyers/'.$cliente->client_integration_id;

        $documento = $cliente->documento;
        $documento = str_replace('-','', $documento);
        $documento = str_replace('/','', $documento);
        $documento = str_replace('.','', $documento);
        $documento = str_replace('.','', $documento);

        $cep = $cliente->cep;
        $cep = str_replace('.','', $cep);
        $cep = str_replace('-','', $cep);

        $tBody =  '{"first_name":"'.$cliente->nomeCliente.'","last_name":"","email":"'.$cliente->email.'","phone_number":"'.$cliente->celular.'","taxpayer_id":"'.$documento.'","address":{"line1":"'.$cliente->rua.'","line2":"'.$cliente->numero.' '.$cliente->complemento.'","city":"'.$cliente->cidade.'","neighborhood":"'.$cliente->bairro.'","state":"'.$cliente->estado.'","postal_code":"'.$cep.'","country_code":"BR"}}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $tBody);
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: application/json',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);

        $this->gravarLog('[ATUALIZAR DADOS CLIENTE]'.$response, 'cad');

        return $response;
    }

    function atualizarCadastroEstabelecimento() {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/sellers/businesses/'.$emitente->on_behalf_of;

        $cep = $emitente->cep;
        $cep = str_replace('.','', $cep);
        $cep = str_replace('-','', $cep);

        $tBody =  '{"type":"business","business_address":{"line1":"'.$emitente->rua.'","line2":"'.$emitente->numero.' '.$emitente->complemento.'","city":"'.$emitente->cidade.'","neighborhood":"'.$emitente->bairro.'","state":"'.$emitente->uf.'","postal_code":"'.$cep.'","country_code":"BR"}}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $tBody);
        curl_setopt($ch, CURLOPT_URL, $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: application/json',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function getBoletoById($boleto_id) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/boletos/'.$boleto_id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: */*'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$this->getAuthorization()));

        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }

    function getTransactionsById($transaction_id) {

        $emitente       = $this->getEmitente();
        $marketplace_id = $emitente->marketplace_id;

        $url = 'https://api.zoop.ws/v1/marketplaces/'.$marketplace_id.'/transactions/'.$transaction_id;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accept: */*',
                'Authorization: '.$this->getAuthorization()
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function getAuthorization() {
        $emitente = $this->getEmitente();
        return 'Basic '.base64_encode ( $emitente->publishable_key).'Og==';
    }

    function getEmitente()
    {
        $this->db->limit(1);
        $this->db->join('configuracao', 'configuracao.idConfiguracao = emitente.configuracao_id', 'left');
        return $this->db->get('emitente')->row();
    }

    function getConfiguracaoFinanceira() {
        $this->db->limit(1);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get('configuracao')->row();
    }

    function gravarLog($texto, $tipo) {

        $emitente = $this->getEmitente();

        if ($texto == '') return;

        $dataHoje = date('Y-m-d');
        $destino = './assets/log/'.$emitente->cnpj.'/'.$dataHoje;

        $dataHojePtbr = date('d-m-Y');
        $hora = date('hi');

        if (!is_dir($destino)) mkdir($destino, 0777, TRUE);

        $arquivo = fopen($destino.'/'.$tipo.'_'.$dataHojePtbr.'-'.$hora.'.txt','a');
        if ($arquivo == false) die('Não foi possível criar o arquivo.');

        fwrite($arquivo, $texto);
        fclose($arquivo);
    }
}