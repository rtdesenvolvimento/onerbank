<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="AUTOR" content="Resultatec" />
    <title>Carnê Bancário</title>

    <style type="text/css">
        @charset "utf-8";
        /* CSS Document */
        #boleto_parceiro {
            height: 350px;
            width: 666px;
            font-family: Arial, Helvetica, sans-serif;
            margin-bottom: 15px;
            border-bottom-width: 1px;
            border-bottom-style: dashed;
            border-bottom-color: #000000;
        }
        .am {
            font-size: 9px;
            color: #333333;
            height: 10px;
            font-weight: bold;
            margin-bottom: 2px;
            text-align: center;
            width: 320px;
            border-top-width: 1px;
            border-right-width: 2px;
            border-left-width: 2px;
            border-top-style: solid;
            border-right-style: solid;
            border-left-style: solid;
            border-top-color: #000000;
            border-right-color: #000000;
            border-left-color: #000000;
        }
        #boleto{
            height: 416px;
            width: 666px;
            color: #000000;
            font-family: Arial, Helvetica, sans-serif;
        }

        #tb_logo {
            height: 40px;
            width: 666px;
            border-bottom-width: 2px;
            border-bottom-style: solid;
            border-bottom-color: #000000;
        }
        #tb_logo #td_banco {
            height: 22px;
            width: 53px;
            border-right-width: 2px;
            border-left-width: 2px;
            border-right-style: solid;
            border-left-style: solid;
            border-right-color: #000000;
            border-left-color: #000000;
            font-size: 15px;
            font-weight: bold;
            text-align: center;
        }
        .ld {font: bold 15px Arial; color: #000000}
        .td_7_sb {
            height: 26px;
            width: 7px;
        }
        .td_7_cb {
            width: 7px;
            border-left-width: 1px;
            border-left-style: solid;
            border-left-color: #000000;
            height: 26px;
        }
        .td_2 {
            width: 2px;
        }
        .tabelas td{
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: #000000;
        }
        .direito {
            width: 178px;
        }
        .titulo {
            font-size: 9px;
            color: #333333;
            height: 10px;
            font-weight: bold;
            margin-bottom: 2px;
        }
        .var {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            height: 13px;
        }
        .direito .var{
            text-align: right;
        }

        table.bordasimples {
            border-collapse: collapse;
        }

        table.bordasimples tr td {
            border:2px solid #FFFFFF;
            background-color: #CCCCCC;
        }
    </style>

    <?php

    foreach ($parcelas as $parcela) {

        if ($parcela->status == 'cancelada') continue;

        $lancamento = $this->financeiro_model->getById($parcela->lancamentos_id);
        $boleto = $this->financeiro_model->getBoletoByParcela($parcela->idParcela);

        if (count($boleto) < 1) continue;

        $fatura = $this->financeiro_model->getFaturaById($boleto->fatura_id);
        $cliente = $this->clientes_model->getById($lancamento->clientes_id);

        if ($boleto->bank_code != 109) exit();

        $agencia = '8933';
        $conta = '13392';
        $carteira = '109';

        $id_cliente = "1";
        $codigo = '341-7';
        $agencia_codigo = '8933/13392-1';
        $especie_doc = 'DM';
        $aceite = 'N';
        $especie = 'R$';
        $quantidade = '';
        $valor_unitario = '';

        $cedente = $emitente->nome.' - Zoop Brasil';
        $data_vencimento = $this->site_model->formatarData($parcela->dtVencimento);
        $data_documento =  $this->site_model->formatarData($parcela->dtVencimento);
        $data_processamento = $this->site_model->formatarData($parcela->dtVencimento);

        $linha_digitavel = $this->site_model->formatarBarCodeBoleto($boleto->barcode);

        $numero_documento = str_pad($boleto->document_number, 8, '0', STR_PAD_LEFT);
        $cofigo_verificador_nosso_numero = modulo_10($agencia.$conta.$carteira.$numero_documento);

        $nosso_numero = $boleto->bank_code.'/'.$numero_documento.'-'.$cofigo_verificador_nosso_numero;

        $valor_boleto =  $this->site_model->formatarValorMonetario($fatura->valorPagar);

        $multa = $boleto->multa;
        $juros = $boleto->juros;

        if ($multa > 0) $instrucoes1 = 'Multa de '.$multa.'% após o dia '.$this->site_model->formatarData($parcela->dtVencimento).'</br>';
        if ($juros > 0) $instrucoes1 .= 'Após '.$this->site_model->formatarData($parcela->dtVencimento).', cobrar Juros de '.$juros.'% ao dia</br>';

        $instrucoes2 = $boleto->description;

        $sacado1 = $cliente->nomeCliente;

        $sacado = $cliente->nomeCliente.' CPF/CNPJ: '.$cliente->documento;
        $endereco1 =  $cliente->rua.' '.$cliente->numero.', '.$cliente->bairro;
        $endereco2 = 'CEP: '.$cliente->cep.' '.$cliente->cidade.'-'.$cliente->estado;

        $codigo_barras = $boleto->barcode;
    ?>
<body onload="window.print();">
<br/><br/>
<table>
    <tr><td valign="top">
            <table class="tabelas" style="width:195px; border-left:solid; border-left-width:2px; border-left-color:#000000;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width:150px;"><img src="<?php echo base_url() ?>assets/images/boleto/logoitau.jpg" alt="Banco Bradesco" width="100" height="30" title="caixa" /></td>
                    <td id="td_banco"><b><?php echo $codigo; ?></b></td>
                </tr>
            </table>
            <table class="tabelas" style="width:195px; border-left:solid; border-left-width:2px; border-left-color:#000000;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">Pagador</div>
                        <div class="var" style="font-size: 6px;"><?php echo $sacado1; ?></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">Número do Documento</div>
                        <div class="var"><?php echo $numero_documento; ?></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">Data Vencimento</div>
                        <div class="var"><?php echo $data_vencimento; ?></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">Ag / Cód Cedente</div>
                        <div class="var"><?php echo $agencia_codigo; ?></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">Nosso Número</div>
                        <div class="var"><?php echo $nosso_numero; ?></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">Valor Documento</div>
                        <div class="var"><?php echo $valor_boleto; ?></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">(-) Desconto / Abatimentos</div>
                        <div class="var"></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">(+) Mora / Multa
                        </div>
                        <div class="var"></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">(+) Outros acréscimos
                        </div>
                        <div class="var"></div></td>
                </tr>
                <tr>
                    <td class="td_7_sb" style="width:15px;"> </td>
                    <td style="width:103px;"><div class="titulo">(=) Valor cobrado
                        </div>
                        <div class="var"></div></td>
                </tr>
            </table>
        </td><td>
            <table border="0" cellpadding="0" cellspacing="0" id="tb_logo">
                <tr>
                    <td rowspan="2" valign="bottom" style="width:150px;"><img src="<?php echo base_url() ?>assets/images/boleto/logoitau.jpg" alt="Banco Bradesco" width="150" height="40" title="caixa" /></td>
                    <td align="center" valign="bottom" style="font-size: 9px; border:none;">Banco</td>
                    <td rowspan="2" align="right" valign="bottom" style="width:6px;"></td>
                    <td rowspan="2" align="right" valign="bottom" style="font-size: 15px; font-weight:bold; width:445px;"><?php echo $linha_digitavel; ?></td>
                    <td rowspan="2" align="right" valign="bottom" style="width:2px;"></td>
                </tr>
                <tr>
                    <td id="td_banco"><?php echo $codigo; ?></td>
                </tr>
            </table>
            <table class="tabelas" style="width:666px; border-left:solid; border-left-width:2px; border-left-color:#000000;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="td_7_sb"> </td>
                    <td style="width: 468px;"><div class="titulo">Local do Pagamento</div>
                        <div class="var">ATÉ O VENCIMENTO PAGUE EM QUALQUER BANCO OU CORRESPONDENTE NÃO BANCÁRIO.</div></td>
                    <td class="td_7_cb"> </td>
                    <td class="direito"><div class="titulo">Vencimento</div>
                        <div class="var"><?php echo $data_vencimento; ?></div></td>
                    <td class="td_2"> </td>
                </tr>
                <tr>
                    <td class="td_7_sb"> </td>
                    <td><div class="titulo">Beneficiário/CPF/CNPJ</div>
                        <div class="var"><span class="cp"><?php echo $cedente; ?></span></div></td>
                    <td class="td_7_cb"> </td>
                    <td class="direito"><div class="titulo">Ag&ecirc;ncia / C&oacute;digo do Cedente</div>
                        <div class="var"><?php echo $agencia_codigo; ?></div></td>
                    <td> </td>
                </tr>
            </table>
            <table class="tabelas" style="width:666px; border-left:solid; border-left-width:2px; border-left-color:#000000;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="td_7_sb"> </td>
                    <td style="width:103px;"><div class="titulo">Data  Documento</div>
                        <div class="var"><?php echo $data_documento; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:133px;"><div class="titulo">N&uacute;mero Documento</div>
                        <div class="var"><?php echo $numero_documento; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:62px;"><div class="titulo">Esp&eacute;cie Doc.</div>
                        <div class="var"><?php echo $especie_doc; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:34px;"><div class="titulo">Aceite</div>
                        <div class="var"><?php echo $aceite ;?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:103px;"><div class="titulo">Data de Processamento</div>
                        <div class="var"><?php echo $data_processamento; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td class="direito"><div class="titulo">Nosso N&uacute;mero</div>
                        <div class="var"><?php echo $nosso_numero; ?></div></td>
                    <td class="td_2"> </td>
                </tr>
            </table>
            <table class="tabelas" style="width:666px; border-left:solid; border-left-width:2px; border-left-color:#000000;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td_7_sb"> </td>
                    <td style="width:118px;"><div class="titulo">Uso Banco</div>
                        <div class="var"> </div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:55px;"><div class="titulo">Carteira</div>
                        <div class="var"><?php echo $carteira; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:55px;"><div class="titulo">Esp. Moeda</div>
                        <div class="var"><?php echo $especie; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:104px;"><div class="titulo">Qtde Moeda</div>
                        <div class="var"><?php echo $quantidade; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td style="width:103px;"><div class="titulo">Valor</div>
                        <div class="var"><?php echo $valor_unitario; ?></div></td>
                    <td class="td_7_cb"> </td>
                    <td class="direito"><div class="titulo">Valor do Documento</div>
                        <div class="var"><?php echo $valor_boleto; ?></div></td>
                    <td class="td_2"> </td>
                </tr>
            </table>
            <table class="tabelas" style="width:666px; border-left:solid; border-left-width:2px; border-left-color:#000000;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td rowspan="5" class="td_7_sb"></td>
                    <td rowspan="5" valign="top"><div class="titulo" style="margin-bottom:18px;">Informações de Responsabilidade do Beneficiário</div>
                        <div class="var"><?php echo $instrucoes1; ?><br />
                            <?php echo $instrucoes2; ?></div></td>
                    <td class="td_7_cb"></td>
                    <td class="direito"><div class="titulo">(+) Multa / Mora</div>
                        <div class="var"> </div></td>
                    <td class="td_2"></td>
                </tr>
                <tr>
                    <td class="td_7_cb"></td>
                    <td class="direito"><div class="titulo">(+) Outros Acr&eacute;scimos</div>
                        <div class="var"> </div></td>
                    <td class="td_2"></td>
                </tr>
                <tr>
                    <td class="td_7_cb"></td>
                    <td class="direito" valign="top"><div class="titulo">(=) Valor Cobrado</div>
                        <div class="var"> </div></td>
                    <td class="td_2"></td>
                </tr>
            </table>
            <table width="528" height="38" border="0" cellpadding="0" cellspacing="0" class="tabelas" style="width:666px; height:45px; border-left:solid; border-left-width:2px; border-left-color:#000000;">
                <tr>
                    <td width="7" height="38" class="td_7_sb"></td>
                    <td width="570" valign="top"><div class="titulo">Pagador/CPF/CNPJ/Endereço</div>
                        <div class="var" style="margin-bottom:2px; height:auto"><?php echo $sacado; ?><br><?php echo $endereco1; ?> / <?php echo $endereco2; ?>
                        </div></td>
                    <td width="83" class="td_7_sb"></td>

                    <td width="4" class="td_2"></td>
                </tr>
            </table>
            <table style="width:666px; border-top:solid; border-top-width:2px; border-top-color:#000000" border="0" cellspacing="0" cellpadding="0">
                <tr>

                    <td width="7" class="td_7_sb"> </td>
                    <td width="440" style="width: 417px; height:50px;"><?php
                        // GERA CODIGO DE BARRA
                        $linha = "$codigo_barras";

                        // Definimos as dimensoes das imagens
                        $fino = 1;
                        $largo = 3;
                        $altura = 40;

                        // Criamos um array associativo com os binários
                        $Bar[0] = "00110";
                        $Bar[1] = "10001";
                        $Bar[2] = "01001";
                        $Bar[3] = "11000";
                        $Bar[4] = "00101";
                        $Bar[5] = "10100";
                        $Bar[6] = "01100";
                        $Bar[7] = "00011" ;
                        $Bar[8] = "10010";
                        $Bar[9] = "01010";

                        // Inicio padrao do Código de Barras
                        echo "<img src='".base_url()."assets/images/boleto/imagens/p.png' width=$fino height=$altura border=0>";
                        echo "<img src='".base_url()."assets/images/boleto/imagens/b.png' width=$fino height=$altura border=0>";
                        echo "<img src='".base_url()."assets/images/boleto/imagens/p.png' width=$fino height=$altura border=0>";
                        echo "<img src='".base_url()."assets/images/boleto/images/b.png' width=$fino height=$altura border=0>";

                        // Checando para saber se o conteúdo é impar
                        if (bcmod(strlen($linha),2) <> 0) {
                            $linha = '0'.$linha;
                        }

                        for ($a = 0; $a < strlen($linha); $a++){

                            $Preto  = $linha[$a];
                            $CodPreto  = $Bar[$Preto];

                            $a = $a+1; // Sabemos que o Branco é um depois do Preto...
                            $Branco = $linha[$a];
                            $CodBranco = $Bar[$Branco];


                            // Encontrado o CodPreto e o CodBranco vamos fazer outro looping dentro do nosso
                            for ($y = 0; $y < 5; $y++) { // O for vai pegar os binários

                                if ($CodPreto[$y] == '0') { // Se o binario for preto e fino ecoa
                                    echo "<img src='".base_url()."assets/images/boleto/p.png'  width=$fino height=$altura border=0>";
                                }

                                if ($CodPreto[$y] == '1') { // Se o binario for preto e grosso ecoa
                                    echo "<img src='".base_url()."assets/images/boleto/p.png'  width=$largo height=$altura border=0>";
                                }

                                if ($CodBranco[$y] == '0') { // Se o binario for branco e fino ecoa
                                    echo "<img src='".base_url()."assets/images/boleto/b.png'  width=$fino height=$altura border=0>";
                                }

                                if($CodBranco[$y] == '1') { // Se o binario for branco e grosso ecoa
                                    echo "<img src='".base_url()."assets/images/boleto/b.png'  width=$largo height=$altura border=0>";
                                }
                            }

                        } // Fechamos nosso looping maior

                        // Encerramos o código ecoando o final(encerramento)

                        // Final padrao do Codigo de Barras
                        echo "<img src='".base_url()."assets/images/boleto/p.png'  width=$largo height=$altura border=0>";
                        echo "<img src='".base_url()."assets/images/boleto/b.png' width=$fino  height=$altura border=0>";
                        echo "<img src='".base_url()."assets/images/boleto/p.png'  width=$fino  height=$altura border=0>";
                        ?></td>
                    <td width="37" class="td_7_sb"> </td>
                    <td width="178" valign="top"><div class="titulo" style="text-align:left;">Autenticaçao Mecânica</div></td>
                    <td width="4" class="td_2"> </td>
                </tr>
            </table>
        </td></tr></table>
<br><hr size="1"><br>
</body>
<?php }?>
</html>

<?php
function modulo_10($num) {
    $numtotal10 = 0;
    $fator = 2;

    // Separacao dos numeros
    for ($i = strlen($num); $i > 0; $i--) {
        // pega cada numero isoladamente
        $numeros[$i] = substr($num,$i-1,1);
        // Efetua multiplicacao do numero pelo (falor 10)
        // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Ita�
        $temp = $numeros[$i] * $fator;
        $temp0=0;
        foreach (preg_split('//',$temp,-1,PREG_SPLIT_NO_EMPTY) as $k=>$v){ $temp0+=$v; }
        $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
        // monta sequencia para soma dos digitos no (modulo 10)
        $numtotal10 += $parcial10[$i];
        if ($fator == 2) {
            $fator = 1;
        } else {
            $fator = 2; // intercala fator de multiplicacao (modulo 10)
        }
    }

    // v�rias linhas removidas, vide fun��o original
    // Calculo do modulo 10
    $resto = $numtotal10 % 10;
    $digito = 10 - $resto;
    if ($resto == 0) {
        $digito = 0;
    }

    return $digito;

}
?>
