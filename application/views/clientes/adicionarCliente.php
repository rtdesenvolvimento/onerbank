<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de cliente<small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">
                    <div class="row">
                         <div class="col-md-6 col-sm-6">
                            <label for="origem" >Origem</label>
                            <select name="origem" id="origem" class="form-control" required="required">
                                <option value="Particular">Particular</option>
                                <option value="Financeira">Financeira</option>
                                <option value="Seguradora">Seguradora</option>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="tipoPessoa" > Tipo de pessoa</label>
                            <select name="tipoPessoa" id="tipoPessoa" class="form-control" required="required">
                                <option value="PF">Pessoa Física</option>
                                <option value="PJ">Pessoa Jurídica</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="documento"><div id="div_cpfCnpj">CPF</div></label>
                            <input id="documento" type="text" style="float: left;margin-right: 10px;" class="form-control" placeholder="Digite o CPF/CNPJ de acordo com o Tipo de pessoa" name="documento" value=""/>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <label for="documento"><div id="div_cpfCnpj">Nome*</div></label>
                            <input id="nomeCliente"  class="form-control" type="text"  required="required" name="nomeCliente"  placeholder="Digite o Nome ou Razação social." value=""/>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <label for="nomeFantasiaApelido" ><div id="div_nomeFantasia">Apelido</div></label>
                            <input id="nomeFantasiaApelido"  class="form-control" type="text" name="nomeFantasiaApelido"
                                   value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                        </div>
                        <div id="div_rg" style="width: 100%;">
                            <div class="col-md-3 col-sm-3">
                                <label for="rg">RG</label>
                                <input id="rg" type="text" class="form-control" name="rg"
                                       value="<?php echo set_value('rg'); ?>"/>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="orgaoEmissor">Orgão Emissor</label>
                                <input id="orgaoEmissor" type="text" class="form-control" name="orgaoEmissor"
                                       value="<?php echo set_value('orgaoEmissor'); ?>"/>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="estadoOrgaoEmissor">Estado Emissor</label>
                                <input id="estadoOrgaoEmissor" type="text" class="form-control" name="estadoOrgaoEmissor"
                                       value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label for="dataOrgaoEmissor">Data Emissão</label>
                                <input id="dataOrgaoEmissor" type="date" class="form-control" name="dataOrgaoEmissor"
                                       value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="telefone">Telefone</label>
                            <input id="telefone" type="text" class="form-control" name="telefone"
                                   value="<?php echo set_value('telefone'); ?>"/>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="celular">Celular</label>
                            <input id="celular" type="text" class="form-control" name="celular"
                                   value="<?php echo set_value('celular'); ?>"/>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label for="email">Email</label>
                            <input id="email" type="email" class="form-control" name="email"
                                   value="<?php echo set_value('email'); ?>"/>
                        </div>
                        <div class="col-md-3 col-sm-3" id="div_sexo">
                            <label for="sexo">Sexo</label>
                            <select name="sexo" id="sexo" class="form-control" required="required">
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="email"><div id="div_dataNascimento">Data de nascimento</div></label>
                            <input type="date" name="data_nascimento" class="form-control" id="data_nascimento"
                                   value="<?php echo set_value('data_nascimento'); ?>"/>
                        </div>
                    </div>
                    <div class="row" id="div_informacoes_fiscais" style="display: none;">
                        <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Informações fiscais <small>do cliente</small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <label for="IE">IE</label>
                                            <input id="IE" type="text" class="form-control" name="IE"
                                                   value="<?php echo set_value('IE'); ?>"/>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label for="IESUF">Insc. SUFRAMA</label>
                                            <input id="IESUF" type="text" class="form-control" name="IESUF"
                                                   value="<?php echo set_value('IESUF'); ?>"/>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label for="IEST">Insc. ST</label>
                                            <input id="IEST" type="text" class="form-control" name="IEST"
                                                   value="<?php echo set_value('IEST'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span12" id="divProgress" style="margin-left: 0"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Informações adicionais <small>do cliente</small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="span12" style="margin-left: 0">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab5" data-toggle="tab">Endereço</a></li>
                                            <li id="tabContato" style="display: none;"><a href="#tab2" data-toggle="tab">Contato</a></li>
                                            <li><a href="#tab3" data-toggle="tab">Senha</a></li>
                                            <li><a href="#tab4" data-toggle="tab">Observação</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab5">
                                                <div class="row well" style="padding: 1%; margin-left: 0">
                                                     <div class="col-md-3 col-sm-3">
                                                        <label for="cep">CEP<small> [TAB] consulta cep (Necessita Internet)</small></label>
                                                        <input id="cep" type="text" name="cep" class="form-control" onBlur="getConsultaCEP();"
                                                               value="<?php echo set_value('cep'); ?>"/>
                                                    </div>

                                                    <div class="col-md-7 col-sm-7">
                                                        <label for="rua">Rua</label>
                                                        <input id="rua" type="text" name="rua" class="form-control"  value="<?php echo set_value('rua'); ?>"/>
                                                    </div>

                                                    <div class="col-md-2 col-sm-2">
                                                        <label for="numero">Número</label>
                                                        <input id="numero" type="text" name="numero" class="form-control"  value="<?php echo set_value('numero'); ?>"/>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <label for="complemento">Complemento</label>
                                                        <input id="complemento" type="text" name="complemento" class="form-control" value="<?php echo set_value('complemento'); ?>"/>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <label for="bairro">Bairro</label>
                                                        <input id="bairro" type="text" name="bairro" class="form-control" value="<?php echo set_value('bairro'); ?>"/>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <label for="codIBGECidade">Cod.IBGE</label>
                                                        <input id="codIBGECidade" type="text" name="codIBGECidade" class="form-control" value="<?php echo set_value('codIBGECidade'); ?>"/>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <label for="cidade">Cidade</label>
                                                        <input id="cidade" type="text" name="cidade" class="form-control" value="<?php echo set_value('cidade'); ?>"/>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <label for="codIBGEEstado">Cod.IBGE</label>
                                                        <input id="codIBGEEstado" type="text" name="codIBGEEstado" class="form-control"  value="<?php echo set_value('codIBGEEstado'); ?>"/>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <label for="estado">Estado</label>
                                                        <input id="estado" type="text" name="estado" class="form-control" value="<?php echo set_value('estado'); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab2">
                                                <div id="div_contato">
                                                    <div class="row well" style="padding: 1%; margin-left: 0">
                                                        <div class="col-md-4 col-sm-4">
                                                            <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                                                            <input id="contatoNomeCliente" class="form-control" type="text" name="contatoNomeCliente"
                                                                   value="<?php echo set_value('contatoNomeCliente'); ?>"/>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4">
                                                            <label for="contatoCpf">CPF</label>
                                                            <input id="contatoCpf" type="text" class="form-control"name="contatoCpf"
                                                                   value="<?php echo set_value('contatoCpf'); ?>"/>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4">
                                                            <label for="contatoSexo">Sexo</label>
                                                            <select name="contatoSexo" id="contatoSexo" class="form-control"required="required">
                                                                <option value="M">Masculino</option>
                                                                <option value="F">Feminino</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label for="contatoTelefone">Telefone</label>
                                                            <input id="contatoTelefone" type="text" class="form-control" name="contatoTelefone"
                                                                   value="<?php echo set_value('contatoTelefone'); ?>"/>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label for="contatoCelular">Celular</label>
                                                            <input id="contatoCelular" type="text" class="form-control" name="contatoCelular"
                                                                   value="<?php echo set_value('contatoCelular'); ?>"/>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label for="email">Email</label>
                                                            <input id="contatoEmail" type="email" class="form-control" name="contatoEmail"
                                                                   value="<?php echo set_value('contatoEmail'); ?>"/>
                                                        </div>

                                                        <div class="col-md-3 col-sm-3">
                                                            <label for="email">Data de nascimento</label>
                                                            <input type="date" name="contatoDataNascimento" class="form-control" id="contatoDataNascimento"
                                                                   value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab3">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <label for="senha">Senha<small> Deixe em branco e a senha padrão será o (cpj/cnpj) do cliente</small></label>
                                                    <input type="text" name="senha" class="form-control" id="senha"
                                                           value=""/>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab4">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <label for="senha">Observações do cliente</label>
                                                        <textarea class="form-control"  id="observacao" name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Adicionar</button>
                            <a href="<?php echo base_url() ?>index.php/clientes" id="" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $("#documento").mask("999.999.999-99");
        $('#contatoCpf').mask("999.999.999-99");
        $('#documento').focus();

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#formCliente').validate({
            rules: {
                nomeCliente: {required: true}
            },
            messages: {
                nomeCliente: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

    });
</script>




