<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $emitente->nome;?></title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/css/datepicker.css">

    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/theme-default.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilo.css" type="text/css"/>

    <style>
        body{margin: 0;}

        .btn-blue {
            background-color: #1d589e;
            border-color: #eef3f9;
            color: white;
        }
        .fundo
        {
            background-image: url('https://i.pinimg.com/originals/67/b0/02/67b00256a0c3a755dcfe3aed1d77ac7c.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
            bottom: 0;
            color: black;
            left: 0;
            overflow: auto;
            padding: 3em;
            position: absolute;
            right: 0;
            top: 0;
        }

        @media screen and (min-width: 700px) {.painel_principal {}}
        @media screen and (max-width: 500px) {.painel_principal {margin-left: -25%}}

        @media screen and (min-width: 700px) {.logotipo {padding-left: 0%;}}
        @media screen and (max-width: 500px) {.logotipo {width: 50%;margin-left: 20%;margin-top: -20%}}
    </style>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>

<!-- end: SLIDING BAR -->
<div class="container">
    <div class="fundo">
        <!-- Menu -->
        <div class="container-fluid" style="margin-top: -5%;">
            <div class="row">
                <div class="col-md-3">
                    <a href="<?php echo base_url(); ?>clientes/pageCadCliente/<?php echo $this->uri->segment(3);?>">
                        <img src="<?php echo $emitente->url_logo;?>" class="img-responsive logotipo" alt="Logotipo" style="    margin-top: 30%;">
                    </a>
                </div>
                <div class="col-md-7">
                    <!-- Chamada -->
                    <div class="esquerda barraChamada" style="font-family: adlibn;">
                        <span style="color: #0f0f0f;">
                            <?php echo $emitente->frase_efeito;?>
                        </span>
                    </div>
                    <!-- Fim da Chamada -->
                </div>
            </div>
        </div>

        <div class="row painel_principal">
            <div class="col-sm-12">
                <!-- start: FORM WIZARD PANEL -->
                <div >
                    <div class="panel-body">
                        <form action="#" role="form" class="smart-wizard form-horizontal" id="form">
                            <div id="wizard" class="swMain">
                                <ul>
                                    <li>
                                        <a href="#step-1">
                                            <div class="stepNumber">1</div>
                                            <span class="stepDesc">1&ordm; Etapa</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <div class="stepNumber">2</div>
                                            <span class="stepDesc">2&ordm; Etapa</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <div class="stepNumber">3</div>
                                            <span class="stepDesc">3&ordm; Etapa</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress progress-xs transparent-black no-radius active">
                                    <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                                        <span class="sr-only"> 0% Complete (success)</span>
                                    </div>
                                </div>
                                <div id="step-1">
                                    <h2 class="StepTitle center">Cadastre os dados do pagador</h2>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Tipo <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <label class="radio-inline">
                                                <input type="radio" class="grey" value="pj" name="tipoPessoa" id="tipoPessoaJ" >
                                                Juridica
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" class="grey" value="pf" name="tipoPessoa" checked="checked" id="tipoPessoaF">
                                                Fisica
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <span id="spanCpf">CPF</span><span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF/CNPJ">
                                            <span id="spanCpf"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <span id="spanNome"> Nome </span><span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                                            <input type="hidden" value="" id="idPessoaPrincipal" name="idPessoaPrincipal"/>
                                            <input type="hidden" value="" id="idLancamento" name="idLancamento"/>
                                        </div>
                                    </div>
                                    <div class="form-group" id="divSexo">
                                        <label class="col-sm-3 control-label">
                                            Sexo <span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <select class="form-control" id="sexo" name="sexo">
                                                <option value="M">Masculino</option>
                                                <option value="F">Feminino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="divRG">
                                        <label class="col-sm-3 control-label">
                                            RG <span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="rg" name="rg" placeholder="RG">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Telefone celular <span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            E-mail <span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="E-mail">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            CEP <span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" id="cep" name="cep" placeholder="CEP">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Endereço<span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Nº<span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="numero" name="numero" placeholder="Número">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Compl.<span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Complemento">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Bairro<span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Cidade<span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Estado<span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="estado" name="estado" placeholder="estado">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button class="btn btn-blue next-step btn-block" style="text-align: center;">
                                                Salvar e continuar <i class="fa fa-arrow-circle-right"></i><br/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-2">
                                    <h2 class="StepTitle center">Cadastre os dados do boleto.</h2>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Valor do vencimento <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control money" min="1" id="valorVencimento" name="valorVencimento" placeholder="R$ Valor do vencimento">
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Data do vencimento <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="date" value="<?php echo date('Y-m-d', strtotime("+1 days", strtotime(date('Y-m-d'))))?>" id="dataVencimento" name="dataVencimento"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Carência (dias)<span class="symbol"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" id="limitePagamento" value="3" name="limitePagamento" placeholder="Limite para o pagamento do boleto em dias">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Instruções do boleto</span>
                                        </label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control" id="descricaoBoleto" rows="4" name="descricaoBoleto" placeholder="Instruções utilizadas na parte central do boleto..."><?php echo $emitente->instrucaoBoletoOnline;?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <button class="btn btn-blue next-step btn-block" style="text-align: center;">
                                                Emitir boleto <i class="fa fa-dollar"></i><br/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-3">
                                    <h3>Dados do pagador</h3>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            CPF/CNPJ :
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="cpf"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Nome:
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="nome"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Telefone celular:
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="celular"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            E-mail:
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="email"></p>
                                        </div>
                                    </div>
                                    <h3>Dados do boleto</h3>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Valor do vencimento:
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="valorVencimento"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Data do vencimento:
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="dataVencimento"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Carência (dias):
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="limitePagamento"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Instruções do boleto:
                                        </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static display-value" data-display="descricaoBoleto"></p>
                                        </div>
                                    </div>
                                    <div class="alert alert-success" style="text-align: center;display: none;" id="divImprimirBoleto">
                                        <a href="" id="imprimirBoleto" target="_blank" class="btn btn-lg btn-light-blue hidden-print">
                                            IMPRIMIR BOLETO <i class="fa fa-print"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-8">
                                            <button onclick="concluirCadastro();"  class="btn btn-blue next-step btn-block" style="text-align: left;">
                                                Emitir um novo boleto
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                <div id="step-4" style="display: none;">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-6">
                                            <div class="form-group">
                                                <div class="col-sm-3 col-sm-offset-6">
                                                    <button class="btn btn-blue next-step btn-block" style="text-align: left;">
                                                        Concluir cadastro
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- end: FORM WIZARD PANEL -->
            </div>
        </div>
    </div>
</div>

<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/excanvas.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/velocity/jquery.velocity.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/truncate/jquery.truncate.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>

<script src="<?php echo base_url(); ?>assets/js/subview.js"></script>
<script src="<?php echo base_url(); ?>assets/js/subview-examples.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url(); ?>assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo base_url(); ?>assets/js/form-wizard-cadastro-pessoa.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE JAVASCRIPTS  -->
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<!-- end: CORE JAVASCRIPTS  -->
<script>

    var base_url = '<?php echo base_url();?>';

    jQuery(document).ready(function() {

        Main.init();
        FormWizard.init();

        $("#cpf").mask("99.999.999/9999-99");
        $("#celular").mask("(99) 99999-9999");
        $('.dataNascimento').mask("99/99/9999");

        $( document ).ready(function() {
            $(".money").maskMoney();
        });

        $('#cep').change(function (event) {
            prreencherEndereco($(this).val());
        });

        $('#cpf').blur(function (event) {
            var cpf_cnpj = $(this);
            verificaCadastro(cpf_cnpj);
        });

        document.getElementById("valorVencimento").onclick=function(){
            var begin=0
            var end=0
            if(this.setSelectionRange){
                this.focus();
                this.setSelectionRange(begin,end);
            }else if (this.createTextRange){
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', begin);
                range.select();
            }
            $('#valorVencimento').select();
        }

        document.getElementById("cpf").onclick=function(){
            var begin=0;
            var end=0;
            if(this.setSelectionRange){
                this.focus();
                this.setSelectionRange(begin,end);
            }else if (this.createTextRange){
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', begin);
                range.select();
            }

            if (document.getElementsByName('tipoPessoa')[0].checked) $("#cpf").mask("99.999.999/9999-99");
            else $("#cpf").mask("999.999.999-99");
        }

        document.getElementById("celular").onclick=function(){
            var begin=0
            var end=0
            if(this.setSelectionRange){
                this.focus();
                this.setSelectionRange(begin,end);
            }else if (this.createTextRange){
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', begin);
                range.select();
            }
        }

        $('#salvarCloncluirBoleto').click(function (event) {
            salvarBoleto(event);
        });

        setInterval(function () {
            if (document.getElementsByName('tipoPessoa')[0].checked) {
                $('#divSexo').hide();
                $('#divRG').hide();
                $('#spanNome').html('Razão social');
                $('#spanCpf').html("CNPJ");
            } else {
                $('#spanNome').html('Nome');
                $('#divSexo').show();
                $('#divRG').show();
                $('#spanCpf').html("CPF");
            }
        },800);
    });

    function prreencherEndereco(cep) {
        if (cep === '') return;
        var url = 'https://api.postmon.com.br/v1/cep/'+cep;
        $.get(url,{
                cep: cep
            },
            function (data) {
                if(data !== -1){
                    $("#endereco").val(data.logradouro);
                    $("#bairro").val( data.bairro );
                    $("#cidade").val( data.cidade );
                    $("#estado").val( data.estado );
                    $('#numero').focus();
                }
            });
    }

    function salvarBoleto(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url+"clientes/cadFinanceiroOnlineIntegracaoSubmit",
            data: $('#form').serialize(),
            dataType: 'html',
            success: function (pessoa) {
                $('#step001-pts').click();
            }
        });
    }

    function verificaCadastro(cpf) {

        limparFormularioCliente();

        if (cpf  !== '') {
            $.ajax({
                type: "POST",
                url: base_url+"clientes/verificarCadatro",
                data: {
                    cpf: $('#cpf').val()
                },
                dataType: 'json',
                success: function (pessoa) {

                    if (pessoa.nomeCliente !== undefined) {
                        var cep =  pessoa.cep;

                        if (cep !== '') {
                            cep = cep.replace('.', '');
                            cep = cep.replace('-', '');
                        }

                        $('#nome').val(pessoa.nomeCliente);
                        $('#sexo').val(pessoa.sexo);
                        $('#rg').val(pessoa.rg);
                        $('#celular').val(pessoa.celular);
                        $('#email').val(pessoa.email);
                        $('#cep').val(cep);
                        $('#endereco').val(pessoa.rua);
                        $('#numero').val(pessoa.numero);
                        $('#complemento').val(pessoa.complemento);
                        $('#bairro').val(pessoa.bairro);
                        $('#cidade').val(pessoa.cidade);
                        $('#estado').val(pessoa.estado);
                    } else  {
                        consultaPessoa(cpf, '<?php echo base_url();?>');
                    }
                }
            });
        }
    }

    function limparFormularioCliente() {
        $('#nome').val('');
        $('#rg').val('');
        $('#celular').val('');
        $('#email').val('');
        $('#cep').val('');
        $('#endereco').val('');
        $('#numero').val('');
        $('#complemento').val('');
        $('#bairro').val('');
        $('#cidade').val('');
        $('#estado').val('');
    }

    function consultaPessoa(tagCpfCnpj, base_url) {

        var cpf_cnpj = tagCpfCnpj.val();

        if (cpf_cnpj !== '' && cpf_cnpj !== '___.___.___-__' && cpf_cnpj !== '__.___.___/____-__') {
            if (document.getElementsByName('tipoPessoa')[0].checked) {
                if (!valida_cnpj(cpf_cnpj)) {
                    alert('CNPJ inválido!');
                    tagCpfCnpj.val('');
                } else {

                    var cpf_cnpj = cpf_cnpj.replace(/[^0-9]/g, '');

                    $.ajax({
                        type: "POST",
                        url: base_url+"clientes/getConsutaWSCliente/"+cpf_cnpj,
                        dataType: 'json',
                        success: function (empresa) {

                            if (empresa.status === undefined) {
                                alert("Não foi possível encontrar o cnpj");
                                return;
                            }

                            if (empresa.status === 'ERROR') {
                                alert(empresa.message);
                                return;
                            }

                            var d = new Date(empresa.abertura);
                            var date = [
                                d.getFullYear(),
                                ('0' + (d.getMonth() + 1)).slice(-2),
                                ('0' + d.getDate()).slice(-2)
                            ].join('-');

                            var cep = empresa.cep;
                            cep = cep.replace('-','');
                            cep = cep.replace('.','');

                            if (empresa.situacao !== 'ATIVA') {

                                if (confirm('Esta empres encontra-se na situacao '+empresa.situacao+', motivo ' + empresa.motivo_situacao+'. Deseja realmente importar seus dados?')) {
                                    $('#nome').val(empresa.nome);
                                    $('#celular').val(empresa.telefone);
                                    $('#email').val(empresa.email);
                                    $('#cep').val(cep);
                                    $('#endereco').val(empresa.logradouro);
                                    $('#numero').val(empresa.numero);
                                    $('#complemento').val(empresa.complemento);
                                    $('#bairro').val(empresa.bairro);
                                    $('#cidade').val(empresa.municipio);
                                    $('#estado').val(empresa.uf);
                                }
                            } else {
                                $('#nome').val(empresa.nome);
                                $('#celular').val(empresa.telefone);
                                $('#email').val(empresa.email);
                                $('#cep').val(cep);
                                $('#endereco').val(empresa.logradouro);
                                $('#numero').val(empresa.numero);
                                $('#complemento').val(empresa.complemento);
                                $('#bairro').val(empresa.bairro);
                                $('#cidade').val(empresa.municipio);
                                $('#estado').val(empresa.uf);
                            }
                        }
                    });
                }
            } else if (!valida_cpf(cpf_cnpj)) {
                alert('CPF inválido!');
                tagCpfCnpj.val('');
            }
        }

        verificaCadastro();
    }

    function concluirCadastro() {
        location.reload();
    }

</script>
</body>
<!-- end: BODY -->
</html>