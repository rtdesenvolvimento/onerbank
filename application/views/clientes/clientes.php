<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-users"></i> Clientes (<?php echo count($results); ?>)</small></h2>
                <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aCliente')) { ?>
                    <ul class="nav navbar-right panel_toolbox">
                        <a class="btn btn-success" href="<?php echo base_url(); ?>index.php/clientes/adicionar"><i class="fa fa-plus"></i> Cadastrar Cliente</a>
                    </ul>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12">
                    <div class="well" style="overflow: auto">
                        <div class="input-group form-group row pull-right top_search">
                            <input type="text" class="form-control" id="termo" value="<?php if($termo) echo $termo ;?>" placeholder="Digite os termos da pesquisa de clientes">
                            <span class="input-group-btn">
                              <button class="btn btn-default" type="button" id="buscar"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php foreach ($results as $r) {?>
                    <div class="col-md-4 col-sm-4  profile_details">
                        <div class="well profile_view" style="<?php if ($r->inativo) echo 'background: linear-gradient(to bottom, #ededed 1%,#e66b6b 100%);color: #34495e;';?>">
                            <div class="col-sm-12">
                                <h4 class="brief"><i><?php echo $r->nomeCliente?></i></h4>
                                <div class="left col-md-12 col-sm-12">
                                    <p><strong>Documento: </strong> <?php echo $r->documento;?></p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-map-marker"></i> <?php echo $r->rua.' '.$r->numero.' - '.$r->bairro.' - '.$r->cidade.'/'.$r->estado.' CEP: '.$r->cep;?></li>
                                        <li><i class="fa fa-phone"></i> <a href="tel:<?php echo $r->telefone;?>" target="_blank"><?php echo $r->telefone;?> </a> </li>
                                        <li><i class="fa fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=55<?php echo $r->celular;?>" target="_blank"> <?php echo $r->celular;?> </a></li>
                                        <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $r->email;?>" target="_blank"> <?php echo $r->email;?>  </a></li>
                                        <?php if ($r->observacao != ''){?>
                                            <li><i class="fa fa-briefcase user-profile-icon"></i> <?php echo $r->observacao;?></li>
                                        <?php }?>
                                        <?php if ($r->inativo){?>
                                            <li><i class="fa fa fa-warning"></i> <?php echo $r->observacaoInativo;?></li>
                                        <?php }?>
                                    </ul>
                                </div>
                                <a href="<?php echo base_url() . 'index.php/clientes/editar/' . $r->idClientes;?>"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-user"> </i> Editar </button></a>
                                <a href="<?php echo base_url() . 'index.php/clientes/profile/' . $r->idClientes;?>"><button type="button" class="btn btn-warning btn-sm"><i class="fa fa-eye"> </i> Ver Detalhes </button></a>
                                <div class="right col-md-5 col-sm-5 text-center" style="display: none;">
                                    <img src="<?php echo base_url(); ?>assets/images/img.jpg" alt="" class="img-circle img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var cliente = $(this).attr('cliente');
            $('#idCliente').val(cliente);

        });

        $("#buscar").click(function (event) {
            window.location.href = "<?php echo base_url() ?>index.php/clientes/buscar/" + $('#termo').val();
        });

        $("#termo").keypress(function (event) {
            if (event.which === 13) {
                window.location.href = "<?php echo base_url() ?>index.php/clientes/buscar/" + $('#termo').val();
            }
        });

    });
</script>