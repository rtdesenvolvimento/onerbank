<div class="">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-user"></i> Cadastro de cliente <small>editar</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-edit"></i> Dados do cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-car"></i> Veículos</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-2 col-sm-2">
                                    <label for="codigocliente" >Código do cliente</label>
                                    <input id="codigocliente"  class="form-control" disabled type="text" style="float: left;margin-right: 10px;" name="codigocliente"
                                           value="<?php echo  str_pad($result->idClientes, 4, '0', STR_PAD_LEFT); ?>"/>
                                </div>
                                <div class="col-md-5 col-sm-5">
                                    <label for="origem" >Origem</label>
                                    <select name="origem" id="origem" class="form-control" required="required">
                                        <option value="">selecione uma origem</option>
                                        <option <?php if ($result->origem == 'Particular') echo 'selected="selected"'?>  value="Financeira">Particular</option>
                                        <option <?php if ($result->origem == 'Financeira') echo 'selected="selected"'?>  value="Financeira">Financeira</option>
                                        <option <?php if ($result->origem == 'Seguradora') echo 'selected="selected"'?>  value="Seguradora">Seguradora</option>
                                    </select>
                                </div>
                                <div class="col-md-5 col-sm-5">
                                    <label for="tipoPessoa" >Tipo de pessoa</label>
                                     <select name="tipoPessoa" id="tipoPessoa" class="form-control" required="required">
                                        <option <?php if ($result->tipoPessoa == 'PF') echo 'selected="selected"'?> value="PF">Pessoa Física</option>
                                        <option <?php if ($result->tipoPessoa == 'PJ') echo 'selected="selected"'?> value="PJ">Pessoa Jurídica</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                     <label for="documento"><div id="div_cpfCnpj">CPF</div></label>
                                     <input id="documento" type="text" style="float: left;margin-right: 10px;" class="form-control" name="documento"
                                           value="<?php echo $result->documento; ?>"/>
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <label for="documento"><div id="div_cpfCnpj">Nome*</div></label>
                                    <input id="nomeCliente" class="form-control"type="text"  required="required" name="nomeCliente"
                                           value="<?php echo $result->nomeCliente; ?>"/>
                                    <?php echo form_hidden('idClientes', $result->idClientes) ?>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <label for="nomeFantasiaApelido" ><div id="div_nomeFantasia">Apelido</div></label>
                                    <input id="nomeFantasiaApelido"  class="form-control" type="text" name="nomeFantasiaApelido"
                                           value="<?php echo $result->nomeFantasiaApelido; ?>"/>
                                </div>
                                <div id="div_rg" style="width: 100%;">
                                    <div class="col-md-3 col-sm-3">
                                        <label for="rg">RG</label>
                                        <input id="rg" type="text" class="form-control" name="rg"
                                               value="<?php echo $result->rg; ?>"/>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="orgaoEmissor">Orgão Emissor</label>
                                        <input id="orgaoEmissor" type="text" class="form-control" name="orgaoEmissor"
                                               value="<?php echo $result->orgaoEmissor; ?>"/>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="estadoOrgaoEmissor">Estado Emissor</label>
                                        <input id="estadoOrgaoEmissor" type="text" class="form-control" name="estadoOrgaoEmissor"
                                               value="<?php echo $result->estadoOrgaoEmissor; ?>"/>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label for="dataOrgaoEmissor">Data Emissão</label>
                                        <input id="dataOrgaoEmissor" type="date" class="form-control" name="dataOrgaoEmissor"
                                               value="<?php echo $result->dataOrgaoEmissor; ?>"/>
                                    </div>
                                </div>
                                 <div class="col-md-2 col-sm-2">
                                    <label for="telefone">Telefone</label>
                                    <input id="telefone" type="text" class="form-control" name="telefone"
                                           value="<?php echo $result->telefone; ?>"/>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <label for="celular">Celular</label>
                                    <input id="celular" type="text" class="form-control" name="celular"
                                           value="<?php echo $result->celular; ?>"/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="<?php echo $result->email; ?>"/>
                                </div>
                                <div class="col-md-3 col-sm-3" id="div_sexo">
                                    <label for="sexo">Sexo</label>
                                    <select name="sexo" id="sexo" class="form-control" required="required">
                                        <option <?php if ($result->sexo == 'M') echo 'selected="selected"'?> value="M">Masculino</option>
                                        <option <?php if ($result->sexo == 'F') echo 'selected="selected"'?> value="F">Feminino</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <label for="email"><div id="div_dataNascimento">Data de nascimento</div></label>
                                    <input type="date" name="data_nascimento" class="form-control" id="data_nascimento"
                                           value="<?php echo $result->data_nascimento; ?>"/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label for="isBloquearCliente"><div id="isBloquearCliente"> Clique aqui para bloquear o cliente</div></label>
                                    <input id="inativo" <?php if($result->inativo == 1) echo  'checked="checked"';?> type="checkbox" name="inativo"/>
                                </div>
                            </div>
                            <div class="row" id="div_informacoes_fiscais" style="<?php if($result->tipoPessoa == 'PF') echo 'display: none;';?>" >
                                <div class="col-md-12 col-sm-12 ">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Informações fiscais <small>do cliente</small></h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="row">
                                                 <div class="col-md-4 col-sm-4">
                                                    <label for="IE">IE</label>
                                                    <input id="IE" type="text" class="form-control" name="IE"
                                                           value="<?php echo $result->IE; ?>"/>
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                    <label for="IESUF">Insc. SUFRAMA</label>
                                                    <input id="IESUF" type="text" class="form-control" name="IESUF"
                                                           value="<?php echo $result->IESUF; ?>"/>
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                    <label for="IEST">Insc. ST</label>
                                                    <input id="IEST" type="text" class="form-control" name="IEST"
                                                           value="<?php echo $result->IEST; ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 ">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Informações adicionais <small>do cliente</small></h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="span12" style="margin-left: 0">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab5" data-toggle="tab"><i class="fa fa-map"></i> Endereço</a></li>
                                                    <li id="tabContato" style="display: none;"><a href="#tab2" data-toggle="tab"><i class="fa fa-user"></i> Contato</a></li>
                                                    <li><a href="#tab3" data-toggle="tab"><i class="fa fa-key"></i> Senha</a></li>
                                                    <li><a href="#tab4" data-toggle="tab"><i class="fa fa-comment"></i> Observação</a></li>
                                                    <li id="tabBloqueio" style="display: none;"><a href="#tab6" data-toggle="tab"><i class="fa fa-trash"></i> Motivo do bloqueio</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab5">
                                                        <div class="row well" style="padding: 1%; margin-left: 0">
                                                            <div class="col-md-3 col-sm-3">
                                                                <label for="cep">CEP <small>[TAB] consulta cep (Necessita Internet)</small></label>
                                                                <input id="cep" type="text" name="cep" class="form-control" onBlur="getConsultaCEP();"
                                                                       value="<?php echo $result->cep; ?>"/>
                                                            </div>
                                                            <div class="col-md-7 col-sm-7">
                                                                <label for="rua">Rua</label>
                                                                <input id="rua" type="text" name="rua"  class="form-control"
                                                                       value="<?php echo $result->rua; ?>"/>
                                                            </div>
                                                            <div class="col-md-2 col-sm-2">
                                                                <label for="numero">Número</label>
                                                                <input id="numero" type="text" name="numero"  class="form-control"
                                                                       value="<?php echo $result->numero; ?>"/>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6">
                                                                <label for="complemento">Complemento</label>
                                                                <input id="complemento" type="text" name="complemento" class="form-control"
                                                                       value="<?php echo $result->complemento; ?>"/>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6">
                                                                <label for="bairro">Bairro</label>
                                                                <input id="bairro" type="text" name="bairro" class="form-control"
                                                                       value="<?php echo $result->bairro; ?>"/>
                                                            </div>
                                                            <div class="col-md-2 col-sm-2">
                                                                <label for="codIBGECidade">Cod.IBGE</label>
                                                                <input id="codIBGECidade" type="text" name="codIBGECidade" class="form-control"
                                                                       value="<?php echo $result->codIBGECidade; ?>"/>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4">
                                                                <label for="cidade">Cidade</label>
                                                                <input id="cidade" type="text" name="cidade" class="form-control"
                                                                       value="<?php echo $result->cidade; ?>"/>
                                                            </div>
                                                            <div class="col-md-2 col-sm-2">
                                                                <label for="codIBGEEstado">Cod.IBGE</label>
                                                                <input id="codIBGEEstado" type="text" name="codIBGEEstado" class="form-control"
                                                                       value="<?php echo $result->codIBGEEstado; ?>"/>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4">
                                                                <label for="estado">Estado</label>
                                                                <input id="estado" type="text" name="estado" class="form-control"
                                                                       value="<?php echo $result->estado; ?>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane " id="tab2">
                                                        <div class="row well" style="padding: 1%; margin-left: 0">
                                                            <div class="col-md-4 col-sm-4">
                                                                <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                                                                <input id="contatoNomeCliente" class="form-control" type="text" name="contatoNomeCliente"
                                                                       value="<?php echo $result->contatoNomeCliente; ?>"/>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4">
                                                                <label for="contatoCpf">CPF</label>
                                                                <input id="contatoCpf" type="text" class="form-control" name="contatoCpf"
                                                                       value="<?php echo $result->contatoCpf; ?>"/>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4">
                                                                <label for="contatoSexo">Sexo</label>
                                                                <select name="contatoSexo" id="contatoSexo" class="form-control" required="required">
                                                                    <option <?php if ($result->contatoSexo == 'M') echo 'selected="selected"'?> value="M">Masculino</option>
                                                                    <option <?php if ($result->contatoSexo == 'F') echo 'selected="selected"'?> value="F">Feminino</option>
                                                                </select>
                                                            </div>
                                                             <div class="col-md-3 col-sm-3">
                                                                <label for="contatoTelefone">Telefone</label>
                                                                <input id="contatoTelefone" type="text" class="form-control" name="contatoTelefone"
                                                                       value="<?php echo $result->contatoTelefone; ?>"/>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3">
                                                                <label for="contatoCelular">Celular</label>
                                                                <input id="contatoCelular" type="text" class="form-control" name="contatoCelular"
                                                                       value="<?php echo $result->contatoCelular; ?>"/>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3">
                                                                <label for="email">Email</label>
                                                                <input id="contatoEmail" type="email" class="form-control" name="contatoEmail"
                                                                       value="<?php echo $result->contatoEmail; ?>"/>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3">
                                                                <label for="contatoDataNascimento">Data de nascimento</label>
                                                                <input type="date" name="contatoDataNascimento" class="form-control" id="contatoDataNascimento"
                                                                       value="<?php echo $result->contatoDataNascimento; ?>"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="tab3">
                                                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                            <label for="senha">Senha</label>
                                                            <input type="text" name="senha" class="form-control" id="senha"
                                                                   value="<?php echo $result->senha; ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="tab4">
                                                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                            <div class="span12">
                                                                <label for="senha">Observações do cliente</label>
                                                                <textarea class="form-control" name="observacao" cols="30" rows="5"><?php echo $result->observacao; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="tab6">
                                                        <div class="span12 well" style="padding: 1%; margin-left: 0;">
                                                            <div class="span12">
                                                                <label for="senha">Motivo do bloqueio</label>
                                                                <textarea class="form-control" name="observacaoInativo" cols="30" rows="5"><?php echo $result->observacaoInativo; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ln_solid"></div>
                                                <div class="item form-group">
                                                    <div class="col-md-12" style="text-align: right">
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Alterar</button>
                                                        <a href="<?php echo base_url() ?>clientes/profile/<?php echo $result->idClientes;?>" class="btn btn-warning"><i class="fa fa-eye"></i> Ver detalhes</a>
                                                        <a href="<?php echo base_url() ?>clientes" id="" class="btn btn-primary"><i class="fa fa-backward"></i>  Voltar</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <form id="formVeiculosCliente" action="#" method="post">
                            <div class="row">
                                <div class="col-md-2 col-sm-2">
                                    <div class="formBuscaGSA">
                                        <label for="placa">
                                            Placa
                                            <a href="#Cliente" class="consulta_placa_click" style="float: right;margin-right: 4px;"><i class="icon-search tip-right"></i></i></a>
                                        </label>
                                        <input type="text" class="form-control consulta_placa" name="placa" required="required" id="placa"
                                               placeholder="Digite a placa do veículo"/>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <label for="tipoVeiculo">Tipo do Veículo</label>
                                    <select name="tipoveiculo_id" id="tipoveiculo_id" required="required" class="form-control" required="required">
                                        <option value="">selecione o tipo de veículo</option>
                                        <?php foreach ($tiposveiculo as $tipoveiculo) { ?>
                                            <option value="<?php echo $tipoveiculo->idTipoveiculo; ?>"><?php echo $tipoveiculo->nome?></option>
                                        <?php  }?>
                                    </select>
                                    <input type="text" style="display: none;" class="form-control" name="tipoVeiculo" id="tipoVeiculo"/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <input type="hidden" name="idVeiculo" id="idVeiculo"/>
                                    <input type="hidden" name="idClienteProduto" id="idClienteProduto" value="<?php echo $result->idClientes ?>"/>
                                    <label for="">Modelo</label>
                                    <input type="text" class="form-control" required="required" name="modelo" id="modelo"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label for="marca">Marca</label>
                                    <input type="text" class="form-control" required="required" name="marca" id="marca"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <label for="cor">Cor</label>
                                    <input type="text" class="form-control" name="cor" id="cor"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <label for="ano">Ano</label>
                                    <input type="number" class="form-control" name="ano" id="ano"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <label for="municipio">Município</label>
                                    <input type="text" class="form-control" name="municipio" id="municipio"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <label for="uf">UF</label>
                                    <input type="text" class="form-control" name="uf" id="uf"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label for="chassi">Chassi</label>
                                    <input type="text" class="form-control" name="chassi" id="chassi"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <label for="placa">Tipos de combustível</label>
                                    <input type="checkbox" style="margin-top: -2px;" name="gasolina" id="gasolina" value="1">&nbsp;Gasolina&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" style="margin-top: -2px;" name="etanol" id="etanol" value="1">&nbsp;Etanol&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" style="margin-top: -2px;" name="diesel" id="diesel" value="1">&nbsp;Diesel&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" style="margin-top: -2px;" name="gnv" id="gnv" value="1">&nbsp;Gás veicular (GNV)&nbsp;&nbsp;&nbsp;
                                    <br/>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <label for="situacao">Situação</label>
                                    <input type="text" readonly style="font-size: 16px;" class="form-control" name="situacao" id="situacao"
                                           placeholder=""/>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <label for="observacao">Observação</label>
                                    <textarea class="form-control" id="observacao" name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <div class="col-md-12" style="text-align: right">
                                    <button class="btn btn-success" id="btnAdicionarProduto"><i class="fa fa-save"></i> Salvar</button>
                                </div>
                            </div>
                        </form>
                        <div class="span12" id="divVeiculos" style="margin-left: 0">
                                <table class="table table-bordered" id="tblProdutos">
                                    <thead>
                                    <tr>
                                        <th style="text-align: left;">Veículos</th>
                                        <th style="text-align: center;width:10%;">Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($veiculos as $veiculo) {

                                        $strSituacao = '';
                                        $tipoCumbustivel = '';

                                        if($veiculo->situacao != 'Sem restrição') {
                                            $strSituacao = '<font color="red">'.$veiculo->situacao.'</font>';
                                        } else {
                                            $strSituacao = '<font color="#5bc0de">'.$veiculo->situacao.'</font>';
                                        }

                                        if ($veiculo->gasolina == 1) {
                                            $tipoCumbustivel = '<br/>Gasolina';
                                        }

                                        if ($veiculo->etanol == 1) {
                                            $tipoCumbustivel .= '<br/>Etanol';
                                        }

                                        if ($veiculo->diesel == 1) {
                                            $tipoCumbustivel .= '<br/>Diesel';
                                        }

                                        if ($veiculo->gnv == 1) {
                                            $tipoCumbustivel .= '<br/>Gás veicular (GNV)';
                                        }

                                        echo '<tr>';
                                        echo '<td style="text-align: left;">' .
                                            $veiculo->tipoVeiculo.' '.$veiculo->cor.'/'.$veiculo->ano.'<br/>'.
                                            'Placa: '. $veiculo->placa.
                                            '</br>Modelo: '.$veiculo->modelo.
                                            '<br/>Marca: '. $veiculo->marca.
                                            '<br/>Chassi: '. $veiculo->chassi.
                                            '<br/>Combustível(is):'.$tipoCumbustivel.
                                            '<br/>Localidade: '.$veiculo->municipio.'/'.$veiculo->uf.'<br/>'.$strSituacao.'<br/>';

                                        $anexos = $this->clientes_model->getAnexosByVeiculo($veiculo->idVeiculo);

                                        $cont = 1;
                                        $flag = 5;
                                        foreach ($anexos as $a) {

                                            if ($a->thumb == null) {
                                                $thumb = base_url() . 'assets/img/icon-file.png';
                                                $link = base_url() . 'assets/img/icon-file.png';
                                            } else {
                                                $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                                $link = $a->url . $a->anexo;
                                            }

                                            if ($cont == $flag) {
                                                echo '<div style="margin-left: 0" class="span2"><a href="#modal-anexo" style="width: 50%;height: 50%;" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'"  imagem="' . $a->idFotoVeiculo . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                                $flag += 4;
                                            } else {
                                                echo '<div class="span2"><a href="#modal-anexo" style="width: 50%;height: 50%;" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idFotoVeiculo . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                            }
                                            $cont++;
                                        }

                                        echo '</td> <td style="text-align: center;width:20%;">
                                            <span modelo="' . $veiculo->modelo . '" 
                                                  marca="' . $veiculo->marca . '"
                                                  placa="' . $veiculo->placa . '"
                                                  cor="' . $veiculo->cor . '"
                                                  municipio="' . $veiculo->municipio . '"
                                                  uf="' . $veiculo->uf . '"
                                                  situacao="' . $veiculo->situacao . '"
                                                  ano="' . $veiculo->ano . '"
                                                  chassi="' . $veiculo->chassi . '"
                                                  gasolina="' . $veiculo->gasolina . '"
                                                  etanol="' . $veiculo->etanol . '"
                                                  diesel="' . $veiculo->diesel . '"
                                                  gnv="' . $veiculo->gnv . '"
                                                  observacao="' . $veiculo->observacao . '"
                                                  tipoVeiculo="' . $veiculo->tipoVeiculo . '"
                                                  tipoveiculo_id="' . $veiculo->tipoveiculo_id . '"
                                                  idAcaoEditar="' . $veiculo->idVeiculo . '" >
                                                  <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button>
                                                </span>
                                                <a href="#cadFotoVeiculo" idVeiculo="'.$veiculo->idVeiculo.'" onclick="vincularIdentificadorVeiculo('.$veiculo->idVeiculo.')" data-toggle="modal">
                                                    <span class="btn"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-photo"> </i> </button></span>
                                                </a>
                                            <span idAcao="' . $veiculo->idVeiculo . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></span>
                                          </td>';
                                        echo '</tr>';
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

 
<!-- Modal adicionar fotos ao veiculo -->
<div id="cadFotoVeiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Adicionar fotos do veículo</h3>
    </div>
    <div class="modal-body">
        <form id="formFotoVeiculo" enctype="multipart/form-data" action="javascript:;"
          accept-charset="utf-8" method="post">

        <div class="span12" style="margin-left: 0">
            <label for="descricao">Observação</label>
            <textarea class="span12" rows="5" cols="5" id="observacaoimg" name="observacaoimg"></textarea>
        </div>

        <div class="span7">
            <input type="hidden" name="idVeiculoArquivo" id="idVeiculoArquivo" value=""/>
            <label for="">Fotos / Anexos*</label>
            <input type="file" class="span12" required="required" name="userfile[]" id="userfile" multiple="multiple" size="20"/>
        </div>

        <div class="span3">
            <label for="">.</label>
            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i>Anexar</button>
        </div>
    </form>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo" style="text-align: center">
            <div class='progress progress-info progress-striped active'>
                <div class='bar' style='width: 100%'></div>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">LINK</label>
            <input type="text" id="link-visualizar" readonly name="link-visualizar"
                   class="span12"/>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">Observação</label>
            <textarea class="span12" rows="5" readonly cols="5" id="observacaoimg-visualizar" name="observacaoimg-visualizar"></textarea>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="data-visualizar">Data</label>
            <input type="date" id="data-visualizar" readonly name="data-visualizar"
                   class="span4"/>

            <label for="hora-visualizar">Hora</label>
            <input type="time"  id="hora-visualizar"  readonly name="hora-visualizar"
                   class="span4"/>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
        <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
        <a href="" target="_blank" class="btn btn-invers" id="abrir-imagem">Abrir</a>
        <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/veiculo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        <?php if ($result->tipoPessoa == 'PJ') { ?>
            $('#tabContato').show();
            $('#div_sexo').hide();
            $('#div_rg').hide();

            $('#div_dataNascimento').html('Data da fundação');
            $('#div_nome').html('Razão Social*');
            $('#div_nomeFantasia').html('Nome Fantasia');
            $('#div_cpfCnpj').html('CNPJ / Nome*');

            $("#documento").mask("99.999.999/9999-99");
        <?php } else { ?>
            $('#tabContato').hide();
            $('#div_sexo').show();
            $('#div_rg').show();

            $('#div_dataNascimento').html('Data de nascimento');
            $('#div_nome').html('Nome*');
            $('#div_nomeFantasia').html('Apelido');
            $('#div_cpfCnpj').html('CPF / Nome*');

            $("#documento").mask("999.999.999-99");
        <?php } ?>

        <?php if($result->inativo == 1) {?>
            $('#tabBloqueio').show();
        <?php } else { ?>
            $('#tabBloqueio').hide();
        <?php }?>

        $('#contatoCpf').mask("999.999.999-99");

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#inativo').click(function (e) {
            if($(this).is(":checked")){
                $('#tabBloqueio').show();
            } else {
                $('#tabBloqueio').hide();
            }
        });

        $('#tipoveiculo_id').change(function (event) {
             $('#tipoVeiculo').val( $( "#tipoveiculo_id option:selected" ).text());
        });

        $('#formCliente').validate({
            rules: {
                nomeCliente: {required: true}
            },
            messages: {
                nomeCliente: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formFotoVeiculo").validate({
            submitHandler: function (form) {
                var dados = new FormData(form);
                $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/adicionarFotoVeiculo",
                    data: dados,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divVeiculos").load("<?php echo current_url();?> #divVeiculos");
                            $("#userfile").val('');
                            $('#observacaoimg').val('');
                            $('#idVeiculoArquivo').val('');
                            $('#cadFotoVeiculo').modal('hide');
                        }
                        else {
                            $("#divVeiculos").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> ' + data.mensagem + '</div>');
                        }
                    },
                    error: function () {
                        $("#divVeiculos").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você anexou o(s) arquivo(s).</div>');
                    }
                });
                return false;
            }
        });

        $(document).on('click', '.anexo', function (event) {
            event.preventDefault();
            var link            = $(this).attr('link');
            var id              = $(this).attr('imagem');
            var url             = '<?php echo base_url(); ?>index.php/clientes/excluirAnexo/';
            var data            =  $(this).attr('data');
            var hora            =  $(this).attr('hora');
            var observacaoimg   =  $(this).attr('observacaoimg');

            $("#div-visualizar-anexo").html('<img src="' + link + '" alt="">');
            $("#excluir-anexo").attr('link', url + id);

            $('#link-visualizar').val(link);
            $('#observacaoimg-visualizar').val(observacaoimg);
            $('#data-visualizar').val(data);
            $('#hora-visualizar').val(hora);
            $('#abrir-imagem').attr('href',link);
            $("#download").attr('href', "<?php echo base_url(); ?>index.php/clientes/downloadanexo/" + id);
        });

        $(document).on('click', '#excluir-anexo', function (event) {
            event.preventDefault();
            var link = $(this).attr('link');
            $('#modal-anexo').modal('hide');
            $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

            $.ajax({
                type: "POST",
                url: link,
                dataType: 'json',
                success: function (data) {
                    if (data.result == true) {
                        $("#divVeiculos").load("<?php echo current_url();?> #divVeiculos");
                    } else {
                        alert(data.mensagem);
                    }
                }
            });
        });

        $("#formVeiculosCliente").validate({
            rules: {
                modelo: {required: true},
                marca: {required: true},
                placa: {required: true},
                tipoVeiculo: {required: true}
            },
            messages: {
                modelo: {required: 'Insira um modelo'},
                marca: {required: 'Insira uma marca'},
                placa: {required: 'Insira uma placa'},
                tipoVeiculo: {required: 'Insira o tipo do veículo'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                var tipoCombustivel = false;

                if ($("#gasolina").is(":checked")){
                    tipoCombustivel = true;
                }

                if ($("#etanol").is(":checked")){
                    tipoCombustivel = true;
                }

                if ($("#diesel").is(":checked")){
                    tipoCombustivel = true;
                }

                if ($("#gnv").is(":checked")){
                    tipoCombustivel = true;
                }

                if (!tipoCombustivel) {
                    alert("Tipo de combustível é um campo obrigatório!");
                    return;
                }

                $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/adicionarVeiculo",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divVeiculos").load("<?php echo current_url();?> #divVeiculos");
                            $("#marca").val('');
                            $("#placa").val('');
                            $("#tipoVeiculo").val('');
                            $('#tipoveiculo_id').val('');
                            $("#cor").val('');
                            $("#ano").val('');
                            $('#idVeiculo').val('');
                            $('#chassi').val('');
                            $('#observacao').val('');
                            $('#municipio').val('');
                            $('#uf').val('');
                            $('#situacao').val('');
                            $('#gasolina').prop( "checked", false );
                            $('#etanol').prop( "checked", false );
                            $('#diesel').prop( "checked", false );
                            $('#gnv').prop( "checked", false );
                            $("#modelo").val('').focus();

                            $('#situacao').css('background','#eee');
                            liberado = false;
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar o veliculo.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', 'span', function (event) {

            var idVeiculo = $(this).attr('idAcao');
            var idAcaoEditar = $(this).attr('idAcaoEditar');

            if ((idVeiculo % 1) === 0) {
                $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/excluirVeiculo",
                    data: "idVeiculo=" + idVeiculo,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divVeiculos").load("<?php echo current_url();?> #divVeiculos");
                            $("#marca").val('');
                            $("#placa").val('');
                            $("#tipoVeiculo").val('');
                            $('#tipoveiculo_id').val('');
                            $("#cor").val('');
                            $("#ano").val('');
                            $('#idVeiculo').val('');
                            $('#chassi').val('');
                            $('#observacao').val('');
                            $('#municipio').val('');
                            $('#uf').val('');
                            $('#situacao').val('');
                            $('#gasolina').prop( "checked", false );
                            $('#etanol').prop( "checked", false );
                            $('#diesel').prop( "checked", false );
                            $('#gnv').prop( "checked", false );

                            $("#modelo").val('').focus();

                            $('#situacao').css('background','#eee');
                            liberado = false;
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir o veiculo.');
                        }
                    }
                });
                return false;
            } else if (idAcaoEditar != '' && idAcaoEditar != undefined) {

                var marca           = $(this).attr('marca');
                var placa           = $(this).attr('placa');
                var tipoVeiculo     = $(this).attr('tipoVeiculo');
                var tipoveiculo_id  = $(this).attr('tipoveiculo_id');
                var cor             = $(this).attr('cor');
                var ano             = $(this).attr('ano');
                var modelo          = $(this).attr('modelo');
                var chassi          = $(this).attr('chassi');
                var observacao      = $(this).attr('observacao');
                var municipio       = $(this).attr('municipio');
                var uf              = $(this).attr('uf');
                var situacao        = $(this).attr('situacao');

                var gasolina        = $(this).attr('gasolina');
                var etanol          = $(this).attr('etanol');
                var diesel          = $(this).attr('diesel');
                var gnv             = $(this).attr('gnv');

                $('#gasolina').prop( "checked", false );
                $('#etanol').prop( "checked", false );
                $('#diesel').prop( "checked", false );
                $('#gnv').prop( "checked", false );

                if (situacao != 'Sem restrição') {
                    $('#situacao').css('background','red');
                    $('#situacao').val(situacao);
                    liberado = true;
                    piscarVermelho();
                } else {
                    $('#situacao').css('background','#71d80036');
                    liberado = false;
                }

                if (gasolina==1){
                    $('#gasolina').prop( "checked", true );
                }

                if (etanol==1){
                    $('#etanol').prop( "checked", true );
                }

                if (diesel==1){
                    $('#diesel').prop( "checked", true );
                }

                if (gnv==1){
                    $('#gnv').prop( "checked", true );
                }

                $("#marca").val(marca);
                $("#placa").val(placa);
                $("#tipoVeiculo").val(tipoVeiculo);
                $("#cor").val(cor);
                $("#ano").val(ano);
                $('#idVeiculo').val(idAcaoEditar);
                $('#tipoveiculo_id').val(tipoveiculo_id);
                $("#modelo").val(modelo).focus();
                $("#chassi").val(chassi).focus();
                $("#observacao").val(observacao).focus();
                $('#municipio').val(municipio);
                $('#uf').val(uf);
                $('#situacao').val(situacao);
            }
        });

        $('.consulta_placa').blur(function (event) {
            var placa = $(this).val();
            consulta_veiculo_sinesp(placa,'<?php echo base_url();?>');
        });

        $('.consulta_placa_click').click(function (event) {
            var placa = $('#placa').val();
            if (placa != '') {
                consulta_veiculo_sinesp(placa,'<?php echo base_url();?>');
            } else {
                alert('Digite uma placa para realizar a consulta.');
            }
        });
    });

    function vincularIdentificadorVeiculo(idVeiculo) {
        $('#idVeiculoArquivo').val(idVeiculo);
    }

</script>

