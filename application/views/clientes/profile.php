<div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <h2><?php echo $result->nomeCliente;?><small>relatório financeiro</small></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php if ($result->inativo){?>
                <div class="col-md-12 col-sm-12">
                    <div class="alert alert-danger alert-dismissible " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><i class="fa fa fa-warning"></i> Atenção! Cliente bloqueado!</strong> <br/> <?php echo $result->observacaoInativo;?>
                    </div>
                </div>
            <?php }?>
            <div class="col-md-12 col-sm-12 ">
                <div class="profile_title" style="display:none;">
                    <div class="col-md-6">
                        <h2>User Activity Report</h2>
                    </div>
                    <div class="col-md-6">
                        <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                        </div>
                    </div>
                </div>
                <!-- start of user-activity-graph -->
                <div id="graph_bar" style="width:100%; height:280px;display: none;"></div>
                <!-- end of user-activity-graph -->

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"> <i class="fa fa-money"></i> Gerenciar Faturas</a></li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-user"></i> Dados do cliente</a></li>
                        <li role="presentation" class="" style="display: none;"><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane active " id="tab_content1" aria-labelledby="home-tab">
                            <iframe src="<?php echo base_url();?>financeiro/faturas?clienteId=<?php echo $result->idClientes;?>" style="width: 100%;height: 2000px;" frameborder="0"></iframe>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="tab_content2" aria-labelledby="home-tab">
                            <div class="panel-body">
                                <div class="col-md-12 col-sm-12  profile_left">
                                    <div class="profile_img" style="text-align: center;display: none">
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <img class="img-responsive avatar-view" style="width: 70%;" src="<?php echo base_url() ?>assets/arquivos/sem_foto.png" width="40%" alt="Avatar" title="Change the avatar">
                                        </div>
                                    </div>
                                    <br/>
                                    <h4 style="text-align: center;"> <?php echo $result->nomeFantasiaApelido;?></h4>
                                    <ul class="list-unstyled user_data">
                                        <?php if ($result->tipoPessoa == 'PJ'){?>
                                            <li> <i class="fa fa-building user-profile-icon"></i> Pessoa Jurídica</li>
                                        <?php } else { ?>
                                            <?php if ($result->sexo == 'M'){?>
                                                <li> <i class="fa fa-male user-profile-icon"></i> Marculino</li>
                                            <?php } else {?>
                                                <li> <i class="fa fa-female user-profile-icon"></i> Feminíno</li>
                                            <?php }?>
                                        <?php } ?>

                                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo $result->rua.' '.$result->numero.' - '.$result->bairro.' - '.$result->cidade.'/'.$result->estado.' CEP: '.$result->cep;?>
                                        </li>
                                        <li>
                                            <i class="fa fa-folder-open user-profile-icon"></i> <?php echo $result->documento;?>
                                        </li>
                                        <li>
                                            <i class="fa fa-phone user-profile-icon"></i> <a href="tel:<?php echo $result->telefone;?>" target="_blank"> <?php echo $result->telefone;?></a>
                                        </li>
                                        <li>
                                            <i class="fa fa-whatsapp user-profile-icon"></i>  <a href="https://api.whatsapp.com/send?phone=55<?php echo $result->celular;?>" target="_blank">  <?php echo $result->celular;?> </a>
                                        </li>

                                        <li>
                                            <i class="fa fa-envelope user-profile-icon"></i>  <a href="mailto:<?php echo $result->email;?>" target="_blank"> <?php echo $result->email;?> </a>
                                        </li>
                                        <?php if ($result->observacao != ''){?>
                                            <li>
                                                <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $result->observacao;?>
                                            </li>
                                        <?php }?>

                                        <li class="m-top-xs" style="display: none;">
                                            <i class="fa fa-external-link user-profile-icon"></i>
                                            <a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>
                                        </li>
                                    </ul>
                                    <div class="ln_solid"></div>
                                    <div style="text-align: right;">
                                        <a href="<?php echo base_url() . 'clientes/editar/' . $result->idClientes;?>" class="btn btn-primary"><i class="fa fa-edit m-right-xs"></i> Editar</a>
                                        <a href="<?php echo base_url() . 'clientes/';?>" class="btn btn-secondary"><i class="fa fa-backward m-right-xs"></i> Voltar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>