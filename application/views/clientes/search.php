<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_content">
                <form  action="<?php echo base_url(); ?>index.php/clientes/search" id="formSearch" method="post">
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span12" style="margin-left: 0">
                            <div class="span8" style="margin-left: 0">
                                <input type="text" class="form-control" name="termo" value="<?php echo $termo;?>" placeholder="Digite o termo a pesquisar"/>
                            </div>
                            <div class="span4">
                                <button class="btn btn-primary"><i class="fa fa-search"></i> Pesquisar</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="span12" style="margin-left: 0">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead class="thead_class">
                                <tr>
                                    <th style="text-align: left;width: 250px;">Nome</th>
                                    <th style="text-align: left;width: 250px;">CPF/CNPJ</th>
                                    <th style="text-align: left;width: 250px;">Telefone</th>
                                </tr>
                                </thead>
                                <tbody class="tbody_class">
                                <?php foreach ($results as $r) {

                                    $inativo = $r->inativo;
                                    $observacaoInativo  = $r->observacaoInativo;

                                    $background = '';
                                    if ($inativo == 1) {
                                        $background = '#ee5f5b59';
                                        $inativo = 'sim';
                                    } else {
                                        $inativo = 'não';
                                    }

                                    echo '<tr class="searchPreenche" cliente_id='.$r->idClientes.' style="background: '.$background.';cursor: pointer;">';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->nomeCliente . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->documento . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->telefone . '</td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>

                            <?php echo $this->pagination->create_links();?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    $('.create_links').click(function (event) {
        var url = $(this).attr('href');
        if (url !== undefined) {
            $('#div_searchCliente').load(url);
        }
        return false;
    });

    $('#formSearch').submit(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/clientes/search",
            data: $("#formSearch").serialize(),
            dataType: 'html',
            success: function (html) {
                $('#div_searchCliente').html(html);
            }
        });
    });

    $('.searchPreenche').click(function (event) {
        var cliente_id = $(this).attr('cliente_id');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/clientes/consultaCliente",
            data: {cliente_id:cliente_id},
            dataType: 'json',
            success: function (cliente) {
                $('#clientes_id option[value='+cliente.idClientes+']').remove();
                $('#clientes_id').append('<option value="'+cliente.idClientes+'">' + cliente.nomeCliente + '</option>');
                $('#clientes_id').val(cliente.idClientes);

                //funcao apos selecionar cliente
                fecharModelSercheCliente(cliente);
            }
        });
    });
</script>