<div class="widget-box">
    <div class="widget-title">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab1">Dados do Cliente</a></li>
            <li><a data-toggle="tab" href="#tab2">Ordens de Serviço</a></li>
            <li><a data-toggle="tab" href="#tab3">Vendas</a></li>

            <div class="buttons">
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eCliente')){
                        echo '<a class="btn btn-mini btn-info" href="'.base_url().'index.php/clientes/editar/'.$result->idClientes.'"><i class="icon-pencil icon-white"></i> Editar</a>';
                    } ?>
            </div>
        </ul>
    </div>
    <div class="widget-content tab-content">

        <div id="tab1" class="tab-pane active" style="min-height: 300px">
            <div class="accordion" id="collapse-group">
                            <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                                            <span class="icon"><i class="icon-list"></i></span>
                                            <?php if ($result->tipoPessoa== 'PF') {?>
                                                <h5>Dados Pessoais</h5>
                                            <?php } else { ?>
                                                <h5>Dados Empresariais</h5>
                                            <?php } ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse in accordion-body" id="collapseGOne">
                                    <div class="widget-content">
                                        <table class="table table-bordered">
                                            <tbody>

                                                <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Código</strong></td>
                                                    <td>
                                                        <?php echo str_pad($result->idClientes, 4, '0', STR_PAD_LEFT); ?>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Tipo</strong></td>
                                                    <td>
                                                        <?php if ($result->tipoPessoa== 'PJ') {?>
                                                            Pessoa Jurídica
                                                        <?php } else { ?>
                                                            Pessoa Fisíca
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php if ($result->tipoPessoa== 'PF') {?>
                                                    <tr>
                                                        <td style="text-align: right; width: 30%"><strong>Sexo</strong></td>
                                                        <td>
                                                            <?php if ($result->sexo== 'M') {?>
                                                                Masculino
                                                            <?php } else { ?>
                                                                Feminino
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                 <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Origem</strong></td>
                                                     <td><?php echo $result->origem ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Nome / Razão social</strong></td>
                                                    <td><?php echo $result->nomeCliente ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Nome Fantasia / Apelido</strong></td>
                                                    <td><?php echo $result->nomeFantasiaApelido ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right"><strong>CPF/CNPJ</strong></td>
                                                    <td>
                                                        <?php if ($result->tipoPessoa== 'PJ') {?>
                                                            <?php echo $result->documento ?>
                                                        <?php } else {

                                                            $dataOrgaoEmissor = '';
                                                            if ($result->dataOrgaoEmissor){
                                                                $dataOrgaoEmissor = date('d/m/Y',  strtotime($result->dataOrgaoEmissor));
                                                            }
                                                            ?>
                                                            <?php echo $result->documento.' Org.Emissor '.$result->orgaoEmissor.'/'.$result->estadoOrgaoEmissor.' Data emissão '.$dataOrgaoEmissor?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><strong>Data de Cadastro</strong></td>
                                                    <td><?php echo date('d/m/Y',  strtotime($result->dataCadastro)) ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right"><strong>Observação</strong></td>
                                                    <td><?php echo $result->observacao; ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right"><strong>Cliente bloqueado?</strong></td>
                                                    <?php if ($result->inativo == 1){?>
                                                        <td><?php echo 'sim'; ?></td>
                                                    <?php } else { ?>
                                                        <td><?php echo 'não'; ?></td>
                                                    <?php } ?>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right"><strong>Motivo pelo bloqueio</strong></td>
                                                    <td><?php echo $result->observacaoInativo; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">
                                            <span class="icon"><i class="icon-list"></i></span><h5>Contatos</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse accordion-body" id="collapseGTwo">
                                    <div class="widget-content">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Telefone</strong></td>
                                                    <td><?php echo $result->telefone ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><strong>Celular</strong></td>
                                                    <td><?php echo $result->celular ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><strong>Email</strong></td>
                                                    <td><?php echo $result->email ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <?php if ($result->tipoPessoa== 'PJ') {?>
                                <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#pessoaContato" data-toggle="collapse">
                                            <span class="icon"><i class="icon-list"></i></span><h5>Pessoa para Contato</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse accordion-body" id="pessoaContato">
                                    <div class="widget-content">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td style="text-align: right; width: 30%"><strong>Nome</strong></td>
                                                <td><?php echo $result->contatoNomeCliente ?></td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right; width: 30%"><strong>Sexo</strong></td>
                                                <td>
                                                    <?php if ($result->contatoSexo== 'M') {?>
                                                        Masculino
                                                    <?php } else { ?>
                                                        Feminino
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                            <?php if ($result->contatoDataNascimento) {?>
                                                <tr>
                                                    <td style="text-align: right"><strong>Data de nascimento</strong></td>
                                                    <td><?php echo date('d/m/Y',  strtotime($result->contatoDataNascimento)) ?></td>
                                                </tr>
                                            <?php } ?>

                                            <tr>
                                                <td style="text-align: right; width: 30%"><strong>CPF</strong></td>
                                                <td><?php echo $result->contatoCpf ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 30%"><strong>Telefone</strong></td>
                                                <td><?php echo $result->contatoTelefone ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right"><strong>Celular</strong></td>
                                                <td><?php echo $result->contatoCelular ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right"><strong>Email</strong></td>
                                                <td><?php echo $result->contatoEmail ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse">
                                            <span class="icon"><i class="icon-list"></i></span><h5>Endereço</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse accordion-body" id="collapseGThree">
                                    <div class="widget-content">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: right; width: 30%"><strong>Rua</strong></td>
                                                    <td><?php echo $result->rua ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><strong>Número</strong></td>
                                                    <td><?php echo $result->numero ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right"><strong>Complemento</strong></td>
                                                    <td><?php echo $result->complemento ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right"><strong>Bairro</strong></td>
                                                    <td><?php echo $result->bairro ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><strong>Cidade</strong></td>
                                                    <td><?php echo $result->cidade ?> - <?php echo $result->estado ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><strong>CEP</strong></td>
                                                    <td><?php echo $result->cep ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
							
							
							
							<div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#collapseG4" data-toggle="collapse">
                                            <span class="icon"><i class="icon-list"></i></span><h5>Veículos</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse accordion-body" id="collapseG4">
                                    <div class="widget-content">
                                        <table class="table table-bordered">
                                            <tbody>
												<?php foreach ($veiculos as $veiculo) {
                                                    $strSituacao = '';
                                                    $tipoCumbustivel = '';

                                                    if($veiculo->situacao != 'Sem restrição') {
                                                        $strSituacao = '<font color="red">'.$veiculo->situacao.'</font>';
                                                    } else {
                                                        $strSituacao = '<font color="#5bc0de">'.$veiculo->situacao.'</font>';
                                                    }

                                                    if ($veiculo->gasolina == 1) {
                                                        $tipoCumbustivel = '<br/>Gasolina';
                                                    }

                                                    if ($veiculo->etanol == 1) {
                                                        $tipoCumbustivel .= '<br/>Etanol';
                                                    }

                                                    if ($veiculo->diesel == 1) {
                                                        $tipoCumbustivel .= '<br/>Diesel';
                                                    }

                                                    if ($veiculo->gnv == 1) {
                                                        $tipoCumbustivel .= '<br/>Gás veicular (GNV)';
                                                    }
												    ?>
													<tr>
														<td style="text-align: right; width: 30%"><strong>Veículo</strong></td>
														<td>
															<?php echo 	'Tipo:'.$veiculo->tipoVeiculo.'<br/>Marca:'.$veiculo->marca.'<br/>Modelo:'.$veiculo->modelo.'<br/>Placa:'.$veiculo->placa.'<br/>Cor:'.$veiculo->cor.'<br/>Ano:'.$veiculo->ano.'<br/>Combustível(is):'.$tipoCumbustivel.'<br/>CHASSI '.$veiculo->chassi.'<br/>Situação:'.$strSituacao.'<br/>'.$veiculo->observacao;?>
														</td>
													</tr>
												<?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
							
							
                        </div>
        </div>


        <!--Tab 2-->
        <div id="tab2" class="tab-pane" style="min-height: 300px">
            <?php if (!$results) { ?>
                        <table class="table table-bordered ">
                            <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>#</th>
                                    <th>Data Inicial</th>
                                    <th>Data Final</th>
                                    <th>Descricao</th>
                                    <th>Defeito</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">Nenhuma OS Cadastrada</td>
                                </tr>
                            </tbody>
                        </table>
                
                <?php } else { ?>

                        <table class="table table-bordered ">
                            <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>#</th>
                                    <th>Data Inicial</th>
                                    <th>Data Final</th>
                                    <th>Descricao</th>
                                    <th>Defeito</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                foreach ($results as $r) {
                    $dataInicial = date(('d/m/Y'), strtotime($r->dataInicial));
                    $dataFinal = date(('d/m/Y'), strtotime($r->dataFinal));
                    echo '<tr>';
                    echo '<td>' . $r->idOs . '</td>';
                    echo '<td>' . $dataInicial . '</td>';
                    echo '<td>' . $dataFinal . '</td>';
                    echo '<td>' . $r->descricaoProduto . '</td>';
                    echo '<td>' . $r->defeito . '</td>';

                    echo '<td>';
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){
                        echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top" ><i class="icon-eye-open"></i></a>';
                    }
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                        echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    }
                    
                    echo  '</td>';
                    echo '</tr>';
                } ?>
                            <tr>

                            </tr>
                        </tbody>
                    </table>
            

            <?php  } ?>

        </div>
		
		
		
		        <!--Tab 2-->
        <div id="tab3" class="tab-pane" style="min-height: 300px">
            <?php if (!$vendas) { ?>
                
                        <table class="table table-bordered ">
                            <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>#</th>
                                    <th>Data da Venda</th>
                                    <th>Valor Total</th>
                                    <th>Faturada</th>
									<th></th>
                                 </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td colspan="5">Nenhuma Venda Cadastrada</td>
                                </tr>
                            </tbody>
                        </table>
                
                <?php } else { ?>

                        <table class="table table-bordered ">
                            <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>#</th>
                                    <th>Data da Venda</th>
                                    <th>Valor Total</th>
                                    <th>Faturada</th>
									<th></th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                foreach ($vendas as $r) {
                    $dataVenda = date(('d/m/Y'), strtotime($r->dataVenda));
                    echo '<tr>';
                    echo '<td>' . $r->idVendas . '</td>';
                    echo '<td>' . $dataVenda . '</td>';
                    echo '<td>' . $r->valorTotal . '</td>';
                    echo '<td>' . $r->faturado . '</td>';

                    echo '<td>';
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){
                        echo '<a href="' . base_url() . 'index.php/vendas/visualizar/' . $r->idVendas . '" style="margin-right: 1%" class="btn tip-top" ><i class="icon-eye-open"></i></a>';
                    }
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                        echo '<a href="' . base_url() . 'index.php/vendas/editar/' . $r->idVendas . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    }
                    
                    echo  '</td>';
                    echo '</tr>';
                } ?>
                            <tr>

                            </tr>
                        </tbody>
                    </table>
            

            <?php  } ?>

        </div>
    </div>
</div>