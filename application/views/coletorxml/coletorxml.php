<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Importação Manual de XML <small>coletar notas manualmente</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo base_url() ?>index.php/coletorxml/uploadXMLNF" enctype="multipart/form-data" method="post">
                    <div class="row well">
                        <div class="col-md-6 col-sm-6">
                            <input type="file" multiple name="files[]" id="files[]" accept="text/xml"  required="required">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <button class="btn btn-warning" id="excel"><i class="fa fa-upload"></i> UPLOAD XMLS</button><small>Máximo 20</small>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12" >
                        <div class="alert alert-danger alert-dismissible " role="alert"  style="margin-top: 10px;">
                            <strong>Atenção!!!</strong><br/>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <input type="checkbox" style="margin-top: -3px;" value="1" id="importarNotasSaida" name="importarNotasSaida"/>
                            Clique neste campo quando for Importar apenas as Notas Fiscais <b>Emitidas</b> pelo seu próprio CNPJ para um cliente, não clique para importar notas de fornecedores!<br/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sitemap"></i> Coletor de XML's</small></h2>
                <ul class="nav navbar-right panel_toolbox">

                    <a href="#" id="baixarxml" class="btn btn-primary">
                        <i class="fa fa-download"></i> Coletar
                    </a>

                    <a href="#" id="manifestarxml" class="btn btn-warning">
                        <i class="fa fa-inbox"></i> Manifestar
                    </a>

                    <a href="#" id="donwloadxml" class="btn btn-success">
                        <i class="fa fa-folder-open"></i> Baixar XML
                    </a>

                    <a href="#" id="cadastrarXML" class="btn btn-info">
                        <i class="fa fa-save"></i> Cadastrar XML
                    </a>

                    <a href="#" id="imprimir" class="btn btn-dark">
                        <i class="fa fa-print"></i> Imprimir DANFE
                    </a>
                </ul>
                <div class="clearfix"></div>
                <div class="alert alert-info alert-dismissible " role="alert"  style="margin-top: 10px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Atenção!</strong><br/>
                    Pode haver um periodo de até 30 minutos para liberar o donwload do XML após manifestação da nota fiscal na Receita Federal.<br/>
                    Havendo urgência você pode baixar o xml diretamente no website da fazendo <a href=http://www.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=completa target="_blank">CLIQUE AQUI</a> usando a chave de acesso, baixe e importe o xml para o sistema.
                </div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th><input name="" type="checkbox" value="0" onclick="marcarTodas();" id="marcarTodos" /></th>
                                    <th>Nº</th>
                                    <th>Data</th>
                                    <th>Fornecedor</th>
                                    <th style="text-align: right;">Valor</th>
                                    <th style="text-align: center;">Manifestada</th>
                                    <th style="text-align: center;">Donwload</th>
                                    <th style="text-align: center;">Importada</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {

                                    $chave = $r->chave;
                                    $numero = substr($chave, 28, 6);

                                    echo '<tr>';
                                    echo '    <td><input class="elementSelected" type="checkbox" importada="'.$r->importada.'" donwload="'.$r->donwload.'"  value="'.$r->manifestaid.'"></td>';
                                    echo '    <td>' . $numero . '</td>';
                                    echo '    <td>' . $r->data . '</td>';
                                    echo '    <td>' . $r->xNome . '</td>';
                                    echo '    <td style="text-align: right;">R$' . $this->site_model->formatarValorMonetario($r->valor) . '</td>';
                                    echo '    <td style="text-align: center;">' . $r->manifestada . '</td>';
                                    echo '    <td style="text-align: center;">' . $r->donwload . '</td>';
                                    echo '    <td style="text-align: center;">' . $r->importada . '</td>';
                                    echo '<td style="text-align: center;">';

                                    if($r->importada == 'S'){

                                        $produtos = $this->coletorxml_model->getProdutosNotaFiscalFornecedorAtualizados($chave);

                                        if (count($produtos) > 0) echo '<a style="margin-right: 1%" href="'.base_url().'index.php/nfefornecedor/editar/'.$r->chave.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-thumbs-o-up"> </i> </button></a>';
                                        else  echo '<a style="margin-right: 1%" href="'.base_url().'index.php/nfefornecedor/editar/'.$r->chave.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">

    var TENTATIVAS          = 5;
    var contadorBaixarXML   = 0;
    var contadorManifesta   = 0;
    var contadorDonwloadXML = 0;

    $(document).ready(function () {

        $('#imprimir').click(function (event) {
            var elemento = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfeManifesta.php?arquivo='+$(elemento).get(0)+ '', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#baixarxml').click(function () {
            verificarNFFornecedor();
        });

        $('#manifestarxml').click(function () {

            var elemento  = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                var ids = '';

                for(var i=0;i<elemento.length;i++) ids += elemento[i]+',';

                manifestar(ids);
            }
        });

        $('#donwloadxml').click(function () {
            var elemento  = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {

                var ids = '';
                for(var i=0;i<elemento.length;i++)  ids += elemento[i]+',';

                donwloadXML(ids);
            }
        });

        $('#cadastrarXML').click(function (event) {
            var elemento  = new Array();
            var importadas = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                importadas.push($(this).attr('importada'));
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                cadatrarXMLs(elemento, importadas);
            }
        });
    });

    function cadatrarXMLs(elemento, importada) {
        var ids = '';
        for(var i=0;i<elemento.length;i++) {
            if (importada[i] === 'N')  ids += elemento[i]+',';
        }

        cadastrarXML(ids);
    }

    function cadastrarXML(idsmanifesto) {

        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde,  Realizando o Cadastro do XML....</h3>'});

        var importarNotasSaida = 0;

        if ($("#importarNotasSaida").is(":checked")) importarNotasSaida = 1;
        else importarNotasSaida = 0;

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/ImpXMLFornecedor.php?manifestaid="+idsmanifesto+'&importarNotasSaida='+importarNotasSaida,
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    mensagemDeSucesso('Sucesso!',data.msg);
                } else {
                    mensagemDeErro('Error!', data.msg);
                }
            }
        });
    }

    function verificarNFFornecedor() {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde, Buscando XMLs emitidos pelo Fornecedor. Esse processo pode demorar alguns minutos...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/dfe.php",
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    mensagemDeSucesso('Sucesso!',data.msg);
                } else {
                    if (contadorBaixarXML >= TENTATIVAS) {
                        mensagemDeErro('Error!', data.msg);
                    } else {
                        verificarNFFornecedor();
                        contadorBaixarXML = contadorBaixarXML + 1;
                    }
                }
            }
        });
    }

    function manifestar(idsManifesto) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde, Manifestando o recebimentos das notas. Esse processo pode demorar alguns minutos...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/manifesta.php?idManifesto="+idsManifesto,
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    mensagemDeSucesso('Sucesso!',data.msg);
                } else {

                    if (contadorManifesta >= TENTATIVAS) {
                        mensagemDeErro('Error!', data.msg);
                    } else {
                        manifestar(idsManifesto);
                        contadorManifesta = contadorManifesta + 1;
                    }
                }
            }
        });
    }

    function donwloadXML(idsManifesto) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde, Realizando o Donwload do XML para o servidor. Esse processo pode demorar alguns minutos...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/donwload.php?idManifesto="+idsManifesto,
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    mensagemDeSucesso('Sucesso!',data.msg);
                } else {
                    if (contadorDonwloadXML >= TENTATIVAS) {
                        mensagemDeErro('Error!', data.msg);
                    } else {
                        donwloadXML(idsManifesto);
                        contadorDonwloadXML = contadorDonwloadXML + 1;
                    }
                }
            }
        });
    }

    function marcarTodas() {
        if ($('#marcarTodos').prop("checked")){
            $('.elementSelected').each(
                function(){
                    $(this).attr("checked", true);
                    $(this).prop("checked", true);
                }
            );
        }else{
            $('.elementSelected').each(
                function(){
                    $(this).attr("checked", false);
                    $(this).prop("checked", false);
                }
            );
        }
    }
</script>