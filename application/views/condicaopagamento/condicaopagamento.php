<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sort-numeric-desc"></i> Condições de pagamento (<?php echo count($results); ?>)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a class="btn btn-success" href="<?php echo base_url(); ?>condicaopagamento/adicionar"><i class="fa fa-plus"></i> Cadastrar Condição de pagamento</a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="width: 70%;">Nome</th>
                                    <th>Nº parcelas</th>
                                    <th style="width: 10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    echo '<tr>';
                                    echo '    <td>' . $r->nome . '</td>';
                                    echo '    <td>' . $r->parcelas . '</td>';
                                    echo '    <td>';
                                    echo '        <div class="btn-group btn-group-sm" role="group" aria-label="...">';
                                    echo '          <a href="' . base_url() . 'index.php/condicaopagamento/editar/' . $r->idCondicaoPagamento . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    echo '          <a href="#modal-excluir" role="button" data-toggle="modal" condicaopagamento="' . $r->idCondicaoPagamento . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>  ';
                                    echo '        </div>';
                                    echo '    </td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/condicaopagamento/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Condição de pagamento</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idCondicaoPagamento" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir esta condição de pagamento?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var condicaopagamento = $(this).attr('condicaopagamento');
            $('#idCondicaoPagamento').val(condicaopagamento);
        });
    });
</script>