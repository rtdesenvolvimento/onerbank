<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>.:<?php echo TITLE_SISTEMA;?>:.</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-login.css"/>
    <link href="<?php echo base_url() ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <script src="<?php echo base_url() ?>js/jquery-1.10.2.min.js"></script>
</head>
<body>
<div id="loginbox">
    <form class="form-vertical" id="formLogin" method="post" action="<?php echo base_url() ?>index.php/conecte/login">
        <?php if ($this->session->flashdata('error') != null) { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php } ?>
        <div class="control-group normal_text"><h3><img src="<?php echo base_url() ?>assets/img/<?php echo PACOTE;?>/logo.png" alt="Logo"/>
            </h3></div>

        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"><i class="icon-building"></i></span>
                    <input id="cnpjempresa" readonly required="required" name="cnpjempresa" value="<?php echo $this->uri->segment(3);?>" type="text" placeholder="CNPJ Empresa"/>
                </div>
            </div>
        </div>


        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"><i class="icon-user"></i></span>
                    <input name="cpfCnpj" id="cpfCnpj" type="text" placeholder="Digite seu CPF ou CPNJ"/><br/>
                </div>
            </div>
        </div>

        <div class="controls">
            <div class="main_input_box">
                <span class="add-on bg_lg"><i class="icon-lock"></i></span><input name="senha" required="required" type="password"
                                                                                  placeholder="Senha"/>
            </div>
        </div>

        <div class="form-actions" style="text-align: center">
            <button class="btn btn-info btn-large"/>
            Acessar</button>
        </div>
    </form>

</div>

<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $('#email').focus();

        $('#cpfCnpj').keyup(function() {
            $(this).val(this.value.replace(/\D/g, ''));
        });

        $('#cpfCnpj').click(function () {
            $(this).unmask();
            var valor = $(this).val();
            valor = replaceAll('.', '',valor)
            valor = replaceAll('-', '',valor)
            valor = replaceAll('/', '',valor)
            $(this).val(valor);
        });

        $("#cpfCnpj").blur(function() {
            var valor = $(this).val();
            var qtdLetras  = valor.length;
            if (qtdLetras === 14) {
                if (!valida_cnpj( $(this).val())) {
                    alert('CNPJ inválido!');
                    $(this).val('');
                    return false;
                } else {
                    $(this).mask("99.999.999/9999-99"); // CNPJ
                }

            } else if (qtdLetras === 11){
                if (!valida_cpf( $(this).val())) {
                    alert('CPF inválido!');
                    $(this).val('');
                    return false;
                } else {
                    $(this).mask("999.999.999-99"); // CPF
                }
            } else if (valor  != ''){
                alert('CPF/CNPJ inválido!');
            }
        });

        $("#formLogin").validate({
            rules: {
                email: {required: true, email: true},
                senha: {required: true}
            },
            messages: {
                email: {required: 'Campo Requerido.', email: 'Insira Email válido'},
                senha: {required: 'Campo Requerido.'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();


                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/conecte/logincpf?ajax=true",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            window.location.href = "<?php echo base_url();?>index.php/conecte/painel";
                        }
                        else {
                            $('#call-modal').trigger('click');
                        }
                    }
                });
                return false;
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

    });

    function replaceAll(find, replace, str)
    {
        while( str.indexOf(find) > -1)
        {
            str = str.replace(find, replace);
        }
        return str;
    }

</script>

<a href="#notification" id="call-modal" role="button" class="btn" data-toggle="modal" style="display: none ">notification</a>
<div id="notification" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">.:<?php echo NOME_SISTEMA;?>:.</h4>
    </div>
    <div class="modal-body">
        <h5 style="text-align: center">Os dados de acesso estão incorretos, por favor tente novamente!</h5>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Fechar</button>

    </div>
</div>


</body>

</html>