<?php

if (!$results) {
    ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Ordens de Serviço</h5>

        </div>

        <div class="widget-content nopadding">


            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Responsável</th>
                    <th>Data Inicial</th>
                    <th>Data Final</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td colspan="6">Nenhuma OS Cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>


    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Ordens de Serviço</h5>

        </div>

        <div class="widget-content nopadding">


            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Nº O.S</th>
                    <th>Responsável</th>
                    <th>Data Inicial</th>
                    <th>Data Final</th>
                    <th>Status</th>
                    <th>Estágio</th>
                    <th>Detalhes</th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    $setor_nome     = '';
                    $dados_cliente  = $r->nome;
                    $veiculo_id     = $r->veiculo_id;
                    $veiculoStr     = '';

                    if ($r->setorAtual_id) {
                        $setor = $this->db->get_where('setor' , array('idSetor' => $r->setorAtual_id ))->row();
                        if (count($setor) > 0) {
                            $setor_nome = $setor->nome;
                        }
                    }

                    if ($veiculo_id) {
                        $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                        if (count($veiculo) > 0) {
                            $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                        }
                    }

                    $dataInicial    = date(('d/m/Y'), strtotime($r->dataInicial));
                    $dataFinal      = '-';

                    if ($r->dataFinal != '0000-00-00') {
                        $dataFinal = date(('d/m/Y'), strtotime($r->dataFinal));
                    }

                    echo '<tr>';
                    echo '<td style="text-align: center;width: 5%">' . $r->idOs . '</td>';
                    echo '<td style="text-align: center;">' . $dados_cliente .'</td>';
                    echo '<td style="text-align: center">' . $dataInicial . '</td>';
                    echo '<td style="text-align: center">' . $dataFinal . '</td>';
                    echo '<td style="text-align: center">' . $r->status . '</td>';
                    echo '<td style="text-align: center">Veículo <b>' . $veiculoStr.'</b><br/>encontra-se na <b>'.$setor_nome . '</b></td>';

                    echo '<td style="text-align: center"><a href="' . base_url() . 'index.php/conecte/visualizarOs/' . $r->idOs . '" class="btn tip-top"><i class="icon-eye-open"></i></a></td>';
                    echo '</tr>';
                } ?>
                <tr>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php echo $this->pagination->create_links();
} ?>
