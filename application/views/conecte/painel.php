
  <div class="quick-actions_homepage">
    <ul class="quick-actions">
      <li class="bg_lo span3"> <a href="<?php echo base_url()?>index.php/conecte/os"> <i class="icon-tags"></i> Ordens de Serviço</a> </li>
      <li class="bg_ls span3"> <a href="<?php echo base_url()?>index.php/conecte/compras"><i class="icon-shopping-cart"></i> Compras</a></li>
      <li class="bg_lg span3"> <a href="<?php echo base_url()?>index.php/conecte/conta"><i class="icon-star"></i> Minha Conta</a></li>
    </ul>
  </div>
 

  <div class="span12" style="margin-left: 0">
      
      <div class="widget-box">
          <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Últimas Ordens de Serviço</h5></div>
          <div class="widget-content">
              <table class="table table-responsive">
                  <thead>
                      <tr>
                          <th>Nº O.S</th>
                          <th>Data Inicial</th>
                          <th>Data Final</th>
                          <th>Garantia</th>
                          <th>Status</th>
                          <th>Estágio</th>
                          <th>Detalhes</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php 
                      if($os != null){
                          foreach ($os as $o) {

                              $setor_nome     = '';
                              $veiculo_id     = $o->veiculo_id;
                              $veiculoStr     = '';

                              if ($o->setorAtual_id) {
                                  $setor = $this->db->get_where('setor' , array('idSetor' => $o->setorAtual_id ))->row();
                                  if (count($setor) > 0) {
                                      $setor_nome = $setor->nome;
                                  }
                              }

                              if ($veiculo_id) {
                                  $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                                  if (count($veiculo) > 0) {
                                      $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                                  }
                              }

                              $dataFinal = '';

                              if ($o->dataFinal != '0000-00-00') {
                                  $dataFinal = date('d/m/Y',strtotime($o->dataFinal));
                              }
                              echo '<tr>';
                              echo '<td style="text-align: center;width:5%;">'.$o->idOs.'</td>';
                              echo '<td style="text-align: center">'.date('d/m/Y',strtotime($o->dataInicial)).'</td>';
                              echo '<td style="text-align: center">'.$dataFinal.'</td>';
                              echo '<td style="text-align: center">'.$o->garantia.'</td>';
                              echo '<td style="text-align: center">'.$o->status.'</td>';
                              echo '<td style="text-align: center">Veículo <b>'.$veiculoStr.'</b><br/>encontra-se na <b>'.$setor_nome . '</b></td>';
                              echo '<td style="text-align: center"> <a href="'.base_url().'index.php/conecte/visualizarOs/'.$o->idOs.'" class="btn"> <i class="icon-eye-open" ></i> </a></td>';
                              echo '</tr>';
                          }
                      }
                      else{
                          echo '<tr><td colspan="3">Nenhum ordem de serviço encontrada.</td></tr>';
                      }    

                      ?>
                  </tbody>
              </table>
          </div>
      </div>

      <div class="widget-box">
          <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Últimas Compras</h5></div>
          <div class="widget-content">
              <table class="table table-responsive">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Data de Venda</th>
                          <th>Responsável</th>
                          <th>Faturado</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php 
                      if($compras != null){
                          foreach ($compras as $p) {
                              if($p->faturado == 1){$faturado = 'Sim';} else{$faturado = 'Não';}
                              echo '<tr>';
                              echo '<td>'.$p->idVendas.'</td>';
                              echo '<td>'.date('d/m/Y',strtotime($p->dataVenda)).'</td>';
                              echo '<td>'.$p->nome.'</td>';
                              echo '<td>'.$faturado.'</td>';
                              echo '<td> <a href="'.base_url().'index.php/conecte/visualizarCompra/'.$p->idVendas.'" class="btn"> <i class="icon-eye-open" ></i> </a></td>';
                              echo '</tr>';
                          }
                      }
                      else{
                          echo '<tr><td colspan="5">Nenhum venda encontrada.</td></tr>';
                      }    

                      ?>
                  </tbody>
              </table>
          </div>
      </div>
    
  </div>
