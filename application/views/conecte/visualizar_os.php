<?php $totalServico = 0;
$totalProdutos = 0; ?>
<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Ordem de Serviço</h5>
                <div class="buttons">

                    <a id="imprimir" title="Imprimir" class="btn btn-mini btn-inverse" href=""><i
                                class="icon-print icon-white"></i> Imprimir</a>
                </div>
            </div>
            <div class="widget-content" id="printOs">
                <div class="invoice-content">
                    <div class="invoice-head" style="margin-bottom: 0">

                        <table class="table">
                            <tbody>
                            <?php if ($emitente == null) { ?>

                                <tr>
                                    <td colspan="3" class="alert">Os dados do emitente não foram configurados.</td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td style="width: 25%"><img src=" <?php echo $emitente->url_logo; ?> "></td>
                                    <td><span style="font-size: 20px; "> <?php echo $emitente->nome; ?></span> </br>
                                        <span><?php echo $emitente->cnpj; ?> </br> <?php echo $emitente->rua . ', nº:' . $emitente->numero . ', ' . $emitente->bairro . ' - ' . $emitente->cidade . ' - ' . $emitente->uf; ?> </span> </br>
                                        <span> E-mail: <?php echo $emitente->email . ' - Fone: ' . $emitente->telefone; ?></span>
                                    </td>
                                    <td style="width: 18%; text-align: center">#Ordem de Serviço:
                                        <span><?php echo $result->idOs ?></span></br> </br>
                                        <span>Emissão: <?php echo date('d/m/Y') ?></span></td>
                                </tr>

                            <?php } ?>
                            </tbody>
                        </table>


                        <table class="table">
                            <tbody>
                            <tr>
                                <td style="width: 50%; padding-left: 0">
                                    <ul>
                                        <li>
                                                <span><h5>Cliente</h5>
                                                <span><?php echo $result->nomeCliente ?></span><br/>
                                                <span><?php echo $result->rua ?>, <?php echo $result->numero ?>
                                                    , <?php echo $result->bairro ?></span><br/>
                                                <span><?php echo $result->cidade ?>
                                                    - <?php echo $result->estado ?></span>
                                        </li>
                                    </ul>
                                </td>
                                <td style="width: 50%; padding-left: 0">
                                    <ul>
                                        <li>
                                            <span><h5>Responsável</h5></span>
                                            <span><?php echo $result->nome ?></span> <br/>
                                            <span>Telefone: <?php echo $result->telefone ?></span><br/>
                                            <span>Email: <?php echo $result->email ?></span>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                    <div style="margin-top: 0; padding-top: 0">

                        <?php if ($result->descricaoProduto != null) { ?>
                            <hr style="margin-top: 0">
                            <h5>Descrição</h5>
                            <p>
                                <?php echo $result->descricaoProduto ?>

                            </p>
                        <?php } ?>

                        <?php if ($result->defeito != null) { ?>
                            <hr style="margin-top: 0">
                            <h5>Defeito</h5>
                            <p>
                                <?php echo $result->defeito ?>
                            </p>
                        <?php } ?>
                        <?php if ($result->laudoTecnico != null) { ?>
                            <hr style="margin-top: 0">
                            <h5>Laudo Técnico</h5>
                            <p>
                                <?php echo $result->laudoTecnico ?>
                            </p>
                        <?php } ?>
                        <?php if ($result->observacoes != null) { ?>
                            <hr style="margin-top: 0">
                            <h5>Observações</h5>
                            <p>
                                <?php echo $result->observacoes ?>
                            </p>
                        <?php } ?>

                        <?php if ($produtos != null) { ?>
                            <br/>
                            <table class="table table-bordered" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th>Acompanhamento<br/>Clique sobre a foto para ter mais informações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                    $imagens = $this->os_model->getAnexosAvulsos($id_os);

                                    echo '<tr>';

                                    echo '<td style="text-align: center;">';
                                    foreach ($imagens as $a) {

                                        if ($a->thumb == null) {
                                            $thumb = base_url() . 'assets/img/icon-file.png';
                                            $link = base_url() . 'assets/img/icon-file.png';
                                        } else {
                                            $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                            $link = $a->url . $a->anexo;
                                        }

                                        echo ' <a href="#modal-anexo" idprodutoservico="'.$produto->idProdutos_os.'" tipo="Produto" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a> ';
                                    }

                                    echo '</td>';
                                    echo '</tr>';
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>

                        <?php if ($produtos != null) { ?>
                            <br/>
                            <table class="table table-bordered" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th>Produto</th>
                                    <th>Acompanhamento por setor<br/>Clique sobre a foto para ter mais informações</th>
                                    <th>Quantidade</th>
                                    <th>Sub-total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($produtos as $p) {

                                    $imagens = $this->os_model->getAnexosByProdutos($id_os, $p->produtos_id);

                                    $totalProdutos = $totalProdutos + $p->subTotal;

                                    echo '<tr>';
                                    echo '<td>' . $p->descricao . '</td>';

                                    echo '<td style="text-align: center;">';
                                    foreach ($imagens as $a) {

                                        if ($a->thumb == null) {
                                            $thumb = base_url() . 'assets/img/icon-file.png';
                                            $link = base_url() . 'assets/img/icon-file.png';
                                        } else {
                                            $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                            $link = $a->url . $a->anexo;
                                        }

                                        echo ' <a href="#modal-anexo" idprodutoservico="'.$produto->idProdutos_os.'" tipo="Produto" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a> ';
                                    }

                                    echo '</td>';

                                    echo '<td style="width: 5%;text-align: center;">' . $p->quantidade . '</td>';
                                    echo '<td style="width: 10%;">R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                    echo '</tr>';
                                } ?>

                                <tr>
                                    <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ <?php echo number_format($totalProdutos, 2, ',', '.'); ?></strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        <?php } ?>

                        <?php if ($servicos != null) { ?>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Serviço</th>
                                    <th>Acompanhamento por setor<br/>Clique sobre a foto para ter mais informações</th>
                                    <th>Sub-total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                setlocale(LC_MONETARY, 'en_US');
                                foreach ($servicos as $s) {

                                    $imagens = $this->os_model->getAnexosByServico($id_os,$s->servicos_id);

                                    $preco = $s->preco;
                                    $totalServico = $totalServico + $preco;
                                    echo '<tr>';
                                    echo '<td>' . $s->nome . '</td>';

                                    echo '<td style="text-align: center;">';
                                    foreach ($imagens as $a) {

                                        if ($a->thumb == null) {
                                            $thumb = base_url() . 'assets/img/icon-file.png';
                                            $link = base_url() . 'assets/img/icon-file.png';
                                        } else {
                                            $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                            $link = $a->url . $a->anexo;
                                        }

                                        echo ' <a href="#modal-anexo" idprodutoservico="'.$produto->idProdutos_os.'" tipo="Produto" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a> ';
                                    }

                                    echo '</td>';

                                    echo '<td>R$ ' . number_format($s->preco, 2, ',', '.') . '</td>';
                                    echo '</tr>';
                                } ?>

                                <tr>
                                    <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                    <td><strong>R$ <?php echo number_format($totalServico, 2, ',', '.'); ?></strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        <?php } ?>
                        <hr/>

                        <h4 style="text-align: right">Valor Total:
                            R$ <?php echo number_format($totalProdutos + $totalServico, 2, ',', '.'); ?></h4>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo" style="text-align: center">
            <div class='progress progress-info progress-striped active'>
                <div class='bar' style='width: 100%'></div>
            </div>
        </div>


        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">LINK</label>
            <input type="text" id="link-visualizar" readonly name="link-visualizar"
                   class="span12"/>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="localfoto-visualizar">Local da Foto</label>
            <input type="text"  class="span12" id="localfoto-visualizar" readonly name="localfoto-visualizar">
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">Observação</label>
            <textarea class="span12" rows="5" readonly cols="5" id="observacaoimg-visualizar" name="observacaoimg-visualizar"></textarea>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="data-visualizar">Data</label>
            <input type="date" id="data-visualizar" readonly name="data-visualizar"
                   class="span4"/>

            <label for="hora-visualizar">Hora</label>
            <input type="time"  id="hora-visualizar"  readonly name="hora-visualizar"
                   class="span4"/>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
        <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
        <a href="" target="_blank" class="btn btn-invers" id="abrir-imagem">Abrir</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {


        $("#imprimir").click(function () {

            //pega o Html da DIV
            var divElements = document.getElementById('printOs').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;
        })



        $(document).on('click', '.anexo', function (event) {
            event.preventDefault();

            var link = $(this).attr('link');
            var id = $(this).attr('imagem');
            var url = '<?php echo base_url(); ?>os/excluirAnexo/';
            var observacaoimg = $(this).attr('observacaoimg');
            var data = $(this).attr('data');
            var hora = $(this).attr('hora');
            var localfoto = $(this).attr('localfoto');

            $("#div-visualizar-anexo").html('<img src="' + link + '" alt="">');
            $("#excluir-anexo").attr('link', url + id);

            $('#link-visualizar').val(link);
            $('#observacaoimg-visualizar').val(observacaoimg);
            $('#data-visualizar').val(data);
            $('#hora-visualizar').val(hora);
            $('#localfoto-visualizar').val(localfoto);

            $('#abrir-imagem').attr('href', link);
            $("#download").attr('href', "<?php echo base_url(); ?>index.php/os/downloadanexo/" + id);

        });

    });
</script>