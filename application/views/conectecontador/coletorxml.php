

<!--[if lt IE 9]>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/dist/excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabela_responsiva.css"/>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.donutRenderer.min.js"></script>

<a href="#" id="baixarxml" class="btn btn-info">
    <i class="icon-user-md"></i> Coletar Emissão XML
</a>

<a href="#" id="manifestarxml" class="btn btn-warning">
    <i class="icon-info-sign"></i> Manifestar Recebimento XML
</a>

<a href="#" id="donwloadxml" class="btn btn-success">
    <i class="icon-download"></i> Baixar XML p/ Servidor
</a>

<a href="#" id="cadastrarXML" class="btn btn-info" style="display: none;">
    <i class="icon-upload"></i> Cadastrar XML
</a>

<a href="#" id="imprimir" class="btn btn-warning">
    <i class="icon-print"></i> Imprimir
</a>

<a href="#" id="baixar" class="btn btn-info">
    <i class="icon-download"></i> Baixar .zip
</a>
<br/> <br/>

<form action="<?php echo base_url() ?>index.php/conectecontador/nfentradas" method="get">
    <div class="span12 well">

        <div class="span3">
            <select id="mes" name="mes">
                <option value="01" <?php if($mes == '01') echo 'selected="selected"';?>>Janeiro <?php echo date('Y');?></option>
                <option value="02" <?php if($mes == '02') echo 'selected="selected"';?>>Fevereiro <?php echo date('Y');?></option>
                <option value="03" <?php if($mes == '03') echo 'selected="selected"';?>>Março <?php echo date('Y');?></option>
                <option value="04" <?php if($mes == '04') echo 'selected="selected"';?>>Abril <?php echo date('Y');?></option>
                <option value="05" <?php if($mes == '05') echo 'selected="selected"';?>>Maio <?php echo date('Y');?></option>
                <option value="06" <?php if($mes == '06') echo 'selected="selected"';?>>Junho <?php echo date('Y');?></option>
                <option value="07" <?php if($mes == '07') echo 'selected="selected"';?>>Julho <?php echo date('Y');?></option>
                <option value="08" <?php if($mes == '08') echo 'selected="selected"';?>>Agosto <?php echo date('Y');?></option>
                <option value="09" <?php if($mes == '09') echo 'selected="selected"';?>>Setembro <?php echo date('Y');?></option>
                <option value="10" <?php if($mes == '10') echo 'selected="selected"';?>>Outubro <?php echo date('Y');?></option>
                <option value="11" <?php if($mes == '11') echo 'selected="selected"';?>>Novembro <?php echo date('Y');?></option>
                <option value="12" <?php if($mes == '12') echo 'selected="selected"';?>>Dezembro <?php echo date('Y');?></option>
            </select>
        </div>
        <div class="span1">
            <button class="span12 btn"  id="buscar"> <i class="icon-search"></i></button>
        </div>

    </div>
</form>

<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Notas coletadas</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive" id="tbManisfesta">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhum Nota Coletada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Notas coletadas</h5>
        </div>
        <div class="widget-content nopadding">
            <table class='table table-bordered' id="tbManisfesta">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th><input name="" type="checkbox" value="0" onclick="marcarTodas();" id="marcarTodos" /></th>
                    <th>Nº</th>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Chave</th>
                    <th style="text-align: center;">Manifestada</th>
                    <th style="text-align: center;">Donwload</th>
                    <th style="text-align: center;">Baixar</th>
                    <th style="text-align: right;">Valor</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $totalNFManifestada = 0;
                $totalNFNaoManifestada = 0;
                foreach ($results as $r) {
                    $chave = $r->chave;
                    $numero = substr($chave, 28, 6);

                    if($r->manifestada == 'S') $totalNFManifestada = $totalNFManifestada + $r->valor;
                    else $totalNFNaoManifestada = $totalNFNaoManifestada + $r->valor;

                    echo '<tr>';
                    echo '    <td><input class="elementSelected" type="checkbox" importada="'.$r->importada.'" donwload="'.$r->donwload.'"  value="'.$r->manifestaid.'"></td>';
                    echo '    <td>' . $numero . '</td>';
                    echo '    <td>' . $r->data . '</td>';
                    echo '    <td>' . $r->xNome . '</td>';
                    echo '    <td>' . $r->chave . '</td>';
                    echo '    <td style="text-align: center;">' . $r->manifestada . '</td>';
                    echo '    <td style="text-align: center;">' . $r->donwload . '</td>';
                    echo '<td style="text-align: center;">';

                    if($r->donwload == 'S'){
                        $arquivo = base_url() . "/emissores/v4/nfephp-master/geracao/xml_entrada_download.php?chave=". $r->chave.""; // Ambiente Windows
                        echo '<a href="' . $arquivo . '" target="_blank">XML</a>';
                    }
                    echo '</td>';
                    echo '<td style="text-align: right;">R$ ' . number_format(   $r->valor , 2, ',', ' ') . '</td>';
                    echo '</tr>';
                } ?>
                <tr>
                    <td colspan="8" style="text-align: right;">Número NF-e</td>
                    <td colspan="1" style="text-align: right;"><?php echo count($results);?></td>
                </tr>
                <tr>
                    <td colspan="8" style="text-align: right;">Total de NF-e não manifestada</td>
                    <td colspan="1" style="text-align: right;">R$ <?php echo number_format(   $totalNFNaoManifestada , 2, ',', ' ');?></td>
                </tr>
                <tr>
                    <td colspan="8" style="text-align: right;">Total de NF-e manifestada</td>
                    <td colspan="1" style="text-align: right;">R$ <?php echo number_format(   $totalNFManifestada , 2, ',', ' ');?></td>
                </tr>
                <tr>
                    <td colspan="8" style="text-align: right;">Total NF-e</td>
                    <td colspan="1" style="text-align: right;">R$ <?php echo number_format(   $totalNFManifestada - $totalNFNaoManifestada , 2, ',', ' ');?></td>
                </tr>

                <tr>
                    <td colspan="9" style="text-align: center;">
                        <h5>Estatísticas de NF-e Manifestada (Entrada)</h5>
                        <div id="chart-nfeentradamanifestada" style=""></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="9" style="text-align: center;">
                        <h5>Estatísticas de NF-e Não Manifestada (Entrada)</h5>
                        <div id="chart-nfeentradanaomanifestada" style=""></div>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>

<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">

    var TENTATIVAS          = 5;
    var contadorBaixarXML   = 0;
    var contadorManifesta   = 0;
    var contadorDonwloadXML = 0;

    $(document).ready(function () {


        <?php if ($manifestadaAll != null) { ?>
            var data = [
                <?php foreach ($manifestadaAll as $nfe) {

                $dhEmi = $nfe->data;
                $mesG =  explode('/', $dhEmi);


                $mesG = getMes($mesG[1]);

                echo "['" .$mesG.' - R$ '.number_format($nfe->valor, 2, ',', ' '). "', " . $nfe->valor . "],";
            } ?>

            ];
            var plot1 = jQuery.jqplot('chart-nfeentradamanifestada', [data],
                {
                    seriesDefaults: {
                        // Make this a pie chart.
                        renderer: jQuery.jqplot.PieRenderer,
                        rendererOptions: {
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            showDataLabels: true
                        }
                    },
                    legend: {show: true, location: 'e'}
                }
            );
        <?php } ?>

        <?php if ($naoManifestadaAll != null) { ?>
            var data = [
                <?php foreach ($naoManifestadaAll as $nfe) {

                $dhEmi = $nfe->data;
                $mesG =  explode('/', $dhEmi);


                $mesG = getMes($mesG[1]);

                echo "['" .$mesG.' - R$ '.number_format($nfe->valor, 2, ',', ' '). "', " . $nfe->valor . "],";
            } ?>

            ];
            var plot1 = jQuery.jqplot('chart-nfeentradanaomanifestada', [data],
                {
                    seriesDefaults: {
                        // Make this a pie chart.
                        renderer: jQuery.jqplot.PieRenderer,
                        rendererOptions: {
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            showDataLabels: true
                        }
                    },
                    legend: {show: true, location: 'e'}
                }
            );
        <?php } ?>

        $('#imprimir').click(function (event) {
            var elemento = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfeManifesta.php?arquivo='+$(elemento).get(0)+ '', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#baixar').click(function (event) {

            var mes = $('#mes').val();
            debugger;
            mes = mes + "<?php echo date('Y');?>";

            window.open('<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/xml_entrada_downloadAll.php?periodo='+mes+ '', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');

        });

        $('#baixarxml').click(function () {
            verificarNFFornecedor();
        });

        $('#manifestarxml').click(function () {

            var elemento  = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                for(var i=0;i<elemento.length;i++) {
                    manifestar(elemento[i]);
                }
            }
        });

        $('#donwloadxml').click(function () {
            var elemento  = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                for(var i=0;i<elemento.length;i++) {
                    donwloadXML(elemento[i]);
                }
            }
        });

        $('#cadastrarXML').click(function (event) {
            var elemento  = new Array();
            var importadas = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                importadas.push($(this).attr('importada'));
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                cadatrarXMLs(elemento, importadas);
            }
        });
    });

    function cadatrarXMLs(elemento, importada) {
        for(var i=0;i<elemento.length;i++) {
            if (importada[i] === 'N') {
                cadastrarXML(elemento[i]);
            }
        }
    }

    function cadastrarXML(idmanifesto,) {

        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde,  Realizando o Cadastro do XML....</h3>'});

        var importarNotasSaida = 0;

        if ($("#importarNotasSaida").is(":checked")) importarNotasSaida = 1;
        else importarNotasSaida = 0;

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/ImpXMLFornecedor.php?manifestaid="+idmanifesto+'&importarNotasSaida='+importarNotasSaida,
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {

                    $("#tbManisfesta").load("<?php echo current_url();?> #tbManisfesta");

                    $('#dSuccess-msg').html(data.msg);
                    $('.success-msg').show();

                    $('#dError-msg').html('');
                    $('.error-msg').hide();

                } else {
                    $('#dError-msg').html(data.msg);
                    $('.error-msg').show();

                    $('#dSuccess-msg').html('');
                    $('.success-msg').hide();
                }
            }
        });
    }

    function verificarNFFornecedor() {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde, Buscando XMLs emitidos pelo Fornecedor. Esse processo pode demorar alguns minutos...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/dfe.php",
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {

                    $("#tbManisfesta").load("<?php echo current_url();?> #tbManisfesta");

                    $('#dSuccess-msg').html(data.msg);
                    $('.success-msg').show();

                    $('#dError-msg').html('');
                    $('.error-msg').hide();

                } else {

                    if (contadorBaixarXML >= TENTATIVAS) {
                        $('#dError-msg').html(data.msg);
                        $('.error-msg').show();

                        $('#dSuccess-msg').html('');
                        $('.success-msg').hide();
                    } else {
                        verificarNFFornecedor();
                        contadorBaixarXML = contadorBaixarXML + 1;
                    }
                }
            }
        });
    }

    function manifestar(idManifesto) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde, Manifestando o recebimentos das notas. Esse processo pode demorar alguns minutos...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/manifesta.php?idManifesto="+idManifesto,
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {

                    $("#tbManisfesta").load("<?php echo current_url();?> #tbManisfesta");

                    $('#dSuccess-msg').html(data.msg);
                    $('.success-msg').show();

                    $('#dError-msg').html('');
                    $('.error-msg').hide();

                } else {

                    if (contadorManifesta >= TENTATIVAS) {
                        $('#dError-msg').html(data.msg);
                        $('.error-msg').show();
                    } else {
                        manifestar(idManifesto);
                        contadorManifesta = contadorManifesta + 1;
                    }
                }
            }
        });
    }

    function donwloadXML(idManifesto) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" />  Aguarde, Realizando o Donwload do XML para o servidor. Esse processo pode demorar alguns minutos...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/donwload.php?idManifesto="+idManifesto,
            dataType: "json",
            success: function (data)
            {
                $.unblockUI();
                if (data.status === 'ok') {

                    $("#tbManisfesta").load("<?php echo current_url();?> #tbManisfesta");

                    $('#dSuccess-msg').html(data.msg);
                    $('.success-msg').show();

                    $('#dError-msg').html('');
                    $('.error-msg').hide();

                } else {

                    if (contadorDonwloadXML >= TENTATIVAS) {
                        $('#dError-msg').html(data.msg);
                        $('.error-msg').show();
                    } else {
                        donwloadXML(idManifesto);
                        contadorDonwloadXML = contadorDonwloadXML + 1;
                    }
                }
            }
        });
    }

    function marcarTodas() {
        if ($('#marcarTodos').prop("checked")){
            $('.elementSelected').each(
                function(){
                    $(this).attr("checked", true);
                    $(this).prop("checked", true);
                }
            );
        }else{
            $('.elementSelected').each(
                function(){
                    $(this).attr("checked", false);
                    $(this).prop("checked", false);
                }
            );
        }
    }
</script>


<?php

function getMes($mes) {
    if ($mes == '01') {
        return 'Janeiro';
    } else if ($mes == '02'){
        return 'Fevereiro';
    } else if ($mes == '03'){
        return 'Março';
    } else if ($mes == '04'){
        return 'Abril';
    } else if ($mes == '05'){
        return 'Maio';
    } else if ($mes == '06'){
        return 'Junho';
    } else if ($mes == '07'){
        return 'Julho';
    } else if ($mes == '08'){
        return 'Agosto';
    } else if ($mes == '09'){
        return 'Setembro';
    } else if ($mes == '10'){
        return 'Outubro';
    } else if ($mes == '11'){
        return 'Novembro';
    } else if ($mes == '12'){
        return 'Dezembro';
    }
}

?>