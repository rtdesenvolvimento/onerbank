<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Editar Inventário</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes do Inventário</a></li>
                     </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="span12" id="divEditarVenda">
                                <form action="<?php echo current_url(); ?>" method="post" id="formInventario">
                                    <?php echo form_hidden('idInventario', $result->idInventario) ?>
                                    <h3>#Inventário: <?php echo $result->idInventario ?></h3>

                                    <div class="span12 well" style="padding: 1%; margin-left: 0">

                                        <div class="span3">
                                            <label for="cliente">Origem<span class="required">*</span></label>
                                            <div class="controls">
                                                <select name="filial_id" readonly class="span12" id="filial_id" required="required">
                                                    <option value="<?php echo $origem->idFilial; ?>"><?php echo $origem->nome; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <label for="status">Status<span class="required">*</span></label>
                                            <select class="span12" name="status" disabled id="status">
                                                <option <?php if($result->status == 'Aberto'){echo 'selected';} ?> value="Aberto">Aberto</option>
                                                <option <?php if($result->status == 'Confirmado'){echo 'selected';} ?> value="Confirmado">Confirmado</option>
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <label for="tecnico">Responsável<span class="required">*</span></label>
                                            <div class="controls">
                                                <select name="usuarios_id" class="span12" id="usuarios_id" disabled required="required">
                                                    <option value="">--Selecione um cliente--</option>
                                                    <?php foreach ($usuarios as $usuario) {?>
                                                        <option <?php if ($result->usuarios_id == $usuario->idUsuarios) echo 'selected="selected"'; ?> value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <label for="status">Zerar produtos <small>fora do inventário</small><span class="required">*</span></label>
                                            <select class="span12" disabled name="zerarEstoqueProdutoForaInventario" id="zerarEstoqueProdutoForaInventario">
                                                <option <?php if($result->zerarEstoqueProdutoForaInventario == 'S'){echo 'selected';} ?> value="S">Sim</option>
                                                <option <?php if($result->zerarEstoqueProdutoForaInventario == 'N'){echo 'selected';} ?> value="N">Não</option>
                                            </select>
                                        </div>
                                        <div class="span12" style="padding: 0%; margin-left: 0">
                                            <div class="span4">
                                                <label for="dataInventario">Data</label>
                                                <input id="dataInventario" class="span12" required="required" disabled type="date" name="dataInventario"
                                                       value="<?php echo $result->dataInventario;?>"/>
                                            </div>
                                            <div class="span4">
                                                <label for="status">Liberar para contador<span class="required">*</span></label>
                                                <select class="span12" name="liberarContador"  disabled id="liberarContador">
                                                    <option <?php if($result->liberarContador == 'S'){echo 'selected';} ?> value="S">Sim</option>
                                                    <option <?php if($result->liberarContador == 'N'){echo 'selected';} ?> value="N">Não</option>
                                                </select>
                                            </div>
                                            <div class="span4">
                                                <label for="numeroLivro">Nº Livro</label>
                                                <input id="numeroLivro" class="span12" type="text" name="numeroLivro" value="<?php echo $result->numeroLivro;?>"/>
                                            </div>
                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="span8 offset2" style="text-align: center">


                                                <button class="btn btn-primary" id="btnContinuar"><i
                                                        class="icon-white icon-ok"></i> Alterar
                                                </button>

                                                <a href="<?php echo base_url() ?>index.php/conectecontador/relatorioEstoque/<?php echo $result->idInventario; ?>"
                                                   class="btn btn-warning" target="_blank"><i class="icon-print"></i> Relatório
                                                </a>

                                                <a href="<?php echo base_url() ?>index.php/conectecontador/inventarioestoque" class="btn"><i
                                                        class="icon-arrow-left"></i> Voltar</a>
                                            </div>

                                        </div>
                                    </div>

                                </form>

                                <div class="span12 well" style="padding: 1%; margin-left: 0">

                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tablItens" data-toggle="tab">Produtos</a></li>
                                        <li><a href="#tab4" data-toggle="tab">Observações do inventário aqui</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tablItens">
                                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                <form id="formProdutos"
                                                      action="<?php echo base_url(); ?>index.php/pedido/adicionarProduto"
                                                      method="post" style="display: none;">

                                                    <div class="span3">
                                                        <input type="hidden" name="idInventario" id="idInventario"
                                                               value="<?php echo $result->idInventario ?>"/>
                                                        <label for="">Produto</label>

                                                        <div class="controls">
                                                            <input type="text" name="idProduto" value="" class="form-control pos-tip ui-autocomplete-input" id="idProduto" data-placement="top" data-trigger="focus" placeholder="Scanear/Procurar Produto pelo Nome/Código" title="" autocomplete="off" data-original-title="Por favor, comece a digitar o código/nome para receber sugestões ou apenas scaneie um Cód. de Barra " tabindex="1">
                                                            <input type="hidden" name="product_id" id="product_id" value=""/>
                                                        </div>
                                                    </div>


                                                    <div class="span2">
                                                        <label for="">Ajustar custo (R$)</label>
                                                        <input type="text" placeholder="Custo" id="custo" name="custo"
                                                               class="span12"/>
                                                    </div>

                                                    <div class="span2">
                                                        <label for="">Ajustar venda (R$)</label>
                                                        <input type="text" placeholder="Venda" id="venda" name="venda"
                                                               class="span12"/>
                                                    </div>

                                                    <div class="span2">
                                                        <label for="">Tipo</label>

                                                        <div class="controls">
                                                            <select name="tipo" class="span12" id="tipo" required="required">
                                                                <option value="SUBSTITUIR">(~) SUBSTITUIR</option>
                                                                <option value="ENTRADA">(+) ENTRADA</option>
                                                                <option value="SAIDA">( - ) SAÍDA</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="span1">
                                                        <label for="">Estoque</label>
                                                        <input type="text" placeholder="Quantidade" required="required" id="quantidade"
                                                               name="quantidade" class="span12" value="1"/>
                                                    </div>

                                                    <?php if($result->status == 'Aberto'){ ?>
                                                        <div class="span2">
                                                            <label for="">&nbsp</label>
                                                            <button class="btn btn-success span12" id="btnAdicionarProduto"><i
                                                                    class="icon-white icon-plus"></i> Adicionar
                                                            </button>
                                                        </div>
                                                    <?php }?>
                                                </form>

                                                <div class="span12" id="divProdutos" style="margin-left: 0">
                                                    <table class="table table-bordered" id="tblProdutos">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: left;">Produto</th>
                                                            <th style="text-align: right;">Tipo</th>
                                                            <th style="text-align: right;">Estoque</th>
                                                            <th style="text-align: right;">Preço de venda</th>
                                                            <th style="text-align: right;">Preço de custo</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        foreach ($produtos as $p) {

                                                            echo '<tr>';
                                                            echo '<td style="text-align: left;">' . $p->descricao . '</td>';
                                                            echo '<td style="text-align: right;">' . $p->tipo . '</td>';
                                                            echo '<td style="text-align: right;">' . $p->quantidade . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->custo, 2, ',', '.') . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->venda, 2, ',', '.') . '</td>';
                                                            echo '</tr>';
                                                        } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane" id="tab4">

                                            <form action="<?php echo current_url(); ?>" method="post" id="formObservacao">
                                                <?php echo form_hidden('idInventario', $result->idInventario) ?>
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <label for="senha">Observações do inventário</label>
                                                        <textarea class="span12"  name="observacao" cols="30" rows="5"><?php echo $result->observacao; ?></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/inventario.js"></script>

<script type="text/javascript">

    var BASE_URL = '<?php echo base_url();?>';

    $(document).ready(function () {

        $(".money").maskMoney();

        $("#formInventario").validate({
            rules: {
                dataInventario: {required: true}
            },
            messages: {
                dataInventario: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formObservacao").validate({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/inventario/editarObservacao",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        alert('Alterado');
                    }
                });
                return false;
            }
        });

        $("#formProdutos").validate({
            rules: {
                quantidade: {required: true},
                idProduto: {required: true}
            },
            messages: {
                quantidade: {required: 'Insira a quantidade'},
                idProduto: {required: 'Insira o produto'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/inventario/adicionarProduto",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");

                            $("#quantidade").val('1');
                            $("#custo").val('');
                            $('#product_id').val('');

                            $("#idProduto").val('').focus();
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', 'a', function (event) {
            var idProduto   = $(this).attr('idAcao');
            var quantidade  = $(this).attr('quantAcao');
            var produto     = $(this).attr('prodAcao');

            if ((idProduto % 1) == 0) {
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/inventario/excluirProduto",
                    data: "idProduto=" + idProduto + "&quantidade=" + quantidade + "&produto=" + produto,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
    });

    function getConsultaCEP() {
        if($.trim($("#cepEntrega").val()) != ""){
            var url = 'http://api.postmon.com.br/v1/cep/'+$("#cepEntrega").val();
            $.get(url,{
                    cep:$("#cepEntrega").val()
                },
                function (data) {
                    if(data != -1){
                        $("#ruaEntrega").val( data.logradouro  );
                        $("#bairroEntrega").val( data.bairro );
                        $("#cidadeEntrega").val( data.cidade );
                        $("#estadoEntrega").val( data.estado );
                    }
                });
        }
    }


</script>

