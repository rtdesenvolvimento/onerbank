<head>
    <title>Relatório de PGDAS</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body style="background-color: transparent">



<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">MAPA RESUMO DE VENDAS PARA SIMPLES NACIONAL (PGDAS)</h4>
                </div>
                <div class="widget-content nopadding">

                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #eee;">
                            <th style="font-size: 1.2em;" colspan="9"></th>
                            <th style="font-size: 1.2em;text-align: center;" colspan="3">ICMS</th>
                        </tr>
                        </thead>
                        <thead>
                        <tr style="background: #eee;">
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Data</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Documento</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Inicio</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Final</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Vendas</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Canc.</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Acre.</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Desc.</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Ven. Líquidas</th>
                            <th style="font-size: 1.2em;text-align: right;">Tributadas</th>
                            <th style="font-size: 1.2em;text-align: right;">Substituídas</th>
                            <th style="font-size: 1.2em;text-align: right;">Isentas</th>
                        </tr>
                        </thead>

                        <!-- NF-e !-->
                        <tbody>
                        <?php

                        $totalVendasNFE = 0;
                        $totalCanceladasNFE = 0;
                        $totalDescontoNFE = 0;
                        $totalAcrescimoNFE = 0;
                        $totalVendasLiquidasNFE = 0;
                        $totalTributadasNFE = 0;
                        $totalSubstituidasNFE = 0;
                        $totalIsentasNFE = 0;

                        foreach ($nfe as $p) {

                            $vlNotasCanceladas = 0;

                            //$cancelamento = $this->Relatorios_model->pgdasNFeCanceladas($p->dhEmi);
                            //if (count($cancelamento) > 0) $vlNotasCanceladas = $cancelamento->valortotalnf;

                            $acrescimo =  0;
                            $desconto = $p->vDescnf;

                            $vendasBruta = $p->valortotalprod - $acrescimo + $desconto;
                            $vendasLiquidas = $vendasBruta + $acrescimo - $desconto - $vlNotasCanceladas;
                            $tributadas = $p->vICMSnf;
                            $substituidas = $p->vICMSSTnf;
                            $isentadas = $vendasLiquidas - $tributadas - $substituidas;

                            $totalVendasNFE += $vendasBruta;
                            $totalCanceladasNFE += $cancelamento;
                            $totalDescontoNFE += $desconto;
                            $totalAcrescimoNFE += $acrescimo;
                            $totalVendasLiquidasNFE += $vendasLiquidas;
                            $totalTributadasNFE += $tributadas;
                            $totalSubstituidasNFE += $substituidas;
                            $totalIsentasNFE += $isentadas;

                            echo '<tr>';
                            echo '<td style="text-align: center;">' . date(('d/m/Y'), strtotime($p->dhEmi)) . '</td>';
                            echo '<td style="text-align: center;">' . $p->documento . '</td>';
                            echo '<td style="text-align: center;">' . str_pad($p->inicio, 8, '0', STR_PAD_LEFT)  . '</td>';
                            echo '<td style="text-align: center;">' . str_pad($p->final, 8, '0', STR_PAD_LEFT)  . '</td>';
                            echo '<td style="text-align: right;">R$' . number_format($vendasBruta, 2, ',', '.') . '</td>';
                            echo '<td style="text-align: right;">R$'. number_format($vlNotasCanceladas, 2, ',', '.') .'</td>';
                            echo '<td style="text-align: right;">R$' . number_format($acrescimo, 2, ',', '.') . '</td>';
                            echo '<td style="text-align: right;">R$' . number_format($desconto, 2, ',', '.') . '</td>';
                            echo '<td style="text-align: right;">R$' . number_format( $vendasLiquidas, 2, ',', '.') . '</td>';?>
                            <td style="text-align: right;">R$<?php echo number_format($tributadas, 2, ',', '.')  ;?></td>
                            <td style="text-align: right;">R$<?php echo number_format($substituidas, 2, ',', '.')   ;?></td>
                            <td style="text-align: right;">R$<?php echo number_format( $isentadas , 2, ',', '.')  ?></td>
                            <?php echo '</tr>';
                        }
                        ?>

                        <!-- NFC-e !-->
                        <?php

                        $totalVendasNFCE = 0;
                        $totalCanceladasNFCE = 0;
                        $totalDescontoNFCE = 0;
                        $totalAcrescimoNFCE = 0;
                        $totalVendasLiquidasNFCE = 0;
                        $totalTributadasNFCE = 0;
                        $totalSubstituidasNFCE = 0;
                        $totalIsentasNFCE = 0;

                        foreach ($nfce as $p) {

                            $vlNotasCanceladas = 0;

                            $cancelamento = $this->Relatorios_model->pgdasNFCeCanceladas($p->dhEmi);

                            if (count($cancelamento) > 0) $vlNotasCanceladas = $cancelamento->valor;

                            $acrescimo = $p->vOutronf;
                            $desconto = $p->vDescnf;

                            $vendasBruta = $p->vNF - $acrescimo + $desconto;
                            $vendasLiquidas = $vendasBruta + $acrescimo - $desconto - $vlNotasCanceladas;
                            $tributadas = $p->vICMSnf;
                            $substituidas = $p->vBCSTnf;
                            $isentadas = $vendasLiquidas - $tributadas - $substituidas;

                            $totalVendasNFCE += $vendasBruta;
                            $totalCanceladasNFCE += $vlNotasCanceladas;
                            $totalVendasLiquidasNFCE += $vendasLiquidas;
                            $totalDescontoNFCE += $desconto;
                            $totalAcrescimoNFCE += $acrescimo;
                            $totalTributadasNFCE += $tributadas;
                            $totalSubstituidasNFCE += $substituidas;
                            $totalIsentasNFCE += $isentadas;

                            echo '<tr>';
                            echo '<td style="text-align: center;">' . date(('d/m/Y'), strtotime($p->dhEmi)) . '</td>';
                            echo '<td style="text-align: center;">' . $p->documento . '</td>';
                            echo '<td style="text-align: center;">' . str_pad($p->inicio, 8, '0', STR_PAD_LEFT)  . '</td>';
                            echo '<td style="text-align: center;">' . str_pad($p->final, 8, '0', STR_PAD_LEFT)  . '</td>';
                            echo '<td style="text-align: right;">R$' . number_format($vendasBruta, 2, ',', '.') . '</td>';
                            echo '<td style="text-align: right;">R$'. number_format($vlNotasCanceladas, 2, ',', '.') .'</td>';
                            echo '<td style="text-align: right;">R$' . number_format($acrescimo, 2, ',', '.') . '</td>';
                            echo '<td style="text-align: right;">R$' . number_format($desconto, 2, ',', '.') . '</td>';
                            echo '<td style="text-align: right;">R$' . number_format( $vendasLiquidas, 2, ',', '.') . '</td>';?>
                            <td style="text-align: right;">R$<?php echo number_format($tributadas, 2, ',', '.')  ;?></td>
                            <td style="text-align: right;">R$<?php echo number_format($substituidas, 2, ',', '.')   ;?></td>
                            <td style="text-align: right;">R$<?php echo number_format($isentadas, 2, ',', '.')  ?></td>
                            <?php echo '</tr>';
                        }
                        ?>

                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;"><?php echo number_format( $totalVendasNFE + $totalVendasNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalCanceladasNFE + $totalCanceladasNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalAcrescimoNFE + $totalAcrescimoNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalDescontoNFE + $totalDescontoNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalVendasLiquidasNFE + $totalVendasLiquidasNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalTributadasNFE + $totalTributadasNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalSubstituidasNFE + $totalSubstituidasNFCE, 2, ',', '.');?></td>
                            <td style="text-align: right;"><?php echo number_format( $totalIsentasNFE + $totalIsentasNFCE, 2, ',', '.');?></td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
            <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>
        </div>

    </div>
</div>
<!-- Arquivos js-->

<script src="<?php echo base_url();?>js/excanvas.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
<script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>js/sosmc.js"></script>
<script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>

