<?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
    <a href="<?php echo base_url();?>index.php/inventario/adicionar" class="btn btn-success">
        <i class="icon-plus icon-white"></i> FAZER INVENTÁRIO</a>
<?php } ?>

<?php

if(!$results){?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Inventário</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Data</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="6">Nenhum Inventário cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else{?>

    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Pedidos</h5>
        </div>

        <div class="widget-content nopadding">

            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="text-align: left;">Cód</th>
                    <th style="text-align: left;">Origem</th>
                    <th style="text-align: center;">Data</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">Nº Livro</th>
                    <th style="text-align: center;">Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    $dataInventario = date(('d/m/Y'),strtotime($r->dataInventario));

                    echo '<tr>';
                    echo '<td style="text-align: left;">'.$r->idInventario.'</td>';
                    echo '<td style="text-align: left;">'.$r->origem.'</td>';
                    echo '<td style="text-align: center;">'.$dataInventario.'</td>';
                    echo '<td style="text-align: center;">'.$r->status.'</td>';
                    echo '<td style="text-align: center;">'.$r->numeroLivro.'</td>';
                    echo '<td style="text-align: center;">';
                    echo '<a style="margin-right: 1%" href="'.base_url().'index.php/conectecontador/relatorioEstoque/'.$r->idInventario.'" class="btn btn-warning tip-top"><i class="icon-print icon-white"></i></a>';
                    echo '<a style="margin-right: 1%" href="'.base_url().'index.php/conectecontador/editarInventarioEstoque/'.$r->idInventario.'" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '</td>';
                    echo '</tr>';
                }?>
                <tr>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php echo $this->pagination->create_links();}?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/vendas/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Venda</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idVenda" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir este pedido de compra?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', 'a', function(event) {
            var venda = $(this).attr('venda');
            $('#idVenda').val(venda);
        });
    });

</script>