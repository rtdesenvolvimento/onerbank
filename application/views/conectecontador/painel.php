
<!--[if lt IE 9]>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/dist/excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabela_responsiva.css"/>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.donutRenderer.min.js"></script>

<style>
    .donwloadA {
        font-size: 30px;
    }
</style>
<div class="span12" style="margin-left: 0">
    <h4>Dowload dos XMLS das NF-e e NFC-e</h4>
    <table class='table table-bordered' id='tabela-mensalidades'>
        <thead>
        <tr>
            Mes:<br/>
            <select id="mes" name="mes" onchange="abrirConsulta();">
                <option value="01" <?php if($mes == '01') echo 'selected="selected"';?>>Janeiro</option>
                <option value="02" <?php if($mes == '02') echo 'selected="selected"';?>>Fevereiro</option>
                <option value="03" <?php if($mes == '03') echo 'selected="selected"';?>>Março</option>
                <option value="04" <?php if($mes == '04') echo 'selected="selected"';?>>Abril</option>
                <option value="05" <?php if($mes == '05') echo 'selected="selected"';?>>Maio</option>
                <option value="06" <?php if($mes == '06') echo 'selected="selected"';?>>Junho</option>
                <option value="07" <?php if($mes == '07') echo 'selected="selected"';?>>Julho</option>
                <option value="08" <?php if($mes == '08') echo 'selected="selected"';?>>Agosto</option>
                <option value="09" <?php if($mes == '09') echo 'selected="selected"';?>>Setembro</option>
                <option value="10" <?php if($mes == '10') echo 'selected="selected"';?>>Outubro</option>
                <option value="11" <?php if($mes == '11') echo 'selected="selected"';?>>Novembro</option>
                <option value="12" <?php if($mes == '12') echo 'selected="selected"';?>>Dezembro</option>
            </select>
        </tr>

        <?php
        $anoAtual = date('Y');
        ?>

        <tr style='backgroud-color: #2D335B'>
            <th style="text-align: center;width: 10%;"></th>
            <th style="text-align: center;width: 90%;"><?php echo $strMes;?>/<?php echo $anoAtual;?></th>
        </tr>
        </thead>
        <tbody>

        <!--NFC-E-->
        <tr>
            <td style="text-align: center;">NFC-e</td>
            <td>
                <table class='table table-bordered' id='tabela-mensalidades'>
                    <?php if ( ($nfcePendentes->qtd + $nfcePendentesNull->qtd) > 0) {?>
                        <tr><td style="background: #FFEB3B;">
                                Notas Pendentes:<br/>
                                <small>*Notas pedentes poderão haver divergencias, instrua o cliente a verificar as pedências na emissão.</small>
                            </td><td style="text-align: center;background: #FFEB3B;"><?php echo $nfcePendentes->qtd + $nfcePendentesNull->qtd;?></td><td style="text-align: right;background: #FFEB3B;">R$ <?php if ($nfcePendentes->totalvNF>0 || $nfcePendentesNull >0)  echo number_format($nfcePendentes->totalvNF + $nfcePendentesNull->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <?php } else { ?>
                        <tr><td>Notas Pendentes:</td><td style="text-align: center;">0</td><td style="text-align: right;">R$ 0,00</td></tr>
                    <?php } ?>
                    <tr><td>Notas Autorizadas:</td><td style="text-align: center;"><?php echo $nfceAutorizadas->qtd;?></td><td style="text-align: right;">R$ <?php if ($nfceAutorizadas->totalvNF>0) echo  number_format($nfceAutorizadas->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <tr><td>Notas Canceladas:</td><td style="text-align: center;"><?php echo $nfceCanceladas->qtd;?></td><td style="text-align: right;">R$ <?php if ($nfceCanceladas->totalvNF>0) echo number_format( $nfceCanceladas->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <tr><td>Notas Inutilizadas:</td><td style="text-align: center;"><?php echo $nfceInutilizadas->qtd;?></td><td style="text-align: right;">R$ <?php if ($nfceInutilizadas->totalvNF>0) echo number_format( $nfceInutilizadas->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <tr>
                        <td>Download XML (NFC-e + NF-e):</td><td></td>
                        <td style="text-align: center;">
                            <a class="donwloadA" href="<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/xml_download.php?periodo=<?php echo $anoAtual.$mes;?>" target="_blank"><i class="icon icon-download"></i><span></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td>Totalizador</td>
                        <td style="text-align: center;"><?php echo $nfcePendentes->qtd + $nfcePendentesNull->qtd  + $nfceAutorizadas->qtd + $nfceCanceladas->qtd + $nfceInutilizadas->qtd;?></td>
                        <td style="text-align: right;">R$ <?php echo number_format( $nfcePendentes->totalvNF + $nfcePendentesNull->totalvNF + $nfceAutorizadas->totalvNF + $nfceCanceladas->totalvNF + $nfceInutilizadas->totalvNF, 2, ',', ' '); ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <h5>Estatísticas de NFC-e (Autorizadas/Canceladas e Inutilizadas)</h5>
                            <div id="chart-nfce" style=""></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">NF-e</td>
            <td>
                <table class='table table-bordered' id='tabela-mensalidades'>
                    <?php if ( ($nfePendentes->qtd + $nfePendentesNull->qtd) > 0) {?>
                        <tr><td style="background: #FFEB3B;">Notas Pendentes:<br/>
                                <small>*Notas pedentes poderão haver divergencias, instrua o cliente a verificar as pedências na emissão.</small>
                            </td><td style="text-align: center;background: #FFEB3B;"><?php echo $nfePendentes->qtd + $nfePendentesNull->qtd;?></td><td style="text-align: right;background: #FFEB3B;">R$ <?php if ($nfePendentes->totalvNF>0 || $nfePendentesNull >0)  echo number_format($nfePendentes->totalvNF + $nfePendentesNull->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <?php } else { ?>
                        <tr><td>Notas Pendentes:</td><td style="text-align: center;">0</td><td style="text-align: right;">R$ 0,00</td></tr>
                    <?php } ?>
                    <tr><td>Notas Autorizadas:</td><td style="text-align: center;"><?php echo $nfeAutorizadas->qtd;?></td><td style="text-align: right;">R$ <?php if ($nfeAutorizadas->totalvNF>0) echo  number_format($nfeAutorizadas->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <tr><td>Notas Canceladas:</td><td style="text-align: center;"><?php echo $nfeCanceladas->qtd;?></td><td style="text-align: right;">R$ <?php if ($nfeCanceladas->totalvNF>0) echo number_format( $nfeCanceladas->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <tr><td>Notas Inutilizadas:</td><td style="text-align: center;"><?php echo $nfeInutilizadas->qtd;?></td><td style="text-align: right;">R$ <?php if ($nfeInutilizadas->totalvNF>0) echo number_format( $nfeInutilizadas->totalvNF, 2, ',', ' '); else echo '0.00';?></td></tr>
                    <tr>
                        <td>Download XML (NFC-e + NF-e):</td><td></td>
                        <td style="text-align: center;">
                            <a class="donwloadA" href="<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/xml_download.php?periodo=<?php echo $anoAtual.$mes;?>" target="_blank"><i class="icon icon-download"></i><span></span></a>
                        </td>
                    </tr>
                    <tr>
                        <td>Totalizador</td>
                        <td style="text-align: center;"><?php echo $nfePendentes->qtd + $nfePendentesNull->qtd  + $nfeAutorizadas->qtd + $nfeCanceladas->qtd + $nfeInutilizadas->qtd;?></td>
                        <td style="text-align: right;">R$ <?php echo number_format( $nfePendentes->totalvNF + $nfePendentesNull->totalvNF + $nfeAutorizadas->totalvNF + $nfeCanceladas->totalvNF + $nfeInutilizadas->totalvNF, 2, ',', ' '); ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <h5>Estatísticas de NF-e (Autorizadas/Canceladas e Inutilizadas)</h5>
                            <div id="chart-nfe" style=""></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Totalizadores</td>
            <td>
                <h5>Pendentes / Autorizadas / Canceladas e Inutilizadas</h5><br/>
                Total: R$ <?php echo number_format( $nfcePendentes->totalvNF + $nfcePendentesNull->totalvNF + $nfceAutorizadas->totalvNF + $nfceCanceladas->totalvNF + $nfceInutilizadas->totalvNF + $nfePendentes->totalvNF + $nfePendentesNull->totalvNF + $nfeAutorizadas->totalvNF + $nfeCanceladas->totalvNF + $nfeInutilizadas->totalvNF, 2, ',', ' ');?><br/>
                Qtd total: <?php echo ( $nfcePendentes->qtd + $nfcePendentesNull->qtd  + $nfceAutorizadas->qtd + $nfceCanceladas->qtd + $nfceInutilizadas->qtd + $nfePendentes->qtd + $nfePendentesNull->qtd  + $nfeAutorizadas->qtd + $nfeCanceladas->qtd + $nfeInutilizadas->qtd);?><br/>
                <br/>
                <h5>Autorizadas / Canceladas</h5>
                Total: R$ <?php echo number_format(   $nfceAutorizadas->totalvNF + $nfceCanceladas->totalvNF + $nfeAutorizadas->totalvNF + $nfeCanceladas->totalvNF , 2, ',', ' ');?><br/>
                Qtd total: <?php echo ( $nfceAutorizadas->qtd + $nfceCanceladas->qtd + $nfeAutorizadas->qtd + $nfeCanceladas->qtd);?>

            </td>
        </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        <?php if ($graficoNFCE != null) { ?>

                    var data = [
                        <?php foreach ($graficoNFCE as $nfce) {

                            $dhEmi = $nfce->dhEmi;
                            $mesG =  explode('-', $dhEmi);
                            $mesG = getMes($mesG[1]);

                        echo "['" .$mesG.' - R$ '.number_format($nfce->totalvNF, 2, ',', ' '). "', " . $nfce->totalvNF . "],";
                    } ?>

                    ];
                    var plot1 = jQuery.jqplot('chart-nfce', [data],
                        {
                            seriesDefaults: {
                                // Make this a pie chart.
                                renderer: jQuery.jqplot.PieRenderer,
                                rendererOptions: {
                                    // Put data labels on the pie slices.
                                    // By default, labels show the percentage of the slice.
                                    showDataLabels: true
                                }
                            },
                            legend: {show: true, location: 'e'}
                        }
                    );
        <?php } ?>



        <?php if ($graficoNFE != null) { ?>

        var data = [
            <?php foreach ($graficoNFE as $nfe) {

            $dhEmi = $nfe->dhEmi;
            $mesG =  explode('-', $dhEmi);
            $mesG = getMes($mesG[1]);

            echo "['" .$mesG.' - R$ '.number_format($nfe->totalvNF, 2, ',', ' '). "', " . $nfe->totalvNF . "],";
        } ?>

        ];
        var plot1 = jQuery.jqplot('chart-nfe', [data],
            {
                seriesDefaults: {
                    // Make this a pie chart.
                    renderer: jQuery.jqplot.PieRenderer,
                    rendererOptions: {
                        // Put data labels on the pie slices.
                        // By default, labels show the percentage of the slice.
                        showDataLabels: true
                    }
                },
                legend: {show: true, location: 'e'}
            }
        );
        <?php } ?>
    });

    function abrirConsulta() {
        var mes = document.getElementById('mes').value;
        window.location = '<?php echo base_url() ?>index.php/conectecontador/painel/'+mes;
    }
</script>


<?php

function getMes($mes) {
    if ($mes == '01') {
        return 'Janeiro';
    } else if ($mes == '02'){
        return 'Fevereiro';
    } else if ($mes == '03'){
        return 'Março';
    } else if ($mes == '04'){
        return 'Abril';
    } else if ($mes == '05'){
        return 'Maio';
    } else if ($mes == '06'){
        return 'Junho';
    } else if ($mes == '07'){
        return 'Julho';
    } else if ($mes == '08'){
        return 'Agosto';
    } else if ($mes == '09'){
        return 'Setembro';
    } else if ($mes == '10'){
        return 'Outubro';
    } else if ($mes == '11'){
        return 'Novembro';
    } else if ($mes == '12'){
        return 'Dezembro';
    }
}

?>