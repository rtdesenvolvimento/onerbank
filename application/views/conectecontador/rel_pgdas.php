<div class="row-fluid" style="margin-top: 0" id="div_filtros">

    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
                <h5>Relatórios Customizáveis - PGDAS</h5>
            </div>
            <div class="widget-content">
                <div class="span12 well">
                    <div class="span12 alert alert-info">Deixe em branco caso não deseje utilizar o parâmetro.</div>
                    <form action="<?php echo base_url() ?>index.php/relatorios/produtosCustom" id="formRelatorio" method="get">
                        <div class="span12 well">
                            <div class="span6">
                                <label for="">Data início:</label>
                                <input type="date" name="dataInicio" class="span12" />
                            </div>
                            <div class="span6">
                                <label for="">até:</label>
                                <input type="date"  name="dataFinal" class="span12" />
                            </div>
                        </div>

                        <div class="span12" style="margin-left: 0; text-align: center">
                            <input type="reset" class="btn" value="Limpar" />
                            <button class="btn btn-inverse" id="pdf"><i class="icon-print icon-white"></i> PDF</button>
                            <button id="preview" class="btn btn-inverse"><i class="icon-eye-open"></i> Preview</button>
                        </div>
                    </form>
                </div>
                .
            </div>
        </div>
    </div>
</div>

<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-inverse span2"><i class="icon-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
            <h5>Preview</h5>

        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/conectecontador/pgdasCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/conectecontador/pgdasCustom');
            $("#formRelatorio").submit();
        });


        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });
    });
</script>