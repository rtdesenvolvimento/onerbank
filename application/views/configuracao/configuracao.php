<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5></h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhuma Configuração Cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Configuração</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    echo '<tr>';
                    echo '    <td>' . $r->idConfiguracao . '</td>';
                    echo '    <td>' . $r->descricao . '</td>';
                    echo '    <td>';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/configuracao/editar/' . $r->idConfiguracao . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>


<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var configuracao = $(this).attr('configuracao');
            $('#idConfiguracao').val(configuracao);
        });
    });
</script>