<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Configurações gerais do sistema <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formConfiguracao" method="post" class="form-horizontal">
                    <?php echo form_hidden('idConfiguracao', $result->idConfiguracao) ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label for="descricao" class="control-label">Descrição</label>
                            <div class="controls">
                                <input id="descricao" type="text" readonly class="form-control" name="descricao"
                                       value="<?php echo $result->descricao ?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6" style="padding: 1%; margin-left: 0">
                            <label for="tecnico">Consumidor não identificado<span class="required">*</span></label>
                            <select name="cliente_padrao_id" class="form-control" id="usuarios_id" required="required">
                                <option value="">--Selecione um cliente padrão--</option>
                                <?php foreach ($clientes as $cliente) {?>
                                    <option <?php if ($result->cliente_padrao_id == $cliente->idClientes) echo 'selected="selected"'; ?> value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-12 col-sm-12" style="margin-bottom: 20px;">
                            <div class="controls">
                                <input id="permiteEstoqueNegativo" <?php if($result->permiteEstoqueNegativo == 1) echo 'checked="checked"';?> type="checkbox" value="1" name="permiteEstoqueNegativo"/> Permite estoque negativo?
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12" style="margin-bottom: 20px;">
                            <div class="controls">
                                <input id="usarNFCE" <?php if($result->usarNFCE == 1) echo 'checked="checked"';?> type="checkbox" value="1" name="usarNFCE"/> Usar NFC-e no PDV?
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Configurações da NF-e / NFC-e</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-12 col-sm-12" style="padding: 1%; margin-left: 0;">
                                        <div class="formBuscaGSA" id="divBuscaCliente">
                                            <table width="100%">
                                                <tr>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <label for="numeroProximoNNFE" >Inicia Númeração da NF-e</label>
                                                        <input id="numeroProximoNNFE" class="form-control" type="text" value="<?php echo $result->numeroProximoNNFE ?>" name="numeroProximoNNFE">
                                                    </td>
                                                    <td colspan="4">
                                                        <label for="numeroDeSerieNFE" >Número de Série NF-e</label>
                                                        <input id="numeroDeSerieNFE" class="form-control" type="text" value="<?php echo $result->numeroDeSerieNFE ?>" name="numeroDeSerieNFE">
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <label for="numeroProximoNFCE" >Inicia Númeração da NFC-e</label>
                                                        <input id="numeroProximoNFCE" class="form-control" type="text" value="<?php echo $result->numeroProximoNFCE ?>" name="numeroProximoNFCE">
                                                    </td>
                                                    <td colspan="4">
                                                        <label for="numeroDeSerieNFCE" >Número de Série NFC-e</label>
                                                        <input id="numeroDeSerieNFCE" class="form-control" type="text" value="<?php echo $result->numeroDeSerieNFCE ?>" name="numeroDeSerieNFCE">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <label for="pICMSST">Alíquota(%) ICMS ST</label>
                                                        <input id="pICMSST" class="form-control" type="text" value="<?php echo $result->pICMSST ?>" name="pICMSST">
                                                    </td>
                                                    <td colspan="4">
                                                        <label for="pIPI">Alíquota(%) IPI</label>
                                                        <input id="pIPI" class="form-control" type="text" value="<?php echo $result->pIPI ?>" name="pIPI">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <label for="pPIS">Alíquota(%) PIS</label>
                                                        <input id="pPIS" class="form-control" type="text" value="<?php echo $result->pPIS ?>" name="pPIS">
                                                    </td>
                                                    <td colspan="4">
                                                        <label for="pCOFINS">Alíquota(%) COFINS</label>
                                                        <input id="pCOFINS" class="form-control" type="text" value="<?php echo $result->pCOFINS ?>" name="pCOFINS">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <label for="pCOFINS">Nat. de operação (Estadual)</label>
                                                        <select name="naturezaoperacaodentroestado_id" class="form-control" class="form-control">
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($naturezasoperacao as $operacao) {
                                                                if ($result->naturezaoperacaodentroestado_id == $operacao->idNaturezaoperacao) {
                                                                    echo '<option value="' . $operacao->idNaturezaoperacao . '" selected="selected">' . $operacao->nome . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $operacao->idNaturezaoperacao . '">' . $operacao->nome . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td colspan="4">
                                                        <label for="pCOFINS">Nat. de operação (Interestadual)</label>
                                                        <select name="naturezaoperacaoforaestado_id" class="form-control" class="form-control">
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($naturezasoperacao as $operacao) {
                                                                if ($result->naturezaoperacaoforaestado_id == $operacao->idNaturezaoperacao) {
                                                                    echo '<option value="' . $operacao->idNaturezaoperacao . '" selected="selected">' . $operacao->nome . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $operacao->idNaturezaoperacao . '">' . $operacao->nome . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <label for="CNPJCredenciadoraCartao">Informar o CNPJ da Credenciadora de cartão de crédito / débito </label>
                                                        <input id="CNPJCredenciadoraCartao" class="form-control" type="text" value="<?php echo $result->CNPJCredenciadoraCartao ?>" name="CNPJCredenciadoraCartao">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <label for="tBand">Bandeira da operadora de cartão de crédito e/ou débito
                                                        </label>
                                                        <select name="tBand" class="form-control" class="form-control">
                                                            <option value=""></option>
                                                            <option value="01" <?php if ($result->tBand == '01') echo 'selected="selected"';?> >Visa</option>
                                                            <option value="02" <?php if ($result->tBand == '02') echo 'selected="selected"';?>>Mastercard</option>
                                                            <option value="03" <?php if ($result->tBand == '03') echo 'selected="selected"';?>>American Express</option>
                                                            <option value="04" <?php if ($result->tBand == '04') echo 'selected="selected"';?>>Sorocred</option>
                                                            <option value="05" <?php if ($result->tBand == '05') echo 'selected="selected"';?>>Diners Club</option>
                                                            <option value="06" <?php if ($result->tBand == '06') echo 'selected="selected"';?>>Elo</option>
                                                            <option value="07" <?php if ($result->tBand == '07') echo 'selected="selected"';?>>Hipercard</option>
                                                            <option value="08" <?php if ($result->tBand == '08') echo 'selected="selected"';?>>Aura</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr><td colspan="8">&nbsp;</td></tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Configurações de cobranca - apenas para quem utiliza integração com a zoop.com.br</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-12 col-sm-12" style="padding: 1%; margin-left: 0">
                                        <div class="formBuscaGSA" id="divBuscaCliente">
                                            <table width="100%">
                                                <tr>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    <td width="12.5%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="qtdDiasVencimentoEnviarCobranca" >Quantos dias antes do vencimento você deseja enviar as cobranças?</label>
                                                            <select name="qtdDiasVencimentoEnviarCobranca" class="form-control" required="required" class="form-control">
                                                                <option value="1" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 1) echo 'selected="selected"';?>>1 dia antes do vencimento</option>
                                                                <option value="2" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 2) echo 'selected="selected"';?>>2 dias antes do vencimento</option>
                                                                <option value="3" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 3) echo 'selected="selected"';?>>3 dias antes do vencimento</option>
                                                                <option value="4" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 4) echo 'selected="selected"';?>>4 dias antes do vencimento</option>
                                                                <option value="5" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 5) echo 'selected="selected"';?>>5 dias antes do vencimento</option>
                                                                <option value="6" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 6) echo 'selected="selected"';?>>6 dias antes do vencimento</option>
                                                                <option value="7" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 7) echo 'selected="selected"';?>>7 dias antes do vencimento</option>
                                                            </select>
                                                        </td>

                                                        <td colspan="4">
                                                            <label for="prazoMaximoPagamentoAposVencimento" >Qual prazo máximo de pagamento após vencimento?</label>
                                                            <select name="prazoMaximoPagamentoAposVencimento" class="form-control" required="required" class="form-control">
                                                                <option value="3" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 3) echo 'selected="selected"';?>>até 3 dias</option>
                                                                <option value="5" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 5) echo 'selected="selected"';?>>até 5 dias</option>
                                                                <option value="7" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 7) echo 'selected="selected"';?>>até 7 dias</option>
                                                                <option value="10" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 10) echo 'selected="selected"';?>>até 10 dias</option>
                                                                <option value="15" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 15) echo 'selected="selected"';?>>até 15 dias</option>
                                                                <option value="20" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 20) echo 'selected="selected"';?>>até 20 dias</option>
                                                                <option value="25" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 25) echo 'selected="selected"';?>>até 25 dias</option>
                                                                <option value="29" <?php if ($result->qtdDiasVencimentoEnviarCobranca == 29) echo 'selected="selected"';?>>até 29 dias</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="multaAtrasoBoleto" >Qual multa por atraso de pagamento? <small>2% máximo permitido por lei</small></label>
                                                            <input id="multaAtrasoBoleto" class="form-control" type="number" value="<?php echo $result->multaAtrasoBoleto ?>" name="multaAtrasoBoleto">
                                                        </td>
                                                        <td colspan="4">
                                                            <label for="jurosAtrasoBoleto" >Qual os juros diário por atraso de pagamento?<small>1% máximo permitido por lei</small></label>
                                                            <input id="jurosAtrasoBoleto" class="form-control" type="number" value="<?php echo $result->jurosAtrasoBoleto ?>" name="jurosAtrasoBoleto">
                                                        </td>
                                                    </tr>
                                            </table>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12" style="padding: 1%; margin-left: 0;display: none;">
                            <div class="formBuscaGSA" id="divBuscaCliente">
                                <label for="tecnico">Configurações NFS-e/RPS</label>

                                <table width="100%">
                                    <tr>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                        <td width="12.5%"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <label for="serieRps" >Série da Nota</label>
                                            <input id="serieRps" class="span12" type="text" value="<?php echo $result->serieRps ?>" name="serieRps">
                                        </td>
                                        <td colspan="3">
                                            <label for="numeroProximoNumeroRps" >Número RPS Inícial</label>
                                            <input id="numeroProximoNumeroRps" class="span12" type="text" value="<?php echo $result->numeroProximoNumeroRps ?>" name="numeroProximoNumeroRps">
                                        </td>
                                        <td colspan="2">
                                            <label for="numeroProximoNumeroLote" >Número Lote Inícial</label>
                                            <input id="numeroProximoNumeroLote" class="span12" type="text" value="<?php echo $result->numeroProximoNumeroLote ?>" name="numeroProximoNumeroLote">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <label for="isCalcularValorISS" >Calcular valor do ISS?</label>
                                            <select class="span12" name="isCalcularValorISS" id="isCalcularValorISS">
                                                <option <?php if ($result->isCalcularValorISS == 1) echo 'selected="selected"';?> value="1">Sim</option>
                                                <option <?php if ($result->isCalcularValorISS == 2) echo 'selected="selected"';?> value="2">Não</option>
                                            </select>
                                        </td>
                                        <td colspan="3">
                                            <label for="isReterISS" >Exigibilidade de ISS?</label>
                                            <select class="span12" name="isReterISS" id="isCalcularValorISS">
                                                <option <?php if ($result->isReterISS == 1) echo 'selected="selected"';?> value="1">Sim</option>
                                                <option <?php if ($result->isReterISS == 2) echo 'selected="selected"';?> value="2">Não</option>
                                            </select>
                                        </td>
                                        <td colspan="2">
                                            <label for="isReterINSS" >Reter INSS?</label>
                                            <select class="span12" name="isReterINSS" id="isReterINSS">
                                                <option <?php if ($result->isReterINSS == 1) echo 'selected="selected"';?> value="1">Sim</option>
                                                <option <?php if ($result->isReterINSS == 2) echo 'selected="selected"';?> value="2">Não</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <label for="aliquotaIR" >% IR</label>
                                            <input id="aliquotaIR" class="span12" type="number" value="<?php echo $result->aliquotaIR ?>" name="aliquotaIR">
                                        </td>
                                        <td colspan="2">
                                            <label for="aliquotaINSS" >% INSS</label>
                                            <input id="aliquotaINSS" class="span12" type="number" value="<?php echo $result->aliquotaINSS ?>" name="aliquotaINSS">
                                        </td>
                                        <td colspan="2">
                                            <label for="aliquotaCofins" >% COFINS</label>
                                            <input id="aliquotaCofins" class="span12" type="number" value="<?php echo $result->aliquotaCofins ?>" name="aliquotaCofins">
                                        </td>
                                        <td colspan="1">
                                            <label for="aliquotaPIS" >% PIS</label>
                                            <input id="aliquotaPIS" class="span12" type="number" value="<?php echo $result->aliquotaPIS ?>" name="aliquotaPIS">
                                        </td>
                                        <td colspan="1">
                                            <label for="aliquotaContribuicaoSocial" >% CSLL</label>
                                            <input id="aliquotaContribuicaoSocial" class="span12" type="number" value="<?php echo $result->aliquotaContribuicaoSocial ?>" name="aliquotaContribuicaoSocial">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <label for="descontarIRValor" >Descontar IR?</label>
                                            <select class="span12" name="descontarIRValor" id="descontarIRValor">
                                                <option <?php if ($result->descontarIRValor == 1) echo 'selected="selected"';?> value="1">Sim</option>
                                                <option <?php if ($result->descontarIRValor == 2) echo 'selected="selected"';?> value="2">Não</option>
                                            </select>
                                        </td>
                                        <td colspan="4">
                                            <label for="valorMinimoCalculoIR" >Valor Mínimo Calculo IR</label>
                                            <input id="valorMinimoCalculoIR" class="span12" type="number" value="<?php echo $result->valorMinimoCalculoIR ?>" name="valorMinimoCalculoIR">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <label for="reterCSLLPISCONFINSNotaAcima5000" >Reter CSLL/PIS/COFINS?</label>
                                            <select class="span12" name="reterCSLLPISCONFINSNotaAcima5000" id="reterCSLLPISCONFINSNotaAcima5000">
                                                <option <?php if ($result->reterCSLLPISCONFINSNotaAcima5000 == 1) echo 'selected="selected"';?> value="1">Sim</option>
                                                <option <?php if ($result->reterCSLLPISCONFINSNotaAcima5000 == 2) echo 'selected="selected"';?> value="2">Não</option>
                                            </select>
                                        </td>
                                        <td colspan="4">
                                            <label for="reterCSLLPISCONFINSSomaImpostosAcima" >Reter CSLL/PIS/COFINS Acima de ?</label>
                                            <input id="reterCSLLPISCONFINSSomaImpostosAcima" class="span12" type="number" value="<?php echo $result->reterCSLLPISCONFINSSomaImpostosAcima ?>" name="reterCSLLPISCONFINSSomaImpostosAcima">
                                        </td>
                                    </tr>
                                    <tr><td colspan="8">&nbsp;</td></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-actions" style="text-align: right;">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Alterar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formConfiguracao').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>