<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                      <i class="icon-align-justify"></i>
                </span>
                <h5>Adicionar Grupo TecnoSpeed</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formConfiguracaoTecnoSpeed" method="post" class="form-horizontal">

                    <div class="control-group">
                        <label for="cod_grupo" class="control-label">Código<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cod_grupo" type="text" required="required" name="cod_grupo"
                                   value=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição<span class="required">*</span></label>
                        <div class="controls">
                            <input id="descricao" type="text" required="required" name="descricao"
                                   value=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="grupo" class="control-label">Gupo<span class="required">*</span></label>
                        <div class="controls">
                            <input id="grupo" type="text" required="required" name="grupo" value=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="usuario" class="control-label">Usuário<span class="required">*</span></label>
                        <div class="controls">
                            <input id="usuario" type="text" required="required" name="usuario" value=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="senha" class="control-label">Senha<span class="required">*</span></label>
                        <div class="controls">
                            <input id="senha" type="text" required="required" name="senha" value=""/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/configuracaotecnospeed" id="btnAdicionar" class="btn"><i
                                        class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formConfiguracaoTecnoSpeed').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>