<a href="<?php echo base_url() ?>index.php/configuracaotecnospeed/adicionar" class="btn btn-success">
    <i class="icon-plus icon-white"></i> Adicionar novo Grupo TecnoSpeed
</a>
<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Grupo TecnoSpeed</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Descrição</th>
                    <th>grupo</th>
                    <th>usuario</th>
                    <th>senha</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhuma Grupo TecnoSpeed cadastrado Cadastrado</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Grupo TecnoSpeed</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Código</th>
                    <th>Descrição</th>
                    <th>grupo</th>
                    <th>usuario</th>
                    <th>senha</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    echo '<tr>';
                    echo '    <td>' . $r->idConfiguracaotecnospeed . '</td>';
                    echo '    <td>' . $r->cod_grupo . '</td>';
                    echo '    <td>' . $r->descricao . '</td>';
                    echo '    <td>' . $r->grupo . '</td>';
                    echo '    <td>' . $r->usuario . '</td>';
                    echo '    <td>' . $r->senha . '</td>';
                    echo '    <td>';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/configuracaotecnospeed/editar/' . $r->idConfiguracaotecnospeed . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '      <a href="#modal-excluir" role="button" data-toggle="modal" configuracaotecnospeed="' . $r->idConfiguracaotecnospeed . '" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>  ';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/configuracaotecnospeed/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Grupo</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idConfiguracaotecnospeed" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este grupo?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var configuracaotecnospeed = $(this).attr('configuracaotecnospeed');
            $('#idConfiguracaotecnospeed').val(configuracaotecnospeed);
        });
    });
</script>