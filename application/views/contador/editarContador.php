<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                      <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Contador</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formContador" method="post" class="form-horizontal">
                    <?php echo form_hidden('idContador', $result->idContador) ?>

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" required="required" value="<?php echo $result->nome ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cnpjCpf" class="control-label">CNPJ/CPF</label>
                        <div class="controls">
                            <input id="cnpjCpf" type="text" name="cnpjCpf" value="<?php echo $result->cnpjCpf ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="login" class="control-label">Login<span class="required">*</span></label>
                        <div class="controls">
                            <input id="login" type="text" name="login" required="required" value="<?php echo $result->login ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="senha" class="control-label">Senha<span class="required">*</span></label>
                        <div class="controls">
                            <input id="senha" type="text" name="senha" required="required" value="<?php echo $result->senha ?>"/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/contador" id="btnAdicionar" class="btn"><i
                                        class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formContador').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>