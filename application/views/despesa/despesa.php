<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Plano de contas - Despesas</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a class="btn btn-success" href="<?php echo base_url(); ?>despesa/adicionar">Cadastrar Despesa</a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>Código</th>
                                    <th>Despesa superior</th>
                                    <th>Nome</th>
                                    <th style="width: 10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    echo '<tr>';
                                    echo '    <td>' . $r->codigo . '</td>';
                                    echo '    <td>' . $r->superior . '</td>';
                                    echo '    <td>' . $r->nome . '</td>';
                                    echo '    <td>';
                                    echo '        <div class="btn-group btn-group-sm" role="group" aria-label="...">';
                                    echo '          <a href="' . base_url() . 'index.php/despesa/editar/' . $r->idDespesa . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    echo '          <a href="#modal-excluir" role="button" data-toggle="modal" despesa="' . $r->idDespesa . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>  ';
                                    echo '        </div>';
                                    echo '    </td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/despesa/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Despesa</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idDespesa" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este tipo de despesa?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var despesa = $(this).attr('despesa');
            $('#idDespesa').val(despesa);
        });
    });
</script>