<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de despesa <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formDespesa" method="post" class="form-horizontal">
                    <?php echo form_hidden('idDespesa', $result->idDespesa) ?>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label for="grupoProduto_id" class="control-label">Despesa superior</label>
                            <div class="controls">
                                <select name="despesaSuperior_id" class="form-control">
                                    <option value="">Selecione uma despesa superior</option>
                                    <?php
                                    foreach($despesas as $despesa){
                                        $sel = '';
                                        if ($despesa->idDespesa == $result->despesaSuperior_id) $sel = 'selected="selected"';
                                        echo '<option value="'.$despesa->idDespesa.'" '.$sel.'>'.$despesa->nome.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label for="codigo" class="control-label">Código<span class="required">*</span></label>
                            <div class="controls">
                                <input id="codigo" type="text" name="codigo" class="form-control" value="<?php echo $result->codigo ?>"/>
                            </div>
                        </div>
                        <div class="col-md-10 col-sm-10">
                            <label for="nome" class="control-label">Descrição<span class="required">*</span></label>
                            <div class="controls">
                                <input id="nome" type="text" name="nome" class="form-control" value="<?php echo $result->nome ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Alterar</button>
                            <a href="<?php echo base_url() ?>index.php/despesa" id="btnAdicionar" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formDespesa').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>