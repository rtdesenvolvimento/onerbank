<?php

$licenca  =  fopen('banco_dados.txt', 'r');
$hostname = '';
$username = '';
$password = '';

$index = 0;
while (!feof($licenca)) :
    $linha = fgets($licenca, 1024);

    if ($index == 0) {
        $hostname = $linha;
    }

    if ($index == 1) {
        $username = $linha;
    }

    if ($index == 2) {
        $password = $linha;
    }

    $index = $index + 1;
endwhile;
fclose($licenca);

?>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Cadastro de Oficina Matriz</h5>
            </div>
            <div class="widget-content nopadding">

                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="span12" id="divCadastrarOs">

                            <ul class="nav nav-tabs">
                                <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da Oficina Matriz</a></li>
                            </ul>

                            <form action="<?php echo current_url(); ?>" id="formEmpresa" method="post"  enctype="multipart/form-data"  class="form-horizontal">


                                <div class="span12" style="padding: 1%;margin-left: 0">

                                    <div class="span3">
                                        <div class="formBuscaGSA">
                                            <label for="tipoPessoa" >Tipo de pessoa</label>
                                            <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                                                <option value="PF">Pessoa Física</option>
                                                <option value="PJ" selected="selected">Pessoa Jurídica</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="span9">
                                        <div class="formBuscaGSA">
                                            <label for="documento"><div id="div_cpfCnpj">CPF / Nome ou Razão Social*</div></label>

                                            <input id="documento"  onblur="consultaDadosEmpresa('<?php echo base_url();?>');" type="number" style="float: left;margin-right: 10px;" class="span4"
                                                   required="required" name="documento"
                                                   value=""/>

                                            <input id="nomeEmpresa"  class="span8" type="text"  required="required" name="nomeEmpresa"
                                                   value=""/>
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%;margin-left: 0;margin-top: -25px;">

                                    <div class="span10">
                                        <label for="nomeFantasiaApelido" ><div id="div_nomeFantasia">Apelido/Nome Fantasia</div></label>
                                        <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                                               value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                                    </div>

                                    <div class="span2" id="div_sexo" style="display: none;">
                                        <label for="sexo">Sexo</label>
                                        <select name="sexo" id="sexo" class="span12 chzn" required="required">
                                            <option value="M">Masculino</option>
                                            <option value="F">Feminino</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0;display: none;" id="div_rg">
                                    <div class="span3">
                                        <label for="rg">RG</label>
                                        <input id="rg" type="text" class="span12" name="rg"
                                               value="<?php echo set_value('rg'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="orgaoEmissor">Orgão Emissor</label>
                                        <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                                               value="<?php echo set_value('orgaoEmissor'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="estadoOrgaoEmissor">Estado Emissor</label>
                                        <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                                               value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="dataOrgaoEmissor">Data Emissão</label>
                                        <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                                               value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span3">
                                        <label for="telefone">Telefone</label>
                                        <input id="telefone" type="text" class="span12" name="telefone"
                                               value="<?php echo set_value('telefone'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="celular">Celular</label>
                                        <input id="celular" type="text" class="span12" name="celular"
                                               value="<?php echo set_value('celular'); ?>"/>
                                    </div>

                                    <div class="span4">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="span12" name="email"
                                               value="<?php echo set_value('email'); ?>"/>
                                    </div>

                                    <div class="span2">
                                        <label for="email">Data de nascimento</label>
                                        <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                                               value="<?php echo set_value('data_nascimento'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="margin-left: 0">

                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#dadosEndereco" data-toggle="tab">Dados do endereço</a></li>
                                        <li><a href="#dadosContato" data-toggle="tab">Dados do contato</a></li>
                                        <li><a href="#dadosAcesso" data-toggle="tab">Banco de Dados</a></li>
                                        <li><a href="#dadosFiscais" data-toggle="tab">Dados Fiscais</a></li>
                                        <li><a href="#dadosNFE" data-toggle="tab">Config.  NF-e</a></li>
                                        <li><a href="#dadosNFSE" data-toggle="tab">Config.  NFS-e</a></li>
                                        <li><a href="#dadosObservacao" data-toggle="tab">Observação</a></li>
                                    </ul>
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="dadosEndereco">
                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span3">
                                                    <label for="cep">CEP<span class="required">*</span></label>
                                                    <input id="cep" type="text" name="cep" class="span12" required="required" onBlur="getConsultaCEP();"
                                                           value="<?php echo set_value('cep'); ?>"/>
                                                    <small>[TAB] consulta cep (Necessita Internet)</small>
                                                </div>

                                                <div class="span6">
                                                    <label for="rua">Rua<span class="required">*</span></label>
                                                    <input id="rua" type="text" name="rua"  class="span12" required="required" value="<?php echo set_value('rua'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="numero">Número</label>
                                                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span12">
                                                    <label for="complemento">Complemento<span class="required">*</span></label>
                                                    <input id="complemento" type="text" name="complemento" class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span5">
                                                    <label for="bairro">Bairro<span class="required">*</span></label>
                                                    <input id="bairro" type="text" name="bairro" required="required" class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                                                </div>

                                                <div class="span1">
                                                    <label for="codIBGECidade">Cod.IBGE</label>
                                                    <input id="codIBGECidade" type="text" readonly name="codIBGECidade" class="span12"  value="<?php echo set_value('codIBGECidade'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="cidade">Cidade<span class="required">*</span></label>
                                                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                                                </div>

                                                <div class="span1">
                                                    <label for="codIBGEEstado">Cod.IBGE</label>
                                                    <input id="codIBGEEstado"  readonly type="text" name="codIBGEEstado" class="span12"  value="<?php echo set_value('codIBGEEstado'); ?>"/>
                                                </div>

                                                <div class="span2">
                                                    <label for="estado">Estado<span class="required">*</span></label>
                                                    <input id="estado" type="text" name="estado" required="required" class="span12"  value="<?php echo set_value('estado'); ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="dadosContato">

                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span7">
                                                    <label for="contatoNomeEmpresa" >Nome</label>
                                                    <input id="contatoNomeEmpresa"  class="span12" type="text" name="contatoNomeEmpresa"
                                                           value="<?php echo set_value('contatoNomeEmpresa'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="contatoCpf">CPF</label>
                                                    <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                                                           value="<?php echo set_value('contatoCpf'); ?>"/>
                                                </div>

                                                <div class="span2">
                                                    <label for="contatoSexo">Sexo</label>
                                                    <select name="contatoSexo" id="contatoSexo" class="span12 chzn" required="required">
                                                        <option value="M">Masculino</option>
                                                        <option value="F">Feminino</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span3">
                                                    <label for="contatoTelefone">Telefone</label>
                                                    <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                                                           value="<?php echo set_value('contatoTelefone'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="contatoCelular">Celular</label>
                                                    <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                                                           value="<?php echo set_value('contatoCelular'); ?>"/>
                                                </div>

                                                <div class="span4">
                                                    <label for="email">Email</label>
                                                    <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                                                           value="<?php echo set_value('contatoEmail'); ?>"/>
                                                </div>

                                                <div class="span2">
                                                    <label for="email">Data de nascimento</label>
                                                    <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                                                           value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane" id="dadosAcesso">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="hostname">Hostname<span class="required">*</span></label>
                                                    <input type="text" name="hostname" required="required" class="span12" id="hostname"
                                                           value="<?php echo $hostname;?>"/>
                                                </div>

                                                <div class="span4">
                                                    <label for="username">Username<span class="required">*</span></label>
                                                    <input type="text" name="username" required="required" class="span12" id="username"
                                                           value="<?php echo $username;?>"/>
                                                </div>

                                                <div class="span4">
                                                    <label for="password">Password</label>
                                                    <input type="text" name="password"  class="span12" id="password"
                                                           value="<?php echo $password;?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="dadosFiscais">
                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span6">
                                                    <label for="natureza_juridica">Natureza Jurídica</label>
                                                    <input id="natureza_juridica" type="text" class="span12" name="natureza_juridica"
                                                           value="<?php echo set_value('natureza_juridica'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="configuracao_tecnospeed_id">Grupo TecnoSpeed</label>
                                                    <select name="configuracao_tecnospeed_id" id="configuracao_tecnospeed_id" class="span12">
                                                        <?php foreach ($tecnospeeds as $tecnospeed) {?>
                                                            <option value="<?php echo $tecnospeed->idConfiguracaotecnospeed;?>"><?php echo $tecnospeed->descricao;?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="span3">
                                                    <label for="contador_id">Contador</label>
                                                    <select name="contador_id" id="contador_id" class="span12">
                                                        <?php foreach ($contadores as $contador) {?>
                                                            <option value="<?php echo $contador->idContador;?>"><?php echo $contador->nome;?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span3">
                                                    <label for="IE">Insc. Est</label>
                                                    <input id="IE" type="text" class="span12" name="IE"
                                                           value="<?php echo set_value('IE'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="IESUF">Insc. SUFRAMA</label>
                                                    <input id="IESUF" type="text" class="span12" name="IESUF"
                                                           value="<?php echo set_value('IESUF'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="IEST">Insc. ST</label>
                                                    <input id="IEST" type="text" class="span12" name="IEST"
                                                           value="<?php echo set_value('IEST'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="insmun">Insc. MN</label>
                                                    <input id="insmun" type="text" class="span12" name="insmun"
                                                           value="<?php echo set_value('insmun'); ?>"/>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="ambiente">Ambiente</label>
                                                    <select name="ambiente" class="span12" id="ambiente">
                                                        <option value="1">Produção</option>
                                                        <option value="2" selected="selected">Homologação</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="certificado">Certificado</label>
                                                    <select name="certificado" class="span12" id="certificado">
                                                        <option value="A1">A1</option>
                                                        <option value="A3">A3</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="regimeTributario">CRT - Código de Regime Tributário da NF-e</label>
                                                    <select name="regimeTributario" class="span12" id="regimeTributario">
                                                        <option value="1 selected="selected">Simples Nacional</option>
                                                        <option value="2">Simples Nacional, exesso sublime de receita bruta.</option>
                                                        <option value="3">Regime Normal. (v2.0)</option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span4">
                                                    <label for="cert">Certificado Digital A1</label>
                                                    <input type="file" name="cert" id="arquivocert" class="span12">
                                                </div>

                                                <div class="span4">
                                                    <label for="cert">Senha do Certificado</label>
                                                    <input type="text" name="certsenha" class="span12" value=""/>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="dadosNFE">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span3">
                                                    <label for="cnae">CNAE</label>
                                                    <input id="cnae" type="text" class="span12" name="cnae"
                                                           value="<?php echo set_value('cnae'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="csc">CSC (NFC-e)</label>
                                                    <input id="csc" type="text" class="span12" name="csc"
                                                           value="<?php echo set_value('csc'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="idcsc">ID CSC</label>
                                                    <input id="idcsc" type="text" class="span12" name="idcsc"
                                                           value="<?php echo set_value('idcsc'); ?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="tokenibpt">Token IBPT</label>
                                                    <input id="tokenibpt" type="text" class="span12" name="tokenibpt"
                                                           value="<?php echo set_value('tokenibpt'); ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="dadosNFSE">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span6">
                                                    <label for="regimeEspecialTributacao">Regime especial de tributação</label>
                                                    <select name="regimeEspecialTributacao" class="span12" id="regimeEspecialTributacao">
                                                        <option value="0">Nenhuma das opções</option>
                                                        <option value="1">Microempresa munícipal</option>
                                                        <option value="2">Estimativa</option>
                                                        <option value="3">Sociedade de profissionais</option>
                                                        <option value="4">Cooperativa</option>
                                                        <option value="5">Microempresário individual (MEI)</option>
                                                        <option value="6">Microempresário e Empresa de Pequeno Porte, (ME EPP)</option>
                                                        <option value="7">Lucro Real</option>
                                                        <option value="8">Lucro Presumido</option>
                                                    </select>
                                                </div>

                                                <div class="span6">
                                                    <label for="tipoTributacao">Tipo de tribução</label>
                                                    <select name="tipoTributacao" class="span12" id="tipoTributacao">
                                                        <option value="1">Isenta de ISS</option>
                                                        <option value="2">Imune</option>
                                                        <option value="3">Não Incidência no Município</option>
                                                        <option value="4">Não Tributável</option>
                                                        <option value="5">Retida</option>
                                                        <option value="6" selected="selected">Tributável dentro do município</option>
                                                        <option value="7">Tributável fora do município</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="incentivadorCultural">Incentivador Cultural</label>
                                                    <select name="incentivadorCultural" class="span12" id="incentivadorCultural">
                                                        <option value="1">Sim</option>
                                                        <option value="2" selected="selected">Não</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="incentivoFiscal">Incentivo Fiscal</label>
                                                    <select name="incentivoFiscal" class="span12" id="incentivoFiscal">
                                                        <option value="1">Sim</option>
                                                        <option value="2" selected="selected">Não</option>
                                                    </select>
                                                </div>


                                                <div class="span4">
                                                    <label for="naturezaTributacao">Natureza da tributacao</label>
                                                    <select name="naturezaTributacao" class="span12" id="naturezaTributacao">
                                                        <option value="1">Simples Nacional</option>
                                                        <option value="2">Fixo</option>
                                                        <option value="3">Depósito em Juízo</option>
                                                        <option value="4">Exigibilidade suspensa por decisão judicial</option>
                                                        <option value="5">Exigibilidade suspensa por rocedimento administrativo</option>
                                                        <option value="6">Isenção parcial.</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="dadosObservacao">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span12">
                                                    <textarea class="span12" id="observacao" name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span6 offset3" style="text-align: center">
                                        <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i>
                                            Adicionar
                                        </button>
                                        <a href="<?php echo base_url() ?>index.php/empresa" id="" class="btn"><i
                                                class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/empresa.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });
    });
</script>




