<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Editar Oficina</h5>
            </div>
            <div class="widget-content nopadding">
                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>

                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da Oficina Matriz</a></li>
                    <li  id="tabfilial"><a href="#tab2" data-toggle="tab">Filiais</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="span12" id="divCadastrarEmpresa">

                            <form action="<?php echo current_url(); ?>" id="formEmpresa" method="post"  enctype="multipart/form-data"  class="form-horizontal">
                                <?php echo form_hidden('idEmpresa', $result->idEmpresa) ?>

                                <div class="span12" style="padding: 1%;margin-left: 0">

                                    <div class="span3">
                                        <div class="formBuscaGSA">
                                            <label for="tipoPessoa" >Tipo de pessoa</label>
                                            <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                                                <option value="PF" <?php if ($result->tipoPessoa == 'PF') echo 'selected="selected"';?>>Pessoa Física</option>
                                                <option value="PJ" <?php if ($result->tipoPessoa == 'PJ') echo 'selected="selected"';?>>Pessoa Jurídica</option>
                                            </select>
                                            <br/>
                                        </div>
                                    </div>

                                    <div class="span9">
                                        <div class="formBuscaGSA">
                                            <label for="documento"><div id="div_cpfCnpj">CPF / Nome ou Razão Social*</div></label>

                                            <input id="documento"  onblur="consultaDadosEmpresa('<?php echo base_url();?>');" type="number" style="float: left;margin-right: 10px;" class="span4"
                                                   required="required" name="documento"
                                                   value="<?php echo $result->documento;?>"/>

                                            <input id="nomeEmpresa"  class="span8" type="text"  required="required" name="nomeEmpresa"
                                                   value="<?php echo $result->nomeEmpresa;?>"/>
                                            <br/>
                                        </div>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%;margin-left: 0;margin-top: -25px;">

                                    <div class="span10">
                                        <label for="nomeFantasiaApelido" ><div id="div_nomeFantasia">Apelido/Nome Fantasia</div></label>
                                        <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                                               value="<?php echo $result->nomeFantasiaApelido;?>"/>
                                    </div>

                                    <div class="span2" id="div_sexo" style="display: none;">
                                        <label for="sexo">Sexo</label>
                                        <select name="sexo" id="sexo" class="span12 chzn" required="required">
                                            <option value="M" <?php if ($result->sexo == 'M') echo 'selected="selected"';?>>Masculino</option>
                                            <option value="F" <?php if ($result->sexo == 'F') echo 'selected="selected"';?>>Feminino</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0;display: none;" id="div_rg">
                                    <div class="span3">
                                        <label for="rg">RG</label>
                                        <input id="rg" type="text" class="span12" name="rg"
                                               value="<?php echo $result->rg;?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="orgaoEmissor">Orgão Emissor</label>
                                        <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                                               value="<?php echo $result->orgaoEmissor;?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="estadoOrgaoEmissor">Estado Emissor</label>
                                        <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                                               value="<?php echo $result->estadoOrgaoEmissor;?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="dataOrgaoEmissor">Data Emissão</label>
                                        <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                                               value="<?php echo $result->dataOrgaoEmissor;?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span3">
                                        <label for="telefone">Telefone</label>
                                        <input id="telefone" type="text" class="span12" name="telefone"
                                               value="<?php echo $result->telefone;?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="celular">Celular</label>
                                        <input id="celular" type="text" class="span12" name="celular"
                                               value="<?php echo $result->celular;?>"/>
                                    </div>

                                    <div class="span4">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="span12" name="email"
                                               value="<?php echo $result->email;?>"/>
                                    </div>

                                    <div class="span2">
                                        <label for="email">Data de nascimento</label>
                                        <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                                               value="<?php echo $result->data_nascimento;?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="margin-left: 0">

                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#dadosEndereco" data-toggle="tab">Dados do endereço</a></li>
                                        <li><a href="#dadosContato" data-toggle="tab">Dados do contato</a></li>
                                        <li><a href="#dadosAcesso" data-toggle="tab">Banco de Dados</a></li>
                                        <li><a href="#dadosFiscais" data-toggle="tab">Dados Fiscais</a></li>
                                        <li><a href="#dadosNFE" data-toggle="tab">Config.  NF-e</a></li>
                                        <li><a href="#dadosNFSE" data-toggle="tab">Config.  NFS-e</a></li>
                                        <li><a href="#dadosBoleto" data-toggle="tab">Boletos</a></li>
                                        <li><a href="#dadosObservacao" data-toggle="tab">Observação</a></li>
                                    </ul>
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="dadosEndereco">
                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span3">
                                                    <label for="cep">CEP<span class="required">*</span></label>
                                                    <input id="cep" type="text" name="cep" class="span12" required="required" onBlur="getConsultaCEP();"
                                                           value="<?php echo $result->cep;?>"/>
                                                    <small>[TAB] consulta cep (Necessita Internet)</small>
                                                </div>

                                                <div class="span6">
                                                    <label for="rua">Rua<span class="required">*</span></label>
                                                    <input id="rua" type="text" name="rua"  class="span12" required="required" value="<?php echo $result->rua;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="numero">Número</label>
                                                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo $result->numero;?>"/>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span12">
                                                    <label for="complemento">Complemento<span class="required">*</span></label>
                                                    <input id="complemento" type="text" name="complemento" class="span12"  value="<?php echo $result->complemento;?>"/>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span5">
                                                    <label for="bairro">Bairro<span class="required">*</span></label>
                                                    <input id="bairro" type="text" name="bairro" required="required" class="span12"  value="<?php echo $result->bairro;?>"/>
                                                </div>

                                                <div class="span1">
                                                    <label for="codIBGECidade">Cod.IBGE</label>
                                                    <input id="codIBGECidade" type="text" readonly name="codIBGECidade" class="span12"  value="<?php echo $result->codIBGECidade;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="cidade">Cidade<span class="required">*</span></label>
                                                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo $result->cidade;?>"/>
                                                </div>

                                                <div class="span1">
                                                    <label for="codIBGEEstado">Cod.IBGE</label>
                                                    <input id="codIBGEEstado"  readonly type="text" name="codIBGEEstado" class="span12"  value="<?php echo $result->codIBGEEstado;?>"/>
                                                </div>

                                                <div class="span2">
                                                    <label for="estado">Estado<span class="required">*</span></label>
                                                    <input id="estado" type="text" name="estado" required="required" class="span12"  value="<?php echo $result->estado;?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="dadosContato">

                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span7">
                                                    <label for="contatoNomeEmpresa" >Nome</label>
                                                    <input id="contatoNomeEmpresa"  class="span12" type="text" name="contatoNomeEmpresa"
                                                           value="<?php echo $result->contatoNomeEmpresa;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="contatoCpf">CPF</label>
                                                    <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                                                           value="<?php echo $result->contatoCpf;?>"/>
                                                </div>

                                                <div class="span2">
                                                    <label for="contatoSexo">Sexo</label>
                                                    <select name="contatoSexo" id="contatoSexo" class="span12 chzn" required="required">
                                                        <option value="M" <?php if ($result->contatoSexo == 'M') echo 'selected="selected"';?>>Masculino</option>
                                                        <option value="F" <?php if ($result->contatoSexo == 'F') echo 'selected="selected"';?>>Feminino</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span3">
                                                    <label for="contatoTelefone">Telefone</label>
                                                    <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                                                           value="<?php echo $result->contatoTelefone;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="contatoCelular">Celular</label>
                                                    <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                                                           value="<?php echo $result->contatoCelular;?>"/>
                                                </div>

                                                <div class="span4">
                                                    <label for="email">Email</label>
                                                    <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                                                           value="<?php echo $result->contatoEmail;?>"/>
                                                </div>

                                                <div class="span2">
                                                    <label for="email">Data de nascimento</label>
                                                    <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                                                           value="<?php echo $result->contatoDataNascimento;?>"/>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane" id="dadosAcesso">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="hostname">Hostname<span class="required">*</span></label>
                                                    <input type="text" name="hostname" required="required" class="span12" id="hostname"
                                                           value="<?php echo $result->hostname;?>"/>
                                                </div>

                                                <div class="span4">
                                                    <label for="username">Username<span class="required">*</span></label>
                                                    <input type="text" name="username" required="required" class="span12" id="username"
                                                           value="<?php echo $result->username;?>"/>
                                                </div>

                                                <div class="span4">
                                                    <label for="password">Password</label>
                                                    <input type="text" name="password"  class="span12" id="password"
                                                           value="<?php echo $result->password;?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="dadosFiscais">
                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span6">
                                                    <label for="natureza_juridica">Natureza Jurídica</label>
                                                    <input id="natureza_juridica" type="text" class="span12" name="natureza_juridica"
                                                           value="<?php echo $result->natureza_juridica;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="configuracao_tecnospeed_id">Grupo TecnoSpeed</label>
                                                    <select name="configuracao_tecnospeed_id" id="configuracao_tecnospeed_id" class="span12">
                                                        <option></option>
                                                        <?php foreach ($tecnospeeds as $tecnospeed) {?>
                                                            <option <?php if ($result->configuracao_tecnospeed_id == $tecnospeed->idConfiguracaotecnospeed) echo 'selected="selected"'?> value="<?php echo $tecnospeed->idConfiguracaotecnospeed;?>"><?php echo $tecnospeed->descricao;?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>


                                                <div class="span3">
                                                    <label for="contador_id">Contador</label>
                                                    <select name="contador_id" id="contador_id" class="span12">
                                                        <option></option>
                                                        <?php foreach ($contadores as $contador) {?>
                                                            <option <?php if ($result->contador_id == $contador->idContador) echo 'selected="selected"'?> value="<?php echo $contador->idContador;?>"><?php echo $contador->nome;?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span3">
                                                    <label for="IE">Insc. Est</label>
                                                    <input id="IE" type="text" class="span12" name="IE"
                                                           value="<?php echo $result->IE;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="IESUF">Insc. SUFRAMA</label>
                                                    <input id="IESUF" type="text" class="span12" name="IESUF"
                                                           value="<?php echo $result->IESUF;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="IEST">Insc. ST</label>
                                                    <input id="IEST" type="text" class="span12" name="IEST"
                                                           value="<?php echo $result->IEST;?>"/>
                                                </div>

                                                <div class="span3">
                                                    <label for="insmun">Insc. MN</label>
                                                    <input id="insmun" type="text" class="span12" name="insmun"
                                                           value="<?php echo $result->insmun;?>"/>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="ambiente">Ambiente</label>
                                                    <select name="ambiente" class="span12" id="ambiente">
                                                        <option value="1" <?php if ($result->ambiente == '1') echo 'selected="selected"';?>>Produção</option>
                                                        <option value="2" <?php if ($result->ambiente == '2') echo 'selected="selected"';?>>Homologação</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="certificado">Certificado</label>
                                                    <select name="certificado" class="span12" id="certificado">
                                                        <option value="A1"  <?php if ($result->certificado == 'A1') echo 'selected="selected"';?>>A1</option>
                                                        <option value="A3"  <?php if ($result->certificado == 'A3') echo 'selected="selected"';?>>A3</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="regimeTributario">CRT - Código de Regime Tributário da NF-e</label>
                                                    <select name="regimeTributario" class="span12" id="regimeTributario">
                                                        <option value="1" <?php if ($result->regimeTributario == '1') echo 'selected="selected"';?>>Simples Nacional</option>
                                                        <option value="2" <?php if ($result->regimeTributario == '2') echo 'selected="selected"';?>>Simples Nacional, exesso sublime de receita bruta.</option>
                                                        <option value="3" <?php if ($result->regimeTributario == '3') echo 'selected="selected"';?>>Regime Normal. (v2.0)</option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">

                                                <div class="span4">
                                                    <label for="cert">Certificado Digital A1</label>
                                                    <input type="file" name="cert" id="arquivocert" class="span12">
                                                </div>

                                                <div class="span4">
                                                    <label for="cert">Senha do Certificado</label>
                                                    <input type="text" name="certsenha" class="span12" value="<?php echo $result->certsenha;?>"/>
                                                </div>

                                                <div class="span4">
                                                    <?php
                                                    if ($result->cert != '') :
                                                        $base_path = FCPATH.'emissores/v4/';
                                                        $file = '' . $base_path . '/nfephp-master/certs/'.$result->documento.'/'.$result->cert.'';

                                                        $certs = array();
                                                        $pkcs12 = file_get_contents($file);
                                                        if (openssl_pkcs12_read($pkcs12, $certs, $result->certsenha)) :

                                                            $certificado = array();
                                                            $certificado = openssl_x509_parse(openssl_x509_read($certs['cert']));

                                                            echo '<br/>';
                                                            //print_r( $dados );
                                                            //Dados mais importantes
                                                            //                                            echo $dados['subject']['C'] . '<br>'; //País
                                                            //                                            echo $dados['subject']['ST'] . '<br>'; //Estado
                                                            //                                            echo $dados['subject']['L'] . '<br>'; //Município
                                                            echo '<b>Emitido Para </b><br/>' . $certificado['subject']['CN'] . '<br>'; //Razão Social e CNPJ / CPF
                                                            echo '<b>Validade </b><br/>' . date('d/m/Y', $certificado['validTo_time_t']) . '<br>'; //Validade
                                                            //                                            echo $dados['extensions']['subjectAltName'] . '<br>'; //Emails Cadastrados separado por ,
                                                        endif;
                                                    else :
                                                        echo '<h4> Sem Certificado. </h4>';
                                                    endif;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="dadosNFE">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span3">
                                                    <label for="cnae">CNAE</label>
                                                    <input id="cnae" type="text" class="span12" name="cnae"
                                                           value="<?php echo $result->cnae;?>"/>
                                                </div>
                                                <div class="span3">
                                                    <label for="csc">CSC (NFC-e)</label>
                                                    <input id="csc" type="text" class="span12" name="csc"
                                                           value="<?php echo $result->csc;?>"/>
                                                </div>
                                                <div class="span3">
                                                    <label for="idcsc">ID CSC</label>
                                                    <input id="idcsc" type="text" class="span12" name="idcsc"
                                                           value="<?php echo $result->idcsc;?>"/>
                                                </div>
                                                <div class="span3">
                                                    <label for="tokenibpt">Token IBPT</label>
                                                    <input id="tokenibpt" type="text" class="span12" name="tokenibpt"
                                                           value="<?php echo $result->tokenibpt;?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="dadosBoleto">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="marketplace_id">Marketplace_id</label>
                                                    <input id="marketplace_id" type="text" class="span12" name="marketplace_id"
                                                           value="<?php echo $result->marketplace_id;?>"/>
                                                </div>
                                                <div class="span4">
                                                    <label for="publishable_key">Publishable key</label>
                                                    <input id="publishable_key" type="text" class="span12" name="publishable_key"
                                                           value="<?php echo $result->publishable_key;?>"/>
                                                </div>
                                                <div class="span4">
                                                    <label for="on_behalf_of">On_behalf_of</label>
                                                        <input id="on_behalf_of" type="text" class="span12" name="on_behalf_of"
                                                               value="<?php echo $result->on_behalf_of;?>"/>
                                                </div>
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <label for="instrucaoBoletoOnline">Instruções boleto</label>
                                                    <textarea id="instrucaoBoletoOnline" class="span12" name="instrucaoBoletoOnline"><?php echo $result->instrucaoBoletoOnline;?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="dadosNFSE">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span6">
                                                    <label for="regimeEspecialTributacao">Regime especial de tributação</label>
                                                    <select name="regimeEspecialTributacao" class="span12" id="regimeEspecialTributacao">
                                                        <option value="0" <?php if ($result->regimeEspecialTributacao == '0') echo 'selected="selected"';?>>Nenhuma das opções</option>
                                                        <option value="1" <?php if ($result->regimeEspecialTributacao == '1') echo 'selected="selected"';?>>Microempresa munícipal</option>
                                                        <option value="2" <?php if ($result->regimeEspecialTributacao == '2') echo 'selected="selected"';?>>Estimativa</option>
                                                        <option value="3" <?php if ($result->regimeEspecialTributacao == '3') echo 'selected="selected"';?>>Sociedade de profissionais</option>
                                                        <option value="4" <?php if ($result->regimeEspecialTributacao == '4') echo 'selected="selected"';?>>Cooperativa</option>
                                                        <option value="5" <?php if ($result->regimeEspecialTributacao == '5') echo 'selected="selected"';?>>Microempresário individual (MEI)</option>
                                                        <option value="6" <?php if ($result->regimeEspecialTributacao == '6') echo 'selected="selected"';?>>Microempresário e Empresa de Pequeno Porte, (ME EPP)</option>
                                                        <option value="7" <?php if ($result->regimeEspecialTributacao == '7') echo 'selected="selected"';?>>Lucro Real</option>
                                                        <option value="8" <?php if ($result->regimeEspecialTributacao == '8') echo 'selected="selected"';?>>Lucro Presumido</option>
                                                    </select>
                                                </div>

                                                <div class="span6">
                                                    <label for="tipoTributacao">Tipo de tribução</label>
                                                    <select name="tipoTributacao" class="span12" id="tipoTributacao">
                                                        <option value="1" <?php if ($result->tipoTributacao == '1') echo 'selected="selected"';?>>Isenta de ISS</option>
                                                        <option value="2" <?php if ($result->tipoTributacao == '2') echo 'selected="selected"';?>>Imune</option>
                                                        <option value="3" <?php if ($result->tipoTributacao == '3') echo 'selected="selected"';?>>Não Incidência no Município</option>
                                                        <option value="4" <?php if ($result->tipoTributacao == '4') echo 'selected="selected"';?>>Não Tributável</option>
                                                        <option value="5" <?php if ($result->tipoTributacao == '5') echo 'selected="selected"';?>>Retida</option>
                                                        <option value="6" <?php if ($result->tipoTributacao == '6') echo 'selected="selected"';?>>Tributável dentro do município</option>
                                                        <option value="7" <?php if ($result->tipoTributacao == '7') echo 'selected="selected"';?>>Tributável fora do município</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span4">
                                                    <label for="incentivadorCultural">Incentivador Cultural</label>
                                                    <select name="incentivadorCultural" class="span12" id="incentivadorCultural">
                                                        <option value="1" <?php if ($result->incentivadorCultural == '1') echo 'selected="selected"';?>>Sim</option>
                                                        <option value="2" <?php if ($result->incentivadorCultural == '2') echo 'selected="selected"';?>>Não</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="incentivoFiscal">Incentivo Fiscal</label>
                                                    <select name="incentivoFiscal" class="span12" id="incentivoFiscal">
                                                        <option value="1" <?php if ($result->incentivoFiscal == '1') echo 'selected="selected"';?>>Sim</option>
                                                        <option value="2" <?php if ($result->incentivoFiscal == '2') echo 'selected="selected"';?>>Não</option>
                                                    </select>
                                                </div>

                                                <div class="span4">
                                                    <label for="naturezaTributacao">Natureza da tributacao</label>
                                                    <select name="naturezaTributacao" class="span12" id="naturezaTributacao">
                                                        <option value="1" <?php if ($result->naturezaTributacao == '1') echo 'selected="selected"';?>>Simples Nacional</option>
                                                        <option value="2" <?php if ($result->naturezaTributacao == '2') echo 'selected="selected"';?>>Fixo</option>
                                                        <option value="3" <?php if ($result->naturezaTributacao == '3') echo 'selected="selected"';?>>Depósito em Juízo</option>
                                                        <option value="4" <?php if ($result->naturezaTributacao == '4') echo 'selected="selected"';?>>Exigibilidade suspensa por decisão judicial</option>
                                                        <option value="5" <?php if ($result->naturezaTributacao == '5') echo 'selected="selected"';?>>Exigibilidade suspensa por rocedimento administrativo</option>
                                                        <option value="6" <?php if ($result->naturezaTributacao == '6') echo 'selected="selected"';?>>Isenção parcial.</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="dadosObservacao">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <div class="span12">
                                                    <textarea class="span12" id="observacao" name="observacao" cols="30" rows="5"><?php echo $result->observacao;?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span7 offset3" style="text-align: center">
                                        <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i>
                                            Alterar
                                        </button>

                                        <button type="button" id="criardatabase" class="btn btn-inverse"><i class="icon-ok icon-white"></i>
                                            Criar DataBase
                                        </button>

                                        <button type="button" id="criartabelas" class="btn btn-warning"><i class="icon-ok icon-white"></i>
                                            Criar Tabelas
                                        </button>

                                        <button type="button" id="acessarbase" class="btn btn-info"><i class="icon-arrow-left"></i>
                                            Acessar
                                        </button>

                                        <a href="<?php echo base_url() ?>index.php/empresa" id="" class="btn"><i
                                                    class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab2">
                        <div class="span12" id="divCadastrarEmpresa">
                            <form action="<?php echo base_url(); ?>index.php/empresa/adicionarFilial" id="formFilial" method="post" class="form-horizontal">

                                <div class="span12" style="padding: 1%">
                                    <div class="span10">

                                        <label for="nome" >Nome</label>
                                        <input id="nomeFilial"  class="span12" type="text" required="required" name="nome" value=""/>
                                        <input type="hidden" name="idEmpresa" value="<?php echo $result->idEmpresa;?>" />
                                        <input type="hidden" id="idFilial" name="idFilial" value=""/>
                                        <input type="hidden" id="idFilialCliente" name="idFilialCliente" value=""/>
                                        <input type="hidden" id="documento" name="documento" value="<?php echo $result->documento;?>"/>
                                    </div>

                                    <div class="span2">
                                        <label for="status">Status</label>
                                        <select name="status" id="statusFilial" class="span12 chzn" required="required">
                                            <option value="1">Ativa</option>
                                            <option value="2">Inativa</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">

                                    <div class="span3">
                                        <label for="cnpj">CNPJ</label>
                                        <input id="cnpj" type="number" class="span12" required="required" name="cnpj"
                                               value=""/>
                                    </div>

                                    <div class="span3">
                                        <label for="ie">I.E</label>
                                        <input id="ieFilial" type="number" class="span12" name="ie"
                                               value=""/>
                                    </div>

                                    <div class="span3">
                                        <label for="telefone">Telefone</label>
                                        <input id="telefoneFilial" type="text" class="span12" name="telefone"
                                               value=""/>
                                    </div>

                                    <div class="span3">
                                        <label for="email">E-mail</label>
                                        <input id="emailFilial" type="email" class="span12" name="email"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">

                                    <div class="span3">
                                        <label for="cep">CEP</label>
                                        <input id="cepFilial" type="text" name="cep" class="span12" onBlur="getConsultaCEPFilial();"
                                               value="<?php echo set_value('cep'); ?>"/>
                                        <small>[TAB] consulta cep (Necessita Internet)</small>
                                    </div>

                                    <div class="span6">
                                        <label for="rua">Rua</label>
                                        <input id="ruaFilial" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="numero">Número</label>
                                        <input id="numeroFilial" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span12">
                                        <label for="complemento">Complemento</label>
                                        <input id="complementoFilial" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span5">
                                        <label for="bairro">Bairro</label>
                                        <input id="bairroFilial" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                                    </div>

                                    <div class="span4">
                                        <label for="cidade">Cidade</label>
                                        <input id="cidadeFilial" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                                    </div>
                                    <div class="span3">
                                        <label for="estado">Estado</label>
                                        <input id="estadoFilial" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span6 offset3" style="text-align: center">
                                        <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i>
                                            Salvar
                                        </button>

                                        <a href="#" id="limpar" class="btn">Limpar</a>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <div class="span12" id="divProdutos" style="margin-left: 0;">
                            <table class="table table-bordered" id="tblFiliais">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>CNPJ</th>
                                    <th>Contato</th>
                                    <th>Endereço</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $contador = 0;
                                foreach ($filiais as $filial) {
                                    echo '<tr>';
                                    echo '<td>' . $filial->nome . '</td>';
                                    echo '<td>' . $filial->cnpj. '</td>';
                                    echo '<td>' . $filial->telefone.'<br/>'. $filial->email. '</td>';
                                    echo '<td style="text-align: left;">' . $filial->rua . ' nº ' . $filial->numero . ' <br/>' . $filial->bairro . ' - ' . $filial->cidade . '/' . $filial->estado  . '<br/> '.   $filial->cep.'</td>';
                                    if ($filial->status == 1) {
                                        echo '<td style="text-align: center;">Ativo</td>';
                                    } else {
                                        echo '<td style="text-align: center;">Inativo</td>';
                                    }
                                    if ($contador == 0) {
                                        echo '<td style="width: 5%;">-</td>';
                                    } else {
                                        echo '<td style="width: 5%;"><a href="" idFilial="' . $filial->idFilial . '"class="btn btn-top classfiliais"><i class="icon-ok icon-white"></i></a></td>';
                                    }
                                    echo '</tr>';

                                    $contador = $contador + 1;
                                } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/empresa.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        <?php if ($result->tipoPessoa == 'PJ') { ?>
            $('#div_contato').show();
            $('#div_sexo').hide();
            $('#div_rg').hide();
        <?php } else { ?>
            $('#div_contato').hide();
            $('#div_sexo').show();
            $('#div_rg').show();
        <?php } ?>

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });

        $('#formEmpresa').validate({
            rules: {
                nomeEmpresa: {required: true}
            },
            messages: {
                nomeEmpresa: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $('#criardatabase').click(function (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/empresa/criarBanco/<?php echo $result->idEmpresa;?>",
                dataType: 'html',
                success: function (data) {
                    if (data.indexOf('PDOException') !== -1) {
                        alert('Data base já existe ou houve um erro de conexão. Verifique o erro abaixo!\n\n\n\n'+data);
                    } else {
                        if (data.indexOf('getaddrinfo') !== -1) {
                            alert(data);
                        } else {
                            alert('Data base criada com sucesso!');
                        }
                    }
                 }
            });
        });

        $('#criartabelas').click(function (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/empresa/criarTabelas/<?php echo $result->idEmpresa;?>",
                dataType: 'html',
                async: false,
                cache: false,
                success: function (data) {
                    if (data.indexOf('PDOException') !== -1) {
                        alert('Tabelas já existe ou houve um erro de conexão. Verifique o erro abaixo!\n\n\n\n'+data);
                    } else {
                        if (data.indexOf('getaddrinfo') !== -1) {
                            alert(data);
                        } else {
                            alert('Tabelas criada com sucesso!');
                        }
                     }
                }
            });
        });

        $('.classfiliais').click(function (e) {

            var idFilial = $(this).attr('idFilial');
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/empresa/getFilialByIDJSON/"+idFilial,
                dataType: 'json',
                async: false,
                cache: false,
                success: function (data) {

                    var filial = data.filial;

                    $('#nomeFilial').val(filial.nome);
                    $('#cnpj').val(filial.cnpj);
                    $('#bairroFilial').val(filial.bairro);
                    $('#cepFilial').val(filial.cep);
                    $('#cidadeFilial').val(filial.cidade);
                    $('#complementoFilial').val(filial.complemento);
                    $('#emailFilial').val(filial.email);
                    $('#estadoFilial').val(filial.estado);
                    $('#ieFilial').val(filial.ie);
                    $('#numeroFilial').val(filial.numero);
                    $('#ruaFilial').val(filial.rua);
                    $('#statusFilial').val(filial.status);
                    $('#telefoneFilial').val(filial.telefone);
                    $('#idFilial').val(filial.idFilial);
                    $('#idFilialCliente').val(filial.idFilialCliente);
                }
            });
        });

        $('#limpar').click(function (e) {

            e.preventDefault();
            $('#nomeFilial').val('');
            $('#cnpj').val('');
            $('#bairroFilial').val('');
            $('#cepFilial').val('');
            $('#cidadeFilial').val('');
            $('#complementoFilial').val('');
            $('#emailFilial').val('');
            $('#estadoFilial').val('');
            $('#ieFilial').val('');
            $('#numeroFilial').val('');
            $('#ruaFilial').val('');
            $('#statusFilial').val('');
            $('#telefoneFilial').val('');
            $('#idFilial').val('');
            $('#idFilialCliente').val('');
        });

        $('#acessarbase').click(function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/empresa/sair/",
                dataType: 'html',
                async: false,
                cache: false,
                success: function (data) {
                    window.open("<?php echo base_url();?>index.php/mapos/login/<?php echo $result->documento; ?>", "_self");
                }
            });
        });
    });
</script>

