 <a href="<?php echo base_url(); ?>index.php/empresa/adicionar" class="btn btn-success"><i
        class="icon-plus icon-white"></i> Adicionar Oficina Matriz</a>


<?php
if (!$results){
?>

<div class="widget-box">

    <div class="col-xs-6 text-left">

        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Empresas</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th>Nome</th>
                    <th>CPF/CNPJ</th>
                    <th>Telefone</th>
                    <th>Endereço</th>
                    <th style="text-align: left;">Status</th>
                    <th> Ações </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhum Oficina Matriz Cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php }else{?>
<div class="widget-box">
    <div class="widget-title">
    <span class="icon">
        <i class="icon-user"></i>
     </span>
        <h5>Oficinas</h5>
    </div>

    <div class="widget-content nopadding">
        <div class="col-xs-6 text-left">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th style="width: 5%;text-align: center;">#</th>
                    <th style="text-align: left;">Nome</th>
                    <th style="text-align: left;">CPF/CNPJ</th>
                    <th style="text-align: left;">Telefone</th>
                    <th style="text-align: left;">Endereço</th>
                    <th style="text-align: left;">Cadastro</th>
                    <th style="text-align: left;">Status</th>
                    <th style="width: 15%;"> Ações </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    echo '<tr>';
                    echo '<td style="width: 5%;text-align: center;">' . str_pad($r->idEmpresa, 4, '0', STR_PAD_LEFT) . '</td>';
                    echo '<td style="text-align: left;">' . $r->nomeEmpresa . '</td>';
                    echo '<td style="text-align: left;">' . $r->documento . '</td>';
                    echo '<td style="text-align: left;">' . $r->telefone . '</td>';
                    echo '<td style="text-align: left;">' . $r->rua . ' nº ' . $r->numero . ' - ' . $r->bairro . ' - ' . $r->cidade . '/' . $r->estado . ' - ' . $r->cep . '</td>';
                    echo '<td style="text-align: center;">' .  date(('d/m/Y'), strtotime($r->dataCadastro ))  . '</td>';

                    if ($r->status == 1) {
                        echo '<td style="text-align: center;">Ativo</td>';
                    } else {
                        echo '<td style="text-align: center;">Inativo</td>';
                    }

                    echo '<td>';
                    echo '<a href="' . base_url() . 'index.php/empresa/visualizar/' . $r->idEmpresa . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                    echo '<a href="' . base_url() . 'index.php/empresa/editar/' . $r->idEmpresa . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    if ($r->status == 1) {
                        echo '<a href="#modal-inativar" id="inativar_empresa" role="button" data-toggle="modal" empresa="' . $r->idEmpresa . '" style="margin-right: 1%" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>';
                    } else {
                        echo '<a href="#modal-ativar" id="ativar_empresa" role="button" data-toggle="modal" empresa="' . $r->idEmpresa . '" style="margin-right: 1%" class="btn btn tip-top"><i class="icon-ok icon-white"></i></a>';
                    }

                    echo '</td>';


                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    } ?>

<!-- Modal -->
<div id="modal-inativar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/empresa/inativar" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Inativar Empresa</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idEmpresa" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente inativar esta oficiona?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Inativar</button>
        </div>
    </form>
</div>

<!-- Modal -->
<div id="modal-ativar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/empresa/ativar" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Ativar Empresa</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idEmpresaAtivar" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente ativar esta oficiona?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Ativar</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var empresa = $(this).attr('empresa');
            $('#idEmpresa').val(empresa);
            $('#idEmpresaAtivar').val(empresa);
        });
    });
</script>

