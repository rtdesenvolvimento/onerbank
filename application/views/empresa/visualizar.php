<div class="widget-box">
    <div class="widget-title">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab1">Dados da Empresa Matriz</a></li>

            <div class="buttons">
                <?php
                    echo '<a class="btn btn-mini btn-info" href="'.base_url().'index.php/empresa/editar/'.$result->idEmpresa.'"><i class="icon-pencil icon-white"></i> Editar</a>';
                 ?>
            </div>
        </ul>
    </div>
    <div class="widget-content tab-content">
        <div id="tab1" class="tab-pane active" style="min-height: 300px">
            <div class="accordion" id="collapse-group">
                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span>
                                <?php if ($result->tipoPessoa== 'PF') {?>
                                    <h5>Dados Pessoais</h5>
                                <?php } else { ?>
                                    <h5>Dados da Oficina Matriz</h5>
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                    <div class="collapse in accordion-body" id="collapseGOne">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>


                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Status</strong></td>
                                    <td>
                                        <?php if ($result->status == 1) {?>
                                            Ativo
                                        <?php } else { ?>
                                            Inativo
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Código</strong></td>
                                    <td>
                                        <?php echo str_pad($result->idEmpresa, 4, '0', STR_PAD_LEFT); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Tipo</strong></td>
                                    <td>
                                        <?php if ($result->tipoPessoa== 'PJ') {?>
                                            Pessoa Jurídica
                                        <?php } else { ?>
                                            Pessoa Fisíca
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php if ($result->tipoPessoa== 'PF') {?>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Sexo</strong></td>
                                        <td>
                                            <?php if ($result->sexo== 'M') {?>
                                                Masculino
                                            <?php } else { ?>
                                                Feminino
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Nome / Razão social</strong></td>
                                    <td><?php echo $result->nomeEmpresa; ?></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Nome Fantasia / Apelido</strong></td>
                                    <td><?php echo $result->nomeFantasiaApelido ?></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right"><strong>CPF/CNPJ</strong></td>
                                    <td>
                                        <?php if ($result->tipoPessoa== 'PJ') {?>
                                            <?php echo $result->documento ?>
                                        <?php } else {

                                            $dataOrgaoEmissor = '';
                                            if ($result->dataOrgaoEmissor){
                                                $dataOrgaoEmissor = date('d/m/Y',  strtotime($result->dataOrgaoEmissor));
                                            }
                                            ?>
                                            <?php echo $result->documento.' Org.Emissor '.$result->orgaoEmissor.'/'.$result->estadoOrgaoEmissor.' Data emissão '.$dataOrgaoEmissor?>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Data de Cadastro</strong></td>
                                    <td><?php echo date('d/m/Y',  strtotime($result->dataCadastro)) ?></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right"><strong>Observação</strong></td>
                                    <td><?php echo $result->observacao; ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#dadosacessobd" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span><h5>Dados de acesso ao BD</h5>
                            </a>
                        </div>
                    </div>
                    <div class="collapse accordion-body" id="dadosacessobd">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>hostname</strong></td>
                                    <td><?php echo $result->hostname ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Username</strong></td>
                                    <td><?php echo $result->username ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Password</strong></td>
                                    <td><?php echo $result->password ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span><h5>Contatos</h5>
                            </a>
                        </div>
                    </div>
                    <div class="collapse accordion-body" id="collapseGTwo">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Telefone</strong></td>
                                    <td><?php echo $result->telefone ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Celular</strong></td>
                                    <td><?php echo $result->celular ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Email</strong></td>
                                    <td><?php echo $result->email ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <?php if ($result->tipoPessoa== 'PJ') {?>
                    <div class="accordion-group widget-box">
                        <div class="accordion-heading">
                            <div class="widget-title">
                                <a data-parent="#collapse-group" href="#pessoaContato" data-toggle="collapse">
                                    <span class="icon"><i class="icon-list"></i></span><h5>Pessoa para Contato</h5>
                                </a>
                            </div>
                        </div>
                        <div class="collapse accordion-body" id="pessoaContato">
                            <div class="widget-content">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Nome</strong></td>
                                        <td><?php echo $result->contatoNomeEmpresa; ?></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Sexo</strong></td>
                                        <td>
                                            <?php if ($result->contatoSexo== 'M') {?>
                                                Masculino
                                            <?php } else { ?>
                                                Feminino
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <?php if ($result->contatoDataNascimento) {?>
                                        <tr>
                                            <td style="text-align: right"><strong>Data de nascimento</strong></td>
                                            <td><?php echo date('d/m/Y',  strtotime($result->contatoDataNascimento)) ?></td>
                                        </tr>
                                    <?php } ?>

                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>CPF</strong></td>
                                        <td><?php echo $result->contatoCpf ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Telefone</strong></td>
                                        <td><?php echo $result->contatoTelefone ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Celular</strong></td>
                                        <td><?php echo $result->contatoCelular ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Email</strong></td>
                                        <td><?php echo $result->contatoEmail ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="accordion-group widget-box">
                    <div class="accordion-heading">
                        <div class="widget-title">
                            <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse">
                                <span class="icon"><i class="icon-list"></i></span><h5>Endereço</h5>
                            </a>
                        </div>
                    </div>
                    <div class="collapse accordion-body" id="collapseGThree">
                        <div class="widget-content">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="text-align: right; width: 30%"><strong>Rua</strong></td>
                                    <td><?php echo $result->rua ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Número</strong></td>
                                    <td><?php echo $result->numero ?></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right"><strong>Complemento</strong></td>
                                    <td><?php echo $result->complemento ?></td>
                                </tr>

                                <tr>
                                    <td style="text-align: right"><strong>Bairro</strong></td>
                                    <td><?php echo $result->bairro ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>Cidade</strong></td>
                                    <td><?php echo $result->cidade ?> - <?php echo $result->estado ?></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><strong>CEP</strong></td>
                                    <td><?php echo $result->cep ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>