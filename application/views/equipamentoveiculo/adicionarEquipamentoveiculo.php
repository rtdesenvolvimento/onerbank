<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Equipamento automóvel</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formEquipamentoveiculo" method="post" class="form-horizontal" >

                    <div class="span12" id="divavarias" style="margin-left: 0">
                        <div class="span12" style="margin-left: 0">
                            <iframe height="400" style="margin: 0;display: none;" id="iframeimagem" frameborder="0" scrolling="yes" src="<?php echo base_url(); ?>tipoveiculo/iframePontosAvariaTipoveiculo" width="100%"></iframe>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Tipo veículo<span class="required">*</span></label>
                        <div class="controls">
                            <select name="tipoVeiculo_id" id="tipoVeiculo_id" required="required">
                                <option value=""></option>
                                <?php foreach ($tiposveiculo as $tipoveiculo) {?>
                                    <option value="<?php echo $tipoveiculo->idTipoveiculo; ?>"><?php echo $tipoveiculo->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Visão automóvel<span class="required">*</span></label>
                        <div class="controls">
                            <select name="idVisaoautomovel" id="idVisaoautomovel" required="required">
                                <option value=""></option>
                                <?php foreach ($visualautomovel as $visao) {?>
                                    <option value="<?php echo $visao->idVisaoautomovel; ?>"><?php echo $visao->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" required="required" value="<?php echo set_value('nome'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição</label>
                        <div class="controls">
                            <textarea id="descricao" name="descricao"><?php echo set_value('descricao'); ?></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">X<span class="required">*</span></label>
                        <div class="controls">
                            <input id="client_x"  readonly type="number" name="client_x" required="required" value="<?php echo set_value('client_x'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Y<span class="required">*</span></label>
                        <div class="controls">
                            <input id="client_y"  readonly type="number" name="client_y" required="required" value="<?php echo set_value('client_y'); ?>"  />
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <button type="button" id="limpar" class="btn btn-success"><i class="icon-remove icon-white"></i> Limpar</button>
                                <a href="<?php echo base_url() ?>index.php/equipamentoveiculo" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#tipoVeiculo_id').change(function (e) {
            var idTipoveiculo = $(this).val();

            if (idTipoveiculo !== '') {
                var url = '<?php echo base_url(); ?>tipoveiculo/iframePontosAvariaTipoveiculo/' + idTipoveiculo;
                $('#iframeimagem').attr('src', url);
                $('#iframeimagem').show();
            } else {
                $('#iframeimagem').hide();
                $('#client_x').val('');
                $('#client_y').val('');
            }
        });

        $('#limpar').click(function (e) {
            var idTipoveiculo = $('#tipoVeiculo_id').val();
            var url = '<?php echo base_url(); ?>tipoveiculo/iframePontosAvariaTipoveiculo/'+idTipoveiculo;
            $('#iframeimagem').attr('src',url);

            $('#client_x').val('');
            $('#client_y').val('');
        });

        $('#formEquipamentoveiculo').validate({
            rules :{
                nome:{ required: true}
            },
            messages:{
                nome :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });

    function atribuirXY(x,y) {
        $('#client_x').val(x);
        $('#client_y').val(y);
    }

</script>