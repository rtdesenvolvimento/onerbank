<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                      <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Estágio</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formEstagioos" method="post" class="form-horizontal">
                    <?php echo form_hidden('idEstagioos', $result->idEstagioos) ?>
                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value="<?php echo $result->nome ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="alias" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="alias" type="text" name="alias" value="<?php echo $result->alias ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="status" class="control-label">Status<span class="required">*</span></label>
                        <div class="controls">
                            <select name="status" name="status" class="form-control">
                                <option value="1" <?php if($result->status == 1){echo 'selected';}?>>sim</option>
                                <option value="2" <?php if($result->status == 2){echo 'selected';}?>>não</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição</label>
                        <div class="controls">
                            <input id="descricao" type="text" name="descricao"
                                   value="<?php echo $result->descricao ?>"/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/estagioos" id="btnAdicionar" class="btn"><i
                                        class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formEstagioos').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>