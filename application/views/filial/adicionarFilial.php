<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Filial</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formFilial" method="post" class="form-horizontal" >
                    <div class="control-group">
                        <label for="nome" class="control-label">Empresa<span class="required">*</span></label>
                        <div class="controls">
                            <select name="idEmpresa" id="idEmpresa" required="required">
                                <option value=""></option>
                                <?php foreach ($empresas as $empresa) {?>
                                    <option value="<?php echo $empresa->idEmpresa; ?>"><?php echo $empresa->nomeEmpresa; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" required="required" type="text" name="nome" value=""  />
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/filial" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>