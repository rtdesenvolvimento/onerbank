<a href="<?php echo base_url() ?>index.php/filial/adicionar" class="btn btn-success">
    <i class="icon-plus icon-white"></i> Adicionar Filial
</a>
<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Filiais</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Empresa</th>
                    <th>Nome</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhuma Filial cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Filiais</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Empresa</th>
                    <th>Nome</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {

                    $empresa = $this->empresa_model->getById($r->idEmpresa);

                    echo '<tr>';
                    echo '    <td>' . $r->idFilial . '</td>';
                    echo '    <td>' . $empresa->nomeEmpresa . '</td>';
                    echo '    <td>' . $r->nome . '</td>';
                    echo '    <td>';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/filial/editar/' . $r->idFilial . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '      <a href="#modal-excluir" role="button" data-toggle="modal" filial="' . $r->idFilial . '" class="btn btn-danger tip-top" ><i class="icon-remove icon-white"></i></a>  ';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/filial/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Filial</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idFilial" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir esta filial?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var filial = $(this).attr('filial');
            $('#idFilial').val(filial);
        });
    });
</script>