<?php

$situacao = $this->input->get('situacao');
$periodo = $this->input->get('periodo');

$dataInicial = $this->input->get('dataInicial');
$dataFinal = $this->input->get('dataFinal');

$dataPagamentoInicial = $this->input->get('dataPagamentoInicial');
$dataPagamentoFinal = $this->input->get('dataPagamentoFinal');

$previsaoPagamentoInicial = $this->input->get('previsaoPagamentoInicial');
$previsaoPagamentoFinal = $this->input->get('previsaoPagamentoFinal');

$tipolancamento = $this->input->get('tipolancamento');

$termo = $this->input->get('termo');

if (!$periodo) $periodo = 'mes';
if (!$tipolancamento) $tipolancamento = 'todos';
if (!$situacao)  $situacao = 'FATURADA_BOLETO';

?>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-search"></i> Filtros</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-success" style="color: #ffffff"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Adicionar Novo Lançamento</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#modalReceita" data-toggle="modal" role="button" class="dropdown-item">Receita</a></li>
                            <li><a href="#modalDespesa" data-toggle="modal" role="button" class="dropdown-item">Despesa</a></li>
                            <li><a id="cancelar-parcelas-selecao" class="dropdown-item">Cancelar</a></li>
                            <li style="display: none;"><a id="gerarBoleto" class="dropdown-item">Gerar boletos</a></li>
                        </ul>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="get" id="frmLancamentos">
                    <div class="col-md-12 well">
                        <div class="col-md-12">
                            <input type="text" class="form-control" value="<?php if ($termo) echo $termo; ?>" id="termo" name="termo" placeholder="Digite os termos da pesquisa para buscar clientes ou fornecedores...">
                        </div>
                        <div class="col-md-5">
                            <label>Tipo</label>
                            <select name="tipolancamento" class="form-control">
                                <option value="todos">Todos</option>
                                <option value="receita" <?php if ($tipolancamento == 'receita') {
                                    echo 'selected';
                                } ?>>Receita
                                </option>
                                <option value="despesa" <?php if ($tipolancamento == 'despesa') {
                                    echo 'selected';
                                } ?>>Despesa
                                </option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Período</label>
                            <select name="periodo" id="periodo" class="form-control">
                                <option value="dia">Dia</option>
                                <option value="semana" <?php if ($periodo == 'semana') {
                                    echo 'selected';
                                } ?>>Semana
                                </option>
                                <option value="mes" <?php if ($periodo == 'mes') {
                                    echo 'selected';
                                } ?>>Mês
                                </option>
                                <option value="ano" <?php if ($periodo == 'ano') {
                                    echo 'selected';
                                } ?>>Ano
                                </option>
                                <option value="filtros_por_data" <?php if ($periodo == 'filtros_por_data') {
                                    echo 'selected';
                                } ?>>Filtrar Por Datas
                                </option>
                                <option value="todos" <?php if ($periodo == 'todos') {
                                    echo 'selected';
                                } ?>>Todos
                                </option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Status</label>
                            <select name="situacao" class="form-control">
                                <option value="todos">Todos</option>
                                <option style="display: none;" value="FATURADA" <?php if ($situacao == 'FATURADA') {
                                    echo 'selected';
                                } ?>>Abertas
                                </option>
                                <option value="FATURADA_BOLETO" <?php if ($situacao == 'FATURADA_BOLETO') {
                                    echo 'selected';
                                } ?>>Abertas
                                </option>
                                <option value="PAGO" <?php if ($situacao == 'PAGO') {
                                    echo 'selected';
                                } ?>>Pagas
                                </option>
                                <option value="ATRASADA" <?php if ($situacao == 'ATRASADA') {
                                    echo 'selected';
                                } ?>>Vencidas
                                </option>
                                <option value="cancelada" <?php if ($situacao == 'cancelada') {
                                    echo 'selected';
                                } ?>>Canceladas
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="span8" id="filtrarPorData" style="display:none">
                        <div class="col-md-12 well">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label for="">Vencimento de:</label>
                                    <input type="date" name="dataInicial" class="form-control" value="<?php echo $dataInicial ?>"/>
                                </div>
                                <div class="col-md-6">
                                    <label for="">até:</label>
                                    <input type="date" name="dataFinal" class="form-control" value="<?php echo $dataFinal ?>"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label for="">Pagamento de:</label>
                                    <input type="date" name="dataPagamentoInicial" class="form-control" value="<?php echo $dataPagamentoInicial ?>"/>
                                </div>
                                <div class="col-md-6">
                                    <label for="">até:</label>
                                    <input type="date" name="dataPagamentoFinal" class="form-control" value="<?php echo $dataPagamentoFinal ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span1" style="text-align: right;">
                        <button type="button" id="btnsubmitform" class="span12 btn btn-primary"><i class="fa fa-search"></i> Pesquisar</button>
                        <button type="button" id="btnDonwloadPDF" class="span12 btn btn-secondary"><i class="fa fa-download"></i> PDF</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive" style="overflow-x: hidden;">
                            <table id="datatable-responsive" style="overflow-x: hidden;" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="text-align: center;width: 3%"> <input type="checkbox" onclick="checarTodos();" id="checkbox-all"></th>
                                    <th style="text-align: center;">#</th>
                                    <th style="text-align: left;">Pessoa</th>
                                    <th style="text-align: center;"><i class="fa fa-usd"></i></th>
                                    <th style="text-align: center;">Venc.</th>
                                    <th style="text-align: center;">Valor</th>
                                    <th style="text-align: right;">Acres.</th>
                                    <th style="text-align: right;">Desc.</th>
                                    <th style="text-align: right;">Pagar</th>
                                    <th style="text-align: right;">Pago</th>
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: right;">NF</th>
                                    <th style="text-align: right;">PDV</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $totalReceita = 0;
                                $totalPago = 0;
                                $totalDespesa = 0;
                                $totalCusto   = 0;

                                $totalComNotaFiscal = 0;

                                $saldo  = 0;
                                $os     = null;

                                foreach ($results as $r) {

                                    $boleto = $this->financeiro_model->getBoletoByParcela($r->idParcela);
                                    $lancamentoParcela = $this->financeiro_model->getById($r->idLancamentos);
                                    $venda = $this->vendas_model->getById($lancamentoParcela->venda);
                                    $valorNF = 0;
                                    $possuiNota = false;
                                    $possuiVenda = false;
                                    $nNF = null;

                                    if (count($venda) > 0) {
                                        $nNF = $venda->nNF;
                                        $nfce = $this->nfce_model->getById($nNF);
                                        $possuiVenda = true;
                                        if (count($nfce) > 0) {
                                            $valorNF = $nfce->vNF;
                                            $possuiNota = true;
                                            $totalComNotaFiscal = $totalComNotaFiscal + $valorNF;
                                        }
                                    }

                                    $vencimento = date(('d/m/Y'), strtotime($r->dtVencimento));
                                    $data_pagamento = '-';
                                    $previsaoPagamentoEditar = null;
                                    $os = $r->os;
                                    $totalCusto += $r->custo;
                                    $plano = '';
                                    $isBoleto = TRUE;
                                    $isBoletoDisabled = '';
                                    $labelPagamento = 'info';

                                    if ($r->tipo == 'receita') $receita = $r->receita;
                                    else  $receita = $r->despesa;

                                    if ($r->linkpagamento_id != null) $receita = $r->linkpagamento;

                                    $status = '';

                                    if ($r->status == 'FATURADA') {
                                        if (strtotime($r->dtVencimento) < strtotime(date('Y-m-d'))) {
                                            if ($situacao == 'FATURADA_BOLETO') continue;
                                            else  $status = 'Vencido';
                                        } else {
                                            if (count($boleto) > 0) $status = 'Aberta';
                                            else $status = 'Aberta';
                                        }
                                    } else if ($r->status == 'PAGO') {
                                        $status = 'Pago';
                                    } else if ($r->status == 'ABERTO') {
                                        $status = 'Aberta';
                                    } else if ($r-> status == 'cancelada'){
                                        $status = 'Cancelada';
                                        $isBoletoDisabled = 'disabled="disabled"';
                                    }

                                    if ($r->status == 'PAGO') {
                                        $data_pagamento = date(('d/m/Y'), strtotime($r->dtUltimoPagamento)) . '</small>';
                                        $status = 'Pago';
                                        $labelPagamento = 'success';
                                    } else if ($r->dtUltimoPagamento) $data_pagamento = date(('d/m/Y'), strtotime($r->dtUltimoPagamento)) . '</small>';

                                    if ($r->tipo == 'receita') {
                                        $label = 'success';

                                        if ($r->status != 'cancelada') {
                                            $totalReceita += $r->valorVencimento;
                                            $totalPago += $r->valorPago - $r->taxas;
                                        }

                                        $plano = $r->receita;
                                        $isBoleto = TRUE;
                                    } else {
                                        $label = 'important';

                                        if ($r->status != 'cancelada') {
                                            $totalDespesa += $r->valorVencimento;
                                            $totalPago += $r->valorPago - $r->taxas;
                                        }

                                        $plano = $r->despesa;
                                        $isBoleto = FALSE;
                                    }

                                    $nomeClienteFornecedor = '';

                                    if ($r->fornecedor_id) $nomeClienteFornecedor = $r->nomeFornecedor;
                                    if ($r->clientes_id) $nomeClienteFornecedor = $r->nomeCliente;

                                    $colorTaxa = '';
                                    if ($r->taxa == 1) $colorTaxa = 'color: red;';

                                    echo '<tr>';
                                    echo '<td style="text-align: center;width: 3%"><input class="elementSelected" ' . $isBoletoDisabled . ' type="checkbox" situacao="' . $r->status . '" value="' . $r->idParcela . '"></td>';

                                    if ($r->tipo == 'despesa')   echo '<td style="text-align: center;" title="Conta pagar"><i class="fa fa-upload" style="font-size: 14px;color: #BA2121;"> </td>';
                                    else echo '<td style="text-align: center;" title="Conta receber"><i class="fa fa-money" style="font-size: 14px;color: #0f7864;"></td>';

                                    if ($r->taxa == 1) echo '<td style="text-align: left;"><a href="'.base_url().'clientes/profile/'.$r->clientes_id.'">' . $nomeClienteFornecedor . '<br/><small>(Taxa da cobrança)</small></a></td>';
                                    else echo '<td style="text-align: left;"><a href="'.base_url().'clientes/profile/'.$r->clientes_id.'">' . $nomeClienteFornecedor . '<br/><small>('.ucfirst($receita) .')</small></a></td>';

                                    if ($r->tipocobrancatipo === 'boleto') echo '<td style="text-align: center;" title="Boleto"><i class="fa fa-barcode" style="font-size: 14px;color: #000000;"></td>';
                                    else if ($r->tipocobrancatipo === 'cartao') echo '<td style="text-align: center;" title="Cartão"><i class="fa fa-credit-card" style="font-size: 14px;color: #00008B;"></td>';
                                    else echo '<td style="text-align: center;" title="Pagamento em mãos"><i class="fa fa-inbox" style="font-size: 14px;color: #00008B;"></td>';

                                    echo '<td style="text-align: center;'.$colorTaxa.'">' . $vencimento . '</td>';

                                    echo '<td style="text-align: right;"> R$ ' . number_format($r->valorVencimento - $r->acrescimo +  $r->desconto, 2, ',', '.') . '<br/><small>'.$r->numeroParcela . '/' . $r->totalParcelas .'</small></td>';

                                    echo '<td style="text-align: right;'.$colorTaxa.'">R$ ' . number_format($r->acrescimo, 2, ',', '.') . '</td>';

                                    echo '<td style="text-align: right;'.$colorTaxa.'">R$ ' . number_format($r->desconto, 2, ',', '.') . '</td>';

                                    if ($r->taxa == 1) echo '<td style="text-align: center;'.$colorTaxa.'">-</td>';
                                    else echo '<td style="text-align: right;"> R$ ' . number_format($r->valorPagar, 2, ',', '.') .'</td>';

                                    if ($r->taxa == 1) echo '<td style="text-align: right;'.$colorTaxa.'"> - R$ ' . number_format($r->valorPago, 2, ',', '.') .'<br/><small>'.$data_pagamento. '</small></td>';
                                    else echo '<td style="text-align: right;'.$colorTaxa.'">R$ ' . number_format($r->valorPago, 2, ',', '.') .'<br/><small>'.$data_pagamento.  '</small></td>';

                                    echo '<td style="text-align: center;'.$colorTaxa.'">' . ucfirst($status) . '</td>';

                                    if ($possuiNota) echo '<td style="text-align: right;"> <a href="'.base_url().'nfce/editar/'.$nNF.'" target="_blank"> R$'.number_format($valorNF,2,',','.').'<br/><small>'.$nNF.'</small>'.'</a></td>';
                                    else  echo '<td style="text-align: right;"> - </td>';

                                    if ($possuiVenda) echo '<td style="text-align: right;"> <a href="'.base_url().'pdv/impressao/'.$venda->idVendas.'" target="_blank"> PDV <br/><small>'.$lancamentoParcela->venda.'</small></a></td>';
                                    else echo '<td style="text-align: right;">-</td>';

                                    echo '<td>';
                                    if ($r->status == 'PAGO' || $r->status == 'cancelada') {
                                        echo '<a class="estornar" href="#modalEstornarPagamento" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '"> <i class="fa fa-check-circle-o" style="font-size: 14px;color: #4AB858;"></i></a>';
                                    } else {

                                        if ($r->tipo == 'despesa') {
                                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eLancamento')) {
                                                echo '<a class="pagar" href="#modalPagarParcela" title="Baixar parcela"  style="margin-right: 1%" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '" valorVencimento="' . $r->valorVencimento . '" valorPago="' . $r->valorPago . '" dtVencimento="' . $r->dtVencimento . '"><img src="' . base_url() . 'assets/img/baixar-parcela.png"></a>';
                                                if ($r->tipo == 'receita') echo '<a class="gerarLinkPagamento" href="#modalLinkPagamento" title="Gerar Link pagamento"  style="margin-right: 1%" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '" valorVencimento="' . $r->valorVencimento . '" valorPago="' . $r->valorPago . '" dtVencimento="' . $r->dtVencimento . '"><img src="' . base_url() . 'assets/img/link-pagamento.png"></a>';
                                                echo '<a class="editar" href="#modalEditarParcela" title="Editar dados da parcela" style="margin-right: 1%" data-toggle="modal" role="button" idLancamento="' . $r->idParcela . '" valor="' . $r->valorPagar . '" status="' . $r->status . '" custo="' . $r->custo . '" previsaoPagamento="' . $r->previsaoPagamento . '" vencimento="' . $r->dtVencimento . '" pagamento="' . $r->data_pagamento . '" baixado="' . $r->baixado . '" fornecedor_id="' . $r->fornecedor_id . '" clientes_id="' . $r->clientes_id . '" cliente="' . $nomeClienteFornecedor . '" formaPgto="' . $r->forma_pgto . '" tipo="' . $r->tipo . '" os="' . $r->os . '" venda="' . $r->venda . '"><img src="' . base_url() . 'assets/img/botEditGrid.png"></a>';
                                                echo '<a class="estornar" href="#modalEstornarPagamento" title="Ver pagamentos" style="margin-right: 1%;cursor: pointer;" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '"> <i class="fa fa-check-circle-o" style="font-size: 14px;color: #4AB858;"></i></a>';
                                                echo '<a class="excluirRegistro" title="Cancelar parcela" idParcela="' . $r->idParcela . '" style="margin-right: 1%;cursor: pointer;"><img src="' . base_url() . 'assets/img/botExcluirGrid.png"></a>';
                                            }
                                        } else {
                                            if (count($boleto) > 0) {
                                                echo '<a class="pagar" href="#modalPagarParcela" title="Baixar parcela" style="margin-right: 1%" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '"  valorVencimento="' . $r->valorVencimento . '" valorPago="' . $r->valorPago . '" dtVencimento="' . $r->dtVencimento . '"><img src="' . base_url() . 'assets/img/baixar-parcela.png"></a>';
                                                echo '<a class="editar" href="#modalEditarParcela" title="Gerar 2ª via do boleto" style="margin-right: 1%" data-toggle="modal" role="button" idLancamento="' . $r->idParcela . '" valor="' . $r->valorPagar . '" status="' . $r->status . '" custo="' . $r->custo . '" previsaoPagamento="' . $r->previsaoPagamento . '" vencimento="' . $r->dtVencimento . '" pagamento="' . $r->data_pagamento . '" baixado="' . $r->baixado . '" fornecedor_id="' . $r->fornecedor_id . '" clientes_id="' . $r->clientes_id . '" cliente="' . $nomeClienteFornecedor . '" formaPgto="' . $r->forma_pgto . '" tipo="' . $r->tipo . '" os="' . $r->os . '" venda="' . $r->venda . '"><img src="' . base_url() . 'assets/img/2via.png"></a>';
                                                echo '<a class="gerarLinkPagamento" href="#modalLinkPagamento" title="Gerar Link pagamento"  style="margin-right: 1%" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '" valorVencimento="' . $r->valorVencimento . '" valorPago="' . $r->valorPago . '" dtVencimento="' . $r->dtVencimento . '"><img src="' . base_url() . 'assets/img/link-pagamento.png"></a>';
                                                echo '<a style="margin-right: 1%" href="' . base_url() . 'boleto/gerarBoleto/' . $this->session->userdata('cnpjempresa') . '/' . $boleto->transactions_integration_id . '" title="Gerar Boleto" target="_blank"><img src="' . base_url() . 'assets/img/cdBarras.png"></a>';
                                                echo '<a class="enviarEmail" title="Gerar boleto enviar por e-mail" href="#modal-email" style="margin-right: 1%" data-toggle="modal" role="button" transactions_integration_id="' . $boleto->transactions_integration_id . '" numeroParcela="' . $r->numeroParcela . '/' . $r->totalParcelas . '" dtVencimento="' . $vencimento . '" emailCliente="' . $r->emailCliente . '" idParcela="' . $r->idParcela . '"><img src="' . base_url() . 'assets/img/avisoNaoLido.png"></a>';
                                                echo '<a class="enviarWhatsApp" title="Gerar boleto e envir link por WhatsApp Web" href="#modal-whatsapp" style="margin-right: 1%" data-toggle="modal" role="button" celularCliente="' . $r->celularCliente . '" transactions_integration_id="' . $boleto->transactions_integration_id . '"><img src="' . base_url() . 'assets/img/icone-whatsapp.png"></a>';
                                                echo '<a class="estornar" href="#modalEstornarPagamento" title="Ver pagamentos" data-toggle="modal" role="button" style="margin-right: 1%;cursor: pointer;" idParcela="' . $r->idParcela . '">  <i class="fa fa-check-circle-o" style="font-size: 14px;color: #4AB858;"></i></a>';
                                                echo '<a href="' . base_url() . 'boleto/carne/' . $r->idLancamentos . '" title="Gerar Carne de pagamento" target="newBlank" style="margin-right: 1%;cursor: pointer;"> <i class="fa fa-newspaper-o" style="font-size: 14px;color: #4AB858;"></i></a>';
                                                echo '<a class="excluirRegistro" title="Cancelar parcela" idParcela="' . $r->idParcela . '" style="margin-right: 1%;cursor: pointer;"><img src="' . base_url() . 'assets/img/botExcluirGrid.png"></a>';
                                            } else {
                                                if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eLancamento')) {
                                                    echo '<a class="pagar" href="#modalPagarParcela" title="Baixar parcela"  style="margin-right: 1%" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '" valorVencimento="' . $r->valorVencimento . '" valorPago="' . $r->valorPago . '" dtVencimento="' . $r->dtVencimento . '"><img src="' . base_url() . 'assets/img/baixar-parcela.png"></a>';
                                                    if ($r->tipo == 'receita') echo '<a class="gerarLinkPagamento" href="#modalLinkPagamento" title="Gerar Link pagamento"  style="margin-right: 1%" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '" valorVencimento="' . $r->valorVencimento . '" valorPago="' . $r->valorPago . '" dtVencimento="' . $r->dtVencimento . '"><img src="' . base_url() . 'assets/img/link-pagamento.png"></a>';
                                                    echo '<a class="editar" href="#modalEditarParcela" title="Editar dados da parcela" style="margin-right: 1%" data-toggle="modal" role="button" idLancamento="' . $r->idParcela . '" valor="' . $r->valorPagar . '" status="' . $r->status . '" custo="' . $r->custo . '" previsaoPagamento="' . $r->previsaoPagamento . '" vencimento="' . $r->dtVencimento . '" pagamento="' . $r->data_pagamento . '" baixado="' . $r->baixado . '" fornecedor_id="' . $r->fornecedor_id . '" clientes_id="' . $r->clientes_id . '" cliente="' . $nomeClienteFornecedor . '" formaPgto="' . $r->forma_pgto . '" tipo="' . $r->tipo . '" os="' . $r->os . '" venda="' . $r->venda . '"><img src="' . base_url() . 'assets/img/botEditGrid.png"></a>';
                                                    echo '<a class="estornar" href="#modalEstornarPagamento" title="Ver pagamentos" style="margin-right: 1%;cursor: pointer;" data-toggle="modal" role="button" idParcela="' . $r->idParcela . '">  <i class="fa fa-check-circle-o" style="font-size: 14px;color: #4AB858;"></i></a>';
                                                    echo '<a class="excluirRegistro" title="Cancelar parcela" idParcela="' . $r->idParcela . '" style="margin-right: 1%;cursor: pointer;"><img src="' . base_url() . 'assets/img/botExcluirGrid.png"></a>';
                                                }
                                            }
                                        }
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6" style="margin-top: 20px;"></div>
                    <div class="col-sm-6" style="margin-top: 20px;">
                        <div class="table-responsive">
                            <table class="table" style="width: 98%;">
                                <tr>
                                    <td colspan="12" style="text-align: right; color: blue"> <strong>Total Receitas:</strong></td>
                                    <td colspan="9" style="text-align: right; color: blue"><strong>R$ <?php echo number_format($totalReceita,2,',','.') ?></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="12" style="text-align: right; color: green"> <strong>Total Pago:</strong></td>
                                    <td colspan="9" style="text-align: right; color: green"><strong>R$ <?php echo number_format($totalPago,2,',','.') ?></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="12" style="text-align: right; color: red"> <strong>Total Despesas:</strong></td>
                                    <td colspan="9" style="text-align: right; color: red"><strong>R$ <?php echo number_format($totalDespesa,2,',','.') ?></strong></td>
                                </tr>
                                <tr>
                                    <td colspan="12" style="text-align: right"> <strong>Lucro Líquido:</strong></td>
                                    <td colspan="9" style="text-align: right;"><strong>R$ <?php echo number_format($totalReceita - $totalDespesa - $totalCusto,2,',','.') ?></strong></td>
                                </tr>
                                <tr style="display: none;">
                                    <td colspan="12" style="text-align: right"> <strong>Total Com Nota Fiscal:</strong></td>
                                    <td colspan="9" style="text-align: right;"><strong>R$ <?php echo number_format($totalComNotaFiscal,2,',','.') ?></strong></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal nova receita -->
<div id="modalReceita" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-money"></i> Conta a receber</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form id="formReceita" method="post">
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-12" id="div_clienteConta">
                            <label for="cliente">Cliente*</label>
                            <select name="cliente" class="form-control" id="cliente" required="required">
                                    <option>--selecione--</option>
                                <?php foreach ($clientes as $cliente) { ?>
                                    <option value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                                <?php } ?>
                            </select>
                            <!--<a href="#adicionarCliente" id="addCliente" style="margin-top: -2%" role="button" data-toggle="modal" class="btn btn-p tip-top"><i id="pincliente" class="icon-plus-sign icon-white"></i></a>!-->
                        </div>
                        <div class="col-md-8" style="display: none;" id="div_fornecedorConta">
                            <label for="fornecedor">Fornecedor</label>
                            <select name="fornecedor_id" class="form-control" id="fornecedor_id" required="required">
                                <option value="">--Selecione um fornecedor--</option>
                                <?php foreach ($fornecedores as $fornecdor) { ?>
                                    <option value="<?php echo $fornecdor->idFornecedor; ?>"><?php echo $fornecdor->nomeFornecedor; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6" style="margin-left: 0">
                            <label for="receita_id">Categoria*</label>
                            <select name="receita_id" class="form-control" id="receita_id" required="required">
                                <option value="">--Selecione um cliente--</option>
                                <?php foreach ($receitas as $receita) { ?>
                                    <option value="<?php echo $receita->idReceita; ?>"><?php echo $receita->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="valor">Valor*</label>
                            <input class="form-control money" id="valor-receita" type="text" name="valor"/>
                            <input type="hidden" id="tipo" name="tipo" value="receita"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label for="valor">Data primeiro vencimento</label>
                            <input class="form-control" value="<?php echo date('Y-m-d'); ?>" id="dataPrimeiroVencimento-receita" type="date" name="dataPrimeiroVencimento"/>
                        </div>
                        <div class="col-md-6">
                            <label for="receita_id">Qtd Parcelas*</label>
                            <select class="form-control" name="condicao_pagamento_id" id="condicao_pagamento_id-receita" required="required">
                                <option value="">-- Selecione --</option>
                                <?php foreach ($condicoes as $condicao) { ?>
                                    <option value="<?php echo $condicao->idCondicaoPagamento; ?>"><?php echo $condicao->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" style="background:#f5f5f5;border-radius:4px;margin: 0;border:1px solid #ddd;margin-top: 10px;">
                        <div class="span12">
                            <table class="table table-responsive" id="tbParcelas-receita">
                                <thead>
                                <th>#</th>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <label for="tipo_cobranca_id">Tipo de cobrança?</label>
                        <select class="form-control" name="tipo_cobranca_id" id="tipo_cobranca_id" required="required">
                            <?php foreach ($tiposcobranca as $tipocobranca) { ?>
                                <option value="<?php echo $tipocobranca->idTipocobranca; ?>"><?php echo $tipocobranca->descricao; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
                        <textarea class="form-control" id="descricao" placeholder="Deixe mais informações sobre essa conta neste campo." name="descricao"></textarea>
                        <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>"/>
                    </div>
                    <div class="span12" style="margin-left: 0;display: none;">
                        <div class="span4" style="margin-left: 0">
                            <label for="recebido">Recebido?</label>
                            &nbsp &nbsp &nbsp &nbsp<input id="recebido" type="checkbox" name="recebido" value="1"/>
                        </div>
                        <div id="divRecebimento" class="span8" style=" display: none">
                            <div class="span6">
                                <label for="recebimento">Data Recebimento</label>
                                <input class="span12" id="recebimento" type="date" name="recebimento"/>
                            </div>
                            <div class="span6">
                                <label for="formaPgto">Forma Pgto</label>
                                <select name="formaPgto" id="formaPgto" class="span12">
                                    <?php foreach ($formasPagamento as $formaPagamento) { ?>
                                        <option value="<?php echo $formaPagamento->nome; ?>"><?php echo $formaPagamento->nome; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Cancelar</button>
                    <button class="btn btn-success" type="button" id="btnAdicionarNovaReceita"><i class="fa fa-save"></i> Adicionar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal nova despesa -->
<div id="modalDespesa" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-upload"></i> Conta a pagar</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form id="formDespesa" method="post">
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-12" id="div_clienteConta">
                            <label for="cliente">Cliente*</label>
                            <select name="cliente" class="form-control" id="cliente" required="required">
                                <ption>--selecione--</ption>
                                <?php foreach ($clientes as $cliente) { ?>
                                    <option value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                                <?php } ?>
                            </select>
                            <!--<a href="#adicionarCliente" id="addCliente" style="margin-top: -2%" role="button" data-toggle="modal" class="btn btn-p tip-top"><i id="pincliente" class="icon-plus-sign icon-white"></i></a>!-->
                        </div>
                        <div class="col-md-8" style="display: none;" id="div_fornecedorConta">
                            <label for="fornecedor">Fornecedor</label>
                            <select name="fornecedor_id" class="form-control" id="fornecedor_id" required="required">
                                <option value="">--Selecione um fornecedor--</option>
                                <?php foreach ($fornecedores as $fornecdor) { ?>
                                    <option value="<?php echo $fornecdor->idFornecedor; ?>"><?php echo $fornecdor->nomeFornecedor; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6" style="margin-left: 0;">
                            <label for="despesa_id">Categoria*</label>
                            <select name="despesa_id" class="form-control" id="despesa_id" required="required">
                                <option value="">--Selecione um cliente--</option>
                                <?php foreach ($despesas as $despesas) { ?>
                                    <option value="<?php echo $despesas->idDespesa; ?>"><?php echo $despesas->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="valor">Valor*</label>
                            <input class="form-control money" id="valor-despesa" type="text" name="valor"/>
                            <input type="hidden" id="tipo" name="tipo" value="receita"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label for="valor">Data primeiro vencimento</label>
                            <input class="form-control" value="<?php echo date('Y-m-d'); ?>" id="dataPrimeiroVencimento-despesa" type="date" name="dataPrimeiroVencimento"/>
                        </div>
                        <div class="col-md-6">
                            <label for="receita_id">Qtd Parcelas*</label>
                            <select class="form-control" name="condicao_pagamento_id" id="condicao_pagamento_id-despesa" required="required">
                                <option value="">-- Selecione --</option>
                                <?php foreach ($condicoes as $condicao) { ?>
                                    <option value="<?php echo $condicao->idCondicaoPagamento; ?>"><?php echo $condicao->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" style="background:#f5f5f5;border-radius:4px;margin: 0;border:1px solid #ddd;margin-top: 10px;">
                        <div class="span12">
                            <table class="table table-responsive" id="tbParcelas-despesa">
                                <thead>
                                <th>Valor</th>
                                <th>Vencimento</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <label for="tipo_cobranca_id">Tipo de cobrança?</label>
                        <select class="form-control" name="tipo_cobranca_id" id="tipo_cobranca_id" required="required">
                            <?php foreach ($tiposcobranca as $tipocobranca) { ?>
                                <option value="<?php echo $tipocobranca->idTipocobranca; ?>"><?php echo $tipocobranca->descricao; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
                        <textarea class="form-control" id="descricao" placeholder="Deixe mais informações sobre essa conta neste campo." name="descricao"></textarea>
                        <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>"/>
                    </div>
                    <div class="span12" style="margin-left: 0;display: none;">
                        <div class="span4" style="margin-left: 0">
                            <label for="recebido">Recebido?</label>
                            &nbsp &nbsp &nbsp &nbsp<input id="recebido" type="checkbox" name="recebido" value="1"/>
                        </div>
                        <div id="divRecebimento" class="span8" style=" display: none">
                            <div class="span6">
                                <label for="recebimento">Data Recebimento</label>
                                <input class="span12" id="recebimento" type="date" name="recebimento"/>
                            </div>
                            <div class="span6">
                                <label for="formaPgto">Forma Pgto</label>
                                <select name="formaPgto" id="formaPgto" class="span12">
                                    <?php foreach ($formasPagamento as $formaPagamento) { ?>
                                        <option value="<?php echo $formaPagamento->nome; ?>"><?php echo $formaPagamento->nome; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Cancelar</button>
                    <button class="btn btn-success" type="button" id="btnAdicionarNovaDespesa"><i class="fa fa-save"></i> Adicionar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal editar parcela -->
<div id="modalEditarParcela" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Parcela</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form id="modalEditarParcela" action="<?php echo base_url() ?>index.php/financeiro/editarParcela" method="post">
                <div class="modal-body">
                    <div class="span12" style="margin-left: 0">
                        <div class="span12" style="margin-left: 0;display: none;" id="div_fornecedor">
                            <label for="fornecedor">Fornecedor</label>
                            <select name="fornecedor_id" class="form-control" id="fornecedor_idEditar" required="required">
                                <option value="">--Selecione um fornecedor--</option>
                                <?php foreach ($fornecedores as $fornecdor) { ?>
                                    <option value="<?php echo $fornecdor->idFornecedor; ?>"><?php echo $fornecdor->nomeFornecedor; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="span12" style="margin-left: 0;display: none;" id="div_cliente">
                            <label for="cliente">Cliente</label>
                            <select name="cliente" class="form-control" id="clienteEditar" required="required">
                                <option value="">--Selecione um cliente--</option>
                                <?php foreach ($clientes as $cliente) { ?>
                                    <option value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <div class="span6">
                            <label for="valor">Valor pagar</label>
                            <input type="hidden" name="tipo" value="despesa"/>
                            <input type="hidden" id="idEditar" name="id" value=""/>
                            <input class="form-control money" type="text" name="valor" id="valorEditar"/>
                        </div>
                        <div class="span6">
                            <label for="vencimento">Novo vencimento</label>
                            <input class="form-control" type="date" required="required" name="vencimento" id="vencimentoEditar"/>
                        </div>
                    </div>
                    <div class="span12" style="margin-top: 15px;">
                        <textarea class="form-control" id="descricaoEditar" name="descricao" placeholder="Descreva neste espaço as informações desta parcela"></textarea>
                        <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" id="btnApenasEditarParcela"><i class="fa fa-edit"></i> Edita parcela</button>
                    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="btnEstornarParcela"><i class="fa fa-barcode"></i> Gerar 2º via boleto</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal para estornar pagamento -->
<div id="modalEstornarPagamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-money"></i> Pagamentos já realizados</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form id="formEstornarPagamento" method="post">
                <div class="modal-body">
                    <div class="widget-content nopadding">
                        <table class="table table-responsive" id="tbPagamentos">
                            <thead>
                            <tr style="backgroud-color: #2D335B">
                                <th style="text-align: center;">Nº</th>
                                <th style="text-align: center;">Forma de pagamento</th>
                                <th style="text-align: right;">Valor</th>
                                <th style="text-align: center;">Vencimento</th>
                                <th style="text-align: center;">Pagamento</th>
                                <th style="text-align: center;" colspan="2">Ações</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Gerar Link -->
<div id="modalLinkPagamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-barcode"></i> Gerar Link de Pagamento</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form id="formGerarlinkPagamentoParcela" method="post">
                <div class="modal-body">
                    <input type="hidden" name="idParcelaLinkPagamento" id="idParcelaLinkPagamento"/>
                    <div class="control-group">
                        <label for="forma_pagamento" class="control-label">Formas de cobrança disponível<span class="required">*</span></label>
                        <div class="controls">
                            <select id="forma_pagamento" class="form-control" name="forma_pagamento">
                                <option value="credito_boleto">Cartão de Crédito e Boletos</option>
                                <option value="boleto">Apenas boletos</option>
                                <option value="credito">Apenas Cartão de Crédito</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="modo_parcelamento" class="control-label">Mode de parcelamento<span class="required">*</span></label>
                        <div class="controls">
                            <select id="modo_parcelamento" class="form-control" name="modo_parcelamento" required="required" class="form-control">
                                <option value="interest_free">Sem juros, com o custo já calculado no valor da transação.</option>
                                <option value="with_interest">Modo de parcelamento, com juros por conta do comprador</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" style="display: none;">
                        <label for="numero_parcelas" class="control-label">Nº de parcelas para crédito<span class="required">*</span></label>
                        <div class="controls">
                            <input type="number" id="numero_parcelas" class="form-control" min="1" max="12" name="numero_parcelas" required="required" value="1"/>
                        </div>
                    </div>
                    <div class="control-group" style="background: #ddd;padding: 15px;font-size: 14px;text-align: center;font-weight: 700;cursor: pointer;display: none;" id="diGrupoLinkGerado">
                        <div class="controls">
                            <div id="divLinkGerado"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" id="btnGerarLinkPagamento"><i class="fa fa-barcode"></i> Gerar Link</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal pagar parcela -->
<div id="modalPagarParcela" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-usd"></i> Pagar Parcela</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form id="formPagarParcela" method="post">
                <div class="modal-body">
                    <div class="col-md-12" style="text-align: right">
                        <div class="col-md-9" style="text-align: right;font-size: 23px;"><label for="vencimentoParcela">Vencimento:</label></div>
                        <div class="col-md-3">
                            <label for="vencimentoParcela" style="font-size: 23px;" id="lValorVencimento"></label>
                            <input type="hidden" id="idParcelaPagamento" name="idParcela"/>
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align: right">
                        <div class="col-md-9" style="text-align: right;font-size: 23px;"><label for="pagamentoParcela">Pagamentos:</label></div>
                        <div class="col-md-3">
                            <label for="pagamentoParcela" style="font-size: 23px;border-bottom: 1px solid #607d8b;" id="lValorPago"></label>
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align: right">
                        <div class="col-md-9" style="text-align: right;font-size: 23px;"><label for="valorCompensar">Compensar:</label></div>
                        <div class="col-md-3">
                            <label for="valorCompensar" style="font-size: 23px;" id="lValorCompensar"></label>
                        </div>
                    </div>
                    <div class="col-md-12" style="background:#f5f5f5;border-radius:4px;margin: 0;border:1px solid #ddd;">
                        <div class="col-md-12">
                            <table class="table table-responsive" id="tbPagamento">
                                <thead>
                                <th>Forma pagamento</th>
                                <th>Data pagamento</th>
                                <th>Valor</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <label for="descricao">Observações do pagamento</label>
                        <textarea class="form-control" id="descricaoEditar" name="descricao"></textarea>
                        <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" type="button" onclick="adicionarPagamento();"><i class="fa fa-plus"></i> Adicionar pagamento</button>
                    <button class="btn btn-primary" type="button" id="btnPagamentoParcela"><i class="fa fa-money"></i> Confirmar Pagamento</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal editar lançamento -->
<div id="modalEditar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="formEditar" action="<?php echo base_url() ?>index.php/financeiro/editar" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Editar Lançamento</h3>
        </div>
        <div class="modal-body">
            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0;display: none;" id="div_fornecedor">
                    <label for="fornecedor">Fornecedor*</label>
                    <select name="fornecedor_id" class="span12" id="fornecedor_idEditar" required="required">
                        <option value="">--Selecione um fornecedor--</option>
                        <?php foreach ($fornecedores as $fornecdor) { ?>
                            <option value="<?php echo $fornecdor->idFornecedor; ?>"><?php echo $fornecdor->nomeFornecedor; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="span12" style="margin-left: 0;display: none;" id="div_cliente">
                    <label for="cliente">Cliente*</label>
                    <select name="cliente" class="span12" id="clienteEditar" required="required">
                        <option value="">--Selecione um cliente--</option>
                        <?php foreach ($clientes as $cliente) { ?>
                            <option value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span6">
                    <label for="valor">Valor*</label>
                    <input type="hidden" name="tipo" value="despesa"/>
                    <input type="hidden" id="idEditar" name="id" value=""/>
                    <input type="hidden" id="os" name="os" value=""/>
                    <input type="hidden" id="venda" name="venda" value=""/>
                    <input class="span12 money" type="text" name="valor" id="valorEditar"/>
                </div>
                <div class="span6">
                    <label for="valor">Custo</label>
                    <input class="span12 money" type="text" name="custo" id="custoEditar" value="0.00" min="0"/>
                </div>
            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span6">
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12" type="date" required="required" name="vencimento" id="vencimentoEditar"/>
                </div>
                <div class="span6">
                    <label for="previsaoPagamento">Data Previsão de Pagamento</label>
                    <input class="span12" type="date" name="previsaoPagamento" id="previsaoPagamentoEditar"/>
                </div>
                <div class="span3" style="display: none">
                    <label for="vencimento">Tipo*</label>
                    <select class="span12" name="tipo" id="tipoEditar">
                        <option value="receita">Receita</option>
                        <option value="despesa">Despesa</option>
                    </select>
                </div>
            </div>
            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <textarea class="span12" id="descricaoEditar" name="descricao"></textarea>
                <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""/>
            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="pago">Foi Pago?</label>
                    &nbsp &nbsp &nbsp &nbsp<input id="pagoEditar" type="checkbox" name="pago" value="1"/>
                </div>
                <div id="divPagamentoEditar" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="pagamento">Data Pagamento</label>
                        <input class="span12" id="pagamentoEditar" type="date" value="<?php echo date('Y-m-d'); ?>" name="pagamento"/>
                    </div>
                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto" id="formaPgtoEditar" class="span12">
                            <?php foreach ($formasPagamento as $formaPagamento) { ?>
                                <option value="<?php echo $formaPagamento->nome; ?>"><?php echo $formaPagamento->nome; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelarEditar">Cancelar</button>
            <button class="btn btn-primary">Salvar Alterações</button>
            <button class="btn btn-primary" id="btnAbrirOSVinculada">Abrir O.S/Venda</button>
        </div>
    </form>
</div>

<!-- Modal Excluir lançamento-->
<div id="modalExcluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Excluir Lançamento</h3>
    </div>
    <div class="modal-body">
        <h5 style="text-align: center">Deseja realmente excluir esse lançamento?</h5>
        <input name="id" id="idExcluir" type="hidden" value=""/>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
        <button class="btn btn-danger" id="btnExcluir">Excluir Lançamento</button>
    </div>
</div>

<!-- Modal enviar email -->
<div id="modal-email" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-usd"></i> Enviar e-mail do boleto para o cliente</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form action="#" method="post" id="frm-enviar-email">
                <div class="modal-body">
                    <div class="span12" style="margin-left: 0">
                        <label for="descricao">Para*</label>
                        <input class="form-control" type="text" name="para" id="emailPara" required="required" value=""/>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <label for="descricao">Assunto*</label>
                        <input class="form-control" type="text" name="assunto" id="assuntoEmail" required="required" value=""/>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <label for="descricao">Digite aqui sua mensagem*</label>
                        <textarea class="form-control redactor" rows="5" cols="10" id="mensagem" required="required" name="mensagem"></textarea>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idParcela" value="">
                        <input type="hidden" name="transactions_integration_id" id="transactions_integration_id">
                        <button class="btn btn-primary" type="button" id="btn-enviar-email"><i class="fa fa-send"></i> Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal enviar WhatsApp -->
<div id="modal-whatsapp" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-usd"></i> Enviar mensagem para o whatsApp do cliente</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <form action="<?php echo base_url(); ?>" method="post">
                <div class="modal-body">
                    <div class="span6" style="margin-left: 0">
                        <label for="descricao">Celular (apenas números)*</label>
                        <input class="form-control" type="number" name="celularCliente" placeholder="99 9 9999 9999" id="celularCliente" required="required" value=""/>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <label for="descricao">Mensagem*</label>
                        <textarea class="form-control" rows="5" cols="10" id="mensagemWhatsApp" required="required" name="mensagemWhatsApp"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" id="enviarWhatsApp"><i class="fa fa-whatsapp"></i> Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>js/formata.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $(".money").maskMoney();

        $('#btnsubmitform').click(function (event) {
            $('#frmLancamentos').attr('action', '<?php echo base_url();?>financeiro');
            $('#frmLancamentos').submit();
        });

        $('#btnDonwloadPDF').click(function (event) {
            $('#frmLancamentos').attr('action', '<?php echo base_url();?>financeiro/lancamentosRelatorioExecuta');
            $('#frmLancamentos').submit();
        });

        $('#gerarBoleto').click(function (event) {
            var elementos = new Array();

            $('.elementSelected:checked').each(function () {
                elementos.push($(this).val());
            });

            if (elementos.length === 0) {
                alert('Nenhum registro financeiro selecionado!');
            } else {
                for (var i = 0; i < elementos.length; i++) {
                    gerarBoletos(elementos[i]);
                }
            }
        });

        $('#cancelar-parcelas-selecao').click(function (event) {

            if (confirm('Deseja realmente cancelar as constas selecionadas?')) {
                var elementos = new Array();

                $('.elementSelected:checked').each(function () {
                    elementos.push($(this).val());
                });

                if (elementos.length === 0) {
                    alert('Nenhum registro financeiro selecionado!');
                } else {
                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Cancelando Aguarde...</h3>'});
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/financeiro/cancelarParcelas",
                        data: {
                            parcelasId: elementos
                        },
                        dataType: "json",
                        success: function (retorno) {
                            location.reload();
                        }
                    });
                }
            }
        });

        $('#condicao_pagamento_id-receita').change(function (event) {
            buscarParcelamentoReceita();
        });

        $('#condicao_pagamento_id-despesa').change(function (event) {
            buscarParcelamentoDespesa();
        });

        $('#dataPrimeiroVencimento-receita').change(function (event) {
            buscarParcelamentoReceita();
        });

        $('#dataPrimeiroVencimento-despesa').change(function (event) {
            buscarParcelamentoDespesa();
        });

        $('#valor-receita').keypress(function (event) {
            buscarParcelamentoReceita();
        });

        $('#valor-despesa').keypress(function (event) {
            buscarParcelamentoDespesa();
        });

        <?php if($periodo == 'filtros_por_data'){?>
        $('#filtrarPorData').show("slow");
        <?php } ?>

        $('#pago').click(function (event) {
            var flag = $(this).is(':checked');
            if (flag == true) {
                $('#divPagamento').show();
            } else {
                $('#divPagamento').hide();
            }
        });

        $('#recebido').click(function (event) {
            var flag = $(this).is(':checked');
            if (flag == true) {
                $('#divRecebimento').show();
            } else {
                $('#divRecebimento').hide();
            }
        });

        $('#pagoEditar').click(function (event) {
            var flag = $(this).is(':checked');
            if (flag == true) {
                $('#divPagamentoEditar').show();
            } else {
                $('#divPagamentoEditar').hide();
            }
        });


        $("#formReceita").validate({
            rules: {
                valor: {required: true},
                vencimento: {required: true}

            },
            messages: {
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            }
        });

        $("#formDespesa").validate({
            rules: {
                valor: {required: true},
                vencimento: {required: true}

            },
            messages: {
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            }
        });

        $(document).on('click', '.excluirRegistro', function (event) {
            if (confirm('Deseja realmente cancelar a parcela?')) {
                var idParcela = $(this).attr('idparcela');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/financeiro/cancelarParcela",
                    data: {
                        idParcela: idParcela
                    },
                    dataType: 'json',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
            return false;
        });

        $(document).on('click', '#btnAbrirOSVinculada', function (event) {
            var os = $("#os").val();
            var venda = $("#venda").val();
            var pagina = '';

            if (os !== '') pagina = '<?php echo base_url();?>index.php/os/visualizar/' + os;
            if (venda !== '') pagina = '<?php echo base_url();?>/index.php/vendas/visualizar/' + venda;

            if (pagina) window.open(pagina);
            else alert('Esse lançamento não possui venda ou o.s!');

        });

        $(document).on('click', '.enviarEmail', function (event) {
            var idParcela = $(this).attr('idparcela');
            var emailCliente = $(this).attr('emailcliente');
            var numeroParcela = $(this).attr('numeroparcela');
            var dtVencimento = $(this).attr('dtvencimento');
            var idBoleto = $(this).attr('transactions_integration_id');

            $('#mensagem').val('\n\n\n\n[LINK_BOLETO]');
            $('#assuntoEmail').val('Boleto da parcela ' + numeroParcela + ' com vencimento para ' + dtVencimento);
            $('#emailPara').val(emailCliente);
            $('#idParcela').val(idParcela);
            $('#transactions_integration_id').val(idBoleto);
        });

        $(document).on('click', '.enviarWhatsApp', function (event) {
            var idBoleto = $(this).attr('transactions_integration_id');
            var celularcliente = $(this).attr('celularcliente');

            celularcliente = celularcliente.replace('(', '');
            celularcliente = celularcliente.replace(')', '');
            celularcliente = celularcliente.replace('-', '');
            celularcliente = celularcliente.replace(' ', '');

            $('#celularCliente').val(celularcliente);
            $('#mensagemWhatsApp').val('\n\n\n*Segue o Boleto*: ' + '<?php echo base_url() . 'boleto/gerarBoleto/' . $this->session->userdata('cnpjempresa')?>/' + idBoleto);
        });

        $('#enviarWhatsApp').click(function (event) {
            var celularCliente = $('#celularCliente').val();
            var mensagemWhatsApp = $('#mensagemWhatsApp').val();

            if (celularCliente === '') {
                alert('Celular do cliente é obrigatório para o envio da mensagem');
                return;
            }

            window.open('https://api.whatsapp.com/send?phone=55'+celularCliente+'&text='+mensagemWhatsApp,'newPage');

            location.reload();
        });

        $(document).on('click', '.gerarLinkPagamento', function (event) {
            var idParcela = $(this).attr('idParcela');
            $("#idParcelaLinkPagamento").val(idParcela);
            $('#diGrupoLinkGerado').hide();
        });

        $(document).on('click', '.pagar', function (event) {

            var valorVencimento = $(this).attr('valorVencimento');
            var valorPago = $(this).attr('valorPago');
            var idParcela = $(this).attr('idParcela');

            $("#idParcelaPagamento").val(idParcela);
            $('#lValorVencimento').html(' R$' + getDouble(valorVencimento).toFixed(2));
            $('#lValorPago').html(' R$-' + getDouble(valorPago).toFixed(2));
            $('#tbPagamento tbody').html('');

            adicionarPagamento();
            calcularValorPagamento();
        });

        $(document).on('click', '.editar', function (event) {

            var tipo = $(this).attr('tipo');
            var status = $(this).attr('status');
            var dataPagamento = $(this).attr('pagamento');

            if (tipo === 'receita') {
                $('#div_cliente').show();
                $('#div_fornecedor').hide();

                $('#fornecedor_idEditar').removeAttr("required");
                $('#clienteEditar').attr("required", 'required');

            } else {
                $('#div_cliente').hide();
                $('#div_fornecedor').show();

                $('#clienteEditar').removeAttr("required");
                $('#fornecedor_idEditar').attr("required", 'required');
            }

            $("#idEditar").val($(this).attr('idLancamento'));
            $("#descricaoEditar").val($(this).attr('descricao'));
            $("#fornecedorEditar").val($(this).attr('cliente'));
            $("#valorEditar").val($(this).attr('valor'));
            $("#custoEditar").val($(this).attr('custo'));
            $("#vencimentoEditar").val($(this).attr('vencimento'));
            $("#previsaoPagamentoEditar").val($(this).attr('previsaoPagamento'));
            $('#fornecedor_idEditar').val($(this).attr('fornecedor_id'));
            $('#clienteEditar').val($(this).attr('clientes_id'));

            if (dataPagamento === '31/12/1969') dataPagamento = '<?php echo date('d/m/Y');?>';

            $("#pagamentoEditar").val(dataPagamento);
            $("#formaPgtoEditar").val($(this).attr('formaPgto'));
            $("#tipoEditar").val($(this).attr('tipo'));
            $("#urlAtualEditar").val($(location).attr('href'));
            $("#os").val($(this).attr('os'));
            $("#venda").val($(this).attr('venda'));

            var baixado = $(this).attr('baixado');
            if (baixado === 1) {
                $("#pagoEditar").attr('checked', true);
                $("#divPagamentoEditar").show();
            } else {
                $("#pagoEditar").attr('checked', false);
                $("#divPagamentoEditar").hide();
            }

            if (status === 'FATURADA') {
                $('#valorEditar').attr('disabled', true);
                $('#clienteEditar').attr('disabled', true);
            } else {
                $('#valorEditar').attr('disabled', false);
                $('#clienteEditar').attr('disabled', false);
            }
        });

        $('#btnEstornarParcela').click(function (event) {

            if (confirm('Deseja realmente estornar boleto e gerar uma 2º via?')) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/financeiro/estornarParcela",
                    data: {
                        idParcela: $('#idEditar').val(),
                        vencimento: $('#vencimentoEditar').val()
                    },
                    dataType: 'html',
                    success: function (data) {
                        gerarBoletos($('#idEditar').val());
                    }
                });
            }
        });

        $('#btn-enviar-email').click(function (event) {
            $('#modal-email').modal('hide');
            $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Enviando e-mail...</h3>'});
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/boleto/enviarEmail",
                data: $("#frm-enviar-email").serialize(),
                dataType: 'html',
                success: function (data) {
                    debugger;
                    location.reload();
                }
            });
        });

        $('#btnApenasEditarParcela').click(function (event) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/editarParcela",
                data: {
                    id: $('#idEditar').val(),
                    vencimento: $('#vencimentoEditar').val(),
                    urlAtual: '<?php echo current_url() ?>'
                },
                dataType: 'html',
                success: function (data) {
                    location.reload();
                }
            });
        });

        $('#btnAdicionarNovaReceita').click(function (event) {

            event.preventDefault();
            $('#modalReceita').modal('hide');
            $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Gerando Aguarde...</h3>'});

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/adicionarReceita",
                data: $("#formReceita").serialize(),
                dataType: 'html',
                success: function (retorno) {
                    debugger;
                    $.unblockUI();
                    $('#modalReceita').modal('hide');

                    if (retorno.trim() !== '') {

                        alert('ERRO!! '+retorno.trim());
                        /*
                        new PNotify({
                            title: 'Erro',
                            text: retorno,
                            type: 'error',
                            styling: 'bootstrap3'
                        });*/
                    } else {
                        location.reload();
                    }
                }
            });
        });

        $('#btnAdicionarNovaDespesa').click(function (event) {

            event.preventDefault();
            $('#modalDespesa').modal('hide');
            $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Gerando Aguarde...</h3>'});

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/adicionarDespesa",
                data: $("#formDespesa").serialize(),
                success: function (retorno) {
                    location.reload();
                }
            });
        });

        $('#btnPagamentoParcela').click(function (event) {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/pagar",
                data: $("#formPagarParcela").serialize(),
                success: function (retorno) {
                    location.reload();
                }
            });
        });

        $(document).on('click', '.estornar', function (event) {
            var id = $(this).attr('idparcela')

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/buscarPagamentos",
                data: {
                    'idParcela': id
                },
                dataType: 'json',
                success: function (parcelas) {
                    var html = '';
                    $(parcelas).each(function (index, pagamento) {
                        html += '<tr> ' +
                            '       <td style="text-align: center;">' + (index + 1) + '</td>' +
                            '       <td style="text-align: center;">' + pagamento.nome + '</td>' +
                            '       <td style="text-align: right;">R$' + pagamento.valor + '</td>' +
                            '       <td style="text-align: center;">' + formatDatePtBr(pagamento.dtVencimento) + '</td>' +
                            '       <td style="text-align: center;">' + formatDatePtBr(pagamento.dtUltimoPagamento) + '</td>';
                        //'       <td style="text-align: center;"><a  onclick="estornarPagamento(' + pagamento.idParcela + ',' + pagamento.fatura_id + ',' + pagamento.idPagamento + ');" title="Estornar pagamento" idPagamento="' + pagamento.idPagamento + '" style="margin-right: 1%"><img src="<?php echo base_url();?>assets/img/botExcluirGrid.png"></a></td>';

                        if (pagamento.transaction_id != null) html += '<td style="text-align: center;"><a  href="<?php echo base_url()?>pag/recibo/' + pagamento.transaction_id + '" target="newpage" style="margin-right: 1%"><img src="<?php echo base_url();?>assets/img/btnImprimir.gif"></a></td>';

                        html += '   </tr>';
                    });
                    $('#tbPagamentos tbody').html(html);
                }
            });
            return false;
        });

        $(document).on('click', '#btnGerarLinkPagamento', function (event) {
            var parcelaId = $("#idParcelaLinkPagamento").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/gerarLinkPagamentoParcela",
                data: {
                    parcelaId: parcelaId,
                    modo_parcelamento: $('#modo_parcelamento').val(),
                    forma_pagamento: $('#forma_pagamento').val(),
                    numero_parcelas: $('#numero_parcelas').val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#divLinkGerado').html(data.url);
                    $('#diGrupoLinkGerado').show();
                    debugger;
                }
            });
            return false;
        });

        $(document).on('click', '#btnExcluir', function (event) {
            var id = $("#idExcluir").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/excluirLancamento",
                data: "id=" + id,
                dataType: 'json',
                success: function (data) {
                    if (data.result == true) {
                        $("#btnCancelExcluir").trigger('click');
                        $("#datatable-responsive").html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
                        $("#datatable-responsive").load($(location).attr('href') + " #datatable-responsive");

                    } else {
                        $("#btnCancelExcluir").trigger('click');
                        alert('Ocorreu um erro ao tentar excluir produto.');
                    }
                }
            });
            return false;
        });
    });

    $('#periodo').on('change', function () {
        if (this.value === 'filtros_por_data') {
            $('#filtrarPorData').show("slow");
        } else {
            $('#filtrarPorData').hide("slow");
        }
    });

    function copiarLink() {
        copyTextToClipboard($('#divLinkGerado').html());
    }

    function copyTextToClipboard(text) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
            window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter", text);
        }
        document.body.removeChild(textArea);
    }

    function gerarBoletos(elemento) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde gerando boletos (' + elemento + ')...</h3>'});
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/financeiro/gerarBoletoZoop",
            data: {
                idParcela: elemento
            },
            dataType: "html",
            success: function (retorno) {
                $.unblockUI();

                $('#modalEditarParcela').modal('hide');

                if (retorno.trim() !== '') {
                    alert('ERRO!! '+retorno.trim());
                    /*
                    new PNotify({
                        title: 'Erro',
                        text: retorno,
                        type: 'error',
                        styling: 'bootstrap3'
                    });*/
                } else {
                    location.reload();
                }
            }
        });
    }

    function buscarParcelamentoReceita() {

        var condicoes = $('#condicao_pagamento_id-receita').val();

        if (condicoes !== '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/buscarParcelas",
                data: {
                    condicao: condicoes,
                    dataPrimeiroVencimento: $('#dataPrimeiroVencimento-receita').val(),
                    valor: $('#valor-receita').val()
                },
                dataType: 'html',
                success: function (html) {
                    $('#tbParcelas-receita tbody').html(html);
                    $(".money").maskMoney();
                }
            });
        } else {
            $('#tbParcelas-receita tbody').html('');
        }
    }

    function buscarParcelamentoDespesa() {

        var condicoes = $('#condicao_pagamento_id-despesa').val();

        if (condicoes !== '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/buscarParcelas",
                data: {
                    condicao: condicoes,
                    dataPrimeiroVencimento: $('#dataPrimeiroVencimento-despesa').val(),
                    valor: $('#valor-despesa').val()
                },
                dataType: 'html',
                success: function (html) {
                    $('#tbParcelas-despesa tbody').html(html);
                }
            });
        } else {
            $('#tbParcelas-despesa tbody').html('');
        }
    }

    function estornarPagamento(idParcela, idFatura, idPagamento) {

        if (confirm('Deseja realmente estornar o pagamento?')) {
            if (idPagamento !== '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/financeiro/estornarPagamento",
                    data: {
                        pagamentoId: idPagamento,
                        faturaId: idFatura,
                        parcelaId: idParcela,

                    },
                    dataType: 'json',
                    success: function (result) {
                        if (!result.error) {
                            $('#modalEstornarPagamento').modal('hide');

                            $("#datatable-responsive").html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
                            $("#datatable-responsive").load($(location).attr('href') + " #datatable-responsive");
                        } else {
                            alert(result.message);
                            $('#modalEstornarPagamento').modal('hide');
                        }
                    }
                });
            }
        }
    }

    function adicionarPagamento() {

        var html = '<tr>\n' +
            '   <td style="padding: 0px;">\n' +
            '       <select name="formaPgto[]" class="form-control">\n' +
            '           <?php foreach ($formasPagamento as $formaPagamento) {?>\n' +
            '               <option value="<?php echo $formaPagamento->idFormaPagamento;?>"><?php echo $formaPagamento->nome;?></option> ' +
            '           <?php } ?>\n' +
            '       </select>\n' +
            '   </td>\n' +
            '   <td style="padding: 0px;">\n' +
            '       <input type="date" class="form-control" name="dataPagamento[]" value="<?php echo date("Y-m-d");?>"> ' +
            '   </td>\n' +
            '   <td style="padding: 0px;">\n' +
            '       <input type="text" class="form-control" name="valorPagamento[]" onblur="formatarNumero(this,\'0.00\');" onkeyup="calcularValorPagamento();" value=""/>\n' +
            '   </td>\n' +
            '</tr> ';
        $('#tbPagamento tbody').append(html);
    }

    function calcularValorPagamento() {

        var valorVencimento = getDouble($('#lValorVencimento').html().replace('R$', ''));
        var valorPago = getDouble($('#lValorPago').html().replace('R$', '').replace('-', ''));
        var pagamento = 0;

        jQuery("input[name='valorPagamento[]']").each(function () {
            pagamento += getDouble(this.value);
        });

        var faltaPagar = valorVencimento - pagamento - valorPago;
        $('#lValorCompensar').html('R$' + faltaPagar.toFixed(2));

        if (faltaPagar < 0) $('#btnPagamentoParcela').attr('disabled', true);
        else $('#btnPagamentoParcela').attr('disabled', false);
    }

    function formatDatePtBr(data) {
        return data.split('-').reverse().join('/');
    }

    function checarTodos() {
        $('.elementSelected').each(function (index, obj) {
            if ($('#checkbox-all').is(':checked')) {
                if (obj.disabled) obj.checked = false;
                else obj.checked = true;
            } else {
                obj.checked = false;
            }
        });
    }

    function atualizarValorVencimentoAoEditarParcela(tag, pointer) {

        let valorDigitado = 0;
        let totalParcelasSuperior = 0;
        let tagVencimentos = $("input[name='valorVencimento[]']");
        let valorVencimento = $('#valor-receita').val();
        let totalParcelas =  tagVencimentos.length;

        if (valorVencimento === '') valorVencimento = 0;
        else valorVencimento = parseFloat(valorVencimento);

        $(tagVencimentos).each(function(index, element ){

            var valorElementoSuperior = element.value;

            if (index > pointer) {

                let novoVencimentoElement = (valorVencimento - valorDigitado)/(totalParcelas-totalParcelasSuperior);

                if (novoVencimentoElement <= 0) {
                    alert('A somatória dos vencimentos não pode ultrapassou o valor da conta e o valor dos vencimentos das parcelas deve ser maior que ZERO.');

                    tag.value = tag.getAttribute('vencimento');
                    atualizarValorVencimentoAoEditarParcela(tag, pointer);
                    return false;
                } else {
                    element.value = novoVencimentoElement.toFixed(2);
                    element.setAttribute('vencimento', novoVencimentoElement);
                }
            } else {
                valorDigitado += parseFloat(valorElementoSuperior);
                totalParcelasSuperior++;
            }
        });
    }
</script>