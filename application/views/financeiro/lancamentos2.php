<?php
date_default_timezone_set('America/Sao_Paulo');
?>

<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>

<?php

$situacao = $this->input->get('situacao');
$periodo = $this->input->get('periodo');

$dataInicial = $this->input->get('dataInicial');
$dataFinal = $this->input->get('dataFinal');

$previsaoPagamentoInicial = $this->input->get('previsaoPagamentoInicial');
$previsaoPagamentoFinal = $this->input->get('previsaoPagamentoFinal');

$tipolancamento = $this->input->get('tipolancamento');
?>

<style type="text/css">
    label.error{
        color: #b94a48;
    }

    input.error{
        border-color: #b94a48;
    }
    input.valid{
        border-color: #5bb75b;
    }
</style>


<?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aLancamento')){ ?>
    <div class="span5" style="margin-left: 0">
        <a href="#modalReceita" data-toggle="modal" role="button" class="btn btn-success tip-bottom"><i class="icon-plus icon-white"></i> Nova Receita</a>
        <a href="#modalDespesa" data-toggle="modal" role="button" class="btn btn-danger tip-bottom"><i class="icon-plus icon-white"></i> Nova Despesa</a>
    </div>
<?php } ?>

<div class="span7" style="margin-left: 0">
    <form action="<?php echo current_url(); ?>" method="get" >

        <div class="span2" style="margin-left: 0">
            <label>Tipo <i class="icon-info-sign tip-top"></i></label>
            <select name="tipolancamento" class="span12">
                <option value="todos">Todos</option>
                <option value="receita" <?php if($tipolancamento == 'receita'){ echo 'selected';} ?>>Receita</option>
                <option value="despesa" <?php if($tipolancamento == 'despesa'){ echo 'selected';} ?>>Despesa</option>
            </select>
        </div>

        <div class="span4">
            <label>Período <i class="icon-info-sign tip-top" title="Lançamentos com vencimento no período."></i></label>
            <select name="periodo" id="periodo" class="span12">
                <option value="dia">Dia</option>
                <option value="semana" <?php if($periodo == 'semana'){ echo 'selected';} ?>>Semana</option>
                <option value="filtros_por_data" <?php if($periodo == 'filtros_por_data'){ echo 'selected';} ?>>Filtrar Por Datas</option>
                <option value="mes" <?php if($periodo == 'mes'){ echo 'selected';} ?>>Mês</option>
                <option value="ano" <?php if($periodo == 'ano'){ echo 'selected';} ?>>Ano</option>
                <option value="todos" <?php if($periodo == 'todos'){ echo 'selected';} ?>>Todos</option>
            </select>
        </div>
        <div class="span4">
            <label>Situação <i class="icon-info-sign tip-top" title="Lançamentos com situação específica ou todos."></i></label>
            <select name="situacao" class="span12">
                <option value="todos">Todos</option>
                <option value="previsto" <?php if($situacao == 'previsto'){ echo 'selected';} ?>>Previsto</option>
                <option value="atrasado" <?php if($situacao == 'atrasado'){ echo 'selected';} ?>>Atrasado</option>
                <option value="realizado" <?php if($situacao == 'realizado'){ echo 'selected';} ?>>Realizado</option>
                <option value="pendente" <?php if($situacao == 'pendente'){ echo 'selected';} ?>>Pendente</option>
            </select>
        </div>
        <div class="span2" >
            <label>&nbsp;</label>
            <button type="submit" class="span12 btn btn-primary">Filtrar</button>
        </div>
        <div class="span12" id="filtrarPorData" style="display:none">
            <div class="widget-box">
                <div class="widget-content">
                    <div class="span12 well">

                        <div class="span6">
                            <label for="">Vencimento de:</label>
                            <input type="date" name="dataInicial" class="span12" value="<?php echo $dataInicial ?>"/>
                        </div>
                        <div class="span6">
                            <label for="">até:</label>
                            <input type="date" name="dataFinal" class="span12" value="<?php echo $dataFinal ?>"/>
                        </div>

                    </div>

                    <div class="span12 well"  style="margin-left: 0">

                        <div class="span6">
                            <label for="">Previsão de Pagamento de:</label>
                            <input type="date" name="previsaoPagamentoInicial" class="span12" value="<?php echo $previsaoPagamentoInicial ?>" />
                        </div>
                        <div class="span6">
                            <label for="">até:</label>
                            <input type="date" name="previsaoPagamentoFinal" class="span12" value="<?php echo $previsaoPagamentoFinal ?>"/>
                        </div>

                    </div>
                    &nbsp
                </div>
            </div>
        </div>
    </form>
</div>

<div class="span12" style="margin-left: 0;">

    <?php

    if(!$results){?>
        <div class="widget-box">
            <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
                <h5>Lançamentos Financeiros</h5>

            </div>

            <div class="widget-content nopadding">


                <table class="table table-bordered ">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th>#</th>
                        <th>Data Inicial</th>
                        <th>Data Final</th>
                        <th>Status</th>
                        <th>Defeito</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td colspan="6">Nenhuma lançamento encontrado</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } else{?>


    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Lançamentos Financeiros</h5>

        </div>

        <div class="widget-content nopadding">


            <table class="table table-responsive" id="divLancamentos">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="text-align: center;">Tipo</th>
                    <th style="text-align: left;">Cliente / Fornecedor</th>
                    <th>Vencimento</th>
                    <th>Previsão de Pagamento</th>
                    <th>Pagamento</th>
                    <th>Status</th>
                    <th style="text-align: right;">Custo</th>
                    <th style="text-align: right;">Valor</th>
                    <th style="text-align: center;">Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $totalReceita = 0;
                $totalDespesa = 0;
                $totalCusto   = 0;

                $saldo = 0;
                $os = null;

                foreach ($results as $r) {

                    $vencimento             = date(('d/m/Y'),strtotime($r->data_vencimento));
                    $data_pagamento         = '-';
                    $previsaoPagamento      = '-';
                    $previsaoPagamentoEditar= null;
                    $os                     = $r->os;
                    $totalCusto             += $r->custo;

                    if ($r->baixado) {
                        $data_pagamento = date(('d/m/Y'),strtotime($r->data_pagamento)).'<br/><small>('.$r->forma_pgto.')</small>';
                        $status = 'Pago';
                    } else {
                        $status = 'Pendente';
                    }

                    if ($r->previsaoPagamento != null && $r->previsaoPagamento != '0000-00-00') {
                        $previsaoPagamento = date(('d/m/Y'),strtotime($r->previsaoPagamento));
                        $previsaoPagamentoEditar = date(('d/m/Y'),strtotime($r->previsaoPagamento));
                    }

                    if($r->tipo == 'receita'){
                        $label = 'success';
                        $totalReceita += $r->valor;
                    } else{
                        $label = 'important';
                        $totalDespesa += $r->valor;
                    }

                    $nomeClienteFornecedor = '';
                    if ($r->fornecedor_id) {
                        $nomeClienteFornecedor = $r->nomeFornecedor;
                    }

                    if ($r->clientes_id) {
                        $nomeClienteFornecedor = $r->nomeCliente;
                    }

                    echo '<tr>';
                    echo '<td style="text-align: center;"><span class="label label-'.$label.'">'.ucfirst($r->tipo).'</span></td>';
                    echo '<td>'.$nomeClienteFornecedor.'</td>';
                    echo '<td style="text-align: center;">'.$vencimento.'</td>';
                    echo '<td style="text-align: center;">'.$previsaoPagamento.'</td>';
                    echo '<td style="text-align: center;">'.$data_pagamento.'</td>';
                    echo '<td style="text-align: center;">'.$status.'</td>';
                    echo '<td style="text-align: right;"> R$ '.number_format($r->custo,2,',','.').'</td>';
                    echo '<td style="text-align: right;"> R$ '.number_format($r->valor,2,',','.').'</td>';

                    echo '<td style="text-align: center;">';
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eLancamento')){
                        echo '<a href="#modalEditar" style="margin-right: 1%" data-toggle="modal" role="button" idLancamento="'.$r->idLancamentos.'" descricao="'.$r->descricao.'" valor="'.$r->valor.'" custo="'.$r->custo.'" previsaoPagamento="'.$r->previsaoPagamento.'" vencimento="'.$r->data_vencimento.'" pagamento="'.$r->data_pagamento.'" baixado="'.$r->baixado.'" fornecedor_id="'.$r->fornecedor_id.'" clientes_id="'.$r->clientes_id.'" cliente="'.$nomeClienteFornecedor.'" formaPgto="'.$r->forma_pgto.'" tipo="'.$r->tipo.'" os="'.$r->os.'" venda="'.$r->venda.'" class="btn btn-info tip-top editar"><i class="icon-pencil icon-white"></i></a>';
                    }
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'dLancamento')){
                        echo '<a href="#modalExcluir" data-toggle="modal" role="button" idLancamento="'.$r->idLancamentos.'" class="btn btn-danger tip-top excluir"><i class="icon-remove icon-white"></i></a>';
                    }

                    echo '</td>';
                    echo '</tr>';
                }?>
                <tr>

                </tr>
                </tbody>
                <tfoot>

                <tr>
                    <td colspan="8" style="text-align: right; color: blue"> <strong>Total Custo:</strong></td>
                    <td colspan="2" style="text-align: left; color: blue"><strong>R$ <?php echo number_format($totalCusto,2,',','.') ?></strong></td>
                </tr>

                <tr>
                    <td colspan="8" style="text-align: right; color: green"> <strong>Total Receitas:</strong></td>
                    <td colspan="2" style="text-align: left; color: green"><strong>R$ <?php echo number_format($totalReceita,2,',','.') ?></strong></td>
                </tr>

                <tr>
                    <td colspan="8" style="text-align: right; color: black"> <strong>Lucro Bruto:</strong></td>
                    <td colspan="2" style="text-align: left; color: black"><strong>R$ <?php echo number_format($totalReceita-$totalCusto,2,',','.') ?></strong></td>
                </tr>

                <tr>
                    <td colspan="8" style="text-align: right; color: red"> <strong>Total Despesas:</strong></td>
                    <td colspan="2" style="text-align: left; color: red"><strong>R$ <?php echo number_format($totalDespesa,2,',','.') ?></strong></td>
                </tr>
                <tr>
                    <td colspan="8" style="text-align: right"> <strong>Lucro Líquido:</strong></td>
                    <td colspan="2" style="text-align: left;"><strong>R$ <?php echo number_format($totalReceita - $totalDespesa - $totalCusto,2,',','.') ?></strong></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

</div>

<?php echo $this->pagination->create_links();}?>

<!-- Modal nova receita -->
<div id="modalReceita" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="formReceita" action="<?php echo base_url() ?>index.php/financeiro/adicionarReceita" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo NOME_SISTEMA?> - Adicionar Receita</h3>
            <input type="hidden" name="tipoLancamento" value="receita">
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0">
                    <label for="cliente">Cliente*</label>
                    <select name="cliente" class="span12" id="cliente" required="required">
                        <option value="">--Selecione um cliente--</option>
                        <?php foreach ($clientes as $cliente) {?>
                            <option value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                        <?php } ?>
                    </select>
                    <!--<a href="#adicionarCliente" id="addCliente" style="margin-top: -2%" role="button" data-toggle="modal" class="btn btn-p tip-top"><i id="pincliente" class="icon-plus-sign icon-white"></i></a>!-->
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0">
                    <label for="receita_id">Receita*</label>
                    <select name="receita_id" class="span12" id="receita_id" required="required">
                        <option value="">--Selecione um cliente--</option>
                        <?php foreach ($receitas as $receita) {?>
                            <optgroup label="Geral">
                                <option value="<?php echo $receita->idReceita; ?>"><?php echo $receita->nome; ?></option>
                            </optgroup>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span6">
                    <label for="valor">Valor*</label>
                    <input type="hidden" id="tipo" name="tipo" value="receita" />
                    <input class="span12 money" id="valor" type="text" name="valor"  />
                </div>

                <div class="span6">
                    <label for="valor">Custo</label>
                    <input class="span12 money" id="custoDespesa" type="text" name="custoDespesa" value="0.00"  />
                </div>

            </div>


            <div class="span12" style="margin-left: 0">
                <div class="span6" >
                    <label for="previsaoPagamento">Data Previsão de Pagamento</label>
                    <input class="span12" id="previsaoPagamento" type="date" name="previsaoPagamento"  />
                </div>

                <div class="span6" >
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12" id="vencimento" required="required" type="date" name="vencimento"  value="<?php echo date('Y-m-d');?>" />
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <textarea class="span12" id="descricao" name="descricao" ></textarea>
                <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>"  />
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="recebido">Recebido?</label>
                    &nbsp &nbsp &nbsp &nbsp<input  id="recebido" type="checkbox" name="recebido" value="1" />
                </div>
                <div id="divRecebimento" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="recebimento">Data Recebimento</label>
                        <input class="span12" id="recebimento" type="date" name="recebimento" />
                    </div>
                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto" id="formaPgto" class="span12">
                            <?php foreach ($formasPagamento as $formaPagamento) {?>
                                <option value="<?php echo $formaPagamento->nome;?>"><?php echo $formaPagamento->nome;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-success">Adicionar Receita</button>
        </div>
    </form>
</div>

<!-- Modal nova despesa -->
<div id="modalDespesa" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="formDespesa" action="<?php echo base_url() ?>index.php/financeiro/adicionarDespesa" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">MapOS - Adicionar Despesa</h3>
            <input type="hidden" name="tipoLancamento" value="despesa"/>
        </div>
        <div class="modal-body">
            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0">
                    <label for="fornecedor">Fornecedor*</label>
                    <select name="fornecedor_id" class="span12" id="fornecedor_id" required="required">
                        <option value="">--Selecione um fornecedor--</option>
                        <?php foreach ($fornecedores as $fornecdor) {?>
                            <option value="<?php echo $fornecdor->idFornecedor; ?>"><?php echo $fornecdor->nomeFornecedor; ?></option>
                        <?php } ?>
                    </select>
                </div>


            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span6" style="margin-left: 0">
                    <label for="valor">Valor*</label>
                    <input type="hidden"  name="tipo" value="despesa" />
                    <input class="span12 money"  type="text" name="valor"  />
                </div>

                <div class="span6">
                    <label for="valor">Custo</label>
                    <input class="span12 money" id="custoReceita" type="text" name="custoReceita" value="0.00" min="0" />
                </div>
            </div>

            <div class="span12" style="margin-left: 0">

                <div class="span6" >
                    <label for="previsaoPagamento">Data Previsão de Pagamento</label>
                    <input class="span12" type="date" name="previsaoPagamento"  />
                </div>

                <div class="span6" >
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12"  type="date" required="required" name="vencimento" value="<?php echo date('Y-m-d');?>"  />
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <textarea class="span12" id="descricao" name="descricao"></textarea>
                <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>"  />
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="pago">Foi Pago?</label>
                    &nbsp &nbsp &nbsp &nbsp<input  id="pago" type="checkbox" name="pago" value="1" />
                </div>
                <div id="divPagamento" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="pagamento">Data Pagamento</label>
                        <input class="span12 datepicker" id="pagamento" type="text" name="pagamento" />
                    </div>

                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto"  class="span12">
                            <?php foreach ($formasPagamento as $formaPagamento) {?>
                                <option value="<?php echo $formaPagamento->nome;?>"><?php echo $formaPagamento->nome;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Adicionar Despesa</button>
        </div>
    </form>
</div>

<!-- Modal editar lançamento -->
<div id="modalEditar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="formEditar" action="<?php echo base_url() ?>index.php/financeiro/editar" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">MapOS - Editar Lançamento</h3>
        </div>
        <div class="modal-body">
            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

            <div class="span12" style="margin-left: 0">

                <div class="span12" style="margin-left: 0;display: none;" id="div_fornecedor">
                    <label for="fornecedor">Fornecedor*</label>
                    <select name="fornecedor_id" class="span12" id="fornecedor_idEditar" required="required">
                        <option value="">--Selecione um fornecedor--</option>
                        <?php foreach ($fornecedores as $fornecdor) {?>
                            <option value="<?php echo $fornecdor->idFornecedor; ?>"><?php echo $fornecdor->nomeFornecedor; ?></option>
                        <?php } ?>
                    </select>
                </div>


                <div class="span12" style="margin-left: 0;display: none;" id="div_cliente">
                    <label for="cliente">Cliente*</label>
                    <select name="cliente" class="span12" id="clienteEditar" required="required">
                        <option value="">--Selecione um cliente--</option>
                        <?php foreach ($clientes as $cliente) {?>
                            <option value="<?php echo $cliente->idClientes; ?>"><?php echo $cliente->nomeCliente; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span6">
                    <label for="valor">Valor*</label>
                    <input type="hidden"  name="tipo" value="despesa" />
                    <input type="hidden"  id="idEditar" name="id" value="" />
                    <input type="hidden"  id="os" name="os" value="" />
                    <input type="hidden"  id="venda" name="venda" value="" />
                    <input class="span12 money"  type="text" name="valor" id="valorEditar" />
                </div>

                <div class="span6">
                    <label for="valor">Custo</label>
                    <input class="span12 money"  type="text" name="custo" id="custoEditar" value="0.00" min="0" />
                </div>

            </div>

            <div class="span12" style="margin-left: 0">

                <div class="span6" >
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12"  type="date" required="required" name="vencimento" id="vencimentoEditar"  />
                </div>

                <div class="span6" >
                    <label for="previsaoPagamento">Data Previsão de Pagamento</label>
                    <input class="span12"  type="date" name="previsaoPagamento" id="previsaoPagamentoEditar"  />
                </div>


                <div class="span3" style="display: none">
                    <label for="vencimento">Tipo*</label>
                    <select class="span12" name="tipo" id="tipoEditar">
                        <option value="receita">Receita</option>
                        <option value="despesa">Despesa</option>
                    </select>
                </div>

            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <textarea class="span12" id="descricaoEditar" name="descricao"></textarea>
                <input id="urlAtualEditar" type="hidden" name="urlAtual" value=""  />
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="pago">Foi Pago?</label>
                    &nbsp &nbsp &nbsp &nbsp<input  id="pagoEditar" type="checkbox" name="pago" value="1" />
                </div>
                <div id="divPagamentoEditar" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="pagamento">Data Pagamento</label>
                        <input class="span12" id="pagamentoEditar" type="date" value="<?php echo date('Y-m-d');?>" name="pagamento" />
                    </div>

                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto" id="formaPgtoEditar"  class="span12">
                            <?php foreach ($formasPagamento as $formaPagamento) {?>
                                <option value="<?php echo $formaPagamento->nome;?>"><?php echo $formaPagamento->nome;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelarEditar">Cancelar</button>
            <button class="btn btn-primary">Salvar Alterações</button>
            <button class="btn btn-primary" id="btnAbrirOSVinculada">Abrir O.S/Venda</button>
        </div>
    </form>
</div>


<!-- Modal Excluir lançamento-->
<div id="modalExcluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">MapOS - Excluir Lançamento</h3>
    </div>
    <div class="modal-body">
        <h5 style="text-align: center">Deseja realmente excluir esse lançamento?</h5>
        <input name="id" id="idExcluir" type="hidden" value="" />
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
        <button class="btn btn-danger" id="btnExcluir">Excluir Lançamento</button>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $( document ).ready(function() {
            $(".money").maskMoney();
        });

        <?php if($periodo == 'filtros_por_data'){?>
        $('#filtrarPorData').show("slow");
        <?php } ?>

        $('#pago').click(function(event) {
            var flag = $(this).is(':checked');
            if(flag == true){
                $('#divPagamento').show();
            }
            else{
                $('#divPagamento').hide();
            }
        });

        $('#recebido').click(function(event) {
            var flag = $(this).is(':checked');
            if(flag == true){
                $('#divRecebimento').show();
            }
            else{
                $('#divRecebimento').hide();
            }
        });

        $('#pagoEditar').click(function(event) {
            var flag = $(this).is(':checked');
            if(flag == true){
                $('#divPagamentoEditar').show();
            }
            else{
                $('#divPagamentoEditar').hide();
            }
        });


        $("#formReceita").validate({
            rules:{
                valor: {required:true},
                vencimento: {required:true}

            },
            messages:{
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            }
        });

        $("#formDespesa").validate({
            rules:{
                valor: {required:true},
                vencimento: {required:true}

            },
            messages:{
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            }
        });


        $(document).on('click', '.excluir', function(event) {
            $("#idExcluir").val($(this).attr('idLancamento'));
        });

        $(document).on('click', '#btnAbrirOSVinculada', function(event) {

            var os      = $("#os").val();
            var venda   = $("#venda").val();
            var pagina  = '';

            if (os !== '') {
                pagina = '<?php echo base_url();?>index.php/os/visualizar/'+os;
            }

            if (venda !== '') {
                pagina = '<?php echo base_url();?>/index.php/vendas/visualizar/'+venda;
            }

            if (pagina) {
                window.open(pagina);
            } else {
                alert('Esse lançamento não possui venda ou o.s!');
            }
        });


        $(document).on('click', '.editar', function(event) {

            var tipo = $(this).attr('tipo');

            if (tipo === 'receita') {
                $('#div_cliente').show();
                $('#div_fornecedor').hide();

                $('#fornecedor_idEditar').removeAttr( "required" );
                $('#clienteEditar').attr( "required" , 'required' );

            } else {
                $('#div_cliente').hide();
                $('#div_fornecedor').show();

                $('#clienteEditar').removeAttr( "required" );
                $('#fornecedor_idEditar').attr( "required" , 'required' );
            }

            $("#idEditar").val($(this).attr('idLancamento'));
            $("#descricaoEditar").val($(this).attr('descricao'));
            $("#fornecedorEditar").val($(this).attr('cliente'));
            $("#valorEditar").val($(this).attr('valor'));
            $("#custoEditar").val($(this).attr('custo'));
            $("#vencimentoEditar").val($(this).attr('vencimento'));
            $("#previsaoPagamentoEditar").val($(this).attr('previsaoPagamento'));
            $('#fornecedor_idEditar').val($(this).attr('fornecedor_id'));
            $('#clienteEditar').val($(this).attr('clientes_id'));

            var dataPagamento = $(this).attr('pagamento');

            if (dataPagamento == '31/12/1969') {
                dataPagamento = '<?php echo date('d/m/Y');?>';
            }

            $("#pagamentoEditar").val(dataPagamento);
            $("#formaPgtoEditar").val($(this).attr('formaPgto'));
            $("#tipoEditar").val($(this).attr('tipo'));
            $("#urlAtualEditar").val($(location).attr('href'));
            $("#os").val($(this).attr('os'));
            $("#venda").val($(this).attr('venda'));

            var baixado = $(this).attr('baixado');
            if(baixado == 1){
                $("#pagoEditar").attr('checked', true);
                $("#divPagamentoEditar").show();
            } else{
                $("#pagoEditar").attr('checked', false);
                $("#divPagamentoEditar").hide();
            }
        });

        $(document).on('click', '#btnExcluir', function(event) {
            var id = $("#idExcluir").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/financeiro/excluirLancamentoV1",
                data: "id="+id,
                dataType: 'json',
                success: function(data)
                {
                    if(data.result == true){
                        $("#btnCancelExcluir").trigger('click');
                        $("#divLancamentos").html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
                        $("#divLancamentos").load( $(location).attr('href')+" #divLancamentos" );

                    }
                    else{
                        $("#btnCancelExcluir").trigger('click');
                        alert('Ocorreu um erro ao tentar excluir produto.');
                    }
                }
            });
            return false;
        });

        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });});

    $('#periodo').on('change', function() {
        if( this.value == 'filtros_por_data') {
            $('#filtrarPorData').show("slow");
        } else {
            $('#filtrarPorData').hide("slow");
        }
    })


</script>