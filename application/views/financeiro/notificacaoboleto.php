<html>
<body>
<div style="background:#f2f2f2;font-family:'Open Sans','Helvetica',sans-serif">
    <table border="0" width="95%" cellpadding="0" cellspacing="0" style="max-width:620px;min-width:375px;background:#ffffff" align="center">
        <tbody>
        <tr style="background-color:#0181a0;height:75px">
            <td width="10%"></td>
            <td style="text-align: center;"><img alt="" src="https://onerbank.com.br/wp-content/uploads/2019/06/Logo-OnerBank.png" data-image-whitelisted="" class="CToWUd"><a href="https://www.juno.com.br" style="float:right" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.juno.com.br&amp;source=gmail&amp;ust=1576888846630000&amp;usg=AFQjCNF-PgdDGUJdkWv6KfeV1or2cG7scg"><img alt=""
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      src="https://mail.google.com/mail/u/0?ui=2&amp;ik=efe3e5987d&amp;attid=0.0.4&amp;permmsgid=msg-f:1653043942055018074&amp;th=16f0cab5044cfe5a&amp;view=fimg&amp;sz=s0-l75-ft&amp;attbid=ANGjdJ-ylue-Bdb2np9X6au-b2NtkknMawsAtFRJv83bPaCZ-LR6j-nXH-0lrPPU7354evxOYR_FSu3p1bYiF_RQSuVXPmvvQNjt93NhWbLmZBzYxtrEF2ppPKznMFI&amp;disp=emb" data-image-whitelisted="" class="CToWUd"></a></td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td><p style="padding:20px 0;font-size:18px;font-weight:bold;text-align:left;color:#0181a0">
                    Boleto recebido!
                </p></td>
            <td style="border-left:2px solid #0181a0" width="10%"></td>
        </tr>
        <tr>
            <td width="10%" style="vertical-align:top"></td>
            <td style="border-top:2px solid #0181a0"></td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td>
                <div style="font-size:16px;text-align:left;color:#000000;padding:5px 0;line-height:1.75;display:inline-flex">
                    <img alt="Logo" src="<?php echo $emitente->url_logo;?>" data-image-whitelisted=""width="30%" class="CToWUd"><br><br>
                    <div style="float:left">
                        <p style="font-size:14px;color:#333333;line-height:24.5px">Olá, <span style="font-size:16px;font-weight:600;color:#0181a0"><?php echo $cliente->nomeCliente;?></span>, tudo bem?</p>
                        <p style="font-size:14px;color:#333333;line-height:24.5px"><span style="font-size:16px;font-weight:600;color:#0181a0"><?php echo $emitente->nome?></span> enviou um
                            boleto de <br/><span style="font-size:16px;font-weight:600;color:#0181a0">R$ <?php echo $this->site_model->formatarValorMonetario($fatura->valorFatura); ?></span> com vencimento em <span style="font-size:16px;font-weight:600;color:#0181a0"><?php echo $this->site_model->formatarData($fatura->dtVencimento); ?></span> para você.</p>
                    </div>
                </div>
            </td>
            <td width="10%"></td>
        </tr>
        <tr style="background-color:#f4f7fa">
            <td width="10%"></td>
            <td>
                <table border="0" style="line-height:20px">
                    <tbody>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="font-size:12px;color:#999999;line-height:24px">Dados do boleto</td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;color:#333333;line-height:24.5px">Descrição:</td>
                    </tr>
                    <tr>
                        <td style="font-size:16px;font-weight:600;color:#0181a0"><?php echo $receita->nome;?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;color:#333333;line-height:24.5px">Emitido por:</td>
                    </tr>
                    <tr>
                        <td style="font-size:16px;font-weight:600;color:#0181a0"><?php echo $emitente->nome?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;color:#333333;line-height:24.5px">Email: <span style="font-size:16px;font-weight:600;color:#0181a0"><a href="mailto:<?php echo $cliente->email;?>" target="_blank"><?php echo $cliente->email;?></a></span></td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;color:#333333;line-height:24.5px">Valor: <span style="font-size:16px;font-weight:600;color:#0181a0">R$ <?php echo $this->site_model->formatarValorMonetario($fatura->valorFatura); ?></span></td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;color:#333333;line-height:24.5px">Vencimento: <span style="font-size:16px;font-weight:600;color:#0181a0"><?php echo $this->site_model->formatarData($fatura->dtVencimento); ?></span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td><br>
                <center>
                    <?php
                        //$this->site_model->geradorDeCodigoDeBarras($boleto->barcode);
                    ?>
                    <!--<div style="text-align:center"><img alt="BarCode" src="<?php echo  base_url().'barcode.gif'?>" data-image-whitelisted="" class="CToWUd"></div>!-->

                    <div style="text-align:center"><img alt="BarCode" src="<?php echo 'https://chart.apis.google.com/chart?cht=qr&chl='.$boleto->url_boleto.'&chs=150x150';?>" data-image-whitelisted="" class="CToWUd"></div>
                    <small></small></center>
                <br>
            </td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td>
                <p style="font-size:14px;color:#333333;line-height:24.5px;padding:20px 0;text-align:center">Você pode copiar o código do boleto abaixo para pagar online:<br><span style="font-size:16px;font-weight:600;color:#0181a0">
                        <?php echo $this->site_model->formatarBarCodeBoleto($boleto->barcode);?>
                    </span>
                </p>
            </td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td>
                <center><a href="<?php echo $boleto->url_boleto?>" target="_blank">
                        <table border="0" style="background-color:#55d200;border-radius:3px;color:#ffffff;display:inline-block;font-family:'Open Sans','Arial',sans-serif;font-size:16px;line-height:35px;padding:0px 10px 0px 10px;border-radius:4px">
                            <tbody>
                            <tr>
                                <td width="200px">
                                    <center>Visualizar boleto clique aqui</center>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </a></center>
            </td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td>
                <p style="font-size:12px;color:#333333;padding:20px 0"></p>
            </td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td style="border-bottom:2px solid #0181a0"></td>
            <td width="10%" style="vertical-align:bottom"></td>
        </tr>
        <tr>
            <td style="border-right:2px solid #0181a0" width="10%"></td>
            <td><p style="line-height:1.75;float:right;font-family:'Helvetica';font-size:12px;text-align:right;color:#999999;line-height:1.57">Enviado por OnerBank © <?php echo date('Y');?> |  AMBIENTE SEGURO</span></p></td>
            <td width="10%"></td>
        </tr>
        <tr>
            <td colspan="3">
                <br>
                <div style="font-size:11px;color:#aaaaaa;line-height:16px;text-align:center">
                    <strong>OnerBank</strong> - Sua solução completa em pagamentos.
                    <br>
                    <br>
                    <a href="https://onerbank.com.br/" target="_blank">https://onerbank.com.br/</a>
                </div>
                <br>
            </td>
        </tr>
        </tbody>
    </table>
    <table width="95%" border="0" cellpadding="0" cellspacing="0" style="max-width:630px;min-width:375px" align="center">
        <tbody></tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL">
        <br><br></div>
</div>
</body>
</html>