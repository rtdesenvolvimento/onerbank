<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Cadastro de Fornecedor</h5>
            </div>
            <div class="widget-content nopadding">

                <?php if ($custom_error != '') {
                    echo '<div class="alert alert-danger">' . $custom_error . '</div>';
                } ?>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="span12" id="divCadastrarOs">

                            <ul class="nav nav-tabs">
                                <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes do Fornecedor</a></li>
                            </ul>

                            <form action="<?php echo current_url(); ?>" id="formFornecedor" method="post" class="form-horizontal">

                                <div class="span12" style="padding: 1%">
                                    <div class="span2">
                                        <label for="tipoPessoa">Tipo de pessoa</label>
                                        <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                                            <option value="PJ">Pessoa Jurídica</option>
                                            <option value="PF">Pessoa Física</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">

                                    <div class="span4">
                                        <label for="nomeFornecedor" >Nome / Razão Social<span class="required">*</span></label>
                                        <input id="nomeFornecedor"  class="span12" type="text" name="nomeFornecedor"
                                               value="<?php echo set_value('nomeFornecedor'); ?>"/>
                                    </div>

                                    <div class="span4">
                                        <label for="nomeFantasiaApelido" >Nome Fantasia / Apelido</label>
                                        <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                                               value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                                    </div>

                                    <div class="span2">
                                        <label for="documento">CPF/CNPJ</label>
                                        <input id="documento" type="text" class="span12" name="documento"
                                               value="<?php echo set_value('documento'); ?>"/>
                                    </div>

                                    <div class="span2" id="div_sexo" style="display: none;">
                                        <label for="sexo">Sexo</label>
                                        <select name="sexo" id="sexo" class="span12 chzn" required="required">
                                            <option value="M">Masculino</option>
                                            <option value="F">Feminino</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0;display: none;" id="div_rg">
                                    <div class="span3">
                                        <label for="rg">RG</label>
                                        <input id="rg" type="text" class="span12" name="rg"
                                               value="<?php echo set_value('rg'); ?>"/>
                                        </select>
                                    </div>

                                    <div class="span3">
                                        <label for="orgaoEmissor">Orgão Emissor</label>
                                        <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                                               value="<?php echo set_value('orgaoEmissor'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="estadoOrgaoEmissor">Estado Emissor</label>
                                        <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                                               value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="dataOrgaoEmissor">Data Emissão</label>
                                        <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                                               value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span3">
                                        <label for="telefone">Telefone</label>
                                        <input id="telefone" type="text" class="span12" name="telefone"
                                               value="<?php echo set_value('telefone'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="celular">Celular</label>
                                        <input id="celular" type="text" class="span12" name="celular"
                                               value="<?php echo set_value('celular'); ?>"/>
                                    </div>

                                    <div class="span4">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="span12" name="email"
                                               value="<?php echo set_value('email'); ?>"/>
                                    </div>

                                    <div class="span2">
                                        <label for="email">Data de nascimento</label>
                                        <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                                               value="<?php echo set_value('data_nascimento'); ?>"/>
                                    </div>

                                </div>

                                <ul class="nav nav-tabs">
                                    <li class="active" id="tabDetalhes">
                                        <a href="#tab1" data-toggle="tab">
                                            Dados do endereço
                                        </a>
                                    </li>
                                </ul>

                                <div class="span12" style="padding: 1%; margin-left: 0">

                                    <div class="span3">
                                        <label for="cep">CEP</label>
                                        <input id="cep" type="text" name="cep" class="span12" onBlur="getConsultaCEP();"
                                               value="<?php echo set_value('cep'); ?>"/>
                                        <small>[TAB] consulta cep (Necessita Internet)</small>
                                    </div>

                                    <div class="span6">
                                        <label for="rua">Rua</label>
                                        <input id="rua" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                                    </div>

                                    <div class="span3">
                                        <label for="numero">Número</label>
                                        <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span12">
                                        <label for="complemento">Complemento</label>
                                        <input id="complemento" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span5">
                                        <label for="bairro">Bairro</label>
                                        <input id="bairro" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                                    </div>

                                    <div class="span4">
                                        <label for="cidade">Cidade</label>
                                        <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                                    </div>
                                    <div class="span3">
                                        <label for="estado">Estado</label>
                                        <input id="estado" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                                    </div>
                                </div>

                                <div id="div_contato">
                                    <ul class="nav nav-tabs">
                                        <li class="active" id="tabDetalhes">
                                            <a href="#tab1" data-toggle="tab">
                                                Dados do  Contato
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="span12" style="padding: 1%; margin-left: 0">

                                        <div class="span7">
                                            <label for="contatoNomeFornecedor" >Nome<span class="required">*</span></label>
                                            <input id="contatoNomeFornecedor"  class="span12" type="text" name="contatoNomeFornecedor"
                                                   value="<?php echo set_value('contatoNomeFornecedor'); ?>"/>
                                        </div>

                                        <div class="span3">
                                            <label for="contatoCpf">CPF</label>
                                            <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                                                   value="<?php echo set_value('contatoCpf'); ?>"/>
                                        </div>

                                        <div class="span2">
                                            <label for="contatoSexo">Sexo</label>
                                            <select name="contatoSexo" id="contatoSexo" class="span12 chzn" required="required">
                                                <option value="M">Masculino</option>
                                                <option value="F">Feminino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span3">
                                            <label for="contatoTelefone">Telefone</label>
                                            <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                                                   value="<?php echo set_value('contatoTelefone'); ?>"/>
                                        </div>

                                        <div class="span3">
                                            <label for="contatoCelular">Celular</label>
                                            <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                                                   value="<?php echo set_value('contatoCelular'); ?>"/>
                                        </div>

                                        <div class="span4">
                                            <label for="email">Email</label>
                                            <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                                                   value="<?php echo set_value('contatoEmail'); ?>"/>
                                        </div>

                                        <div class="span2">
                                            <label for="email">Data de nascimento</label>
                                            <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                                                   value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active" id="tabDetalhes">
                                        <a href="#tab1" data-toggle="tab">
                                            Observação
                                        </a>
                                    </li>
                                </ul>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span12">
                                        <textarea class="span12"  name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                                    </div>
                                </div>


                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span6 offset3" style="text-align: center">
                                        <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i>
                                            Adicionar
                                        </button>
                                        <a href="<?php echo base_url() ?>index.php/fornecedor" id="" class="btn"><i
                                                class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">

    function getConsultaCEP() {
        if ($.trim($("#cep").val()) != "") {
            var url = 'http://api.postmon.com.br/v1/cep/' + $("#cep").val();
            $.get(url, {
                    cep: $("#cep").val()
                },
                function (data) {
                    if (data != -1) {
                        $("#rua").val(data.logradouro);
                        $("#bairro").val(data.bairro);
                        $("#cidade").val(data.cidade);
                        $("#estado").val(data.estado);
                        $('#numero').focus();
                    } else {
                        alert("Endereço não encontrado");
                    }
                });
        }
    }

    $(document).ready(function () {

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });

        $('#formFornecedor').validate({
            rules: {
                nomeFornecedor: {required: true}
            },
            messages: {
                nomeFornecedor: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>




