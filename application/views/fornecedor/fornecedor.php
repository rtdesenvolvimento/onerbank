<?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aCliente')) { ?>
    <a href="<?php echo base_url(); ?>index.php/fornecedor/adicionar" class="btn btn-success"><i
            class="icon-plus icon-white"></i> Adicionar Fornecedor</a>
<?php } ?>

<?php
if (!$results){
?>

<div class="widget-box">

    <div class="col-xs-6 text-left">

        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Fornecedores</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th>Nome</th>
                    <th>CPF/CNPJ</th>
                    <th>Telefone</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhum Fornecedor Cadastrado</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php }else{?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-user"></i>
         </span>
            <h5>Fornecedores</h5>
        </div>

        <div class="widget-content nopadding">
            <div class="col-xs-6 text-left">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th style="text-align: left;">Nome</th>
                        <th style="text-align: left;">CPF/CNPJ</th>
                        <th style="text-align: left;">Telefone</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($results as $r) {
                        echo '<tr>';
                        echo '<td style="text-align: left;">' . $r->nomeFornecedor . '</td>';
                        echo '<td style="text-align: left;">' . $r->documento . '</td>';
                        echo '<td style="text-align: left;">' . $r->telefone . '</td>';
                        echo '<td>';
                        if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
                            echo '<a href="' . base_url() . 'index.php/fornecedor/visualizar/' . $r->idFornecedor . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                        }
                        if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eCliente')) {
                            echo '<a href="' . base_url() . 'index.php/fornecedor/editar/' . $r->idFornecedor . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                        }
                        if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dCliente')) {
                            echo '<a href="#modal-excluir" role="button" data-toggle="modal" fornecedor="' . $r->idFornecedor . '" style="margin-right: 1%" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>';
                        }
                        echo '</td>';
                        echo '</tr>';
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php echo $this->pagination->create_links();
        } ?>

        <!-- Modal -->
        <div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <form action="<?php echo base_url() ?>index.php/fornecedor/excluir" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 id="myModalLabel">Excluir Fornecedor</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idFornecedor" name="id" value=""/>
                    <h5 style="text-align: center">Deseja realmente excluir este fornecedor e os dados associados a ele
                        (OS, Vendas, Receitas)?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Excluir</button>
                </div>
            </form>
        </div>

        <style type="text/css">
            .busca_veiculo input {
                margin-bottom: 0;
            }

        </style>


        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on('click', 'a', function (event) {
                    var fornecedor = $(this).attr('fornecedor');
                    $('#idFornecedor').val(fornecedor);

                });
            });
        </script>
    </div>
</div>