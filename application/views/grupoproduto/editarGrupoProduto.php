<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Grupo de produto<small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formGrupoProduto" method="post" class="form-horizontal">
                    <?php echo form_hidden('idGrupoProduto', $result->idGrupoProduto) ?>
                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" class="form-control" value="<?php echo $result->nome ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Observação</label>
                        <div class="controls">
                            <textarea id="descricao" class="form-control" name="descricao"><?php echo $result->descricao ?></textarea>
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Alterar</button>
                            <a href="<?php echo base_url() ?>index.php/grupoproduto" id="btnAdicionar" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formGrupoProduto').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>