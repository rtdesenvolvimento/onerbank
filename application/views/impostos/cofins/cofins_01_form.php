<script>
    function SomaAliqCOFINS(valor) {
        var ValorBc = $("#vBCCOFINS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vCOFINS").val(numeroParaMoeda(total));
    }

</script>

<div class="row">
    <div class="col-md-4">
        <label>Base Cálc. <span class="text-danger">*</span></label>
        <input name="vBCCOFINS" id="vBCCOFINS" onkeyup="SomaAliqCOFINS(document.getElementById('pCOFINS').value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Alíquota(%). <span class="text-danger">*</span></label>
        <input name="pCOFINS" id="pCOFINS" onkeypress="mascara(this, mvalor);" onkeyup="SomaAliqCOFINS(this.value)" type="text" required="" class="form-control" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Valor. <span class="text-danger">*</span></label>
        <input name="vCOFINS" id="vCOFINS" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control" placeholder=""> 
    </div>
</div>
