<script>
    function SomaAliqCOFINS(valor) {
        var ValorBc = $("#vBCCOFINS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vCOFINS").val(numeroParaMoeda(total));
    }

    function SomaTotalCOFINS(valor) {
        var vlrunitario = $("#vAliqProdCOFINS").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vCOFINS").val(numeroParaMoeda(total));
    }
</script>

<div class="row">
    <div class="col-md-4">
        <label>QTD. Vendida <span class="text-danger">*</span></label>
        <input name="qBCProdCOFINS" value="0" id="qBCProdCOFINS"  onkeyup="SomaTotalCOFINS(this.value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control valor" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Valor Un. <span class="text-danger">*</span></label>
        <input name="vAliqProdCOFINS" value="0.00" id="vAliqProdCOFINS"  onkeyup="SomaTotalCOFINS(document.getElementById('qBCProdCOFINS').value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control valor" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Valor. <span class="text-danger">*</span></label>
        <input name="vCOFINS" id="vCOFINS" value="0.00" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control" placeholder=""> 
    </div>
</div>
