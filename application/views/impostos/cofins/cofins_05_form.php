<script>
    function TipoCOFINS(valor) {
        if (valor == 'Valor') {
            $("#valorcofins").css("display", "block");
            $("#percentualcofins").css("display", "none");
            $(".percentualcofins").val('');

        } else if (valor == 'Percentual') {
            $("#percentualcofins").css("display", "block");
            $("#valorcofins").css("display", "none");
            $(".valorcofins").val('');

        } else {
            $("#valorcofins").css("display", "none");
            $("#percentualcofins").css("display", "none");
            $(".percentualcofins").val('');
            $(".valorcofins").val('');
        }
    }

    function SomaAliqCOFINS(valor) {
        var ValorBc = $("#vBCCOFINS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vCOFINS").val(numeroParaMoeda(total));
    }

    function SomaTotalCOFINS(valor) {
        var vlrunitario = $("#vAliqProdCOFINS").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vCOFINS").val(numeroParaMoeda(total));
    }
</script>
<div class="row">
    <div class="col-md-3">
        <label>Tipo Cálc. <span class="text-danger">*</span></label>
        <select onchange="TipoCOFINS(this.value)" class="form-control">
            <option></option>
            <option value="Percentual">% (Percent) </option>
            <option value="Valor">R$ (Valor) </option>
        </select>
    </div>

    <div id="percentualcofins" style="display: none;">
        <div class="col-md-3">
            <label>Valor Base. <span class="text-danger">*</span></label>
            <input name="vBCCOFINS" id="vBCCOFINS" onkeyup="SomaAliqCOFINS(document.getElementById('pCOFINS').value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control percentualcofins" placeholder=""> 
        </div>

        <div class="col-md-3">
            <label>Alíquota(R$). <span class="text-danger">*</span></label>
            <input name="pCOFINS" id="pCOFINS" onkeyup="SomaAliqCOFINS(this.value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control percentualcofins" placeholder=""> 
        </div>
    </div>

    <div id="valorcofins" style="display: none;">
        <div class="col-md-3">
            <label>QTD. Vendida <span class="text-danger">*</span></label>
            <input name="qBCProdCOFINS" id="qBCProdCOFINS" onkeyup="SomaTotalCOFINS(this.value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control valorcofins" placeholder=""> 
        </div>

        <div class="col-md-3">
            <label>Alíquota(%). <span class="text-danger">*</span></label>
            <input name="vAliqProdCOFINS" id="vAliqProdCOFINS" onkeyup="SomaTotalCOFINS(document.getElementById('qBCProdCOFINS').value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control valorcofins" placeholder=""> 
        </div>
    </div>



    <div class="col-md-3">
        <label>Valor <span class="text-danger">*</span></label>
        <input name="vCOFINS" id="vCOFINS" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control percentualcofins valorcofins" placeholder=""> 
    </div>



</div>
