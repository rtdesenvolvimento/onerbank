<script>
    function TipoCOFINS(valor) {
        if (valor == 'Valor') {
            $("#valorpis").css("display", "block");
            $("#percentualpis").css("display", "none");
            $(".percentualpis").val('0.00');

        } else if (valor == 'Percentual') {
            $("#percentualpis").css("display", "block");
            $("#valorpis").css("display", "none");
            $(".valorpis").val('0.00');

        } else {
            $("#valorpis").css("display", "none");
            $("#percentualpis").css("display", "none");
            $(".percentualpis").val('0.00');
            $(".valorpis").val('0.00');
        }
    }

    function SomaAliqCOFINS(valor) {
        var ValorBc = $("#vBCCOFINS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vCOFINS").val(numeroParaMoeda(total));
    }

    function SomaTotalCOFINS(valor) {
        var vlrunitario = $("#vAliqProdCOFINS").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vCOFINS").val(numeroParaMoeda(total));
    }
</script>
<div class="row">
    <div class="col-md-3">
        <label>Tipo Cálc. <span class="text-danger">*</span></label>
        <select onchange="TipoCOFINS(this.value)" class="form-control">
            <option></option>
            <option value="Percentual">% (Percentual) </option>
            <option value="Valor">R$ (Valor) </option>
        </select>
    </div>

    <div id="percentualpis" style="display: none;">
        <div class="col-md-3">
            <label>Valor Base. <span class="text-danger">*</span></label>
            <input name="vBCCOFINS" value="0.00" id="vBCCOFINS" onkeyup="SomaAliqCOFINS(document.getElementById('pCOFINS').value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control percentualpis" placeholder=""> 
        </div>

        <div class="col-md-3">
            <label>Alíquota(%). <span class="text-danger">*</span></label>
            <input name="pCOFINS" value="0.00" id="pCOFINS" onkeyup="SomaAliqCOFINS(this.value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control percentualpis" placeholder=""> 
        </div>
    </div>

    <div id="valorpis" style="display: none;">
       <div class="col-md-3">
        <label>QTD. Vendida <span class="text-danger">*</span></label>
        <input name="qBCProdCOFINS" value="0.00" id="qBCProdCOFINS"  onkeyup="SomaTotalCOFINS(this.value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control valorpis" placeholder=""> 
    </div>

    <div class="col-md-3">
        <label>Valor Un. <span class="text-danger">*</span></label>
        <input name="vAliqProdCOFINS" value="0.00" id="vAliqProdCOFINS"  onkeyup="SomaTotalCOFINS(document.getElementById('qBCProdCOFINS').value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control valopisr" placeholder=""> 
    </div>
    </div>



    <div class="col-md-3">
        <label>Valor. <span class="text-danger">*</span></label>
        <input name="vCOFINS" id="vCOFINS" value="0.00" onkeypress="mascara(this, mvalor);" type="text" class="form-control" placeholder=""> 
    </div>



</div>
