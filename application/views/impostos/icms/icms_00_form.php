<script>
    $(document).ready(function() {
        $(".money").maskMoney();
    });
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span12">
        <label>Modalid. de determ. da BC ICMS ST <span class="text-danger">*</span></label>
        <select name="modBC" class="span12">
            <option value="0">Margem Valor Agregado (%); </option>
            <option value="1">Pauta (Valor) </option>
            <option value="2">Preço Tabelado Máx. (valor) </option>
            <option value="3">Valor da operação</option>
        </select> 
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label>Base Cal. <span class="text-danger">*</span></label>
        <input name="vBC" id="vBC" onkeyup="SomaAliq(document.getElementById('pICMS').value)" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Aliq ICMS. <span class="text-danger">*</span></label>
        <input name="pICMS" id="pICMS" onkeyup="SomaAliq(this.value)" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Valor ICMS. <span class="text-danger">*</span></label>
        <input name="vICMS" id="vICMS" value="0.00" type="text" class="span12 money">
    </div>
</div>

<script>
    $("#vBC").val($("#vlrtotal").val());

    function SomaAliq(valor) {
        var ValorBc = $("#vBC").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * moedaParaNumero(valorAliq);
        $("#vICMS").val(total.toFixed(2));
    }

    function SomaAliqST(valor) {
        var ValorBc = $("#vBCST").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * moedaParaNumero(valorAliq);
        $("#vICMSST").val(numeroParaMoeda(total));
    }

    var intervalo = window.setInterval(function () {
          SomaAliq($("#pICMS").val());
    }, 1000);

    window.setTimeout(function () {
        clearInterval(intervalo);
    }, 4000);

</script>