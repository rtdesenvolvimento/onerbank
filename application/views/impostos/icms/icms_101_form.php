<script>
    $(document).ready(function() {
        $(".money").maskMoney();
    });
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label title="Alíquota aplicável de cálculo do crédito">Aliq. Aplicável <span class="text-danger">*</span></label>
        <input name="pCredSN" type="text" class="span12 money">
    </div>
    
    <div class="span6">
        <label title="Valor crédito do ICMS que pode ser aproveitado nos termos do art. 23 da LC">Valor crédito do ICMS <span class="text-danger">*</span></label>
        <input name="vCredICMSSN" type="text" class="span12 money">
    </div>
</div>