<script>
    $(document).ready(function() {
        $(".money").maskMoney();
    });
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label>Modalid. de determ. da BC ICMS<span class="text-danger">*</span></label>
        <select name="modBC" class="span12">
            <option value="0"> Margem Valor Agregado (%); </option>
            <option value="1"> Pauta (Valor) </option>
            <option value="2"> Preço Tabelado Máx. (valor) </option>
            <option value="3"> Valor da operação</option>
        </select> 
    </div>

    <div class="span2">
        <label>Aliq ICMS. <span class="text-danger">*</span></label>
        <input name="pICMS" type="text" class="span12 money">
    </div>
    
    <div class="span3">
        <label>% Red. BC ICMS. <span class="text-danger">*</span></label>
        <input name="pRedBC" type="text" class="span12 money">
    </div>
    <div class="span3">
        <label>% BC Op. Prop. <span class="text-danger">*</span></label>
        <input name="pBCOP" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span5">
        <label>Modalid. de determ. da BC ICMS ST <span class="text-danger">*</span></label>
        <select name="modBCST" class="span12">
            <option value="0"> Preço tabelado ou máximo sugerido; </option>
            <option value="1"> Lista Negativa (valor) </option>
            <option value="2"> Lista Positiva (valor) </option>
            <option value="3"> Lista Neutra (valor)</option>
            <option value="4"> Margem Valor Agregado (%)</option>
            <option value="5"> Pauta (valor)</option>
        </select> 
    </div>

    <div class="span4">
        <label>% Red. BC ICMS ST <span class="text-danger">*</span></label>
        <input name="pRedBCST" type="text" class="span12 money">
    </div>

    <div class="span3">
        <label>% MVA ICMS ST<span class="text-danger">*</span></label>
        <input name="pMVAST" type="text" class="span12 money">
    </div>

</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label>Aliq ICMS ST. <span class="text-danger">*</span></label>
        <input name="pICMSST" type="text" class="span12 money">
    </div>

    <div class="span5">
        <label>UF do ICMS ST devido na op. <span class="text-danger">*</span></label>
        <select name="modUFST" class="span12">
            <option value="AC">Acre</option> 
            <option value="AL">Alagoas</option> 
            <option value="AM">Amazonas</option> 
            <option value="AP">Amapá</option> 
            <option value="BA">Bahia</option> 
            <option value="CE">Ceará</option> 
            <option value="DF">Distrito Federal</option> 
            <option value="ES">Espírito Santo</option> 
            <option value="GO">Goiás</option> 
            <option value="MA">Maranhão</option> 
            <option value="MT">Mato Grosso</option> 
            <option value="MS">Mato Grosso do Sul</option> 
            <option value="MG">Minas Gerais</option> 
            <option value="PA">Pará</option> 
            <option value="PB">Paraíba</option> 
            <option value="PR">Paraná</option> 
            <option value="PE">Pernambuco</option> 
            <option value="PI">Piauí</option> 
            <option value="RJ">Rio de Janeiro</option> 
            <option value="RN">Rio Grande do Norte</option> 
            <option value="RO">Rondônia</option> 
            <option value="RS">Rio Grande do Sul</option> 
            <option value="RR">Roraima</option> 
            <option value="SC">Santa Catarina</option> 
            <option value="SE">Sergipe</option> 
            <option value="SP">São Paulo</option> 
            <option value="TO">Tocantins</option> 
        </select> 
    </div>
</div>