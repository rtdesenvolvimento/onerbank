<script>
    $(document).ready(function() {
        $(".money").maskMoney();
    });
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label title="Alíquota aplicável de cálculo do crédito">Aliq. Aplicável <span class="text-danger">*</span></label>
        <input name="pCredSN" type="text" class="span12 money">
    </div>

    <div class="span6">
        <label title="Valor crédito do ICMS que pode ser aproveitado nos termos do art. 23 da LC">Valor crédito do ICMS <span class="text-danger">*</span></label>
        <input name="vCredICMSSN" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label title="Modalide de determ. da BC ICMS ST">Determ. da BC ICMS ST <span class="text-danger">*</span></label>
        <select name="modBCST" class="span12">
            <option value="0"> Preço tabelado ou máximo sugerido; </option>
            <option value="1"> Lista Negativa (valor) </option>
            <option value="2"> Lista Positiva (valor) </option>
            <option value="3"> Lista Neutra (valor)</option>
            <option value="4"> Margem Valor Agregado (%)</option>
            <option value="5"> Pauta (valor)</option>
        </select> 
    </div>

    <div class="span4">
        <label>% Red. BC ICMS ST </label>
        <input name="pRedBCST" type="text" class="span12 money">
    </div>
    
     <div class="span4">
        <label>% MVA ICMS ST</label>
        <input name="pMVAST" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
   
    <div class="span4">
        <label>Base Cal ST. <span class="text-danger">*</span></label>
        <input name="vBCST" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Aliq ICMS ST <span class="text-danger">*</span></label>
        <input name="pICMSST" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Valor ICMS. <span class="text-danger">*</span></label>
        <input name="vICMSST" value="0.00" type="text" class="span12 money">
    </div>
</div>