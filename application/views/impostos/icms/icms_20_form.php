<script>

    $(document).ready(function() {
        $("#vBC").val($("#vlrtotal").val());
        $(".money").maskMoney();
    });

    function SomaAliq(valor) {
        var ValorBc = $("#vBC").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vICMS").val(numeroParaMoeda(total));
    }
    
    function SomaAliqST(valor) {
        var ValorBc = $("#vBCST").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vICMSST").val(numeroParaMoeda(total));
    }
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label title="Modalide de determ. da BC ICMS ST">Determ. da BC ICMS ST <span class="text-danger">*</span></label>
        <select name="modBC" class="span12">
            <option value="0"> Margem Valor Agregado (%); </option>
            <option value="1"> Pauta (Valor) </option>
            <option value="2"> Preço Tabelado Máx. (valor) </option>
            <option value="3"> Valor da operação</option>

        </select> 
    </div>

    <div class="span3">
        <label>Base Cal. <span class="text-danger">*</span></label>
        <input name="vBC" id="vBC" onkeyup="SomaAliq(document.getElementById('pICMS').value)" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span2">
        <label>Aliq ICMS <span class="text-danger">*</span></label>
        <input name="pICMS" id="pICMS" onkeyup="SomaAliq(this.value)" type="text" class="span12 money">
    </div>

    <div class="span3">
        <label>Valor ICMS. <span class="text-danger">*</span></label>
        <input name="vICMS" id="vICMS" value="0.00" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span3">
        <label>% Red. BC ICMS. <span class="text-danger">*</span></label>
        <input name="pRedBC" type="text" class="span12 money">
    </div>
    
     <div class="span3">
        <label>Valor ICMS Deson</label>
        <input name="vICMSDeson" value="0.00"  type="text" class="span12 money">
    </div>

    <div class="span6">
        <label>Motivo da Desoneração do ICMS</label>
        <select name="motDesICMS" class="span12">
            <option value="0"> </option>
            <option value="1"> Táxi </option>
            <option value="3"> Produtor Agropecuário </option>
            <option value="4"> Frotista/Locadora </option>
            <option value="5"> Diplomático/Consular</option>
            <option value="6"> Utilitários e Motocicletas da Amazônia Ocidental e Áreas de Livre Comércio (Resolução 714/88 e 790/94 – CONTRAN e suas alterações)</option>
            <option value="7"> SUFRAMA</option>
            <option value="8"> Venda a Órgão Público</option>
            <option value="9"> Outros. (NT 2011/004)</option>
            <option value="10"> Deficiente Condutor (Convênio ICMS 38/12)</option>
            <option value="11"> Deficiente Não Condutor (Convênio ICMS 38/12)</option>
        </select> 
    </div>
</div>