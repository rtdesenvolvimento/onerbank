<script>
    $(document).ready(function() {
        $("#vBCSTRet").val($("#vlrtotal").val());
        $(".money").maskMoney();
    });

    function somaAliq(valor) {
        var ValorBc = $("#vBCSTRet").val();
        var valorAliq = valor / 100;
        var total = parseFloat(ValorBc) * valorAliq;
        $("#vICMSSTRet").val(parseFloat(total).toFixed(2));
    }
</script>
<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label>Base Cal.</label>
        <input name="vBCSTRet" id="vBCSTRet" type="text" class="money">
    </div>

    <div class="span4">
        <label>Alíquota</label>
        <input name="pST" id="pST" onkeyup="somaAliq(this.value)" type="text" class="money">
    </div>

    <div class="span4">
        <label>Valor da BC do ICMS ST retido<span class="text-danger">*</span></label>
        <input name="vICMSSTRet" id="vICMSSTRet" onkeyup="somaAliq(document.getElementById('pST').value)" value="0,00" type="text" class="money">
    </div>
</div>


