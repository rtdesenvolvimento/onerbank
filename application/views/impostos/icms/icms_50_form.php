<script>

    $(document).ready(function() {
        $(".money").maskMoney();
    });
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label>Valor ICMS Deson</label>
        <input name="vICMSDeson" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span6">
        <label>Motivo da Desoneração do ICMS</label>
        <select name="motDesICMS" class="span12">
            <option value="0"> </option>
            <option value="1"> Táxi </option>
            <option value="3"> Produtor Agropecuário </option>
            <option value="4"> Frotista/Locadora </option>
            <option value="5"> Diplomático/Consular</option>
            <option value="6"> Utilitários e Motocicletas da Amazônia Ocidental e Áreas de Livre Comércio (Resolução 714/88 e 790/94 – CONTRAN e suas alterações)</option>
            <option value="7"> SUFRAMA</option>
            <option value="8"> Venda a Órgão Público</option>
            <option value="9"> Outros. (NT 2011/004)</option>
            <option value="10"> Deficiente Condutor (Convênio ICMS 38/12)</option>
            <option value="11"> Deficiente Não Condutor (Convênio ICMS 38/12)</option>
        </select> 
    </div>
</div>
