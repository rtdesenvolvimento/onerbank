<script>

    $(document).ready(function() {
        $(".money").maskMoney();
    });

    function SomaAliq(valor) {
        var ValorBc = $("#vBC").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vICMS").val(numeroParaMoeda(total));
    }
    
    function SomaAliqST(valor) {
        var ValorBc = $("#vBCST").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vICMSST").val(numeroParaMoeda(total));
    }
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label title="Modalide de determ. da BC ICMS ST">Determ. da BC ICMS ST <span class="text-danger">*</span></label>
        <select name="modBC" class="span12">
            <option value="9">  </option>
            <option value="0"> Margem Valor Agregado (%); </option>
            <option value="1"> Pauta (Valor) </option>
            <option value="2"> Preço Tabelado Máx. (valor) </option>
            <option value="3"> Valor da operação</option>

        </select> 
    </div>
    <div class="span6">
        <label>% Red. BC ICMS </label>
        <input name="pRedBC" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">

    <div class="span4">
        <label>Base Cal. <span class="text-danger">*</span></label>
        <input name="vBC" id="vBC" onkeyup="SomaAliq(document.getElementById('pICMS').value)" value="0.00" type="text" class="span12 money">
    </div>
    
    <div class="span4">
        <label>Aliq ICMS. <span class="text-danger">*</span></label>
        <input name="pICMS" id="pICMS" onkeyup="SomaAliq(this.value)" type="text"class="span12 money">
    </div>
 

    <div class="span4">
        <label>Valor ICMS Op. <span class="text-danger">*</span></label>
        <input name="vICMSOp" value="0.00" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">

    <div class="span4">
        <label>Percentual Dif. <span class="text-danger">*</span></label>
        <input name="pDif" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Valor ICMS Dif. <span class="text-danger">*</span></label>
        <input name="vICMSDif" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Valor ICMS <span class="text-danger">*</span></label>
        <input name="vICMS" value="0.00" type="text" id="vICMS" class="span12 money">
    </div>
</div>