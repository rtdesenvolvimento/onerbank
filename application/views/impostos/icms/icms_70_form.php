<script>
    $(document).ready(function() {
        $(".money").maskMoney();
    });
</script>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label title="Modalidade de determ. da BC ICMS">Determ. da BC ICMS <span class="text-danger">*</span></label>
        <select name="modBC" class="span12">
            <option value="0">Margem Valor Agregado (%); </option>
            <option value="1">Pauta (Valor) </option>
            <option value="2">Preço Tabelado Máx. (valor) </option>
            <option value="3">Valor da operação</option>
        </select> 
    </div>

    <div class="span2">
        <label>%Red ICMS </label>
        <input name="pRedBC" type="text" class="span12 money">
    </div>

    <div class="span2">
        <label>Base Cal. <span class="text-danger">*</span></label>
        <input name="vBC" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span2">
        <label>Aliq ICMS <span class="text-danger">*</span></label>
        <input name="pICMS" type="text" required="" class="span12 money">
    </div>

    <div class="span2">
        <label>Val. ICMS <span class="text-danger">*</span></label>
        <input name="vICMS" value="0.00" type="text" class="span12 money">
    </div>

</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label title="Modalide de determ. da BC ICMS ST">Determ. da BC ICMS ST <span class="text-danger">*</span></label>
        <select name="modBCST" class="span12">
            <option value="0">Preço tabelado ou máximo sugerido; </option>
            <option value="1">Lista Negativa (valor) </option>
            <option value="2">Lista Positiva (valor) </option>
            <option value="3">Lista Neutra (valor)</option>
            <option value="4">Margem Valor Agregado (%)</option>
            <option value="5">Pauta (valor)</option>
        </select> 
    </div>

    <div class="span4">
        <label>% Red. ICMS ST</label>
        <input name="pRedBCST" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>% MVA ICMS ST</label>
        <input name="pMVAST" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label>Base Cal ST. <span class="text-danger">*</span></label>
        <input name="vBCST" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Aliq ICMS ST <span class="text-danger">*</span></label>
        <input name="pICMSST" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label> ICMS. <span class="text-danger">*</span></label>
        <input name="vICMSST" value="0.00" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label>Valor ICMS Deson</label>
        <input name="vICMSDeson" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span6">
        <label>Motivo da Desoneração do ICMS</label>
        <select name="motDesICMS" class="span12">
            <option value=""></option>
            <option value="3">Uso na agropecuária </option>
            <option value="9">Outros</option>
            <option value="12">Órgão de fomento e desenvolvimento agropecuário</option>
        </select> 
    </div>

</div>