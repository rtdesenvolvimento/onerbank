<script>

    $(document).ready(function() {
        $("#vBC").val($("#vlrtotal").val());
        $(".money").maskMoney();
    });

    function SomaAliq(valor) {
        var ValorBc = $("#vBC").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vICMS").val(numeroParaMoeda(total));
    }
    
    function SomaAliqST(valor) {
        var ValorBc = $("#vBCST").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
        $("#vICMSST").val(numeroParaMoeda(total));
    }
</script>


<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label title="Alíquota aplicável de cálculo do crédito">Aliq. Aplicável </label>
        <input name="pCredSN" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span6">
        <label title="Valor crédito do ICMS que pode ser aproveitado nos termos do art. 23 da LC">Valor crédito do ICMS </label>
        <input name="vCredICMSSN" value="0.00" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label title="Modalidade de determ. da BC ICMS">Determ. da BC ICMS </label>
        <select name="modBC" class="span12">
            <option value="0">Margem Valor Agregado (%); </option>
            <option value="1">Pauta (Valor) </option>
            <option value="2">Preço Tabelado Máx. (valor) </option>
            <option value="3">Valor da operação</option>
        </select>
    </div>

    <div class="span3">
        <label>Base Cal. </label>
        <input name="vBC" id="vBC" onkeyup="SomaAliq(document.getElementById('pICMS').value)" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span3">
        <label>Aliq ICMS </label>
        <input name="pICMS" id="pICMS" onkeyup="SomaAliq(this.value)" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span2">
        <label>Valor ICMS. </label>
        <input name="vICMS" id="vICMS" value="0.00" type="text" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span6">
        <label title="Modalide de determ. da BC ICMS ST">Determ. da BC ICMS ST </label>
        <select name="modBCST" class="span12">
            <option value="0">Preço tabelado ou máximo sugerido; </option>
            <option value="1">Lista Negativa (valor) </option>
            <option value="2">Lista Positiva (valor) </option>
            <option value="3">Lista Neutra (valor)</option>
            <option value="4">Margem Valor Agregado (%)</option>
            <option value="5">Pauta (valor)</option>

        </select>
    </div>

    <div class="span3">
        <label>% Red. BC ICMS ST</label>
        <input name="pRedBCST" type="text" value="0.00" class="span12 money">
    </div>


    <div class="span3">
        <label>% MVA ICMS ST</label>
        <input name="pMVAST" type="text" value="0.00" class="span12 money">
    </div>
</div>

<div class="span12" style="padding: 1%; margin-left: 0">
    <div class="span4">
        <label>Base Cal ST.</label>
        <input name="vBCST" id="vBCST" onkeyup="SomaAliqST(document.getElementById('pICMSST').value)" value="0.00" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Aliq ICMS ST</label>
        <input name="pICMSST" id="pICMSST" value="0.00" onkeyup="SomaAliqST(this.value)" type="text" class="span12 money">
    </div>

    <div class="span4">
        <label>Valor ICMS. </label>
        <input name="vICMSST" id="vICMSST" value="0.00" type="text" class="span12 money">
    </div>
</div>
