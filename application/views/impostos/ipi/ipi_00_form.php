<script>

    function SomaAliqIPI(valor) {
        var ValorBc = $("#vBCIPI").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vIPI").val(numeroParaMoeda(total));
    }
    
    function SomaTotalIPI(valor) {
       var vlrunitario = $("#vUnidIPI").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vIPI").val(numeroParaMoeda(total));
    }


    function TipoIPI(valor) {
        if (valor == 'Valor') {
            $("#valoripi").css("display", "block");
            $("#percentualipi").css("display", "none");
            $(".percentualipi").val('0.00');

        } else if (valor == 'Percentual') {
            $("#percentualipi").css("display", "block");
            $("#valoripi").css("display", "none");
            $(".valoripi").val('0.00');

        } else {
            $("#valoripi").css("display", "none");
            $("#percentualipi").css("display", "none");
            $(".percentualipi").val('0.00');
            $(".valoripi").val('0.00');
        }
    }
</script>

<div class="row">
    <div class="col-md-4">
        <label title="Classe de Enquadramento">Classe Enq.</label>
        <input type="text" name="clEnq" class="form-control">
    </div>

    <div class="col-md-4">
        <label title="Código de Enquadramento">Código Enq. <span class="text-danger">*</span></label>
        <input name="cEnq" type="text" required="" class="form-control" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label title="CNPJ do Produtor">CNPJ Produtor</label>
        <input name="CNPJProd" type="text" class="form-control" placeholder=""> 
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label title="Código do Selo de controle">Código Selo</label>
        <input type="text" name="cSelo" class="form-control">
    </div>
    <div class="col-md-2">
        <label title="Quantidade do Selo de controle">QTD. Selo</label>
        <input type="text"value="0" name="qSelo" class="form-control">
    </div>
    <div class="col-md-4">
        <label  title="Tipo de Cálculo">Tipo Cálculo <span class="text-danger">*</span></label>
        <select onchange="TipoIPI(this.value)" required="" class="form-control">
            <option></option>
            <option>Percentual</option>
            <option>Valor</option>
        </select>
    </div>
</div>

<div class="row">
    <div id="percentualipi" style="display: none">
        <div class="col-md-4">
            <label title="Valor da Base de Cálculo">Base de Cálculo</label>
            <input type="text" name="vBCIPI" id="vBCIPI" onkeyup="SomaAliqIPI(document.getElementById('pIPI').value)" value="0.00" onkeypress="mascara(this, mvalor);" class="form-control percentualipi">
        </div>
        <div class="col-md-4">
            <label title="Alíquota">Alíquota</label>
            <input type="text" name="pIPI" id="pIPI" onkeyup="SomaAliqIPI(this.value)" value="0.00" onkeypress="mascara(this, mvalor);" class="form-control percentualipi">
        </div>
    </div>

    <div id="valoripi" style="display: none">
        <div class="col-md-4">
            <label title="Quantidade Total unidade padrão">QTD</label>
            <input type="text" name="qUnidIPI" id="qUnidIPI" onkeyup="SomaTotalIPI(this.value)" value="0" class="form-control valoripi">
        </div>
        <div class="col-md-4">
            <label title="Valor por Unidade">Valor Un</label>
            <input type="text" name="vUnidIPI" id="vUnidIPI" onkeyup="SomaTotalIPI(document.getElementById('qUnidIPI').value)" value="0.00" onkeypress="mascara(this, mvalor);" class="form-control valoripi">
        </div>
    </div>

    <div class="col-md-4">
        <label title="Valor do IPI">Valor IPI</label>
        <input type="text" name="vIPI" id="vIPI" value="0.00" onkeypress="mascara(this, mvalor);" class="form-control valoripi percentualipi">
    </div>

</div>

<!--onkeypress="mascara(this, mvalor);"--> 