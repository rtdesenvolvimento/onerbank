<script>

    function SomaAliqIPI(valor) {
        var ValorBc = $("#vBCIPI").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vIPI").val(numeroParaMoeda(total));
    }
    
    function SomaTotalIPI(valor) {
       var vlrunitario = $("#vUnidIPI").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vIPI").val(numeroParaMoeda(total));
    }


    function TipoIPI(valor) {
        if (valor == 'Valor') {
            $("#valoripi").css("display", "block");
            $("#percentualipi").css("display", "none");
            $(".percentualipi").val('0.00');

        } else if (valor == 'Percentual') {
            $("#percentualipi").css("display", "block");
            $("#valoripi").css("display", "none");
            $(".valoripi").val('0.00');

        } else {
            $("#valoripi").css("display", "none");
            $("#percentualipi").css("display", "none");
            $(".percentualipi").val('0.00');
            $(".valoripi").val('0.00');
        }
    }
</script>

<div class="row">
    <div class="col-md-4">
        <label title="Classe de Enquadramento">Classe Enq.</label>
        <input type="text" name="clEnq" class="form-control">
    </div>

    <div class="col-md-4">
        <label title="Código de Enquadramento">Código Enq. <span class="text-danger">*</span></label>
        <input name="cEnq" type="text" required="" class="form-control" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label title="CNPJ do Produtor">CNPJ Produtor</label>
        <input name="CNPJProd" type="text" class="form-control" placeholder=""> 
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label title="Código do Selo de controle">Código Selo</label>
        <input type="text" name="cSelo" class="form-control">
    </div>
    <div class="col-md-2">
        <label title="Quantidade do Selo de controle">QTD. Selo</label>
        <input type="text"value="0" name="qSelo" class="form-control">
    </div> 
</div>


<!--onkeypress="mascara(this, mvalor);"--> 