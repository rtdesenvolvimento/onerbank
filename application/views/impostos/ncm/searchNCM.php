<form  action="<?php echo base_url(); ?>index.php/impostos/searchNCM" id="formSearch" method="post">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Consulta de Nomenclatura Comum do Mercosul (NCM)</h3>
    </div>

    <div class="span12" style="padding: 1%; margin-left: 0">

        <div class="span12" style="margin-left: 0">
            <div class="span8" style="margin-left: 0">
                <input type="text" class="span12" name="termo" value="<?php echo $termo;?>" placeholder="Digite o termo a pesquisar"/>
            </div>
            <div class="span4">
                <button class="span12 btn"><i class=" icon-search"></i> Pesquisar</button>
            </div>
        </div>
    </div>

    <div class="modal-body">

        <div class="span12" style="margin-left: 0">
            <table class="table table-bordered" style="cursor: hand;">
                <thead class="thead_class">
                <tr>
                    <th style="text-align: left;width: 100px;">Código</th>
                    <th style="text-align: left;width: 650px;">Descrição</th>
                </tr>
                </thead>
                <tbody class="tbody_class">
                <?php foreach ($results as $r) {

                    echo '<tr class="searchPreenche" cod_ncm='.$r->cod_ncm.' style="cursor: pointer;">';
                    echo '    <td style="text-align: left;width: 100px;cursor: pointer;">' . $r->cod_ncm . '</td>';
                    echo '    <td style="text-align: left;width: 650px;cursor: pointer;">' . $r->nome_ncm . '</td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>

            <?php echo $this->pagination->create_links();?>
        </div>
    </div>
</form>

<script>

    $('.create_links').click(function (event) {
        var url = $(this).attr('href');
        if (url != undefined) {
            $('#div_searchNCM').load(url);
        }
        return false;
    });

    $('#formSearch').submit(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/impostos/searchNCM",
            data: $("#formSearch").serialize(),
            dataType: 'html',
            success: function (html) {
                $('#div_searchNCM').html(html);
            }
        });
    });

    $('.searchPreenche').click(function (event) {
        var cod_ncm = $(this).attr('cod_ncm');
        fecharModelSerchNCM(cod_ncm);
    });

</script>