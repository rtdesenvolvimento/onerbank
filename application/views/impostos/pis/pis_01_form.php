<script>
    function SomaAliqPIS(valor) {
        var ValorBc = $("#vBCPIS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vPIS").val(numeroParaMoeda(total));
    }

</script>

<div class="row">
    <div class="col-md-4">
        <label>Base Cálc. <span class="text-danger">*</span></label>
        <input name="vBCPIS" id="vBCPIS" onkeyup="SomaAliqPIS(document.getElementById('pPIS').value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Alíquota(%). <span class="text-danger">*</span></label>
        <input name="pPIS" id="pPIS" onkeypress="mascara(this, mvalor);" onkeyup="SomaAliqPIS(this.value)" type="text" required="" class="form-control" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Valor. <span class="text-danger">*</span></label>
        <input name="vPIS" id="vPIS" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control" placeholder=""> 
    </div>
</div>
