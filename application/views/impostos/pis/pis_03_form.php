<script>
    function SomaAliqPIS(valor) {
        var ValorBc = $("#vBCPIS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vPIS").val(numeroParaMoeda(total));
    }

    function SomaTotalPIS(valor) {
        var vlrunitario = $("#vAliqProdPIS").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vPIS").val(numeroParaMoeda(total));
    }
</script>

<div class="row">
    <div class="col-md-4">
        <label>QTD. Vendida <span class="text-danger">*</span></label>
        <input name="qBCProdPIS" value="0" id="qBCProdPIS"  onkeyup="SomaTotalPIS(this.value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control valor" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Valor Un. <span class="text-danger">*</span></label>
        <input name="vAliqProdPIS" value="0.00" id="vAliqProdPIS"  onkeyup="SomaTotalPIS(document.getElementById('qBCProdPIS').value)" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control valor" placeholder=""> 
    </div>

    <div class="col-md-4">
        <label>Valor. <span class="text-danger">*</span></label>
        <input name="vPIS" id="vPIS" value="0.00" onkeypress="mascara(this, mvalor);" type="text" required="" class="form-control" placeholder=""> 
    </div>
</div>
