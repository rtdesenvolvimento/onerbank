<script>
    function TipoPIS(valor) {
        if (valor == 'Valor') {
            $("#valorpis").css("display", "block");
            $("#percentualpis").css("display", "none");
            $(".percentualpis").val('0.00');

        } else if (valor == 'Percentual') {
            $("#percentualpis").css("display", "block");
            $("#valorpis").css("display", "none");
            $(".valorpis").val('0.00');

        } else {
            $("#valorpis").css("display", "none");
            $("#percentualpis").css("display", "none");
            $(".percentualpis").val('0.00');
            $(".valorpis").val('0.00');
        }
    }

    function SomaAliqPIS(valor) {
        var ValorBc = $("#vBCPIS").val();
        var valorAliq = valor / 100;
        var total = moedaParaNumero(ValorBc) * valorAliq;
//        alert(total);
        $("#vPIS").val(numeroParaMoeda(total));
    }

    function SomaTotalPIS(valor) {
        var vlrunitario = $("#vAliqProdPIS").val();
        var total = valor * moedaParaNumero(vlrunitario);
//        alert(total);
        $("#vPIS").val(numeroParaMoeda(total));
    }
</script>
<div class="row">
    <div class="col-md-3">
        <label>Tipo Cálc. <span class="text-danger">*</span></label>
        <select onchange="TipoPIS(this.value)" class="form-control">
            <option></option>
            <option value="Percentual">% (Percentual) </option>
            <option value="Valor">R$ (Valor) </option>
        </select>
    </div>

    <div id="percentualpis" style="display: none;">
        <div class="col-md-3">
            <label>Valor Base. <span class="text-danger">*</span></label>
            <input name="vBCPIS" value="0.00" id="vBCPIS" onkeyup="SomaAliqPIS(document.getElementById('pPIS').value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control percentualpis" placeholder=""> 
        </div>

        <div class="col-md-3">
            <label>Alíquota(%). <span class="text-danger">*</span></label>
            <input name="pPIS" value="0.00" id="pPIS" onkeyup="SomaAliqPIS(this.value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control percentualpis" placeholder=""> 
        </div>
    </div>

    <div id="valorpis" style="display: none;">
       <div class="col-md-3">
        <label>QTD. Vendida <span class="text-danger">*</span></label>
        <input name="qBCProdPIS" value="0.00" id="qBCProdPIS"  onkeyup="SomaTotalPIS(this.value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control valorpis" placeholder=""> 
    </div>

    <div class="col-md-3">
        <label>Valor Un. <span class="text-danger">*</span></label>
        <input name="vAliqProdPIS" value="0.00" id="vAliqProdPIS"  onkeyup="SomaTotalPIS(document.getElementById('qBCProdPIS').value)" onkeypress="mascara(this, mvalor);" type="text" class="form-control valopisr" placeholder=""> 
    </div>
    </div>



    <div class="col-md-3">
        <label>Valor. <span class="text-danger">*</span></label>
        <input name="vPIS" id="vPIS" value="0.00" onkeypress="mascara(this, mvalor);" type="text" class="form-control" placeholder=""> 
    </div>



</div>
