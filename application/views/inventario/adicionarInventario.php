<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de inventário<small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="post" id="formInventario">
                    <div class="span12" style="padding: 1%">
                        <div class="col-md-3 col-sm-3">
                            <label for="dataInventario">Data<span class="required">*</span></label>
                            <input id="dataInventario" class="form-control" type="date" required="required" name="dataInventario" value="<?php echo date('Y-m-d');?>"  />
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <label for="cliente">Origem<span class="required">*</span></label>
                            <div class="controls">
                                <select name="filial_id" readonly class="form-control" id="filial_id" required="required">
                                    <option value="<?php echo $origem->idFilial; ?>"><?php echo $origem->nome; ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="tecnico">Responsável<span class="required">*</span></label>
                            <div class="controls">
                                <select name="usuarios_id" class="form-control" id="usuarios_id" required="required">
                                    <option value="">--Selecione um usuario--</option>
                                    <?php foreach ($usuarios as $usuario) {?>
                                        <option value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="span12" style="text-align: right;">
                        <div class="span6 offset3">
                            <button class="btn btn-success" id="btnContinuar"><i class="fa fa-save"></i> Adicionar</button>
                            <a href="<?php echo base_url() ?>index.php/inventario" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#usuarios_id').val(<?php echo  $this->session->userdata('id');?>);

        $("#formInventario").validate({
            rules:{
                cliente: {required:true},
                tecnico: {required:true},
                dataInventario: {required:true}
            },
            messages:{
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataInventario: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });

</script>

