<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/helpers/jquery-ui-1.10.3.custom.css">
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Inventário:<small>#<?php echo $result->idInventario ?></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 ">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Dados do inventário</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <form action="<?php echo current_url(); ?>" method="post" id="formInventario">
                                    <?php echo form_hidden('idInventario', $result->idInventario) ?>
                                    <div class="row" style="padding: 1%; margin-left: 0">
                                        <div class="col-md-4 col-sm-4">
                                            <label for="cliente">Origem<span class="required">*</span></label>
                                            <div class="controls">
                                                <select name="filial_id" readonly class="form-control" id="filial_id"
                                                        required="required">
                                                    <option value="<?php echo $origem->idFilial; ?>"><?php echo $origem->nome; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label for="status">Status<span class="required">*</span></label>
                                            <select class="form-control" name="status" disabled id="status">
                                                <option <?php if ($result->status == 'Aberto') {
                                                    echo 'selected';
                                                } ?> value="Aberto">Aberto
                                                </option>
                                                <option <?php if ($result->status == 'Confirmado') {
                                                    echo 'selected';
                                                } ?> value="Confirmado">Confirmado
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label for="tecnico">Responsável<span class="required">*</span></label>
                                            <div class="controls">
                                                <select name="usuarios_id" class="form-control" id="usuarios_id"
                                                        required="required">
                                                    <option value="">--Selecione um cliente--</option>
                                                    <?php foreach ($usuarios as $usuario) { ?>
                                                        <option <?php if ($result->usuarios_id == $usuario->idUsuarios) echo 'selected="selected"'; ?>
                                                                value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label for="status">Zerar produtos <small>fora do inventário</small><span
                                                        class="required">*</span></label>
                                            <select class="form-control" name="zerarEstoqueProdutoForaInventario"
                                                    id="zerarEstoqueProdutoForaInventario">
                                                <option <?php if ($result->zerarEstoqueProdutoForaInventario == 'S') {
                                                    echo 'selected';
                                                } ?> value="S">Sim
                                                </option>
                                                <option <?php if ($result->zerarEstoqueProdutoForaInventario == 'N') {
                                                    echo 'selected';
                                                } ?> value="N">Não
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label for="dataInventario">Data</label>
                                            <input id="dataInventario" class="form-control" required="required"
                                                   type="date" name="dataInventario"
                                                   value="<?php echo $result->dataInventario; ?>"/>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label for="numeroLivro">Nº Livro</label>
                                            <input id="numeroLivro" class="form-control" type="text" name="numeroLivro"
                                                   value="<?php echo $result->numeroLivro; ?>"/>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <label for="status">Liberar para contador<span
                                                        class="required">*</span></label>
                                            <select class="form-control" name="liberarContador" id="liberarContador">
                                                <option <?php if ($result->liberarContador == 'S') {
                                                    echo 'selected';
                                                } ?> value="S">Sim
                                                </option>
                                                <option <?php if ($result->liberarContador == 'N') {
                                                    echo 'selected';
                                                } ?> value="N">Não
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="text-align: right;">
                                            <div class="span12">
                                                <button class="btn btn-success" id="btnContinuar"><i
                                                            class="fa fa-save"></i> Alterar
                                                </button>
                                                <?php if ($result->status == 'Aberto') { ?>
                                                    <a href="<?php echo base_url() ?>index.php/inventario/confirmar/<?php echo $result->idInventario; ?>"
                                                       class="btn btn-primary"
                                                       onclick="return verificarCancelamento();"><i
                                                                class="fa fa-check"></i> Confirmar
                                                    </a>
                                                    <a href="<?php echo base_url() ?>index.php/inventario/gerarInventarioEstoqueAtual/<?php echo $result->idInventario; ?>"
                                                       class="btn btn-danger"
                                                       onclick="return verificarGeracaoInventarioEstoqueAtual();"><i
                                                                class="fa fa-location-arrow"></i> Gerar Inventário do
                                                        estoque atual
                                                    </a>
                                                <?php } ?>
                                                <a href="<?php echo base_url() ?>index.php/inventario/relatorioEstoque/<?php echo $result->idInventario; ?>"
                                                   class="btn btn-dark" target="_blank"><i class="fa fa-file-pdf-o"></i>
                                                    Relatório
                                                </a>
                                                <a href="<?php echo base_url() ?>index.php/inventario"
                                                   class="btn btn-primary"><i
                                                            class="fa fa-backward"></i> Voltar</a>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12" style="margin-top: 20px;">
                                            <span style="color: red;">*ATENÇÃO AO CONFIRMAR O IVENTÁRIO O MESMO ATUALIZAR O PREÇO DE COMPRA E PREÇO DE VENDA E O ESTOQUE DE ACORDO COM A LISTA AQUI EXIBIDA.</span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 ">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Itens do invetário</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tablItens" data-toggle="tab">Produtos</a></li>
                                        <li><a href="#tab4" data-toggle="tab">Observações do inventário aqui</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tablItens">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <?php if ($result->status == 'Aberto') { ?>
                                                    <form id="formProdutos"
                                                          action="<?php echo base_url(); ?>index.php/pedido/adicionarProduto"
                                                          method="post">
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-3">
                                                                <input type="hidden" name="idInventario" id="idInventario"
                                                                       value="<?php echo $result->idInventario ?>"/>
                                                                <label for="">Produto</label>
                                                                <div class="controls">
                                                                    <input type="text" name="idProduto" value=""
                                                                           class="form-control pos-tip ui-autocomplete-input"
                                                                           id="idProduto" data-placement="top"
                                                                           data-trigger="focus"
                                                                           placeholder="Scanear/Procurar Produto pelo Nome/Código"
                                                                           title="" autocomplete="off"
                                                                           data-original-title="Por favor, comece a digitar o código/nome para receber sugestões ou apenas scaneie um Cód. de Barra "
                                                                           tabindex="1">
                                                                    <input type="hidden" name="product_id" id="product_id"
                                                                           value=""/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-sm-2">
                                                                <label for="">Ajustar custo (R$)</label>
                                                                <input type="text" placeholder="Custo" id="custo"
                                                                       name="custo"
                                                                       class="form-control"/>
                                                            </div>
                                                            <div class="span2">
                                                                <label for="">Ajustar venda (R$)</label>
                                                                <input type="text" placeholder="Venda" id="venda"
                                                                       name="venda"
                                                                       class="form-control"/>
                                                            </div>
                                                            <div class="col-md-2 col-sm-2">
                                                                <label for="">Tipo</label>
                                                                <div class="controls">
                                                                    <select name="tipo" class="form-control" id="tipo"
                                                                            required="required">
                                                                        <option value="SUBSTITUIR">(~) SUBSTITUIR</option>
                                                                        <option value="ENTRADA">(+) ENTRADA</option>
                                                                        <option value="SAIDA">( - ) SAÍDA</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 col-sm-1">
                                                                <label for="">Estoque</label>
                                                                <input type="text" placeholder="Quantidade"
                                                                       required="required" id="quantidade"
                                                                       name="quantidade" class="form-control" value="1"/>
                                                            </div>
                                                            <div class="col-md-1 col-sm-1">
                                                                <label for="">&nbsp</label>
                                                                <button class="btn btn-success span12"
                                                                        id="btnAdicionarProduto">
                                                                    Adicionar
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                <?php }?>
                                                <div class="span12" id="divProdutos" style="margin-left: 0">
                                                    <table class="table table-bordered" id="tblProdutos">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: left;">Produto</th>
                                                            <th style="text-align: right;">Tipo</th>
                                                            <th style="text-align: right;">Estoque</th>
                                                            <th style="text-align: right;">Preço de venda</th>
                                                            <th style="text-align: right;">Preço de custo</th>
                                                            <th style="text-align: center;">Ações</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        foreach ($produtos as $p) {
                                                            echo '<tr>';
                                                            echo '<td style="text-align: left;">' . $p->descricao . '</td>';
                                                            echo '<td style="text-align: right;">' . $p->tipo . '</td>';
                                                            echo '<td style="text-align: right;">' . $p->quantidade . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->venda, 2, ',', '.') . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->custo, 2, ',', '.') . '</td>';
                                                            if ($result->status == 'Aberto') {
                                                                echo '<td style="text-align: center;"><a href="" idAcao="' . $p->idItens . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a></td>';
                                                            } else {
                                                                echo '<td style="text-align: center;">-</td>';
                                                            }

                                                            echo '</tr>';
                                                        } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <form action="<?php echo current_url(); ?>" method="post"
                                                  id="formObservacao">
                                                <?php echo form_hidden('idInventario', $result->idInventario) ?>
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <textarea class="form-control" name="observacao" cols="30"
                                                                  rows="5"><?php echo $result->observacao; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="span12" style="text-align: right;">
                                                    <div class="span8 offset2">
                                                        <button class="btn btn-success" id="btnContinuar"><i
                                                                    class="icon-white icon-ok"></i> Alterar Dados
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/inventario.js"></script>

<script type="text/javascript">

    var BASE_URL = '<?php echo base_url();?>';

    $(document).ready(function () {

        $(".money").maskMoney();

        $("#formInventario").validate({
            rules: {
                dataInventario: {required: true}
            },
            messages: {
                dataInventario: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formObservacao").validate({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/inventario/editarObservacao",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        alert('Alterado');
                    }
                });
                return false;
            }
        });

        $("#formProdutos").validate({
            rules: {
                quantidade: {required: true},
                idProduto: {required: true}
            },
            messages: {
                quantidade: {required: 'Insira a quantidade'},
                idProduto: {required: 'Insira o produto'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/inventario/adicionarProduto",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");

                            $("#quantidade").val('1');
                            $("#custo").val('');
                            $('#product_id').val('');

                            $("#idProduto").val('').focus();
                        } else {
                            alert('Ocorreu um erro ao tentar adicionar produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', 'a', function (event) {
            var idProduto = $(this).attr('idAcao');
            var quantidade = $(this).attr('quantAcao');
            var produto = $(this).attr('prodAcao');

            if ((idProduto % 1) == 0) {
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/inventario/excluirProduto",
                    data: "idProduto=" + idProduto + "&quantidade=" + quantidade + "&produto=" + produto,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");
                        } else {
                            alert('Ocorreu um erro ao tentar excluir produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
    });

    function getConsultaCEP() {
        if ($.trim($("#cepEntrega").val()) != "") {
            var url = 'http://api.postmon.com.br/v1/cep/' + $("#cepEntrega").val();
            $.get(url, {
                    cep: $("#cepEntrega").val()
                },
                function (data) {
                    if (data != -1) {
                        $("#ruaEntrega").val(data.logradouro);
                        $("#bairroEntrega").val(data.bairro);
                        $("#cidadeEntrega").val(data.cidade);
                        $("#estadoEntrega").val(data.estado);
                    }
                });
        }
    }
</script>