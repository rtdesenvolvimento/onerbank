<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-cogs"></i> Inventário de estoque (<?php echo count($results); ?>)</small></h2>
                <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
                    <ul class="nav navbar-right panel_toolbox">
                        <a class="btn btn-success" href="<?php echo base_url(); ?>inventario/adicionar"><i class="fa fa-plus"></i> Novo Inventário</a>
                    </ul>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="text-align: left;">Origem</th>
                                    <th style="text-align: center;">Data</th>
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    $dataInventario = date(('d/m/Y'),strtotime($r->dataInventario));

                                    echo '<tr>';
                                    echo '<td style="text-align: left;">'.$r->origem.'</td>';
                                    echo '<td style="text-align: center;">'.$dataInventario.'</td>';
                                    echo '<td style="text-align: center;">'.$r->status.'</td>';

                                    echo '<td style="text-align: center;">';

                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){
                                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/inventario/visualizar/'.$r->idInventario.'" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                                    }

                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/inventario/editar/'.$r->idInventario.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>';
                                    }

                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'dVenda')){
                                        echo '<a href="#modal-excluir" role="button" data-toggle="modal" venda="'.$r->idInventario.'"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/vendas/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Venda</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idVenda" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir este pedido de compra?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', 'a', function(event) {
            var venda = $(this).attr('venda');
            $('#idVenda').val(venda);
        });
    });

</script>