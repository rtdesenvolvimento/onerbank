<html>

<head>
    <title>Relatório de inventário</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body>
<br/><br/><br/>
<table style="width: 100%">
    <tr>
        <td style="width: 50%;"></td>
        <td style="width: 50%;text-align: right;">
            <?php echo 'Estoque existente em: 31 de dezembro de 2016'?><br/>
            * * * * * Registro de Inventário * * * * *
        </td>
    </tr>
</table>

<table style="width: 100%;font-size: 12px;">
    <thead>
    <tr>
        <th style="font-size: 1.2em; padding: 5px;text-align: center;">Código</th>
        <th style="font-size: 1.2em; padding: 5px;text-align: left;">Descrição do Artigo</th>
        <th style="font-size: 1.2em; padding: 5px;text-align: right;">Quantidade</th>
        <th style="font-size: 1.2em; padding: 5px;text-align: left;">Unidade</th>
        <th style="font-size: 1.2em; padding: 5px;text-align: right;">Unitário</th>
        <th style="font-size: 1.2em; padding: 5px;text-align: right;">Total</th>
    </tr>
    </thead>
    <tbody>
    <?php

    $total = 0;
    foreach ($produtos as $produto) {
        $estoque = $this->produtos_model->getProdutoFilialById($produto->produtos_id);
        $total = $total + $estoque->precoVenda* $produto->quantidade;
        ?>
        <tr>
            <td style="text-align: center;"><?php echo $produto->ean;?></td>
            <td style="text-align: left;"><?php echo $produto->descricao;?></td>
            <td style="text-align: right;"><?php echo $this->site_model->formatarValorMonetario($produto->quantidade, 3);?></td>
            <td style="text-align: left;"><?php echo $produto->unidade;?></td>
            <td style="text-align: right;"><?php echo $this->site_model->formatarValorMonetario( $estoque->precoVenda, 6);?></td>
            <td style="text-align: right;"><?php echo $this->site_model->formatarValorMonetario($estoque->precoVenda* $produto->quantidade);?></td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="6" style="border-top: 1px solid #2c2f3b;text-align: right;">Valor. . . . . . . . . . . . : <?php echo $this->site_model->formatarValorMonetario($total);?></td>
    </tr>
    </tfoot>
</table>

</body>
</html>