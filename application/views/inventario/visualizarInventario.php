<?php $totalProdutos = 0;?>

<style type="text/css">
    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 11px;
        color: #666;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>

<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Relatório de Inventário</h5>
                <div class="buttons">
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                        echo '<a class="btn btn-mini btn-info" href="'.base_url().'index.php/inventario/editar/'.$result->idInventario.'"><i class="icon-pencil icon-white"></i> Editar</a>';
                    } ?>

                    <a id="imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i> Imprimir</a>
                </div>
            </div>
            <div class="widget-content" id="printOs">
                <div class="invoice-content">
                    <table class="table" style="margin-top: 0">
                        <tbody>

                        <?php if($emitente == null) {?>
                            <tr>
                                <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<</td>
                            </tr>
                        <?php } else {?>

                            <tr>
                                <td style="width: 10%"><img src=" <?php echo $emitente->url_logo; ?> "></td>
                                <td> <span style="font-size: 20px; "> <?php echo $emitente->nome; ?></span> </br><span><?php echo $emitente->cnpj; ?> </br> <?php echo $emitente->rua.', nº:'.$emitente->numero.', '.$emitente->bairro.' - '.$emitente->cidade.' - '.$emitente->uf; ?> </span> </br> <span> E-mail: <?php echo $emitente->email.' - Fone: '.$emitente->telefone; ?></span></td>
                                <td style="width: 30%; text-align: left">
                                    <b>#Inventário:</b> <span ><?php echo $result->idInventario?></span>
                                    </br>
                                    <b>Status:</b> <span ><?php echo $result->status;?></span>
                                    </br>
                                    <b>Origem:</b> <span ><?php echo $result->origem;?></span>
                                    </br>
                                    <b>Responsável:</b> <span ><?php echo $result->nome;?></span>
                                    </br>
                                    <span><b>Emissão: </b><?php echo date(('d/m/Y'),strtotime($result->dataInventario));?></span>
                                </td>
                            </tr>

                        <?php } ?>
                        </tbody>
                    </table>

                    <table class="table" style="margin-top: 0">
                        <tbody>
                        <?php if ($result->observacao) {?>
                            <tr>
                                <td colspan="2">
                                    <span><h6>Observação</h6>
                                        <?php echo $result->observacao;?>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>


                    <div style="margin-top: 0; padding-top: 0">


                        <?php if($produtos != null){?>

                            <table class="table table-bordered table-condensed" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th width="50%" style="font-size: 15px;text-align: left;">Produto</th>
                                    <th width="5%" style="font-size: 15px;text-align: center;">Tipo</th>
                                    <th width="5%" style="font-size: 15px;text-align: center;">Quantidade</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($produtos as $p) {

                                    $totalProdutos = $totalProdutos + $p->subTotal;
                                    echo '<tr>';
                                    echo '<td style="text-align: left;">'.$p->descricao.'</td>';
                                    echo '<td style="text-align: center;">'.$p->tipo.'</td>';
                                    echo '<td style="text-align: center;"> '.$p->quantidade.'   </td>';
                                    echo '</tr>';
                                }?>

                                </tbody>
                            </table>
                        <?php }?>
                    </div>

                    <br/><br/><br/><br/>
                    <p align="center">________________________________________<br /><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $result->nome; ?></font></p>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $("#imprimir").click(function(){

            //pega o Html da DIV
            var divElements = document.getElementById('printOs').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;
        });
    });
</script>