<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Link de pagamento<small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formLinkPagamento" method="post" class="form-horizontal" >

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="descricao" class="form-control" required="required" value=""  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="valor" class="control-label">Valor<span class="required">*</span></label>
                        <div class="controls">
                            <input type="number" id="valor" name="valor" class="form-control" required="required" value=""  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="validade" class="control-label">Válido até<span class="required">*</span></label>
                        <div class="controls">
                            <input type="date" id="validade" name="validade" class="form-control" required="required" value=""  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="forma_pagamento" class="control-label">Formas de cobrança disponível<span class="required">*</span></label>
                        <div class="controls">
                            <select id="forma_pagamento" name="forma_pagamento" class="form-control">
                                <option value="credito_boleto">Cartão de Crédito e Boletos</option>
                                <option value="boleto">Apenas boletos</option>
                                <option value="credito">Apenas Cartão de Crédito</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="modo_parcelamento" class="control-label">Mode de parcelamento<span class="required">*</span></label>
                        <div class="controls">
                            <select id="modo_parcelamento" name="modo_parcelamento" required="required" class="form-control">
                                <option value="interest_free">Sem juros, com o custo já calculado no valor da transação.</option>
                                <option value="with_interest">Modo de parcelamento, com juros por conta do comprador</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_parcelas" class="control-label">Nº de parcelas para crédito<span class="required">*</span></label>
                        <div class="controls">
                            <input type="number" id="numero_parcelas" class="form-control" min="1" max="12" name="numero_parcelas" required="required" value="1"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="status" class="control-label">Status<span class="required">*</span></label>
                        <div class="controls">
                            <select id="status" name="status" class="form-control">
                                <option value="ATIVO">ATIVO</option>
                                <option value="INATIVO">INATIVO</option>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Cadastrar</button>
                            <a href="<?php echo base_url() ?>index.php/linkpagamento" id="btnAdicionar" class="btn btn-primary"><i class="icon-arrow-left"></i> Voltar</a
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#formLinkPagamento').validate({
            rules :{
                nome:{ required: true}
            },
            messages:{
                nome :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>