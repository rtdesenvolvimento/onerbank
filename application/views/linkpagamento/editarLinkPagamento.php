<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Link de pagamento<small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formLinkPagamento" method="post" class="form-horizontal" >
                    <input type="hidden" name="idLinkPagamento" id="idLinkPagamento" value="<?php echo $result->idLinkPagamento; ?>">
                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" class="form-control" name="descricao" required="required" value="<?php echo $result->descricao; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Valor<span class="required">*</span></label>
                        <div class="controls">
                            <input type="number" id="valor" class="form-control" name="valor" required="required" value="<?php echo $result->valor; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Válido até<span class="required">*</span></label>
                        <div class="controls">
                            <input type="date" id="validade" class="form-control" name="validade" required="required" value="<?php echo $result->validade; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="forma_pagamento" class="control-label">Formas de cobrança disponível<span class="required">*</span></label>
                        <div class="controls">
                            <select id="forma_pagamento" name="forma_pagamento" class="form-control">
                                <option value="credito_boleto" <?php if ($result->forma_pagamento == 'credito_boleto') echo 'selected="selected"';?>>Cartão de Crédito e Boletos</option>
                                <option value="boleto" <?php if ($result->forma_pagamento == 'boleto') echo 'selected="selected"';?>>Apenas boletos</option>
                                <option value="credito" <?php if ($result->forma_pagamento == 'credito') echo 'selected="selected"';?>>Apenas Cartão de Crédito</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="modo_parcelamento" class="control-label">Mode de parcelamento<span class="required">*</span></label>
                        <div class="controls">
                            <select id="modo_parcelamento" name="modo_parcelamento" required="required" class="form-control">
                                <option value="with_interest" <?php if ($result->modo_parcelamento == 'with_interest') echo 'selected="selected"';?>>Modo de parcelamento, com juros por conta do cliente</option>
                                <option value="interest_free" <?php if ($result->modo_parcelamento == 'interest_free') echo 'selected="selected"';?>>Ou sem juros, com o custo já calculado no valor da transação.</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_parcelas" class="control-label">Nº de parcelas para crédito<span class="required">*</span></label>
                        <div class="controls">
                            <input type="number" class="form-control" id="numero_parcelas" min="1" max="12" name="numero_parcelas" required="required" value="<?php echo $result->numero_parcelas; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="status" class="control-label">Status<span class="required">*</span></label>
                        <div class="controls">
                            <select id="status" name="status" class="form-control">
                                <option value="ATIVO" <?php if ($result->status == 'ATIVO') echo 'selected="selected"';?>>ATIVO</option>
                                <option value="INATIVO" <?php if ($result->status == 'INATIVO') echo 'selected="selected"';?>>INATIVO</option>
                            </select>
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Alterar</button>
                            <a href="<?php echo base_url() ?>index.php/linkpagamento" id="btnAdicionar" class="btn btn-primary"><i class="icon-arrow-left"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#formLinkPagamento').validate({
            rules :{
                nome:{ required: true}
            },
            messages:{
                nome :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>