<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-link"></i> Links de pagamento</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a class="btn btn-success" href="<?php echo base_url(); ?>linkpagamento/adicionar"><i class="fa fa-plus"></i> Cadastrar Link de pagamento</a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>Validade até</th>
                                    <th>Descrição</th>
                                    <th>Status</th>
                                    <th>Valor</th>
                                    <th style="width: 10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $link) {

                                    $botao = '<!-- INICIO BOTAO ONERBANK - NAO EDITAR -->
                                <form action="'.$link->url.'" method="post">
                                <input type="image" src="'.base_url().'assets/js/images/quero-comprar" style="width: 10%;height: 10%;" name="submit" alt="Pague com a OnerBank!" />
                                </form>
                            <!-- FINAL BOTAO ONERBANK -->';

                                    echo '<tr>';
                                    echo '    <td>' . $this->site_model->formatarData($link->validade) . '</td>';
                                    echo '    <td>' . $link->descricao . '</td>';
                                    echo '    <td>' . $link->status . '</td>';
                                    echo '    <td>R$ ' . $this->site_model->formatarValorMonetario($link->valor) . '</td>';
                                    echo '    <td>';
                                    echo '        <div class="btn-group btn-group-sm" role="group" aria-label="...">';
                                    echo '          <a href="#" onclick="copyTextToClipboard(\''.$link->url.'\');" title="Cópiar o link de pagamento (Cltr + C)"><button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-copy"> </i> </button></a>';
                                    echo '          <a href="'. base_url().'index.php/linkpagamento/editar/' . $link->idLinkPagamento . '" title="Editar o link de pagamento"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    echo '          <a href="#modal-excluir" role="button" data-toggle="modal" linkpagamento="' . $link->idLinkPagamento . '" title="Deletar o link de pagamento" ><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>  ';
                                    echo '          <a href="#" onclick="botaoIntegracao(\''.$link->url.'\');" title="Copiar o link de pagamento para integração em seu Website"><button type="button" class="btn btn-warning btn-sm"><i class="fa fa-code"> </i> </button></a>';
                                    echo '        </div>';
                                    echo '    </td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/linkpagamento/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Acessório</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idLinkPagamento" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este Link de pagamento?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var linkpagamento = $(this).attr('linkpagamento');
            $('#idLinkPagamento').val(linkpagamento);
        });
    });

    function botaoIntegracao(url) {
        var html = '' +
            '<!-- INICIO BOTAO ONERBANK - NAO EDITAR -->\n' +
            '<form action="'+url+'" method="post">\n' +
            '<input type="image" style="width: 15%;"  src="<?php echo base_url()?>assets/js/images/quero-comprar.png" name="submit" alt="Pague com a OnerBank!" />\n' +
            '</form>\n' +
            '<!-- FINAL BOTAO ONERBANK -->';

        copyTextToClipboard(html);
    }

    function copyTextToClipboard(text) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
            window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter", text);
        }

        document.body.removeChild(textArea);
    }

</script>