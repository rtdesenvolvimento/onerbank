<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Log<small></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="post" class="form-horizontal">
                    <div class="control-group">
                        <label for="nome" class="control-label">Data<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" class="form-control" type="date" name="data-arquivo" value="<?php echo $data; ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <select class="form-control" name="arquivo">
                                <option value="">--selecione um arquivo na lista---</option>
                                <?php

                                $destino = './assets/log/'.$emitente->cnpj.'/'.$data;

                                if (is_dir($destino)) {
                                    $diretorio = dir($destino);
                                    while($arquivoon = $diretorio->read()){
                                        if ($arquivoon == '..') continue;
                                        if ($arquivoon == '.') continue; ?>

                                        <?php if ($arquivo == $arquivoon){?>
                                            <option value="<?php echo $arquivoon;?>" selected="selected"><?php echo $arquivoon?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $arquivoon;?>"><?php echo $arquivoon?></option>
                                        <?php }?>
                                    <?php }
                                    $diretorio -> close(); ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <textarea name="arquivo-text" class="form-control" rows="50" cols="50"><?php echo $leitura;?></textarea>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Buscar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




