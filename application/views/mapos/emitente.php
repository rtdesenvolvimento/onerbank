<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sitemap"></i> Dados do emitente</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-content ">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="width: 25%">
                                        <img src=" <?php echo $dados->url_logo; ?> " width="50%"></td>
                                    <td><span style="font-size: 20px; "> <?php echo $dados->nome; ?> </span> </br>
                                        <span><?php echo $dados->cnpj; ?> </br> <?php echo $dados->rua . ', nº:' . $dados->numero . ', ' . $dados->bairro . ' - ' . $dados->cidade . ' - ' . $dados->uf; ?> </span> </br>
                                        <span> E-mail: <?php echo $dados->email . ' - Fone: ' . $dados->telefone; ?></span> </br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="text-align: right;">
                                <div class="ln_solid"></div>
                                <a href="#" data-target=".modalAlterar" data-toggle="modal" role="button" class="btn btn-primary"> <i class="fa fa-eye"></i> Visualizar os Dados</a>
                                <a href="#" data-target=".modalLogo" data-toggle="modal" role="button" class="btn btn-info"><i class="fa fa-photo"></i> Alterar Logomarca</a>
                            </div>
                        </div>
                        <!-- Model para visualizar os dados  do emitente -->
                        <div class="modal fade modalAlterar" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"><i class="fa fa-building"></i> <?php echo NOME_SISTEMA;?> - Dados do Emitente</h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?php echo base_url(); ?>index.php/mapos/editarEmitente" id="formAlterar"
                                              enctype="multipart/form-data" method="post" class="form-horizontal">
                                            <div class="modal-body">

                                                <div class="alert alert-info alert-dismissible " role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong>Informações!</strong><br/> Todos os dados apresentados nesta unidade só poderão ser alteradas pelo Super Administrador do sistema <?php echo NOME_SISTEMA;?>
                                                </div>

                                                <div class="control-group">
                                                    <label for="cnpj" class="control-label"><span class="required">CNPJ*</span></label>
                                                    <div class="controls">
                                                        <input class="form-control" type="text" disabled name="cnpj" value="<?php echo $dados->cnpj; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="nome" class="control-label">Razão Social<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input id="nome" class="form-control"  type="text" disabled name="nome" value="<?php echo $dados->nome; ?>"/>
                                                        <input id="nome" type="hidden" name="id" value="<?php echo $dados->id; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="nomeFantasia" class="control-label">Nome Fantasia</label>
                                                    <div class="controls">
                                                        <input id="nome" class="form-control" type="text" disabled name="nomeFanstasia" value="<?php echo $dados->nomeFanstasia; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="ie" class="control-label">Insc. Estadual</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="ie" disabled value="<?php echo $dados->ie; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="inscst" class="control-label">Insc. ST</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="inscst" disabled value="<?php echo $dados->inscst; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="cnae" class="control-label">CNAE</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="cnae" disabled value="<?php echo $dados->cnae; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="insmun" class="control-label">Insc. Mun.</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="insmun" disabled value="<?php echo $dados->insmun; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="cep" class="control-label"><span class="required">CEP*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="cep" disabled value="<?php echo $dados->cep; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">Logradouro*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="logradouro" disabled value="<?php echo $dados->rua; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">Número*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="numero" disabled value="<?php echo $dados->numero; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="complemento" class="control-label">Complemento</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="complemento" disabled value="<?php echo $dados->complemento; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">Bairro*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="bairro" disabled value="<?php echo $dados->bairro; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">Cidade*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="cidade" disabled value="<?php echo $dados->cidade; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">UF*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="uf" disabled value="<?php echo $dados->uf; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">Telefone*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="telefone"  disabled value="<?php echo $dados->telefone; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="descricao" class="control-label"><span class="required">E-mail*</span></label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="email" disabled value="<?php echo $dados->email; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="regimeTributario" class="control-label">OptanteSimples Nacional<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="regimeTributario" class="form-control" disabled id="regimeTributario">
                                                            <option value="1" <?php if($dados->regimeTributario == 1) echo 'selected="selected"';?>>Sim</option>
                                                            <option value="2" <?php if($dados->regimeTributario == 2) echo 'selected="selected"';?>>Não</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="regimeEspecialTributacao" class="control-label"><span class="required">Regime especial de tributação *</span></label>
                                                    <div class="controls">
                                                        <select name="regimeEspecialTributacao" class="form-control" disabled id="regimeEspecialTributacao">
                                                            <option value="0" <?php if($dados->regimeEspecialTributacao == 0) echo 'selected="selected"';?>>Nenhuma das opções</option>
                                                            <option value="1" <?php if($dados->regimeEspecialTributacao == 1) echo 'selected="selected"';?>>Microempresa munícipal</option>
                                                            <option value="2" <?php if($dados->regimeEspecialTributacao == 2) echo 'selected="selected"';?>>Estimativa</option>
                                                            <option value="3" <?php if($dados->regimeEspecialTributacao == 3) echo 'selected="selected"';?>>Sociedade de profissionais</option>
                                                            <option value="4" <?php if($dados->regimeEspecialTributacao == 4) echo 'selected="selected"';?>>Cooperativa</option>
                                                            <option value="5" <?php if($dados->regimeEspecialTributacao == 5) echo 'selected="selected"';?>>Microempresário individual (MEI)</option>
                                                            <option value="6" <?php if($dados->regimeEspecialTributacao == 6) echo 'selected="selected"';?>>Microempresário e Empresa de Pequeno Porte, (ME EPP)</option>
                                                            <option value="7" <?php if($dados->regimeEspecialTributacao == 7) echo 'selected="selected"';?>>Lucro Real</option>
                                                            <option value="8" <?php if($dados->regimeEspecialTributacao == 8) echo 'selected="selected"';?>>Lucro Presumido</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="csc" class="control-label">CSC (NFC-e)</label>
                                                    <div class="controls">
                                                        <input type="text"  class="form-control"name="csc" disabled value="<?php echo $dados->csc; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="idcsc" class="control-label">ID CSC</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="idcsc" disabled value="<?php echo $dados->idcsc; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="tokenibpt" class="control-label">Token IBPT</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="tokenibpt"  disabled value="<?php echo $dados->tokenibpt; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="ambiente" class="control-label"><span class="required">Ambiente*</span></label>
                                                    <div class="controls">
                                                        <select name="ambiente" class="form-control" disabled id="ambiente">
                                                            <option value="1" <?php if($dados->ambiente == 1) echo 'selected="selected"';?>>Produção</option>
                                                            <option value="2" <?php if($dados->ambiente == 2) echo 'selected="selected"';?>>Homologação</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="incentivadorCultural" class="control-label">Incentivador Cultural<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="incentivadorCultural" class="form-control" disabled id="incentivadorCultural">
                                                            <option value="1" <?php if($dados->incentivadorCultural == '1') echo 'selected="selected"';?>>Sim</option>
                                                            <option value="2" <?php if($dados->incentivadorCultural == '2') echo 'selected="selected"';?>>Não</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="incentivoFiscal" class="control-label">Incentivo Fiscal<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="incentivoFiscal"  class="form-control" disabled id="incentivoFiscal">
                                                            <option value="1" <?php if($dados->incentivoFiscal == '1') echo 'selected="selected"';?>>Sim</option>
                                                            <option value="2" <?php if($dados->incentivoFiscal == '2') echo 'selected="selected"';?>>Não</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="certificado" class="control-label">Certificado<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="certificado"  class="form-control" disabled id="certificado">
                                                            <option value="A1" <?php if($dados->certificado == 'A1') echo 'selected="selected"';?>>A1</option>
                                                            <option value="A3" <?php if($dados->certificado == 'A3') echo 'selected="selected"';?>>A3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="tipoTributacao" class="control-label">Tipo de tribução<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="tipoTributacao" class="form-control" disabled id="tipoTributacao">
                                                            <option></option>
                                                            <option value="1" <?php if($dados->tipoTributacao == '1') echo 'selected="selected"';?>>Isenta de ISS</option>
                                                            <option value="2" <?php if($dados->tipoTributacao == '2') echo 'selected="selected"';?>>Imune</option>
                                                            <option value="3" <?php if($dados->tipoTributacao == '3') echo 'selected="selected"';?>>Não Incidência no Município</option>
                                                            <option value="4" <?php if($dados->tipoTributacao == '4') echo 'selected="selected"';?>>Não Tributável</option>
                                                            <option value="5" <?php if($dados->tipoTributacao == '5') echo 'selected="selected"';?>>Retida</option>
                                                            <option value="6" <?php if($dados->tipoTributacao == '6') echo 'selected="selected"';?>>Tributável dentro do município</option>
                                                            <option value="7" <?php if($dados->tipoTributacao == '7') echo 'selected="selected"';?>>Tributável fora do município</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="naturezaTributacao" class="control-label">Natureza da tributacao<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="naturezaTributacao" class="form-control" disabled id="naturezaTributacao">
                                                            <option></option>
                                                            <option value="1" <?php if($dados->naturezaTributacao == '1') echo 'selected="selected"';?>>Simples Nacional</option>
                                                            <option value="2" <?php if($dados->naturezaTributacao == '2') echo 'selected="selected"';?>>Fixo</option>
                                                            <option value="3" <?php if($dados->naturezaTributacao == '3') echo 'selected="selected"';?>>Depósito em Juízo</option>
                                                            <option value="4" <?php if($dados->naturezaTributacao == '4') echo 'selected="selected"';?>>Exigibilidade suspensa por decisão judicial</option>
                                                            <option value="5" <?php if($dados->naturezaTributacao == '5') echo 'selected="selected"';?>>Exigibilidade suspensa por rocedimento administrativo</option>
                                                            <option value="6" <?php if($dados->naturezaTributacao == '6') echo 'selected="selected"';?>>Isenção parcial.</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group" style="display: none;">
                                                    <label for="certificado" class="control-label"><span class="required">Certificado Digital A1*</span></label>
                                                    <div class="controls">
                                                        <input type="file" name="cert" class="form-control" disabled id="arquivocert" class="upload form-control">
                                                    </div>
                                                </div>
                                                <div class="control-group" style="display: none;">
                                                    <label for="certsenha" class="control-label">Senha do Certificado</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="certsenha" disabled value="<?php echo $dados->certsenha; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <?php
                                                        if ($dados->cert != '') :
                                                            $base_path = FCPATH.'emissores/v4/';
                                                            $file = '' . $base_path . '/nfephp-master/certs/'.$dados->cnpj.'/'.$dados->cert.'';

                                                            $certs = array();
                                                            $pkcs12 = file_get_contents($file);
                                                            if (openssl_pkcs12_read($pkcs12, $certs, $dados->certsenha)) :

                                                                $certificado = array();
                                                                $certificado = openssl_x509_parse(openssl_x509_read($certs['cert']));

                                                                echo '<br/>';
                                                                //print_r( $dados );
                                                                //Dados mais importantes
                                    //                                            echo $dados['subject']['C'] . '<br>'; //País
                                    //                                            echo $dados['subject']['ST'] . '<br>'; //Estado
                                    //                                            echo $dados['subject']['L'] . '<br>'; //Município
                                                                echo '<b>Emitido Para </b><br/>' . $certificado['subject']['CN'] . '<br>'; //Razão Social e CNPJ / CPF
                                                                echo '<b>Validade </b><br/>' . date('d/m/Y', $certificado['validTo_time_t']) . '<br>'; //Validade
                                    //                                            echo $dados['extensions']['subjectAltName'] . '<br>'; //Emails Cadastrados separado por ,
                                                            endif;
                                                        else :
                                                            echo '<h4> Sem Certificado. </h4>';
                                                        endif;
                                                        ?>
                                                    </div>
                                                </div>
                                                 <?php if ($dados->cert != '') {?>
                                                    <div class="control-group">
                                                        <label for="certsenha" class="control-label">Baixar certificado</label>
                                                        <div class="controls">
                                                            <a href="<?php echo base_url(); ?>emissores/v4/nfephp-master/certs/<?php echo $dados->cnpj?>/<?php echo $dados->cert;?>" target="_blank">Clique aqui</a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="control-group" style="display: none;">
                                                    <label for="ramo_atividade" class="control-label"><span class="required">Ramo de atividade*</span></label>
                                                    <div class="controls">
                                                        <select name="ramo_atividade" class="form-control" disabled id="ramo_atividade">
                                                            <option value="1" <?php if($dados->ramo_atividade == 1) echo 'selected="selected"';?>>Assistência técnica de veículos</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="configuracao_id" class="control-label"><span class="required">Configuração Geral*</span></label>
                                                    <div class="controls">
                                                        <select name="configuracao_id" class="form-control" disabled id="configuracao_id">
                                                            <?php foreach ($configuracoes as $configuracao) {?>
                                                                <option <?php if($configuracao->idConfiguracao == $dados->configuracao_id) echo 'selected="selected"';?> value="<?php echo $configuracao->idConfiguracao;?>" ><?php echo $configuracao->descricao;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="configuracao_tecnospeed_id" class="control-label"><span class="required">Configuração TecnoSpeed</span></label>
                                                    <div class="controls">
                                                        <select name="configuracao_tecnospeed_id" class="form-control" disabled id="configuracao_tecnospeed_id">
                                                            <option></option>
                                                            <?php foreach ($configuracoesTecnnoSpeed as $configuracao) {?>
                                                                <option <?php if($configuracao->idConfiguracaotecnospeed == $dados->configuracao_id) echo 'selected="selected"';?> value="<?php echo $configuracao->idConfiguracaotecnospeed;?>" ><?php echo $configuracao->grupo;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Fechar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Model alterar logomarca -->
                        <div class="modal fade modalLogo" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"><i class="fa fa-photo"></i> <?php echo NOME_SISTEMA;?> - Alterar Logomarca</h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?php echo base_url(); ?>index.php/mapos/editarLogo" id="formLogo" enctype="multipart/form-data"
                                              method="post" class="form-horizontal">
                                            <div class="modal-body">
                                                <div class="span12 alert alert-info">Selecione uma nova imagem da logomarca. Tamanho indicado (130 X
                                                    130).
                                                </div>
                                                <div class="control-group">
                                                    <label for="logo" class="control-label"><span class="required">Logomarca*</span></label>
                                                    <div class="controls">
                                                        <input type="file" name="userfile" value=""/>
                                                        <input id="nome" type="hidden" name="id" value="<?php echo $dados->id; ?>"/>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary"> <i class="fa fa-save"></i> Alterar</button>
                                                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Fechar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $("#formLogo").validate({
            rules: {
                userfile: {required: true}
            },
            messages: {
                userfile: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formCadastrar").validate({
            rules: {
                userfile: {required: true},
                nome: {required: true},
                cnpj: {required: true},
                ie: {required: true},
                logradouro: {required: true},
                numero: {required: true},
                bairro: {required: true},
                cidade: {required: true},
                uf: {required: true},
                telefone: {required: true},
                email: {required: true}
            },
            messages: {
                userfile: {required: 'Campo Requerido.'},
                nome: {required: 'Campo Requerido.'},
                cnpj: {required: 'Campo Requerido.'},
                ie: {required: 'Campo Requerido.'},
                logradouro: {required: 'Campo Requerido.'},
                numero: {required: 'Campo Requerido.'},
                bairro: {required: 'Campo Requerido.'},
                cidade: {required: 'Campo Requerido.'},
                uf: {required: 'Campo Requerido.'},
                telefone: {required: 'Campo Requerido.'},
                email: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formAlterar").validate({
            rules: {
                userfile: {required: true},
                nome: {required: true},
                cnpj: {required: true},
                ie: {required: true},
                logradouro: {required: true},
                numero: {required: true},
                bairro: {required: true},
                cidade: {required: true},
                uf: {required: true},
                telefone: {required: true},
                email: {required: true}
            },
            messages: {
                userfile: {required: 'Campo Requerido.'},
                nome: {required: 'Campo Requerido.'},
                cnpj: {required: 'Campo Requerido.'},
                ie: {required: 'Campo Requerido.'},
                logradouro: {required: 'Campo Requerido.'},
                numero: {required: 'Campo Requerido.'},
                bairro: {required: 'Campo Requerido.'},
                cidade: {required: 'Campo Requerido.'},
                uf: {required: 'Campo Requerido.'},
                telefone: {required: 'Campo Requerido.'},
                email: {required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
                $(element).parents('.control-group').removeClass('success');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>