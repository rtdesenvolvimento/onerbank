<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/login.css"/>
    <title><?php echo TITLE_SISTEMA; ?></title>
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/favicon.ico"/>
</head>
<body>
<div class="container">
    <div class="forms-container">
        <div class="signin-signup">

            <form class="sign-in-form" id="formLogin" method="post" action="<?php echo base_url() ?>index.php/mapos/verificarLogin">
                <h2 class="title"><img src="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/logo.png" alt="Logo"/></h2>
                <div class="input-field">
                    <i class="fas fa-building"></i>
                    <input type="text" id="cnpjempresa" name="cnpjempresa"
                           value="<?php echo $this->uri->segment(3); ?>"
                           placeholder="CNPJ é obrigatório."/>
                </div>
                <div class="input-field">
                    <i class="fas fa-user"></i>
                    <input type="text" id="usuariologin" name="usuariologin" placeholder="Login"/>
                </div>
                <div class="input-field">
                    <i class="fas fa-lock"></i>
                    <input type="password" id="senha" name="senha" placeholder="Password" />
                </div>
                <div class="input-field">
                    <i class="fas fa-sitemap"></i>
                    <span id="idConfiguracao">
                        <select class="combostyle" name="filial" required="required" style="color: #aaa;width: 100%;" id="filial">
                            <option value="">Selecione uma filial</option>
                        </select>
                    </span>
                </div>
                <?php if ($this->session->flashdata('error') != null) { ?>
                    <div style="text-align: left;">
                        <div class="wrap-input100 validate-input" id="falha-login" style="margin-top: 4px;">
                            <span style="color: #c4051c;font-size: 14px;font-weight: 500;">Aténção!</span><br/>
                            <span style="font-size: 13px;font-weight: 300;color: #aaa;">Usuário ou senha inválidos.</span>
                        </div>
                    </div>
                <?php } ?>
                <input type="submit" value="Login" class="btn solid"/>
            </form>
            <form class="sign-up-form" id="formLogin" method="post" action="<?php echo base_url() ?>index.php/conecte/login">
                <h2 class="title"><img src="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/logo.png" alt="Logo"/></h2>
                <div class="input-field">
                    <i class="fas fa-user"></i>
                    <input type="text" placeholder="Username"/>
                </div>
                <div class="input-field">
                    <i class="fas fa-envelope"></i>
                    <input type="email" placeholder="Email"/>
                </div>
                <div class="input-field">
                    <i class="fas fa-lock"></i>
                    <input type="password" placeholder="Password"/>
                </div>
                <input type="submit" class="btn" value="Login"/>
            </form>
        </div>
    </div>

    <div class="panels-container">
        <div class="panel left-panel">
            <div class="content">
                <h3>Você é um contador?</h3>
                <p>
                    Você pode acompanhar as movimentações contábeis dos seus
                    clientes que usam OnerBank. Acesse abaixo sua conta OnerBank+ Contador.
                </p>
                <button class="btn transparent" id="sign-up-btn">
                    Acessar Conta
                </button>
            </div>
            <img src="<?php echo base_url() ?>assets/img/log.svg" class="image" alt=""/>
        </div>
        <div class="panel right-panel">
            <div class="content">
                <h3>Prestador de serviço ou venda no comercio ?</h3>
                <p>
                    Gerencie seu financeiro, compras, estoque de forma simplificada e unificada com disponíbilidade online.
                </p>
                <button class="btn transparent" id="sign-in-btn">
                    Acessar Conta
                </button>
            </div>
            <img src="<?php echo base_url() ?>assets/img/register.svg" class="image" alt=""/>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/login.js"></script>

<script>

    var select =
        '         <select class="combostyle" name="filial" required="required" style="color: #aaa;width: 100%;" id="filial">\n' +
        '           <option value="">Selecione uma filial</option>\n' +
        '         </select>';

    $(document).ready(function () {

        $('#usuariologin').focus();

        <?php if ($this->uri->segment(3)){?>
        var cnpjempresa = $('#cnpjempresa').val();

        if (cnpjempresa !== '') {

            $('#div_usuasriologin').show();
            $('#div_senha').show();

            var usuario = $(this).val();
            usuario = usuario.replace("@", "arroba")
            $('#idConfiguracao').load('<?php echo base_url();?>index.php/mapos/buscar_filial/' + usuario+'/'+cnpjempresa);
            $('#password').focus();
        } else {
            $('#div_usuasriologin').hide();
            $('#div_senha').hide();
            $('#idConfiguracao').html(select);
        }
        <?php }?>

        $('#usuariologin').blur(function(){

            var cnpjempresa = $('#cnpjempresa').val();

            if (cnpjempresa !== '') {

                $('#div_usuasriologin').show();
                $('#div_senha').show();

                var usuario = $(this).val();
                usuario = usuario.replace("@", "arroba")
                $('#idConfiguracao').load('<?php echo base_url();?>index.php/mapos/buscar_filial/' + usuario+'/'+cnpjempresa);
                $('#password').focus();
            } else {
                $('#idConfiguracao').html(select);
            }
        });

        $('#cnpjempresa').blur(function () {
            var cnpjempresa = $(this).val();

            if (cnpjempresa !== '') {

                var usuario = $('#usuariologin').val();
                usuario = usuario.replace("@", "arroba")
                $('#idConfiguracao').html(select);

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/mapos/buscar_filial/" + usuario+'/'+cnpjempresa,
                    dataType: 'html',
                    success: function (retorno) {
                        if (retorno !== '') {
                            $('#idConfiguracao').html(retorno);
                            $('#password').focus();
                        } else {
                            $('#div_usuasriologin').show();
                            $('#div_senha').show();
                            $('#password').focus();
                        }
                    }
                });
            } else {
                $('#idConfiguracao').html(select);
            }
        });

        $("#formlogin").submit({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                debugger;

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/mapos/verificarLogin?ajax=true",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) window.location.href = "<?php echo base_url();?>index.php/mapos";
                        else $('#falha-login').show();
                    }
                });
                return false;
            }
        });
    });

    function versenha() {
        var inputSenha = $('#senha');
        var type =  inputSenha.attr('type');

        if (type === 'text') {
            inputSenha.attr('type','password');
            $('#versenha').addClass('fa-lock');
            $('#versenha').removeClass('fa-eye');
        } else {
            inputSenha.attr('type','text');
            $('#versenha').removeClass('fa-lock');
            $('#versenha').addClass('fa-eye');
        }
    }
</script>

</body>
</html>
