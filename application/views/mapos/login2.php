<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title><?php echo TITLE_SISTEMA; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/favicon.ico"/>

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/util-login.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/main-login.css">
    <!--===============================================================================================-->

    <style>

        ._client {
            text-align: center;
            color: #004b99;
            text-transform: uppercase;
            font-weight: 600;
            margin-left: 70px;
            margin-bottom: 15px;
        }

        ._left {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 38px;
            height: 21px;
            line-height: 24px;
            margin: auto;
            text-align: center;
            text-decoration: none;

            left: 4px;
            color: #004b99;
            font-size: 16px;
        }

        ._right {
            position: absolute;
            top: 0;
            bottom: 0;
            width: 515px;
            height: 21px;
            line-height: 24px;
            margin: auto;
            text-align: center;
            text-decoration: none;

            left: 4px;
            color: #004b99;
            font-size: 16px;
            cursor: pointer;
        }

        a{
            color: #00ad5b;
        }
    </style>
</head>
<body>

<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" id="formLogin" method="post" action="<?php echo base_url() ?>index.php/mapos/verificarLogin">
            <span class="_client">Acesse seu sistema </span>
            <div class="wrap-input100 validate-input" data-validate="CNPJ é obrigatório.">
                <i class="_left fa fa-road"></i><input id="cnpjempresa" class="input100" type="text" name="cnpjempresa" value="<?php echo $this->uri->segment(3); ?>" placeholder="CNPJ da Empresa">
            </div>
            <div class="wrap-input100 validate-input" data-validate="Login é obrigátorio.">
                <i class="_left fa fa-user"></i><input id="usuariologin" class="input100" type="text" name="usuariologin" placeholder="Login">
            </div>
            <div class="wrap-input100 validate-input" data-validate="Senha é obrigatório.">
                <i class="_left fa fa-lock" id="versenha" style="cursor: pointer;" onmouseover="versenha();" onmouseout="versenha();"></i>
                <input id="senha" class="input100" type="password" name="senha" placeholder="Senha">
            </div>
            <div class="wrap-input100 validate-input" data-validate="Filial é obrigatório." id="idConfiguracao">
                <select class="combostyle" name="filial" required="required" style="color: #aaa;width: 100%;padding: 0 40px;" id="filial">
                    <option value="">Selecione uma filial</option>
                </select>
            </div>
            <div class="wrap-input100 validate-input" id="falha-login" style="border: none;margin-top: 4px;display: none;">
                <span style="color: #c4051c;font-size: 14px;font-weight: 500;">Aténção!</span><br/>
                <span style="font-size: 13px;font-weight: 300;color: #aaa;">Usuário ou senha inválidos.</span>
            </div>
            <?php if ($this->session->flashdata('error') != null) { ?>
                <div class="alert alert-danger" style="margin-top: 10px;width: 100%;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <span style="color: #c4051c;font-size: 14px;font-weight: 500;">Aténção!</span><br/>
                    <span style="font-size: 13px;font-weight: 300;">Usuário ou senha inválidos.</span>
                </div>
            <?php } ?>
            <a href="#" style="display: none;">Esqueci minha senha</a>
            <div class="container-contact100-form-btn">
                <button class="contact100-form-btn">
                    <i class="fa fa-sign-in" style="margin-right: 2px;"></i> Acessar o Sistema
                </button>
            </div>
            <div class="container-contact100-form-btn" style="padding-top: 10px;">
                <a href="<?php echo base_url() ?>conectecontador">
                    <i class="fa fa-calculator"></i> Conecte Contador
                </a>
            </div>
            <div class="container-contact100-form-btn" style="padding-top: 4px;">
                <a href="<?php echo base_url() ?>conecte/cliente">
                    <i class="fa fa-users"></i> Área do Cliente
                </a>
            </div>
            <div class="container-contact100-form-btn" style="padding-top: 4px;">
                <a href="<?php echo base_url() ?>master">
                    <i class="fa fa-building-o"></i> Painel Administrador
                </a>
            </div>
            <hr>
            <span class="container-contact100-form-btn" style='font-size: 12px;border-top: 1px solid #ccc; margin-top: 22px;'>
                <div>
                    © <?php echo date('Y'); ?> <?php echo NOME_SISTEMA; ?> (<?php echo VERSAO; ?>)<br/>
                    <a href="https://<?php echo SITE_EMPRESA; ?>" target="_blank"><?php echo SITE_EMPRESA; ?></a>
                </div>
            </span>
        </form>
        <div class="contact100-more flex-col-c-m" style="background: #004b99">
             <span class="contact100-form-title">
                <img src="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/logo.png" alt="Logo"/>
            </span>
            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                     <span class="contact100-form-title">
                     </span>
                </div>
            </div>
            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                    <span class="fa fa-whatsapp" style="font-size: 20px;"></span>
                </div>
                <div class="flex-col size2">
                    <span class="txt1 p-b-20">
                        Suporte
                    </span>
                    <span class="txt3">
                        <a href="https://api.whatsapp.com/send?phone=55<?php echo TELEFONE_SUPORTE;?>" target="_blank"><?php echo TELEFONE_SUPORTE;?></a>
                    </span>
                </div>
            </div>
            <div class="dis-flex size1 p-b-47">
                <div class="txt1 p-r-25">
                    <span class="lnr lnr-envelope"></span>
                </div>
                <div class="flex-col size2">
                    <span class="txt1 p-b-20">E-mail</span>
                    <span class="txt3"><?php echo EMAIL_CONTATO; ?></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dropDownSelect1"></div>
<a href="#notification" id="call-modal" role="button" class="btn" data-toggle="modal" style="display: none ">notification</a>

<div id="notification" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel"><?php echo NOME_SISTEMA;?></h4>
    </div>
    <div class="modal-body">
        <h5 style="text-align: center">Os dados de acesso estão incorretos, por favor tente novamente!</h5>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Fechar</button>
    </div>
</div>

<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/popper.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/daterangepicker/moment.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>assets/vendor/select2/select2.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/main-login.js"></script>
<script src="<?php echo base_url() ?>assets/js/validate.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>

<script>

    var select =
        '         <select class="combostyle" name="filial" required="required" style="color: #aaa;width: 100%;padding: 0 40px;" id="filial">\n' +
        '           <option value="">Selecione uma filial</option>\n' +
        '         </select>';

    $(document).ready(function () {

        $('#usuariologin').focus();

        <?php if ($this->uri->segment(3)){?>
        var cnpjempresa = $('#cnpjempresa').val();

        if (cnpjempresa !== '') {

            $('#div_usuasriologin').show();
            $('#div_senha').show();

            var usuario = $(this).val();
            usuario = usuario.replace("@", "arroba")
            $('#idConfiguracao').load('<?php echo base_url();?>index.php/mapos/buscar_filial/' + usuario+'/'+cnpjempresa);
            $('#password').focus();
        } else {
            $('#div_usuasriologin').hide();
            $('#div_senha').hide();
            $('#idConfiguracao').html(select);
        }
        <?php }?>

        $('#usuariologin').blur(function(){

            var cnpjempresa = $('#cnpjempresa').val();

            if (cnpjempresa !== '') {

                $('#div_usuasriologin').show();
                $('#div_senha').show();

                var usuario = $(this).val();
                usuario = usuario.replace("@", "arroba")
                $('#idConfiguracao').load('<?php echo base_url();?>index.php/mapos/buscar_filial/' + usuario+'/'+cnpjempresa);
                $('#password').focus();
            } else {
                $('#div_usuasriologin').hide();
                $('#div_senha').hide();
                $('#idConfiguracao').html(select);
            }
        });

        $('#cnpjempresa').blur(function () {
            var cnpjempresa = $(this).val();

            if (cnpjempresa !== '') {

                var usuario = $('#usuariologin').val();
                usuario = usuario.replace("@", "arroba")
                $('#idConfiguracao').html(select);

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/mapos/buscar_filial/" + usuario+'/'+cnpjempresa,
                    dataType: 'html',
                    success: function (retorno) {
                        if (retorno !== '') {
                            $('#idConfiguracao').html(retorno);
                            $('#div_usuasriologin').show();
                            $('#div_senha').show();
                            $('#password').focus();
                        } else {
                            $('#div_usuasriologin').show();
                            $('#div_senha').show();
                            $('#password').focus();
                        }
                    }
                });
            } else {
                $('#div_usuasriologin').hide();
                $('#div_senha').hide();
                $('#idConfiguracao').html(select);
            }
        });

        $("#formlogin").submit({
            submitHandler: function (form) {
                var dados = $(form).serialize();

                debugger;
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/mapos/verificarLogin?ajax=true",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            window.location.href = "<?php echo base_url();?>index.php/mapos";
                        } else {
                            $('#falha-login').show();
                        }
                    }
                });
                return false;
            }
        });
    });

    function versenha() {
        var inputSenha = $('#senha');
        var type =  inputSenha.attr('type');

        if (type === 'text') {
            inputSenha.attr('type','password');
            $('#versenha').addClass('fa-lock');
            $('#versenha').removeClass('fa-eye');
        } else {
            inputSenha.attr('type','text');
            $('#versenha').removeClass('fa-lock');
            $('#versenha').addClass('fa-eye');
        }
    }
</script>

</body>
</html>