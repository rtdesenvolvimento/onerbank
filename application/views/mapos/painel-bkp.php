<!--[if lt IE 9]>
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>js/dist/excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabela_responsiva.css"/>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.donutRenderer.min.js"></script>


<!--Action boxes-->
<div class="container-fluid">
    <div class="quick-actions_homepage">
        <ul class="quick-actions">
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) { ?>
                <li class="bg_lb"><a href="<?php echo base_url() ?>index.php/clientes"> <i class="icon-group"></i>
                        Clientes</a></li>
            <?php } ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                <li class="bg_lg"><a href="<?php echo base_url() ?>index.php/produtos"> <i class="icon-barcode"></i>
                        Produtos</a></li>
            <?php } ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vServico')) { ?>
                <li class="bg_ly"><a href="<?php echo base_url() ?>index.php/servicos"> <i class="icon-wrench"></i>
                        Serviços</a></li>
            <?php } ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
                <li class="bg_lo"><a href="<?php echo base_url() ?>index.php/os"> <i class="icon-tags"></i> OS</a></li>
            <?php } ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vVenda')) { ?>
                <li class="bg_ls"><a href="<?php echo base_url() ?>index.php/vendas"><i class="icon-shopping-cart"></i>
                        Vendas</a></li>
            <?php } ?>

            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
                <?php if ($emitente->ramo_atividade == 1) { ?>
                    <li class="bg_lh"><a href="<?php echo base_url() ?>index.php/veiculos"><i class="icon-ambulance"></i> Ve&iacute;culos</a>
                    </li>
                <?php } ?>
            <?php } ?>

        </ul>
    </div>
</div>
<!--End-Action boxes-->

<div class="row-fluid" style="margin-top: 0">
    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
        <div class="span12">

            <div class="widget-box">
                <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Produtos Com Estoque
                        Mínimo</h5></div>
                <div class="widget-content">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Preço de Venda</th>
                            <th>Estoque</th>
                            <th>Estoque Mínimo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($produtos != null) {
                            foreach ($produtos as $p) {
                                echo '<tr>';
                                echo '<td>' . $p->descricao . '</td>';
                                echo '<td>R$ ' . $p->precoVenda . '</td>';
                                echo '<td>' . $p->estoque . '</td>';
                                echo '<td>' . $p->estoqueMinimo . '</td>';
                                echo '<td>';
                                if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eProduto')) {
                                    echo '<a href="' . base_url() . 'index.php/produtos/editar/' . $p->idProdutos . '" class="btn btn-info"> <i class="icon-pencil" ></i> </a>  ';
                                }
                                echo '</td>';
                                echo '</tr>';
                            }
                        } else {
                            echo '<tr><td colspan="5">Nenhum produto com estoque baixo.</td></tr>';
                        }

                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
    <div class="span12" style="margin-left: 0">

        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Ordens de Serviço Em
                    Aberto</h5></div>
            <div class="widget-content">
                <table class="table table-responsive">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="text-align: left;">Cliente</th>
                        <th style="text-align: left;">Responsável</th>
                        <th>Data Inicial</th>
                        <th>Previsão de entrega</th>
                        <th style="text-align: right;">Valor</th>
                        <th>Status</th>
                        <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($ordens != null) {
                        foreach ($ordens as $r) {

                            $dataInicial = date(('d/m/Y'), strtotime($r->dataInicial));
                            $dataFinal = '-';

                            if ($r->dataFinal != '0000-00-00') {
                                $dataFinal = date(('d/m/Y'), strtotime($r->dataFinal));
                            }

                            $veiculo_id = $r->veiculo_id;
                            $veiculoStr = '';

                            if ($veiculo_id) {
                                $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                                if (count($veiculo) > 0) {
                                    $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                                }
                            }

                            $dados_cliente = $r->nomeCliente;

                            if ($r->telefone) {
                                $dados_cliente = $dados_cliente .'<br/>'.$r->telefone;
                            }

                            if ($r->celular) {
                                $dados_cliente = $dados_cliente .'<br/>'.$r->celular;
                            }

                            switch ($r->status) {
                                case 'Retirado':
                                    $cor = '#FF0000';
                                    break;
                                case 'Em Bancada':
                                    $cor = '#0000FF';
                                    break;
                                case 'Orçamento':
                                    $cor = '#008B8B';
                                    break;
                                case 'Aguardando peça':
                                    $cor = '#FF7F00';
                                    break;
                                case 'Pronto':
                                    $cor = '#00FF00';
                                    break;
                                case 'Aguardando Aprovação':
                                    $cor = '#FFA500';
                                    break;
                                case 'Aprovado':
                                    $cor = '#00FA9A';
                                    break;
                                case 'Recusado':
                                    $cor = '#FFD700';
                                    break;
                                case 'Garantia':
                                    $cor = '#8B6914';
                                    break;
                                case 'S/Conserto':
                                    $cor = '#9B30FF';
                                    break;
                                case 'Aberto':
                                    $cor = '#fbb450';
                                    break;
                                case 'Faturado':
                                    $cor = '#5bb75b';
                                    break;
                                case 'Em Andamento':
                                    $cor = '#5bc0de';
                                    break;
                                case 'Finalizado':
                                    $cor = '#e5e5e5';
                                    break;
                                case 'Cancelado':
                                    $cor = '#ee5f5b';
                                    break;
                                default:
                                    $cor = '#E0E4CC';
                                    break;
                            }

                            echo '<tr class="abrirOs" idOs="'.$r->idOs.'">';
                            echo '<td style="text-align: left;">' . $dados_cliente .'<br/>'.$veiculoStr.'</td>';
                            echo '<td style="text-align: left;">' . $r->nomeResponsavel.'</td>';
                            echo '<td style="text-align: center;">' . $dataInicial . '</td>';
                            echo '<td style="text-align: center;">' . $dataFinal . '</td>';
                            echo '<td style="text-align: right">R$ '.$r->valorTotal.'</td>';
                            echo '<td style="text-align: center"><span class="badge" style="background-color: '.$cor.'; border-color: '.$cor.'">'.$r->status.'</span> </td>';
                            echo '<td style="width: 18%;text-align: center;">';
                            // echo '                    <div class="text-center">';
                            // echo '               <div class="btn-group text-left">';
                            // echo '         <button type="button"';
                            //echo ' class="btn btn-default btn-xs btn-primary dropdown-toggle"';
                            // echo '    data-toggle="dropdown"><spa style="color: white;"> Ações </span> <span class="caret"></span></button>';
                            // echo '    <ul class="dropdown-menu pull-right" role="menu">';
                            // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) {
                            // echo '<li>';
                            //   echo '    <a href="'.base_url() . 'index.php/os/visualizar/' . $r->idOs.'"><i class="icon-eye-open"></i>';
                            // echo 'Ver detalhes da O.S';
                            //   echo '    </a>';
                            //  echo '</li>';

                            //  echo '<li>';
                            //   echo '    <a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'"><i class="icon-print"></i>';
                            //  echo 'Imprimir O.S';
                            //   echo '    </a>';
                            //  echo '</li>';
                            //    }
                            // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
                            //   echo '<li>';
                            //   echo '    <a href="'.base_url() . 'index.php/os/editar/' . $r->idOs.'"><i class="icon-pencil icon-white"></i>';
                            //   echo 'Editar O.S';
                            //  echo '    </a>';
                            //  echo '</li>';
                            //  }
                            // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dOs')) {
                            //  echo '<li>';
                            //   echo '    <a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" ><i class="icon-remove icon-white"></i>';
                            //  echo 'Excluir O.S';
                            //  echo '    </a>';
                            //  echo '</li>';
                            // }
                            echo ' </ul>';
                            echo ' </div>';
                            echo '</div>';

                            echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open""></i></a>';
                            echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                            echo '<a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                            //echo '<a href="'.base_url() . 'index.php/os/imprimir_os/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                            echo '<a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" style="margin-right: 1%" class="btn btn-danger tip-top" ><i class="icon-remove icon-white"></i>';

                            echo '</td>';

                            echo '</tr>';
                        }
                    } else {
                        echo '<tr><td colspan="8">Nenhuma OS em aberto.</td></tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
<?php if ($estatisticas_financeiro != null) {
    if ($estatisticas_financeiro->total_receita != null || $estatisticas_financeiro->total_despesa != null || $estatisticas_financeiro->total_receita_pendente != null || $estatisticas_financeiro->total_despesa_pendente != null) { ?>
        <div class="row-fluid" style="margin-top: 0">

            <div class="span4">

                <div class="widget-box">
                    <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas
                            financeiras - Realizado</h5></div>
                    <div class="widget-content">
                        <div class="row-fluid">
                            <div class="span12">
                                <div id="chart-financeiro" style=""></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="span4">

                <div class="widget-box">
                    <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas
                            financeiras - Pendente</h5></div>
                    <div class="widget-content">
                        <div class="row-fluid">
                            <div class="span12">
                                <div id="chart-financeiro2" style=""></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="span4">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Total em caixa /
                            Previsto</h5></div>
                    <div class="widget-content">
                        <div class="row-fluid">
                            <div class="span12">
                                <div id="chart-financeiro-caixa" style=""></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    <?php }
} ?>

<?php if ($vistoria != null) { ?>
    <div class="row-fluid" style="margin-top: 0">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas de
                        Vistoria</h5></div>
                <div class="widget-content">
                    <div class="row-fluid">
                        <div class="span12">
                            <div id="chart-vistoria" style=""></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php } ?>
<?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
    <?php if ($os != null) { ?>
        <div class="row-fluid" style="margin-top: 0">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas de
                            OS</h5></div>
                    <div class="widget-content">
                        <div class="row-fluid">
                            <div class="span12">
                                <div id="chart-os" style=""></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>

<div class="row-fluid" style="margin-top: 0">

    <div class="span12">

        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas do
                    Sistema</h5></div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12">
                        <ul class="site-stats">
                            <li class="bg_lh"><i class="icon-group"></i>

                                <?php
                                    $cnpjempresa =  $this->session->userdata('cnpjempresa');
                                    $otherdb = $this->load->database($cnpjempresa, TRUE);
                                ?>
                                <strong><?php echo $otherdb->count_all('clientes'); ?></strong>
                                <small>Clientes</small>
                            </li>
                            <li class="bg_lh"><i class="icon-barcode"></i>
                                <strong><?php echo $otherdb->count_all('produtos'); ?></strong>
                                <small>Produtos</small>
                            </li>
                            <li class="bg_lh"><i class="icon-tags"></i>
                                <strong><?php echo  $otherdb->count_all('os'); ?></strong>
                                <small>Ordens de Serviço</small>
                            </li>
                            <li class="bg_lh"><i class="icon-wrench"></i>
                                <strong><?php echo $otherdb->count_all('servicos'); ?></strong>
                                <small>Serviços</small>
                            </li>

                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.abrirOs').click(function (event) {
            var idOs = $(this).attr('idOs');
            window.location = '<?php echo base_url();?>index.php/os/editar/'+idOs;
        });

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>emissores/v4/nfephp-master/install/saveconfig.php",
            dataType: 'html',
            success: function (cliente) {
            }
        });


        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/programacaomanutencaopreventiva/verificarManutencaoPeriodo",
            dataType: 'html',
            success: function (cliente) {
            }
        });

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/programacaomanutencaopreventivaveiculo/verificarManutencaoPeriodo",
            dataType: 'html',
            success: function (cliente) {
            }
        });

    });

</script>

<?php if ($os != null) { ?>
    <script type="text/javascript">

        $(document).ready(function () {
            var data = [
                <?php foreach ($os as $o) {
                echo "['" . $o->status . "', " . $o->total . "],";
            } ?>

            ];
            var plot1 = jQuery.jqplot('chart-os', [data],
                {
                    seriesDefaults: {
                        // Make this a pie chart.
                        renderer: jQuery.jqplot.PieRenderer,
                        rendererOptions: {
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            showDataLabels: true
                        }
                    },
                    legend: {show: true, location: 'e'}
                }
            );

        });
    </script>
<?php } ?>


<?php if ($vistoria != null) { ?>
    <script type="text/javascript">

        $(document).ready(function () {
            var data = [
                <?php foreach ($vistoria as $o) {
                echo "['" . $o->status . "', " . $o->total . "],";
            } ?>

            ];
            var plot1 = jQuery.jqplot('chart-vistoria', [data],
                {
                    seriesDefaults: {
                        // Make this a pie chart.
                        renderer: jQuery.jqplot.PieRenderer,
                        rendererOptions: {
                            // Put data labels on the pie slices.
                            // By default, labels show the percentage of the slice.
                            showDataLabels: true
                        }
                    },
                    legend: {show: true, location: 'e'}
                }
            );
        });
    </script>
<?php } ?>

<?php if (isset($estatisticas_financeiro) && $estatisticas_financeiro != null) {
    if ($estatisticas_financeiro->total_receita != null || $estatisticas_financeiro->total_despesa != null || $estatisticas_financeiro->total_receita_pendente != null || $estatisticas_financeiro->total_despesa_pendente != null) {
        ?>
        <script type="text/javascript">

            $(document).ready(function () {

                var data2 = [['Total Receitas',<?php echo ($estatisticas_financeiro->total_receita != null) ? $estatisticas_financeiro->total_receita : '0.00'; ?>], ['Total Despesas', <?php echo ($estatisticas_financeiro->total_despesa != null) ? $estatisticas_financeiro->total_despesa : '0.00'; ?>]];
                var plot2 = jQuery.jqplot('chart-financeiro', [data2],
                    {

                        seriesColors: ["#9ACD32", "#FF8C00", "#EAA228", "#579575", "#839557", "#958c12", "#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
                        seriesDefaults: {
                            // Make this a pie chart.
                            renderer: jQuery.jqplot.PieRenderer,
                            rendererOptions: {
                                // Put data labels on the pie slices.
                                // By default, labels show the percentage of the slice.
                                dataLabels: 'value',
                                showDataLabels: true
                            }
                        },
                        legend: {show: true, location: 'e'}
                    }
                );


                var data3 = [['Total Receitas',<?php echo ($estatisticas_financeiro->total_receita_pendente != null) ? $estatisticas_financeiro->total_receita_pendente : '0.00'; ?>], ['Total Despesas', <?php echo ($estatisticas_financeiro->total_despesa_pendente != null) ? $estatisticas_financeiro->total_despesa_pendente : '0.00'; ?>]];
                var plot3 = jQuery.jqplot('chart-financeiro2', [data3],
                    {

                        seriesColors: ["#90EE90", "#FF0000", "#EAA228", "#579575", "#839557", "#958c12", "#953579", "#4b5de4", "#d8b83f", "#ff5800", "#0085cc"],
                        seriesDefaults: {
                            // Make this a pie chart.
                            renderer: jQuery.jqplot.PieRenderer,
                            rendererOptions: {
                                // Put data labels on the pie slices.
                                // By default, labels show the percentage of the slice.
                                dataLabels: 'value',
                                showDataLabels: true
                            }
                        },
                        legend: {show: true, location: 'e'}
                    }
                );


                var data4 = [['Total em Caixa',<?php echo($estatisticas_financeiro->total_receita - $estatisticas_financeiro->total_despesa); ?>], ['Total a Entrar', <?php echo($estatisticas_financeiro->total_receita_pendente - $estatisticas_financeiro->total_despesa_pendente); ?>]];
                var plot4 = jQuery.jqplot('chart-financeiro-caixa', [data4],
                    {

                        seriesColors: ["#839557", "#d8b83f", "#d8b83f", "#ff5800", "#0085cc"],
                        seriesDefaults: {
                            // Make this a pie chart.
                            renderer: jQuery.jqplot.PieRenderer,
                            rendererOptions: {
                                // Put data labels on the pie slices.
                                // By default, labels show the percentage of the slice.
                                dataLabels: 'value',
                                showDataLabels: true
                            }
                        },
                        legend: {show: true, location: 'e'}
                    }
                );


            });

        </script>

    <?php }
} ?>