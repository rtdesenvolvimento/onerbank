<?php
$cnpjempresa =  $this->session->userdata('cnpjempresa');
$otherdb = $this->load->database($cnpjempresa, TRUE);
?>
<!-- top tiles -->
<div class="row" style="display: inline-block;">
    <div class="col-md-12 col-sm-12">
        <div class="top_tiles">
            <a href="<?php echo base_url() ?>financeiro">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-barcode"></i></div>
                        <div class="count"><?php echo $qtdBoletosGeradosMes; ?></div>
                        <h3>Boletos gerados</h3>
                        <p>no mês</p>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url() ?>financeiro">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-money"></i></div>
                        <div class="count"><?php echo $qtdBoletosPagosMes; ?></div>
                        <h3>Boletos pagos</h3>
                        <p>no mês</p>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url() ?>financeiro">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-usd"></i></div>
                        <div class="count"><?php echo $qtdBoletosGeradosMes - $qtdBoletosPagosMes; ?></div>
                        <h3>Boletos abertos</h3>
                        <p>no mês</p>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url() ?>financeiro">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-warning"></i></div>
                        <div class="count"><?php echo $qtdBoletosVencidosMes; ?></div>
                        <h3>Boletos vencidos</h3>
                        <p>no mês</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="top_tiles" style="display: none;">
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) { ?>
                <a href="<?php echo base_url() ?>clientes">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i></div>
                            <div class="count"><?php echo $otherdb->count_all('clientes'); ?></div>
                            <h3>Clientes</h3>
                            <p>Clientes ativos</p>
                        </div>
                    </div>
                </a>
            <?php } ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                <a href="<?php echo base_url() ?>produtos">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-cart-plus"></i></div>
                            <div class="count"><?php echo $otherdb->count_all('produtos'); ?></div>
                            <h3>Produtos</h3>
                            <p>Produtos ativos</p>
                        </div>
                    </div>
                </a>
            <?php } ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vServico')) { ?>
                <a href="<?php echo base_url() ?>produtos">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-cogs"></i></div>
                            <div class="count"><?php echo $otherdb->count_all('servicos'); ?></div>
                            <h3>Serviços</h3>
                            <p>Serviços ativos</p>
                        </div>
                    </div>
                </a>
            <?php } ?>
            <a href="<?php echo base_url() ?>linkpagamento">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-link"></i></div>
                        <div class="count"><?php echo $otherdb->count_all('link_pagamento'); ?></div>
                        <h3>Links</h3>
                        <p>Links de pagamentos</p>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-12 col-sm-12">
        <div class="tile_count">
            <div class="col-md-2 col-sm-4  tile_stats_count" style="color: #1abb9c;">
                <span class="count_top"><i class="fa fa-money"></i> Contas a receber hoje</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasReceberHoje->vencimento);?></div>
            </div>

            <div class="col-md-2 col-sm-4  tile_stats_count" style="color: #e74c3c;">
                <span class="count_top"><i class="fa fa-money"></i> Contas a pagar hoje</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasPagarHoje->vencimento);?></div>
            </div>


            <div class="col-md-2 col-sm-4  tile_stats_count" style="color: #1abb9c;;">
                <span class="count_top"><i class="fa fa-money"></i> Na Semana</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasReceberSemana->vencimento);?></div>
            </div>

            <div class="col-md-2 col-sm-4  tile_stats_count" style="color: #e74c3c;">
                <span class="count_top"><i class="fa fa-money"></i> Na Semana</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasPagarSemana->vencimento);?></div>
            </div>

            <div class="col-md-2 col-sm-4  tile_stats_count" style="color: #1abb9c;;">
                <span class="count_top"><i class="fa fa-money"></i> No Mês</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasReceberMes->vencimento);?></div>
            </div>

            <div class="col-md-2 col-sm-4  tile_stats_count" style="color: #e74c3c;">
                <span class="count_top"><i class="fa fa-money"></i> No Mês</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasPagarMes->vencimento);?></div>
            </div>

        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="tile_count">
            <div class="col-md-3 col-sm-4  tile_stats_count" style="color: #1abb9c;">
                <span class="count_top"><i class="fa fa-money"></i>  Vencidas no mês</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasReceberVencidas->vencimento);?></div>
            </div>
        </div>
        <div class="tile_count">
            <div class="col-md-3 col-sm-4  tile_stats_count" style="color: #e74c3c;">
                <span class="count_top"><i class="fa fa-money"></i> Vencidas no mês</span>
                <div class="count">R$ <?php echo $this->site_model->formatarValorMonetario($contasPagarVencidas->vencimento);?></div>
            </div>
        </div>
    </div>
</div>
<!-- /top tiles -->

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3><span style="color: #26b99a;">Receitas</span> X <span style="color: #03586a;">Despesas</span> <small> <?php echo date('Y');?></small></h3>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_content">
                    <canvas id="dasboard-barChart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">

    <!-- pie chart -->
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Receitas por Categoria <small><?php echo date('Y');?></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content2">
                <div id="pie-char-receitas" style="width:100%; height:300px;"></div>
            </div>
        </div>
    </div>
    <!-- /Pie chart -->

    <!-- pie chart -->
    <div class="col-md-6 col-sm-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Despesas por Categoria <small><?php echo date('Y');?></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content2">
                <div id="pie-char-despesas" style="width:100%; height:300px;"></div>
            </div>
        </div>
    </div>
    <!-- /Pie chart -->
</div>

<script>
    $(window).load(function () {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>emissores/v4/nfephp-master/install/saveconfig.php",
            dataType: 'html',
            success: function (cliente) {
            }
        });

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/programacaomanutencaopreventiva/verificarManutencaoPeriodo",
            dataType: 'html',
            success: function (cliente) {
            }
        });

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/programacaomanutencaopreventivaveiculo/verificarManutencaoPeriodo",
            dataType: 'html',
            success: function (cliente) {
            }
        });

        if ($('#pie-char-receitas').length) {

            Morris.Donut({
                element: 'pie-char-receitas',
                data: [
                    <?php
                    $receitas = $this->mapos_model->contasReceberAnoGraficoAno();

                    foreach ($receitas as $receita){?>
                        <?php if ($receita->receita != ''){?>
                        { label: '<?php echo $receita->receita;?>', value: <?php echo $receita->vencimento;?> },
                        <?php }?>
                    <?php } ?>
                ],
                formatter: function (y) {
                    return 'R$ '+y.toFixed(2);
                },
                resize: true
            });
        }

        if ($('#pie-char-despesas').length) {

            Morris.Donut({
                element: 'pie-char-despesas',
                data: [
                    <?php
                    $despesas = $this->mapos_model->contasPagarAnoGraficoAno();

                    foreach ($despesas as $despesa){?>
                    <?php if ($despesa->despesa != ''){?>
                    { label: '<?php echo $despesa->despesa;?>', value: <?php echo $despesa->vencimento;?> },
                    <?php }?>
                    <?php } ?>
                ],
                formatter: function (y) {
                    return 'R$ '+y.toFixed(2);
                },
                resize: true
            });
        }

        if ($('#dasboard-barChart').length) {

            var ctx = document.getElementById("dasboard-barChart");
            var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
                    datasets: [{
                        label: '#Receitas',
                        backgroundColor: "#26B99A",
                        data: [
                            <?php echo $this->mapos_model->contasReceberAno('01') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('02') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('03') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('04') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('05') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('06') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('07') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('08') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('09') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('10') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('11') ?>,
                            <?php echo $this->mapos_model->contasReceberAno('12') ?>
                        ]
                    }, {
                        label: '#Despesas',
                        backgroundColor: "#03586A",
                        data: [
                            <?php echo $this->mapos_model->contasPagarAno('01') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('02') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('03') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('04') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('05') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('06') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('07') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('08') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('09') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('10') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('11') ?>,
                            <?php echo $this->mapos_model->contasPagarAno('12') ?>
                        ]
                    }]
                },

                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    });
</script>