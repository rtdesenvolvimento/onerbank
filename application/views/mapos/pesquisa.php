<div class="span12" style="margin-left: 0; margin-top: 0">
    <div class="span12" style="margin-left: 0">
        <form action="<?php echo current_url() ?>">
            <div class="span10" style="margin-left: 0">
                <input type="text" class="span12" value="<?php echo $termo;?>" name="termo" required="required" placeholder="Digite o termo a pesquisar"/>
            </div>
            <div class="span2">
                <button class="span12 btn"><i class=" icon-search"></i> Pesquisar</button>
            </div>
        </form>
    </div>

    <div class="span12" style="margin-left: 0; margin-top: 0">

        <!--Clientes-->
        <div class="span12" style="min-height: 200px">
            <div class="widget-box" style="min-height: 200px">
                <div class="widget-title">
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                    <h5>Clientes</h5>
                </div>

                <div class="widget-content nopadding">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>CPF/CNPJ</th>
                            <th>bloqueado</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($clientes == null) {
                            echo '<tr><td colspan="4">Nenhum cliente foi encontrado.</td></tr>';
                        }
                        foreach ($clientes as $r) {

                            $inativo = $r->inativo;
                            $observacaoInativo  = $r->observacaoInativo;

                            $background = '';
                            if ($inativo == 1) {
                                $background = '#ee5f5b59';
                                $inativo = 'sim';
                            } else {
                                $inativo = 'não';
                            }

                            echo '<tr style="background: '.$background.'">';
                            echo '<td>' . $r->idClientes . '</td>';
                            echo '<td>' . $r->nomeCliente . '</td>';
                            echo '<td>' . $r->documento . '</td>';
                            echo '<td style="text-align: center;">' . $inativo.'<br/>'. $observacaoInativo . '</td>';
                            echo '<td style="text-align: center;">';

                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
                                echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/clientes/visualizar/' . $r->idClientes . '" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                            }
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eCliente')) {
                                echo '<a href="' . base_url() . 'index.php/clientes/editar/' . $r->idClientes . '" class="btn btn-info tip-top" ><i class="icon-pencil icon-white"></i></a>';
                            }


                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                        <tr>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <!--Veiulos -->
        <div class="span12" style="margin-left: 0; margin-top: 0">
            <div class="widget-box" style="min-height: 200px">
                <div class="widget-title">
                <span class="icon">
                    <i class="icon-barcode"></i>
                </span>
                    <h5>Veículos</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-responsive">
                        <thead>
                        <tr style="backgroud-color: #2D335B">
                            <th>Modelo</th>
                            <th>Marca</th>
                            <th>Placa</th>
                            <th>Tipo</th>
                            <th>Cor</th>
                            <th>Ano</th>
                            <th>Combustível(is)</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($veiculos == null) {
                            echo '<tr><td colspan="4">Nenhum produto foi encontrado.</td></tr>';
                        }
                        foreach ($veiculos as $r) {
                            $combustivel = '';

                            if ($r->gasolina==1){
                                $combustivel = 'Gasolina, ';
                            }

                            if ($r->etanol==1){
                                $combustivel .= 'Etanol, ';
                            }

                            if ($r->diesel==1){
                                $combustivel .= 'Diesel, ';
                            }

                            if ($r->gnv==1){
                                $combustivel .= 'Gás veicular (GNV), ';
                            }

                            echo '<tr>';
                            echo '<td>' . $r->modelo . '</td>';
                            echo '<td>' . $r->marca . '</td>';
                            echo '<td>' . $r->placa . '</td>';
                            echo '<td>' . $r->tipoVeiculo . '</td>';
                            echo '<td>' . $r->cor . '</td>';
                            echo '<td>' . $r->ano . '</td>';
                            echo '<td>' . $combustivel . '</td>';
                            echo '<td style="text-align: center;">';
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) {
                                echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/veiculos/visualizar/' . $r->cliente_id .'/'. $r->idVeiculo . '" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                            }
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eCliente')) {
                                echo '<a href="' . base_url() . 'index.php/clientes/editar/' . $r->cliente_id . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                            }
                            echo '</td>';
                            echo '</tr>';
                        } ?>
                        <tr>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!--Ordens de Serviço-->
    <div class="span12" style="margin-left: 0; margin-top: 0">
        <div class="widget-box" style="min-height: 200px">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Ordens de Serviço</h5>
            </div>

            <div class="widget-content nopadding">
                <table class="table table-responsive">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="text-align: center;">#</th>
                        <th style="text-align: left;">Cliente</th>
                        <th style="text-align: left;">Responsável</th>
                        <th>Data Inicial</th>
                        <th>Data Final</th>
                        <th style="text-align: right;">Valor</th>
                        <th>Status</th>
                        <th style="text-align: center;">Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($os == null) {
                        echo '<tr><td colspan="4">Nenhuma os foi encontrado.</td></tr>';
                    }
                    foreach ($os as $r) {
                        $dataInicial = date(('d/m/Y'), strtotime($r->dataInicial));
                        $dataFinal = '-';

                        if ($r->dataFinal != '0000-00-00') {
                            $dataFinal = date(('d/m/Y'), strtotime($r->dataFinal));
                        }

                        $veiculo_id = $r->veiculo_id;
                        $veiculoStr = '';

                        if ($veiculo_id) {
                            $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                            if (count($veiculo) > 0) {
                                $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                            }
                        }

                         $dados_cliente = $r->nomeCliente;

                        if ($r->telefone) {
                            $dados_cliente = $dados_cliente .'<br/>'.$r->telefone;
                        }

                        if ($r->celular) {
                            $dados_cliente = $dados_cliente .'<br/>'.$r->celular;
                        }

                        switch ($r->status) {
                            case 'Retirado':
                                $cor = '#FF0000';
                                break;
                            case 'Em Bancada':
                                $cor = '#0000FF';
                                break;
                            case 'Orçamento':
                                $cor = '#008B8B';
                                break;
                            case 'Aguardando peça':
                                $cor = '#FF7F00';
                                break;
                            case 'Pronto':
                                $cor = '#00FF00';
                                break;
                            case 'Aguardando Aprovação':
                                $cor = '#FFA500';
                                break;
                            case 'Aprovado':
                                $cor = '#00FA9A';
                                break;
                            case 'Recusado':
                                $cor = '#FFD700';
                                break;
                            case 'Garantia':
                                $cor = '#8B6914';
                                break;
                            case 'S/Conserto':
                                $cor = '#9B30FF';
                                break;
                            case 'Aberto':
                                $cor = '#fbb450';
                                break;
                            case 'Faturado':
                                $cor = '#5bb75b';
                                break;
                            case 'Em Andamento':
                                $cor = '#5bc0de';
                                break;
                            case 'Finalizado':
                                $cor = '#e5e5e5';
                                break;
                            case 'Cancelado':
                                $cor = '#ee5f5b';
                                break;
                            default:
                                $cor = '#E0E4CC';
                                break;
                        }

                        echo '<tr>';
                        echo '<td style="text-align: center;">' . $r->idOs . '</td>';
                        echo '<td style="text-align: left;">' . $dados_cliente .'<br/>'.$veiculoStr.'</td>';
                        echo '<td style="text-align: left;">' . $r->nomeResponsavel.'</td>';
                        echo '<td style="text-align: center;">' . $dataInicial . '</td>';
                        echo '<td style="text-align: center;">' . $dataFinal . '</td>';
                        echo '<td style="text-align: right">R$ '.$r->valorTotal.'</td>';
                        echo '<td style="text-align: center"><span class="badge" style="background-color: '.$cor.'; border-color: '.$cor.'">'.$r->status.'</span> </td>';
                        echo '<td style="width: 18%;text-align: center;">';
                        // echo '                    <div class="text-center">';
                        // echo '               <div class="btn-group text-left">';
                        // echo '         <button type="button"';
                        //echo ' class="btn btn-default btn-xs btn-primary dropdown-toggle"';
                        // echo '    data-toggle="dropdown"><spa style="color: white;"> Ações </span> <span class="caret"></span></button>';
                        // echo '    <ul class="dropdown-menu pull-right" role="menu">';
                        // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) {
                        // echo '<li>';
                        //   echo '    <a href="'.base_url() . 'index.php/os/visualizar/' . $r->idOs.'"><i class="icon-eye-open"></i>';
                        // echo 'Ver detalhes da O.S';
                        //   echo '    </a>';
                        //  echo '</li>';

                        //  echo '<li>';
                        //   echo '    <a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'"><i class="icon-print"></i>';
                        //  echo 'Imprimir O.S';
                        //   echo '    </a>';
                        //  echo '</li>';
                        //    }
                        // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
                        //   echo '<li>';
                        //   echo '    <a href="'.base_url() . 'index.php/os/editar/' . $r->idOs.'"><i class="icon-pencil icon-white"></i>';
                        //   echo 'Editar O.S';
                        //  echo '    </a>';
                        //  echo '</li>';
                        //  }
                        // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dOs')) {
                        //  echo '<li>';
                        //   echo '    <a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" ><i class="icon-remove icon-white"></i>';
                        //  echo 'Excluir O.S';
                        //  echo '    </a>';
                        //  echo '</li>';
                        // }
                        echo ' </ul>';
                        echo ' </div>';
                        echo '</div>';

                        echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open""></i></a>';
                        echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                        echo '<a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                        //echo '<a href="'.base_url() . 'index.php/os/imprimir_os/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                        echo '<a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" style="margin-right: 1%" class="btn btn-danger tip-top" ><i class="icon-remove icon-white"></i>';

                        echo '</td>';

                        echo '</tr>';
                    }
                    ?>
                    <tr>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Ordens de Vistoria-->
    <div class="span12" style="margin-left: 0; margin-top: 0">
        <div class="widget-box" style="min-height: 200px">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Vistorias</h5>
            </div>

            <div class="widget-content nopadding">
                <table class="table table-responsive">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Responsável</th>
                        <th>Data/Hora vistoria</th>
                        <th>GNV</th>
                        <th>Tanque</th>
                        <th>KM Entrada/Saída</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($vistoria == null) {
                        echo '<tr><td colspan="4">Nenhuma vistoria foi encontrado.</td></tr>';
                    }
                    foreach ($vistoria as $r) {
                        switch ($r->status) {
                            case 'Retirado':
                                $cor = '#FF0000';
                                break;
                            case 'Em Bancada':
                                $cor = '#0000FF';
                                break;
                            case 'Orçamento':
                                $cor = '#008B8B';
                                break;
                            case 'Aguardando peça':
                                $cor = '#FF7F00';
                                break;
                            case 'Pronto':
                                $cor = '#00FF00';
                                break;
                            case 'Aguardando Aprovação':
                                $cor = '#FFA500';
                                break;
                            case 'Aprovado':
                                $cor = '#00FA9A';
                                break;
                            case 'Recusado':
                                $cor = '#FFD700';
                                break;
                            case 'Garantia':
                                $cor = '#8B6914';
                                break;
                            case 'S/Conserto':
                                $cor = '#9B30FF';
                                break;
                            case 'Aberto':
                                $cor = '#fbb450';
                                break;
                            case 'Faturado':
                                $cor = '#5bb75b';
                                break;
                            case 'Em Andamento':
                                $cor = '#5bc0de';
                                break;
                            case 'Finalizado':
                                $cor = '#e5e5e5';
                                break;
                            case 'Cancelado':
                                $cor = '#ee5f5b';
                                break;
                            default:
                                $cor = '#E0E4CC';
                                break;
                        }

                        $veiculo_id = $r->veiculo_id;
                        $veiculoStr = '';

                        if ($veiculo_id) {
                            $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                            if (count($veiculo) > 0) {
                                $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                            }
                        }

                        $cliente = $this->clientes_model->getById($r->clientes_id);
                        $dados_cliente = $cliente->nomeCliente;

                        if ($cliente->telefone) {
                            $dados_cliente = $dados_cliente .'<br/>'.$cliente->telefone;
                        }

                        if ($cliente->celular) {
                            $dados_cliente = $dados_cliente .'<br/>'.$cliente->celular;
                        }

                        $usuario = $this->usuarios_model->getById($r->usuarios_id);

                        echo '<tr>';
                        echo '    <td>' . $r->idVistoria . '</td>';
                        echo '    <td>' . $dados_cliente .'<br/>'.$veiculoStr . '</td>';
                        echo '    <td>' . $usuario->nome . '</td>';
                        echo '    <td style="text-align: center;">' . date(('d/m/Y'), strtotime($r->datavistoria)) .'<br/>' . $r->horavistoria . '</td>';
                        echo '    <td style="text-align: center;">' . $r->gnv . '</td>';
                        echo '    <td style="text-align: center;">' . $r->tanque . '</td>';
                        echo '    <td style="text-align: center;">' . $r->kmentrada.'km<br/>'. $r->kmsaida . 'km</td>';
                        echo '    <td style="text-align: center"><span class="badge" style="background-color: '.$cor.'; border-color: '.$cor.'">'.$r->status.'</span> </td>';
                        echo '    <td style="text-align: center;">';
                        echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/vistoria/editar/' . $r->idVistoria . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                        echo '      <a href="#modal-excluir" role="button" data-toggle="modal" vistoria="' . $r->idVistoria . '" class="btn btn-danger tip-top" ><i class="icon-remove icon-white"></i></a>  ';
                        echo '    </td>';
                        echo '</tr>';
                    }
                    ?>
                    <tr>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="span12" style="margin-left: 0; margin-top: 0">
        <!--Serviços-->
        <div class="span6" style="margin-left: 0">
            <div class="widget-box" style="min-height: 200px">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-wrench"></i>
                    </span>
                    <h5>Serviços</h5>

                </div>

                <div class="widget-content nopadding">
                    <table class="table table-responsive">
                        <thead>
                        <tr style="backgroud-color: #2D335B">
                            <th>#</th>
                            <th>Nome</th>
                            <th>Preço</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($servicos == null) {
                            echo '<tr><td colspan="4">Nenhum serviço foi encontrado.</td></tr>';
                        }
                        foreach ($servicos as $r) {
                            echo '<tr>';
                            echo '<td>' . $r->idServicos . '</td>';
                            echo '<td>' . $r->nome . '</td>';
                            echo '<td>' . $r->preco . '</td>';
                            echo '<td>';
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eServico')) {
                                echo '<a href="' . base_url() . 'index.php/servicos/editar/' . $r->idServicos . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                            }

                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                        <tr>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--Produtoss-->
        <div class="span6" style="margin-top: 0">
            <div class="widget-box" style="min-height: 200px">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-barcode"></i>
                    </span>
                    <h5>Produtos</h5>

                </div>

                <div class="widget-content nopadding">
                    <table class="table table-responsive">
                        <thead>
                        <tr style="backgroud-color: #2D335B">
                            <th>#</th>
                            <th>Nome</th>
                            <th>Preço</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($produtos == null) {
                            echo '<tr><td colspan="4">Nenhum produto foi encontrado.</td></tr>';
                        }
                        foreach ($produtos as $r) {
                            echo '<tr>';
                            echo '<td>' . $r->idProdutos . '</td>';
                            echo '<td>' . $r->descricao . '</td>';
                            echo '<td>' . $r->precoVenda . '</td>';

                            echo '<td>';
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
                                echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/produtos/visualizar/' . $r->idProdutos . '" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                            }
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eProduto')) {
                                echo '<a href="' . base_url() . 'index.php/produtos/editar/' . $r->idProdutos . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                            }

                            echo '</td>';
                            echo '</tr>';
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

