<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>.:<?php echo TITLE_SISTEMA;?>:.</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/matrix-login.css"/>
    <script src="<?php echo base_url() ?>js/jquery-1.10.2.min.js"></script>

    <style>
        #loginbox .normal_text {
            background: #38547b;!important;
        }
    </style>

</head>
<body>
<div id="loginbox">
    <form class="form-vertical" id="formLogin" method="post"
          action="<?php echo base_url() ?>index.php/master/login">
        <?php if ($this->session->flashdata('error') != null) { ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php } ?>
        <div class="control-group normal_text">
            <h4><img src="<?php echo base_url() ?>assets/img/<?php echo PACOTE;?>/logo_master.png" alt="Logo"/><br/>Administrador do Sistema</h4>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"><i class="icon-user"></i></span><input id="masterlogin" required="required" name="masterlogin"
                                                                                      type="text" placeholder="Login"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"><i class="icon-lock"></i></span><input name="senha" required="required" type="password"
                                                                                      placeholder="Senha"/>
                </div>
            </div>
        </div>
        <div class="form-actions" style="text-align: center">
            <button class="btn btn-info btn-large" type="submit">Acessar</button>
        </div>
    </form>
</div>

<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.backstretch.min.js"></script>

<script type="text/javascript">
    $.backstretch("<?php echo base_url()?>/assets/img/background.jpeg", {speed: 500});
</script>


<a href="#notification" id="call-modal" role="button" class="btn" data-toggle="modal" style="display: none ">notification</a>

<div id="notification" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">.:<?php echo NOME_SISTEMA;?>:.</h4>
    </div>
    <div class="modal-body">
        <h5 style="text-align: center">Os dados de acesso estão incorretos, por favor tente novamente!</h5>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Fechar</button>

    </div>
</div>


</body>

</html>









