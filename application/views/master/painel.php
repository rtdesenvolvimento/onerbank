<!--[if lt IE 9]>
<script language="javascript" type="text/javascript"
        src="<?php echo base_url();?>js/dist/excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript"
        src="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/dist/jquery.jqplot.min.css"/>

<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dist/plugins/jqplot.donutRenderer.min.js"></script>


<div class="row-fluid" style="margin-top: 0">

    <div class="span12">

        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Estatísticas do
                    Sistema</h5></div>
            <div class="widget-content">
                <div class="row-fluid">
                    <div class="span12">
                        <ul class="site-stats">
                            <li class="bg_lh"><i class="icon-group"></i>
                                <strong><?php echo $this->db->count_all('empresa'); ?></strong>
                                <small>Oficinas</small>
                            </li>
                            <li class="bg_lh"><i class="icon-barcode"></i>
                                <strong><?php echo $this->db->count_all('filial'); ?></strong>
                                <small>Filiais</small>
                            </li>
                            <li class="bg_lh"><i class="icon-download"></i>
                                <strong><a href="https://www.smarticontrol.com.br/bankoff00001.zip" target="blank"></strong>
                                <small>Download do bando de dados para instalação</small>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>



