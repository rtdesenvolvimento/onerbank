<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Conecte - Área do Cliente</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-media.css"/>
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css"/>


    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
</head>

<body>

<!--Header-part-->
<div id="header">
    <h1><a href="dashboard.html">MapOS</a></h1>
</div>
<!--close-Header-part-->

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">

        <li class=""><a href="<?php echo base_url() ?>index.php/master/sair"><i
                        class="icon icon-share-alt"></i> <span class="text"> Sair</span></a></li>
    </ul>
</div>


<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-list"></i> Menu</a>
    <ul>
        <li class="<?php if (isset($menuPainel)) {
            echo 'active';
        }; ?>"><a href="<?php echo base_url() ?>index.php/master/painel"><i class="icon icon-home"></i>
                <span>Painel</span></a></li>
        <li class="<?php if (isset($menuEmpresa)) {
            echo 'active';
        }; ?>"><a href="<?php echo base_url() ?>index.php/empresa"><i class="icon icon-heart"></i> <span>Oficinas</span></a>
        </li>

        <li class="<?php if (isset($menuContador)) {
            echo 'active';
        }; ?>"><a href="<?php echo base_url() ?>index.php/contador"><i class="icon icon-user"></i> <span>Contador</span></a>
        </li>

        <li class="<?php if (isset($menuConfiguracaoTecnoSpeed)) {
            echo 'active';
        }; ?>"><a href="<?php echo base_url() ?>index.php/configuracaotecnospeed"><i class="icon icon-tags"></i> <span>Grupo TecnoSpeed</span></a>
        </li>

        <li class=""><a href="<?php echo base_url() ?>index.php/master/sair"><i class="icon icon-share-alt"></i> <span>Sair</span></a></li>
    </ul>
</div>


<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="index.html" class="tip-bottom"><i class="icon-home"></i>
                Home</a></div>
    </div>

    <div class="container-fluid">
        <div class="row-fluid">

            <div class="span12">
                <?php if ($this->session->flashdata('error') != null) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success') != null) { ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <?php if (isset($output)) {
                    $this->load->view($output);
                } ?>
            </div>
        </div>

    </div>
</div>
<!--Footer-part-->
<div class="row-fluid">
    <div id="footer" class="span12"> <?php echo date('Y'); ?> © <?php echo NOME_SISTEMA;?> (<?php echo VERSAO;?>)</div>
</div>

<!-- javascript
================================================== -->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


</body>
</html>
