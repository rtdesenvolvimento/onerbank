<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Natureza de operação</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formNaturezaoperacao" method="post" class="form-horizontal" >

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome</label>
                        <div class="controls">
                            <input id="nome" class="span8" type="text" name="nome" required="required" value="<?php echo set_value('nome'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="ICMS" class="control-label">CST/CSOSN <span class="required">*</span></label>
                        <div class="controls">
                            <select name="ICMS" id="ICMS" class="span6" required="required">
                                <option value=""></option>
                                <option value="900">Simples Nacional: 900: Outros </option>
                                <option value="500">Simples Nacional: 500: ICMS cobrado antes por subst trib ou antecipação </option>
                                <option value="400">Simples Nacional: 400: Não tributada </option>
                                <option value="300">Simples Nacional: 300: Imune </option>
                                <option value="203">Simples Nacional: 203: Isenção ICMS p/ faixa de receita bruta e cobr do ICMS por ST </option>
                                <option value="202">Simples Nacional: 202: Sem permissão de crédito, com cobr ICMS por Subst Trib </option>
                                <option value="201">Simples Nacional: 201: Com permissão de crédito, com cobr ICMS por Subst Trib </option>
                                <option value="103">Simples Nacional: 103: Isenção do ICMS para faixa de receita bruta </option>
                                <option value="102">Simples Nacional: 102: Sem permissão de crédito </option>
                                <option value="101">Simples Nacional: 101: Com permissão de crédito </option>
                                <option value="41ST">Repasse 41: ICMS ST retido em operações interestaduais com repasses do Subst Trib </option>
                                <option value="90Part">Partilha 90: Entre UF origem e destino ou definida na legislação - outros </option>
                                <option value="10Part">Partilha 10: Entre UF origem e destino ou definida na legislação com Subst Trib </option>
                                <option value="90">90: Outros </option><option value="70"> 70: Redução de Base Calc e cobr ICMS por subst trib </option>
                                <option value="60">60: ICMS cobrado anteriormente por subst trib </option>
                                <option value="51">51: Diferimento </option>
                                <option value="50">50: Suspesão </option>
                                <option value="41">41: Não tributada </option>
                                <option value="40">40: Isenta </option><option value="30"> 30: Isenta ou não trib com cobr por subst trib </option>
                                <option value="20">20: Com redução de base de cálculo </option>
                                <option value="10">10: Tributada com cobr. por subst. trib. </option>
                                <option value="00">00: Tributada integralmente </option>
                            </select>
                        </div>
                    </div>


                    <div class="control-group">
                        <label for="IPI" class="control-label">IPI</label>
                        <div class="controls">
                            <select name="IPI" id="IPI">
                                <option value=""></option>
                                <option value="00">00: Entrada com recuperação de crédito</option>
                                <option value="01">01: Entrada tributada com alíquota zero </option>
                                <option value="02">02: Entrada isenta</option>
                                <option value="03">03: Entrada não-tributada</option>
                                <option value="04">04: Entrada imune</option>
                                <option value="05">05: Entrada com suspensão</option>
                                <option value="49">49: Outras entradas</option>
                                <option value="50">50: Saída tributada</option>
                                <option value="51">51: Saída tributada com alíquota zero</option>
                                <option value="52">52: Saída isenta</option>
                                <option value="53">53: Saída não-tributada</option>
                                <option value="54">54: Saída imune</option>
                                <option value="55">55: Saída com suspensão</option>
                                <option value="99">99: Outras saídas</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="PIS" class="control-label">PIS</label>
                        <div class="controls">
                            <select name="PIS" id="PIS" class="span6">
                                <option value=""></option>
                                <option value="01"> 01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.) </option>
                                <option value="02"> 02: Operação tributável (BC = valor da operação (alíquota diferenciada) </option>
                                <option value="03"> 03: Operação tributável (BC = quant. x alíq. por unidade de produto) </option>
                                <option value="04"> 04: Operação tributável (tributação monofásica, alíquota zero) </option>
                                <option value="06"> 06: Operação tributável (alíquota zero) </option>
                                <option value="07"> 07: Operação isenta da contribuição </option>
                                <option value="08"> 08: Operação sem incidência da contribuição </option>
                                <option value="09"> 09: Operação com suspensão da contribuição </option>
                                <option value="49"> 49: Outras Operações de Saída </option>
                                <option value="50"> 50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno </option>
                                <option value="51"> 51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno </option>
                                <option value="52"> 52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação </option>
                                <option value="53"> 53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno </option>
                                <option value="54"> 54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação </option>
                                <option value="55"> 55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação </option>
                                <option value="56"> 56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                <option value="60"> 60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno </option>
                                <option value="61"> 61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno </option>
                                <option value="62"> 62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação </option>
                                <option value="63"> 63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno </option>
                                <option value="64"> 64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação </option>
                                <option value="65"> 65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação </option>
                                <option value="66"> 66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                <option value="67"> 67: Crédito Presumido - Outras Operações </option>
                                <option value="70"> 70: Operação de Aquisição sem Direito a Crédito </option>
                                <option value="71"> 71: Operação de Aquisição com Isenção </option>
                                <option value="72"> 72: Operação de Aquisição com Suspensão </option>
                                <option value="73"> 73: Operação de Aquisição a Alíquota Zero </option>
                                <option value="74"> 74: Operação de Aquisição sem Incidência da Contribuição </option>
                                <option value="75"> 75: Operação de Aquisição por Substituição Tributária </option>
                                <option value="98"> 98: Outras Operações de Entrada </option>
                                <option value="99"> 99: Outras operações </option>
                            </select>
                        </div>
                    </div>


                    <div class="control-group">
                        <label for="COFINS" class="control-label">COFINS</label>
                        <div class="controls">
                            <select name="COFINS" id="COFINS" class="span6">
                                <option value=""></option>
                                <option value="01"> 01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.) </option>
                                <option value="02"> 02: Operação tributável (BC = valor da operação (alíquota diferenciada) </option>
                                <option value="03"> 03: Operação tributável (BC = quant. x alíq. por unidade de produto) </option>
                                <option value="04"> 04: Operação tributável (tributação monofásica, alíquota zero) </option>
                                <option value="06"> 06: Operação tributável (alíquota zero) </option>
                                <option value="07"> 07: Operação isenta da contribuição </option>
                                <option value="08"> 08: Operação sem incidência da contribuição </option>
                                <option value="09"> 09: Operação com suspensão da contribuição </option>
                                <option value="49"> 49: Outras Operações de Saída </option>
                                <option value="50"> 50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno </option>
                                <option value="51"> 51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno </option>
                                <option value="52"> 52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação </option>
                                <option value="53"> 53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno </option>
                                <option value="54"> 54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação </option>
                                <option value="55"> 55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação </option>
                                <option value="56"> 56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                <option value="60"> 60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno </option>
                                <option value="61"> 61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno </option>
                                <option value="62"> 62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação </option>
                                <option value="63"> 63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno </option>
                                <option value="64"> 64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação </option>
                                <option value="65"> 65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação </option>
                                <option value="66"> 66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                <option value="67"> 67: Crédito Presumido - Outras Operações </option>
                                <option value="70"> 70: Operação de Aquisição sem Direito a Crédito </option>
                                <option value="71"> 71: Operação de Aquisição com Isenção </option>
                                <option value="72"> 72: Operação de Aquisição com Suspensão </option>
                                <option value="73"> 73: Operação de Aquisição a Alíquota Zero </option>
                                <option value="74"> 74: Operação de Aquisição sem Incidência da Contribuição </option>
                                <option value="75"> 75: Operação de Aquisição por Substituição Tributária </option>
                                <option value="98"> 98: Outras Operações de Entrada </option>
                                <option value="99"> 99: Outras operações </option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="CFOP" class="control-label">CFOP</label>
                        <div class="controls">
                            <input id="CFOP" class="span6" type="text" name="CFOP" required="required" value="<?php echo set_value('CFOP'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="tipoEstoque" class="control-label">Configuração de Estoque</label>
                        <div class="controls">
                            <select name="tipoEstoque" id="tipoEstoque">
                                <option value="NAOMOVER">NÃO MOVIMENTAR ESTOQUE</option>
                                <option value="SAIDA">BAIXAR ESTOQUE</option>
                                <option value="ENTRADA">ENTRADA DE ESTOQUE</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/naturezaoperacao" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#formNaturezaoperacao').validate({
            rules :{
                nome:{ required: true}
            },
            messages:{
                nome :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>





