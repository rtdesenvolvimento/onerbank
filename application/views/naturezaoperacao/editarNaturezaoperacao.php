<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Natureza de operação</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formNaturezaoperacao" method="post" class="form-horizontal" >
                    <?php echo form_hidden('idNaturezaoperacao', $result->idNaturezaoperacao) ?>
                    <div class="control-group">
                        <label for="nome" class="control-label">Nome</label>
                        <div class="controls">
                            <input id="nome" class="span8" type="text" name="nome" required="required" value="<?php echo $result->nome ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="ICMS" class="control-label">CST/CSOSN <span class="required">*</span></label>
                        <div class="controls">
                            <select name="ICMS" id="ICMS" class="span6" required="required">
                                <option <?php if ($result->ICMS == '') echo 'selected="selected"';?> value=""></option>
                                <option <?php if ($result->ICMS == '900') echo 'selected="selected"';?> value="900">Simples Nacional: 900: Outros </option>
                                <option <?php if ($result->ICMS == '500') echo 'selected="selected"';?> value="500">Simples Nacional: 500: ICMS cobrado antes por subst trib ou antecipação </option>
                                <option <?php if ($result->ICMS == '400') echo 'selected="selected"';?> value="400">Simples Nacional: 400: Não tributada </option>
                                <option <?php if ($result->ICMS == '300') echo 'selected="selected"';?> value="300">Simples Nacional: 300: Imune </option>
                                <option <?php if ($result->ICMS == '203') echo 'selected="selected"';?> value="203">Simples Nacional: 203: Isenção ICMS p/ faixa de receita bruta e cobr do ICMS por ST </option>
                                <option <?php if ($result->ICMS == '202') echo 'selected="selected"';?> value="202">Simples Nacional: 202: Sem permissão de crédito, com cobr ICMS por Subst Trib </option>
                                <option <?php if ($result->ICMS == '201') echo 'selected="selected"';?> value="201">Simples Nacional: 201: Com permissão de crédito, com cobr ICMS por Subst Trib </option>
                                <option <?php if ($result->ICMS == '103') echo 'selected="selected"';?> value="103">Simples Nacional: 103: Isenção do ICMS para faixa de receita bruta </option>
                                <option <?php if ($result->ICMS == '102') echo 'selected="selected"';?> value="102">Simples Nacional: 102: Sem permissão de crédito </option>
                                <option <?php if ($result->ICMS == '101') echo 'selected="selected"';?> value="101">Simples Nacional: 101: Com permissão de crédito </option>
                                <option <?php if ($result->ICMS == '41ST') echo 'selected="selected"';?> value="41ST">Repasse 41: ICMS ST retido em operações interestaduais com repasses do Subst Trib </option>
                                <option <?php if ($result->ICMS == '90Part') echo 'selected="selected"';?> value="90Part">Partilha 90: Entre UF origem e destino ou definida na legislação - outros </option>
                                <option <?php if ($result->ICMS == '10Part') echo 'selected="selected"';?> value="10Part">Partilha 10: Entre UF origem e destino ou definida na legislação com Subst Trib </option>
                                <option <?php if ($result->ICMS == '90') echo 'selected="selected"';?> value="90">90: Outros </option><option value="70"> 70: Redução de Base Calc e cobr ICMS por subst trib </option>
                                <option <?php if ($result->ICMS == '60') echo 'selected="selected"';?> value="60">60: ICMS cobrado anteriormente por subst trib </option>
                                <option <?php if ($result->ICMS == '51') echo 'selected="selected"';?> value="51">51: Diferimento </option>
                                <option <?php if ($result->ICMS == '50') echo 'selected="selected"';?> value="50">50: Suspesão </option>
                                <option <?php if ($result->ICMS == '41') echo 'selected="selected"';?> value="41">41: Não tributada </option>
                                <option <?php if ($result->ICMS == '40') echo 'selected="selected"';?> value="40">40: Isenta </option><option value="30"> 30: Isenta ou não trib com cobr por subst trib </option>
                                <option <?php if ($result->ICMS == '20') echo 'selected="selected"';?> value="20">20: Com redução de base de cálculo </option>
                                <option <?php if ($result->ICMS == '10') echo 'selected="selected"';?> value="10">10: Tributada com cobr. por subst. trib. </option>
                                <option <?php if ($result->ICMS == '00') echo 'selected="selected"';?> value="00">00: Tributada integralmente </option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="IPI" class="control-label">IPI</label>
                        <div class="controls">
                            <select name="IPI" id="IPI" class="span6">
                                <option <?php if ($result->IPI == '') echo 'selected="selected"';?> value=""></option>
                                <option <?php if ($result->IPI == '00') echo 'selected="selected"';?> value="00">00: Entrada com recuperação de crédito</option>
                                <option <?php if ($result->IPI == '01') echo 'selected="selected"';?> value="01">01: Entrada tributada com alíquota zero </option>
                                <option <?php if ($result->IPI == '02') echo 'selected="selected"';?> value="02">02: Entrada isenta</option>
                                <option <?php if ($result->IPI == '03') echo 'selected="selected"';?> value="03">03: Entrada não-tributada</option>
                                <option <?php if ($result->IPI == '04') echo 'selected="selected"';?> value="04">04: Entrada imune</option>
                                <option <?php if ($result->IPI == '05') echo 'selected="selected"';?> value="05">05: Entrada com suspensão</option>
                                <option <?php if ($result->IPI == '49') echo 'selected="selected"';?> value="49">49: Outras entradas</option>
                                <option <?php if ($result->IPI == '50') echo 'selected="selected"';?> value="50">50: Saída tributada</option>
                                <option <?php if ($result->IPI == '51') echo 'selected="selected"';?> value="51">51: Saída tributada com alíquota zero</option>
                                <option <?php if ($result->IPI == '52') echo 'selected="selected"';?> value="52">52: Saída isenta</option>
                                <option <?php if ($result->IPI == '53') echo 'selected="selected"';?> value="53">53: Saída não-tributada</option>
                                <option <?php if ($result->IPI == '54') echo 'selected="selected"';?> value="54">54: Saída imune</option>
                                <option <?php if ($result->IPI == '55') echo 'selected="selected"';?> value="55">55: Saída com suspensão</option>
                                <option <?php if ($result->IPI == '99') echo 'selected="selected"';?> value="99">99: Outras saídas</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="PIS" class="control-label">PIS</label>
                        <div class="controls">
                            <select name="PIS" id="PIS" class="span6">
                                <option <?php if ($result->PIS == '') echo 'selected="selected"';?> value=""></option>
                                <option <?php if ($result->PIS == '01') echo 'selected="selected"';?> value="01">01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.)</option>
                                <option <?php if ($result->PIS == '02') echo 'selected="selected"';?> value="02">02: Operação tributável (BC = valor da operação (alíquota diferenciada)</option>
                                <option <?php if ($result->PIS == '03') echo 'selected="selected"';?> value="03">03: Operação tributável (BC = quant. x alíq. por unidade de produto)</option>
                                <option <?php if ($result->PIS == '04') echo 'selected="selected"';?> value="04">04: Operação tributável (tributação monofásica, alíquota zero)</option>
                                <option <?php if ($result->PIS == '06') echo 'selected="selected"';?> value="06">06: Operação tributável (alíquota zero)</option>
                                <option <?php if ($result->PIS == '07') echo 'selected="selected"';?> value="07">07: Operação isenta da contribuição</option>
                                <option <?php if ($result->PIS == '08') echo 'selected="selected"';?> value="08">08: Operação sem incidência da contribuição</option>
                                <option <?php if ($result->PIS == '09') echo 'selected="selected"';?> value="09">09: Operação com suspensão da contribuição</option>
                                <option <?php if ($result->PIS == '49') echo 'selected="selected"';?> value="49">49: Outras Operações de Saída</option>
                                <option <?php if ($result->PIS == '50') echo 'selected="selected"';?> value="50">50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                <option <?php if ($result->PIS == '51') echo 'selected="selected"';?> value="51">51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                                <option <?php if ($result->PIS == '52') echo 'selected="selected"';?> value="52">52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação</option>
                                <option <?php if ($result->PIS == '53') echo 'selected="selected"';?> value="53">53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                <option <?php if ($result->PIS == '54') echo 'selected="selected"';?> value="54">54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                <option <?php if ($result->PIS == '55') echo 'selected="selected"';?> value="55">55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação</option>
                                <option <?php if ($result->PIS == '56') echo 'selected="selected"';?> value="56">56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                <option <?php if ($result->PIS == '60') echo 'selected="selected"';?> value="60">60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno</option>
                                <option <?php if ($result->PIS == '61') echo 'selected="selected"';?> value="61">61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno</option>
                                <option <?php if ($result->PIS == '62') echo 'selected="selected"';?> value="62">62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação</option>
                                <option <?php if ($result->PIS == '63') echo 'selected="selected"';?> value="63">63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno</option>
                                <option <?php if ($result->PIS == '64') echo 'selected="selected"';?> value="64">64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação</option>
                                <option <?php if ($result->PIS == '65') echo 'selected="selected"';?> value="65">65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação</option>
                                <option <?php if ($result->PIS == '66') echo 'selected="selected"';?> value="66">66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                <option <?php if ($result->PIS == '67') echo 'selected="selected"';?> value="67">67: Crédito Presumido - Outras Operações</option>
                                <option <?php if ($result->PIS == '70') echo 'selected="selected"';?> value="70">70: Operação de Aquisição sem Direito a Crédito</option>
                                <option <?php if ($result->PIS == '71') echo 'selected="selected"';?> value="71">71: Operação de Aquisição com Isenção</option>
                                <option <?php if ($result->PIS == '72') echo 'selected="selected"';?> value="72">72: Operação de Aquisição com Suspensão</option>
                                <option <?php if ($result->PIS == '73') echo 'selected="selected"';?> value="73">73: Operação de Aquisição a Alíquota Zero</option>
                                <option <?php if ($result->PIS == '74') echo 'selected="selected"';?> value="74">74: Operação de Aquisição sem Incidência da Contribuição</option>
                                <option <?php if ($result->PIS == '75') echo 'selected="selected"';?> value="75">75: Operação de Aquisição por Substituição Tributária</option>
                                <option <?php if ($result->PIS == '98') echo 'selected="selected"';?> value="98">98: Outras Operações de Entrada</option>
                                <option <?php if ($result->PIS == '99') echo 'selected="selected"';?> value="99">99: Outras operações</option>
                            </select>
                        </div>
                    </div>


                    <div class="control-group">
                        <label for="COFINS" class="control-label">COFINS</label>
                        <div class="controls">
                            <select name="COFINS" id="COFINS" class="span6">
                                <option <?php if ($result->COFINS == '') echo 'selected="selected"';?> value=""></option>
                                <option <?php if ($result->COFINS == '01') echo 'selected="selected"';?> value="01">01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.)</option>
                                <option <?php if ($result->COFINS == '02') echo 'selected="selected"';?> value="02">02: Operação tributável (BC = valor da operação (alíquota diferenciada)</option>
                                <option <?php if ($result->COFINS == '03') echo 'selected="selected"';?> value="03">03: Operação tributável (BC = quant. x alíq. por unidade de produto)</option>
                                <option <?php if ($result->COFINS == '04') echo 'selected="selected"';?> value="04">04: Operação tributável (tributação monofásica, alíquota zero)</option>
                                <option <?php if ($result->COFINS == '06') echo 'selected="selected"';?> value="06">06: Operação tributável (alíquota zero)</option>
                                <option <?php if ($result->COFINS == '07') echo 'selected="selected"';?> value="07">07: Operação isenta da contribuição</option>
                                <option <?php if ($result->COFINS == '08') echo 'selected="selected"';?> value="08">08: Operação sem incidência da contribuição</option>
                                <option <?php if ($result->COFINS == '09') echo 'selected="selected"';?> value="09">09: Operação com suspensão da contribuição</option>
                                <option <?php if ($result->COFINS == '49') echo 'selected="selected"';?> value="49">49: Outras Operações de Saída</option>
                                <option <?php if ($result->COFINS == '50') echo 'selected="selected"';?> value="50">50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                <option <?php if ($result->COFINS == '51') echo 'selected="selected"';?> value="51">51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                                <option <?php if ($result->COFINS == '52') echo 'selected="selected"';?> value="52">52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação</option>
                                <option <?php if ($result->COFINS == '53') echo 'selected="selected"';?> value="53">53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                <option <?php if ($result->COFINS == '54') echo 'selected="selected"';?> value="54">54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                <option <?php if ($result->COFINS == '55') echo 'selected="selected"';?> value="55">55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação</option>
                                <option <?php if ($result->COFINS == '56') echo 'selected="selected"';?> value="56">56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                <option <?php if ($result->COFINS == '60') echo 'selected="selected"';?> value="60">60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno</option>
                                <option <?php if ($result->COFINS == '61') echo 'selected="selected"';?> value="61">61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno</option>
                                <option <?php if ($result->COFINS == '62') echo 'selected="selected"';?> value="62">62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação</option>
                                <option <?php if ($result->COFINS == '63') echo 'selected="selected"';?> value="63">63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno</option>
                                <option <?php if ($result->COFINS == '64') echo 'selected="selected"';?> value="64">64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação</option>
                                <option <?php if ($result->COFINS == '65') echo 'selected="selected"';?> value="65">65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação</option>
                                <option <?php if ($result->COFINS == '66') echo 'selected="selected"';?> value="66">66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                <option <?php if ($result->COFINS == '67') echo 'selected="selected"';?> value="67">67: Crédito Presumido - Outras Operações</option>
                                <option <?php if ($result->COFINS == '70') echo 'selected="selected"';?> value="70">70: Operação de Aquisição sem Direito a Crédito</option>
                                <option <?php if ($result->COFINS == '71') echo 'selected="selected"';?> value="71">71: Operação de Aquisição com Isenção</option>
                                <option <?php if ($result->COFINS == '72') echo 'selected="selected"';?> value="72">72: Operação de Aquisição com Suspensão</option>
                                <option <?php if ($result->COFINS == '73') echo 'selected="selected"';?> value="73">73: Operação de Aquisição a Alíquota Zero</option>
                                <option <?php if ($result->COFINS == '74') echo 'selected="selected"';?> value="74">74: Operação de Aquisição sem Incidência da Contribuição</option>
                                <option <?php if ($result->COFINS == '75') echo 'selected="selected"';?> value="75">75: Operação de Aquisição por Substituição Tributária</option>
                                <option <?php if ($result->COFINS == '98') echo 'selected="selected"';?> value="98">98: Outras Operações de Entrada</option>
                                <option <?php if ($result->COFINS == '99') echo 'selected="selected"';?> value="99">99: Outras operações</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="tipoEstoque" class="control-label">Configuração de Estoque</label>
                        <div class="controls">
                            <select name="tipoEstoque" id="tipoEstoque">
                                <option <?php if ($result->tipoEstoque == 'NAOMOVER') echo 'selected="selected"';?> value="NAOMOVER">NÃO MOVIMENTAR ESTOQUE</option>
                                <option <?php if ($result->tipoEstoque == 'SAIDA') echo 'selected="selected"';?> value="SAIDA">BAIXAR ESTOQUE</option>
                                <option <?php if ($result->tipoEstoque == 'ENTRADA') echo 'selected="selected"';?> value="ENTRADA">ENTRADA DE ESTOQUE</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="CFOP" class="control-label">CFOP</label>
                        <div class="controls">
                            <input id="CFOP" class="span6" type="text" name="CFOP" required="required" value="<?php echo $result->CFOP ?>"  />
                        </div>
                    </div>


                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/naturezaoperacao" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#formNaturezaoperacao').validate({
            rules :{
                nome:{ required: true}
            },
            messages:{
                nome :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>





