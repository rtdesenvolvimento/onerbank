<?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aServico')) { ?>
    <a href="<?php echo base_url() ?>index.php/naturezaoperacao/adicionar" class="btn btn-success"><i
                class="icon-plus icon-white"></i> Adicionar Nova Natureza de operação</a>
<?php } ?>

<?php

if (!$results) {
    ?>

    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-wrench"></i>
         </span>
            <h5>Natureza de operação</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Descrição</th>
                    <th>ICMS</th>
                    <th>IPI</th>
                    <th>PIS</th>
                    <th>COFINS</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>


<?php } else { ?>

    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-wrench"></i>
         </span>
            <h5>Natureza de operação</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Descrição</th>
                    <th>ICMS</th>
                    <th>IPI</th>
                    <th>PIS</th>
                    <th>COFINS</th>
                    <th>CFOP</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    echo '<tr>';
                    echo '<td>' . $r->nome . '</td>';
                    echo '<td>' . $r->ICMS . '</td>';
                    echo '<td>' . $r->IPI . '</td>';
                    echo '<td>' . $r->PIS . '</td>';
                    echo '<td>' . $r->COFINS . '</td>';
                    echo '<td>' . $r->CFOP . '</td>';

                    echo '<td>';
                    echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/naturezaoperacao/editar/' . $r->idNaturezaoperacao . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '<a href="#modal-excluir" role="button" data-toggle="modal" naturezaoperacao="' . $r->idNaturezaoperacao . '" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>  ';
                    echo '</td>';
                    echo '</tr>';
                } ?>
                <tr>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php echo $this->pagination->create_links();
} ?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/naturezaoperacao/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Serviço</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idNaturezaoperacao" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir esta natureza de operação?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>




<script type="text/javascript">
    $(document).ready(function () {

        $("#buscar").keypress(function (event) {
            if (event.which == 13) {
                window.location.href = "<?php echo base_url() ?>index.php/naturezaoperacao/buscar/" + $(this).val();
            }
        });

        $(document).on('click', 'a', function (event) {
            var naturezaoperacao = $(this).attr('naturezaoperacao');
            $('#idNaturezaoperacao').val(naturezaoperacao);

        });

    });

</script>