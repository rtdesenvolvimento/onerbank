<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de NFC-e <small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="post" id="formNFSe">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 ">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Dados da Nota Fiscal</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-12 " style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="modelo" >Modelo<span class="required">*</span></label>
                                                                <input id="modelo"  class="form-control" readonly type="text" value="65" name="modelo">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="serie" >Série<span class="required">*</span></label>
                                                                <input id="serie"  class="form-control" readonly type="text" value="<?php echo $nSerie;?>" name="serie">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="nNF" >Nº<span class="required">*</span></label>
                                                                <input id="nNF"  class="form-control" maxlength="9" readonly type="text" value="<?php echo $nNF;?>" name="nNF">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <label for="dhEmi" >Data Emissão<span class="required">*</span></label>
                                                                <input id="dhEmi"  class="form-control" type="date" value="<?php echo date('Y-m-d');?>" name="dhEmi">
                                                            </td>

                                                            <td colspan="4">
                                                                <label for="hEmi" >Hora Emissão<span class="required">*</span></label>
                                                                <input id="hEmi"  class="form-control" maxlength="5" type="time" value="<?php echo date('H:i');?>" name="hEmi">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <label for="indPag" >Forma Pag.<span class="required">*</span></label>
                                                                <select name="indPag" class="form-control" required="">
                                                                    <option value="0"> À Vista</option>
                                                                    <option value="1"> À Prazo</option>
                                                                    <option value="2">Outros</option>
                                                                </select>
                                                            </td>

                                                            <td colspan="4">
                                                                <label for="tpEmis" >Forma Emi.<span class="required">*</span></label>
                                                                <select name="tpEmis" class="form-control" required="">
                                                                    <option value="1"> Normal</option>
                                                                    <option value="2"> Contingência FS-IA</option>
                                                                    <option value="3"> Contingência SCAN</option>
                                                                    <option value="4"> Contingência DPEC</option>
                                                                    <option value="5"> Contingência FS-DA</option>
                                                                    <option value="6"> Contingência SVC-AN</option>
                                                                    <option value="7"> Contingência SVC-RS</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <label for="finNFe" >Finalidade<span class="required">*</span></label>
                                                                <select name="finNFe" class="form-control" required="">
                                                                    <option value="1"> Normal</option>
                                                                    <option value="2"> Complementar</option>
                                                                    <option value="3"> Ajuste</option>
                                                                    <option value="4"> Devolução de Merc.</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <label for="indPres" >Tipo Atend.<span class="required">*</span></label>
                                                                <select name="indPres" class="form-control" required="">
                                                                    <option value="0"> Não se aplica</option>
                                                                    <option value="1"> Presencial</option>
                                                                    <option value="2"> Não presencial, pela internet</option>
                                                                    <option value="3"> Não presencial, Teleatendimento</option>
                                                                    <option value="5"> Operação presencial, fora do estabelecimento</option>
                                                                    <option value="9"> Não presencial, Outros</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <label for="natOp" >Nat. Operação<span class="required">*</span></label>
                                                                <input id="natOp"  class="form-control" value="VENDA DE MERCADORIA" required="required" type="text" name="natOp">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 ">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Dados do Cliente</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="span6" style="padding: 1%; margin-left: 0">
                                        <div class="span12">
                                            <div class="formBuscaGSA" id="divBuscaCliente">
                                                <div class="col-md-12 col-sm-12  form-group has-feedback">
                                                    <select name="clientes_id" class="form-control has-feedback-left" id="clientes_id" required="required">
                                                        <option value="">--Clique na lupa para consultar clientes.--</option>
                                                    </select>
                                                    <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;display: none;"></div>
                                                    <a href="#" data-target=".searchCliente" id="aSearchCliente" data-toggle="modal">
                                                        <span style="display: block" class="fa fa-search form-control-feedback left"></span>
                                                    </a>

                                                    <a href="#adicionarCliente" id="addCliente" data-toggle="modal" style="float: right;margin-right: 4px;display: none;"><i class="fa fa-plus-circle"></i></a>

                                                </div>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="display: none;">
                                                            <input id="destxNome"  class="form-control" type="text" required="required" name="destxNome">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <label for="destCNPJ" >CPF/CNPJ</label>
                                                            <input id="destCNPJ"  class="form-control" type="text" name="destCNPJ">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="destIE" >Insc. Estadual</label>
                                                            <input id="destIE"  class="form-control" type="text" name="destIE">
                                                        </td>

                                                        <td colspan="3">
                                                            <label for="destISUF" >Insc. SUFRAMA</label>
                                                            <input id="destISUF"  class="form-control" type="text" name="destISUF">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="6">
                                                            <label for="destxLgr" >Endereço</label>
                                                            <input id="destxLgr"  class="form-control" type="text" maxlength="60" name="destxLgr">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="destnro" >Número</label>
                                                            <input id="destnro"  class="form-control" type="text" maxlength="60" name="destnro">
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td colspan="8">
                                                            <label for="destxCpl" >Comp.</label>
                                                            <input id="destxCpl"  class="form-control" type="text" maxlength="60" name="destxCpl">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="destCEP" >CEP</label>
                                                            <input id="destCEP" onblur="getConsultaCEP();"  class="form-control" type="text" name="destCEP">
                                                        </td>

                                                        <td colspan="4">
                                                            <label for="destxBairro" >Bairro</label>
                                                            <input id="destxBairro"  class="form-control" type="text" maxlength="60" name="destxBairro">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <label for="destxMun" >Cidade</label>
                                                            <input id="destxMun"  class="form-control" type="text" value="" name="destxMun">
                                                            <input id="destcMun"  type="hidden" name="destcMun">
                                                        </td>
                                                        <td colspan="2">
                                                            <label for="destUF" >UF</label>
                                                            <input id="destUF"  class="form-control" type="text" value="" name="destUF">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="destfone" >Fone</label>
                                                            <input id="destfone"  class="form-control" type="text" name="destfone">
                                                        </td>
                                                        <td colspan="4">
                                                            <label for="" >E-mail</label>
                                                            <input id="destemail"  class="form-control" type="email" name="destemail">
                                                        </td>
                                                    </tr>
                                                    <tr><td colspan="8"></td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="span12" style="padding: 1%; margin-left: 0;display: none;">
                        <div class="spa12">
                            <div class="formBuscaGSA">
                                <label for="tecnico">Vendedor<span
                                            class="required">*</span></label>
                                <div class="controls">
                                    <select name="usuarios_id" class="form-control" id="usuarios_id" required="required">
                                        <option value="">--Selecione um cliente--</option>
                                        <?php foreach ($usuarios as $usuario) {?>
                                            <option value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                        <?php } ?>
                                    </select>

                                    <table width="100%">
                                        <tr>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                        </tr>
                                        <tr style="display:none;">
                                            <td colspan="4">
                                                <label for="comissaoVendedor" >Comissão</label>
                                                <input id="comissaoVendedor"  class="form-control" type="text" name="comissaoVendedor">
                                            </td>

                                            <td colspan="4">
                                                <label for="valorComissaoVendedor" >Valor da comissão</label>
                                                <input id="valorComissaoVendedor"  class="form-control" type="text" name="valorComissaoVendedor">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="span12" style="text-align: right;">
                        <div class="span6 offset3">
                            <button class="btn btn-success" id="btnContinuar"><i class="fa fa-save"></i> Adicionar</button>
                            <a href="<?php echo base_url() ?>index.php/nfce" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal cadastrar novo cliente -->
<div id="adicionarCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Cadastrar / Editar o cliente</h3>
        </div>

        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span4">
                    <label for="tipoPessoa">Tipo de pessoa</label>
                    <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                        <option value="PF">Pessoa Física</option>
                        <option value="PJ">Pessoa Jurídica</option>
                    </select>
                </div>

                <div class="span4">
                    <label for="origem">Origem</label>
                    <select name="origem" id="origem" class="span12 chzn" required="required">
                        <option value="">selecione uma origem</option>
                        <option value="Particular">Particular</option>
                        <option value="Financeira">Financeira</option>
                        <option value="Seguradora">Seguradora</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span6">
                    <label for="nomeCliente" >Nome / Razão Social<span class="required">*</span></label>
                    <input id="nomeCliente"  class="span12" type="text" required="required" name="nomeCliente"
                           value="<?php echo set_value('nomeCliente'); ?>"/>

                    <input type="hidden" id="cliente_id" name="cliente_id" value="" />
                </div>

                <div class="span6">
                    <label for="nomeFantasiaApelido" >Nome Fantasia / Apelido</label>
                    <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                           value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                </div>


            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span8">
                    <label for="documento">CPF/CNPJ*</label>
                    <input id="documento" type="text" class="span12" required="required" name="documento"
                           value="<?php echo set_value('documento'); ?>"/>
                </div>

                <div class="span4" id="div_sexo">
                    <label for="sexo">Sexo</label>
                    <select name="sexo" id="sexo" class="span12 chzn" required="required">
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0" id="div_rg">
                <div class="span3">
                    <label for="rg">RG</label>
                    <input id="rg" type="text" class="span12" name="rg"
                           value="<?php echo set_value('rg'); ?>"/>
                    </select>
                </div>

                <div class="span3">
                    <label for="orgaoEmissor">Orgão Emissor</label>
                    <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                           value="<?php echo set_value('orgaoEmissor'); ?>"/>
                </div>

                <div class="span2">
                    <label for="estadoOrgaoEmissor">Estado</label>
                    <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                           value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                </div>

                <div class="span4">
                    <label for="dataOrgaoEmissor">Data Emissão</label>
                    <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                           value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="telefone">Telefone</label>
                    <input id="telefone" type="text" class="span12" name="telefone"
                           value="<?php echo set_value('telefone'); ?>"/>
                </div>

                <div class="span6">
                    <label for="celular">Celular</label>
                    <input id="celular" type="text" class="span12" name="celular"
                           value="<?php echo set_value('celular'); ?>"/>
                </div>

            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="span12" name="email"
                           value="<?php echo set_value('email'); ?>"/>
                </div>

                <div class="span6">
                    <label for="email">Data de nascimento</label>
                    <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                           value="<?php echo set_value('data_nascimento'); ?>"/>
                </div>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Dados do endereço
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span3">
                    <label for="cep">CEP</label>
                    <input id="cep" type="text" name="cep" class="span12" onBlur="getConsultaCEP();"
                           value="<?php echo set_value('cep'); ?>"/>
                    <small>[TAB] consulta cep (Necessita Internet)</small>
                </div>

                <div class="span6">
                    <label for="rua">Rua</label>
                    <input id="rua" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                </div>

                <div class="span3">
                    <label for="numero">Número</label>
                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <label for="complemento">Complemento</label>
                    <input id="complemento" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span5">
                    <label for="bairro">Bairro</label>
                    <input id="bairro" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                </div>

                <div class="span4">
                    <label for="cidade">Cidade</label>
                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                </div>
                <div class="span3">
                    <label for="estado">Estado</label>
                    <input id="estado" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                </div>
            </div>

            <div id="div_contato" style="display: none;">
                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes">
                        <a href="#tab1" data-toggle="tab">
                            Dados do  Contato
                        </a>
                    </li>
                </ul>
                <div class="span12" style="padding: 1%; margin-left: 0">

                    <div class="span12">
                        <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                        <input id="contatoNomeCliente"  class="span12" type="text" name="contatoNomeCliente"
                               value="<?php echo set_value('contatoNomeCliente'); ?>"/>
                    </div>

                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span8">
                        <label for="contatoCpf">CPF</label>
                        <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                               value="<?php echo set_value('contatoCpf'); ?>"/>
                    </div>

                    <div class="span4">
                        <label for="contatoSexo">Sexo</label>
                        <select name="contatoSexo" id="contatoSexo" class="span12 chzn">
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                        </select>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="contatoTelefone">Telefone</label>
                        <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                               value="<?php echo set_value('contatoTelefone'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="contatoCelular">Celular</label>
                        <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                               value="<?php echo set_value('contatoCelular'); ?>"/>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="email">Email</label>
                        <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                               value="<?php echo set_value('contatoEmail'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="email">Data de nascimento</label>
                        <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                               value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Senha para acessar o portal do cliente
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <label for="senha">Senha</label>
                <input type="password" name="senha" class="span12" id="senha"
                       value=""/>
                <small>Deixe em branco e a senha padrão será o (cpj/cnpj) do cliente</small>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Observação
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <textarea class="span12" id="observacao"  name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6 offset3" style="text-align: center">
                    <button type="submit" class="btn btn-success">
                        <div id="div_buttom_adicionar"><i class="icon-plus icon-white"></i> Adicionar</div>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal searchCliente -->
<div class="modal fade searchCliente" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-users"></i> Consulta de clientes</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div id="div_searchCliente"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/notafiscal.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $('#usuarios_id').val(<?php echo  $this->session->userdata('id');?>);

        $("#formNFSe").validate({
            rules:{
                cliente: {required:true},
                tecnico: {required:true},
                dataVenda: {required:true}
            },
            messages:{
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataVenda: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $('#aSearchCliente').click(function (event) {
            $('#div_searchCliente').load("<?php echo base_url();?>index.php/clientes/search")
        });

        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });

        $('#clientes_id').change(function (e) {
            var clientes_id = $(this).val();
            if (clientes_id) {
                $('#pincliente').attr('class', 'icon-pencil icon-white');
                $('#div_buttom_adicionar').html("Editar");

                $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                $('#aAdicionarVeiculo').removeAttr('disabled');
            } else {
                $('#cliente_id').val('');
                $('#tipoPessoa').val('PF');
                $('#origem').val('');
                $('#nomeCliente').val('');
                $('#sexo').val('M');
                $('#nomeFantasiaApelido').val('');
                $('#documento').val('');
                $('#rg').val('');
                $('#orgaoEmissor').val('');
                $('#estadoOrgaoEmissor').val('');
                $('#dataOrgaoEmissor').val('');
                $('#data_nascimento').val('');
                $('#telefone').val('');
                $('#celular').val('');
                $('#email').val('');
                $('#rua').val('');
                $('#numero').val('');
                $('#bairro').val('');
                $('#cidade').val('');
                $('#estado').val('');
                $('#cep').val('');
                $('#complemento').val('');
                $('#observacao').val('');
                $('#contatoNomeCliente').val('');
                $('#contatoSexo').val('');
                $('#contatoCpf').val('');
                $('#contatoEmail').val('');
                $('#contatoDataNascimento').val('');
                $('#contatoTelefone').val('');
                $('#contatoCelular').val('');
                $('#dataCadastro').val('');
                $('#senha').val('');

                $('#pincliente').attr('class', 'icon-plus-sign icon-white');
                $('#div_buttom_adicionar').html("<i class=\"icon-plus icon-white\"></i> Adicionar");
            }
        });


        $('#addCliente').click(function (event) {
            event.preventDefault();
            var cliente_id = $('#clientes_id').val();

            if (cliente_id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/consultaCliente",
                    data: "cliente_id=" + cliente_id,
                    dataType: 'json',
                    success: function (cliente) {

                        $('#cliente_id').val(cliente_id);
                        $('#tipoPessoa').val(cliente.tipoPessoa);
                        $('#origem').val(cliente.origem);
                        $('#nomeCliente').val(cliente.nomeCliente);
                        $('#sexo').val(cliente.sexo);
                        $('#nomeFantasiaApelido').val(cliente.nomeFantasiaApelido);
                        $('#documento').val(cliente.documento);
                        $('#rg').val(cliente.rg);
                        $('#orgaoEmissor').val(cliente.orgaoEmissor);
                        $('#estadoOrgaoEmissor').val(cliente.estadoOrgaoEmissor);
                        $('#dataOrgaoEmissor').val(cliente.dataOrgaoEmissor);
                        $('#data_nascimento').val(cliente.data_nascimento);
                        $('#telefone').val(cliente.telefone);
                        $('#celular').val(cliente.celular);
                        $('#email').val(cliente.email);
                        $('#rua').val(cliente.rua);
                        $('#numero').val(cliente.numero);
                        $('#bairro').val(cliente.bairro);
                        $('#cidade').val(cliente.cidade);
                        $('#estado').val(cliente.estado);
                        $('#cep').val(cliente.cep);
                        $('#complemento').val(cliente.complemento);
                        $('#observacao').val(cliente.observacao);
                        $('#contatoNomeCliente').val(cliente.contatoNomeCliente);
                        $('#contatoSexo').val(cliente.contatoSexo);
                        $('#contatoCpf').val(cliente.contatoCpf);
                        $('#contatoEmail').val(cliente.contatoEmail);
                        $('#contatoDataNascimento').val(cliente.contatoDataNascimento);
                        $('#contatoTelefone').val(cliente.contatoTelefone);
                        $('#contatoCelular').val(cliente.contatoCelular);
                        $('#dataCadastro').val(cliente.dataCadastro);
                        $('#senha').val(cliente.senha);
                    }
                });
            }
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });


        $( "#formCliente" ).submit(function() {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/clientes/adicionarAjax",
                data: $("#formCliente").serialize(), // serializes the form's elements.
                success: function(cliente)
                {
                    if (cliente) {
                        cliente = JSON.parse(cliente);

                        $('#clientes_id option[value='+cliente.idClientes+']').remove();

                        $('#clientes_id').append('<option value="'+cliente.idClientes+'">' + cliente.nomeCliente + '</option>');
                        $('#clientes_id').val(cliente.idClientes);
                        $('#adicionarCliente').modal('hide');
                        $('#pincliente').attr('class', 'icon-pencil icon-white');
                        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                        $('#aAdicionarVeiculo').removeAttr('disabled');
                    }
                }
            });
        });

        $('#tpNF').blur(function (event) {
            if ($(this).val() === '1') {
                $('#natOp').val('VENDA DE MERCADORIA');
            } else {
                $('#natOp').val('COMPRA DE MERCADORIA');
            }
        })
    });


    function fecharModelSercheCliente(cliente) {
        $('#pincliente').attr('class', 'icon-edit icon-white');

        $('#destxNome').val(cliente.nomeCliente);
        $('#destCNPJ').val(cliente.documento);
        $('#destIE').val(cliente.IE);
        $('#destISUF').val(cliente.IESUF);
        $('#destxLgr').val(cliente.rua);
        $('#destnro').val(cliente.numero);
        $('#destxCpl').val(cliente.complemento);
        $('#destCEP').val(cliente.cep);
        $('#destxBairro').val(cliente.bairro);
        $('#destxMun').val(cliente.cidade);
        $('#destcMun').val(cliente.codIBGECidade);
        $('#destUF').val(cliente.estado);
        $('#destfone').val(cliente.telefone);
        $('#destemail').val(cliente.email);

        if(cliente.inativo == 1){
            $('#div_cliente_restricao').show();
            $('#div_cliente_restricao').html('Cliente com restrições: '+cliente.observacaoInativo);
            $('#btnContinuar').hide();
            $('#btn-faturar').hide();
        } else {
            $('#div_cliente_restricao').hide();
            $('#btnContinuar').show();
            $('#btn-faturar').show();
        }
        $('.close').click();
    }

</script>