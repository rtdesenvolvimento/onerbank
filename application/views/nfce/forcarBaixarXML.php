<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-download"></i> Tentar baixar XML</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formReceita" method="post" class="form-horizontal" >
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <label for="codigo" class="control-label">Nº da nota<span class="required">*</span></label>
                            <input id="numero" class="form-control" type="text" name="numero" required="required" value=""  />
                        </div>
                    </div>
                </form>
                <div class="ln_solid"></div>
                <div class="form-actions">
                    <div class="span12" style="text-align: right;">
                        <div class="span6 offset3">
                            <button type="submit" onclick="baixar();" class="btn btn-success"> <i class="fa fa-clock-o"></i> Atualizar chave ou baixar xml</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function baixar() {
        var numero = document.getElementById('numero').value;
        window.open('<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/baixar_xml_nota_autorizada.php?nNF='+numero,'newpage');
    }
    function gerarxml() {
        var numero = document.getElementById('numero').value;
        window.open('<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/gera_nfce.php?nNF='+numero,'newpage');
    }
    function assinar() {
        var numero = document.getElementById('numero').value;
        window.open('<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/assina_nfce.php?arquivo='+numero,'newpage');
    }
</script>

