<link href="<?php echo base_url(); ?>assets/css/nfe/nfe.css" rel="stylesheet">

<style>
    .situacao_nota_inutlizada {
        display: inline;
        padding: .2em .6em .3em;
        font-weight: 700;
        line-height: 1;
        border-radius: .25em;
        background-color: #ad5e64;
        color: #ffffff;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-search"></i> Filtros</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="get" id="frmLancamentos">
                    <div class="col-md-12 well">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label for="">Data Emissão de:</label>
                                <input type="date" name="dataInicial" required="required" class="form-control" value="<?php echo $dataInicial ?>"/>
                            </div>
                            <div class="col-md-6">
                                <label for="">até:</label>
                                <input type="date" name="dataFinal" required="required" class="form-control" value="<?php echo $dataFinal ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="span1" style="text-align: right;">
                        <button type="submit" class="span12 btn btn-primary"><i class="fa fa-search"></i> Pesquisar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <img src="<?= base_url()?>/assets/img/nfce.png" width="4%" title="NFC-e">
                <ul class="nav navbar-right panel_toolbox">
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
                        <a href="#" id="transmitir" class="btn btn-primary">
                            <i class="fa fa-send"></i> Transmitir
                        </a>
                        <a href="#" id="imprimir" class="btn btn-info">
                            <i class="fa fa-print"></i> Imprimir DANFE
                        </a>
                        <a href="#" id="baixarxml" class="btn btn-warning">
                            <i class="fa fa-download"></i> Baixar XML
                        </a>
                        <a href="<?php echo base_url();?>index.php/nfce/adicionar" id="adicionarNF" class="btn btn-success">
                            <i class="fa fa-plus"></i>  NFC-e
                        </a>
                        <a href="#" id="cancelar" class="btn btn-danger">
                            <i class="fa fa-trash"></i> Cancelar
                        </a>
                        <?php
                        $this->db->where('filial_id', $this->session->userdata('filial_id'));
                        $this->db->from('nfce');
                        $allResults = $this->db->get()->result();
                        $contadorPedencias = 0;

                        foreach ($allResults as $rt) {
                            if ($rt->status == '') {
                                $contadorPedencias = $contadorPedencias + 1;
                            }
                        }
                        ?>
                        <a href="<?php echo base_url();?>index.php/nfce/nfcePendente" class="btn btn-info">
                            <i class="fa fa-magic"></i> Corrigir Pendencias <b>(<?php echo $contadorPedencias;?>)</b>
                        </a>
                        <!--
                        <a href="#" id="excluir" class="btn btn-danger">
                            <i class="icon-trash icon-trash"></i> Excluir
                        </a>
                        !-->
                    <?php } ?>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" >
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="text-align: center;">#</th>
                                    <th style="text-align: center;">N.</th>
                                    <th style="text-align: center;">Emisão</th>
                                    <th style="text-align: left;">Cliente</th>
                                    <th style="text-align: right;">Bruto</th>
                                    <th style="text-align: right;">Acres.</th>
                                    <th style="text-align: right;">Desc.</th>
                                    <th style="text-align: right;">Líquido</th>
                                    <th style="text-align: center;">Situação</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($results as $r) {

                                    $emitente = $this->db->get_where('emitente', array('filial_id'=>$this->session->userdata('filial_id')))->row();
                                    $pasta = date('Ym', strtotime($r->dhEmi));
                                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/emissores/v4/nfephp-master/XML/" . $emitente->cnpj . "/NF-e/producao/enviadas/aprovadas/$pasta/{$r->chavenfe}-nfce.xml"; // Ambiente Windows

                                    $dataEmissao = date(('d/m/Y'),strtotime($r->dhEmi)).' '.date(('H:i'),strtotime($r->hEmi));
                                    $status = $r->status;

                                    if ($status == '') {
                                        $status = '<span class="situacao_nota_pendente">PENDENTE</span>';
                                    } else if ($status == '100') {
                                        $status = '<span class="situacao_nota_autorizada">AUTORIZADA</span>';
                                    } else if ($status == '101') {
                                        $status = '<span class="situacao_nota_cancelada">CANCELADA</span>';
                                    }else if ($status == '999') {
                                        $status = '<span class="situacao_nota_inutlizada">INUTILIZADA</span>';
                                    }

                                    echo '<tr>';
                                    echo '<td><input class="elementSelected" type="checkbox" situacao="'.$r->status.'" value="'.$r->nNF.'"></td>';
                                    echo '<td style="text-align: center;">'.$r->nNF.'</td>';
                                    echo '<td style="text-align: center;">'.$dataEmissao.'</td>';
                                    echo '<td style="text-align: left"><a href="'.base_url().'index.php/clientes/profile/'.$r->clientes_id.'">'.$r->destxNome.'<br/>'.$r->destCNPJ.'</a></td>';
                                    echo '<td style="text-align: right;">R$'.$this->site_model->formatarValorMonetario($r->vNF - $r->vOutronf + $r->vDescnf  ) .'</td>';
                                    echo '<td style="text-align: right;">R$'.$this->site_model->formatarValorMonetario($r->vOutronf) .'</td>';
                                    echo '<td style="text-align: right;">R$'.$this->site_model->formatarValorMonetario($r->vDescnf) .'</td>';
                                    echo '<td style="text-align: right;">R$'.$this->site_model->formatarValorMonetario($r->vNF) .'</td>';

                                    if (!file_exists($filename)) {
                                        echo '<td style="text-align: center;width:5%;"><span class="situacao_nota_inutlizada">'.$status.' <a href="https://dfe-portal.svrs.rs.gov.br/NFCESSL/DownloadXMLDFe?OrigemSite=1&Ambiente=1&ChaveAcessoDfe='.$r->chavenfe.'" target="_blank"> BAIXAR XML </a> </span></td>';
                                    } else {
                                        echo '<td style="text-align: center;width:5%;">'.$status.'</td>';
                                    }

                                    echo '<td style="text-align: center;">';
                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/nfce/editar/'.$r->nNF.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">
    var CONFIRMACAO_AUTORIZA_USO_NF = 'Autorizado o uso da NF-e';
    var ERRO_DIFERENCA_DIGEST = 'Os documentos se referem a diferentes objetos. Os digest são diferentes.';
    var ERRO_SEM_DADOS_NECESSARIOS = 'Não foram passados todos os dados necessários.';
    var NAO_CONSTA_NA_BASE_DA_SEFAZ = 'Rejeicao: NF-e nao consta na base de dados da SEFAZ';

    $(document).ready(function(){

        $('#transmitir').click(function (event) {
            var elemento = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/nfce/getNotaFiscalById",
                    data: {
                        nNF: $(elemento).get(0)
                    },
                    dataType: "json",
                    success: function (nfe) {
                        if (nfe.status === '100') {
                            alert("nota ja autorizada");
                        } else {
                            //GeraNFe(nfe.nNF);
                            ValidarAntesTransmitir(nfe.nNF);
                        }
                    }
                });
            }
        });

        $('#imprimir').click(function (event) {
            var elemento = new Array();
            var situacao =0;

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                situacao =  parseInt($(this).attr('situacao'));
            });


            if (situacao === 101) {
                alert("Nota Cancelada!");
                return;
            }

            if (situacao !== 100) {
                alert("Nota não autorizada!");
                return;
            }

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfce.php?nNFbd='+$(elemento).get(0) +'', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#baixarxml').click(function (event) {

            var ano = '<?php echo date('Y');?>';
            var mes = '<?php echo date('m');?>';
            var anomes =  prompt('EXPORTAR XML - Ano e Mês: EXMPLO: 201801',ano+''+mes);

            if (anomes !== '') {
                window.open('<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/xml_nfce_download.php?periodo='+anomes+ '', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#cancelar').click(function (event) {

            if (confirm('Deseja realmente cancelar a nfe?')) {

                var elemento    = new Array();
                var situacao = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 101) {
                        alert("Nota já Cancelada!");
                        return;
                    }

                    if (situacao !== 100) {
                        alert("Nota não esta autorizada para ser cancelada!");
                        return;
                    }

                    var justificativa =  prompt('Digite a justificatido cancelamento!');

                    if (justificativa === '') {
                        alert("Justiticativa é obrigatório para o cancelamento da nota.");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Cancelando a NFCe Aguarde...</h3>'});

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/cancela_nfce.php",
                        data: {
                            nNF: $(elemento).get(0),
                            justificativa: justificativa,
                        },
                        dataType: "html",
                        success: function (data) {
                            $.ajax({
                                type: "POST",
                                url: BASE_URL + "index.php/nfce/controleEstoqueCancelamento",
                                data: {
                                    nNF : $(elemento).get(0)
                                },
                                dataType: 'html',
                                success: function () {
                                    $.unblockUI();
                                    alert('Cancelada com sucesso!');
                                    location.reload();
                                }
                            });
                        }
                    });
                }
            }
        });

        $('#excluir').click(function (event) {

            if (confirm('Deseja realmente excluir a nfe?')) {

                var elemento    = new Array();
                var situacao    = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 100) {
                        alert("Nota autorizada! Não é possível excluir;");
                        return;
                    }

                    if (situacao === 101) {
                        alert("Nota cancelada! Não é possível excluir;");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Excluindo a NFCe Aguarde...</h3>'});

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/nfce/excluir/"+$(elemento).get(0),
                        dataType: "html",
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            }
        });
    });

    function GeraNFe(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Gerando XML...<small>(Está demorando? Clique <a href="https://www.smarticontrol.com.br/index.php/mapos/sair">AQUI</a> pra Sair do Sistema e acesse novamente)</small></h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/gera_nfce.php?nNF="+valor,
            dataType: "json",
            success: function (data) {
                $.unblockUI();
                if (data.Status === 'Ok') {
                    AssinaNFE(valor);
                } else {
                    alert('Erro ao Exportar Arquivo.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.responseText) {
                    var str = jqXHR.responseText;
                    var retorno = jQuery.parseJSON(str.substr(3));
                    if (retorno.Status === 'Ok') {
                        SituacaoNFeA3();
                    } else {
                        toastr.warning('Erro ao Exportar Arquivo.');
                    }
                }
            }
        });
    }

    function SituacaoNFeA3() {
        $.ajax({
            type: "GET",
            url: "http://localhost/rt-php-sinf_v2.0/_backend/_controller/_select/a3/nfe_select_situacao.php",
            dataType: "json",
            success: function (data)
            {
                if (data.situacao === 'S') {
                    document.getElementById('alertsucesso').style.display = 'None';
                    Carrega('_grid', 'nfe_grid.php', 'NF-e');
                } else {
                    setTimeout(function () {
                        SituacaoNFeA3();
                    }, 3000);
                }
            }
        });
    }

    function AssinaNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Assinando XML...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/assina_nfce.php",
            data: {
                arquivo: valor
            },
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    TransmiteNFE(valor);
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                    mensagemDeErro('Error!', $(retorno).find("motivo").text());
                }
            }
        });
    }

    function TransmiteNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Enviando XML ao Sefaz...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/envia_nfce.php?arquivo=" + valor,
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    RetornoNFE($(retorno).find("recibo").text(), valor);
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                    mensagemDeErro('Error!', $(retorno).find("motivo").text());
                }
            }
        });
    }

    function RetornoNFE(valor, nct) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Retornando Dados da SEFAZ. </br><small>(Está demorando? Clique <a href="https://www.smarticontrol.com.br/index.php/mapos/sair">AQUI</a> pra Sair do Sistema e acesse novamente)</small></h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/consulta_recibo.php?arquivo="+nct,
            dataType: "html",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfce.php?nNFbd=' + nct + '', '',
                        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');

                    mensagemDeSucesso('Autorizada!','Nota fiscal autorizada com sucesso!!!');
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                    mensagemDeErro('Error!', $(retorno).find("motivo").text());
                }
            }
        });
    }

    function ValidarAntesTransmitir(nNF) {

        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Validando XML...<small>(Está demorando? Clique <a href="https://www.smarticontrol.com.br/index.php/mapos/sair">AQUI</a> pra Sair do Sistema e acesse novamente)</small></h3>'});

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/atualizar_notas_duplicidade.php?nNF="+nNF,
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();

                var motivo = $(retorno).find("motivo").text();

                if (motivo === CONFIRMACAO_AUTORIZA_USO_NF) {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    mensagemDeSucesso('Sucesso!', $(retorno).find("motivo").text());
                } else if (motivo === ERRO_DIFERENCA_DIGEST) {
                    mensagemDeErro('Error!', 'Não foi possível realizar a Retransmissão da NFC-e.' +
                        '<br/> A NFC-e já encontra-se autorizada na SEFAZ. ' +
                        '<br/> Acesse a tela de NFC-e pendentes para realizar o ajuste!');

                } else if (retorno == null || motivo  === 'Arquivo nao encontrato.' || motivo === ERRO_SEM_DADOS_NECESSARIOS || motivo === NAO_CONSTA_NA_BASE_DA_SEFAZ) {
                    GeraNFe(nNF);
                } else {
                    mensagemDeErro('Error!', motivo);
                }
            }
        });
    }
</script>