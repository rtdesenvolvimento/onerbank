<link href="<?php echo base_url(); ?>assets/css/nfe/nfe.css" rel="stylesheet">
<?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Importação Manual de XML</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo base_url() ?>index.php/nfce/uploadXMLNF" enctype="multipart/form-data" method="post">
                    <div class="row well">
                        <div class="col-md-6 col-sm-6">
                            <input type="file" name="xmlNF" id="xmlNF" required="required">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <button class="btn btn-warning" id="excel"><i class="fa fa-upload"></i> UPLOAD XML NFC-e</button>
                        </div>
                    </div>
                </form>
                <div class="alert alert-info alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Atenção</strong> </br>Enquanto houver NFC-e pendente de autorização no mês vigente, não é possível realizar o fechamento do mês.
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-magic"></i> NFC-e pendente(s) de autorização.</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a href="#" id="retransmitir" class="btn btn-primary">
                        <i class="fa fa-send"></i> Retransmitir
                    </a>
                    <a href="#" id="inutilizarNF" class="btn btn-success">
                        <i class="fa fa-cc-amex"></i> Inutilizar Numeração da Nota
                    </a>
                    <a href="#" id="excluir" class="btn btn-danger">
                        <i class="fa fa-trash"></i> Excluir
                    </a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="text-align: right;">#</th>
                                    <th style="text-align: center;">Tipo</th>
                                    <th style="text-align: right;">Nota</th>
                                    <th style="text-align: right;">Emissão</th>
                                    <th>Cliente</th>
                                    <th style="text-align: right;">Valor</th>
                                    <th style="text-align: right;">Situação</th>
                                    <th style="text-align: right;">XML</th>
                                    <th style="text-align: right;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $emitente = $this->db->get_where('emitente', array('filial_id'=>$this->session->userdata('filial_id')))->row();
                                $this->db->where('status', NULL );
                                $this->db->where('filial_id', $this->session->userdata('filial_id'));
                                $this->db->from('nfce');
                                $results = $this->db->get()->result();

                                foreach ($results as $r) {
                                    $dataEmissao = date(('d/m/Y'),strtotime($r->dhEmi)).' '.date(('H:i'),strtotime($r->hEmi));
                                    $status = $r->status;
                                    $status = '<span class="situacao_nota_pendente">PENDENTE</span>';

                                    echo '<tr>';
                                    echo '<td style="text-align: center;"><input class="elementSelected" type="checkbox" situacao="' . $r->status . '" value="' . $r->nNF . '"></td>';
                                    echo '<td style="text-align: left;">NF-e</td>';
                                    echo '<td style="text-align: left;">' . $r->nNF. '</td>';
                                    echo '<td style="text-align: center;">' . $dataEmissao . '</td>';
                                    echo '<td style="text-align: left"><a href="' . base_url() . 'index.php/clientes/profile/' . $r->clientes_id . '">' . $r->destxNome.'<br/>'.$r->destCNPJ . '</a></td>';
                                    echo '<td style="text-align: right;">R$' . $r->vNF . '</td>';
                                    echo '<td style="text-align: center;">' . $status . '</td>';

                                    $pasta = date('Ym', strtotime($r->dhEmi));
                                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/emissores/v4/nfephp-master/XML/" . $emitente->cnpj . "/NF-e/producao/enviadas/aprovadas/$pasta/{$r->chavenfe}-nfce.xml"; // Ambiente Windows
                                    $arquivo = base_url() . "/emissores/v4/nfephp-master/XML/" . $emitente->cnpj . "/NF-e/producao/enviadas/aprovadas/$pasta/{$r->chavenfe}-nfce.xml"; // Ambiente Windows

                                    $sefaz = 'https://www.sefaz.rs.gov.br/SSL_Client/NFE-CER-CON-XML.aspx?HML=false&ChaveAcesso=' . $r->chavenfe . '&section=nfe&dl=yes&VCCnpjEmi=' . $emitente->cnpj . '&VCCnpjCpfDest=&VCCnpjCpfTransp=0&LogConsultaId=44490972';

                                    if (!file_exists($filename)) {
                                        echo '<td style="text-align: left;"><a href="' . $sefaz . '" id="'. $r->nNF.'">BAIXAR</a></td>';
                                    } else {
                                        echo '<td style="text-align: left;"><a href="' . $arquivo . '" target="_blank">XML</a></td>';
                                    }

                                    echo '<td style="text-align: center;">';
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eVenda')) {
                                        echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/nfce/editar/' . $r->nNF . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }

                                $this->db->where('status','');
                                $this->db->where('filial_id', $this->session->userdata('filial_id'));
                                $this->db->from('nfce');
                                $results = $this->db->get()->result();

                                foreach ($results as $r) {

                                    $dataEmissao = date(('d/m/Y'),strtotime($r->dhEmi)).' '.date(('H:i'),strtotime($r->hEmi));
                                    $status = $r->status;
                                    $status = '<span class="situacao_nota_pendente">PENDENTE</span>';

                                    echo '<tr>';
                                    echo '<td><input class="elementSelected" type="checkbox" situacao="' . $r->status . '" value="' . $r->nNF . '"></td>';
                                    echo '<td style="text-align: left;">NFC-e</td>';
                                    echo '<td style="text-align: left;">' . $r->nNF. '</td>';
                                    echo '<td style="text-align: left;">' . $dataEmissao . '</td>';
                                    echo '<td style="text-align: left"><a href="' . base_url() . 'index.php/clientes/profile/' . $r->clientes_id . '">' . $r->destxNome.'<br/>'.$r->destCNPJ. '</a></td>';
                                    echo '<td style="text-align: left;">R$ ' . $r->vNF . '</td>';
                                    echo '<td style="text-align: left;">' . $status . '</td>';

                                    $pasta = date('Ym', strtotime($r->dhEmi));
                                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/emissores/v4/nfephp-master/XML/" . $emitente->cnpj . "/NF-e/producao/enviadas/aprovadas/$pasta/{$r->chavenfe}-nfce.xml"; // Ambiente Windows
                                    $arquivo = base_url() . "/emissores/v4/nfephp-master/XML/" . $emitente->cnpj . "/NF-e/producao/enviadas/aprovadas/$pasta/{$r->chavenfe}-nfce.xml"; // Ambiente Windows
                                    $sefaz = 'https://www.sefaz.rs.gov.br/SSL_Client/NFE-CER-CON-XML.aspx?HML=false&ChaveAcesso=' . $r->chavenfe . '&section=nfe&dl=yes&VCCnpjEmi=' . $emitente->cnpj . '&VCCnpjCpfDest=&VCCnpjCpfTransp=0&LogConsultaId=44490972';

                                    if (!file_exists($filename)) {
                                        echo '<td style="text-align: left;"><a href="' . $sefaz . '" id="'. $r->nNF.'">BAIXAR</a></td>';
                                    } else {
                                        echo '<td style="text-align: left;"><a href="' . $arquivo . '" target="_blank">XML</a></td>';
                                    }

                                    echo '<td style="text-align: center;">';
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eVenda')) {
                                        echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/nfce/editar/' . $r->nNF . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">

    var CONFIRMACAO_AUTORIZA_USO_NF = 'Autorizado o uso da NF-e';
    var ERRO_DIFERENCA_DIGEST = 'Os documentos se referem a diferentes objetos. Os digest são diferentes.';
    var ERRO_SEM_DADOS_NECESSARIOS = 'Não foram passados todos os dados necessários.';

    $(document).ready(function(){

        $('#retransmitir').click(function (event) {

            $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Retransmitindo XML...</h3>'});

            var elemento = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhuma NFC-e Selecionado para Retransmissão!');
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/atualizar_notas_duplicidade.php?nNF="+$(elemento).get(0),
                    dataType: "xml",
                    success: function (retorno) {
                        $.unblockUI();
                        var motivo = $(retorno).find("motivo").text();

                        if (motivo === CONFIRMACAO_AUTORIZA_USO_NF) {
                            $("#datatable").load("<?php echo current_url();?> #datatable");

                            $('#dSuccess-msg').html($(retorno).find("motivo").text());
                            $('.success-msg').show();

                            $('#dError-msg').html('');
                            $('.error-msg').hide();
                        } else if (motivo === ERRO_DIFERENCA_DIGEST) {
                            $("#datatable").load("<?php echo current_url();?> #datatable");

                            mensagemDeErro('Error!', 'Não foi possível realizar a Retransmissão da NFC-e.' +
                                '<br/> A NFC-e já encontra-se autorizada na SEFAZ. ' +
                                '<br/> Será necessário realizar o upload do XML manualmente.' +
                                '<br/> <a href="#" onclick="donwloadXMLSEFAZ('+$(elemento).get(0)+')">Clique Aqui para baixar o XML da SEFAZ</a>');

                        } else if (retorno == null || motivo === ERRO_SEM_DADOS_NECESSARIOS) {
                                PrimeiroTrasmitir($(elemento).get(0));
                        } else {
                            $("#datatable").load("<?php echo current_url();?> #datatable");
                            mensagemDeErro('Error!', motivo);
                        }
                    }
                });
            }
        });

        $('#excluir').click(function (event) {

            if (confirm('Deseja realmente excluir a nfe?')) {

                var elemento    = new Array();
                var situacao    = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 100) {
                        alert("Nota autorizada! Não é possível excluir;");
                        return;
                    }

                    if (situacao === 101) {
                        alert("Nota cancelada! Não é possível excluir;");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Excluindo a nfse Aguarde...</h3>'});

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/nfce/excluir/"+$(elemento).get(0),
                        dataType: "html",
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            }
        });


        $('#inutilizarNF').click(function (event) {

            if (confirm('Deseja realmente cancelar a nfce?')) {

                var elemento    = new Array();
                var situacao = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 101) {
                        alert("Nota já Cancelada!");
                        return;
                    }

                    if (situacao === 999) {
                        alert("Nota já está inutilizada!");
                        return;
                    }

                    var justificativa =  prompt('Digite a justificatido para a inutilização da NFC-e!');

                    if (justificativa === '') {
                        alert("Justiticativa é obrigatório para a inutilização da nota.");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Inutilização da nfe Aguarde...</h3>'});

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/inutiliza_nfce.php",
                        data: {
                            nIni: $(elemento).get(0),
                            nFin: $(elemento).get(0),
                            xJust: justificativa,
                        },
                        dataType: "html",
                        success: function (data) {
                            $.ajax({
                                type: "POST",
                                url: BASE_URL + "index.php/nfce/controleEstoqueCancelamento",
                                data: {
                                    nNF : $(elemento).get(0)
                                },
                                dataType: 'html',
                                success: function () {
                                    $.unblockUI();
                                    alert('Initulizada com sucesso!');
                                    location.reload();
                                }
                            });
                        }
                    });
                }
            }
        });

    });

    function PrimeiroTrasmitir(nNF) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/nfce/getNotaFiscalById",
            data: {
                nNF: nNF
            },
            dataType: "json",
            success: function (nfe) {
                if (nfe.status === '100') {
                    alert("nota ja autorizada");
                } else {
                    GeraNFe(nfe.nNF);
                }
            }
        });
    }

    function GeraNFe(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Gerando XML...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/gera_nfce.php?nNF="+valor,
            dataType: "json",
            success: function (data) {
                $.unblockUI();
                if (data.Status === 'Ok') {
                    AssinaNFE(valor);
                } else {
                    alert('Erro ao Exportar Arquivo.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.responseText) {
                    var str = jqXHR.responseText;
                    var retorno = jQuery.parseJSON(str.substr(3));
                    if (retorno.Status === 'Ok') {
                        SituacaoNFeA3();
                    } else {
                        toastr.warning('Erro ao Exportar Arquivo.');
                    }
                }
            }
        });
    }

    function SituacaoNFeA3() {
        $.ajax({
            type: "GET",
            url: "http://localhost/rt-php-sinf_v2.0/_backend/_controller/_select/a3/nfe_select_situacao.php",
            dataType: "json",
            success: function (data)
            {
                if (data.situacao === 'S') {
                    document.getElementById('alertsucesso').style.display = 'None';
                    Carrega('_grid', 'nfe_grid.php', 'NF-e');
                } else {
                    setTimeout(function () {
                        SituacaoNFeA3();
                    }, 3000);
                }
            }
        });
    }

    function AssinaNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Assinando XML...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/assina_nfce.php",
            data: {
                arquivo: valor
            },
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    TransmiteNFE(valor);
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");

                    mensagemDeErro('Error!', 'Não foi possível encontrar a NFC-e na base da SEFAZ. ' +
                        '<br/>Verifique o erro retornado:' +
                        '<br/>'+$(retorno).find("motivo").text()+
                        '<br/><b>Se você estiver no Mês vigente da NFC-e edite conforme o erro retornado e tente autorizar novamente!</b>' +
                        '<br/><b>Se você estiver em outro Mês não vigente desta NFC-e inutilize a numeração usando o botão Inutilizar Numeração.</b>');

                }
            }
        });
    }

    function TransmiteNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Enviando XML ao Sefaz...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/envia_nfce.php?arquivo=" + valor,
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    RetornoNFE($(retorno).find("recibo").text(), valor);
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");

                    mensagemDeErro('Error!', 'Não foi possível encontrar a NFC-e na base da SEFAZ. ' +
                        '<br/>Verifique o erro retornado:' +
                        '<br/>'+$(retorno).find("motivo").text()+
                        '<br/><b>Se você estiver no Mês vigente da NFC-e edite conforme o erro retornado e tente autorizar novamente!</b>' +
                        '<br/><b>Se você estiver em outro Mês não vigente desta NFC-e inutilize a numeração usando o botão Inutilizar Numeração.</b>');
                }
            }
        });
    }

    function RetornoNFE(valor, nct) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Retornando Dados do Sefaz</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/consulta_recibo.php?arquivo="+nct,
            dataType: "html",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    $("#datatable").load("<?php echo current_url();?> #datatable");
                    window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfce.php?nNFbd=' + nct + '', '',
                        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');

                    mensagemDeSucesso('Autorizada!','Nota fiscal autorizada com sucesso!!!');

                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");

                    mensagemDeErro('Error!','Não foi possível encontrar a NFC-e na base da SEFAZ. ' +
                        '<br/>Verifique o erro retornado:' +
                        '<br/>'+$(retorno).find("motivo").text()+
                        '<br/><b>Se você estiver no Mês vigente da NFC-e edite conforme o erro retornado e tente autorizar novamente!</b>' +
                        '<br/><b>Se você estiver em outro Mês não vigente desta NFC-e inutilize a numeração usando o botão Inutilizar Numeração.</b>');
                }
            }
        });
    }

    function donwloadXMLSEFAZ(nNF) {
        document.getElementById(nNF).click()
    }
</script>