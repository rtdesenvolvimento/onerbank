<link href="<?php echo base_url(); ?>assets/css/nfe/nfe.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <img src="<?= base_url()?>/assets/img/nfe.jpg" width="4%" title="NFC-e">
                <ul class="nav navbar-right panel_toolbox">
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
                        <a href="#" id="transmitir" class="btn btn-primary">
                            <i class="fa fa-send"></i> Transmitir
                        </a>
                        <a href="#" id="imprimir" class="btn btn-info">
                            <i class="fa fa-print"></i>  Imprimir
                        </a>
                        <a href="#" id="carta_correcao" class="btn btn-dark">
                            <i class="fa fa-mail-reply"></i> Carta de correção
                        </a>
                        <a href="#" id="imprimirCCE" class="btn btn-info">
                            <i class="fa fa-print"></i> Imprimir CCE
                        </a>
                        <a href="#" id="baixarxml" class="btn btn-warning">
                            <i class="fa fa-download"></i> Baixar XML
                        </a>
                        <a href="<?php echo base_url();?>index.php/nfe/adicionar" id="adicionarNF" class="btn btn-success">
                            <i class="fa fa-plus"></i> NF-e
                        </a>
                        <a href="#" id="cancelar" class="btn btn-danger">
                            <i class="fa fa-trash"></i> Cancelar
                        </a>
                        <a href="#" id="excluir" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </a>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>#</th>
                                    <th style="text-align: center;">Nota</th>
                                    <th>Tipo</th>
                                    <th style="text-align: center;">Emisão</th>
                                    <th>Cliente</th>
                                    <th style="text-align: right;">Valor</th>
                                    <th style="text-align: center;">Situação</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    $dataEmissao = date(('d/m/Y'),strtotime($r->dhEmi)).' '.date(('H:i'),strtotime($r->hEmi));
                                    $status = $r->status;

                                    if ($status == '') {
                                        $status = '<span class="situacao_nota_pendente">PENDENTE</span>';
                                    } else if ($status == '100') {
                                        if ($r->cce == 'S') {$status = '<span class="situacao_nota_autorizada">Aprovado(CCE)</span>';
                                        } else {
                                            $status = '<span class="situacao_nota_autorizada">AUTORIZADA</span>';
                                        }
                                    } else if ($status == '101') {
                                        $status = '<span class="situacao_nota_cancelada">CANCELADA</span>';
                                    }

                                    echo '<tr>';
                                    echo '<td><input class="elementSelected" type="checkbox" cce="'.$r->cce.'" situacao="'.$r->status.'" value="'.$r->nNF.'"></td>';
                                    echo '<td style="text-align: center;">'.$r->nNF.'</td>';
                                    echo '<td style="text-align: left;">'.$r->natOp.'</td>';
                                    echo '<td style="text-align: center;">'.$dataEmissao.'</td>';
                                    echo '<td style="text-align: left"><a href="'.base_url().'index.php/clientes/profile/'.$r->clientes_id.'">'.$r->destxNome.'<br/>'.$r->destCNPJ.'</a></td>';
                                    echo '<td style="text-align: right;">R$'.$r->valortotalnf.'</td>';
                                    echo '<td style="text-align: center;">'.$status.'</td>';
                                    echo '<td style="text-align: center;">';
                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/nfe/editar/'.$r->nNF.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#transmitir').click(function (event) {
            var elemento = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/nfe/getNotaFiscalById",
                    data: {
                        nNF: $(elemento).get(0)
                    },
                    dataType: "json",
                    success: function (nfe) {
                        if (nfe.status === '100') {
                            alert("nota ja autorizada");
                        } else {
                            GeraNFe(nfe.nNF);
                        }
                    }
                });
            }
        });

        $('#imprimir').click(function (event) {
            var elemento = new Array();
            var situacao =0;

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                situacao =  parseInt($(this).attr('situacao'));
            });


            if (situacao === 101) {
                alert("Nota Cancelada!");
                return;
            }

            if (situacao !== 100) {
                alert("Nota não autorizada!");
                return;
            }

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfe.php?nNFbd='+$(elemento).get(0) +'', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#imprimirCCE').click(function (event) {
            var elemento = new Array();
            var cce      = new Array();
            var situacao =0;

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                situacao =  parseInt($(this).attr('situacao'));
                cce.push($(this).attr('cce'));
            });

            if (situacao === 101) {
                alert("Nota Cancelada!");
                return;
            }

            if (situacao !== 100) {
                alert("Nota não autorizada!");
                return;
            }

            if ($(cce).get(0) === 'N') {
                alert('Não foi possível encontrar nenhuma carta de correção para esta nota');
                return;
            }

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/dacce.php?nNFbd='+$(elemento).get(0) +'', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#baixarxml').click(function (event) {

            var ano = '<?php echo date('Y');?>';
            var mes = '<?php echo date('m');?>';
            var anomes =  prompt('EXPORTAR XML - Ano e Mês: EXMPLO: 201801',ano+''+mes);

            if (anomes !== '') {
                window.open('<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/xml_download.php?periodo='+anomes+ '', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#carta_correcao').click(function (event) {

            var elemento    = new Array();
            var situacao = 0;

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                situacao =  parseInt($(this).attr('situacao'));
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {

                if (situacao !== 100) {
                    alert("Nota não esta autorizada!");
                    return;
                }

                var correcao = prompt('Digite a correção!');

                if (correcao === '') {
                    alert("Correção é obrigatório.");
                    return;
                }

                $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Enviando correção a nfe Aguarde...</h3>'});

                $.ajax({
                    type: "GET",
                    url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/cce_nfe.php",
                    data: {
                        nNF: $(elemento).get(0),
                        correcao: correcao,
                    },
                    dataType: "html",
                    success: function (data) {
                        $.unblockUI();
                        alert(data);
                        location.reload();
                    }
                });
            }
        });

        $('#cancelar').click(function (event) {

            if (confirm('Deseja realmente cancelar a nfe?')) {

                var elemento    = new Array();
                var situacao = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 101) {
                        alert("Nota já Cancelada!");
                        return;
                    }

                    if (situacao !== 100) {
                        alert("Nota não esta autorizada para ser cancelada!");
                        return;
                    }

                    var justificativa =  prompt('Digite a justificatido cancelamento!');

                    if (justificativa === '') {
                        alert("Justiticativa é obrigatório para o cancelamento da nota.");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Cancelando a nfe Aguarde...</h3>'});

                    $.ajax({
                        type: "GET",
                        url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/cancela_nfe.php",
                        data: {
                            nNF: $(elemento).get(0),
                            justificativa: justificativa,
                        },
                        dataType: "html",
                        success: function (data) {
                            $.ajax({
                                type: "POST",
                                url: BASE_URL + "index.php/nfe/controleEstoqueCancelamento",
                                data: {
                                    nNF : $(elemento).get(0)
                                },
                                dataType: 'html',
                                success: function () {
                                    $.unblockUI();
                                    alert('Cancelada com sucesso!');
                                    location.reload();
                                }
                            });
                        }
                    });
                }
            }
        });

        $('#excluir').click(function (event) {

            if (confirm('Deseja realmente excluir a nfe?')) {

                var elemento    = new Array();
                var situacao    = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 100) {
                        alert("Nota autorizada! Não é possível excluir;");
                        return;
                    }

                    if (situacao === 101) {
                        alert("Nota cancelada! Não é possível excluir;");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Excluindo a NF-e Aguarde...</h3>'});

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/nfe/excluir/"+$(elemento).get(0),
                        dataType: "html",
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            }
        });
    });

    function GeraNFe(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Gerando XML...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/gera_nfe.php?nNF="+valor,
            dataType: "json",
            success: function (data) {
                $.unblockUI();
                if (data.Status === 'Ok') {
                    AssinaNFE(valor);
                } else {
                    alert('Erro ao Exportar Arquivo.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.responseText) {
                    var str = jqXHR.responseText;
                    var retorno = jQuery.parseJSON(str.substr(3));
                    if (retorno.Status === 'Ok') {
                        SituacaoNFeA3();
                    } else {
                        toastr.warning('Erro ao Exportar Arquivo.');
                    }
                }
            }
        });
    }

    function SituacaoNFeA3() {
        $.ajax({
            type: "GET",
            url: "http://localhost/rt-php-sinf_v2.0/_backend/_controller/_select/a3/nfe_select_situacao.php",
            dataType: "json",
            success: function (data)
            {
                if (data.situacao === 'S') {
                    document.getElementById('alertsucesso').style.display = 'None';
                    Carrega('_grid', 'nfe_grid.php', 'NF-e');
                } else {
                    setTimeout(function () {
                        SituacaoNFeA3();
                    }, 3000);
                }
            }
        });
    }

    function AssinaNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Assinando XML...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/assina_nfe.php",
            data: {
                arquivo: valor
            },
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    TransmiteNFE(valor);
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                    mensagemDeErro('Error!', $(retorno).find("motivo").text());
                }
            }
        });
    }

    function TransmiteNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Enviando XML ao Sefaz...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/envia_nfe.php?arquivo=" + valor,
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    RetornoNFE($(retorno).find("recibo").text(), valor);
                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                    mensagemDeErro('Error!', $(retorno).find("motivo").text());
                }
            }
        });
    }

    function RetornoNFE(valor, nct) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Retornando Dados do Sefaz</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracao/consulta_recibo.php?arquivo="+nct,
            dataType: "html",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    $("#tbNFE").load("<?php echo current_url();?> #tbNFE");

                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "index.php/nfe/controleEstoque",
                        data: {
                            nNF : nct
                        },
                        dataType: 'html',
                        success: function () {
                            $("#datatable").load("<?php echo current_url();?> #datatable");

                            window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfe.php?nNFbd=' + nct + '', '',
                                'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');

                            mensagemDeSucesso('Autorizada!','Nota fiscal autorizada com sucesso!!!');
                        }
                    });

                } else {
                    $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                    mensagemDeErro('Error!', $(retorno).find("motivo").text());
                }
            }
        });
    }

</script>