<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<style>
    a {color: #2196f3;}
</style>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>NF-e do fornecedor coletada</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da NF-e</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divCadastrarOs">
                                <form action="<?php echo current_url(); ?>" method="post" id="formNFe">
                                    <?php echo form_hidden('nNF', $result->nNF) ?>
                                    <input type="hidden" id="percentualICMS" name="percentualICMS" value="<?php echo $result->percentualICMS;?>f"/>

                                    <div class="span6" style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">Dados NF-e</label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="modelo" >Modelo<span class="required">*</span></label>
                                                                <input id="modelo"  class="span12" readonly type="text" value="<?php echo $result->modelo; ?>" name="modelo">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="serie" >Série<span class="required">*</span></label>
                                                                <input id="serie"  class="span12" readonly type="text" value="<?php echo $result->serie; ?>" name="serie">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="nNF" >Nº<span class="required">*</span></label>
                                                                <input id="nNF"  class="span12" maxlength="9" readonly type="text" value="<?php echo $result->nNF; ?>" name="nNF">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <label for="dhEmi" >Data Emissão<span class="required">*</span></label>
                                                                <input id="dhEmi"  readonly class="span12" type="text" value="<?php echo $result->dhEmi; ?>" name="dhEmi">
                                                            </td>

                                                            <td colspan="4">
                                                                <label for="hEmi" >Hora Emissão<span class="required">*</span></label>
                                                                <input id="hEmi"  readonly class="span12" type="text" value="<?php echo $result->hEmi; ?>" name="hEmi">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <label for="dhSaiEnt" >Data Saida<span class="required">*</span></label>
                                                                <input id="dhSaiEnt" readonly  class="span12" type="text" value="<?php echo $result->dhSaiEnt; ?>" name="dhSaiEnt">
                                                            </td>

                                                            <td colspan="4">
                                                                <label for="hSaiEnt" >Hora Saida<span class="required">*</span></label>
                                                                <input id="hSaiEnt"  readonly class="span12" type="text" value="<?php echo $result->hSaiEnt; ?>" name="hSaiEnt">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <label for="indPag" >Forma Pag.<span class="required">*</span></label>
                                                                <select name="indPag" class="span12" required="" readonly>
                                                                    <option <?php if ($result->indPag == '0') echo 'selected="selected"';?> value="0">À Vista</option>
                                                                    <option <?php if ($result->indPag == '1') echo 'selected="selected"';?> value="1">À Prazo</option>
                                                                    <option <?php if ($result->indPag == '2') echo 'selected="selected"';?> value="2">Outros</option>
                                                                </select>
                                                            </td>
                                                            <td colspan="4">
                                                                <label for="tpNF" >Operação<span class="required">*</span></label>
                                                                <select name="tpNF" class="span12" required="" readonly>
                                                                    <option <?php if ($result->tpNF == '1') echo 'selected="selected"';?> value="1">Saida</option>
                                                                    <option <?php if ($result->tpNF == '0') echo 'selected="selected"';?> value="0">Entrada</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <label for="tpEmis" >Forma Emi.<span class="required">*</span></label>
                                                                <select name="tpEmis" class="span12" required="" readonly>
                                                                    <option <?php if ($result->tpEmis == '1') echo 'selected="selected"';?> value="1">Normal</option>
                                                                    <option <?php if ($result->tpEmis == '2') echo 'selected="selected"';?> value="2">Contingência FS-IA</option>
                                                                    <option <?php if ($result->tpEmis == '3') echo 'selected="selected"';?> value="3">Contingência SCAN</option>
                                                                    <option <?php if ($result->tpEmis == '4') echo 'selected="selected"';?> value="4">Contingência DPEC</option>
                                                                    <option <?php if ($result->tpEmis == '5') echo 'selected="selected"';?> value="5">Contingência FS-DA</option>
                                                                    <option <?php if ($result->tpEmis == '6') echo 'selected="selected"';?> value="6">Contingência SVC-AN</option>
                                                                    <option <?php if ($result->tpEmis == '7') echo 'selected="selected"';?> value="7">Contingência SVC-RS</option>
                                                                </select>
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="finNFe" >Finalidade<span class="required">*</span></label>
                                                                <select name="finNFe" class="span12" required="" readonly>
                                                                    <option <?php if ($result->finNFe == '1') echo 'selected="selected"';?> value="1">Normal</option>
                                                                    <option <?php if ($result->finNFe == '2') echo 'selected="selected"';?> value="2">Complementar</option>
                                                                    <option <?php if ($result->finNFe == '3') echo 'selected="selected"';?> value="3">Ajuste</option>
                                                                    <option <?php if ($result->finNFe == '4') echo 'selected="selected"';?> value="4">Devolução de Merc.</option>
                                                                </select>
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="indPres" >Tipo Atend.<span class="required">*</span></label>
                                                                <select name="indPres" class="span12" required="" readonly>
                                                                    <option <?php if ($result->indPres == '0') echo 'selected="selected"';?> value="0">Não se aplica</option>
                                                                    <option <?php if ($result->indPres == '1') echo 'selected="selected"';?> value="1">Presencial</option>
                                                                    <option <?php if ($result->indPres == '2') echo 'selected="selected"';?> value="2">Não presencial, pela internet</option>
                                                                    <option <?php if ($result->indPres == '3') echo 'selected="selected"';?> value="3">Não presencial, Teleatendimento</option>
                                                                    <option <?php if ($result->indPres == '5') echo 'selected="selected"';?> value="5">Operação presencial, fora do estabelecimento</option>
                                                                    <option <?php if ($result->indPres == '9') echo 'selected="selected"';?> value="9">Não presencial, Outros</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <label for="natOp" >Nat. Operação<span class="required">*</span></label>
                                                                <input id="natOp" disabled class="span12" type="text" value="<?php echo $result->natOp; ?>" name="natOp">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6" style="padding: 1%; margin-left: 0">
                                        <div class="span12">
                                            <div class="formBuscaGSA" id="divBuscaCliente">
                                                <label for="cliente">
                                                    Fornecedor/Cliente<span class="required"></span>
                                                </label>
                                                <div class="controls">
                                                    <label for="destxNome" >Nome/Razao Social<span class="required">*</span></label>
                                                    <select name="clientes_id" class="span12" id="clientes_id" required="required" readonly>
                                                        <option value="">--selecione um cliente--</option>
                                                        <option  selected="selected" value="<?php echo $result->clientes_id; ?>"><?php echo $result->destxNome; ?></option>
                                                    </select>
                                                    <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;display: none;"></div>
                                                </div>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="display: none;">
                                                            <label for="destxNome" >Nome/Razao Social<span class="required">*</span></label>
                                                            <input id="destxNome" class="span12" readonly type="text" value="<?php echo $result->destxNome; ?>" required="required" name="destxNome">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <label for="destCNPJ" >CPF/CNPJ<span class="required">*</span></label>
                                                            <input id="destCNPJ"  class="span12" readonly type="text" value="<?php echo $result->destCNPJ; ?>" required="required" name="destCNPJ">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="destIE" >Insc. Estadual</label>
                                                            <input id="destIE"  class="span12" readonly type="text" value="<?php echo $result->destIE; ?>" name="destIE">
                                                        </td>

                                                        <td colspan="3">
                                                            <label for="destISUF" >Insc. SUFRAMA</label>
                                                            <input id="destISUF"  class="span12" readonly type="text" value="<?php echo $result->destISUF; ?>" name="destISUF">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="destxLgr" >Endereço<span class="required">*</span></label>
                                                            <input id="destxLgr"  class="span12" readonly type="text" maxlength="60" value="<?php echo $result->destxLgr; ?>" required="required" name="destxLgr">
                                                        </td>
                                                        <td colspan="2">
                                                            <label for="destnro" >Número<span class="required">*</span></label>
                                                            <input id="destnro"  class="span12" readonly type="text" maxlength="60" value="<?php echo $result->destnro; ?>" required="required" name="destnro">
                                                        </td>
                                                        <td colspan="2">
                                                            <label for="destxCpl" >Comp.</label>
                                                            <input id="destxCpl"  class="span12" readonly type="text" maxlength="60" value="<?php echo $result->destxCpl; ?>" name="destxCpl">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="destCEP" >CEP<span class="required">*</span></label>
                                                            <input id="destCEP"  class="span12" readonly type="text" required="required" value="<?php echo $result->destCEP; ?>" name="destCEP">
                                                        </td>
                                                        <td colspan="4">
                                                            <label for="destxBairro" >Bairro<span class="required">*</span></label>
                                                            <input id="destxBairro"  class="span12" readonly type="text" maxlength="60" required="required" value="<?php echo $result->destxBairro; ?>" name="destxBairro">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <label for="destxMun" >Cidade<span class="required">*</span></label>
                                                            <input id="destxMun"  class="span12" readonly type="text" readonly required="required" value="<?php echo $result->destxMun; ?>" lang="60" name="destxMun">
                                                            <input id="destcMun"  type="hidden" required="required" value="<?php echo $result->destcMun; ?>" name="destcMun">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="destUF" >UF<span class="required">*</span></label>
                                                            <input id="destUF"  class="span12" readonly type="text" readonly required="required" value="<?php echo $result->destUF; ?>" name="destUF">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="destfone" >Fone</label>
                                                            <input id="destfone"  class="span12" readonly type="text" maxlength="14" value="<?php echo $result->destfone; ?>" name="destfone">
                                                        </td>
                                                        <td colspan="4">
                                                            <label for="" >E-mail</label>
                                                            <input id="destemail"  class="span12"  readonly type="email" value="<?php echo $result->destemail; ?>" name="destemail">
                                                        </td>
                                                    </tr>
                                                    <tr><td colspan="8"></td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">
                                                    Produtos
                                                </label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <div class="span12" id="divProdutos" style="margin-left: 0">
                                                                    <table class="table table-bordered" id="tblProdutos">
                                                                        <thead>
                                                                        <tr>
                                                                            <th> <input name="" type="checkbox" value="0" onclick="marcarTodas();" id="marcarTodos" /></th>
                                                                            <th>Cód.</th>
                                                                            <th>Desc.</th>
                                                                            <th>UN.Compra</th>
                                                                            <th style="width: 10%;">Qt.Convertida</th>
                                                                            <th style="width: 10%;">UN.Venda</th>
                                                                            <th>Compra (R$)</th>
                                                                            <th style="width: 10%;">Venda (R$)</th>
                                                                            <th>Qtd</th>
                                                                            <th>Total (R$)</th>
                                                                            <th>CFOP</th>
                                                                            <th>CST</th>
                                                                            <th style="text-align: center;">Atualizado</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php
                                                                        foreach ($produtos as $p) {

                                                                            $verificaProdutoByCProd             = $this->produtos_model->getByCProd($p->cProd);
                                                                            $verificaProdutoByCProdByFornecedor = $this->produtos_model->getByCProdByFornecedor($p->cProd);
                                                                            $unidade            = $p->uCom;

                                                                            $nome_produto       = 'Novo';
                                                                            $codigo             = 'Novo';
                                                                            $precoCompra        =  0;
                                                                            $estoque            =  0;
                                                                            $precoVenda         =  0;
                                                                            $qtdUnidadeVenda    =  1;
                                                                            $dessaciarProduto   = '';

                                                                            $vinculadoFornecedor = '<br/><a href="#searchProduto" id="aSearchProduto" cProd="'.$p->cProd.'" data-toggle="modal" class="btn-small btn-success aSearchProduto"><i class="icon-plus"></i> (Associar produto)</a>';

                                                                            if (count($verificaProdutoByCProdByFornecedor) > 0) {

                                                                                $produto        = $this->produtos_model->getById($verificaProdutoByCProdByFornecedor->idProduto);
                                                                                $unidade        = $produto->unidade;
                                                                                $codigo         = $produto->cProd;
                                                                                $produtoFilial  = $this->produtos_model->getProdutoFilialById($produto->idProdutos);
                                                                                $nome_produto   = '<a href="'.base_url().'index.php/produtos/editar/'.$produto->idProdutos.'" target="'.$produto->idProdutos.'">'.$produto->descricao.'</a>';

                                                                                $verificaUnidade     = $this->unidade_model->getUnidadeProdutoByIdProduto($produto->idProdutos, $produto->unidade, $unidade,  $this->session->userdata('filial_id') );
                                                                                $dessaciarProduto    = '<a onclick="desassociarProduto('.$verificaProdutoByCProdByFornecedor->idFornecedorproduto.')" class="btn-small btn-success"><i class="icon-remove"></i> (Desassociar produto)</a>';

                                                                                if (count($verificaUnidade) > 0) $qtdUnidadeVenda = $verificaUnidade->quantidade;

                                                                                if (count($produtoFilial) > 0) {
                                                                                    $precoCompra = $produtoFilial->precoCompra;
                                                                                    $precoVenda  = $produtoFilial->precoVenda;
                                                                                    $estoque     = $produtoFilial->estoque;
                                                                                }
                                                                            }

                                                                            if(count($verificaProdutoByCProdByFornecedor) > 0) $vinculadoFornecedor = '';

                                                                            echo '<tr>';
                                                                            echo '<td><input class="elementSelected" type="checkbox" name="elementSelected" value="'.$p->prodnfeid.'"></td>';
                                                                            echo '<td>' . $p->cProd . '<br/><small style="color: royalblue">('.$codigo.')</small></td>';
                                                                            echo '<td>' . $p->xProd . '<br/><small style="color: royalblue">('.$nome_produto.')'.$vinculadoFornecedor.'<br/>'.$dessaciarProduto.'</small></td>';
                                                                            echo '<td>' . $p->uCom . '<br/><small style="color: royalblue">('.$unidade.')</small></td>';
                                                                            echo '<td><input type="number" id="qtdUnidadeVenda_'.$p->prodnfeid.'" style="width: 70%;" name="qtdUnidadeVenda" value="'.$qtdUnidadeVenda.'"></td>';
                                                                            echo '<td><input type="text"   id="unidadeVenda_'.$p->prodnfeid.'" style="width: 70%;" name="unidadeVenda" value="'.$unidade.'"></td>';
                                                                            echo '<td>R$ ' . number_format($p->vProd, 2, ',', '.') . '<br/><small style="color: royalblue">(R$ '.number_format($precoCompra, 2, ',', '.').')</small></td>';
                                                                            echo '<td><input type="number" id="valorCompra_'.$p->prodnfeid.'" style="width: 70%;" name="valorVenda" value="'.$precoVenda.'"></td>';
                                                                            echo '<td>' . $p->qtd . '<br/><small style="color: royalblue">('.$estoque.')</small></td>';
                                                                            echo '<td>R$ ' . number_format($p->vProd * $p->qtd , 2, ',', '.') . '</td>';
                                                                            echo '<td>' . $p->CFOP . '</td>';
                                                                            echo '<td>' . $p->CST . '</td>';
                                                                            echo '<td style="text-align: center;">' . $p->estoque_atualizado . '</td>';
                                                                            echo '</tr>';
                                                                        } ?>
                                                                        <tr>
                                                                            <td colspan="11"> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <br/>
                                                                <div class="span12" style="margin-left: 0">
                                                                    <div class="span12" style="text-align: center">
                                                                        <a href="#" id="cadastroProdutos" class="btn">Cadastrar e Associar Produtos</a>
                                                                        <a href="#" id="entradaEstoque" class="btn">Atualizar Estoque / Valores</a>
                                                                        <a href="#" style="display: none;" id="gerarFinanceiro" class="btn">Gerar Financeiro</a>
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0" id="divTotalizadores">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">TOTAIS</label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">

                                                                <label for="vBCnf" >Base de Cálc.</label>
                                                                <input id="vBCnf" class="span12" type="number" disabled value="<?php echo $result->vBCnf; ?>" name="vBCnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vICMSnf" >Total ICMS</label>
                                                                <input id="vICMSnf" class="span12" type="number" disabled value="<?php echo $result->vICMSnf; ?>" name="vICMSnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vBCSTnf" >Base ICMS ST</label>
                                                                <input id="vBCSTnf" class="span12" type="number" disabled value="<?php echo $result->vBCSTnf; ?>" name="vBCSTnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vICMSSTnf" >Total ICMS ST</label>
                                                                <input id="vICMSSTnf" class="span12" type="number" disabled value="<?php echo $result->vICMSSTnf; ?>" name="vICMSSTnf">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="valortotalprod" >Total Produtos</label>
                                                                <input id="valortotalprod" class="span12" readonly type="number" value="<?php echo $result->valortotalprod; ?>" name="valortotalprod">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vTotTrib" >Total Tributos</label>
                                                                <input id="vTotTrib" class="span12" type="number" disabled value="<?php echo $result->vTotTrib; ?>" name="vTotTrib">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vFretenf" >Total Frete</label>
                                                                <input id="vFretenf" class="span12" type="number" readonly value="<?php echo $result->vFretenf; ?>" name="vFretenf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vOutronf" >Outras</label>
                                                                <input id="vOutronf" class="span12" type="number" readonly value="<?php echo $result->vOutronf; ?>" name="vOutronf">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="vIInf" >Total do Imposto de Importação </label>
                                                                <input id="vIInf" class="span12" disabled type="number" readonly value="<?php echo $result->vIInf; ?>" name="vIInf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vIPInf" >Total IPI</label>
                                                                <input id="vIPInf" class="span12" type="number" disabled value="<?php echo $result->vIPInf; ?>" name="vIPInf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vPISnf" >PIS</label>
                                                                <input id="vPISnf" class="span12" type="number" disabled value="<?php echo $result->vPISnf; ?>" name="vPISnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vCOFINSnf" >COFINS</label>
                                                                <input id="vCOFINSnf" class="span12" type="number" disabled value="<?php echo $result->vCOFINSnf; ?>" name="vCOFINSnf">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="vSegnf" >Total Seguro</label>
                                                                <input id="vSegnf" class="span12" type="number" readonly value="<?php echo $result->vSegnf; ?>" name="vSegnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vDescnf" >Total Desconto</label>
                                                                <input id="vDescnf" class="span12" type="number" readonly value="<?php echo $result->vDescnf; ?>" name="vDescnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vICMSDesonnf" >ICMS Deson.</label>
                                                                <input id="vICMSDesonnf" class="span12"  readonly type="number" disabled value="<?php echo $result->vICMSDesonnf; ?>" name="vICMSDesonnf">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="valortotalnf" >Total da Nota</label>
                                                                <input id="valortotalnf" class="span12" readonly type="number" value="<?php echo $result->vNF; ?>" name="valortotalnf">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0;display: none;">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">COBRANÇA</label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="nFatcob" >Número Fat.</label>
                                                                <input id="nFatcob" class="span12" readonly type="number" value="<?php echo $result->nFatcob; ?>" name="nFatcob">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vOrigcob" >Valor Original</label>
                                                                <input id="vOrigcob" class="span12" readonly type="number" value="<?php echo $result->vOrigcob; ?>" name="vOrigcob">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vDesccob" >Desconto</label>
                                                                <input id="vDesccob" class="span12" readonly type="number" value="<?php echo $result->vDesccob; ?>" name="vDesccob">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="vLiqcob" >Valor Líquido</label>
                                                                <input id="vLiqcob" class="span12" readonly type="number" value="<?php echo $result->vLiqcob; ?>" name="vLiqcob">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">TRANSPORTADOR</label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <label for="modFrete" >Tipo Frete<span class="required">*</span></label>
                                                                <select name="modFrete" class="span12" readonly>
                                                                    <option <?php if ($result->modFrete == '0') echo 'selected="selected"';?> value="0">Próprio</option>
                                                                    <option <?php if ($result->modFrete == '1') echo 'selected="selected"';?> value="1">Destinátario/Remetente</option>
                                                                    <option <?php if ($result->modFrete == '2') echo 'selected="selected"';?> value="2">Terceiros</option>
                                                                    <option <?php if ($result->modFrete == '9') echo 'selected="selected"';?> value="9">Sem Frete</option>
                                                                </select>
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="transpCNPJ" >CPF/CNPJ<span class="required">*</span></label>
                                                                <input id="transpCNPJ" class="span12" readonly value="<?php echo $result->transpCNPJ; ?>" type="text" name="transpCNPJ">
                                                            </td>

                                                            <td colspan="2">
                                                                <label for="transpIE" >Insc. Estadual</label>
                                                                <input id="transpIE" class="span12" readonly type="text" value="<?php echo $result->transpIE; ?>" name="transpIE">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="12">
                                                                <label for="transpxNome" >Nome/Razao Social<span class="required">*</span></label>
                                                                <input id="transpxNome" class="span12" readonly type="text" value="<?php echo $result->transpxNome; ?>" name="transpxNome">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label for="transpCEP" >CEP</label>
                                                                <input id="transpCEP" class="span12" readonly type="text" value="<?php echo $result->transpCEP; ?>" name="transpCEP">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="transpxLgr" >Logradouro</label>
                                                                <input id="transpxLgr" class="span12" readonly type="text" value="<?php echo $result->transpxLgr; ?>" name="transpxLgr">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="transpnro" >Nº</label>
                                                                <input id="transpnro" class="span12" readonly type="text" value="<?php echo $result->transpnro; ?>" name="transpnro">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="transpxCpl" >Com.</label>
                                                                <input id="transpxCpl" class="span12" readonly type="text" value="<?php echo $result->transpxCpl; ?>" name="transpxCpl">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <label for="transpxBairro" >Bairro</label>
                                                                <input id="transpxBairro" class="span12" readonly type="text" value="<?php echo $result->transpxBairro; ?>" name="transpxBairro">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="transpxMun" >Município</label>
                                                                <input id="transpxMun" class="span12" readonly type="text" value="<?php echo $result->transpxMun; ?>" name="transpxMun">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="transpUF" >UF</label>
                                                                <input id="transpUF" class="span12" readonly type="text" value="<?php echo $result->transpUF; ?>" name="transpUF">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <label for="qVol" >Quantidade</label>
                                                                <input id="qVol" class="span12" readonly type="text" value="<?php echo $result->qVol; ?>" name="qVol">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="esp" >Espécie</label>
                                                                <input id="esp" class="span12" readonly type="text" value="<?php echo $result->esp; ?>" name="esp">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="marca" >Marca</label>
                                                                <input id="marca" class="span12" readonly type="text" value="<?php echo $result->marca; ?>" name="marca">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <label for="placa" >Placa</label>
                                                                <input id="placa" class="span12" readonly type="text" value="<?php echo $result->placa; ?>" name="placa">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="VeicUF" >UF do Veículo</label>
                                                                <select name="VeicUF" class="span12" readonly>
                                                                    <option <?php if ($result->modFrete == '0') echo 'selected="selected"';?>  value=""></option>
                                                                    <option <?php if ($result->modFrete == 'mg') echo 'selected="selected"';?>  value="mg">Minas Gerais</option>
                                                                    <option <?php if ($result->modFrete == 'ac') echo 'selected="selected"';?>  value="ac">Acre</option>
                                                                    <option <?php if ($result->modFrete == 'al') echo 'selected="selected"';?>  value="al">Alagoas</option>
                                                                    <option <?php if ($result->modFrete == 'am') echo 'selected="selected"';?>  value="am">Amazonas</option>
                                                                    <option <?php if ($result->modFrete == 'ap') echo 'selected="selected"';?>  value="ap">Amapá</option>
                                                                    <option <?php if ($result->modFrete == 'ba') echo 'selected="selected"';?>  value="ba">Bahia</option>
                                                                    <option <?php if ($result->modFrete == 'ce') echo 'selected="selected"';?>  value="ce">Ceará</option>
                                                                    <option <?php if ($result->modFrete == 'df') echo 'selected="selected"';?>  value="df">Distrito Federal</option>
                                                                    <option <?php if ($result->modFrete == 'es') echo 'selected="selected"';?>  value="es">Espírito Santo</option>
                                                                    <option <?php if ($result->modFrete == 'go') echo 'selected="selected"';?>  value="go">Goiás</option>
                                                                    <option <?php if ($result->modFrete == 'ma') echo 'selected="selected"';?>  value="ma">Maranhão</option>
                                                                    <option <?php if ($result->modFrete == 'mt') echo 'selected="selected"';?>  value="mt">Mato Grosso</option>
                                                                    <option <?php if ($result->modFrete == 'ms') echo 'selected="selected"';?>  value="ms">Mato Grosso do Sul</option>
                                                                    <option <?php if ($result->modFrete == 'pa') echo 'selected="selected"';?>  value="pa">Pará</option>
                                                                    <option <?php if ($result->modFrete == 'pb') echo 'selected="selected"';?>  value="pb">Paraíba</option>
                                                                    <option <?php if ($result->modFrete == 'pr') echo 'selected="selected"';?>  value="pr">Paraná</option>
                                                                    <option <?php if ($result->modFrete == 'pe') echo 'selected="selected"';?>  value="pe">Pernambuco</option>
                                                                    <option <?php if ($result->modFrete == 'pi') echo 'selected="selected"';?>  value="pi">Piauí</option>
                                                                    <option <?php if ($result->modFrete == 'rj') echo 'selected="selected"';?>  value="rj">Rio de Janeiro</option>
                                                                    <option <?php if ($result->modFrete == 'ro') echo 'selected="selected"';?>  value="rn">Rio Grande do Norte</option>
                                                                    <option <?php if ($result->modFrete == 'ro') echo 'selected="selected"';?>  value="ro">Rondônia</option>
                                                                    <option <?php if ($result->modFrete == 'rs') echo 'selected="selected"';?>  value="rs">Rio Grande do Sul</option>
                                                                    <option <?php if ($result->modFrete == 'rr') echo 'selected="selected"';?>  value="rr">Roraima</option>
                                                                    <option <?php if ($result->modFrete == 'sc') echo 'selected="selected"';?>  value="sc">Santa Catarina</option>
                                                                    <option <?php if ($result->modFrete == 'se') echo 'selected="selected"';?>  value="se">Sergipe</option>
                                                                    <option <?php if ($result->modFrete == 'sp') echo 'selected="selected"';?>  value="sp">São Paulo</option>
                                                                    <option <?php if ($result->modFrete == 'to') echo 'selected="selected"';?>  value="to">Tocantins</option>
                                                                </select>
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="RNTC" >RNTC</label>
                                                                <input id="RNTC" class="span12" readonly type="text" value="<?php echo $result->RNTC; ?>" name="RNTC">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <label for="nVol" >Numeração</label>
                                                                <input id="nVol" class="span12" readonly type="text" value="<?php echo $result->nVol; ?>" name="nVol">
                                                            </td>
                                                            <td colspan="3">
                                                                <label for="pesoL" >Peso Liq(kg)</label>
                                                                <input id="pesoL" class="span12" readonly type="number" value="<?php echo $result->pesoL; ?>" name="pesoL">
                                                            </td>
                                                            <td colspan="2">
                                                                <label for="pesoB" >Peso Bruto(kg)</label>
                                                                <input id="pesoB" class="span12"  readonly type="number" value="<?php echo $result->pesoL; ?>" name="pesoB">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">INFORMAÇÕES ADICIONAIS</label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <label for="infCpl" >Info Adicionais</label>
                                                                <textarea id="infCpl" readonly class="span12" name="infCpl"><?php echo $result->infCpl; ?></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8" style="display: none;">
                                                                <label for="autXML" >Autorização Download XML</label>
                                                                <input id="autXML" class="span12" readonly type="text" value="<?php echo $result->autXML; ?>" name="autXML">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">Informações Nota Autorizada</label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="8">
                                                                <label for="chavenfe" >Chave da NF</label>
                                                                <input id="chavenfe" class="span12" readonly type="text" disabled name="chavenfe" value="<?php echo $result->chavenfe; ?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <label for="chavenfe" >Recibo</label>
                                                                <input id="recibo" class="span12" type="text" disabled name="recibo" value="<?php echo $result->recibo; ?>">
                                                            </td>
                                                            <td colspan="4">
                                                                <label for="chavenfe">Protocolo de autorização</label>
                                                                <input id="nProt" class="span12" type="text" disabled name="nProt" value="<?php echo $result->nProt; ?>">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <?php if ($result->status == '') {?>
                                                <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Salvar</button>
                                            <?php }?>
                                            <a href="<?php echo base_url() ?>index.php/coletorxml" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                .
            </div>
        </div>
    </div>
</div>

<!-- Modal searchCliente -->
<div id="searchProduto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchProduto"></div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url();?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url();?>assets/js/sistema/cliente.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">
    var cProd = null;

    $(document).ready(function(){

        $('.aSearchProduto').click(function (event) {
            cProd = $(this).attr('cProd');
            $('#div_searchProduto').load("<?php echo base_url();?>index.php/produtos/search");
        });

        $( "#cadastroProdutos" ).click(function() {

            var elemento = new Array();
            var unidadesVenda = new Array();
            var qtdsVenda = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                unidadesVenda.push($('#unidadeVenda_'+$(this).val()).val());
                qtdsVenda.push($('#qtdUnidadeVenda_'+$(this).val()).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
                return;
            }

            event.preventDefault();

            if (confirm('Deseja realmente cadastar os produtos na base de dados?')) {
                $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Cadastrando produtos Aguarde...</h3>'});

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/nfefornecedor/cadastrarProduto/",
                    data: {
                        elementos: elemento,
                        destCNPJ: $('#destCNPJ').val(),
                        unidadesVenda :  unidadesVenda,
                        qtdsVenda: qtdsVenda
                    },
                    success: function (retorno) {
                        location.reload();
                    }
                });
            }
        });

        $( "#entradaEstoque" ).click(function() {

            var elemento        = new Array();
            var valores         = new Array();
            var unidadesVenda   = new Array();
            var qtdsVenda       = new Array();

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                valores.push($('#valorCompra_'+$(this).val()).val());
                unidadesVenda.push($('#unidadeVenda_'+$(this).val()).val());
                qtdsVenda.push($('#qtdUnidadeVenda_'+$(this).val()).val());
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
                return;
            }

            event.preventDefault();

            if (confirm('Deseja realmente atualizar o estoque e valores?')) {
                $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Atualizando estoque e valores dos produtos Aguarde...</h3>'});

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/nfefornecedor/atualizarEstoqueValor/",
                    data: {
                        elementos: elemento,
                        valores: valores,
                        destCNPJ: $('#destCNPJ').val(),
                        unidadesVenda :  unidadesVenda,
                        qtdsVenda: qtdsVenda
                    },
                    success: function (retorno) {
                        location.reload();
                    }
                });
            }
        });
    });

    function marcarTodas() {
        if ($('#marcarTodos').prop("checked")){
            $('.elementSelected').each(
                function(){
                    $(this).attr("checked", true);
                    $(this).prop("checked", true);
                }
            );
        }else{
            $('.elementSelected').each(
                function(){
                    $(this).attr("checked", false);
                    $(this).prop("checked", false);
                }
            );
        }
    }

    function fecharModelSerchProduto(produto) {

        $('#searchProduto').modal('hide');
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Associando produto Aguarde...</h3>'});

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/nfefornecedor/associarProduto/",
            data: {
                produtoOrigem: produto.idProdutos,
                destCNPJ: $('#destCNPJ').val(),
                cProd : cProd,
                chave : '<?php echo $result->chavenfe;?>'
            },
            success: function (retorno) {
                location.reload();
            }
        });
    }

    function desassociarProduto(idFornecedorproduto) {
        if (confirm('Deseja realmente desassociar produto ?')) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/nfefornecedor/desassociarProduto/",
                data: {
                    idFornecedorproduto: idFornecedorproduto,
                },
                success: function (retorno) {
                    location.reload();
                }
            });
        }
    }

</script>

