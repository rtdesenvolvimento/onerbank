<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Emissão de NFS-e</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da NFS-e</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divCadastrarOs">
                                <?php if($custom_error == true){ ?>
                                    <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados incompletos, verifique os campos com asterisco.<br/><?php echo $custom_error;?></div>
                                <?php } ?>
                                <form action="<?php echo current_url(); ?>" method="post" id="formNFSe">

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span3">
                                            <label for="numeroRps" >Número RPS<span class="required">*</span></label>
                                            <input id="numeroRps"  class="span12" readonly type="text" required="required" name="numeroRps"
                                                   value="<?php echo $proximoNumeroRPS; ?>"/>
                                        </div>

                                        <div class="span3">
                                            <label for="serieRps" >Série RPS<span class="required">*</span></label>
                                            <input id="serieRps"  class="span12" readonly type="text" required="required" name="serieRps"
                                                   value="<?php echo $emitente->serieRps; ?>"/>
                                        </div>

                                        <div class="span3">
                                            <label for="numeroNFSe" >Número NFS-e<span class="required">*</span></label>
                                            <input id="numeroNFSe"  class="span12" readonly type="text" required="required" name="numeroNFSe"
                                                   value="<?php echo $proximoNumeroNFSE; ?>"/>
                                        </div>

                                        <div class="span3">
                                            <label for="dataEmissao" >Data de emissão<span class="required">*</span></label>
                                            <input id="dataEmissao"  class="span12" type="date" required="required" name="dataEmissao"
                                                   value="<?php echo date('Y-m-d');?>"/>
                                        </div>


                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">

                                        <div class="span12">
                                            <div class="formBuscaGSA" id="divBuscaCliente">
                                                <label for="cliente">
                                                    <a href="#searchCliente" id="aSearchCliente" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                                    Cliente<span class="required"></span>
                                                    <a href="#adicionarCliente" id="addCliente" data-toggle="modal" style="float: right;margin-right: 4px;">
                                                        <i id="pincliente" class="icon-plus-sign tip-right"></i></i>
                                                    </a>
                                                </label>
                                                <div class="controls">
                                                    <select name="clientes_id" class="span12" id="clientes_id" required="required">
                                                        <option value="">--selecione um cliente--</option>
                                                    </select>
                                                    <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;display: none;"></div>
                                                </div>

                                                <table width="100%">
                                                    <tr>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                        <td width="12.5%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="display: none;">
                                                            <label for="nomeClienteNota" >Nome<span class="required">*</span></label>
                                                            <input id="nomeClienteNota"  class="span12" type="text" required="required" name="nomeClienteNota">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <label for="cpfCnpjCliente" >CPF/CNPJ<span class="required">*</span></label>
                                                            <input id="cpfCnpjCliente"  class="span12" type="text" required="required" name="cpfCnpjCliente">
                                                        </td>

                                                        <td colspan="3">
                                                            <label for="inscricaoEstadualCliente" >Inscrição Estadual</label>
                                                            <input id="inscricaoEstadualCliente"  class="span12" type="text" name="inscricaoEstadualCliente">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="inscricaoMunicialCliente" >Inscrição Municipal</label>
                                                            <input id="inscricaoMunicialCliente"  class="span12" type="text" name="inscricaoMunicialCliente">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="enderecoCliente" >Endereço<span class="required">*</span></label>
                                                            <input id="enderecoCliente"  class="span12" type="text" required="required" name="enderecoCliente">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="numeroCliente" >Número<span class="required">*</span></label>
                                                            <input id="numeroCliente"  class="span12" type="text" required="required" name="numeroCliente">
                                                        </td>

                                                        <td colspan="2">
                                                            <label for="complementoCliente" >Comp.</label>
                                                            <input id="complementoCliente"  class="span12" type="text" name="complementoCliente">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="1">
                                                            <label for="cepCliente" >CEP<span class="required">*</span></label>
                                                            <input id="cepCliente"  class="span12" type="text" required="required" name="cepCliente">
                                                        </td>

                                                        <td colspan="3">
                                                            <label for="bairroCliente" >Bairro<span class="required">*</span></label>
                                                            <input id="bairroCliente"  class="span12" type="text" required="required" name="bairroCliente">
                                                        </td>

                                                        <td colspan="3">
                                                            <label for="cidadeCliente" >Cidade<span class="required">*</span></label>
                                                            <input id="cidadeCliente"  class="span12" type="text" required="required" name="cidadeCliente">
                                                        </td>

                                                        <td colspan="1">
                                                            <label for="ufCliente" >UF<span class="required">*</span></label>
                                                            <input id="ufCliente"  class="span12" type="text" required="required" name="ufCliente">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4">
                                                            <label for="telefoneCliente" >Fone</label>
                                                            <input id="telefoneCliente"  class="span12" type="text" name="telefoneCliente">
                                                        </td>
                                                        <td colspan="4">
                                                            <label for="" >E-mail</label>
                                                            <input id="emailCliente"  class="span12" type="text" name="emailCliente">
                                                        </td>
                                                    </tr>
                                                    <tr><td colspan="8"></td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="spa12">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">Vendedor<span
                                                        class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="usuarios_id" class="span12" id="usuarios_id" required="required">
                                                        <option value="">--Selecione um cliente--</option>
                                                        <?php foreach ($usuarios as $usuario) {?>
                                                            <option value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                                        <?php } ?>
                                                    </select>

                                                    <table width="100%">
                                                        <tr>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                            <td width="12.5%"></td>
                                                        </tr>
                                                        <tr style="display:none;">
                                                            <td colspan="4">
                                                                <label for="comissaoVendedor" >Comissão</label>
                                                                <input id="comissaoVendedor"  class="span12" type="text" name="comissaoVendedor">
                                                            </td>

                                                            <td colspan="4">
                                                                <label for="valorComissaoVendedor" >Valor da comissão</label>
                                                                <input id="valorComissaoVendedor"  class="span12" type="text" name="valorComissaoVendedor">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0">
                                        <div class="span6 offset3" style="text-align: center">
                                            <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button>
                                            <a href="<?php echo base_url() ?>index.php/notafiscalservico" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
                .
            </div>

        </div>
    </div>
</div>

<!-- Modal cadastrar novo cliente -->
<div id="adicionarCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Cadastrar / Editar o cliente</h3>
        </div>

        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span4">
                    <label for="tipoPessoa">Tipo de pessoa</label>
                    <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                        <option value="PF">Pessoa Física</option>
                        <option value="PJ">Pessoa Jurídica</option>
                    </select>
                </div>

                <div class="span4">
                    <label for="origem">Origem</label>
                    <select name="origem" id="origem" class="span12 chzn" required="required">
                        <option value="">selecione uma origem</option>
                        <option value="Particular">Particular</option>
                        <option value="Financeira">Financeira</option>
                        <option value="Seguradora">Seguradora</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span6">
                    <label for="nomeCliente" >Nome / Razão Social<span class="required">*</span></label>
                    <input id="nomeCliente"  class="span12" type="text" required="required" name="nomeCliente"
                           value="<?php echo set_value('nomeCliente'); ?>"/>

                    <input type="hidden" id="cliente_id" name="cliente_id" value="" />
                </div>

                <div class="span6">
                    <label for="nomeFantasiaApelido" >Nome Fantasia / Apelido</label>
                    <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                           value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                </div>


            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span8">
                    <label for="documento">CPF/CNPJ*</label>
                    <input id="documento" type="text" class="span12" required="required" name="documento"
                           value="<?php echo set_value('documento'); ?>"/>
                </div>

                <div class="span4" id="div_sexo">
                    <label for="sexo">Sexo</label>
                    <select name="sexo" id="sexo" class="span12 chzn" required="required">
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0" id="div_rg">
                <div class="span3">
                    <label for="rg">RG</label>
                    <input id="rg" type="text" class="span12" name="rg"
                           value="<?php echo set_value('rg'); ?>"/>
                    </select>
                </div>

                <div class="span3">
                    <label for="orgaoEmissor">Orgão Emissor</label>
                    <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                           value="<?php echo set_value('orgaoEmissor'); ?>"/>
                </div>

                <div class="span2">
                    <label for="estadoOrgaoEmissor">Estado</label>
                    <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                           value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                </div>

                <div class="span4">
                    <label for="dataOrgaoEmissor">Data Emissão</label>
                    <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                           value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="telefone">Telefone</label>
                    <input id="telefone" type="text" class="span12" name="telefone"
                           value="<?php echo set_value('telefone'); ?>"/>
                </div>

                <div class="span6">
                    <label for="celular">Celular</label>
                    <input id="celular" type="text" class="span12" name="celular"
                           value="<?php echo set_value('celular'); ?>"/>
                </div>



            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="span12" name="email"
                           value="<?php echo set_value('email'); ?>"/>
                </div>

                <div class="span6">
                    <label for="email">Data de nascimento</label>
                    <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                           value="<?php echo set_value('data_nascimento'); ?>"/>
                </div>
            </div>


            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Dados do endereço
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span3">
                    <label for="cep">CEP</label>
                    <input id="cep" type="text" name="cep" class="span12" onBlur="getConsultaCEP();"
                           value="<?php echo set_value('cep'); ?>"/>
                    <small>[TAB] consulta cep (Necessita Internet)</small>
                </div>

                <div class="span6">
                    <label for="rua">Rua</label>
                    <input id="rua" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                </div>

                <div class="span3">
                    <label for="numero">Número</label>
                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <label for="complemento">Complemento</label>
                    <input id="complemento" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span5">
                    <label for="bairro">Bairro</label>
                    <input id="bairro" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                </div>

                <div class="span4">
                    <label for="cidade">Cidade</label>
                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                </div>
                <div class="span3">
                    <label for="estado">Estado</label>
                    <input id="estado" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                </div>
            </div>

            <div id="div_contato" style="display: none;">
                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes">
                        <a href="#tab1" data-toggle="tab">
                            Dados do  Contato
                        </a>
                    </li>
                </ul>
                <div class="span12" style="padding: 1%; margin-left: 0">

                    <div class="span12">
                        <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                        <input id="contatoNomeCliente"  class="span12" type="text" name="contatoNomeCliente"
                               value="<?php echo set_value('contatoNomeCliente'); ?>"/>
                    </div>

                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span8">
                        <label for="contatoCpf">CPF</label>
                        <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                               value="<?php echo set_value('contatoCpf'); ?>"/>
                    </div>

                    <div class="span4">
                        <label for="contatoSexo">Sexo</label>
                        <select name="contatoSexo" id="contatoSexo" class="span12 chzn">
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                        </select>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="contatoTelefone">Telefone</label>
                        <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                               value="<?php echo set_value('contatoTelefone'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="contatoCelular">Celular</label>
                        <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                               value="<?php echo set_value('contatoCelular'); ?>"/>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="email">Email</label>
                        <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                               value="<?php echo set_value('contatoEmail'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="email">Data de nascimento</label>
                        <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                               value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Senha para acessar o portal do cliente
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <label for="senha">Senha</label>
                <input type="password" name="senha" class="span12" id="senha"
                       value=""/>
                <small>Deixe em branco e a senha padrão será o (cpj/cnpj) do cliente</small>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Observação
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <textarea class="span12" id="observacao"  name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6 offset3" style="text-align: center">
                    <button type="submit" class="btn btn-success">
                        <div id="div_buttom_adicionar"><i class="icon-plus icon-white"></i> Adicionar</div>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal searchCliente -->
<div id="searchCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchCliente"></div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $('#usuarios_id').val(<?php echo  $this->session->userdata('id');?>);

        $("#formNFSe").validate({
            rules:{
                cliente: {required:true},
                tecnico: {required:true},
                dataVenda: {required:true}
            },
            messages:{
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataVenda: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $('#aSearchCliente').click(function (event) {
            $('#div_searchCliente').load("<?php echo base_url();?>index.php/clientes/search")
        });

        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });

        $('#clientes_id').change(function (e) {
            var clientes_id = $(this).val();
            if (clientes_id) {
                $('#pincliente').attr('class', 'icon-pencil icon-white');
                $('#div_buttom_adicionar').html("Editar");

                $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                $('#aAdicionarVeiculo').removeAttr('disabled');
            } else {
                $('#cliente_id').val('');
                $('#tipoPessoa').val('PF');
                $('#origem').val('');
                $('#nomeCliente').val('');
                $('#sexo').val('M');
                $('#nomeFantasiaApelido').val('');
                $('#documento').val('');
                $('#rg').val('');
                $('#orgaoEmissor').val('');
                $('#estadoOrgaoEmissor').val('');
                $('#dataOrgaoEmissor').val('');
                $('#data_nascimento').val('');
                $('#telefone').val('');
                $('#celular').val('');
                $('#email').val('');
                $('#rua').val('');
                $('#numero').val('');
                $('#bairro').val('');
                $('#cidade').val('');
                $('#estado').val('');
                $('#cep').val('');
                $('#complemento').val('');
                $('#observacao').val('');
                $('#contatoNomeCliente').val('');
                $('#contatoSexo').val('');
                $('#contatoCpf').val('');
                $('#contatoEmail').val('');
                $('#contatoDataNascimento').val('');
                $('#contatoTelefone').val('');
                $('#contatoCelular').val('');
                $('#dataCadastro').val('');
                $('#senha').val('');

                $('#pincliente').attr('class', 'icon-plus-sign icon-white');
                $('#div_buttom_adicionar').html("<i class=\"icon-plus icon-white\"></i> Adicionar");
            }
        });

        $('#addCliente').click(function (event) {
            event.preventDefault();
            var cliente_id = $('#clientes_id').val();

            if (cliente_id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/consultaCliente",
                    data: "cliente_id=" + cliente_id,
                    dataType: 'json',
                    success: function (cliente) {

                        $('#cliente_id').val(cliente_id);
                        $('#tipoPessoa').val(cliente.tipoPessoa);
                        $('#origem').val(cliente.origem);
                        $('#nomeCliente').val(cliente.nomeCliente);
                        $('#sexo').val(cliente.sexo);
                        $('#nomeFantasiaApelido').val(cliente.nomeFantasiaApelido);
                        $('#documento').val(cliente.documento);
                        $('#rg').val(cliente.rg);
                        $('#orgaoEmissor').val(cliente.orgaoEmissor);
                        $('#estadoOrgaoEmissor').val(cliente.estadoOrgaoEmissor);
                        $('#dataOrgaoEmissor').val(cliente.dataOrgaoEmissor);
                        $('#data_nascimento').val(cliente.data_nascimento);
                        $('#telefone').val(cliente.telefone);
                        $('#celular').val(cliente.celular);
                        $('#email').val(cliente.email);
                        $('#rua').val(cliente.rua);
                        $('#numero').val(cliente.numero);
                        $('#bairro').val(cliente.bairro);
                        $('#cidade').val(cliente.cidade);
                        $('#estado').val(cliente.estado);
                        $('#cep').val(cliente.cep);
                        $('#complemento').val(cliente.complemento);
                        $('#observacao').val(cliente.observacao);
                        $('#contatoNomeCliente').val(cliente.contatoNomeCliente);
                        $('#contatoSexo').val(cliente.contatoSexo);
                        $('#contatoCpf').val(cliente.contatoCpf);
                        $('#contatoEmail').val(cliente.contatoEmail);
                        $('#contatoDataNascimento').val(cliente.contatoDataNascimento);
                        $('#contatoTelefone').val(cliente.contatoTelefone);
                        $('#contatoCelular').val(cliente.contatoCelular);
                        $('#dataCadastro').val(cliente.dataCadastro);
                        $('#senha').val(cliente.senha);
                    }
                });
            }
        });

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });

        $( "#formCliente" ).submit(function() {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/clientes/adicionarAjax",
                data: $("#formCliente").serialize(), // serializes the form's elements.
                success: function(cliente)
                {
                    if (cliente) {
                        cliente = JSON.parse(cliente);

                        $('#clientes_id option[value='+cliente.idClientes+']').remove();

                        $('#clientes_id').append('<option value="'+cliente.idClientes+'">' + cliente.nomeCliente + '</option>');
                        $('#clientes_id').val(cliente.idClientes);
                        $('#adicionarCliente').modal('hide');
                        $('#pincliente').attr('class', 'icon-pencil icon-white');
                        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                        $('#aAdicionarVeiculo').removeAttr('disabled');
                    }
                }
            });
        });
    });


    function fecharModelSercheCliente(cliente) {
        $('#searchCliente').modal('hide');
        $('#pincliente').attr('class', 'icon-edit icon-white');

        $('#nomeClienteNota').val(cliente.nomeCliente);
        $('#cpfCnpjCliente').val(cliente.documento);
        $('#inscricaoEstadualCliente').val(cliente.IE);
        $('#inscricaoMunicialCliente').val(cliente.IESUF);
        $('#enderecoCliente').val(cliente.rua);
        $('#numeroCliente').val(cliente.numero);
        $('#complementoCliente').val(cliente.complemento);
        $('#cepCliente').val(cliente.cep);
        $('#bairroCliente').val(cliente.bairro);
        $('#cidadeCliente').val(cliente.cidade);
        $('#ufCliente').val(cliente.estado);
        $('#telefoneCliente').val(cliente.telefone);
        $('#emailCliente').val(cliente.email);

        if(cliente.inativo == 1){
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ea1f1f 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#ee5f5b00');
            $('#div_cliente_restricao').show();
            $('#div_cliente_restricao').html('Cliente com restrições: '+cliente.observacaoInativo);
            $('#btnContinuar').hide();
            $('#btn-faturar').hide();
        } else {
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ededed 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#fff');
            $('#div_cliente_restricao').hide();
            $('#btnContinuar').show();
            $('#btn-faturar').show();
        }
    }
</script>

