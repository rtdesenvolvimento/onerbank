
<?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
    <a href="<?php echo base_url();?>index.php/nfse/adicionar" id="adicionarNFSE" class="btn btn-success">
        <i class="icon-plus icon-white"></i> Adicionar NFS-e
    </a>

    <a href="#" id="transmitirnfse" class="btn btn-info">
        <i class="icon-forward icon-trash"></i> Transmitir
    </a>

    <a href="#" id="imprimirnfse" class="btn btn-info">
        <i class="icon-print icon-trash"></i> Imprimir
    </a>

    <a href="#" id="cancelarnfse" class="btn btn-warning">
        <i class="icon-remove icon-trash"></i> Cancelar
    </a>

    <a href="#" id="baixarxmlnfse" class="btn btn-success">
        <i class="icon-download"></i> Baixar XML
    </a>

    <a href="#" id="excluirnfse" class="btn btn-danger">
        <i class="icon-trash icon-trash"></i> Excluir
    </a>
<?php } ?>

<?php

if(!$results){?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>NFS-e</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Data da Emissão</th>
                    <th>N. Lote</th>
                    <th>Série</th>
                    <th>N. RPS</th>
                    <th>N. Nota</th>
                    <th>Doc. Tomador</th>
                    <th>Cliente</th>
                    <th>Valor</th>
                    <th>Situação</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="11">Nenhuma NFS-e Cadastrada ate o momento</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else{?>

    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Vendas</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive" id="tbNFSE">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Data da Emissão</th>
                    <th>N. Lote</th>
                    <th>Série</th>
                    <th>N. RPS</th>
                    <th>N. Nota</th>
                    <th>Doc. Tomador</th>
                    <th>Cliente</th>
                    <th>Valor</th>
                    <th>Situação</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    $dataEmissao = date(('d/m/Y'),strtotime($r->dataEmissao));
                    $situacao = $r->situacao;

                    if ($situacao == 1) {
                        $situacao = '<span class="situacao_nota_pendente">PENDENTE</span>';
                    } else if ($situacao == 2) {
                        $situacao = '<span class="situacao_nota_autorizada">AUTORIZADA</span>';
                    } else if ($situacao == 3) {
                        $situacao = '<span class="situacao_nota_rejeitada">REJEITADA</span>';
                    } else if ($situacao == 4) {
                        $situacao = '<span class="situacao_nota_cancelada">CANCELADA</span>';
                    }

                    echo '<tr>';
                    echo '<td><input class="elementSelected" type="checkbox" situacao="'.$r->situacao.'"  numeroNFSe="'.$r->numeroNFSe.'" value="'.$r->idNFSE.'"></td>';
                    echo '<td style="text-align: left;">'.$dataEmissao.'</td>';
                    echo '<td style="text-align: left;">'.$r->numeroLote.'</td>';
                    echo '<td style="text-align: left;">'.$r->serieRps.'</td>';
                    echo '<td style="text-align: left;">'.$r->numeroRps.'</td>';
                    echo '<td style="text-align: left;">'.$r->numeroNFSe.'</td>';
                    echo '<td style="text-align: left;">'.$r->cpfCnpjCliente.'</td>';
                    echo '<td style="text-align: left"><a href="'.base_url().'index.php/clientes/visualizar/'.$r->clientes_id.'">'.$r->nomeClienteNota.'</a></td>';
                    echo '<td style="text-align: left;">R$ '.$r->valorServico.'</td>';
                    echo '<td style="text-align: left;" title="'.$r->rejeicaoMotivo.'">'.$situacao.'</td>';

                    echo '<td style="text-align: center;">';
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/nfse/editar/'.$r->idNFSE.'" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    }
                    echo '</td>';
                    echo '</tr>';
                }?>
                <tr>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php echo $this->pagination->create_links();}?>


<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>


<script type="text/javascript">
    $(document).ready(function(){

        $('#transmitirnfse').click(function (event) {
            var elemento = new Array();
            var situacao = 0;

            $('.elementSelected:checked').each(function () {
                elemento.push($(this).val());
                situacao =  parseInt($(this).attr('situacao'));
            });

            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {

                if (situacao === 2) {
                    alert("Nota de serviço já Autorizada!");
                    return;
                }

                if (situacao === 4) {
                    alert("Nota de serviço já Cancelada!");
                    return;
                }
                $.blockUI({ message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Autorizando a nfse Aguarde...</h3>' });
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/nfse/transmitir",
                    data: {
                        nfse_id: $(elemento).get(0)
                    },
                    dataType: "json",
                    success: function (data) {
                        $.unblockUI();
                        if (data.rejeicaoMotivo !== '') {
                            $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                            $('#dError-msg').html(data.rejeicaoMotivo);
                            $('.error-msg').show();
                        } else {
                            $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                            $('#dSuccess-msg').html("Nota Fiscal de Serviço Autorizada com sucesso!");
                            $('.success-msg').show();

                            window.open('<?php echo base_url();?>index.php/nfse/imprimir/'+data.numeroNFSe, '',
                                'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
                        }
                    }
                });
            }
        });

        $('#imprimirnfse').click(function (event) {
            var elemento = new Array();
            $('.elementSelected:checked').each(function () {
                elemento.push($(this).attr('numeroNFSe') );
            });
            if (elemento.length === 0) {
                alert('Nenhum Registo Selecionado!');
            } else {
                window.open('<?php echo base_url();?>index.php/nfse/imprimir/'+$(elemento).get(0), '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
            }
        });

        $('#cancelarnfse').click(function (event) {

            if (confirm('Deseja realmente cancelar a nfse?')) {

                var elemento    = new Array();
                var elementoId  = new Array();
                var situacao = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).attr('numeroNFSe'));
                    elementoId.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 4) {
                        alert("Nota de serviço já Cancelada!");
                        return;
                    }

                    if (situacao === 1) {
                        alert("Nota de serviço não Autorizada");
                        return;
                    }

                    if (situacao === 3) {
                        alert("Nota de serviço não Autorizada");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Cancelando a nfse Aguarde...</h3>'});

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/nfse/cancelar",
                        data: {
                            numeroNFSe: $(elemento).get(0),
                            idNFSE: $(elementoId).get(0)
                        },
                        dataType: "json",
                        success: function (data) {
                            $.unblockUI();
                            if (data.rejeicaoMotivo !== '') {
                                $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                                $('#dError-msg').html(data.rejeicaoMotivo);
                                $('.error-msg').show();
                            } else {
                                $("#tbNFSE").load("<?php echo current_url();?> #tbNFSE");
                                $('#dSuccess-msg').html("Nota Fiscal de Serviço Cancelada com sucesso!");
                                $('.success-msg').show();
                            }
                        }
                    });
                }
            }
        });

        $('#excluirnfse').click(function (event) {

            if (confirm('Deseja realmente excluir a nfse?')) {

                var elemento    = new Array();
                var elementoId  = new Array();
                var situacao = 0;

                $('.elementSelected:checked').each(function () {
                    elemento.push($(this).attr('numeroNFSe'));
                    elementoId.push($(this).val());
                    situacao =  parseInt($(this).attr('situacao'));
                });

                if (elemento.length === 0) {
                    alert('Nenhum Registo Selecionado!');
                } else {

                    if (situacao === 4) {
                        alert("Nota de serviço esta cancelada e não poderá ser excluida!");
                        return;
                    }

                    if (situacao === 2) {
                        alert("Nota de serviço esta autorizada e não poderá ser excluida!");
                        return;
                    }

                    $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Excluindo a nfe Aguarde...</h3>'});

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/nfse/excluir/"+$(elementoId).get(0),
                        dataType: "html",
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            }
        });

        $('#baixarxmlnfse').click(function (event) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/nfse/exporta",
                dataType: "html",
                success: function (data) {
                    window.open(data, '',
                        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
                }
            });
        });
    });
</script>