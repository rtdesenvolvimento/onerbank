<?php
date_default_timezone_set('America/Sao_Paulo');
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/redactor.css"/>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/redactor.min.js"></script>

<style>
    .widget-content {
        padding: 0px;
        border-bottom: 1px solid #cdcdcd;
    }
</style>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Editar OS</h5>
            </div>
            <div class="widget-content nopadding">

                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
                        <!--<a href="#tab5" data-toggle="tab">Vistoria</a></li>!-->
                        <li id="tabProdutos"><a href="#tab2" data-toggle="tab">Produtos</a></li>
                        <li id="tabServicos"><a href="#tab3" data-toggle="tab">Serviços</a></li>
                        <li id="tabAnexos"><a href="#tab4" data-toggle="tab">Acompanhamento</a></li>
                        <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eNfe')) { ?>
                            <li id="tabNF"><a href="#tab6" data-toggle="tab">NF-e</a></li>
                        <?php }?>

                        <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eNfse')) { ?>
                            <li id="tabNFS"><a href="#tab7" data-toggle="tab">NFS-e</a></li>
                        <?php } ?>
                    </ul>
                    <div class="tab-content">

                        <!--Principal-->
                        <div class="tab-pane active" id="tab1">

                            <div class="span12 well" id="divCadastrarOs">

                                <?php if($custom_error == true){ ?>
                                    <div class="span12 alert alert-danger" id="divInfo"><?php echo $custom_error;?></div>
                                <?php } ?>

                                <form action="<?php echo current_url(); ?>" method="post" id="formOs">
                                    <?php echo form_hidden('idOs', $result->idOs) ?>

                                    <div class="span12" style="margin-left: 0">

                                        <div class="span8">
                                            <div class="formBuscaGSA" id="divBuscaCliente" style="<?php if ($result->inativo == 1) echo  'background: linear-gradient(rgb(234, 31, 31) 1%, rgb(255, 255, 255) 100%);';?>">
                                                <label for="cliente">
                                                    Cliente<span class="required"></span>
                                                    <a href="#adicionarCliente" id="addCliente" data-toggle="modal" class="btn-small btn-success" style="float: right;margin-right: 4px;">
                                                        <i id="pincliente" class="icon-edit tip-right"> Adicionar/Editar</i>
                                                    </a>


                                                </label>
                                                <div class="controls">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="90%">
                                                                <select name="clientes_id" style="<?php if ($result->inativo == 1) echo  'background: #ee5f5b00';?>" class="span12" id="clientes_id" required="required">
                                                                    <option value="">--selecione um cliente--</option>
                                                                    <option  selected="selected" value="<?php echo $result->clientes_id; ?>"><?php echo $result->nomeCliente; ?></option>
                                                                </select>
                                                                <?php if ($result->inativo == 1) { ?>
                                                                    <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;">Cliente com restrições: <?php echo $result->observacaoInativo; ?></div>
                                                                <?php } else { ?>
                                                                    <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;display: none;"></div>
                                                                <?php } ?>
                                                            </td>
                                                            <td width="10%">
                                                                <a href="#searchCliente" id="aSearchCliente" class="btn-small btn-info search_lupa" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="span4">
                                            <div class="formBuscaGSA">
                                                <label for="tecnico">Responsável<span
                                                            class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="usuarios_id" class="span12" id="usuarios_id" required="required">
                                                        <option value="">--Selecione um cliente--</option>
                                                        <?php foreach ($usuarios as $usuario) {?>
                                                            <option <?php if ($result->usuarios_id == $usuario->idUsuarios) echo 'selected="selected"'; ?> value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0">

                                        <div class="span2">
                                            <label for="status">Status<span class="required">*</span></label>
                                            <select class="span12" name="status" id="status" value="">

                                                <?php if ($emitente->ramo_atividade == 1) { ?>

                                                    <option <?php if ($result->status == 'Orçamento') {
                                                        echo 'selected';
                                                    } ?> value="Orçamento">Orçamento
                                                    </option>

                                                    <option <?php if($result->status == 'Garantia'){echo 'selected';} ?> value="Garantia">Garantia</option>
                                                    <option <?php if($result->status == 'Em Produção'){echo 'selected';} ?>   value="Em Produção">Em Produção</option>
                                                    <option <?php if($result->status == 'Produção Pausada'){echo 'selected';} ?>   value="Produção Pausada">Produção Pausada</option>
                                                    <option <?php if($result->status == 'Produzido'){echo 'selected';} ?>   value="Produzido">Produzido</option>

                                                    <option <?php if ($result->status == 'Aberto') {
                                                        echo 'selected';
                                                    } ?> value="Aberto">Aberto
                                                    </option>
                                                    <option <?php if ($result->status == 'Faturado') {
                                                        echo 'selected';
                                                    } ?> value="Faturado">Faturado
                                                    </option>
                                                    <option <?php if ($result->status == 'Finalizado') {
                                                        echo 'selected';
                                                    } ?> value="Finalizado">Finalizado
                                                    </option>
                                                    <option <?php if ($result->status == 'Cancelado') {
                                                        echo 'selected';
                                                    } ?> value="Cancelado">Cancelado
                                                    </option>
                                                <?php } ?>

                                                <?php if ($emitente->ramo_atividade == 2) { ?>
                                                    <option <?php if($result->status == 'Orçamento'){echo 'selected';} ?> value="Orçamento">Orçamento</option>
                                                    <option <?php if($result->status == 'Garantia'){echo 'selected';} ?> value="Garantia">Garantia</option>
                                                    <option <?php if($result->status == 'Pronto'){echo 'selected';} ?> value="Pronto">Pronto</option>
                                                    <option <?php if($result->status == 'Aguardando Aprovacão'){echo 'selected';} ?> value="Aguardando Aprovação">Aguardando Aprovaçao</option>
                                                    <option <?php if($result->status == 'Aprovado'){echo 'selected';} ?> value="Aprovado">Aprovado</option>
                                                    <option <?php if($result->status == 'S/Conserto'){echo 'selected';} ?> value="S/Conserto">S/Conserto</option>
                                                    <option <?php if($result->status == 'Retirado'){echo 'selected';} ?> value="Retirado">Retirado</option>
                                                    <option <?php if($result->status == 'Recusado'){echo 'selected';} ?> value="Recusado">Recusado</option>
                                                    <option <?php if($result->status == 'Em Bancada'){echo 'selected';} ?> value="Em Bancada">Em Bancada</option>
                                                    <option <?php if($result->status == 'Aguardando peça'){echo 'selected';} ?> value="Aguardando peça">aguardando peça</option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="span4">
                                            <label for="dataInicial">Data Inicial<span class="required">*</span></label>
                                            <input id="dataInicial" class="span12" type="date" required="required"
                                                   name="dataInicial"
                                                   value="<?php echo $result->dataInicial?>"/>
                                        </div>
                                        <div class="span4">
                                            <label for="dataFinal">Previsão de entrega</label>
                                            <input id="dataFinal" class="span12" type="date" name="dataFinal"
                                                   value="<?php  echo $result->dataFinal; ?>"/>
                                        </div>

                                        <div class="span2">
                                            <label for="garantia">Garantia</label>
                                            <input id="garantia" type="text" class="span12" name="garantia"
                                                   value="<?php echo $result->garantia ?>"/>
                                        </div>
                                    </div>

                                    <div style="display: <?php if ($emitente->ramo_atividade == 2) echo 'none'; ?>">
                                        <div class="span12" style="margin-left: 0">

                                            <div class="span6">
                                                <div class="formBuscaGSA" id="div_buscaVeiculo" style="<?php if ($result->situacao != 'Sem restrição') { echo  '    background: linear-gradient(rgb(234, 31, 31) 1%, rgb(255, 255, 255) 100%);}'; } else  { echo 'background: linear-gradient(rgba(100, 234, 31, 0.44) 1%, rgb(255, 255, 255) 100%);';};?>">
                                                    <label for="veiculo">
                                                        <a href="#adicionarVeiculo"  id="aAdicionarVeiculo" role="button" data-toggle="modal"><i class="icon-plus-sign icon-white"></i></a>
                                                        <!--<a href="#Cliente" class="consulta_sinesp_click" style="float: right;margin-right: 4px;"><i class="icon-refresh tip-right"></i></i></a>!-->
                                                        Veículo*
                                                        <a href="#programarManutencaoVeiculo" id="btn-manutencao" role="button"
                                                           data-toggle="modal" class="btn-small btn-success"><i class="icon-plus"></i>
                                                            Programar Manutenção
                                                        </a>

                                                     </label>
                                                    <select class="span12" name="veiculo_id" required id="veiculo_id">
                                                        <option value="">--Selecione uma opção--</option>
                                                        <?php
                                                        foreach ($veiculos as $veiculos) {
                                                            if ($veiculos->idVeiculo == $result->veiculo_id) {
                                                                echo '<option value="' . $veiculos->idVeiculo . '" selected >' . $veiculos->tipoVeiculo . '/' . $veiculos->modelo . '/' . $veiculos->marca . '/' . $veiculos->placa . '</option>';
                                                            } else {
                                                                echo '<option value="' . $veiculos->idVeiculo . '">' . $veiculos->tipoVeiculo . '/' . $veiculos->modelo . '/' . $veiculos->marca . '/' . $veiculos->placa . '</option>';
                                                            }
                                                        } ?>
                                                    </select>
                                                    <div id="div_veiculo_restricao" style="text-align: center;margin-top: -8px;"><?php echo $result->situacao ;?></div>
                                                </div>
                                            </div>
                                            <div class="span6">

                                                <div class="formBuscaGSA">
                                                    <label for="kilometragementrada">Kilometragem de entrada e saída</label>

                                                        <input id="kilometragementrada" class="span6" style="float: left;margin-right: 10px;" type="number"
                                                           name="kilometragementrada"
                                                           value="<?php echo $result->kilometragementrada ?>"/>

                                                    <input id="kilometragemsaida" class="span6" type="number"
                                                           name="kilometragemsaida"
                                                           value="<?php echo $result->kilometragemsaida ?>"/>
                                                    <div id="div_veiculo_restricao" style="text-align: center;margin-top: -8px;">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0;margin-bottom: 20px;">
                                        <div class="span6" style="text-align: center">
                                            <label for="gnv">GNV</label>
                                            <input type="radio" required="required" name="gnv" <?php if ($result->gnv == 'r') echo 'checked="checked"';?> value="r">&nbsp;R&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="gnv" <?php if ($result->gnv == 'I') echo 'checked="checked"';?> value="I">&nbsp;I&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="gnv" <?php if ($result->gnv == 'II') echo 'checked="checked"';?> value="II">&nbsp;II&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="gnv" <?php if ($result->gnv == 'III') echo 'checked="checked"';?> value="III">&nbsp;III&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="gnv" <?php if ($result->gnv == 'IIII') echo 'checked="checked"';?> value="IIII">&nbsp;IIIII&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="gnv" <?php if ($result->gnv == 'sem') echo 'checked="checked"';?> value="sem">&nbsp;sem&nbsp;&nbsp;&nbsp;
                                        </div>

                                        <div class="span6"  style="text-align: center">
                                            <label for="tanque">Tanque</label>
                                            <input type="radio" required="required"  name="tanque" <?php if ($result->tanque == 'E') echo 'checked="checked"';?> value="E">&nbsp;E&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="tanque" <?php if ($result->tanque == '1/4') echo 'checked="checked"';?> value="1/4">&nbsp;1/4&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="tanque" <?php if ($result->tanque == '1/2') echo 'checked="checked"';?> value="1/2">&nbsp;1/2&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="tanque" <?php if ($result->tanque == '3/4') echo 'checked="checked"';?> value="3/4">&nbsp;3/4&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="tanque" <?php if ($result->tanque == 'F') echo 'checked="checked"';?> value="F">&nbsp;F&nbsp;&nbsp;&nbsp;
                                            <input type="radio" required="required"  name="tanque" <?php if ($result->tanque == 'sem') echo 'checked="checked"';?> value="sem">&nbsp;sem&nbsp;&nbsp;&nbsp;
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0">
                                        <div class="span4">
                                            <label for="totalCusto">Total do custo</label>
                                            <input id="totalCusto" class="span12" type="number" readOnly
                                                   name="totalCusto" value="<?php echo $totalCusto; ?>"/>
                                        </div>

                                        <div class="span4">
                                            <label for="totalVenda">Total da venda</label>
                                            <input id="totalVenda" class="span12" type="number" readOnly
                                                   name="totalVenda" value="<?php echo $totalSubTotal; ?>"/>
                                        </div>

                                        <div class="span4">
                                            <label for="totalLucro">Total do lucro</label>
                                            <input id="totalLucro" class="span12" type="number" readOnly
                                                   name="totalLucro" value="<?php echo $totalLucro; ?>"/>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0">

                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#descricaoProdutoServico" data-toggle="tab">Observação</a></li>
                                            <li><a href="#tabAcessorios" data-toggle="tab">Acessórios</a></li>
                                            <li><a href="#defeito" data-toggle="tab">Defeito Reclamado</a></li>
                                            <li><a href="#relatorioTecnico" data-toggle="tab">Laudo Técnico</a></li>
                                            <li><a href="#defeitoConstatado" data-toggle="tab">Defeito Constatado</a></li>
                                        </ul>

                                        <div class="tab-content">


                                            <div class="tab-pane active" id="descricaoProdutoServico">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <textarea class="span12" name="descricaoProduto" id="descricaoProduto"
                                                                  cols="30"
                                                                  rows="5"><?php echo $result->descricaoProduto ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tabAcessorios">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <textarea class="span12" name="acessorios" id="acessorios" cols="30"
                                                                  rows="5"><?php echo $result->acessorios ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="defeito">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <textarea class="span12" name="defeito" id="defeito" cols="30"
                                                                  rows="5"><?php echo $result->defeito ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="relatorioTecnico">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <textarea class="span12" name="laudoTecnico" id="laudoTecnico" cols="30"
                                                                  rows="5"><?php echo $result->laudoTecnico ?></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="defeitoConstatado">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <textarea class="span12" name="observacoes" id="observacoes" cols="30"
                                                                  rows="5"><?php echo $result->observacoes ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span12" style="margin-left: 0; text-align: center;">

                                    <button class="btn btn-primary" id="btnContinuar"><i
                                                class="icon-white icon-ok"></i> Salvar
                                    </button>

                                     <?php if ($result->faturado == 0) { ?>
                                        <a href="#modal-faturar" id="btn-faturar" role="button"
                                           data-toggle="modal" class="btn btn-success"><i class="icon-file"></i>
                                            Faturar</a>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eNfse')) { ?>
                                        <?php if ($result->nfse_gerada == 0) { ?>
                                            <a href="<?php echo base_url() ?>index.php/os/gerarNFS/<?php echo $result->idOs; ?>"
                                               class="btn btn-success"><i class="icon-refresh"></i> Gerar NFS-e</a>
                                        <?php } ?>
                                    <?php  }?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eNfe')) { ?>
                                        <?php if ($result->nfe_gerada == 0) { ?>
                                            <a href="<?php echo base_url() ?>index.php/os/gerarNF/<?php echo $result->idOs; ?>"
                                               class="btn btn-success"><i class="icon-refresh"></i> Gerar NF-e</a>
                                        <?php } ?>
                                    <?php } ?>

                                    <a href="<?php echo base_url() ?>index.php/os/visualizar/<?php echo $result->idOs; ?>"
                                       class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar OS</a>

                                    <?php if ($vistoria) {?>
                                        <a href="<?php echo base_url() ?>index.php/vistoria/editar/<?php echo $vistoria->idVistoria; ?>"
                                            class="btn btn-inverse"><i class="icon icon-list"></i> Abrir Vistoria</a>
                                    <?php } ?>

                                    <a href="#modal-email" id="btn-email" role="button"
                                       data-toggle="modal" class="btn btn-success"><i class="icon-envelope"></i>
                                        Enviar-Email</a>

                                    <?php if ($result->produzir == 0 || $result->producaopausada == 1) { ?>
                                        <a href="<?php echo base_url() ?>index.php/os/produzirOS/<?php echo $result->idOs; ?>"
                                           class="btn btn-warning"><i class="icon-fast-forward"></i> Produzir</a>
                                    <?php } else if ($result->producaopausada == 0){ ?>
                                        <a href="<?php echo base_url() ?>index.php/os/pausarProducaoOS/<?php echo $result->idOs; ?>"
                                           class="btn btn-warning"><i class="icon-fast-forward"></i> Pausar Produção</a>
                                    <?php }  ?>

                                    <?php if ($result->faturado == 1) { ?>
                                        <!--<a href="<?php echo base_url() ?>index.php/os/visualizarRecibo/<?php echo $result->idOs; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Recibo</a>!-->
                                     <?php } ?>

                                    <a href="<?php echo base_url() ?>index.php/os" class="btn"><i
                                                class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab5">

                        </div>

                        <!--Produtos-->
                        <div class="tab-pane" id="tab2">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <form id="formProdutos" action="<?php echo base_url() ?>index.php/os/adicionarProduto"
                                      method="post">
                                    <div class="span5">
                                        <input type="hidden" name="idProduto" id="idProduto"/>
                                        <input type="hidden" name="idSetorProduto" id="idSetorProduto"/>
                                        <input type="hidden" name="idOsProduto" id="idOsProduto"
                                               value="<?php echo $result->idOs ?>"/>
                                        <input type="hidden" name="estoque" id="estoque" value=""/>
                                        <input type="hidden" name="preco" id="preco" value=""/>
                                        <input type="hidden" name="filial_customizada" id="filial_customizada" value=""/>

                                        <div class="formBuscaGSA">
                                            <label for="">
                                                Produto
                                                <a href="#modal-consulta-estoque" id="addConsultaProduto" data-toggle="modal">
                                                    <i id="pincliente" style="float: right;margin-right: 10px;" class="icon-bar-chart icon-white"></i></a>
                                            </label>
                                            <div class="controls">
                                                <select name="produto" class="span12" id="produto" required="required">
                                                    <option value="">--Selecione um cliente--</option>
                                                    <?php foreach ($produtosList as $produto) {?>
                                                        <option value="<?php echo $produto->idProdutos; ?>"><?php echo $produto->descricao; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span1">
                                        <label for="">Qtd.</label>
                                        <input type="text" placeholder="Quantidade" id="quantidade" value="1" name="quantidade"
                                               class="span12"/>
                                    </div>

                                    <div class="span1">
                                        <label for="">Custo</label>
                                        <input type="text" placeholder="custo" id="precoCusto" name="precoCusto"
                                               class="span12"/>
                                    </div>

                                    <div class="span1">
                                        <label for="">Venda</label>
                                        <input type="text" placeholder="venda" id="precoVendaOS" name="precoVendaOS"
                                               class="span12"/>
                                    </div>

                                    <div class="span1">
                                        <label for="">&nbsp;</label>
                                        <select name="sel_comissao" style="width: 80px;">
                                            <option value="R$" selected="selected">R$</option>
                                            <option value="%">%</option>
                                        </select>
                                    </div>

                                    <div class="span1">
                                        <label for="">Comissão</label>
                                        <input type="text" placeholder="Comissão" id="comissaoProduto" name="comissaoProduto"
                                               class="span12"/>
                                    </div>

                                    <div class="span2">
                                        <label for="">&nbsp;</label>
                                        <button class="btn btn-success span12" id="btnAdicionarProduto"><i
                                                    class="icon-white icon-plus"></i> Adicionar
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="span12" id="divProdutos" style="margin-left: 0;display: none;">
                                <table class="table table-responsive" id="tblProdutos">
                                    <thead>
                                    <tr>
                                        <th style="text-align: left;">Produto</th>
                                        <th style="text-align: right;">Quantidade</th>
                                        <th style="text-align: right;">Preço Custo</th>
                                        <th style="text-align: right;">Preço Venda</th>
                                        <th>Ações</th>
                                        <th style="text-align: right;">Sub-total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $total = 0;
                                    $totalCusto = 0;

                                    foreach ($produtos as $p) {

                                        $total = $total + $p->subTotal;
                                        $totalCusto = $totalCusto + $p->precoCusto;

                                        echo '<tr>';
                                        echo '<td style="text-align: left;">' . $p->descricao . '</td>';
                                        echo '<td style="text-align: right;">' . $p->quantidade . strtolower($p->unidade) . '</td>';
                                        echo '<td style="text-align: right;">R$  ' . $p->precoCusto . '</td>';
                                        echo '<td style="text-align: right;">R$  ' . $p->precoVendaOS . '</td>';
                                        echo '<td><a href="" idAcao="' . $p->idProdutos_os . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                        echo '<td style="text-align: right;">R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                        echo '</tr>';
                                    } ?>

                                    <tr>
                                        <td colspan="5" style="text-align: right"><strong>Total:</strong></td>
                                        <td>
                                            <strong>
                                                R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                <input type="hidden" id="total-venda"
                                                       value="<?php echo number_format($total, 2); ?>">
                                                <input type="hidden" id="total-custo-venda"
                                                       value="<?php echo number_format($totalCusto, 2); ?>">
                                            </strong>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="accordion" id="collapse-group">
                            <?php
                            $contador = 0;
                            foreach ($setores as $setor) {
                                $nome_setor = $setor->nome;
                                $open = '';
                                $icon = 'icon-remove';

                                $ordemproducao = $this->db->get_where('ordemproducao' , array('os_id' => $result->idOs , 'setor_id' => $setor->idSetor ))->row();

                                if (count($ordemproducao) > 0) {
                                    $isProduzido = $ordemproducao->produzido;

                                    if ($isProduzido == 1){
                                        $icon = 'icon-ok';
                                    }
                                }

                                if ( count($setor) == 1) {
                                    $icon = 'icon-ok';
                                    $open = 'in';
                                }

                                if ($result->setorAtual_id == $setor->idSetor) {
                                    $nome_setor = '>> Veículo atualmente na '.$nome_setor.' <<';
                                    $open = 'in';
                                }
                                ?>
                                    <div class="accordion-group widget-box">
                                        <div class="accordion-heading">
                                            <div class="widget-title">
                                                <a data-parent="#collapse-group" href="#<?php echo $setor->idSetor; ?>" data-toggle="collapse">
                                                    <span class="icon"><i class="<?php echo $icon;?>"></i></span><h5><?php echo $nome_setor; ?></h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="collapse <?php echo $open; ?> accordion-body" id="<?php echo $setor->idSetor; ?>">
                                            <div class="widget-content">
                                                <div class="span12" id="divProdutos_<?php echo $setor->idSetor; ?>" style="margin-left: 0">
                                                    <table class="table table-responsive" id="tblProdutos">
                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th style="text-align: left;">Produto</th>
                                                            <th style="text-align: right;">Quantidade</th>
                                                            <th style="text-align: right;">Preço Custo</th>
                                                            <th style="text-align: right;">Preço Venda</th>
                                                            <th style="text-align: right;">Comissão</th>
                                                            <th style="text-align: right;">Sub-total</th>
                                                            <th>Ações</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $total = 0;
                                                        $totalCusto = 0;

                                                        foreach ($produtos as $p) {

                                                            if ($p->setor_id == $setor->idSetor) {

                                                                $total      = $total + $p->subTotal;
                                                                $totalCusto = $totalCusto + $p->precoCusto;
                                                                $produzido  = $p->produzido_os;
                                                                $backgroud = '#da4f49;';
                                                                $nome_responsavel_producao = '';
                                                                $data_produzido = $p->data_produzido;
                                                                $hora_produzido = $p->hora_produzido;

                                                                if ($produzido == 1) {
                                                                    $backgroud = '#5bb75b;';
                                                                }

                                                                if ($data_produzido) {
                                                                    $data_produzido = date(('d/m/Y'), strtotime($data_produzido));
                                                                }


                                                                if ($p->responsavel_id) {
                                                                    $responsavel = $this->db->get_where('usuarios' , array('idUsuarios' => $p->responsavel_id ))->row();
                                                                    if (count($responsavel) > 0) {
                                                                        $nome_responsavel_producao = $responsavel->nome;
                                                                    }
                                                                }
                                                                echo '<tr>';
                                                                echo '<td style="width: 3%;background:'.$backgroud.'"></td>';
                                                                echo '<td style="width: 50%">' . $p->descricao.'<br/>'.$nome_responsavel_producao.'<br/>'.$data_produzido.' '.$hora_produzido. '</td>';
                                                                echo '<td style="text-align: right;">' . $p->quantidade . strtolower($p->unidade) . '</td>';
                                                                echo '<td style="text-align: right">R$  ' . $p->precoCusto . '</td>';
                                                                echo '<td style="text-align: right">R$  ' . $p->precoVendaOS . '</td>';
                                                                echo '<td style="text-align: right">R$  ' . $p->valorComissao . '</td>';
                                                                echo '<td style="text-align: right">R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                                                echo '<td style="text-align: center"><a href="" idAcao="' . $p->idProdutos_os . '" setor_id="' . $setor->idSetor . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                                                echo '</tr>';
                                                            }
                                                        } ?>

                                                        <tr>
                                                            <td colspan="7" style="text-align: right"><strong>Total:</strong></td>
                                                            <td style="text-align: right">
                                                                <strong>
                                                                    R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                                    <input type="hidden" id="total-venda"
                                                                           value="<?php echo number_format($total, 2); ?>">
                                                                    <input type="hidden" id="total-custo-venda"
                                                                           value="<?php echo number_format($totalCusto, 2); ?>">
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php } ?>
                            </div>

                        </div>

                        <!--Serviços-->
                        <div class="tab-pane" id="tab3">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                    <form id="formServicos"
                                          action="<?php echo base_url() ?>index.php/os/adicionarServico" method="post">

                                        <div class="span5">
                                            <div class="formBuscaGSA">
                                            <input type="hidden" name="idServico" id="idServico"/>
                                            <input type="hidden" name="idSetorServico" id="idSetorServico"/>
                                            <input type="hidden" name="idOsServico"
                                                   value="<?php echo $result->idOs ?>"/>
                                            <input type="hidden" name="precoServico" id="precoServico" value=""/>
                                            <label for="">Serviço</label>
                                            <div class="controls">
                                                <select name="servico" class="span12" id="servico" required="required">
                                                    <option value="">--Selecione um cliente--</option>
                                                    <?php foreach ($servicosList as $servico) {?>
                                                        <option value="<?php echo $servico->idServicos; ?>"><?php echo $servico->nome; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            </div>
                                        </div>


                                        <div class="span1">
                                            <label for="">Custo</label>
                                            <input type="text" placeholder="custo" id="precoCustoServico"
                                                   name="precoCustoServico" class="span12"/>
                                        </div>

                                        <div class="span1">
                                            <label for="">Venda</label>
                                            <input type="text" placeholder="venda" id="precoVendaServico"
                                                   name="precoVendaServico" class="span12"/>
                                        </div>

                                        <div class="span1">
                                            <label for="">&nbsp;</label>
                                            <select name="sel_comissao" style="width: 80px;">
                                                <option value="R$" selected="selected">R$</option>
                                                <option value="%">%</option>
                                            </select>
                                        </div>

                                        <div class="span2">
                                            <label for="">Comissão</label>
                                            <input type="text" placeholder="Comissão" id="comissaoServico" name="comissaoServico"
                                                   class="span12"/>
                                        </div>

                                        <div class="span2">
                                            <label for="">&nbsp;</label>
                                            <button class="btn btn-success span12"><i class="icon-white icon-plus"></i>
                                                Adicionar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            <div class="span12" id="divServicos" style="margin-left: 0;display: none;">
                                    <table class="table table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Serviço</th>
                                            <th>Custo</th>
                                            <th>Preço</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $total = 0;
                                        $totalCusto = 0;

                                        foreach ($servicos as $s) {

                                            $preco = $s->subTotal;
                                            $total = $total + $preco;
                                            $totalCusto = $totalCusto + $s->precoCustoServico;

                                            echo '<tr>';
                                            echo '<td>' . $s->nome . '</td>';
                                            echo '<td>R$ ' . number_format($s->precoCustoServico, 2, ',', '.') . '</td>';
                                            echo '<td>R$ ' . number_format($preco, 2, ',', '.') . '</td>';
                                            echo '<td><span idAcao="' . $s->idServicos_os . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></span></td>';
                                            echo '</tr>';
                                        } ?>

                                        <tr>
                                            <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                            <td>
                                                <strong>
                                                    R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                    <input type="hidden" id="total-servico"
                                                           value="<?php echo number_format($total, 2); ?>">
                                                    <input type="hidden" id="total-custo-servico"
                                                           value="<?php echo number_format($totalCusto, 2); ?>">
                                                </strong>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <div class="accordion" id="collapse-group">
                                    <?php
                                    $contador = 0;
                                    foreach ($setores as $setor) {
                                        $nome_setor = $setor->nome;
                                        $open = '';
                                        $icon = 'icon-remove';

                                        $ordemproducao = $this->db->get_where('ordemproducao' , array('os_id' => $result->idOs , 'setor_id' => $setor->idSetor ))->row();

                                        if (count($ordemproducao) > 0) {
                                            $isProduzido = $ordemproducao->produzido;

                                            if ($isProduzido == 1){
                                                $icon = 'icon-ok';
                                            }
                                        }

                                        if ( count($setor) == 1) {
                                            $icon = 'icon-ok';
                                            $open = 'in';
                                        }


                                        if ($result->setorAtual_id == $setor->idSetor) {
                                            $nome_setor = '>> Veículo atualmente na '.$nome_setor.' <<';
                                            $open = 'in';
                                        }
                                        ?>
                                        <div class="accordion-group widget-box">
                                            <div class="accordion-heading">
                                                <div class="widget-title">
                                                    <a data-parent="#collapse-group" href="#servico_<?php echo $setor->idSetor; ?>" data-toggle="collapse">
                                                        <span class="icon"><i class="<?php echo $icon;?>"></i></span><h5><?php echo $nome_setor; ?></h5>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="collapse <?php echo $open; ?> accordion-body" id="servico_<?php echo $setor->idSetor; ?>">
                                                <div class="widget-content">
                                                    <div class="span12" id="divServicos_<?php echo $setor->idSetor; ?>" style="margin-left: 0">
                                                        <table class="table table-responsive" id="tblProdutos">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th style="text-align: left;">Serviço</th>
                                                                <th style="text-align: right;">Preço Custo</th>
                                                                <th style="text-align: right;">Preço Venda</th>
                                                                <th style="text-align: right;">Comissão</th>
                                                                <th style="text-align: right;">Sub-total</th>
                                                                <th>Ações</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $total = 0;
                                                            $totalCusto = 0;

                                                            foreach ($servicos as $p) {

                                                                if ($p->setor_id == $setor->idSetor) {

                                                                    $total      = $total + $p->subTotal;
                                                                    $totalCusto = $totalCusto + $p->preco;
                                                                    $produzido  = $p->produzido_os;
                                                                    $backgroud = '#da4f49;';
                                                                    $nome_responsavel_producao = '';
                                                                    $data_produzido = $p->data_produzido;
                                                                    $hora_produzido = $p->hora_produzido;

                                                                    if ($data_produzido) {
                                                                        $data_produzido = date(('d/m/Y'), strtotime($data_produzido));
                                                                    }

                                                                    if ($produzido == 1) {
                                                                        $backgroud = '#5bb75b;';
                                                                    }

                                                                    if ($p->responsavel_id) {
                                                                        $responsavel = $this->db->get_where('usuarios' , array('idUsuarios' => $p->responsavel_id ))->row();
                                                                        if (count($responsavel) > 0) {
                                                                            $nome_responsavel_producao = $responsavel->nome;
                                                                        }
                                                                    }
                                                                    echo '<tr>';
                                                                    echo '<td style="width: 3%;background:'.$backgroud.'"></td>';
                                                                    echo '<td style="width: 50%;">' . $p->nome.'<br/>'.$nome_responsavel_producao.'<br/>'.$data_produzido.' '.$hora_produzido. '</td>';
                                                                    echo '<td style="text-align: right">R$  ' . $p->precoCustoServico . '</td>';
                                                                    echo '<td style="text-align: right">R$  ' . $p->preco . '</td>';
                                                                    echo '<td style="text-align: right">R$  ' . $p->valorComissao . '</td>';
                                                                    echo '<td style="text-align: right">R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                                                    echo '<td style="text-align: center"><span idAcao="' . $p->idServicos_os . '" setor_id="' . $setor->idSetor . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></span></td>';
                                                                    echo '</tr>';
                                                                }
                                                            } ?>

                                                            <tr>
                                                                <td colspan="6" style="text-align: right"><strong>Total:</strong></td>
                                                                <td style="text-align: right">
                                                                    <strong>
                                                                        R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                                        <input type="hidden" id="total-venda"
                                                                               value="<?php echo number_format($total, 2); ?>">
                                                                        <input type="hidden" id="total-custo-venda"
                                                                               value="<?php echo number_format($totalCusto, 2); ?>">
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                        </div>

                        <!--Anexos-->
                        <div class="tab-pane" id="tab4">
                             <div class="span12 well" style="padding: 1%; margin-left: 0" id="form-anexos">
                                <form id="formAnexos" enctype="multipart/form-data" action="javascript:;"
                                      accept-charset="utf-8" method="post">

                                    <div class="span12" style="margin-left: 0">
                                        <label for="descricao">Local da Foto*</label>
                                        <input type="text" class="span12" id="localfoto" required="required" name="localfoto">
                                    </div>

                                    <div class="span12" style="margin-left: 0">
                                        <label for="descricao">Observação</label>
                                        <textarea class="span12" rows="5" cols="5" id="observacaoimg" name="observacaoimg"></textarea>
                                    </div>

                                    <div class="span9">

                                        <input type="hidden" name="idOsServico"
                                               value="<?php echo $result->idOs ?>"/>
                                        <label for="">Fotos / Anexos*</label>
                                        <input type="file" class="span12" required="required" name="userfile[]" multiple="multiple"
                                               size="20"/>
                                    </div>
                                    <div class="span2">
                                        <label for="">.</label>
                                        <button class="btn btn-success span12"><i class="icon-white icon-plus"></i>
                                            Nova Foto / Anexo
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="span12" id="divAnexos" style="margin-left: 0">
                                <?php
                                $cont = 1;
                                $flag = 5;
                                foreach ($anexos as $a) {

                                    if ($a->thumb == null) {
                                        $thumb = base_url() . 'assets/img/icon-file.png';
                                        $link = base_url() . 'assets/img/icon-file.png';
                                    } else {
                                        $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                        $link = $a->url . $a->anexo;
                                    }

                                    if ($cont == $flag) {
                                        echo '<div style="margin-left: 0" class="span3"><a href="#modal-anexo" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'"  imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                        $flag += 4;
                                    } else {
                                        echo '<div class="span3"><a href="#modal-anexo" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                    }
                                    $cont++;
                                } ?>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab6">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <div id="div_nfe"> </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab7">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <div id="div_nfse"></div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>

        </div>
    </div>
</div>

<!-- Modal searchCliente -->
<div id="programarManutencaoVeiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Agendar Manutenções</h3>
    </div>

    <div class="modal-body">
        <iframe id="iFrame_programarManutencaoVeiculo" frameborder="0" width="100%" height="800"></iframe>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
    </div>
</div>

<!-- Modal searchCliente -->
<div id="searchCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
     <div id="div_searchCliente"></div>
</div>

<!-- Modal cadastrar novo veiculo do cliente -->
<div id="adicionarVeiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_cadVeiculoModal"></div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo" style="text-align: center">
            <div class='progress progress-info progress-striped active'>
                <div class='bar' style='width: 100%'></div>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">LINK</label>
            <input type="text" id="link-visualizar" readonly name="link-visualizar"
                   class="span12"/>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="localfoto-visualizar">Local da Foto</label>
            <input type="text"  class="span12" id="localfoto-visualizar" readonly name="localfoto-visualizar">
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">Observação</label>
            <textarea class="span12" rows="5" readonly cols="5" id="observacaoimg-visualizar" name="observacaoimg-visualizar"></textarea>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="data-visualizar">Data</label>
            <input type="date" id="data-visualizar" readonly name="data-visualizar"
                   class="span4"/>

            <label for="hora-visualizar">Hora</label>
            <input type="time"  id="hora-visualizar"  readonly name="hora-visualizar"
                   class="span4"/>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
        <a href="#modal-email-foto" id="btn-email" onclick="$('#fechar_modal_foto').click();" role="button"
           data-toggle="modal" class="btn btn-success"><i class="icon-envelope"></i>
            Enviar-Email</a>
        <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
        <a href="" target="_blank" class="btn btn-invers" id="abrir-imagem">Abrir</a>
        <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
    </div>
</div>

<!-- Modal Faturar-->
<div id="modal-faturar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form id="formFaturar" action="<?php echo current_url() ?>" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Faturar Venda</h3>
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>
            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <input class="span12" id="descricao" type="text" name="descricao"
                       value="Fatura de Venda - #<?php echo $result->idOs; ?> "/>

            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0">
                    <label for="cliente">Cliente*</label>
                    <input class="span12" id="cliente" type="text" name="cliente"
                           value="<?php echo $result->nomeCliente ?>"/>
                    <input type="hidden" name="clientes_id"  value="<?php echo $result->clientes_id ?>">
                    <input type="hidden" name="os_id" id="os_id" value="<?php echo $result->idOs; ?>">
                </div>
            </div>
            <div class="span12" style="margin-left: 0">

                <div class="span6" style="margin-left: 0">
                    <label for="custoFatura">Custo*</label>
                    <input class="span12 money" id="custoFatura" type="text" name="custoFatura"
                           value="<?php echo $totalCusto; ?> "/>
                </div>

                <div class="span6">
                    <label for="valor">Valor*</label>
                    <input type="hidden" id="tipo" name="tipo" value="receita"/>
                    <input class="span12 money" id="valor" type="text" name="valor"
                           value="<?php echo number_format($total, 2); ?> "/>
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span6">
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12 datepicker" id="vencimento" type="text" value="<?php echo date('d/m/y'); ?>"
                           name="vencimento"/>
                </div>

                <div class="span6">
                    <label for="previsaoPagamento">Data Previsão de Pagamento</label>
                    <input class="span12 datepicker" id="previsaoPagamento" type="text" name="previsaoPagamento"/>
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="recebido">Recebido?</label>
                    &nbsp &nbsp &nbsp &nbsp <input id="recebido" type="checkbox" name="recebido" value="1"/>
                </div>
                <div id="divRecebimento" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="recebimento">Data Recebimento</label>
                        <input class="span12 datepicker" id="recebimento" type="text" name="recebimento"/>
                    </div>
                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto" id="formaPgto" class="span12">
                            <?php foreach ($formasPagamento as $formaPagamento) {?>
                                <option value="<?php echo $formaPagamento->nome;?>"><?php echo $formaPagamento->nome;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
                <button class="btn btn-primary">Faturar</button>
            </div>
        </div>
    </form>
</div>

<!-- Modal enviar email foto cliente -->
<div id="modal-email-foto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmailFoto" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Enviar E-mail de Orçamento para o cliente</h3>
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Para*</label>
                <input class="span12" type="text" name="para" required="required" value="<?php echo $result->emailCliente; ?>"/>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Assunto*</label>
                <input class="span12"  type="text" name="assunto" required="required" value="<?php echo 'Acompanhamento da OS Nº ' . str_pad($result->idOs, 6, '0', STR_PAD_LEFT) ?>"/>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Mensagem*</label>
                <textarea class="span12 redactor" rows="10" cols="10" id="mensagem-foto" name="mensagem"></textarea>
            </div>

            <div class="modal-footer">
                <input type="hidden" name="idOs" value="<?php echo $result->idOs; ?>">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
                <button class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>
</div>

<!-- Modal enviar email -->
<div id="modal-email" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmail" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Enviar E-mail de Orçamento para o cliente</h3>
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Para*</label>
                <input class="span12" type="text" name="para" required="required" value="<?php echo $result->emailCliente; ?>"/>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Assunto*</label>
                <input class="span12"  type="text" name="assunto" required="required" value="<?php echo 'Orçamento da OS Nº ' . str_pad($result->idOs, 6, '0', STR_PAD_LEFT) ?>"/>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Mensagem*</label>
                <textarea class="span12 redactor" rows="10" cols="10" id="mensagem" name="mensagem"></textarea>
            </div>

            <div class="modal-footer">
                <input type="hidden" name="idOs" value="<?php echo $result->idOs; ?>">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
                <button class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>
</div>

<!-- Modal cadastrar novo cliente -->
<div id="adicionarCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Cadastrar / Editar o cliente</h3>
        </div>

        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span4">
                    <label for="tipoPessoa">Tipo de pessoa</label>
                    <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                        <option value="PF">Pessoa Física</option>
                        <option value="PJ">Pessoa Jurídica</option>
                    </select>
                </div>

                <div class="span4">
                    <label for="origem">Origem</label>
                    <select name="origem" id="origem" class="span12 chzn" required="required">
                        <option value="">selecione uma origem</option>
                        <option value="Particular">Particular</option>
                        <option value="Financeira">Financeira</option>
                        <option value="Seguradora">Seguradora</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span6">
                    <label for="nomeCliente" >Nome / Razão Social<span class="required">*</span></label>
                    <input id="nomeCliente"  class="span12" type="text" required="required" name="nomeCliente"
                           value="<?php echo set_value('nomeCliente'); ?>"/>

                    <input type="hidden" id="cliente_id" name="cliente_id" value="" />
                </div>

                <div class="span6">
                    <label for="nomeFantasiaApelido" >Nome Fantasia / Apelido</label>
                    <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                           value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                </div>


            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span8">
                    <label for="documento">CPF/CNPJ*</label>
                    <input id="documento" type="text" class="span12" required="required" name="documento"
                           value="<?php echo set_value('documento'); ?>"/>
                </div>

                <div class="span4" id="div_sexo">
                    <label for="sexo">Sexo</label>
                    <select name="sexo" id="sexo" class="span12 chzn" required="required">
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0" id="div_rg">
                <div class="span3">
                    <label for="rg">RG</label>
                    <input id="rg" type="text" class="span12" name="rg"
                           value="<?php echo set_value('rg'); ?>"/>
                    </select>
                </div>

                <div class="span3">
                    <label for="orgaoEmissor">Orgão Emissor</label>
                    <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                           value="<?php echo set_value('orgaoEmissor'); ?>"/>
                </div>

                <div class="span2">
                    <label for="estadoOrgaoEmissor">Estado</label>
                    <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                           value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                </div>

                <div class="span4">
                    <label for="dataOrgaoEmissor">Data Emissão</label>
                    <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                           value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="telefone">Telefone</label>
                    <input id="telefone" type="text" class="span12" name="telefone"
                           value="<?php echo set_value('telefone'); ?>"/>
                </div>

                <div class="span6">
                    <label for="celular">Celular</label>
                    <input id="celular" type="text" class="span12" name="celular"
                           value="<?php echo set_value('celular'); ?>"/>
                </div>



            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="span12" name="email"
                           value="<?php echo set_value('email'); ?>"/>
                </div>

                <div class="span6">
                    <label for="email">Data de nascimento</label>
                    <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                           value="<?php echo set_value('data_nascimento'); ?>"/>
                </div>
            </div>


            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Dados do endereço
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span3">
                    <label for="cep">CEP</label>
                    <input id="cep" type="text" name="cep" class="span12" onBlur="getConsultaCEP();"
                           value="<?php echo set_value('cep'); ?>"/>
                    <small>[TAB] consulta cep (Necessita Internet)</small>
                </div>

                <div class="span6">
                    <label for="rua">Rua</label>
                    <input id="rua" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                </div>

                <div class="span3">
                    <label for="numero">Número</label>
                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <label for="complemento">Complemento</label>
                    <input id="complemento" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span5">
                    <label for="bairro">Bairro</label>
                    <input id="bairro" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                </div>

                <div class="span4">
                    <label for="cidade">Cidade</label>
                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                </div>
                <div class="span3">
                    <label for="estado">Estado</label>
                    <input id="estado" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                </div>
            </div>

            <div id="div_contato" style="display: none;">
                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes">
                        <a href="#tab1" data-toggle="tab">
                            Dados do  Contato
                        </a>
                    </li>
                </ul>
                <div class="span12" style="padding: 1%; margin-left: 0">

                    <div class="span12">
                        <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                        <input id="contatoNomeCliente"  class="span12" type="text" name="contatoNomeCliente"
                               value="<?php echo set_value('contatoNomeCliente'); ?>"/>
                    </div>

                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span8">
                        <label for="contatoCpf">CPF</label>
                        <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                               value="<?php echo set_value('contatoCpf'); ?>"/>
                    </div>

                    <div class="span4">
                        <label for="contatoSexo">Sexo</label>
                        <select name="contatoSexo" id="contatoSexo" class="span12 chzn">
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                        </select>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="contatoTelefone">Telefone</label>
                        <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                               value="<?php echo set_value('contatoTelefone'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="contatoCelular">Celular</label>
                        <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                               value="<?php echo set_value('contatoCelular'); ?>"/>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="email">Email</label>
                        <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                               value="<?php echo set_value('contatoEmail'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="email">Data de nascimento</label>
                        <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                               value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Senha para acessar o portal do cliente
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <label for="senha">Senha</label>
                <input type="password" name="senha" class="span12" id="senha"
                       value=""/>
                <small>Deixe em branco e a senha padrão será o (cpj/cnpj) do cliente</small>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Observação
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <textarea class="span12" id="observacao"  name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6 offset3" style="text-align: center">
                    <button type="submit" class="btn btn-success">
                        <div id="div_buttom_adicionar">Editar</div>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- Modal consulta de estoque -->
<div id="modal-consulta-estoque" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmail" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Consulta de Estoque</h3>
        </div>
        <div class="modal-body">

            <div class="span12" style="padding: 1%; margin-left: 0;text-align: center;">
                <label for="senha"><div id="consultaEstoqueNomeProduto"></div></label>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <table class="table table-bordered" id="tblConsultaEstoque">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="text-align: left;">Filial</th>
                        <th style="width: 10%;text-align: right;">Estoque</th>
                        <!--
                        <th style="width: 10%;text-align: right;">Reservado</th>
                        <th style="width: 10%;text-align: right;">Bloqueado</th>
                        <th style="width: 10%;text-align: right;">Disponível</th>
                        !-->
                        <th style="width: 10%;text-align: right;">Venda(R$)</th>
                        <th style="width: 20%;text-align: center;">Localização</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr style="backgroud-color: #2D335B">
                        <th colspan="10"></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/veiculo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cadVeiculoModal.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $(".money").maskMoney();

        $('.redactor').redactor({
            minHeight: 200
        });

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });

        $('#tabDetalhes').click(function (event) {
            location.reload();
        });

        $('#clientes_id').change(function (e) {
            var clientes_id = $(this).val();
            if (clientes_id) {
                $('#pincliente').attr('class', 'icon-edit icon-white');
                $('#div_buttom_adicionar').html("Editar");

                $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                $('#aAdicionarVeiculo').removeAttr('disabled');
            } else {
                $('#cliente_id').val('');
                $('#tipoPessoa').val('PF');
                $('#origem').val('');
                $('#nomeCliente').val('');
                $('#sexo').val('M');
                $('#nomeFantasiaApelido').val('');
                $('#documento').val('');
                $('#rg').val('');
                $('#orgaoEmissor').val('');
                $('#estadoOrgaoEmissor').val('');
                $('#dataOrgaoEmissor').val('');
                $('#data_nascimento').val('');
                $('#telefone').val('');
                $('#celular').val('');
                $('#email').val('');
                $('#rua').val('');
                $('#numero').val('');
                $('#bairro').val('');
                $('#cidade').val('');
                $('#estado').val('');
                $('#cep').val('');
                $('#complemento').val('');
                $('#observacao').val('');
                $('#contatoNomeCliente').val('');
                $('#contatoSexo').val('');
                $('#contatoCpf').val('');
                $('#contatoEmail').val('');
                $('#contatoDataNascimento').val('');
                $('#contatoTelefone').val('');
                $('#contatoCelular').val('');
                $('#dataCadastro').val('');
                $('#senha').val('');

                $('#pincliente').attr('class', 'icon-plus-sign icon-white');
                $('#div_buttom_adicionar').html("<i class=\"icon-plus icon-white\"></i> Adicionar");

                $('#aAdicionarVeiculo').attr('disabled', true);
                $('#aAdicionarVeiculo').prop("href", '#');
            }

            limparCamposVeiculo();
            consultaVeiculoClientePreencher($('#clientes_id').val(),'<?php echo base_url();?>','<?php echo $result->veiculo_id;?>');
        });

        $('#aAdicionarVeiculo').click(function (e) {
            event.preventDefault();
            $('#div_cadVeiculoModal').load("<?php echo base_url();?>index.php/clientes/cadVeiculoModal",
                function(responseTxt, statusTxt, xhr){
                    if (statusTxt == "success") {
                        buscarVeiculo($('#clientes_id').val(),'<?php echo base_url();?>');
                    }
                }
            );
        });

        $('#addConsultaProduto').click(function (event) {
            event.preventDefault();
            var table = $('#tblConsultaEstoque');
            var $tbody = table.append('<tbody />').children('tbody');
            $tbody.empty();
            $('#consultaEstoqueNomeProduto').html('');
            var produto = $('#produto').val();

            if (produto) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/getProdutoFiliais",
                    data: "produto=" + produto,
                    dataType: 'json',
                    success: function (produtofiliais) {
                        $.each(produtofiliais, function(i, produtofilial) {

                            var table = $('#tblConsultaEstoque');
                            var $tbody = table.append('<tbody />').children('tbody');
                            $('#consultaEstoqueNomeProduto').html(produtofilial.produto);

                            $tbody.append('<tr />').children('tr:last')
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")' style='text-align: left;cursor: pointer;'>" + produtofilial.filial + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.estoque + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.reservado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.bloqueado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'><b>" + (produtofilial.estoque -  produtofilial.reservado - produtofilial.bloqueado) + ' ' + produtofilial.unidade + "</b></td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>R$" + produtofilial.precoVenda + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: center;cursor: pointer;'>" + produtofilial.localizacao + " </td>");
                        });
                    }
                });
            } else {
                $('#consultaEstoqueNomeProduto').html('Selecione um produto para consulta estoque');
            }
        });

        $('#aSearchCliente').click(function (event) {
            $('#div_searchCliente').load("<?php echo base_url();?>index.php/clientes/search");
        });

        $('#addCliente').click(function (event) {
            event.preventDefault();
            var cliente_id = $('#clientes_id').val();

            if (cliente_id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/consultaCliente",
                    data: "cliente_id=" + cliente_id,
                    dataType: 'json',
                    success: function (cliente) {

                        $('#cliente_id').val(cliente_id);
                        $('#tipoPessoa').val(cliente.tipoPessoa);
                        $('#origem').val(cliente.origem);
                        $('#nomeCliente').val(cliente.nomeCliente);
                        $('#sexo').val(cliente.sexo);
                        $('#nomeFantasiaApelido').val(cliente.nomeFantasiaApelido);
                        $('#documento').val(cliente.documento);
                        $('#rg').val(cliente.rg);
                        $('#orgaoEmissor').val(cliente.orgaoEmissor);
                        $('#estadoOrgaoEmissor').val(cliente.estadoOrgaoEmissor);
                        $('#dataOrgaoEmissor').val(cliente.dataOrgaoEmissor);
                        $('#data_nascimento').val(cliente.data_nascimento);
                        $('#telefone').val(cliente.telefone);
                        $('#celular').val(cliente.celular);
                        $('#email').val(cliente.email);
                        $('#rua').val(cliente.rua);
                        $('#numero').val(cliente.numero);
                        $('#bairro').val(cliente.bairro);
                        $('#cidade').val(cliente.cidade);
                        $('#estado').val(cliente.estado);
                        $('#cep').val(cliente.cep);
                        $('#complemento').val(cliente.complemento);
                        $('#observacao').val(cliente.observacao);
                        $('#contatoNomeCliente').val(cliente.contatoNomeCliente);
                        $('#contatoSexo').val(cliente.contatoSexo);
                        $('#contatoCpf').val(cliente.contatoCpf);
                        $('#contatoEmail').val(cliente.contatoEmail);
                        $('#contatoDataNascimento').val(cliente.contatoDataNascimento);
                        $('#contatoTelefone').val(cliente.contatoTelefone);
                        $('#contatoCelular').val(cliente.contatoCelular);
                        $('#dataCadastro').val(cliente.dataCadastro);
                        $('#senha').val(cliente.senha);
                    }
                });
            }
        });

        $( "#formCliente" ).submit(function() {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/clientes/adicionarAjax",
                data: $("#formCliente").serialize(), // serializes the form's elements.
                success: function(cliente)
                {
                    if (cliente) {
                        cliente = JSON.parse(cliente);

                        $('#clientes_id option[value='+cliente.idClientes+']').remove();

                        $('#clientes_id').append('<option value="'+cliente.idClientes+'">' + cliente.nomeCliente + '</option>');
                        $('#clientes_id').val(cliente.idClientes);
                        $('#adicionarCliente').modal('hide');
                        $('#pincliente').attr('class', 'icon-edit icon-white');
                        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                        $('#aAdicionarVeiculo').removeAttr('disabled');
                    }
                }
            });
        });

        function calcularTotais() {

            valor = $('#total-venda').val();
            total_servico = $('#total-servico').val();

            //formatando
            valorTotalProdutoCusto = $('#total-custo-venda').val();
            valorTotalProdutoCusto = valorTotalProdutoCusto.replace(',', '');
            valorTotalProdutoCusto = parseFloat(valorTotalProdutoCusto);

            //formatando
            valorTotalServicoCusto = $('#total-custo-servico').val();
            valorTotalServicoCusto = valorTotalServicoCusto.replace(',', '');
            valorTotalServicoCusto = parseFloat(valorTotalServicoCusto);

            //formatando
            valor = valor.replace(',', '');
            valor = parseFloat(valor);

            //formatando
            total_servico = total_servico.replace(',', '');
            total_servico = parseFloat(total_servico);

            //passando informacoes
            var totalVenda = valor + total_servico;
            var totalCusto = valorTotalProdutoCusto + valorTotalServicoCusto
            $('#valor').val(totalVenda);
            $('#custoFatura').val(totalCusto);

            //passando as informações para a tela principal da os
            $('#totalCusto').val(totalCusto);
            $('#totalVenda').val(totalVenda);
            $('#totalLucro').val(totalVenda - totalCusto);
        }

        $('#recebido').click(function (event) {
            var flag = $(this).is(':checked');
            if (flag == true) {
                $('#divRecebimento').show();
            }
            else {
                $('#divRecebimento').hide();
            }
        });

        $(document).on('click', '#btn-faturar', function (event) {

            event.preventDefault();
            valor = $('#total-venda').val();
            total_servico = $('#total-servico').val();

            //formatando
            valorTotalProdutoCusto = $('#total-custo-venda').val();
            valorTotalProdutoCusto = valorTotalProdutoCusto.replace(',', '');
            valorTotalProdutoCusto = parseFloat(valorTotalProdutoCusto);

            //formatando
            valorTotalServicoCusto = $('#total-custo-servico').val();
            valorTotalServicoCusto = valorTotalServicoCusto.replace(',', '');
            valorTotalServicoCusto = parseFloat(valorTotalServicoCusto);

            //formatando
            valor = valor.replace(',', '');
            valor = parseFloat(valor);

            //formatando
            total_servico = total_servico.replace(',', '');
            total_servico = parseFloat(total_servico);

            //passando informacoes
            var totalVenda = valor + total_servico;
            var totalCusto = valorTotalProdutoCusto + valorTotalServicoCusto

            $('#valor').val(totalVenda);
            $('#custoFatura').val(totalCusto);

            //passando as informações para a tela principal da os
            $('#totalCusto').val(totalCusto);
            $('#totalVenda').val(totalVenda);
            $('#totalLucro').val(totalVenda - totalCusto);
        });

        $("#formFaturar").validate({
            rules: {
                descricao: {required: true},
                cliente: {required: true},
                valor: {required: true},
                vencimento: {required: true}
            },
            messages: {
                descricao: {required: 'Campo Requerido.'},
                cliente: {required: 'Campo Requerido.'},
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $('#btn-cancelar-faturar').trigger('click');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/faturar",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {

                            window.location.reload(true);
                        }
                        else {
                            alert('Ocorreu um erro ao tentar faturar OS.');
                            $('#progress-fatura').hide();
                        }
                    }
                });
                return false;
            }
        });

        $('#produto').change(function () {
            $.ajax({
                type: "get", async: false,
                url: "<?php echo base_url(); ?>index.php/os/autoCompleteProdutoById/"+ $(this).val(),
                dataType: "json",
                success: function (data) {
                    if (data.results != null) {
                        var item = data.results[0];
                        $("#idProduto").val(item.id);

                        $("#estoque").val(item.estoque);
                        $("#preco").val(item.preco);
                        $("#precoVendaOS").val(item.preco);
                        $("#precoCusto").val(item.precoCompra);
                        $("#quantidade").focus();
                    } else {
                        $("#idProduto").val($('#produto').val());
                        $("#estoque").val('');
                        $("#preco").val('');
                        $("#precoVendaOS").val('');
                        $("#precoCusto").val('');
                        $("#quantidade").focus();
                    }
                }
            });
        });

        $('#servico').change(function (e) {
            $.ajax({
                type: "get", async: false,
                url: "<?php echo base_url(); ?>index.php/os/autoCompleteServicoById/"+$(this).val(),
                dataType: "json",
                success: function (data) {
                    var item = data.results[0];
                    $("#idServico").val(item.id);
                    $("#precoVendaServico").val(item.preco);
                    $('#idSetorServico').val(item.idSetorServico);
                }
            });
        });

        $("#formOs").validate({
            rules: {
                cliente: {required: true},
                tecnico: {required: true},
                dataInicial: {required: true}
            },
            messages: {
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataInicial: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formProdutos").validate({
            rules: {
                quantidade: {required: true}
            },
            messages: {
                quantidade: {required: 'Insira a quantidade'}
            },
            submitHandler: function (form) {

                var idSetorProduto  = parseInt($("#idSetorProduto").val());

                //if (estoque < quantidade) {
                    //alert('Você não possui estoque suficiente.');
               // }
                //else {
                    var dados = $(form).serialize();

                    $("#divProdutos_"+idSetorProduto).html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/os/adicionarProduto",
                        data: dados,
                        dataType: 'json',
                        success: function (data) {
                            if (data.result == true) {

                                <?php foreach($setores as $setor) {?>
                                    $("#divProdutos_<?php echo $setor->idSetor; ?>").load("<?php echo current_url();?> #divProdutos_<?php echo $setor->idSetor; ?>", function () {
                                        calcularTotais();
                                    });
                                <?php } ?>

                                $("#quantidade").val(1);
                                $("#precoCusto").val('');
                                $("#precoVendaOS").val('');
                                $("#idSetorProduto").val('');
                                $("#comissaoProduto").val('');

                                $("#produto").select2('data', {
                                    id: null,
                                    text: ''
                                });

                                $("#produto").val('').focus();
                            }
                            else {
                                alert('Ocorreu um erro ao tentar adicionar produto.');
                            }
                        }
                    });
                    return false;
                //}
            }
        });

        $("#formServicos").validate({
            rules: {
                servico: {required: true}
            },
            messages: {
                servico: {required: 'Insira um serviço'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                var idSetorServico  = parseInt($("#idSetorServico").val());

                $("#divServicos_"+idSetorServico).html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/adicionarServico",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {

                            <?php foreach($setores as $setor) {?>
                                $("#divServicos_<?php echo $setor->idSetor; ?>").load("<?php echo current_url();?> #divServicos_<?php echo $setor->idSetor; ?>", function () {
                                    calcularTotais();
                                });
                            <?php } ?>
                            $("#servico").val('');
                            $("#precoVendaServico").val('');
                            $('#comissaoServico').val('');

                            $("#servico").select2('data', {
                                id: null,
                                text: ''
                            });

                            $("#precoCustoServico").val('').focus();
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar serviço.');
                        }
                    }
                });
                return false;
            }

        });

        $("#formAnexos").validate({
            submitHandler: function (form) {
                //var dados = $( form ).serialize();
                var dados = new FormData(form);
                $("#form-anexos").hide('1000');
                $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/anexar",
                    data: dados,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divAnexos").load("<?php echo current_url();?> #divAnexos");
                            $("#userfile").val('');
                            $('#observacaoimg').val('');
                            $('#localfoto').val('');
                        }
                        else {
                            $("#divAnexos").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> ' + data.mensagem + '</div>');
                        }
                    },
                    error: function () {
                        $("#divAnexos").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você anexou o(s) arquivo(s).</div>');
                    }
                });
                $("#form-anexos").show('1000');
                return false;
            }

        });

        $(document).on('click', 'a', function (event) {

            var idProduto   = $(this).attr('idAcao');
            var quantidade  = $(this).attr('quantAcao');
            var produto     = $(this).attr('prodAcao');
            var setor_id    = $(this).attr('setor_id');
            var idOsProduto = $('#idOsProduto').val();

            if ((idProduto % 1) == 0) {
                $("#divProdutos_"+setor_id).html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/excluirProduto",
                    data: "idProduto=" + idProduto + "&quantidade=" + quantidade + "&produto=" + produto +"&idOsProduto="+idOsProduto,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divProdutos_"+setor_id).load("<?php echo current_url();?> #divProdutos_"+setor_id, function () {
                                calcularTotais();
                            });
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir produto.');
                        }
                    }
                });
                return false;
            }

        });

        $(document).on('click', 'span', function (event) {
            var idServico   = $(this).attr('idAcao');
            var setor_id    = $(this).attr('setor_id');

            if ((idServico % 1) == 0) {
                $("#divServicos_"+setor_id).html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/os/excluirServico",
                    data: "idServico=" + idServico,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divServicos_"+setor_id).load("<?php echo current_url();?> #divServicos_"+setor_id, function () {
                                calcularTotais();
                            });
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir serviço.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', '.anexo', function (event) {
            event.preventDefault();

            var link = $(this).attr('link');
            var id = $(this).attr('imagem');
            var url = '<?php echo base_url(); ?>os/excluirAnexo/';
            var observacaoimg =  $(this).attr('observacaoimg');
            var data =  $(this).attr('data');
            var hora =  $(this).attr('hora');
            var localfoto = $(this).attr('localfoto');

            $("#div-visualizar-anexo").html('<img src="' + link + '" alt="">');
            $("#excluir-anexo").attr('link', url + id);

            $('#link-visualizar').val(link);
            $('#observacaoimg-visualizar').val(observacaoimg);
            $('#data-visualizar').val(data);
            $('#hora-visualizar').val(hora);
            $('#localfoto-visualizar').val(localfoto);

            $('#mensagem-foto').redactor('set',
                '<p>' +
                '<br/> <b>' + localfoto + '</b>' +
                '<br/>' +
                '<br/>' +
                '<img src="'+link+'" style="background-color: initial;">' +
                '<br/>' + observacaoimg +
                '</p>');

            $('#abrir-imagem').attr('href',link);
            $("#download").attr('href', "<?php echo base_url(); ?>index.php/os/downloadanexo/" + id);

        });

        $(document).on('click', '#excluir-anexo', function (event) {
            event.preventDefault();
            var link = $(this).attr('link');
            $('#modal-anexo').modal('hide');
            $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

            $.ajax({
                type: "POST",
                url: link,
                dataType: 'json',
                success: function (data) {
                    if (data.result == true) {
                        $("#divAnexos").load("<?php echo current_url();?> #divAnexos");
                    }
                    else {
                        alert(data.mensagem);
                    }
                }
            });
        });

        $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});

        $('#tabNF').click(function (event) {
            $('#div_nfe').load('<?php echo base_url();?>/index.php/nfe/iframeNFEOS/<?php echo $result->idOs;?>',function () {
                $('#adicionarNF').hide();
            });
        });

        $('#tabNFS').click(function (event) {
            $('#div_nfse').load('<?php echo base_url();?>/index.php/nfse/iframeNFSOS/<?php echo $result->idOs;?>', function () {
                $('#adicionarNFSE').hide();
            });
        });

        $('#btn-manutencao').click(function (event) {
            var veiculo_id = $('#veiculo_id').val();
            var clientes_id = $('#clientes_id').val();

            if (veiculo_id !== '') {
                $("#iFrame_programarManutencaoVeiculo").attr("src", '<?php echo base_url();?>index.php/veiculos/iFrameVeiculo/'+clientes_id+'/'+veiculo_id);
            } else {
                return;
            }
        });
    });

    function preencherEstoquePorFilial(idProdutoFilial) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/produtos/getUnicProdutoFilial",
            data: "idProdutoFilial=" + idProdutoFilial,
            dataType: 'json',
            success: function (produtofiliais) {
                $("#idProduto").val(produtofiliais.produto_id);

                $("#estoque").val(produtofiliais.estoque);
                $("#preco").val(produtofiliais.precoVenda);
                $("#precoVendaOS").val(produtofiliais.precoVenda);
                $("#precoCusto").val(produtofiliais.precoCompra);
                $("#filial_customizada").val(produtofiliais.filial_id);
                $("#quantidade").focus();

                $("#modal-consulta-estoque .close").click()

            }
        });
    }

    var contador = 2;

    function getPosicaoElemento(offsetTrail, event){ // onde elemID é o id do objeto que quero detectar a posicao no meu caso a imagem.
        if( contador > 8) {
            contador = 2
        }
         offsetTrail.src = '<?php echo base_url().'/img/veiculo/'; ?>/'+contador+'.png';
        contador = contador + 1;
    }

    function exibir(id) {
        var isVisible = $( '#divavaria_' +id ).is( ":visible" );

        if (isVisible) {
            $( '#divavaria_' +id ).hide();
        } else {
            $( '#divavaria_' +id ).show();
        }
    }

    function fecharModelSercheCliente(cliente) {
        $('#searchCliente').modal('hide');

        $('#pincliente').attr('class', 'icon-edit icon-white');
        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
        $('#aAdicionarVeiculo').removeAttr('disabled');
        consultaVeiculoClientePreencher($('#clientes_id').val(),'<?php echo base_url();?>','<?php echo $result->veiculo_id;?>');

        if(cliente.inativo == 1){
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ea1f1f 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#ee5f5b00');
            $('#div_cliente_restricao').show();
            $('#div_cliente_restricao').html('Cliente com restrições: '+cliente.observacaoInativo);
            $('#btnContinuar').hide();
            $('#btn-faturar').hide();
        } else {
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ededed 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#fff');
            $('#div_cliente_restricao').hide();
            $('#btnContinuar').show();
            $('#btn-faturar').show();
        }
    }
</script>




