<?php $totalServico = 0;
$totalProdutos = 0; ?>
<?php
/*
SystemBoys - Garotos de Sistema
http://www.systemboys.com.br
Marcos Aurélio R. Silva
systemboy_marcos@hotmail.com
*/

function convert_number_to_words($valor = 0, $maiusculas = false)
{
    // verifica se tem virgula decimal
    if (strpos($valor, ",") > 0) {
        // retira o ponto de milhar, se tiver
        $valor = str_replace(".", "", $valor);

        // troca a virgula decimal por ponto decimal
        $valor = str_replace(",", ".", $valor);
    }
    $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
    $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões",
        "quatrilhões");

    $c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
        "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
    $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
        "sessenta", "setenta", "oitenta", "noventa");
    $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
        "dezesseis", "dezesete", "dezoito", "dezenove");
    $u = array("", "um", "dois", "três", "quatro", "cinco", "seis",
        "sete", "oito", "nove");

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    $cont = count($inteiro);
    for ($i = 0; $i < $cont; $i++)
        for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
            $inteiro[$i] = "0" . $inteiro[$i];

    $fim = $cont - ($inteiro[$cont - 1] > 0 ? 1 : 2);
    $rt = '';
    for ($i = 0; $i < $cont; $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                $ru) ? " e " : "") . $ru;
        $t = $cont - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000"

        ) $z++; elseif ($z > 0)
            $z--;
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= (($z > 1) ? " de " : "") . $plural[$t];
        if ($r)
            $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                    ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    if (!$maiusculas) {
        return ($rt ? $rt : "zero");
    } elseif ($maiusculas == "2") {
        return (strtoupper($rt) ? strtoupper($rt) : "Zero");
    } else {
        return (ucwords($rt) ? ucwords($rt) : "Zero");
    }
}


$firma = $emitente->nome;
$cnpj_cgc_firma = $emitente->cnpj;
$logomarca = $emitente->url_logo;

$nome_devedor = $result->nomeCliente;
$cpf_cnpj_devedor = $result->documento;
$endereco = $result->rua . ' ' . $result->numero . ',' . $result->bairro . ' - ' . $result->cidade . '/' . $result->estado;

$referente = 'referente a manuten&#231;&#227;o, conserto e (ou) venda de pe&#231;as';
$final = 'estando quitado o d&#233;bito referido a esta data';
$dia_pagamento = date("d");
$mes_pagamento = date("m");
$ano_pagamento = date("y");
$cidade = $emitente->cidade;
$estado = $emitente->uf;
?>

<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/bootstrap.min.css'/>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css'/>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/matrix-style.css'/>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/matrix-media.css'/>

<style type="text/css">
    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 11px;
        color: #666;
        background: #ffffff;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .logo {
        width: 20%;
    }

    .pontilhado {
        border-left-style: dotted;
        border-width: 1px;
        border-color: 660033;
        background-color: cc3366;
        font-family: verdana, arial;
        font-size: 10pt;
    }
</style>

<div>

    <!--############################!-->
    <!--#### VIA DO CLIENTE ########!-->
    <!--############################!-->
    <div class="span7" style="float: left;">

        <div class="invoice-content">
            <table class="table" style="margin-bottom: 0">
                <tbody>
                <?php if ($emitente == null) { ?>
                    <tr>
                        <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a
                                    href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<
                        </td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td style="width: 20%"><img src=" <?php echo $emitente->url_logo; ?> " class="logo"></td>
                        <td style="width: 2%"></td>
                        <td>
                            <span style="font-size: 20px;font-weight: bold;"><?php echo $emitente->nome; ?></span> </br>
                            <p><?php echo $emitente->cnpj; ?></p>
                            <p><?php echo $emitente->rua . ', nº:' . $emitente->numero . ', ' . $emitente->bairro . ' - ' . $emitente->cidade . ' - ' . $emitente->uf; ?> </p> </br>
                            <p><?php echo $emitente->email . ' - Fone: ' . $emitente->telefone; ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <span style="text-transform: uppercase;font-weight: bold;font-size: 16px">Comprovante de Entrega - OS nº <?php echo $result->idOs ?></span>
                            <br/>Hora: <?php echo date('h:i') ?> Data: <?php echo date('d/m/Y') ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td style="width: 40%; padding-left: 0">
                        <ul>
                            <li>
                                <span>Cliente.: <?php echo $result->nomeCliente ?></span><br/>
                                <span>Tel.: <?php echo $result->telefone_cliente . '/' . $result->celular_cliente ?></span><br/>
                                <span>Endereço.: <?php echo $result->rua ?>, <?php echo $result->numero ?>
                                    , <?php echo $result->bairro ?></span>
                                <span><?php echo $result->cidade ?>
                                    - <?php echo $result->estado ?></span>
                            </li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        Equipamento: <?php echo $result->aparelho ?> <br/>
                        Acessórios.: <?php echo $result->acessorios ?>
                    </td>
                    <td align="justify">
                        Modelo: <?php echo $result->modelo ?><br/>
                        Marca: <?php echo $result->marca ?> <br/>
                        Série: <?php echo $result->numero_serie ?>
                    </td>
                </tr>
                </tbody>
            </table>

            <br/>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td><h5><span style="font-weight: bold;text-transform: uppercase;">Problema Informado:</span></h5>
                        <?php echo $result->defeito ?>
                    </td>
                </tr>
                </tbody>
            </table>

            <br/>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td style="text-align: justify">
                        <h5><span style="text-transform: uppercase;font-weight: bold;">Condições de Serviço</span></h5>
                        <p>
                            1 - A Empresa da garantia de 90 dias para mão de obra e peças usadas no conserto, contados a
                            partir da date de entrega.
                        </p>
                        <p>
                            2 - Os Aparelhos não retirados no prazo máximo de 30 dias contados apartir da comunicação
                            para sua retirada sofrerão
                            acréscimo das despesas de armazenamento e seguro.
                        </p>
                        <p>
                            3 - O Aparelho só será devolvido mediante a apresentação desta, portanto guarde-a com
                            cuidado.
                        </p>
                        <p>
                            4 - Ao ligar para a Empresa Prestadora de Serviço, informe o número da Ordem de Serviço para
                            melhora atende-lo.
                        </p>
                        <p>
                            5 - Qualquer dúvida que venha a ter com relação ao serviço entre em contato!
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>

            <br/>
            <br/>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        Entrega/Entrada: <?php echo date('d/m/Y') ?><br/>
                        Hora: <?php echo date('h:i') ?><br/>
                        Situação da Ordem: <?php echo $result->status ?><br/>
                        Técnico Responsável: <?php echo $result->nome ?>
                    </td>
                    <td>
                        Visto: <?php echo $emitente->nome; ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        (X) Via do Cliente ( ) Via da Empresa
                    </td>
                    <td>
                        Visto: <?php echo $result->nomeCliente ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!--############################!-->
    <!--#### VIA DO CLIENTE ########!-->
    <!--############################!-->
    <div class="span7 pontilhado" style="float: left;">

        <div class="invoice-content">
            <table class="table" style="margin-bottom: 0">
                <tbody>
                <?php if ($emitente == null) { ?>
                    <tr>
                        <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a
                                    href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<
                        </td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td style="width: 20%"><img src=" <?php echo $emitente->url_logo; ?> " class="logo"></td>
                        <td style="width: 2%"></td>
                        <td>
                            <span style="font-size: 20px;font-weight: bold;"><?php echo $emitente->nome; ?></span> </br>
                            <p><?php echo $emitente->cnpj; ?></p>
                            <p><?php echo $emitente->rua . ', nº:' . $emitente->numero . ', ' . $emitente->bairro . ' - ' . $emitente->cidade . ' - ' . $emitente->uf; ?> </p> </br>
                            <p><?php echo $emitente->email . ' - Fone: ' . $emitente->telefone; ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <span style="text-transform: uppercase;font-weight: bold;font-size: 16px">Comprovante de Entrega Interno - OS nº <?php echo $result->idOs ?></span>
                            <br/>Hora: <?php echo date('h:i') ?> Data: <?php echo date('d/m/Y') ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td style="width: 40%; padding-left: 0">
                        <ul>
                            <li>
                                <span>Cliente.: <?php echo $result->nomeCliente ?></span><br/>
                                <span>Tel.: <?php echo $result->telefone_cliente . '/' . $result->celular_cliente ?></span><br/>
                                <span>Endereço.: <?php echo $result->rua ?>, <?php echo $result->numero ?>
                                    , <?php echo $result->bairro ?></span>
                                <span><?php echo $result->cidade ?>
                                    - <?php echo $result->estado ?></span>
                            </li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        Equipamento: <?php echo $result->aparelho ?> <br/>
                        Acessórios.: <?php echo $result->acessorios ?>
                    </td>
                    <td align="justify">
                        Modelo: <?php echo $result->modelo ?><br/>
                        Marca: <?php echo $result->marca ?> <br/>
                        Série: <?php echo $result->numero_serie ?>
                    </td>
                </tr>
                </tbody>
            </table>

            <br/>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td><h5><span style="font-weight: bold;text-transform: uppercase;">Problema Informado:</span></h5>
                        <?php echo $result->defeito ?>
                    </td>
                </tr>
                </tbody>
            </table>

            <br/>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td style="text-align: justify">
                        <h5><span style="text-transform: uppercase;font-weight: bold;">Condições de Serviço</span></h5>
                        <p>
                            1 - A Empresa da garantia de 90 dias para mão de obra e peças usadas no conserto, contados a
                            partir da date de entrega.
                        </p>
                        <p>
                            2 - Os Aparelhos não retirados no prazo máximo de 30 dias contados apartir da comunicação
                            para sua retirada sofrerão
                            acréscimo das despesas de armazenamento e seguro.
                        </p>
                        <p>
                            3 - O Aparelho só será devolvido mediante a apresentação desta, portanto guarde-a com
                            cuidado.
                        </p>
                        <p>
                            4 - Ao ligar para a Empresa Prestadora de Serviço, informe o número da Ordem de Serviço para
                            melhora atende-lo.
                        </p>
                        <p>
                            5 - Qualquer dúvida que venha a ter com relação ao serviço entre em contato!
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>

            <br/>
            <br/>

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        Entrega/Entrada: <?php echo date('d/m/Y') ?><br/>
                        Hora: <?php echo date('h:i') ?><br/>
                        Situação da Ordem: <?php echo $result->status ?><br/>
                        Técnico Responsável: <?php echo $result->nome ?>
                    </td>
                    <td>
                        Visto: <?php echo $emitente->nome; ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <br/>
            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        ( ) Via do Cliente (X) Via da Empresa
                    </td>
                    <td>
                        Visto: <?php echo $result->nomeCliente ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>