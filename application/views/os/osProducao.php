<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>


<?php
if (!$results) {
    ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>VEÍCULOS PARA PRODUÇÃO</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Veiculo</th>
                    <th>Ações</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="6">Nenhuma OS Cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>


    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>VEÍCULOS PARA PRODUÇÃO</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="text-align: left">Veículo</th>
                    <th style="width: 20%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {

                    $dados_cliente = $r->nomeCliente;
                    $veiculo_id = $r->veiculo;
                    $veiculoStr = '';

                    if ($veiculo_id) {
                        $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                        if (count($veiculo) > 0) {
                            $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                        }
                    }
                    echo '<tr>';
                    echo '<td>' . $dados_cliente. '<br/>'.$veiculoStr.'</td>';
                    echo '<td style="width: 20%;text-align: center;">';
                    echo '<a href="' . base_url() . 'index.php/os/visualizarSetor/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                    echo '<a href="' . base_url() . 'index.php/os/aceitar_setor/' . $r->idOs . '" style="margin-right: 1%" class="btn btn-success tip-top"><i class="icon-check icon-white"></i> Aceitar</a>';
                    echo '</td>';
                    echo '</tr>';
                } ?>
                <tr>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/os/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir OS</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idOs" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir esta OS?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#buscar").keypress(function (event) {
            if (event.which == 13) {
                window.location.href = "<?php echo base_url() ?>index.php/os/buscar/" + $(this).val();
            }
        });

        $(document).on('click', 'a', function (event) {
            var os = $(this).attr('os');
            $('#idOs').val(os);
        });

        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
    });

</script>