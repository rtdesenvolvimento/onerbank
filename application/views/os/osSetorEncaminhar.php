<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>


<link rel="stylesheet" href="<?php echo base_url(); ?>css/redactor.css"/>
<script type="text/javascript" src="<?php echo base_url() ?>js/redactor.min.js"></script>


<?php
if (!$result) {
    ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5 style="text-transform: uppercase"><?php echo $nome_setor;?> </h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Veiculo</th>
                    <th>Ações</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="6">Nenhuma OS Cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <?php
            $veiculo_id = $result->veiculo;
            $veiculoStr = '';
            if ($veiculo_id) {
                $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                if (count($veiculo) > 0) {
                    $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                }
            } ?>
            <span class="icon">
                <i class="icon-tags"></i>
             </span>
                <h5 style="text-transform: uppercase"><?php echo $nome_setor;?> <br/> <?php echo $veiculoStr; ?></h5>
            </div>
        <form id="formProduzir" action="<?php echo base_url() ?>index.php/os/produzir_pecas" method="post">
            <div class="widget-content nopadding">
                <input type="hidden" name="idOs" value="<?php echo $result->idOs;?>">
                <table class="table table-bordered ">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="width: 2%; text-align: center;">#</th>
                        <th style="text-align: left">Produto / Serviço</th>
                        <th style="width: 8%;text-align: center">Fotos</th>
                        <th style="width: 5%; text-align: center;">Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $usuario_id     =  $this->session->userdata('id');

                    foreach ($produtos as $produto) {

                        $imagens = $this->os_model->getAnexosByProdutosOs($this->uri->segment(3), $produto->idProdutos_os);

                        $ck         = '';
                        $permiteEditar   = true;
                        $responsavel_id = $produto->responsavel_id;
                        $descricao = $produto->descricao;
                        $backgroud = '#da4f49;';
                        $nome_usuario   =  '';
                        $data_produzido = $produto->data_produzido;
                        $hora_produzido = $produto->hora_produzido;
                        $data_produzido_conclusao = $produto->data_produzido_conclusao;
                        $hora_produzido_conclusao = $produto->hora_produzido_conclusao;

                        if ($data_produzido) {
                            $data_produzido = date(('d/m/Y'), strtotime($data_produzido));
                        }

                        if ($produto->responsavel_id) {
                            $responsavel = $this->db->get_where('usuarios' , array('idUsuarios' => $produto->responsavel_id ))->row();
                            if (count($responsavel) > 0) {
                                $nome_usuario = $responsavel->nome;
                            }
                        }

                        if ($responsavel_id) {
                            if ($usuario_id != $responsavel_id) {
                                $permiteEditar = false;
                            }
                        }

                        if ($produto->produzido_os == 1) {
                            $ck         = 'checked="checked"';
                            $backgroud = '#5bb75b;';
                        }

                        if ($nome_usuario) {
                            $descricao = $descricao.'<br/>'.$nome_usuario.'<br/>Inicio produção: '.$data_produzido.' '.$hora_produzido;
                        }

                        if($data_produzido_conclusao) {
                            $descricao = $descricao.'<br/>Final produção:   '.date(('d/m/Y'), strtotime($data_produzido_conclusao)).' '.$hora_produzido_conclusao;
                        }
                        echo '<tr>';
                        echo '<td style="width: 3%;background:'.$backgroud.'"></td>';
                        echo '<td>'.$descricao.'</td>';

                        echo '<td>';
                        foreach ($imagens as $a) {

                            if ($a->thumb == null) {
                                $thumb = base_url() . 'assets/img/icon-file.png';
                                $link = base_url() . 'assets/img/icon-file.png';
                            } else {
                                $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                $link = $a->url . $a->anexo;
                            }

                            echo ' <a href="#modal-anexo" idprodutoservico="'.$produto->idProdutos_os.'" tipo="Produto" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a> ';
                        }

                        echo '<a href="#anexar-novo" id="btn-anexar" role="button"
                                                produtos_id="'.$produto->produtos_id.'"
                                                idprodutoservico="'.$produto->idProdutos_os.'" tipo="Produto"
                                               data-toggle="modal" class="btn btn-success anexo"><i class="icon-white icon-plus"> Foto</i>
                                                 </a>';

                        echo '</td>';

                        echo '<td style="width: 5%; text-align: center;">';

                        if ($permiteEditar) {
                            if (!$data_produzido) {
                                echo '<a href="' . base_url() . 'index.php/os/iniciar_produzir/' . $produto->idProdutos_os . '/produto/' . $produto->os_id . '"  style="margin-right: 1%" class="btn tip-top" >Produzir</a>';
                            } else {
                                if (!$data_produzido_conclusao) {
                                    echo '<a href="' . base_url() . 'index.php/os/finaliza_produzir/' . $produto->idProdutos_os . '/produto/' . $produto->os_id . '"  style="margin-right: 1%" class="btn tip-top" >Finalizar</a>';
                                } else {
                                    echo '<a href="' . base_url() . 'index.php/os/reiniciar_producao/' . $produto->idProdutos_os . '/produto/' . $produto->os_id . '"   style="margin-right: 1%" class="btn tip-top" >Reiniciar Produção</a>';
                                }
                            }
                        } else {
                            echo '-';
                        }

                        //echo '  <input type="checkbox" '.$disabled.' id="'.$produto->idProdutos_os.'" value="produto_'.$produto->idProdutos_os.'" name="produzir[]" '.$ck.'>';
                        echo '</td>';
                        echo '</tr>';
                    }

                    foreach ($servicos as $servico) {

                        $imagens = $this->os_model->getAnexosByServicoOs($this->uri->segment(3), $servico->idServicos_os);

                        $ck         = '';
                        $permiteEditar   = true;
                        $responsavel_id = $servico->responsavel_id;
                        $descricao = $servico->nome;
                        $backgroud = '#da4f49;';
                        $nome_usuario   =  '';
                        $data_produzido = $servico->data_produzido;
                        $hora_produzido = $servico->hora_produzido;
                        $data_produzido_conclusao = $servico->data_produzido_conclusao;
                        $hora_produzido_conclusao = $servico->hora_produzido_conclusao;

                        if ($data_produzido) {
                            $data_produzido = date(('d/m/Y'), strtotime($data_produzido));
                        }

                        if ($servico->responsavel_id) {
                            $responsavel = $this->db->get_where('usuarios' , array('idUsuarios' => $servico->responsavel_id ))->row();
                            if (count($responsavel) > 0) {
                                $nome_usuario = $responsavel->nome;
                            }
                        }

                        if ($responsavel_id) {
                            if ($usuario_id != $responsavel_id) {
                                $permiteEditar = false;
                            }
                        }

                        if ($servico->produzido_os == 1) {
                            $ck         = 'checked="checked"';
                            $backgroud = '#5bb75b;';
                        }

                        if ($nome_usuario) {
                            $descricao = $descricao.'<br/>'.$nome_usuario.'<br/>Inicio produção: '.$data_produzido.' '.$hora_produzido;
                        }

                        if($data_produzido_conclusao) {
                            $descricao = $descricao.'<br/>Final produção:   '.date(('d/m/Y'), strtotime($data_produzido_conclusao)).' '.$hora_produzido_conclusao;
                        }

                        echo '<tr>';
                        echo '<td style="width: 3%;background:'.$backgroud.'"></td>';
                        echo '<td>'.$descricao.'</td>';
                        echo '<td>';

                        foreach ($imagens as $a) {

                            if ($a->thumb == null) {
                                $thumb = base_url() . 'assets/img/icon-file.png';
                                $link = base_url() . 'assets/img/icon-file.png';
                            } else {
                                $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                $link = $a->url . $a->anexo;
                            }

                            echo ' <a href="#modal-anexo" idprodutoservico="'.$servico->idServicos_os.'" tipo="Servico" localfoto="'.$a->localfoto.'" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idAnexos . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a> ';
                        }

                        echo '<a href="#anexar-novo" id="btn-anexar" role="button"
                                                servicos_id="'.$servico->servicos_id.'"
                                                idprodutoservico="'.$servico->idServicos_os.'" tipo="Servico"
                                               data-toggle="modal" class="btn btn-success anexo"><i class="icon-white icon-plus"> Foto</i>
                                                 </a>';
                        echo '</td>';

                        echo '<td style="width: 5%; text-align: center;">';

                        if ($permiteEditar) {
                            if (!$data_produzido) {
                                echo '<a href="' . base_url() . 'index.php/os/iniciar_produzir/' . $servico->idServicos_os . '/servico/' . $servico->os_id . '" style="margin-right: 1%" class="btn tip-top" >Produzir</a>';
                            } else {
                                if (!$data_produzido_conclusao) {
                                    echo '<a href="' . base_url() . 'index.php/os/finaliza_produzir/' . $servico->idServicos_os . '/servico/' . $servico->os_id . '" style="margin-right: 1%" class="btn tip-top" >Finalizar</a>';
                                } else {
                                    echo '<a href="' . base_url() . 'index.php/os/reiniciar_producao/' . $servico->idServicos_os . '/servico/' . $servico->os_id . '" style="margin-right: 1%" class="btn tip-top" >Reiniciar Produção</a>';
                                }
                            }
                        } else {
                            echo '-';
                        }

                        //echo '  <input type="checkbox" '.$disabled.' id="'.$servico->idServicos_os.'" value="servico_'.$servico->idServicos_os.'" name="produzir[]" '.$ck.'>';
                        echo '</td>';
                        echo '</tr>';
                    } ?>
                    <tr>
                    </tr>
                    </tbody>
                </table>
            </div>
             <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6 offset3" style="text-align: center">
                    <!--
                    <button class="btn btn-success" type="submit"><i class="icon icon-cog"></i>
                        Iniciar Produção
                    </button>
                    !-->
                    <a href="<?php echo  base_url(); ?>index.php/os/encaminhar_proximo_setor/<?php echo  $result->idOs;?>" id="btn-faturar" role="button"
                       data-toggle="modal" class="btn btn-warning">ENCAMINHAR <i class="icon-arrow-right "></i>
                    </a>
                </div>
            </div>
        </form>
     </div>
    <?php echo $this->pagination->create_links();
} ?>


<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo" style="text-align: center">
            <div class='progress progress-info progress-striped active'>
                <div class='bar' style='width: 100%'></div>
            </div>
        </div>


        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">LINK</label>
            <input type="text" id="link-visualizar" readonly name="link-visualizar"
                   class="span12"/>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="localfoto-visualizar">Local da Foto</label>
            <input type="text"  class="span12" id="localfoto-visualizar" readonly name="localfoto-visualizar">
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">Observação</label>
            <textarea class="span12" rows="5" readonly cols="5" id="observacaoimg-visualizar" name="observacaoimg-visualizar"></textarea>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="data-visualizar">Data</label>
            <input type="date" id="data-visualizar" readonly name="data-visualizar"
                   class="span4"/>

            <label for="hora-visualizar">Hora</label>
            <input type="time"  id="hora-visualizar"  readonly name="hora-visualizar"
                   class="span4"/>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
        <a href="#modal-email-foto" id="btn-email" onclick="$('#fechar_modal_foto').click();" role="button"
           data-toggle="modal" class="btn btn-success"><i class="icon-envelope"></i>
            Enviar-Email</a>
        <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
        <a href="" target="_blank" class="btn btn-invers" id="abrir-imagem">Abrir</a>
        <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
    </div>
</div>

<!-- NOVA FOTO -->
<div id="anexar-novo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <form  action="<?php echo base_url(); ?>index.php/os/anexarSetor" enctype="multipart/form-data" action="javascript:;"
           accept-charset="utf-8" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Visualizar Anexo</h3>
        </div>
        <div class="modal-body">
            <div class="span12" style="margin-left: 0">
                <label for="localfoto">Local da Foto*</label>
                <input type="text"  class="span12" id="localfoto" required="required" name="localfoto">
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="observacaoimg">Observação</label>
                <textarea class="span12" rows="5" cols="5" id="observacaoimg" name="observacaoimg"></textarea>
            </div>

            <input type="file" class="span12" required="required" name="userfile[]" multiple="multiple"
                   size="20"/>
        </div>

        <div class="modal-footer">
            <input type="hidden" name="tipo" id="tipo" value=""/>
            <input type="hidden" name="idprodutoservico" id="idprodutoservico" />
            <input type="hidden" name="produtos_id" id="produtos_id" />
            <input type="hidden" name="servicos_id" id="servicos_id" />

            <input type="hidden" name="idOsServico" id="idOsServico" value="<?php echo $this->uri->segment(3) ?>"/>
            <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
            <button class="btn btn-primary">Salvar</button>
        </div>
    </form>
</div>

<!-- Modal enviar email foto cliente -->
<div id="modal-email-foto" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmailFotoSetor" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Enviar E-mail de Orçamento para o cliente</h3>
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Para*</label>
                <input class="span12" id="para" type="text" name="para" required="required" value="<?php echo $result->emailCliente; ?>"/>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Assunto*</label>
                <input class="span12" id="assunto" type="text" name="assunto" required="required" value="<?php echo 'Acompanhamento da OS Nº ' . str_pad($result->idOs, 6, '0', STR_PAD_LEFT) ?>"/>
            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Mensagem*</label>
                <textarea class="span12 redactor" rows="10" cols="10" id="mensagem-foto" name="mensagem"></textarea>
            </div>

            <div class="modal-footer">
                <input type="hidden" name="idOs" value="<?php echo $result->idOs; ?>">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
                <button class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>
</div>

<script>

    $(document).ready(function () {

        $('.redactor').redactor({
            minHeight: 200
        });

        $(document).on('click', '.anexo', function (event) {
            event.preventDefault();

            var link = $(this).attr('link');
            var id = $(this).attr('imagem');
            var url = '<?php echo base_url(); ?>os/excluirAnexo/';
            var observacaoimg = $(this).attr('observacaoimg');
            var data = $(this).attr('data');
            var hora = $(this).attr('hora');
            var localfoto = $(this).attr('localfoto');
            var idprodutoservico = $(this).attr('idprodutoservico');
            var tipo =  $(this).attr('tipo');
            var produtos_id =  $(this).attr('produtos_id');
            var servicos_id =  $(this).attr('servicos_id');

            $("#div-visualizar-anexo").html('<img src="' + link + '" alt="">');
            $("#excluir-anexo").attr('link', url + id);

            $('#link-visualizar').val(link);
            $('#observacaoimg-visualizar').val(observacaoimg);
            $('#data-visualizar').val(data);
            $('#hora-visualizar').val(hora);
            $('#localfoto-visualizar').val(localfoto);
            $('#idprodutoservico').val(idprodutoservico);
            $('#tipo').val(tipo);
            $('#produtos_id').val(produtos_id);
            $('#servicos_id').val(servicos_id);

            $('#mensagem-foto').redactor('set',
                '<p>' +
                '<br/> <b>' + localfoto + '</b>' +
                '<br/>' +
                '<br/>' +
                '<img src="' + link + '" style="background-color: initial;">' +
                '<br/>' + observacaoimg +
                '</p>');

            $('#abrir-imagem').attr('href', link);
            $("#download").attr('href', "<?php echo base_url(); ?>index.php/os/downloadanexo/" + id);

        });

        $(document).on('click', '#excluir-anexo', function (event) {
            event.preventDefault();
            var link = $(this).attr('link');
            $('#modal-anexo').modal('hide');
            $("#divAnexos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

            $.ajax({
                type: "POST",
                url: link,
                dataType: 'json',
                success: function (data) {
                    if (data.result == true) {
                        location.reload();
                    }
                    else {
                        alert(data.mensagem);
                    }
                }
            });
        });
    });

</script>

