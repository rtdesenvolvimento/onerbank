<?php
/*
SystemBoys - Garotos de Sistema
http://www.systemboys.com.br
Marcos Aur�lio R. Silva
systemboy_marcos@hotmail.com
*/

function convert_number_to_words($number)
{

    $hyphen = '-';
    $conjunction = ' e ';
    $separator = ', ';
    $negative = 'menos ';
    $decimal = ' ponto ';
    $dictionary = array(
        0 => 'zero',
        1 => 'um',
        2 => 'dois',
        3 => 'tr�s',
        4 => 'quatro',
        5 => 'cinco',
        6 => 'seis',
        7 => 'sete',
        8 => 'oito',
        9 => 'nove',
        10 => 'dez',
        11 => 'onze',
        12 => 'doze',
        13 => 'treze',
        14 => 'quatorze',
        15 => 'quinze',
        16 => 'dezesseis',
        17 => 'dezessete',
        18 => 'dezoito',
        19 => 'dezenove',
        20 => 'vinte',
        30 => 'trinta',
        40 => 'quarenta',
        50 => 'cinquenta',
        60 => 'sessenta',
        70 => 'setenta',
        80 => 'oitenta',
        90 => 'noventa',
        100 => 'cento',
        200 => 'duzentos',
        300 => 'trezentos',
        400 => 'quatrocentos',
        500 => 'quinhentos',
        600 => 'seiscentos',
        700 => 'setecentos',
        800 => 'oitocentos',
        900 => 'novecentos',
        1000 => 'mil',
        1000000 => array('milh�o', 'milh�es'),
        1000000000 => array('bilh�o', 'bilh�es'),
        1000000000000 => array('trilh�o', 'trilh�es'),
        1000000000000000 => array('quatrilh�o', 'quatrilh�es'),
        1000000000000000000 => array('quinquilh�o', 'quinquilh�es')
    );


    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words s� aceita n�meros entre ' . PHP_INT_MAX . ' � ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int)($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $conjunction . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = floor($number / 100) * 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int)($number / $baseUnit);
            $remainder = $number % $baseUnit;
            if ($baseUnit == 1000) {
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[1000];
            } elseif ($numBaseUnits == 1) {
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit][0];
            } else {
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit][1];
            }
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string)$fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}


$totalProdutos = 0;
$totalServico = 0;

foreach ($produtos as $p) {
    $totalProdutos = $totalProdutos + $p->subTotal;
}

foreach ($servicos as $s) {
    $preco = $s->subTotal;
    $totalServico = $totalServico + $preco;
}


$firma = $emitente->nome;
$cnpj_cgc_firma = $emitente->cnpj;
$logomarca = $emitente->url_logo;

$nome_devedor = $result->nomeCliente;
$cpf_cnpj_devedor = $result->documento;
$endereco = $result->rua . ' ' . $result->numero . ',' . $result->bairro . ' - ' . $result->cidade . '/' . $result->estado;
$valor_devedor = $totalProdutos + $totalServico;
$rnumero = 'reais';
if ($valor_devedor == 1) {
    $rnumero = 'real';
}
$valor_extenso_devedor = convert_number_to_words($valor_devedor) . ' ' . $rnumero;
$referente = 'manuten&#231;&#227;o e consertos de autom&#243;veis';
$final = 'estando quitado o d&#233;bito referido a esta data';
$dia_pagamento = date("d");
$mes_pagamento = date("m");
$ano_pagamento = date("y");
$cidade = $emitente->cidade;
$estado = $emitente->uf;
?>

<?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
    echo '<a class="btn btn-mini btn-info" href="' . base_url() . 'index.php/os/editar/' . $result->idOs . '"><i class="icon-pencil icon-white"></i> Editar</a>';
} ?>

<p>
    <a id="imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i>
        Imprimir</a>
</p>

<div class="widget-content" id="printOs">
    <div class="invoice-content">
        <h1 align="center"><img src="<?php echo $logomarca; ?>" alt="<?php echo $firma; ?>" width="100"></h1>
        <h1 align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">RECIBO</font></h1>
        <p align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $firma; ?><br/>CNPJ: <?php echo $cnpj_cgc_firma; ?>
            </font></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p align="right"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">RECIBO:
                R$ <?php echo $valor_devedor; ?></font></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                    style="font-family:Verdana, Arial, Helvetica, sans-serif">Recebemos de (a)
                <strong><?php echo $nome_devedor; ?>

                    <?php if ($cpf_cnpj_devedor) { ?>
                </strong> portador (a) do CPF/CNPJ: <strong><?php echo $cpf_cnpj_devedor; ?></strong>
                <?php } ?>

                , a import&acirc;ncia de R$ <strong><?php echo $valor_devedor; ?></strong>
                (<?php echo $valor_extenso_devedor; ?>) referente &agrave;
                <strong><?php echo $referente; ?></strong>, <?php echo $final; ?>.</font></p>

        <?php if ($result->rua) { ?>
            <p align="right"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">
                    <?php echo $endereco; ?></font>
            </p>
        <?php } ?>

        <p>&nbsp;</p>
        <p align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $cidade; ?>
                - <?php echo $estado; ?>, <?php echo $dia_pagamento; ?>/<?php echo $mes_pagamento; ?>
                /<?php echo $ano_pagamento; ?>.</font></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p align="center">________________________________________<br/><font
                    style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $firma; ?></font></p>


    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#imprimir").click(function () {
            PrintElem('#printOs');
        })

        function PrintElem(elem) {
            Popup($(elem).html());
        }

        function Popup(data) {
            var mywindow = window.open('', 'MapOs', 'height=600,width=800');
            mywindow.document.write('<html><head><title>Map Os</title>');
            mywindow.document.write('<html><head><title>Map Os</title>');
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap-responsive.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-style.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-media.css' />");


            mywindow.document.write("</head><body >");
            mywindow.document.write(data);
            mywindow.document.write("</body></html>");

            mywindow.print();
            mywindow.close();

            return true;
        }

    });
</script>