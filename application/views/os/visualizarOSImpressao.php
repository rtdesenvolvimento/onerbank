<?php $totalServico = 0;
$totalProdutos = 0; ?>
<?php
/*
SystemBoys - Garotos de Sistema
http://www.systemboys.com.br
Marcos Aurélio R. Silva
systemboy_marcos@hotmail.com
*/

function convert_number_to_words($valor = 0, $maiusculas = false)
{
    // verifica se tem virgula decimal
    if (strpos($valor, ",") > 0) {
        // retira o ponto de milhar, se tiver
        $valor = str_replace(".", "", $valor);

        // troca a virgula decimal por ponto decimal
        $valor = str_replace(",", ".", $valor);
    }
    $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
    $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões",
        "quatrilhões");

    $c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
        "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
    $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
        "sessenta", "setenta", "oitenta", "noventa");
    $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
        "dezesseis", "dezesete", "dezoito", "dezenove");
    $u = array("", "um", "dois", "três", "quatro", "cinco", "seis",
        "sete", "oito", "nove");

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    $cont = count($inteiro);
    for ($i = 0; $i < $cont; $i++)
        for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
            $inteiro[$i] = "0" . $inteiro[$i];

    $fim = $cont - ($inteiro[$cont - 1] > 0 ? 1 : 2);
    $rt = '';
    for ($i = 0; $i < $cont; $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                $ru) ? " e " : "") . $ru;
        $t = $cont - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000"

        ) $z++; elseif ($z > 0)
            $z--;
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= (($z > 1) ? " de " : "") . $plural[$t];
        if ($r)
            $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                    ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    if (!$maiusculas) {
        return ($rt ? $rt : "zero");
    } elseif ($maiusculas == "2") {
        return (strtoupper($rt) ? strtoupper($rt) : "Zero");
    } else {
        return (ucwords($rt) ? ucwords($rt) : "Zero");
    }
}


$firma = $emitente->nome;
$cnpj_cgc_firma = $emitente->cnpj;
$logomarca = $emitente->url_logo;

$nome_devedor = $result->nomeCliente;
$cpf_cnpj_devedor = $result->documento;
$endereco = $result->rua . ' ' . $result->numero . ',' . $result->bairro . ' - ' . $result->cidade . '/' . $result->estado;

$referente = 'referente a manuten&#231;&#227;o, conserto e (ou) venda de pe&#231;as';
$final = 'estando quitado o d&#233;bito referido a esta data';
$dia_pagamento = date("d");
$mes_pagamento = date("m");
$ano_pagamento = date("y");
$cidade = $emitente->cidade;
$estado = $emitente->uf;
?>

<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/bootstrap.min.css'/>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css'/>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/matrix-style.css'/>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/matrix-media.css'/>

<style type="text/css">
    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 11px;
        color: #666;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
     }

    img {
        width: 9%;
        height: 9%;
        max-width: 100%;
        vertical-align: middle;
        border: 0;
        -ms-interpolation-mode: bicubic;
    }

    .tamanhotdimg{
        width: 10%
    }
</style>

<div class="widget-content" id="printOs">
    <div class="invoice-content">

        <table class="table" style="margin-bottom: 0">
            <tbody>
            <?php if ($emitente == null) { ?>
                <tr>
                    <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a
                                href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<
                    </td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td class="tamanhotdimg"><img src=" <?php echo $emitente->url_logo; ?> "></td>
                    <td>
                        <span style="font-size: 20px; "> <?php echo $emitente->nome; ?></span> <br/>
                        <span><?php echo $emitente->cnpj; ?> <br/> <?php echo $emitente->rua . ', nº:' . $emitente->numero . ', ' . $emitente->bairro . ' - ' . $emitente->cidade . ' - ' . $emitente->uf; ?> </span> <br/>
                        <span> E-mail: <?php echo $emitente->email . ' - Fone: ' . $emitente->telefone; ?></span>
                    </td>
                    <td style="width: 18%; text-align: center">
                        <b>#Ordem de Serviço:</b> <span><?php echo $result->idOs ?>
									</span></br> </br> <span>Emissão: <?php echo  date(('d/m/Y'), strtotime($result->dataInicial)); ?></span><br/>
                        <?php if ($result->dataFinal != '0000-00-00') {?>
                            </br> <span>Previsão: <?php echo  date(('d/m/Y'), strtotime($result->dataFinal)); ?></span><br/>
                        <?php } ?>
                        <span>Status:<?php echo $result->status ?></span>
                    </td>
                </tr>

            <?php } ?>
            </tbody>
        </table>

        <table class="table" style="margin-bottom: 0">
            <tbody>
            <tr>
                <td style="width: 40%; padding-left: 0">
                    <ul>
                        <li>
                            <span><h5>Cliente</h5>
                            <span><?php echo $result->nomeCliente ?></span><br/>
                            <span><?php echo $result->rua ?>, <?php echo $result->numero ?>
                                , <?php echo $result->bairro ?></span><br/>
                            <span><?php echo $result->cidade ?> - <?php echo $result->estado ?></span>
                        </li>
                    </ul>
                </td>
                <td style="width: 40%; padding-left: 0">
                    <ul>
                        <li>
                            <span><h5>Responsável</h5></span>
                            <span><?php echo $result->nome ?></span> <br/>
                            <span>Telefone: <?php echo $result->telefone ?></span><br/>
                            <span>Email: <?php echo $result->email ?></span>
                        </li>
                    </ul>
                </td>

                <td style="width: 20%; padding-left: 0">
                    <ul>
                        <li>
                            <span><h5>Veículo</h5></span>
                            <?php
                            foreach ($veiculos as $veiculo) { ?>
                                <span>
														<?php echo '' . $veiculo->tipoVeiculo . '/' . $veiculo->marca . '/' . $veiculo->modelo . '/' . $veiculo->placa . '/' . $veiculo->cor . '/' . $veiculo->ano; ?>
													</span><br/>
                            <?php } ?>
                            <span>Kilometragem de entrada: <?php echo $result->kilometragementrada ?></span> <br/>
                            <span>Kilometragem de saída: <?php echo $result->kilometragemsaida ?></span><br/>
                        </li>
                    </ul>
                </td>

            </tr>
            </tbody>
        </table>

        <div style="margin-top: 0; padding-top: 0">

            <table class="table" style="margin-bottom: 0">
                <tbody>
                <tr>
                    <td>
                        <?php if ($result->defeito != null) { ?>
                            <h5>Defeito reclamado</h5>
                            <p>
                                <?php echo $result->defeito ?>
                            </p>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if ($result->observacoes != null) { ?>
                            <h5>Defeito costatado</h5>
                            <p>
                                <?php echo $result->observacoes ?>
                            </p>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php if ($result->laudoTecnico != null) { ?>
                            <h5>Laudo Técnico</h5>
                            <p>
                                <?php echo $result->laudoTecnico ?>
                            </p>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if ($result->acessorios != null) { ?>
                            <h5>Acessórios</h5>
                            <p>
                                <?php echo $result->acessorios ?>
                            </p>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php if ($result->descricaoProduto != null) { ?>
                            <h5>Observações</h5>
                            <p>
                                <?php echo $result->descricaoProduto ?>

                            </p>
                        <?php } ?>
                    </td>
                    <td></td>
                </tr>

                </tbody>
            </table>
            <br/>
                <?php if ($produtos != null) { ?>
                     <table class="table table-bordered;" id="tblProdutos">
                        <thead>
                        <tr>
                            <th width="55%">Produto</th>
                            <th width="5%">Quantidade</th>
                            <th width="20%" style="text-align: right">Sub-total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($produtos as $p) {

                            $totalProdutos = $totalProdutos + $p->subTotal;
                            $valor = (float)str_replace(".", ",", $p->subTotal);
                            echo '<tr>';
                            echo '<td>' . $p->descricao . '</td>';
                            echo '<td > <center> ' . $p->quantidade . ' </center> </td>';

                            echo '<td style="text-align: right">R$ ' . number_format($valor, 2, ',', '.') . '</td>';
                            echo '</tr>';
                        } ?>

                        <tr>
                            <td width="70%" colspan="2" style="text-align: right"><strong>Total:</strong></td>
                            <td width="30%" style="text-align: right">
                                <strong>R$ <?php echo number_format($totalProdutos, 2, ',', '.'); ?></strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                <?php } ?>

                <?php if ($servicos != null) { ?>
                        <br/>
                         <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="70%">Serviço</th>
                                <th width="30%" style="text-align: right">Sub-total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            setlocale(LC_MONETARY, 'en_US');
                            foreach ($servicos as $s) {
                                $preco = $s->subTotal;
                                $totalServico = $totalServico + $preco;
                                $valor = (float)str_replace(".", ",", $preco);

                                if ($valor) {
                                    $valorFormatada = number_format($valor, 2, ',', '.');
                                }

                                echo '<tr>';
                                echo '<td>' . $s->nome . '</td>';
                                echo '<td style="text-align: right">R$ ' . $valorFormatada . '</td>';
                                echo '</tr>';
                            } ?>

                            <tr>
                                <td colspan="1" style="text-align: right"><strong>Total:</strong></td>
                                <td style="text-align: right"><strong>R$ <?php echo number_format($totalServico, 2, ',', '.'); ?></strong>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                     <?php } ?>

            <h4 style="text-align: right">Valor Total:
                R$ <?php echo number_format($totalProdutos + $totalServico, 2, ',', '.'); ?></h4>

            <?php
            $valor_devedor = $totalProdutos + $totalServico;
            $valor_extenso_devedor = convert_number_to_words($valor_devedor);
            ?>

            <?php if ($result->faturado == 1) { ?>
                <h4 align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">RECIBO</font></h4>
                <p align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $firma; ?>
                        <br/>CNPJ: <?php echo $cnpj_cgc_firma; ?></font></p>
                <p align="right"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">RECIBO:
                        R$ <?php echo $valor_devedor; ?></font></p>
                <p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
                            style="font-family:Verdana, Arial, Helvetica, sans-serif">Recebemos de (a)
                        <strong><?php echo $nome_devedor; ?>

                            <?php if ($cpf_cnpj_devedor) { ?>
                        </strong> portador (a) do CPF/CNPJ: <strong><?php echo $cpf_cnpj_devedor; ?></strong>
                        <?php } ?>

                        , a import&acirc;ncia de R$ <strong><?php echo $valor_devedor; ?></strong>
                        (<?php echo $valor_extenso_devedor; ?>) referente &agrave;
                        <strong><?php echo $referente; ?></strong>, <?php echo $final; ?>.</font></p>

                <?php if ($result->rua) { ?>
                    <p align="right"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">
                            <?php echo $endereco; ?></font>
                    </p>
                <?php } ?>

                <p align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $cidade; ?>
                        - <?php echo $estado; ?>, <?php echo $dia_pagamento; ?>/<?php echo $mes_pagamento; ?>
                        /<?php echo $ano_pagamento; ?>.</font></p>
                <p>&nbsp;</p>
                <p align="center">________________________________________<br/><font
                            style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $firma; ?></font></p>

            <?php } ?>

            <br/><br/><br/>
            <table style="margin-bottom: 0;width: 100%">
                <tbody>
                <tr>

                    <td align="center">
                        <br/>
                        _________________________________________<br/>
                        Visto: <?php echo $result->nomeCliente; ?>
                    </td>

                    <td align="center">
                        <br/>
                        _________________________________________<br/>
                        Visto: <?php echo $emitente->nome; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>