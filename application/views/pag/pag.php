<?php

$dataHoje = date('Y-m-d');
$validade = date('Y-m-d');
$taxaIntermediacao = 0;
$modo_parcelamento = '';
$valor = 0;

$cpfComprador = '';
$nomeComprador = '';
$statusParcela = '';

if ($result != null) {

    if ($result->parcela_id != null) {
        $parcela = $this->financeiro_model->getParcelaById($result->parcela_id);
        $lancamento = $this->financeiro_model->getById($parcela->lancamentos_id);
        $cliente = $this->clientes_model->getById($lancamento->clientes_id);

        $cpfComprador = $cliente->documento;
        $nomeComprador = $cliente->nomeCliente;

        $valor = $parcela->valorPagar;

        $statusParcela = $parcela->status;

    } else {
        $valor = $result->valor;
    }

    $validade = $result->validade;
    $formPagamento = $result->forma_pagamento;

    $tarifaFixa = $taxas->tarifaFixa;
    $taxaIntermediacao = $taxas->taxaIntermediacao;
    $modo_parcelamento = $result->modo_parcelamento;

    if ($taxaIntermediacao > 0) $taxaIntermediacao = $taxaIntermediacao/100;

    if ($modo_parcelamento == 'with_interest') {
        $valor = $valor + ($valor * $taxaIntermediacao);
        $valor = $valor + $tarifaFixa;
    }

    if (strtotime($dataHoje) > strtotime($validade)) $formPagamento = '';
    if ($valor <= 0) $validade =  $formPagamento = '';

    if ($result->status == 'INATIVO' || $statusParcela == 'cancelada') $formPagamento = '';
}

?>
<!DOCTYPE html>
<html lang="en" >
<head>

    <title>Checkout de pagamento <?php echo $emitente->nome;?></title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="application-name" content="<?php echo $emitente->nome;?>">

    <meta name="title" content="<?php echo $result->descricao; ?>">

    <meta name="description" content="<?php echo $result->descricao.'<br/> R$ '.$this->site_model->formatarValorMonetario($valor); ?>" >
    <meta property="og:description" content="<?php echo $result->descricao.'<br/> R$ '.$this->site_model->formatarValorMonetario($valor); ?>" />
    <meta name="keywords" content="">

    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />
    <meta property="og:image" content="<?php echo $emitente->url_logo;?>" />
    <meta property="og:type" content="article" />
    <meta name="author" content="<?php echo $emitente->nome;?>"/>

    <link rel="canonical" href="<?php echo base_url();?>">
    <link rel="canonical" href="<?php echo base_url();?>">

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pag/style.css"/>

    <style>

        body {
            background-image: url('<?php echo base_url(); ?>assets/images/fundo.png');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
            bottom: 0;
            color: black;
            left: 0;
            overflow: auto;
            padding: 3em;
            position: absolute;
            right: 0;
            top: 0;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
             position: inherit;
             left: 0px;
             visibility: visible;
        }

        .forma_pagamento {
            cursor: pointer;
            padding: 0px 5px;
            background: #eee;
            font-size: 17px;
        }

        .error_pagamento {
            cursor: pointer;
            padding: 15px 5px;
            background: #f4433673;
            font-size: 17px;
        }
    </style>
</head>
<body>

<!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>

<!--Begin Checkout-->
<div class="container">
    <?php if ($result != null) {?>
    <form action="<?php echo base_url(); ?>pag/pagSend" id="formPagSend" method="post" class="form-horizontal">
        <input type="hidden" name="token" id="token" value="<?php echo $result->token; ?>">
        <input type="hidden" name="cnpj" id="token" value="<?php echo $emitente->cnpj; ?>">
        <div class="box card-panel z-depth-3">
            <div class="merchant">
                <img id="crickets" style="border-radius: 20px;" src="<?php echo $emitente->url_logo;?>" />
                <h5 class="center-align"><?php echo $emitente->nome;?></h5>
                <p><?php echo $this->site_model->dataDeHojePorExtenso(date('Y-m-d'));?></p>
            </div>
            <div class="invoice">
                <table class="highlight">
                    <thead>
                    <tr>
                        <th>QTD</th>
                        <th>ITEM</th>
                        <th class="right-align">PREÇO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td><?php echo $result->descricao; ?></td>
                        <td class="right-align">R$<?php echo $this->site_model->formatarValorMonetario($valor); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="right-align bold">Total</td>
                        <td class="right-align bold">R$<?php echo $this->site_model->formatarValorMonetario($valor); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="payment">
                <div class="forma_pagamento">
                    <?php if ($formPagamento== 'credito'){?>
                        <input type="radio" class="rformapagamento" id="rcc" name="forma" value="cc" checked="checked"> Cartão de crédito
                    <?php }?>

                    <?php if ($formPagamento == 'boleto'){?>
                        <input type="radio" class="rformapagamento" id="rb" name="forma" value="b" onclick="" checked="checked"> Boleto
                    <?php }?>

                    <?php if ($formPagamento == 'credito_boleto'){?>
                        <input type="radio" class="rformapagamento" id="rcc" name="forma" value="cc" checked="checked"> Cartão de crédito
                        <input type="radio" class="rformapagamento" id="rb" name="forma" value="b" onclick=""> Boleto
                    <?php }?>
                    <img src="<?php echo base_url(); ?>assets/js/images/formas-pagamento.png" id="formasPagamento" style="width: 100%">
                </div>
                <h5>Informações de Pagamentos</h5>
                <small>
                    <img alt="Ambiente Seguro" style="width: 25px; height: 25px;" height="40" width="40" src="<?php echo base_url(); ?>assets/js/images/lock.png">
                    <strong style="top: -7px;position: relative;color: #19bc4f;">Você está em um ambiente seguro!</strong>
                </small>
                <?php if ($formPagamento == 'boleto' || $formPagamento== 'credito_boleto'){?>
                    <div id="boleto" <?php if($formPagamento == 'credito_boleto') echo 'style="display: none;'?>">
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="CPF" id="documento" name="documento" type="text" value="<?php echo $cpfComprador;?>"  class="validate">
                                <label for="documento">CPF</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Nome completo" id="nome" name="nome" type="text" class="validate">
                                <label for="nome">Nome completo</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Celular" id="telefone"  name="telefone" type="text" class="validate">
                                <label for="telefone">Celular</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="E-mail" id="email" name="email" type="text" class="validate">
                                <label for="email">E-mail</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="CEP" id="cep" type="text"  name="cep" class="validate">
                                <label for="email">CEP</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Endereço" id="endereco" name="endereco" type="text" class="validate">
                                <label for="endereco">Endereço</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Número" id="numero" name="numero" type="text" class="validate">
                                <label for="numero">Número</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="Complemento" id="complemento" name="complemento" type="text" class="validate">
                                <label for="complemento">Complemento</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Bairro" id="bairro" name="bairro" type="text" class="validate">
                                <label for="bairro">Bairro</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Cidade" id="cidade" name="cidade" type="text" class="validate">
                                <label for="cidade">Cidade</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="Estado" id="estado" name="estado" type="text" class="validate">
                                <label for="estado">Estado</label>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($formPagamento == 'credito' || $formPagamento == 'credito_boleto'){?>
                    <div id="cartao">
                        <div style="background: linear-gradient(135deg, #03A9F4, #1976D2);color: #FFFFFF;">
                        <select id="parcelas" name="parcelas">
                            <?php

                            $inicial = 1;
                            $numeroParcelas = $result->numero_parcelas;

                            if ($result->condicao_pagamento != null) {
                                $condicaoPagamento = $this->condicao_pagamento_model->getById($result->condicao_pagamento );
                                $numeroParcelas = $condicaoPagamento->parcelas;
                                $inicial = $condicaoPagamento->parcelas;
                            }

                            for($i=$inicial; $i<=$numeroParcelas;$i++) {

                                $valorParcela = $valor/$i;
                                $taxaParcela = 0;
                                $totalParcelaComJuros = 0;

                                if ($result->condicao_pagamento != null) {

                                }

                                if ($modo_parcelamento == 'with_interest') {
                                    if ($i==1) {
                                        $taxaParcela = ($taxas->taxaParcelamento1x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==2) {
                                        $taxaParcela = ($taxas->taxaParcelamento2x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==3) {
                                        $taxaParcela = ($taxas->taxaParcelamento3x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==4) {
                                        $taxaParcela = ($taxas->taxaParcelamento4x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==5) {
                                        $taxaParcela = ($taxas->taxaParcelamento5x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==6) {
                                        $taxaParcela = ($taxas->taxaParcelamento6x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==7) {
                                        $taxaParcela = ($taxas->taxaParcelamento7x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==8) {
                                        $taxaParcela = ($taxas->taxaParcelamento8x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==9) {
                                        $taxaParcela = ($taxas->taxaParcelamento9x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==10) {
                                        $taxaParcela = ($taxas->taxaParcelamento10x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==11) {
                                        $taxaParcela = ($taxas->taxaParcelamento11x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }

                                    if ($i==12) {
                                        $taxaParcela = ($taxas->taxaParcelamento12x/100);
                                        $valorParcela = $valorParcela + ($valorParcela*$taxaParcela);
                                        $totalParcelaComJuros = $valorParcela*$i;
                                    }
                                }

                                ?>
                                <?php if ($taxaParcela==0) {?>
                                    <option value="<?php echo $i;?>"> <?php echo $i;?> X R$ <?php echo $this->site_model->formatarValorMonetario($valorParcela) . ' (R$ '.$this->site_model->formatarValorMonetario($valor).')'; ?>  </option>
                                <?php } else { ?>
                                    <option value="<?php echo $i;?>"> <?php echo $i;?> X R$ <?php echo $this->site_model->formatarValorMonetario($valorParcela).' (R$ '.$this->site_model->formatarValorMonetario($totalParcelaComJuros).')'; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="  CPF do comprador" id="cpfComprador" <?php if ($cpfComprador != '') echo 'disabled="disabled"';?> value="<?php echo $cpfComprador;?>" name="cpfComprador" required="required" style="background: linear-gradient(135deg, #03A9F4, #1976D2);color: #ffffff;padding: 3px;font-size: 15px;" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row" style="margin-top: -30px;">
                            <div class="input-field col s12">
                                <input placeholder="  Nome completo do comprador" id="nomeComprador" <?php if ($nomeComprador != '') echo 'disabled="disabled"';?> value="<?php echo $nomeComprador;?>"  required="required" name="nomeComprador" style="background: linear-gradient(135deg, #03A9F4, #1976D2);color: #ffffff;padding: 3px;font-size: 15px;" type="text" class="validate">
                            </div>
                        </div>
                        <div class="credit-card-box card-panel z-depth-2 animation-element slide-left">
                            <div class="flip">
                                <div class="front">
                                    <div class="logo">
                                       <!-- <img src="http://cdn.flaticon.com/svg/39/39134.svg" alt="" />!-->
                                    </div>
                                    <div class="number input-field">
                                        <label for="card-number">Número do Cartão</label>
                                        <input type="text" id="cardnumber" name="cardnumber" class="input-card-number" maxlength="4" />
                                        <input type="text" id="cardnumber-1"  name="card-number-1" class="input-card-number" maxlength="4" />
                                        <input type="text" id="cardnumber-2" name="card-number-2" class="input-card-number" maxlength="4" />
                                        <input type="text" id="cardnumber-3" name="card-number-3" class="input-card-number" maxlength="4" />
                                    </div>
                                    <div class="cvv input-field">
                                        <label for="card-cvv">CVV</label>
                                        <input type="text" id="card-cvv" name="card-cvv" class="input-card-cvv" maxlength="3" />
                                    </div>
                                    <div class="card-holder input-field">
                                        <label for="name">Dono do Cartão</label>
                                        <input type="text" id="name" name="name">
                                    </div>
                                    <div class="card-expiration-date input-field">
                                        <select id="month" name="card-month">
                                            <option></option>
                                            <option value="01">Jan</option>
                                            <option value="02">Fev</option>
                                            <option value="03">Mar</option>
                                            <option value="04">Abr</option>
                                            <option value="05">Mai</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Jul</option>
                                            <option value="08">Aug</option>
                                            <option value="09">Set</option>
                                            <option value="10">Out</option>
                                            <option value="11">Nov</option>
                                            <option value="12">Dez</option>
                                        </select>
                                        <select id="year" name="card-year">
                                            <option></option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                        </select>
                                        <label>Validade</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="button checkout row">
                <button class="col s6 btn-large green btn waves-effect waves-dark register" id="enviar" type="button"><span id="btnConfirmar">Confirmar</span> <i class="fa fa-check"></i></button>
            </div>

            <?php if ($result->venda_id != null){?>
                <div class="button checkout row">
                    <button class="col s6 btn-large red btn waves-effect waves-dark register" id="cancelar" type="button">Cancelar<i class="fa fa-check"></i></button>
                </div>
            <?php } ?>

            <br/>
            <small style="text-align: center;">
                <img alt="Ambiente Seguro" src="<?php echo base_url(); ?>assets/js/images/Logo-OnerBank.png">
                <strong style="top: -7px;position: relative;color: #457e9b;">power by <a href="https://onerbank.com.br/" target="_blank">OnerBank</a></strong>
            </small>
        </div>
    </form>
    <?php } ?>
</div>
<script>
    $(document).ready(function() {

        <?php if ($cpfComprador == '') {?>
            $("#documento").mask("999.999.999-99");
            $("#cpfComprador").mask("999.999.999-99");
        <?php } else {?>
            verificaCadastro('<?php echo $cpfComprador;?>');
        <?php } ?>

        $("#telefone").mask("(99) 99999-9999");

        <?php if ($formPagamento == 'boleto'){?>
            boletoObrigatorio();
        <?php }?>

        <?php if ($result == null) { ?>
            $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/error.png" /><div class="error_pagamento">Link de pagamento não encontrado..</div></h5>'});
        <?php }?>

        <?php if (strtotime($dataHoje) > strtotime($validade) || $result->status == 'INATIVO' ) { ?>
            $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/error.png" /><div class="error_pagamento">Link de pagamento inválido!</div></h5>'});
        <?php }?>

        $.validator.setDefaults({
            errorClass: 'invalid',
            validClass: "valid",
            errorPlacement: function(error, element) {
                $(element)
                    .closest("form")
                    .find("label[for='" + element.attr("id") + "']")
                    .attr('data-error', 'Obrigatório');
            },
            submitHandler: function(form) {
                console.log('form ok');
            }
        });

        $('select').material_select();

        $(".rformapagamento").click(function() {
            if ($("#rcc").prop("checked")) {
                $('#boleto').hide();
                $('#cartao').show();
                $('#btnConfirmar').html('Confirmar');
                $('#formasPagamento').show();
                removerBoletoObragatorio();
                cartaoObrigatorio();
            } else {
                $('#boleto').show();
                $('#cartao').hide();
                $('#btnConfirmar').html('Gerar Boleto');
                $('#formasPagamento').hide();
                boletoObrigatorio();
                removeCartaoObrigatorio();
            }
        });

        $('#cancelar').click(function (event) {
            if (confirm('Deseja realmente cancelar?') ) {

                $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Cancelando Aguarde..</h5>'});

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>pdv/cancelar",
                    data: {
                        vendas_id: '<?php echo $result->venda_id;?>',
                        link_pagamento: '<?php echo $result->idLinkPagamento; ?>'
                    },
                    dataType: 'html',
                    success: function (returnPag) {
                        location.reload();
                    }
                });
            }
        });

        $('#enviar').click(function (event) {
            if ( $("#formPagSend").valid() ) {
                event.preventDefault();

                if ($("#rcc").prop("checked")) {
                    $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Processando Pagamento...</h5>'});
                } else {
                    $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Gerando Boleto...</h5>'});
                }

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>pag/pagSend",
                    data: $("#formPagSend").serialize(), // serializes the form's elements.
                    dataType: 'json',
                    success: function (returnPag) {
                        if (returnPag.error) {
                            $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/error.png" /><div class="error_pagamento">' + returnPag.message_display + '</div><br/><br/><a onclick="recarregar();">Tentar Novamente</a></h5>'});
                        } else if (returnPag.boleto) {
                            $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/success.png" /> Boleto Gerado! <br/><a href="<?php echo base_url();?>boleto/gerarBoleto/<?php echo $emitente->cnpj;?>/'+returnPag.link+'" target="_blank" >Visualizar Boleto<a/> </h5>'});
                        } else {
                            $.blockUI({message: '<h5><img src="<?php echo base_url(); ?>assets/js/images/success.png" /> Pagamento realizado com sucesso!<br/><a href="<?php echo base_url()?>pag/recibo/'+returnPag.numero_transacao+'">Recibo aqui</a></h5>'});
                        }
                    }
                });
            }
        });

        $('#cep').change(function (event) {
            prreencherEndereco($(this).val());
        });

        $('#documento').blur(function (event) {
            var cpf = $(this);
            verificaCadastro(cpf);
        });
    });

    function verificaCadastro(cpf) {

        if (cpf  !== '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>clientes/verificarCadatro",
                data: {
                    cpf: $('#documento').val()
                },
                dataType: 'json',
                success: function (pessoa) {

                    if (pessoa.nomeCliente !== undefined) {
                        var cep =  pessoa.cep;

                        if (cep !== '' && cep !== null) {
                            cep = cep.replace('.', '');
                            cep = cep.replace('-', '');
                        }

                        $('#nome').val(pessoa.nomeCliente);
                        $('#telefone').val(pessoa.celular);
                        $('#email').val(pessoa.email);
                        $('#cep').val(cep);
                        $('#endereco').val(pessoa.rua);
                        $('#numero').val(pessoa.numero);
                        $('#complemento').val(pessoa.complemento);
                        $('#bairro').val(pessoa.bairro);
                        $('#cidade').val(pessoa.cidade);
                        $('#estado').val(pessoa.estado);
                    }
                }
            });
        }
    }

    function prreencherEndereco(cep) {
        if (cep === '') return;
        var url = 'https://api.postmon.com.br/v1/cep/'+cep;
        $.get(url,{
                cep: cep
            },
            function (data) {
                if(data !== -1){
                    $("#endereco").val(data.logradouro);
                    $("#bairro").val( data.bairro );
                    $("#cidade").val( data.cidade );
                    $("#estado").val( data.estado );
                    $('#numero').focus();
                }
            });
    }

    function cartaoObrigatorio() {
        $('#formPagSend').validate({
            rules: {
                cpfComprador: {required: true},
                nomeComprador: {required: true},
            }
        });
    }

    function removeCartaoObrigatorio() {
        $('#formPagSend').validate({
            rules: {
                cpfComprador: {required: false},
                nomeComprador: {required: false},
            }
        });
    }

    function boletoObrigatorio() {

        $('#formPagSend').validate({
            rules: {
                documento: {required: true},
                nome: {required: true},
                cep: {required: true},
                endereco: {required: true},
                bairro: {required: true},
                cidade: {required: true},
                estado: {required: true},
                telefone: {required: true},
            }
        });
    }

    function removerBoletoObragatorio() {
        $('#formPagSend').validate({
            rules: {
                documento: {required: false},
                nome: {required: false},
                cep: {required: false},
                endereco: {required: false},
                bairro: {required: false},
                cidade: {required: false},
                estado: {required: false},
                telefone: {required: true},
            }
        });
    }

    function recarregar() {
        location.reload();
    }
</script>
<!-- partial -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pag/script.js"></script>
</body>
</html>