<html>
<head>
    <title>Comprovante de pagamento</title>
</head>
<body>
<div bgcolor="#F2F2F2" style="background:#f2f2f2;">
    <br><br>
    <table width="95%" border="0" cellpadding="0" cellspacing="0" style="max-width:720px;min-width:375px;background:#3bc3e7;border-radius:3px" bgcolor="#3BC3E7" align="center">
        <tbody>
        <tr>
            <td width="10%"></td>
            <td width="90%"></td>
        </tr>
        <tr>
            <td>
                <img src="<?php echo $emitente->url_logo;?>" alt="OnerBank Facil" width="65px" height="65px" style="display:inline-block;float:left" data-image-whitelisted="" class="CToWUd">
            </td>
            <td style="text-align: center;">
                <p style="font-family:'Open Sans','Arial',sans-serif;margin:0px;margin-bottom:0px;margin-right:35px;padding:18px;font-size:24px;text-align:center;color:#ffffff;font-weight:bold">
                    Comprovante de pagamento
                </p>
            </td>
        </tr>
        </tbody>
    </table>
    <br>
    <table width="95%" border="0" cellpadding="0" cellspacing="0" style="max-width:720px;min-width:375px" align="center">
        <tbody>
        <tr>
            <td>
                <table width="100%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="border-bottom:2px solid #3bc3e7;padding:15px;border-radius:3px" align="center">
                    <tbody>
                    <tr>
                        <td style="width:100%;padding:5px">

                            <p style="font-family:'Open Sans','Arial',sans-serif;text-align:center;font-size:16px;line-height:23px;color:#666;max-width:600px;width:100%;margin-top:25px">
                                Olá, <?php echo $cliente->nomeCliente;?>!<br><br>
                                O pagamento da conta abaixo foi realizado com sucesso.<br><br>
                            </p>

                            <p style="font-family:'Open Sans','Arial',sans-serif;text-align:left;font-size:17px;text-transform:uppercase;font-weight:bolder;line-height:23px;color:#3bc3e7;max-width:600px;width:100%;margin:0px;padding:0 15px">
                                DADOS DA CONTA:
                            </p>
                            <p style="font-family:'Open Sans','Arial',sans-serif;text-align:left;font-size:16px;line-height:23px;color:#666;max-width:600px;width:100%;padding:15px;margin:0">
                                Data de vencimento: <strong><?php echo date(('d/m/Y'),strtotime($parcela->dtVencimento));?></strong><br>
                                Data de pagamento: <strong><?php echo date(('d/m/Y'),strtotime($parcela->dtUltimoPagamento));?></strong><br>
                                Valor da conta: <strong>R$ <?php echo $this->site_model->formatarValorMonetario($parcela->valorVencimento);?></strong><br>
                                Valor pago: <strong>R$ <?php echo $this->site_model->formatarValorMonetario($pagamento->valor);?></strong><br>
                                Condições de pagamento: <strong><?php echo $condicaoPagamento->nome;?></strong><br>
                                <?php
                                    $receita = $receita->nome;
                                    if ($parcela->linkpagamento_id != null ) $receita = $this->linkpagamento_model->getById($parcela->linkpagamento_id)->descricao;
                                ?>
                                Descrição: <strong><?php echo $receita;?></strong><br><br>
                            </p>

                            <p style="font-family:'Open Sans','Arial',sans-serif;text-align:left;font-size:17px;font-weight:bolder;line-height:23px;color:#3bc3e7;max-width:600px;width:100%;margin:0px;padding:0 15px">
                                Número da transação: <?php echo $pagamento->transaction_id;?>
                                <br>
                            </p>
                            <br>

                            <p style="font-family:'Open Sans','Arial',sans-serif;font-size:14px;font-style:italic;padding-left:10px;padding-right:10px;margin:20px 5px 0 5px;text-align:center;color:#666;line-height:24px">
                                <?php echo $emitente->nome;?> afirma que a operação acima foi feita de acordo com os dados informados pelo cliente. Não se preocupe, todos os dados são sigilosos. Em caso de dúvidas, basta entrar em contato!
                            </p>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <table width="95%" border="0" cellpadding="0" cellspacing="0" style="max-width:720px;min-width:375px;text-align: center" align="center">
        <tbody>
        <tr>
            <td><br><br>
                <div style="font-size:11px;font-family:'Open Sans','Arial',sans-serif;color:#aaaaaa;line-height:16px;text-align:center"><strong>OnerBank</strong> - É a forma mais fácil e segura de fazer e receber pagamentos via Internet.<br> Copyright © 2019 OnerBank Todos os direitos reservados. <a href="https://onerbank.com.br/" target="_blank">https://onerbank.com.br/ OnerBank</a> <br></div>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL"></div>
</div>
</body>
</html>