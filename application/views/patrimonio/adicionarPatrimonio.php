<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Patrimônio <small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" >
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span2">
                            <label for="codigo">Nº Pat.<span class="required">*</span></label>
                            <input id="codigo" class="form-control" type="text" readonly name="codigo" required="required" value="<?php echo $numeroPatrimonio;?>"/>
                        </div>
                        <div class="span10">
                            <label for="descricao">Descrição<span class="required">*</span></label>
                            <input id="descricao" class="form-control" type="text" name="descricao" required="required" value=""/>
                        </div>
                    </div>
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span4">
                            <label for="grupoProduto_id">Grupo de produto<span class="required">*</span></label>
                            <select name="grupoProduto_id" class="form-control">
                                <option value="">Selecione um grupo de produto</option>
                                <?php
                                foreach($gruposProduto as $grupoproduto){
                                    echo '<option value="'.$grupoproduto->idGrupoProduto.'">'.$grupoproduto->nome.'</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="span4">
                            <label for="centro_custo_id">Unidade<span class="required">*</span></label>
                            <select name="centro_custo_id" class="form-control">
                                <option value="">Selecione uma unidade</option>
                                <?php
                                foreach($centroCustos as $centrocusto){
                                    echo '<option value="'.$centrocusto->idCentrocusto.'">'.$centrocusto->nome.'</option>';
                                } ?>
                            </select>
                        </div>

                        <div class="span4">
                            <label for="origemAquisicao">Origem<span class="required">*</span></label>
                            <select name="origemAquisicao" class="form-control">
                                <option value="1">Compra</option>
                                <option value="2">Doação</option>
                                <option value="3">Patrimonio Antigo</option>
                            </select>
                        </div>
                    </div>

                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span2">
                            <label for="marca">Marca</label>
                            <input id="marca" class="form-control" type="text" name="marca"  value=""/>
                        </div>

                        <div class="span2">
                            <label for="modelo">Modelo</label>
                            <input id="modelo" class="form-control" type="text" name="modelo"  value=""/>
                        </div>

                        <div class="span2">
                            <label for="numeroSerie">Nº Série</label>
                            <input id="numeroSerie" class="form-control" type="text" name="numeroSerie" value=""/>
                        </div>

                        <div class="span6">
                            <label for="localizacao">Localização</label>
                            <input id="localizacao" class="form-control" type="text" name="localizacao" value=""/>
                        </div>
                    </div>

                    <div class="span12" style="margin-left: 0">

                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#dadosAquisicao" data-toggle="tab">Aquisição</a></li>
                            <li><a href="#depreciacao" data-toggle="tab">Depreciação</a></li>
                            <li><a href="#observacao" data-toggle="tab">Observação</a></li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane active" id="dadosAquisicao">
                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span4">
                                        <label for="dataAquisicao">Data da aquisição</label>
                                        <input id="dataAquisicao" class="form-control" type="date" name="dataAquisicao" value=""/>
                                    </div>

                                    <div class="span4">
                                        <label for="numeroNFCompra">Nº NF de compra</label>
                                        <input id="numeroNFCompra" class="form-control" type="text" name="numeroNFCompra" value=""/>
                                    </div>

                                    <div class="span4">
                                        <label for="valorAquisicao">Valor Aquisição (R$)</label>
                                        <input id="valorAquisicao" class="form-control" type="text" name="valorAquisicao" value=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="depreciacao">
                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span4">
                                        <label for="tempoUso">TU - Tempo de Uso (Meses)</label>
                                        <input id="tempoUso" class="form-control" type="text" name="tempoUso" value=""/>
                                    </div>

                                    <div class="span4">
                                        <label for="tipoDepreciacao">Tipo de depreciação</label>
                                        <select name="tipoDepreciacao" id="tipoDepreciacao" class="form-control chzn">
                                            <option value="1">Depreciação Anual desconto %</option>
                                            <option value="2">Depreciação Mensal desconto %</option>
                                            <option value="3">Depreciação Gerencial</option>
                                            <option value="4">Depreciação Fiscal</option>
                                        </select>
                                    </div>

                                    <div class="span4">
                                        <label for="depreciacaoPercentual">% Depreciação</label>
                                        <input id="depreciacaoPercentual" class="form-control" type="text" name="depreciacaoPercentual" value=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="observacao">
                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                    <textarea class="form-control"  id="observacao" name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="form-actions" style="text-align: right;">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/patrimonio" id="" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();
    });
</script>



