<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Patrimônio <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="widget-content nopadding">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes do Patrimônio</a>
                        </li>
                        <!--<li id="tabFornecedores"><a href="#fornecedores" data-toggle="tab">Fornecedores</a></li>!-->
                        <li id="tabMauntencoesPreventivasAgendadas"><a href="#mauntencoesPreventivasAgendadas"
                                                                       data-toggle="tab">Manutenções Preventivas
                                Agendadas</a></li>
                        <li id="tabHistoricoManutencaoPreventivas"><a href="#historicoManutencoesPreventivas"
                                                                      data-toggle="tab">Histórico de Manutenções
                                Preventivas</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12">

                                <form action="<?php echo current_url(); ?>" id="formProduto" method="post"
                                      class="form-horizontal">
                                    <?php echo form_hidden('idProdutos', $result->idProdutos) ?>

                                    <div class="span12" style="padding: 1%; margin-left: 0">

                                        <div class="span2">
                                            <label for="codigo">Nº Pat.<span class="required">*</span></label>
                                            <input id="codigo" class="form-control" readonly type="text" name="codigo"
                                                   required="required" value="<?php echo $result->cProd; ?>"/>
                                        </div>

                                        <div class="span10">
                                            <label for="descricao">Descrição<span class="required">*</span></label>
                                            <input id="descricao" class="form-control" type="text" name="descricao"
                                                   required="required" value="<?php echo $result->descricao; ?>"/>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">

                                        <div class="span4">
                                            <label for="grupoProduto_id">Grupo de produto<span class="required">*</span></label>
                                            <select name="grupoProduto_id" class="form-control">
                                                <option value="">Selecione um grupo de produto</option>
                                                <?php
                                                foreach ($gruposProduto as $grupoproduto) {
                                                    ?>
                                                    <option <?php if ($result->grupoProduto_id == $grupoproduto->idGrupoProduto) echo 'selected="selected"'; ?>
                                                            value="<?php echo $grupoproduto->idGrupoProduto; ?>"><?php echo $grupoproduto->nome; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="span4">
                                            <label for="centro_custo_id">Unidade<span class="required">*</span></label>
                                            <select name="centro_custo_id" class="form-control">
                                                <option value="">Selecione uma unidade</option>
                                                <?php foreach ($centroCustos as $centrocusto) { ?>
                                                    <option <?php if ($result->centro_custo_id == $centrocusto->idCentrocusto) echo 'selected="selected"'; ?>
                                                            value="<?php echo $centrocusto->idCentrocusto; ?>"><?php echo $centrocusto->nome; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="span4">
                                            <label for="origemAquisicao">Origem<span class="required">*</span></label>
                                            <select name="origemAquisicao" class="form-control">
                                                <option value="1" <?php if ($result->origemAquisicao == 1) echo 'selected="selected"'; ?>>
                                                    Compra
                                                </option>
                                                <option value="2" <?php if ($result->origemAquisicao == 2) echo 'selected="selected"'; ?>>
                                                    Doação
                                                </option>
                                                <option value="3" <?php if ($result->origemAquisicao == 3) echo 'selected="selected"'; ?>>
                                                    Patrimonio Antigo
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span2">
                                            <label for="marca">Marca</label>
                                            <input id="marca" class="form-control" type="text" name="marca"
                                                   value="<?php echo $result->marca; ?>"/>
                                        </div>
                                        <div class="span2">
                                            <label for="modelo">Modelo</label>
                                            <input id="modelo" class="form-control" type="text" name="modelo"
                                                   value="<?php echo $result->modelo; ?>"/>
                                        </div>
                                        <div class="span2">
                                            <label for="numeroSerie">Nº Série</label>
                                            <input id="numeroSerie" class="form-control" type="text" name="numeroSerie"
                                                   value="<?php echo $result->numeroSerie; ?>"/>
                                        </div>
                                        <div class="span6">
                                            <label for="localizacao">Localização</label>
                                            <input id="localizacao" class="form-control" type="text" name="localizacao"
                                                   value="<?php echo $result->localizacao; ?>"/>
                                        </div>
                                    </div>
                                    <div class="span12" style="margin-left: 0">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#dadosAquisicao" data-toggle="tab">Aquisição</a>
                                            </li>
                                            <li><a href="#depreciacao" data-toggle="tab">Depreciação</a></li>
                                            <li><a href="#manutencaoPreventiva" data-toggle="tab">Manutenção
                                                    Preventiva</a></li>
                                            <li><a href="#observacao" data-toggle="tab">Observação</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="dadosAquisicao">
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <div class="span4">
                                                        <label for="dataAquisicao">Data da aquisição</label>
                                                        <input id="dataAquisicao" class="form-control" type="date"
                                                               name="dataAquisicao"
                                                               value="<?php echo $result->dataAquisicao; ?>"/>
                                                    </div>

                                                    <div class="span2">
                                                        <label for="numeroNFCompra">Nº NF de compra</label>
                                                        <input id="numeroNFCompra" class="form-control" type="text"
                                                               name="numeroNFCompra"
                                                               value="<?php echo $result->numeroNFCompra; ?>"/>
                                                    </div>

                                                    <div class="span2">
                                                        <label for="valorAquisicao">Valor Aquisição (R$)</label>
                                                        <input id="valorAquisicao" class="form-control" type="text"
                                                               name="valorAquisicao"
                                                               value="<?php echo $result->valorAquisicao; ?>"/>
                                                    </div>
                                                    <div class="span2">
                                                        <label for="valorDepreciado">Valor Depreciado (R$)</label>
                                                        <input id="valorDepreciado" disabled class="form-control"
                                                               type="text" name="valorDepreciado"
                                                               value="<?php echo $result->valorDepreciado; ?>"/>
                                                    </div>
                                                    <div class="span2">
                                                        <label for="valorAtualPosDepreciacao">Valor Atual do Bem
                                                            (R$)</label>
                                                        <input id="valorAtualPosDepreciacao" disabled
                                                               class="form-control" type="text"
                                                               name="valorAtualPosDepreciacao"
                                                               value="<?php echo $result->valorAtualPosDepreciacao; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="depreciacao">
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <div class="span4">
                                                        <label for="tempoUso">TU - Tempo de Uso (Meses)</label>
                                                        <input id="tempoUso" class="form-control" disabled type="text"
                                                               name="tempoUso"
                                                               value="<?php echo $result->tempoUso; ?>"/>
                                                    </div>
                                                    <div class="span4">
                                                        <label for="tipoDepreciacao">Tipo de depreciação</label>
                                                        <select name="tipoDepreciacao" id="tipoDepreciacao"
                                                                class="form-control chzn">
                                                            <option <?php if ($result->tipoDepreciacao == 2) echo 'selected="selected"'; ?>
                                                                    value="2">Depreciação Mensal %
                                                            </option>
                                                            <option <?php if ($result->tipoDepreciacao == 3) echo 'selected="selected"'; ?>
                                                                    value="3">Depreciação Gerencial
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="span4">
                                                        <label for="depreciacaoPercentual">% Depreciação</label>
                                                        <input id="depreciacaoPercentual" class="form-control"
                                                               type="text" name="depreciacaoPercentual"
                                                               value="<?php echo $result->depreciacaoPercentual; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <div class="span2">
                                                        <label for="valorResidual">Valor Residual (R$)</label>
                                                        <input id="valorResidual" class="form-control" disabled
                                                               type="text" name="valorResidual"
                                                               value="<?php echo $result->valorResidual; ?>"/>
                                                    </div>
                                                    <div class="span4">
                                                        <label for="valorDepreciar">Valor Depreciar (R$)</label>
                                                        <input id="valorDepreciar" class="form-control" type="text"
                                                               name="valorDepreciar"
                                                               value="<?php echo $result->valorDepreciar; ?>"/>
                                                    </div>
                                                    <div class="span2">
                                                        <label for="vidaUtil">Vida Útil (Meses)</label>
                                                        <input id="vidaUtil" class="form-control" type="text"
                                                               name="vidaUtil"
                                                               value="<?php echo $result->vidaUtil; ?>"/>
                                                    </div>
                                                    <div class="span4">
                                                        <label for="valorDepreciacao">Depreciação
                                                            Gerencial/Fiscal</label>
                                                        <input id="valorDepreciacao" class="form-control" disabled
                                                               type="text" name="valorDepreciacao"
                                                               value="<?php echo $result->valorDepreciacao; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="manutencaoPreventiva">
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <table class="table table-bordered" id="tblManutencaoPreventiva">
                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Serviço</th>
                                                            <th>Peça</th>
                                                            <th>Tipo de Manutenção</th>
                                                            <th>Período</th>
                                                            <th>Dt. Inical Preventiva</th>
                                                            <th>Situação</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $contador = 1;
                                                        foreach ($manutencoes as $manutencao) { ?>
                                                            <?php if ($manutencao->indice == $contador) { ?>
                                                                <tr>
                                                                    <td><?php echo $contador; ?></td>
                                                                    <td>
                                                                        <select name="servico_id[]"
                                                                                class="form-control">
                                                                            <option value="">Selecione um serviço
                                                                            </option>
                                                                            <?php foreach ($servicos as $servico) { ?>
                                                                                <option <?php if ($manutencao->servico_id == $servico->idServicos) echo 'selected="selected"'; ?>
                                                                                        value="<?php echo $servico->idServicos; ?>"><?php echo $servico->nome; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <select name="peca_id[]" class="form-control">
                                                                            <option value="">Selecione um serviço
                                                                            </option>
                                                                            <?php foreach ($pecas as $peca) { ?>
                                                                                <option <?php if ($manutencao->peca_id == $peca->idProdutos) echo 'selected="selected"'; ?>
                                                                                        value="<?php echo $peca->idProdutos; ?>"><?php echo $peca->descricao; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <select name="tipoManutencao[]"
                                                                                class="form-control">
                                                                            <option <?php if ($manutencao->tipoManutencao == 1) echo 'selected="selected"'; ?>
                                                                                    value="1">Preventiva em dias
                                                                            </option>
                                                                            <option <?php if ($manutencao->tipoManutencao == 2) echo 'selected="selected"'; ?>
                                                                                    value="2">Preventiva em meses
                                                                            </option>
                                                                            <option <?php if ($manutencao->tipoManutencao == 3) echo 'selected="selected"'; ?>
                                                                                    value="3">Preventiva em anos
                                                                            </option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <input id="periodo" class="form-control"
                                                                               type="text" name="periodo[]"
                                                                               value="<?php echo $manutencao->periodo; ?>"/>
                                                                    </td>
                                                                    <td>
                                                                        <input id="dataInicialPreventiva"
                                                                               class="form-control" type="date"
                                                                               name="dataInicialPreventiva[]"
                                                                               value="<?php echo $manutencao->dataInicialPreventiva; ?>"/>
                                                                    </td>
                                                                    <td>
                                                                        <select name="situacao[]" disabled
                                                                                class="form-control">
                                                                            <option title="ABERTA" <?php if ($manutencao->situacao == 1) echo 'selected="selected"'; ?>value="1">ABERTA</option>
                                                                            <option title="MANUTENÇÃO" <?php if ($manutencao->situacao == 2) echo 'selected="selected"'; ?>value="2">MANUTENÇÃO</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                            <?php $contador = $contador + 1;
                                                        } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="observacao">
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <textarea class="form-control" id="observacao" name="observacao"
                                                              cols="30"
                                                              rows="5"><?php echo $result->observacao; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions" style="text-align: right;">
                                        <div class="span12">
                                            <div class="span6 offset3">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                                    Alterar
                                                </button>
                                                <a href="<?php echo base_url() ?>index.php/patrimonio" id=""
                                                   class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="mauntencoesPreventivasAgendadas">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <div id="div_mauntencoesPreventivasAgendadas"></div>
                            </div>
                        </div>
                        <div class="tab-pane" id="historicoManutencoesPreventivas">
                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                <div id="div_historicoManutencoesPreventivas"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();

        $('#tabMauntencoesPreventivasAgendadas').click(function (event) {
            $('#div_mauntencoesPreventivasAgendadas').load('<?php echo base_url();?>/index.php/programacaomanutencaopreventiva/iframeManutencaoPreventivaAgendada/<?php echo $result->idProdutos;?>', function () {
            });
        });

        $('#tabHistoricoManutencaoPreventivas').click(function (event) {
            $('#div_historicoManutencoesPreventivas').load('<?php echo base_url();?>/index.php/programacaomanutencaopreventiva/iframeManutencaoPreventivaHistorico/<?php echo $result->idProdutos;?>', function () {
            });
        });
    });
</script>



