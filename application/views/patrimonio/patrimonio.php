<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-folder-open"></i> Patrimônio (<?php echo count($results); ?>)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aPatrimonio')) { ?>
                        <a class="btn btn-success" href="<?php echo base_url(); ?>patrimonio/adicionar"><i class="fa fa-plus"></i> Cadastrar Patrimônio</a>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>Cod</th>
                                    <th>Grupo</th>
                                    <th>Unidade</th>
                                    <th style="width: 50%">Nome</th>
                                    <th style="text-align: center;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    $unidade  = $r->unidade;
                                    echo '<tr>';
                                    echo '<td>' . $r->cProd . '</td>';
                                    echo '<td>' . $r->grupoproduto . '</td>';
                                    echo '<td>' . $r->centrocusto . '</td>';
                                    echo '<td>' . $r->descricao . '</td>';
                                    echo '<td style="text-align: center;">';
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
                                        echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/patrimonio/visualizar/' . $r->idProdutos . '"></a>  ';
                                    }
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eProduto')) {
                                        echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/patrimonio/editar/' . $r->idProdutos . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dProduto')) {
                                        echo '<a href="#modal-excluir" role="button" data-toggle="modal" produto="' . $r->idProdutos . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>';
                                    }

                                    echo '</td>';
                                    echo '</tr>';
                                } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Patrimônio</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este Patrimônio?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#buscar").keypress(function (event) {
            if (event.which == 13) {
                window.location.href = "<?php echo base_url() ?>index.php/patrimonio/buscar/" + $(this).val();
            }
        });

        $(document).on('click', 'a', function (event) {
            var produto = $(this).attr('produto');
            $('#idProduto').val(produto);
        });
    });

</script>