<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Venda <?php echo $result->idVendas;?></title>
    <base href="<?php echo base_url();?>"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>

    <style type="text/css" media="all">

        @import "<?php echo base_url(); ?>assets/helpers/bootstrap.min.css";
        @import "<?php echo base_url(); ?>assets/helpers/jquery-ui.css";
        @import "<?php echo base_url(); ?>assets/helpers/font-awesome.min.css";
        @import "<?php echo base_url(); ?>assets/helpers/datatables.css";
        @import "<?php echo base_url(); ?>assets/helpers/icheck/square/blue.css";
        @import "<?php echo base_url(); ?>assets/helpers/redactor.css";
        @import "<?php echo base_url(); ?>assets/helpers/bootstrap-fileupload.css";
        @import "<?php echo base_url(); ?>assets/helpers/ekko-lightbox.css";
        @import "<?php echo base_url(); ?>assets/helpers/perfect-scrollbar.css";
        @import "<?php echo base_url(); ?>assets/helpers/select2.css";
        @import "<?php echo base_url(); ?>assets/helpers/bootstrap-datatimepicker.css";
        @import "<?php echo base_url(); ?>assets/helpers/print.css";

        /*@font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            src: local('Lato Regular'), local('Lato-Regular'), url(../fonts/9k-RPmcnxYEPm8CNFsH2gg.woff) format('woff');
        }
        @font-face {
          font-family: 'Lato';
          font-style: normal;
          font-weight: 700;
          src: local('Lato Bold'), local('Lato-Bold'), url(../fonts/wkfQbvfT_02e2IWO3yYueQ.woff) format('woff');
        }
        body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif; }*/

        @font-face {
            font-family: 'FontAwesome';
            src: url('../fonts/fontawesome-webfont.eot?v=4.1.0');
            src: url('../fonts/fontawesome-webfont.eot?#iefix&v=4.1.0') format('embedded-opentype'), url('../fonts/fontawesome-webfont.woff?v=4.1.0') format('woff'), url('../fonts/fontawesome-webfont.ttf?v=4.1.0') format('truetype'), url('../fonts/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        /*@font-face {
          font-family: 'Ubuntu';
          font-style: normal;
          font-weight: 400;
          src: local('Ubuntu'), url(../fonts/_xyN3apAT_yRRDeqB3sPRg.woff) format('woff');
        }
        @font-face {
          font-family: 'Ubuntu';
          font-style: normal;
          font-weight: 700;
          src: local('Ubuntu Bold'), local('Ubuntu-Bold'), url(../fonts/0ihfXUL2emPh0ROJezvraD8E0i7KZn-EPnyo3HZu7kw.woff) format('woff');
        }*/

        /* With Extended Charset */
        @font-face {
            font-family: 'Ubuntu';
            font-style: normal;
            font-weight: 400;
            src: local('Ubuntu'), url(../fonts/mZSs29ggGoaqrCNB3kDfZQ.woff) format('woff');
        }
        @font-face {
            font-family: 'Ubuntu';
            font-style: normal;
            font-weight: 700;
            src: local('Ubuntu Bold'), local('Ubuntu-Bold'), url(../fonts/trnbTfqisuuhRVI3i45C5w.woff) format('woff');
        }
        body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { font-family: 'Ubuntu', sans-serif; }

        body {
            color: #000;
        }

        #wrapper {
            max-width: 480px;
            margin: 0 auto;
            padding-top: 20px;
        }

        .btn {
            border-radius: 0;
            margin-bottom: 5px;
        }

        h3 {
            margin: 5px 0;
        }

        @media print {
            .no-print {
                display: none;
            }

            #wrapper {
                max-width: 480px;
                width: 100%;
                min-width: 250px;
                margin: 0 auto;
            }

            .no-border {
                border: none !important;
            }

            .border-bottom {
                border-bottom: 1px solid #ddd !important;
            }
        }
    </style>

</head>

<body>

<div id="wrapper">
    <div id="receiptData">
        <div class="no-print">
        </div>
        <div id="receipt-data">
            <div class="text-center">
                <h3 style="text-transform:uppercase;"><?php echo $emitente->nome; ?></h3>
                <p>
                    <span><?php echo $emitente->cnpj; ?> </br>
                        <?php echo $emitente->rua.', nº:'.$emitente->numero.', '.$emitente->bairro.' - '.$emitente->cidade.' - '.$emitente->uf; ?> </span> </br>
                    <span> E-mail: <?php echo $emitente->email.' - Fone: '.$emitente->telefone; ?></span>
                </p>
            </div>
            <div class="col-sm-12 text-center">
                <h4 style="font-weight:bold;">Orçamento</h4>
            </div>
            <p>Orçamento Nº <?php echo $result->idVendas;?><br>
                Cliente: <?php echo $cliente->nomeCliente;?><br>
                Data do orçamento: <?php echo date("d/m/Y", strtotime($result->dataVenda));?>
                <?php if($result->referencia_espera != ''){?>
                    </br>OBS: <?=$result->referencia_espera;?>
                <?php }?>
            </p>
            <div style="clear:both;"></div>
            <table class="table table-striped table-condensed">
                <tbody>
                <?php
                $contador = 1;
                $sutTotal = 0;

                foreach ($produtos as $p) {
                    $sutTotal += $p->subTotal;
                    ?>
                    <tr>
                        <td colspan="2" class="no-border">
                            #<?php echo $contador;?>: <?php echo $p->descricao;?><span class="pull-right">Valor</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="no-border border-bottom"><?php echo $p->quantidade;?> x R$<?php echo number_format($p->subTotal/$p->quantidade,2,',','.')?></td>
                        <td class="no-border border-bottom text-right">R$<?php echo number_format($p->subTotal,2,',','.')?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                <tr>
                    <th>Total de compras</th>
                    <th class="text-right">R$<?php echo number_format($sutTotal,2,',','.')?></th>
                </tr>
                </tfoot>
            </table>
            <div class="well well-sm">
                Obrigado por orçar conosco! Aguardamos sempre de braços abertos!
            </div>
            <div class="col-sm-12 text-center">
                <h5 style="font-weight:bold;"><?php echo date("d/m/Y", strtotime(date('Y-m-d')));?> <?php echo date("H:i");?></h5>
            </div>
        </div>
    </div>
    <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
        <hr>
        <span class="pull-right col-xs-12">
        <a href="javascript:window.print()" id="web_print" class="btn btn-block btn-primary"
           onClick="window.print();return false;">Impressão</a>
        </span>
        <div style="clear:both;"></div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">
    $(window).load(function () {
        //window.print();
    });
</script>
</body>
</html>
