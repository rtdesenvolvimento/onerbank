<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>PDV | <?php echo TITLE_SISTEMA;?></title>
    <base href=".">
    <meta http-equiv="cache-control" content="max-age=0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="pragma" content="no-cache">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/pdv/<?php echo PACOTE;?>/theme.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/pdv/<?php echo PACOTE;?>/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/pdv/<?php echo PACOTE;?>/posajax.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/pdv/<?php echo PACOTE;?>/print.css" type="text/css" media="print">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mensagem.css"/>

    <script>
        var BASE_URL            = '<?php echo base_url();?>';
        var USUARIO_LOGADO_ID   = <?php echo  $this->session->userdata('id');?>;
        var CURRENT_URL         = '<?php echo current_url();?>';
        var REF                 = '<?php echo $result->referencia_espera;?>';
        var VENDA               = '<?php echo $result->idVendas;?>';
        var FATURADA            = '<?php echo $result->faturado;?>';
        var VENDA_STATUS        = '<?php echo $result->status;?>';
        var GRUPO_SELECIONADO   = '<?php echo $grupo_id;?>';
        var CLIENTE_PADRAO      = '<?php echo $configuracao->cliente_padrao_id;?>';
        var PERMITE_ESTOQUE_NEGATIVO = <?php if ($configuracao->permiteEstoqueNegativo) echo 'true'; else echo 'false';?>;

        <?php if ($result->fiscal){?>
            var VENDA_EM_MODO_FISCAL = true;
        <?php } else {?>
            var VENDA_EM_MODO_FISCAL = false;
        <?php } ?>
    </script>

    <style type="text/css">

        body {
            font-size: 11px;
        }

        ::-webkit-scrollbar {
            width: 5px;
        }
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .estoque {
            display: inline;
            padding: .2em .6em .3em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
            background: #a94442;
            float: right;
        }

        .btn-lg, .btn-group-lg>.btn{
            padding: 10px 16px;
            font-size: 14px;
            line-height: 1.33;
            border-radius: 6px;
        }

        .sub-total {
            background: #ffc107;
            font-size: 25px;
            color: #428bca;
        }

        .totalizador {
            border-top: 1px solid #666;
            font-size: 18px;
            border-bottom: 1px solid #333;
            font-weight:bold;
            background:#333;
            color:#FFF;
        }
    </style>
</head>
<body style="" onload="initTimer();">

<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>

<div id="wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <a class="navbar-brand" href="<?php echo base_url();?>" tabindex="-1">
                <span class="logo">
                    <span class="pos-logo-lg">
                        <?php if ($result->fiscal){?>
                            <img src="<?= base_url()?>/assets/img/nfce.png" width="4%">
                        <?php }?>
                        <?php echo 'PDV - '.NOME_SISTEMA.' - Pedido #'.$result->idVendas. ($result->referencia_espera != null ? ' ('.$result->referencia_espera.')' : '') ?>
                    </span>
                    <span class="pos-logo-sm">PDV</span>
                </span>
            </a>
            <div class="header-nav">

                <ul class="nav navbar-nav pull-right">

                    <!--
                    <li class="dropdown">
                        <a class="btn blightOrange pos-tip" id="opened_bills" title="" data-placement="bottom" data-html="true" href="" data-toggle="ajax" data-original-title="&lt;span&gt;Vendas Suspendidas&lt;/span&gt;" tabindex="-1">
                            <i class="fa fa-th"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="btn bdarkGreen pos-tip" id="register_details" title="" data-placement="bottom" data-html="true" href="" data-toggle="modal" data-target="#myModal" data-original-title="&lt;span&gt;Detalhes do Registro&lt;/span&gt;" tabindex="-1">
                            <i class="fa fa-check-circle"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="btn borange pos-tip" id="close_register" title="" data-placement="bottom" data-html="true" href="" data-toggle="modal" data-target="#myModal" data-original-title="&lt;span&gt;Fechar Caixa&lt;/span&gt;" tabindex="-1">
                            <i class="fa fa-times-circle"></i>
                        </a>
                    </li>

                    <li class="dropdown">
                        <a class="btn bdarkGreen pos-tip" id="today_profit" title="" data-placement="bottom" data-html="true" href="" data-toggle="modal" data-target="#myModal" data-original-title="&lt;span&gt;Lucro do Dia&lt;/span&gt;" tabindex="-1">
                            <i class="fa fa-hourglass-half"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="btn bdarkGreen pos-tip" id="today_sale" title="" data-placement="bottom" data-html="true" href=" data-toggle="modal" data-target="#myModal" data-original-title="&lt;span&gt;Vendas de Hoje&lt;/span&gt;" tabindex="-1">
                            <i class="fa fa-heart"></i>
                        </a>
                    </li>
                    !-->
                </ul>

                <ul class="nav navbar-nav pull-right" style="background: #428bca;">
                    <li class="dropdown">
                        <a style="cursor: default;color: #ffffff;" tabindex="-1">
                            <?php echo date('d/m/Y');?>&nbsp;&nbsp;&nbsp;
                            <span id="display_time"><?php echo date('H:i:s');?></span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown hidden-sm">
                        <a style="cursor: pointer;color: #ffffff;"  title="" data-placement="bottom" id="atalhos" href="#" data-toggle="modal" data-target="#sckModal" data-original-title="Atalhos" tabindex="-1">
                            <span>Atalhos (Ctrl+A)</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right" style="display: none;">
                    <li class="dropdown">
                        <a style="cursor: pointer;color: #ffffff;"  href="javascript:window.open('<?php site_url()?>pdv', 'principal', 'status=no,location=no, toolbar=no, menubar=no, directories=no,copyhistory=no, fullscreen=1, scrolling=auto');"><span>Nova Janela</span></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a style="cursor: pointer;color: #ffffff;" tabindex="-1" id="expandir_tela"><span>Expandir (F11)</span></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown hidden-sm">
                        <a style="cursor: pointer;color: #ffffff;"  title="" data-placement="bottom" href="#" data-toggle="modal" id="btconsultaEstoque" data-target="#consultaEstoque" data-original-title="Consulta de estoque" tabindex="-1">
                            <span>Consulta Item (F7)</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right" id="ulPedidosEmEspera">
                    <li class="dropdown hidden-sm">
                        <a style="cursor: pointer;color: #ffffff;"  title="" id="btPedidosEmEspera" tabindex="-1">
                            <span>Pedidos em Espera</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right" style="display: none;" id="voltarItens">
                    <li class="dropdown hidden-sm">
                        <a style="cursor: pointer;color: #ffffff;"  title="" id="btVoltarItens" tabindex="-1">
                            <span><< Voltar para Itens</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right" style="display: none;">
                    <li class="dropdown hidden-sm">
                        <a style="cursor: pointer;color: #ffffff;"  title="" id="btNovoPedido" tabindex="-1">
                            <span>Novo Pedido</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div id="content">
        <div class="c1">
            <div class="pos">
                <div id="pos">
                    <form action="#"  data-toggle="validator" role="form" id="pos-sale-form" method="post" accept-charset="utf-8">
                        <input type="hidden" name="vendas_id"  id="vendas_id" value="<?php echo $result->idVendas;?>" style="display:none;">
                        <div id="leftdiv">
                            <div id="printhead">
                                <h4 style="text-transform:uppercase;"><?php echo NOME_SISTEMA;?></h4>
                                <h5 style="text-transform:uppercase;">Lista de Pedidos</h5>Data <?php echo date('d/m/Y');?> <?php echo date('H:i');?>
                            </div>
                            <div id="left-top">
                                <div style="position: absolute; left:-9999px;"><input type="text" name="test" value="" id="test" class="kb-pad">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">

                                        <?php
                                        echo form_input('clientes_id', (isset($_POST['clientes_id']) ? $_POST['clientes_id'] : ""), 'id="clientes_id" required="required" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                        <div class="input-group-addon no-print" style="padding: 2px 8px; border-left: 0;">
                                            <a href="#editarCliente"  data-toggle="modal" id="editCliente" class="external" tabindex="-1">
                                                <i class="fa fa-pencil" id="addIcon" style="font-size: 1.2em;"></i>
                                            </a>
                                        </div>
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <a href="#cadClientes"  data-toggle="modal" id="adicionarCliente" class="external" tabindex="-1">
                                                <i class="fa fa-plus-circle" id="addIcon" style="font-size: 1.5em;"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="no-print">
                                    <div class="form-group">
                                        <select name="warehouse" disabled id="poswarehouse" class="form-control pos-input-tip" data-placeholder="Selecionar Depósito" required="required" style="width:100%;" >
                                            <option value=""><?php echo $filial->nome;?></option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="ui">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <input type="number" value="1" class="form-control pos-tip ui-autocomplete-input" id="qtdItemAdicionar" data-placement="top"  autocomplete="off" tabindex="1">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="add_item" value="" class="form-control pos-tip ui-autocomplete-input" id="add_item" data-placement="top" data-trigger="focus" placeholder="Scanear/Procurar Produto pelo Nome/Código" title="" autocomplete="off" data-original-title="Por favor, comece a digitar o código/nome para receber sugestões ou apenas scaneie um Cód. de Barra " tabindex="1">

                                                        <div class="input-group-addon" style="padding: 2px 8px;"><i class="fa fa-barcode" id="addIcon" style="font-size: 1.5em;"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both;"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="print">
                                <div id="left-middle" style="height: 193px; min-height: 278px;">
                                    <div id="product-list" class="ps-container" style="height: 188px; min-height: 278px;overflow: auto;">
                                        <table class="table items table-striped table-bordered table-condensed table-hover" id="posTable" style="margin-bottom: 0px; padding: 0px;">
                                            <thead class="tableFloatingHeaderOriginal">
                                            <tr>
                                                <th width="40%">Produto</th>
                                                <th width="15%">Preço</th>
                                                <th width="15%">Qtd.</th>
                                                <th width="20%">Sub-Total</th>
                                                <th style="width: 5%; text-align: center;">
                                                    <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <thead class="tableFloatingHeader" style="display: none; opacity: 0;">
                                            <tr>
                                                <th width="40%">Produto</th>
                                                <th width="15%">Preço</th>
                                                <th width="15%">Qtd.</th>
                                                <th width="20%">Sub-Total</th>
                                                <th style="width: 5%; text-align: center;">
                                                    <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $totalPagar = 0;
                                            $totalItens = 0;
                                            foreach ($produtospdv as $produtopdv) {
                                                $totalPagar = $totalPagar + $produtopdv->subTotal;
                                                $totalItens = $totalItens + $produtopdv->quantidade;
                                                ?>
                                                <tr>
                                                    <td>
                                                        <span class="sname"><?php echo $produtopdv->descricao;?></span>
                                                    </td>
                                                    <td class="text-right">
                                                        <span class="text-right sprice"><?php echo $produtopdv->valorVenda;?></span>
                                                    </td>
                                                    <td>
                                                        <input class="form-control input-sm kb-pad text-center rquantity" minlength="1" min="1" onchange="atualizarProduto(this);" valorVenda="<?php echo $produtopdv->valorVenda;?>" id="idItens_<?php echo $produtopdv->idItens;?>" iditens="<?php echo $produtopdv->idItens;?>" name="quantidade" type="number" value="<?php echo $produtopdv->quantidade;?>" onfocus="this.select();">
                                                    </td>
                                                    <td class="text-right">
                                                        <span class="text-right ssubtotal"><?php echo $produtopdv->subTotal;?></span>
                                                    </td>
                                                    <td class="text-center" iditens="<?php echo $produtopdv->idItens;?>">
                                                        <i class="fa fa-times tip pointer posdel" iditens="<?php echo $produtopdv->idItens;?>" title="Remove" style="cursor:pointer;"></i>
                                                    </td>
                                                </tr>
                                             <?php }?>
                                            </tbody>
                                        </table>
                                        <div style="clear:both;"></div>
                                        <div class="ps-scrollbar-x-rail" style="width: 421px; display: none; left: 0px; bottom: 3px;">
                                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; height: 277px; display: none; right: 3px;">
                                            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                                <div id="left-bottom">
                                    <table id="totalTable" style="width:100%; float:right; padding:5px; color:#000; background: #FFF;">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 5px 10px;border-top: 1px solid #DDD;">Total de Itens</td>
                                            <td></td>
                                            <td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;border-top: 1px solid #DDD;">
                                                <span id="titems"><?php echo $totalItens;?></span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="totalizador fechamento" style="padding: 15px 10px;"  colspan="2">
                                                <span id="lbTotalizador">Total</span>
                                            </td>
                                            <td class="text-right totalizador fechamento" style="padding: 15px 10px;" colspan="2">
                                                <span id="total"><?php echo  'R$ ' .number_format($totalPagar, 2, ',', '.');?></span>
                                            </td>
                                        </tr>
                                        <tr id="exibir-atalhos-pagamento" style="display: none;">
                                            <td colspan="3">
                                                Atalhos para formas de pagamento (F4)
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix"></div>
                                    <div id="botbuttons" class="col-xs-12 text-center">
                                        <input type="hidden" name="biller" id="biller" value="3">
                                        <div class="row">
                                            <div class="col-xs-4" style="padding: 0;">
                                                <div class="btn-group-vertical btn-block">
                                                    <button type="button" class="btn btn-danger btn-block btn-flat"  style="height:50px;" id="reset" tabindex="-1">
                                                        Cancelar (F4)
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-xs-4" style="padding: 0;">
                                                <div class="btn-group-vertical btn-block">
                                                    <button type="button" class="btn btn-primary btn-block" style="height:50px;" id="print_bill" tabindex="-1">
                                                        Orçamento (F9)
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-xs-4" style="padding: 0;">
                                                <button type="button"  class="btn btn-success btn-block" onclick="fechamentoRapidoSubTotal();" id="fechamento-click" style="height:50px;" tabindex="-1">Fechamento (F8)</button>
                                                <button type="button"  class="btn btn-success btn-block" data-toggle="modal" data-target="#paymentModal" id="payment" style="height:50px;display: none;" tabindex="-1"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both; height:5px;"></div>
                                    <div id="num">
                                        <div id="icon"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--PAGAMENTOS !-->
                        <div class="modal fade in" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" tabindex="-1"><span aria-hidden="true"><i class="fa fa-2x">×</i></span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title" id="payModalLabel">Finalizar Venda PDV <div id="divAguardandoPagamento" style="float: right;margin-right: 280px;color: #0044cc;"><input type="checkbox" id="aguardandoPagamento">  Aguardando pagamento <img src="<?php echo base_url()?>assets/js/images/busy.gif" style="display: none;" id="imgAguardandoPagamento" /></div></h4>

                                    </div>
                                    <div class="modal-body" id="payment_content">
                                        <div class="row">
                                            <div class="col-md-10 col-sm-9">

                                                <div class="row">
                                                    <div class="col-sm-12" style="text-align: center;display: none;">
                                                        <label for="tipoPagamento">ESCOLHA A FORMA DE PAGAMENTO</label>
                                                        <select name="tipoPagamento" class="form-control" id="tipoPagamento" required="required">
                                                            <option value="1" selected="selected">Mesclar pagamento (F10)</option>
                                                            <option value="2"> Parcelamento (F2)</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6" style="display: none;">
                                                        <label for="biller">Vendedor</label>
                                                        <div class="select2-container form-control" id="s2id_posbiller"><a href="javascript:void(0)" class="select2-choice" tabindex="-1"> <span class="select2-chosen" id="select2-chosen-3"><?php echo $usuario->nome;?></span><abbr class="select2-search-choice-close"></abbr> <span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a><label for="s2id_autogen3" class="select2-offscreen"></label><input class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-3" id="s2id_autogen3">
                                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                                <div class="select2-search"><label for="s2id_autogen3_search" class="select2-offscreen"></label> <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list" aria-owns="select2-results-3" id="s2id_autogen3_search" placeholder=""></div>
                                                                <ul class="select2-results" role="listbox" id="select2-results-3"></ul>
                                                            </div>
                                                        </div>
                                                        <select name="biller" class="form-control" id="posbiller" required="required" tabindex="-1" title="" style="display: none;">
                                                            <option value="3" selected="selected"> <?php echo $usuario->nome;?> </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br/>
                                                <div id="payments">
                                                    <div class="payment" id="avista">
                                                        <div class="well well-sm well_1">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="amount_1">Dinheiro</label>
                                                                        <input name="amount" type="text" id="amount_1" required="required" class="pa form-control kb-pad1 amount money">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label for="acrescimo">Acrescimo</label>
                                                                        <div class="input-group">
                                                                            <input name="pAcrescimo" type="text" id="pAcrescimo" required="required" class="pa form-control kb-pad1">
                                                                            <input name="acrescimo" type="hidden" id="acrescimo">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label for="desconto">Desconto</label>
                                                                        <div class="input-group">
                                                                            <input name="pDesconto" type="text" id="pDesconto" required="required" class="pa form-control kb-pad1">
                                                                            <input name="desconto" type="hidden" id="desconto">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="cartaoCredito">Crédito a vista</label>
                                                                        <input name="cartaoCredito" type="text" id="cartaoCredito" required="required" class="pa form-control kb-pad1 amount money">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="cartaoDebito">Cartão débito</label>
                                                                        <input name="cartaoDebito" type="text" id="cartaoDebito" required="required" class="pa form-control kb-pad1 amount money">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6" style="display: none;">
                                                                    <div class="form-group">
                                                                        <label for="paid_by_1">Modo do Pagamento</label>
                                                                        <select name="formaPgto" id="formaPgto" style="width: 100%" class="select2-container form-control">
                                                                            <option value="DINHEIRO">DINHEIRO</option>
                                                                            <option value="DEPOSITO EM CONTA">DEPOSITO EM CONTA</option>
                                                                            <option value="CHEQUE">CHEQUE</option>
                                                                            <option value="CARTÃO DE DÉBITO">CARTÃO DE DÉBITO</option>
                                                                            <option value="CARTÃO DE CRÉDITO">CARTÃO DE CRÉDITO</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="payment" id="aprazo" style="display: none;">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <select name="parcelas" id="parcelas" style="width: 100%" class="select2-container form-control">
                                                                            <?php foreach ($condicoes as $condicao) {?>
                                                                                <option value="<?php echo $condicao->idCondicaoPagamento;?>"><?php echo $condicao->nome;?></option>
                                                                            <?php } ?>
                                                                        </select>

                                                                        <div class="input-group-addon" style="padding: 2px 8px;"><i class="fa fa-tags" style="font-size: 1.2em;"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon" style="padding: 2px 8px;"><i class="fa fa-barcode" style="font-size: 1.2em;"></i></div>
                                                                        <select name="tipoCobranca" id="tipoCobranca" style="width: 100%" class="select2-container form-control">
                                                                            <?php foreach ($tiposCobranca as $tipoCobranca) {?>
                                                                                <option value="<?php echo $tipoCobranca->idTipocobranca;?>"><?php echo $tipoCobranca->descricao;?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="well well-sm well_1 col-sm-12">
                                                            <div class="table-responsive" style="height: 137px;overflow: auto;">
                                                                <table id="tbParcelamento"
                                                                       class="table table-bordered table-hover table-striped"
                                                                       style="margin-bottom: 0;">
                                                                    <thead></thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="multi-payment"></div>
                                                <div style="clear:both;"></div>

                                                <div class="font16" style="text-align: center;">
                                                    <img src="<?php echo base_url(); ?>/assets/js/images/sucesso.gif" style="width: 40%;display: none;" id="imgPagamentoRealizadoComSucesso">
                                                    <table class="table table-bordered table-condensed table-striped" id="tbValoresPagamento" style="margin-bottom: 0;">
                                                        <tbody>
                                                        <tr>
                                                            <td width="25%" style="display: none;">Qtd. de Itens</td>
                                                            <td width="25%" style="display: none;" class="text-right"><span id="item_count"><?php echo $totalItens;?></span></td>
                                                            <td width="25%" style="text-align: left;font-weight: 100;"> TOTAL À PAGAR</td>
                                                            <td width="25%" class="text-right"><span id="twt"><?php echo  'R$ ' .number_format($totalPagar, 2, ',', '.');?></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;font-weight: 100;">TOTAL DE DESCONTO</td>
                                                            <td class="text-right"><span id="desconto_oferecido">0.00</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;font-weight: 100;">TOTAL DE ACRÉSCIMO</td>
                                                            <td class="text-right"><span id="acrescimo_oferecido">0.00</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;font-weight: 100;">SUB-TOTAL</td>
                                                            <td class="text-right"><span id="subTotal_oferecido">0.00</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td><td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;font-weight: 100;">VALOR PAGO</td>
                                                            <td class="text-right"><span id="total_paying">0.00</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;font-weight: 100;">VALOR À PAGAR</td>
                                                            <td class="text-right"><span id="falta_pagar">0.00</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;font-weight: 100;">TROCO</td>
                                                            <td class="text-right"><span id="balance">0.00</span>
                                                                <input type="hidden" id="troco" name="troco"/>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div>
                                                    Mesclar Pagamento (F10) <br/>
                                                    Parcelamento (F2)
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-3 text-center" id="divTrocoRapido">
                                                <br/><br/><br/><br/><br/>
                                                 <div class="btn-group btn-group-vertical">
                                                     <button type="button" class="btn btn-lg btn-primary" style="margin-bottom: 5px;background-color: #0c5f88;border-color: #0c5f88;" id="venda-espera" tabindex="-1">EM ESPERA</button>
                                                     <button type="button" class="btn btn-lg btn-info" style="margin-bottom: 5px;    background-color: #209b0a;border-color: #209b0a;" id="gerador-link" tabindex="-1">GERAR LINK</button>

                                                     <button class="btn btn-block btn-lg btn-primary" style="margin-bottom: 5px;    background-color: #555;border-color: #555;" type="button" id="submit-sale" tabindex="-1">FINALIZAR</button>

                                                     <button class="btn btn-block btn-lg btn-success" style="margin-bottom: 5px;    background-color: #555;border-color: #555; display: none;" type="button" id="imprimir-naofiscal" tabindex="-1">IMPRIRMIR (F9)</button>
                                                     <button class="btn btn-block btn-lg btn-primary" type="button" style="display: none;margin-bottom: 5px;    background-color: #1c00ff;border-color: #1c00ff;" id="submit-nfce" tabindex="-1">GERAR NFC-e</button>
                                                     <button class="btn btn-info btn-lg btn-primary" type="button" style="display: none;margin-bottom: 5px;    background-color: #222;border-color: #222;" id="recarregar" tabindex="-1"><span id="sNovaVenda">NOVA VENDA</span></button>


                                                     <table id="tbPagamentos" class="table table-bordered table-condensed table-striped" style="margin-bottom: 0;">
                                                         <tbody>
                                                         <tr>
                                                             <td>Dinheiro</td>
                                                             <td><span id="tbPagamentoDinheiro">R$0.00</span></td>
                                                         </tr>
                                                         <tr>
                                                             <td>Crédito</td>
                                                             <td><span id="tbPagamentoCredito">R$0.00</span></td>
                                                         </tr>
                                                         <tr>
                                                             <td>Débito</td>
                                                             <td><span id="tbPagamentoDebito">R$0.00</span></td>
                                                         </tr>
                                                         </tbody>
                                                     </table>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="control-group" style="background: #ddd;padding: 15px;font-size: 14px;text-align: center;font-weight: 700;cursor: pointer;display: none;" id="diGrupoLinkGerado" >
                                            <div class="controls">
                                                <div id="divLinkGerado"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--###################!-->
                    <!--#### PRODUTOS #####!-->
                    <!--###################!-->
                    <div id="cp">
                        <div id="cpinner">
                            <div class="quick-menu">
                                <div id="proContainer">

                                    <!-- PRODUTOS-->
                                    <div id="ajaxproducts" style="overflow: auto;"></div>
                                    <div id="ajaxpedidosespera" style="overflow: auto;display: none;"></div>
                                    <div style="clear:both;"></div>

                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>

                    <div style="clear:both;"></div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>

    <div class="rotate btn-cat-con">
        <button type="button" id="open-category" class="btn btn-primary open-category">Categorias</button>
    </div>

    <div id="category-slider">
        <div id="category-list">
            <?php foreach ($gruposproduto as $grupoproduto) {?>
                <button id="<?php echo $grupoproduto->idGrupoProduto;?>" type="button" value='<?php echo $grupoproduto->idGrupoProduto;?>' class="btn-prni category" >
                    <img src="<?php echo base_url().'/assets/arquivos/sem_foto.png'?>" style='width:60px;height:60px;' class='img-rounded img-thumbnail' />
                    <span><?php echo $grupoproduto->nome;?> </span>
                </button>
            <?php } ?>
        </div>
    </div>
</div>



<!-- MODAL CADASTRO / EDICAO DE CLIENTES  -->
<div class="modal fade in" id="cadClientes" tabindex="-1" role="dialog" aria-labelledby="editCliente" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="close" data-dismiss="modal" tabindex="-1"><span aria-hidden="true"><i class="fa fa-2x">×</i></span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title" id="editCliente">Adicionar / Editar Cliente</h4>
            </div>
            <form action="" data-toggle="validator" role="form" id="form-cadastro-cliente" method="post" accept-charset="utf-8" class="bv-form">
                <input type="hidden" name="vendaId" value="<?php echo $result->idVendas;?>" />
                <input type="hidden" id="clienteId" name="clienteId" value="" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipoPessoa">Tipo</label>
                                <select id="tipoPessoa" name="tipoPessoa" class="form-control" required="required">
                                    <option value="PF">Física</option>
                                    <option value="PJ">Jurídica</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group person">
                                <label for="documento">CPF/CNPJ</label>
                                <input type="text" name="documento" id="documento" required="required" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group person">
                                <label for="nomeCliente">Nome</label>
                                <input type="text" name="nomeCliente" id="nomeCliente" required="required" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="telefone">Telefone</label>
                                <input type="text" name="telefone" id="telefone" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="celular">Celular</label>
                                <input type="tel" name="celular" id="celular" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" name="email" id="email" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCadastroCiente" class="btn btn-success btn-block">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal EDIT CLIENTE DE CLIENTE -->
<div class="modal fade in" id="consultaEstoque" tabindex="-1" role="dialog" aria-labelledby="consultaEstoque" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 92%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="close" data-dismiss="modal" tabindex="-1"><span aria-hidden="true"><i class="fa fa-2x">×</i></span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title" id="editCliente">Adicionar / Editar Cliente</h4>
            </div>
            <div class="modal-body">
                <iframe id="iFrameConsultaEstoque" frameborder="0" style="width: 100%; height: 450px;"></iframe>
            </div>
        </div>
    </div>
</div>

<!-- Modal EDIT CLIENTE DE CLIENTE -->
<div class="modal fade in" id="editarCliente" tabindex="-1" role="dialog" aria-labelledby="editCliente" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 92%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="close" data-dismiss="modal" tabindex="-1"><span aria-hidden="true"><i class="fa fa-2x">×</i></span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title" id="editCliente">Adicionar / Editar Cliente</h4>
            </div>
            <div class="modal-body">
                <iframe id="iframeEditarCliente" frameborder="0" style="width: 100%; height: 450px;"></iframe>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="sckModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                <i class="fa fa-2x">&times;</i></span><span class="sr-only">Fechar</span>
                </button>
                <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                    <i class="fa fa-print"></i> Imprimir                </button>
                <h4 class="modal-title" id="mModalLabel">Teclas de Atalhos</h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <table class="table table-bordered table-striped table-condensed table-hover"
                       style="margin-bottom: 0px;">
                    <thead>
                    <tr>
                        <th>Teclas de Atalhos</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Ctrl+I</td>
                        <td>Adicionar Novo Cliente</td>
                    </tr>
                    <tr>
                        <td>Ctrl+X</td>
                        <td>Editar Cliente Selecionado</td>
                    </tr>
                    <tr>
                        <td>Ctrl+E</td>
                        <td>Consultar Cliente</td>
                    </tr>
                    <tr>
                        <td>Ctrl+P</td>
                        <td>Venda em Espera</td>
                    </tr>
                    <tr>
                        <td>F2</td>
                        <td>Retrocedor até o campo quantidade</td>
                    </tr>
                    <tr>
                        <td>F4</td>
                        <td>Cancelar Venda</td>
                    </tr>
                    <tr>
                        <td>F7</td>
                        <td>Consulta Item</td>
                    </tr>
                    <tr>
                        <td>F8</td>
                        <td>Subtotal da Venda e fechamento</td>
                    </tr>
                    <tr>
                        <td>F9</td>
                        <td>Imprimir Orçamento</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="sckModalFormasDePagamentoRapido" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                <i class="fa fa-2x">&times;</i></span><span class="sr-only">Fechar</span>
                </button>
                <h4 class="modal-title" id="mModalLabel">Teclas de Atalhos</h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <table class="table table-bordered table-striped table-condensed table-hover"
                       style="margin-bottom: 0px;">
                    <thead>
                    <tr>
                        <th>Teclas de Atalhos</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>F2</td>
                        <td>Parcelamentos</td>
                    </tr>
                    <tr>
                        <td>F6</td>
                        <td>Dinheiro</td>
                    </tr>
                    <tr>
                        <td>F7</td>
                        <td>Cartão de Crédito</td>
                    </tr>
                    <tr>
                        <td>F9</td>
                        <td>Cartão de Débito</td>
                    </tr>
                    <tr>
                        <td>F10</td>
                        <td>Mesclar pagamento</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/formata.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/plugins.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/parse-track-data.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/js/maskmoney.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pdv/pdv.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $("#documento").mask("999.999.999-99");

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        <?php if ($result->clientes_id) {?>
            preencherClientePadrao();
        <?php } else { ?>
            criarConsultaDeCliente();
        <?php } ?>

        $('#btnCadastroCiente').click(function() {

            let documento = $('#documento').val();
            let nomeCliente = $('#nomeCliente').val();

            if (documento === '') { alert('CPF/CNPJ é obrigatório'); $('#documento').focus(); return}
            if (nomeCliente === ''){ alert('Nome do cliente tem preenchimento obrigatório.'); $('#nomeCliente').focus();return}

            $.ajax({
                type: "POST",
                url: BASE_URL+"index.php/pdv/adicionarClientePDV",
                data: $('#form-cadastro-cliente').serialize(),
                dataType: 'json',
                success: function (cliente) {

                    $('#clientes_id').val(cliente.idClientes).select2({
                        minimumInputLength: 1,
                        data: [],
                        initSelection: function (element, callback) {
                            $.ajax({
                                type: "get", async: false,
                                url: BASE_URL+"index.php/clientes/getCliente/"+ $(element).val(),
                                dataType: "json",
                                success: function (data) {
                                    callback(data[0]);
                                }
                            });
                        },
                        ajax: {
                            url: BASE_URL+"index.php/clientes/suggestions",
                            dataType: 'json',
                            quietMillis: 15,
                            data: function (term, page) {
                                return {
                                    term: term,
                                    limit: 10
                                };
                            },
                            results: function (data, page) {
                                if (data.results != null) {
                                    return {results: data.results};
                                } else {
                                    return {results: [{id: '', text: 'No Match Found'}]};
                                }
                            }
                        }
                    }).on('change', function (e) {
                        $.ajax({
                            type: "POST",
                            url: BASE_URL+"index.php/pdv/atualizarClientePDV",
                            data: {
                                clientes_id: $('#clientes_id').val(),
                                idVenda: <?php echo $result->idVendas;?>
                            },
                            dataType: 'json',
                            success: function (data) {}
                        });
                        //this.value
                    }).on('select', function (e) {
                        debugger;
                    });

                    $('#cadClientes').modal('hide');
                },
                error: function (erro) {
                    alert(erro);
                }
            });
        });

        <?php if ($result->faturado == '1') {?>
            $('#sNovaVenda').html('Finalizar Venda');
            notificacaoDePagamento();
        <?php } ?>

        <?php if ($result->status == '2') {?>
            $('#sNovaVenda').html('Finalizar Venda');
            notificacaoDePagamento();
            $('#submit-nfce').hide();
        <?php } ?>
    });

    $('#editCliente').click(function (event) {
        event.preventDefault();
        var clientes_id = $('#clientes_id').val();
        if (clientes_id !== '') {
            $('#iframeEditarCliente').attr('src', '<?php echo base_url();?>index.php/clientes/editarIframe/'+clientes_id);
        } else {
            $('#iframeEditarCliente').attr('src', '<?php echo base_url();?>index.php/clientes/adicionarIframe');
        }
    });

    $('#btconsultaEstoque').click(function (event) {
        event.preventDefault();
        $('#iFrameConsultaEstoque').attr('src', '<?php echo base_url();?>index.php/consultaestoque/consultaEstoqueIFrame');
    });

    function showTimer() {
        var time = new Date();
        var hour = time.getHours();
        var minute = time.getMinutes();
        var second = time.getSeconds();

        if (hour < 10) hour = "0" + hour;
        if (minute < 10) minute = "0" + minute;
        if (second < 10) second = "0" + second;

        var st = hour + ":" + minute + ":" + second;
        document.getElementById("display_time").innerHTML = st;
    }

    function initTimer() {
        setInterval(showTimer, 1000);
    }

    function criarConsultaDeCliente() {
        $('#clientes_id').select2({
            minimumInputLength: 1,
            ajax: {
                url: BASE_URL+"index.php/clientes/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: BASE_URL+"index.php/clientes/getCliente/"+ $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                results: function (data, page) {
                    if(data.results != null) {
                        return { results: data.results };
                    } else {
                        return { results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        }).on('change', function (e) {depoisPreencherCliente(e);})
            .on('select', function (e) {});
    }

    function preencherClientePadrao() {
        $('#clientes_id').val('<?php echo $result->clientes_id;?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: BASE_URL+"index.php/clientes/getCliente/"+ $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: BASE_URL+"index.php/clientes/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        }).on('change', function (e) {depoisPreencherCliente(e);})
            .on('select', function (e) {});
    }

    function depoisPreencherCliente(e) {

        if (e.added.inativo === '1') {
            bootbox.alert('<b style="color: #F45750;">ATENÇÃO! Cliente bloqueado:</b><br/>'+e.added.bloqueio , function (e) {  setTimeout( function () {
                location.reload();
            }, 800) });
        } else {
            $.ajax({
                type: "POST",
                url: BASE_URL+"index.php/pdv/atualizarClientePDV",
                data: {
                    clientes_id: $('#clientes_id').val(),
                    idVenda: <?php echo $result->idVendas;?>
                },
                dataType: 'json',
                success: function (data) {

                }
            });
        }
    }
</script>

 </body>
</html>