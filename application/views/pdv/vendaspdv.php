<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-search"></i> Filtros</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="get" id="frmLancamentos">
                    <div class="col-md-12 well">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label for="">Data da Venda de:</label>
                                <input type="date" name="dataInicial" required="required" class="form-control" value="<?php echo $dataInicial ?>"/>
                            </div>
                            <div class="col-md-6">
                                <label for="">até:</label>
                                <input type="date" name="dataFinal" required="required" class="form-control" value="<?php echo $dataFinal ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="span1" style="text-align: right;">
                        <button type="submit" id="btnsubmitform" class="span12 btn btn-primary"><i class="fa fa-search"></i> Pesquisar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-cart-plus"></i> Vendas realizadas no PDV</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th style="text-align: center;">Emissão</th>
                                    <th style="text-align: center;">Total</th>
                                    <th style="text-align: center;">Acre.</th>
                                    <th style="text-align: center;">Desc.</th>
                                    <th style="text-align: center;">SubTotal</th>
                                    <th style="text-align: center;">Dinheiro</th>
                                    <th style="text-align: center;">Crédito</th>
                                    <th style="text-align: center;">Débito</th>
                                    <th style="text-align: center;">Parc.</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $totalDinheiro = 0;
                                $totalCartaoCredito = 0;
                                $totalCartaoDebito = 0;
                                $totalParcelado = 0;

                                foreach ($results as $r) {
                                    $dataVenda = date(('d/m/Y'),strtotime($r->dataVenda));

                                    $desconto = $r->desconto;
                                    $acrescimo = $r->acrescimo;
                                    $troco = $r->troco;

                                    $totalDinheiro = $totalDinheiro + $r->dinheiro - $troco;
                                    $totalCartaoCredito = $totalCartaoCredito + $r->cartao_credito;
                                    $totalCartaoDebito =  $totalCartaoDebito + $r->cartao_debito;
                                    $totalParcelado = $totalParcelado + $r->valorParcelado;


                                    if ($desconto == '') $desconto = 0;

                                    echo '<tr>';
                                    echo '    <td>'.$r->idVendas.'</td>';
                                    echo '    <td style="width: 60%"><a href="'.base_url().'index.php/clientes/profile/'.$r->idClientes.'">'.$r->nomeCliente.'</a></td>';
                                    echo '    <td style="text-align: center;">'.$dataVenda.'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $r->valorTotal, 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $acrescimo , 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $desconto , 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $r->valorTotal - $desconto + $acrescimo, 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $r->dinheiro - $troco, 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $r->cartao_credito, 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $r->cartao_debito, 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">'.'R$' .number_format( $r->valorParcelado, 2, ',', '.') .'</td>';
                                    echo '    <td style="text-align: center;">';
                                    echo '      <a style="margin-right: 1%" href="'.base_url().'index.php/pdv/impressao/'.$r->idVendas.'" target="_blank"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-print"> </i> </button></a>';
                                    echo '    </td>';
                                    echo '</tr>';
                                }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="10" style="text-align: right;"> Dinheiro</td>
                                    <td colspan="2" style="text-align: right;">R$ <?php echo number_format( $totalDinheiro, 2, ',', '.');?></td>
                                </tr>
                                <tr>
                                    <td colspan="10" style="text-align: right;"> Cartão de Crédito</td>
                                    <td colspan="2" style="text-align: right;">R$ <?php echo number_format( $totalCartaoCredito, 2, ',', '.');?></td>
                                </tr>
                                <tr>
                                    <td colspan="10" style="text-align: right;"> Cartão de Débito</td>
                                    <td colspan="2" style="text-align: right;">R$ <?php echo number_format( $totalCartaoDebito, 2, ',', '.');?></td>
                                </tr>
                                <tr>
                                    <td colspan="10" style="text-align: right;"> Parcelado</td>
                                    <td colspan="2" style="text-align: right;">R$ <?php echo number_format( $totalParcelado, 2, ',', '.');?></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/vendas/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Venda</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idVenda" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir esta Venda?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', 'a', function(event) {
            var venda = $(this).attr('venda');
            $('#idVenda').val(venda);
        });
    });

</script>