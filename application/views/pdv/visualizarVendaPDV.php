<?php $totalProdutos = 0;?>

<?php
/*
SystemBoys - Garotos de Sistema
http://www.systemboys.com.br
Marcos Aurélio R. Silva
systemboy_marcos@hotmail.com
*/



$firma= $emitente->nome;
$cnpj_cgc_firma= $emitente->cnpj;
$logomarca= $emitente->url_logo;

$nome_devedor=$result->nomeCliente;
$cpf_cnpj_devedor=$result->documento;


$endereco= $cliente->rua.' '.$cliente->numero.','.$cliente->bairro.' - '.$cliente->cidade.'/'.$cliente->estado;

$referente='referente a manuten&#231;&#227;o, conserto e (ou) venda de pe&#231;as';
$final='estando quitado o d&#233;bito referido a esta data';
$dia_pagamento= date("d");
$mes_pagamento= date("m");
$ano_pagamento= date("y");
$cidade=$emitente->cidade;
$estado=$emitente->uf;
?>

<style type="text/css">
    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 11px;
        color: #666;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>

<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Venda PDV</h5>
                <div class="buttons">

                    <?php if ($result->nfcegerada == 1){?>
                        <a id="imprimirDANFE" class="btn btn-mini btn-warning" onclick="imprimirDANFE('<?php echo $result->nNF;?>');" href=""><i class="icon-print icon-white"></i> Imprimir NFC-e</a>
                    <?php } ?>

                    <a id="imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i> Imprimir</a>
                </div>
            </div>
            <div class="widget-content" id="printOs">
                <div class="invoice-content">
                    <table class="table" style="margin-top: 0">
                        <tbody>

                        <?php if($emitente == null) {?>
                            <tr>
                                <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<</td>
                            </tr>
                        <?php } else {?>

                            <tr>
                                <td style="width: 10%"><img src=" <?php echo $emitente->url_logo; ?> "></td>
                                <td> <span style="font-size: 20px; "> <?php echo $emitente->nome; ?></span> </br><span><?php echo $emitente->cnpj; ?> </br> <?php echo $emitente->rua.', nº:'.$emitente->numero.', '.$emitente->bairro.' - '.$emitente->cidade.' - '.$emitente->uf; ?> </span> </br> <span> E-mail: <?php echo $emitente->email.' - Fone: '.$emitente->telefone; ?></span></td>
                                <td style="width: 18%; text-align: center">#Venda PDV: <span ><?php echo $result->idVendas?></span></br> </br> <span>Emissão: <?php echo date('d/m/Y');?></span></td>
                            </tr>

                        <?php } ?>
                        </tbody>
                    </table>

                    <table class="table" style="margin-top: 0">
                        <tbody>
                        <tr>
                            <td style="width: 50%; padding-left: 0">
                                <ul>
                                    <li>
                                        <span><h5>Cliente</h5>
                                        <span><?php echo $result->nomeCliente?></span><br/>
                                        <span><?php echo $cliente->rua?>, <?php echo $cliente->numero?>, <?php echo $cliente->bairro?></span><br/>
                                        <span><?php echo $cliente->cidade?> - <?php echo $cliente->estado?></span>
                                    </li>
                                </ul>
                            </td>
                            <td style="width: 50%; padding-left: 0">
                                <ul>
                                    <li>
                                        <span><h5>Vendedor</h5></span>
                                        <span><?php echo $result->nome?></span> <br/>
                                        <span>Telefone: <?php echo $result->telefone?></span><br/>
                                        <span>Email: <?php echo $result->email?></span>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <div style="margin-top: 0; padding-top: 0">
                        <?php if($produtos != null){?>

                            <table class="table table-bordered table-condensed" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th width="55%" style="font-size: 15px">Produto</th>
                                    <th width="5%" style="font-size: 15px">Quantidade</th>
                                    <th width="20%" style="font-size: 15px;text-align: right;">Sub-total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($produtos as $p) {

                                    $totalProdutos = $totalProdutos + $p->subTotal;
                                    echo '<tr>';
                                    echo '<td>'.$p->descricao.'</td>';
                                    echo '<td> <center> '.$p->quantidade.' </center> </td>';

                                    echo '<td style="text-align: right;">R$ '.number_format($p->subTotal,2,',','.').'</td>';
                                    echo '</tr>';
                                }?>

                                <tr>
                                    <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                    <td style="text-align: right;"><strong>R$ <?php echo number_format($totalProdutos,2,',','.');?></strong></td>
                                </tr>
                                </tbody>
                            </table>
                        <?php }?>

                        <h4 style="text-align: right">SubTotal: R$ <?php echo number_format($totalProdutos,2,',','.');?></h4>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>

<script type="text/javascript">

    var link_nfce = '';

    $(document).ready(function(){
        $("#imprimir").click(function(){

            //pega o Html da DIV
            var divElements = document.getElementById('printOs').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;
        });

    });

    function gerarNFCE() {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>index.php/pdv/gerarNFCE/<?php echo $result->idVendas;?>",
            dataType: "json",
            success: function (nfce) {
                debugger;
                if (nfce.nNF !== ''){
                    link_nfce = '<div style="color: #fffff;font-size: 14px;"> <br/>Para editar e corrigir as informações da NFC-e clique no link a seguir <a href="<?php echo base_url(); ?>index.php/nfce/editar/'+nfce.nNF+'">NFC-e</a> </div>';
                    GeraNFe(nfce.nNF);
                }
            }
        });
    }

    function GeraNFe(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Aguarde Gerando XML...</h3>'});

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/gera_nfce.php?nNF="+valor,
            dataType: "json",
            success: function (data) {
                $.unblockUI();
                if (data.Status === 'Ok') {
                    AssinaNFE(valor);
                } else {
                    alert('Erro ao Exportar Arquivo.');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.responseText) {
                    var str = jqXHR.responseText;
                    var retorno = jQuery.parseJSON(str.substr(3));
                    if (retorno.Status === 'Ok') {
                        SituacaoNFeA3();
                    } else {
                        toastr.warning('Erro ao Exportar Arquivo.');
                    }
                }
            }
        });
    }

    function AssinaNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Assinando XML...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/assina_nfce.php",
            data: {
                arquivo: valor
            },
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    TransmiteNFE(valor);
                } else {
                    $('#dError-msg').html($(retorno).find("motivo").text()+link_nfce);
                    $('.error-msg').show();
                }
            }
        });
    }

    function TransmiteNFE(valor) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Enviando XML ao Sefaz...</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/envia_nfce.php?arquivo=" + valor,
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    RetornoNFE($(retorno).find("recibo").text(), valor);
                } else {
                    $('#dError-msg').html($(retorno).find("motivo").text()+link_nfce);
                    $('.error-msg').show();
                }
            }
        });
    }


    function RetornoNFE(valor, nct) {
        $.blockUI({message: '<h3><img src="<?php echo base_url(); ?>assets/js/images/busy.gif" /> Retornando Dados do Sefaz</h3>'});
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>emissores/v4/nfephp-master/geracaonfce/consulta_recibo.php?arquivo="+nct,
            dataType: "xml",
            success: function (retorno) {
                $.unblockUI();
                var status = $(retorno).find("status").text();
                if (status === 'OK') {
                    location.reload();
                    window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfce.php?nNFbd=' + nct + '', '',
                        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
                } else {
                    $('#dError-msg').html($(retorno).find("motivo").text()+link_nfce);
                    $('.error-msg').show();
                }
            }
        });
    }

    function imprimirDANFE(nNF) {
        window.open('<?php echo base_url(); ?>emissores/v4/sped-da-master/geracao/danfce.php?nNFbd=' + nNF + '', '',
            'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
    }
</script>