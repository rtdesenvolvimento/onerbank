<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Editar Pedido de compra</h5>
            </div>
            <div class="widget-content nopadding">


                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da Pedido</a></li>
                        <!--<li class="active" id="tabDetalhesDaNota"><a href="#tab1" data-toggle="tab">Detalhes da Nota</a></li>!-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divEditarVenda">

                                <form action="<?php echo current_url(); ?>" method="post" id="formPedido">
                                    <?php echo form_hidden('idPedido', $result->idPedido) ?>
                                    <h3>
                                        #Pedido de compra: <?php echo $result->idPedido ?> /
                                        <?php if($result->status == 'Aberto'){echo 'Aberto';} ?>
                                        <?php if($result->status == 'Em Orçamento Fornecedor'){echo 'Em Orçamento com Fornecedor';} ?>
                                        <?php if($result->status == 'Pendente de entrega'){echo 'Pendente de entrega';} ?>
                                        <?php if($result->status == 'Entregue'){echo 'Entregue';} ?>
                                        <?php if($result->status == 'Pedido pendente / entrega incompleta'){echo 'Pedido pendente / entrega incompleta';} ?>
                                        <?php if($result->status == 'Cancelado'){echo 'Cancelado';} ?>

                                    </h3>

                                    <div class="span12 well" style="padding: 1%; margin-left: 0">


                                        <div class="span6">
                                            <label for="fornecedor">Fornecedor<span class="required">*</span></label>
                                            <select name="fornecedor_id" class="span12" id="fornecedor_id" required="required">
                                                <option value="">--Selecione um fornecedor--</option>
                                                <?php foreach ($fornecedores as $fornecedor) {?>
                                                    <option <?php if ($result->fornecedor_id == $fornecedor->idFornecedor) echo 'selected="selected"'; ?> value="<?php echo $fornecedor->idFornecedor; ?>"><?php echo $fornecedor->nomeFornecedor; ?></option>
                                                <?php } ?>
                                            </select>
                                            <!--
                                            <input id="clientes_id" type="hidden" name="clientes_id"
                                                   value="<?php echo $result->clientes_id ?>"/>
                                                   !-->
                                            <input id="valorTotal" type="hidden" name="valorTotal" value=""/>
                                        </div>
                                        <div class="span6">
                                            <label for="tecnico">Comprador<span class="required">*</span></label>

                                            <!--
                                            <input id="usuarios_id" type="hidden" name="usuarios_id"
                                                   value="<?php echo $result->usuarios_id ?>"/>
                                                   !-->
                                            <div class="controls">
                                                <select name="usuarios_id" class="span12" id="usuarios_id" required="required">
                                                    <option value="">--Selecione um cliente--</option>
                                                    <?php foreach ($usuarios as $usuario) {?>
                                                        <option <?php if ($result->usuarios_id == $usuario->idUsuarios) echo 'selected="selected"'; ?> value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="span12" style="margin-left: 0">



                                            <div class="span6">
                                                <label for="dataPedido">Data Pedido</label>
                                                <input id="dataPedido" class="span12" required="required" type="date" name="dataPedido"
                                                       value="<?php echo $result->dataPedido;?>"/>
                                            </div>

                                            <div class="span6">
                                                <label for="previsaoEntrega">Data Previsão entrega</label>
                                                <input id="previsaoEntrega" class="span12" type="date" name="previsaoEntrega"
                                                       value="<?php echo $result->previsaoEntrega;?>"/>
                                            </div>
                                        </div>

                                        <div class="span12" style="margin-left: 0">

                                            <div class="span2">
                                                <label for="valorFrete">Valor do frete</label>
                                                <input id="valorFrete" class="span12" type="number" name="valorFrete"
                                                       value="<?php echo $result->valorFrete;?>"/>
                                            </div>

                                            <div class="span2">
                                                <label for="desconto">Desconto</label>
                                                <input id="desconto" class="span12" type="number" name="desconto"
                                                       value="<?php echo $result->desconto;?>"/>
                                            </div>

                                            <div class="span4">
                                                <label for="forma_pagamento_id">Forma de pagamento</label>
                                                <div class="controls">
                                                    <select name="forma_pagamento_id" class="span12" id="forma_pagamento_id">
                                                        <option value="">--Selecione uma forma de pagamento--</option>
                                                        <?php foreach ($formasPagamento as $formaPagamento) {?>
                                                            <option <?php if ($result->forma_pagamento_id == $formaPagamento->idFormaPagamento) echo 'selected="selected"'; ?> value="<?php echo $formaPagamento->idFormaPagamento; ?>"><?php echo $formaPagamento->nome; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="span4">
                                                <label for="condicao_pagamento_id">Condições</label>
                                                <div class="controls">
                                                    <select name="condicao_pagamento_id" class="span12" id="condicao_pagamento_id">
                                                        <option value="">--Selecione uma condição de pagamento--</option>
                                                        <?php foreach ($condicoesPagamento as $condicaoPagamento) {?>
                                                            <option <?php if ($result->condicao_pagamento_id == $condicaoPagamento->idCondicaoPagamento) echo 'selected="selected"'; ?> value="<?php echo $condicaoPagamento->idCondicaoPagamento; ?>"><?php echo $condicaoPagamento->nome; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="span12" style="padding: 1%; margin-left: 0">

                                            <div class="span8 offset2" style="text-align: center">

                                                <?php if ($result->status == 'Aberto') { ?>

                                                    <button class="btn btn-primary" id="btnContinuar"><i
                                                                class="icon-white icon-ok"></i> Alterar
                                                    </button>

                                                    <a href="<?php echo base_url() ?>index.php/pedido/emOrcamentoFornecedor/<?php echo $result->idPedido; ?>"
                                                       class="btn btn-success"><i class="icon-ok"></i>Orçamento Enviado
                                                    </a>
                                                <?php }?>

                                                <?php if ($result->status == 'Em Orçamento Fornecedor') { ?>

                                                    <button class="btn btn-primary" id="btnContinuar"><i
                                                                class="icon-white icon-ok"></i> Alterar
                                                    </button>

                                                    <a href="<?php echo base_url() ?>index.php/pedido/orcamentoConfirmado/<?php echo $result->idPedido; ?>"
                                                       class="btn btn-success" id="confirmarPedido"><i class="icon-ok"></i>Confirmar
                                                    </a>
                                                <?php }?>

                                                <?php if ($result->status == 'Pendente de entrega') { ?>

                                                    <button class="btn btn-primary" id="btnContinuar"><i
                                                                class="icon-white icon-ok"></i> Alterar
                                                    </button>

                                                    <a href="#modal-entregar" id="btn-entregar" role="button" data-toggle="modal"
                                                       class="btn btn-success"><i class="icon-ok"></i> Entregar
                                                        Pedido
                                                    </a>
                                                <?php }?>

                                                <?php if ($result->status == 'Entregue') { ?>
                                                    <a href="<?php echo base_url() ?>index.php/pedido/cancelar/<?php echo $result->idPedido; ?>"
                                                       class="btn btn-success"><i class="icon-ok"></i>Cancelar
                                                    </a>
                                                <?php }?>

                                                <a href="<?php echo base_url() ?>index.php/pedido/visualizar/<?php echo $result->idPedido; ?>"
                                                   class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar
                                                    Pedido
                                                </a>

                                                <a href="<?php echo base_url() ?>index.php/pedido" class="btn"><i
                                                        class="icon-arrow-left"></i> Voltar</a>
                                            </div>

                                        </div>
                                    </div>

                                </form>

                                <div class="span12 well" style="padding: 1%; margin-left: 0">

                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tablItens" data-toggle="tab">Produtos</a></li>
                                        <li><a href="#tab3" data-toggle="tab">Dados de entrega</a></li>
                                        <li><a href="#tab4" data-toggle="tab">Observações da compra aqui</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tablItens">
                                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                <form id="formProdutos"
                                                      action="<?php echo base_url(); ?>index.php/pedido/adicionarProduto"
                                                      method="post">
                                                    <div class="span6">
                                                        <input type="hidden" name="idPedido" id="idPedido"
                                                               value="<?php echo $result->idPedido ?>"/>
                                                        <input type="hidden" name="estoque" id="estoque" value=""/>
                                                        <input type="hidden" name="preco" id="preco" value=""/>

                                                        <div class="formBuscaGSA">
                                                            <label for="">Produto
                                                                <a href="#modal-consulta-estoque" id="addConsultaProduto" data-toggle="modal">
                                                                    <i id="pincliente" style="float: right;margin-right: 10px;" class="icon-bar-chart icon-white"></i></a>
                                                            </label>

                                                            <div class="controls">
                                                                <select name="idProduto" class="span12" id="idProduto" required="required">
                                                                    <option value="">--Selecione um produto--</option>
                                                                    <?php foreach ($produtosList as $produto) {?>
                                                                        <option value="<?php echo $produto->idProdutos; ?>"><?php echo $produto->descricao; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span2">
                                                        <label for="">Quantidade</label>
                                                        <input type="text" placeholder="Quantidade" required="required" id="quantidade"
                                                               name="quantidade" class="span12" value="1"/>
                                                    </div>

                                                    <div class="span2">
                                                        <label for="">Preço de compra</label>
                                                        <input type="text" placeholder="Preço de compra" required="required" id="custo" name="custo"
                                                               class="span12"/>
                                                    </div>

                                                    <div class="span2">
                                                        <label for="">&nbsp</label>
                                                        <?php if ($result->faturado == 0) { ?>
                                                            <button class="btn btn-success span12"
                                                                    id="btnAdicionarProduto"><i class="icon-white icon-plus"></i>
                                                                Adicionar
                                                            </button>
                                                        <?php }  ?>
                                                    </div>
                                                </form>

                                                <div class="span12" id="divProdutos" style="margin-left: 0">
                                                    <table class="table table-bordered" id="tblProdutos">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: left;">Produto</th st>
                                                            <th style="text-align: right;">Quantidade</th>
                                                            <th style="text-align: right;">Preço de compra</th>
                                                            <th style="text-align: right;">SubTotal</th>
                                                            <th style="text-align: center;">Ações</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $total = 0;
                                                        $custo = 0;

                                                        foreach ($produtos as $p) {

                                                            $total = $total + $p->subTotal;
                                                            $custo = $custo + $p->custo;

                                                            echo '<tr>';
                                                            echo '<td style="text-align: left;">' . $p->descricao . '</td>';
                                                            echo '<td style="text-align: right;">' . $p->quantidade . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->custo, 2, ',', '.') . '</td>';

                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                                            if ($result->faturado == 0) {
                                                                echo '<td  style="text-align: center;"><a href="" idAcao="' . $p->idItens . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                                            }
                                                            echo '</tr>';
                                                        } ?>

                                                        <tr>
                                                            <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                                            <td colspan="2" style="text-align: left">
                                                                <strong>
                                                                    R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                                </strong>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="3" style="text-align: right"><strong>Desconto:</strong></td>
                                                            <td colspan="2" style="text-align: left">
                                                                <strong>
                                                                    R$ <?php echo number_format($result->desconto, 2, ',', '.'); ?>
                                                                </strong>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="3" style="text-align: right"><strong>Frete:</strong></td>
                                                            <td colspan="2" style="text-align: left">
                                                                <strong>
                                                                    R$ <?php echo number_format($result->valorFrete, 2, ',', '.'); ?>
                                                                </strong>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="3" style="text-align: right"><strong>SubTotal:</strong></td>
                                                            <td colspan="2" style="text-align: left">
                                                                <strong>
                                                                    R$ <?php echo number_format($total - $result->desconto +$result->valorFrete , 2, ',', '.'); ?>
                                                                </strong>
                                                                <input type="hidden" id="total-venda"
                                                                       value="<?php echo number_format($total - $result->desconto +$result->valorFrete, 2); ?>">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab3">
                                            <form action="<?php echo current_url(); ?>" method="post" id="formEnderecoEntregaPedido">
                                                <input type="hidden" name="idPedido" id="idPedido"
                                                       value="<?php echo $result->idPedido ?>"/>
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">

                                                    <div class="span12" style="margin-left: 0">

                                                        <div class="span3">
                                                            <label for="cepEntrega">CEP</label>
                                                            <input id="cepEntrega" type="text" name="cepEntrega" class="span12" onBlur="getConsultaCEP();"
                                                                   value="<?php echo $result->cepEntrega; ?>"/>
                                                            <small>[TAB] consulta cep (Necessita Internet)</small>
                                                        </div>

                                                        <div class="span6">
                                                            <label for="ruaEntrega">Rua</label>
                                                            <input id="ruaEntrega" type="text" name="ruaEntrega"  class="span12"
                                                                   value="<?php echo $result->ruaEntrega; ?>"/>
                                                        </div>

                                                        <div class="span3">
                                                            <label for="numeroEntrega">Número</label>
                                                            <input id="numeroEntrega" type="text" name="numeroEntrega"  class="span12"
                                                                   value="<?php echo $result->numeroEntrega; ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="span12" style="margin-left: 0">
                                                        <div class="span12">
                                                            <label for="complementoEntrega">Complemento</label>
                                                            <input id="complementoEntrega" type="text" name="complementoEntrega"  class="span12"
                                                                   value="<?php echo $result->complementoEntrega; ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="span12" style="margin-left: 0">
                                                        <div class="span5">
                                                            <label for="bairroEntrega">Bairro</label>
                                                            <input id="bairroEntrega" type="text" name="bairroEntrega"  class="span12"
                                                                   value="<?php echo $result->bairroEntrega; ?>"/>
                                                        </div>

                                                        <div class="span4">
                                                            <label for="cidadeEntrega">Cidade</label>
                                                            <input id="cidadeEntrega" type="text" name="cidadeEntrega"  class="span12"
                                                                   value="<?php echo $result->cidadeEntrega; ?>"/>
                                                        </div>
                                                        <div class="span3">
                                                            <label for="estadoEntrega">Estado</label>
                                                            <input id="estadoEntrega" type="text" name="estadoEntrega"  class="span12"
                                                                   value="<?php echo $result->estadoEntrega; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="span12" style="padding: 1%; margin-left: 0">

                                                    <div class="span8 offset2" style="text-align: center">
                                                        <?php if ($result->faturado == 0) { ?>
                                                            <button class="btn btn-primary" id="btnContinuar"><i
                                                                        class="icon-white icon-ok"></i> Alterar
                                                            </button>
                                                        <?php } ?>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>


                                        <div class="tab-pane" id="tab4">

                                            <form action="<?php echo current_url(); ?>" method="post" id="formObservacaoPedido">
                                                <?php echo form_hidden('idPedido', $result->idPedido) ?>
                                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <label for="senha">Observações do pedido</label>
                                                        <textarea class="span12"  name="observacao" cols="30" rows="5"><?php echo $result->observacaoPedido; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="span12" style="padding: 1%; margin-left: 0">

                                                    <div class="span8 offset2" style="text-align: center">

                                                        <?php if ($result->faturado == 0) { ?>
                                                            <button class="btn btn-primary" id="btnContinuar"><i
                                                                        class="icon-white icon-ok"></i> Alterar
                                                            </button>
                                                        <?php } ?>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal consulta de estoque -->
<div id="modal-consulta-estoque" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmail" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Consulta de Estoque</h3>
        </div>
        <div class="modal-body">

            <div class="span12" style="padding: 1%; margin-left: 0;text-align: center;">
                <label for="senha"><div id="consultaEstoqueNomeProduto"></div></label>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <table class="table table-bordered" id="tblConsultaEstoque">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="text-align: left;">Filial</th>
                        <th style="width: 10%;text-align: right;">Estoque</th>
                        <!--
                        <th style="width: 10%;text-align: right;">Reservado</th>
                        <th style="width: 10%;text-align: right;">Bloqueado</th>
                        <th style="width: 10%;text-align: right;">Disponível</th>
                        !-->
                        <th style="width: 10%;text-align: right;">Venda(R$)</th>
                        <th style="width: 20%;text-align: center;">Localização</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr style="backgroud-color: #2D335B">
                        <th colspan="10"></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>

<!-- Modal Faturar e Entregar pedido-->
<div id="modal-entregar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form id="formEntregarPedido" action="<?php echo current_url() ?>" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Faturar Venda</h3>
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0">
                    <label for="cliente">Fornecedor</label>
                    <input class="span12" id="fornecedor" readonly type="text" name="fornecedor"
                           value="<?php echo $result->nomeFornecedor ?>"/>
                    <input type="hidden" name="fornecedor_id" id="fornecedor_id" value="<?php echo $result->fornecedor_id ?>">
                    <input type="hidden" name="pedido_id" id="pedido_id" value="<?php echo $result->idPedido; ?>">
                </div>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span6">
                    <label for="valor">Valor*</label>
                    <input type="hidden" id="tipo"  name="tipo" value="despesa"/>
                    <input class="span12 money" readonly id="valor" type="text" name="valor"
                           value="<?php echo number_format($total, 2); ?>"/>
                </div>

                <div class="span6">
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12" id="vencimento" required="required" type="date" value="<?php echo date('Y-m-d'); ?>" name="vencimento"/>
                </div>

            </div>

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <input class="span12" readonly id="descricao" type="text" name="descricao"
                       value="Fatura de Compra - #<?php echo $result->idPedido; ?> "/>
            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="recebido">Recebido?</label>
                    &nbsp &nbsp &nbsp &nbsp<input id="recebido" type="checkbox" name="recebido" value="1"/>
                </div>
                <div id="divRecebimento" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="recebimento">Data Recebimento</label>
                        <input class="span12" id="recebimento" type="date" name="recebimento"/>
                    </div>
                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto" id="formaPgto" class="span12">
                            <option value="Dinheiro">Dinheiro</option>
                            <option value="Cartão de Crédito">Cartão de Crédito</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Boleto">Boleto</option>
                            <option value="Depósito">Depósito</option>
                            <option value="Débito">Débito</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
            <button class="btn btn-primary">Faturar</button>
        </div>
    </form>
</div>


<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">

    function getConsultaCEP() {
        if($.trim($("#cepEntrega").val()) != ""){
            var url = 'http://api.postmon.com.br/v1/cep/'+$("#cepEntrega").val();
            $.get(url,{
                    cep:$("#cepEntrega").val()
                },
                function (data) {
                    if(data != -1){
                        $("#ruaEntrega").val( data.logradouro  );
                        $("#bairroEntrega").val( data.bairro );
                        $("#cidadeEntrega").val( data.cidade );
                        $("#estadoEntrega").val( data.estado );
                    }
                });
        }
    }

    $(document).ready(function () {

        $(".money").maskMoney();

        $("#formPedido").validate({
            rules: {
                cliente: {required: true},
                tecnico: {required: true},
                dataVenda: {required: true}
            },
            messages: {
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataVenda: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $('#recebido').click(function (event) {
            var flag = $(this).is(':checked');
            if (flag == true) {
                $('#divRecebimento').show();
            }
            else {
                $('#divRecebimento').hide();
            }
        });

        $("#formEnderecoEntregaPedido").validate({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/pedido/editarEnderecoEntrega",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        alert('Alterado');
                    }
                });
                return false;
            }
        });

        $("#formObservacaoPedido").validate({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/pedido/editarObservacaoPedido",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        alert('Alterado');
                    }
                });
                return false;
            }
        });

        $("#formProdutos").validate({
            rules: {
                quantidade: {required: true},
                custo: {required: true},
                idProduto: {required: true}
            },
            messages: {
                quantidade: {required: 'Insira a quantidade'},
                custo: {required: 'Insira o preço de compra'},
                idProduto: {required: 'Insira o produto'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/pedido/adicionarProduto",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");

                            $("#idProduto").select2('data', {
                                id: null,
                                text: ''
                            });

                            $("#quantidade").val('1');
                            $("#custo").val('');
                            $("#idProduto").val('').focus();
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', 'a', function (event) {

            var idProduto = $(this).attr('idAcao');
            var quantidade = $(this).attr('quantAcao');
            var produto = $(this).attr('prodAcao');

            if ((idProduto % 1) === 0) {
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/pedido/excluirProduto",
                    data: "idProduto=" + idProduto + "&quantidade=" + quantidade + "&produto=" + produto,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', '#btn-entregar', function (event) {
            event.preventDefault();
            valor = $('#total-venda').val();
            valor = valor.replace(',', '');
            $('#valor').val(valor);
        });


        $("#formEntregarPedido").validate({
            rules: {
                descricao: {required: true},
                cliente: {required: true},
                valor: {required: true},
                vencimento: {required: true}
            },
            messages: {
                descricao: {required: 'Campo Requerido.'},
                cliente: {required: 'Campo Requerido.'},
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $('#btn-cancelar-faturar').trigger('click');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/pedido/entregar/"+$('#pedido_id').val(),
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url();?>index.php/pedido/faturar/"+$('#pedido_id').val(),
                                data: dados,
                                dataType: 'html',
                                success: function (data) {
                                    window.location.reload(true);
                                }
                            });

                        } else {
                            alert('Ocorreu um erro ao tentar faturar venda.');
                            $('#progress-fatura').hide();
                        }
                    }
                });

                return false;
            }
        });

        $('#confirmarPedido').click(function (event) {

            var condicaoPagamento = $('#condicao_pagamento_id');
            var formaPagamento    = $('#forma_pagamento_id');
            var dataPrevisaoEntrega = $('#previsaoEntrega');
            var valor = $('#total-venda').val();

            if (parseFloat(valor) <= 0) {
                alert("Não foi adicionado nenhum item ao pedido de compra!");
                return false;
            }

            if (condicaoPagamento.val() == '') {
                alert("Condição de pagamento é obrigatório para confirmar o pedido!");
                return false;
            }

            if (formaPagamento.val() == '') {
                alert("Forma de pagamento é obrigatório para confirmar o pedido!");
                return false;
            }

            if (dataPrevisaoEntrega.val() == '') {
                alert("Previsão para entrega é obrigatório para confirmar o pedido!");
                return false;
            }
            var dados = $("#formPedido").serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/pedido/editar",
                data: dados,
                dataType: 'html',
                async: false,
                success: function (data) {
                    return true;
                }
            });
            return true;
        });

        $('#addConsultaProduto').click(function (event) {
            event.preventDefault();
            var table = $('#tblConsultaEstoque');
            var $tbody = table.append('<tbody />').children('tbody');
            $tbody.empty();
            $('#consultaEstoqueNomeProduto').html('');
            var produto = $('#idProduto').val();

            if (produto) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/getProdutoFiliais",
                    data: "produto=" + produto,
                    dataType: 'json',
                    success: function (produtofiliais) {
                        $.each(produtofiliais, function(i, produtofilial) {

                            var table = $('#tblConsultaEstoque');
                            var $tbody = table.append('<tbody />').children('tbody');
                            $('#consultaEstoqueNomeProduto').html(produtofilial.produto);

                            $tbody.append('<tr />').children('tr:last')
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")' style='text-align: left;cursor: pointer;'>" + produtofilial.filial + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.estoque + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.reservado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.bloqueado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'><b>" + (produtofilial.estoque -  produtofilial.reservado - produtofilial.bloqueado) + ' ' + produtofilial.unidade + "</b></td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>R$" + produtofilial.precoVenda + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: center;cursor: pointer;'>" + produtofilial.localizacao + " </td>");
                        });
                    }
                });
            } else {
                $('#consultaEstoqueNomeProduto').html('Selecione um produto para consulta estoque');
            }
        });

        $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
    });

    function preencherEstoquePorFilial(idProdutoFilial) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/produtos/getUnicProdutoFilial",
            data: "idProdutoFilial=" + idProdutoFilial,
            dataType: 'json',
            success: function (produtofiliais) {
                $("#idProduto").val(produtofiliais.produto_id);
                $("#estoque").val(produtofiliais.estoque);
                $("#custo").val(produtofiliais.precoVenda);
                $("#filial_customizada").val(produtofiliais.filial_id);
                $("#quantidade").focus();
                $("#modal-consulta-estoque .close").click()

            }
        });
    }
</script>

