<?php $totalProdutos = 0;?>

<?php
/*
SystemBoys - Garotos de Sistema
http://www.systemboys.com.br
Marcos Aurélio R. Silva
systemboy_marcos@hotmail.com
*/

function convert_number_to_words($valor=0, $maiusculas=false) {
    // verifica se tem virgula decimal
    if (strpos($valor, ",") > 0) {
        // retira o ponto de milhar, se tiver
        $valor = str_replace(".", "", $valor);

        // troca a virgula decimal por ponto decimal
        $valor = str_replace(",", ".", $valor);
    }
    $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
    $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões",
        "quatrilhões");

    $c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
        "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
    $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
        "sessenta", "setenta", "oitenta", "noventa");
    $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
        "dezesseis", "dezesete", "dezoito", "dezenove");
    $u = array("", "um", "dois", "três", "quatro", "cinco", "seis",
        "sete", "oito", "nove");

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    $cont = count($inteiro);
    for ($i = 0; $i < $cont; $i++)
        for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
            $inteiro[$i] = "0" . $inteiro[$i];

    $fim = $cont - ($inteiro[$cont - 1] > 0 ? 1 : 2);
    $rt = '';
    for ($i = 0; $i < $cont; $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                $ru) ? " e " : "") . $ru;
        $t = $cont - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000"

        )$z++; elseif ($z > 0)
            $z--;
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= ( ($z > 1) ? " de " : "") . $plural[$t];
        if ($r)
            $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                    ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    if (!$maiusculas) {
        return($rt ? $rt : "zero");
    } elseif ($maiusculas == "2") {
        return (strtoupper($rt) ? strtoupper($rt) : "Zero");
    } else {
        return (ucwords($rt) ? ucwords($rt) : "Zero");
    }
}


$firma= $emitente->nome;
$cnpj_cgc_firma= $emitente->cnpj;
$logomarca= $emitente->url_logo;

$nome_devedor=$result->nomeFornecedor;
$cpf_cnpj_devedor=$result->documento;


$endereco= $cliente->rua.' '.$cliente->numero.','.$cliente->bairro.' - '.$cliente->cidade.'/'.$cliente->estado;

$referente='referente a compra de mercadoria';
$final='estando quitado o d&#233;bito referido a esta data';
$dia_pagamento= date("d");
$mes_pagamento= date("m");
$ano_pagamento= date("y");
$cidade=$emitente->cidade;
$estado=$emitente->uf;
?>

<style type="text/css">
    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 11px;
        color: #666;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>

<div class="row-fluid" style="margin-top: 0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Pedido de compra</h5>
                <div class="buttons">
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                        echo '<a class="btn btn-mini btn-info" href="'.base_url().'index.php/pedido/editar/'.$result->idPedido.'"><i class="icon-pencil icon-white"></i> Editar</a>';
                    } ?>

                    <a id="imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i> Imprimir</a>
                </div>
            </div>
            <div class="widget-content" id="printOs">
                <div class="invoice-content">
                    <table class="table" style="margin-top: 0">
                        <tbody>

                        <?php if($emitente == null) {?>
                            <tr>
                                <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<</td>
                            </tr>
                        <?php } else {?>

                            <tr>
                                <td style="width: 10%"><img src=" <?php echo $emitente->url_logo; ?> "></td>
                                <td> <span style="font-size: 20px; "> <?php echo $emitente->nome; ?></span> </br><span><?php echo $emitente->cnpj; ?> </br> <?php echo $emitente->rua.', nº:'.$emitente->numero.', '.$emitente->bairro.' - '.$emitente->cidade.' - '.$emitente->uf; ?> </span> </br> <span> E-mail: <?php echo $emitente->email.' - Fone: '.$emitente->telefone; ?></span></td>
                                <td style="width: 30%; text-align: left">
                                    <b>#Pedido:</b> <span ><?php echo $result->idPedido?></span>
                                    </br>
                                    <b>Status:</b> <span ><?php echo $result->status;?></span>
                                    </br>
                                    <b>Condição de pagamento:</b> <span ><?php echo $result->condicaoPagamento;?></span>
                                    </br>
                                    <b>Forma de pagamento:</b> <span ><?php echo $result->formaPagamento;?></span>
                                    </br>
                                    <span><b>Emissão: </b><?php echo date(('d/m/Y'),strtotime($result->dataPedido));?></span>
                                    <?php if ($result->previsaoEntrega != '0000-00-00') {?>
                                        </br>
                                        <span><b>Previsão: </b><?php echo date(('d/m/Y'),strtotime($result->previsaoEntrega));?></span>
                                    <?php } ?>
                                </td>
                            </tr>

                        <?php } ?>
                        </tbody>
                    </table>

                    <table class="table" style="margin-top: 0">
                        <tbody>
                        <tr>
                            <td style="width: 30%; padding-left: 0">
                                <ul>
                                    <li>
                                        <span><h6>Cliente</h6>
                                        <span><?php echo $result->nomeFornecedor?></span><br/>
                                        <span><?php echo $cliente->rua?>, <?php echo $cliente->numero?>, <?php echo $cliente->bairro?></span><br/>
                                        <span><?php echo $cliente->cidade?> - <?php echo $cliente->estado?></span>
                                    </li>
                                </ul>
                            </td>
                            <td style="width: 30%; padding-left: 0">
                                <ul>
                                    <li>
                                        <span><h6>Vendedor</h6></span>
                                        <span><?php echo $result->nome?></span> <br/>
                                        <span>Telefone: <?php echo $result->telefone?></span><br/>
                                        <span>Email: <?php echo $result->email?></span>
                                    </li>
                                </ul>
                            </td>

                            <td style="width: 30%; padding-left: 0">
                                <ul>
                                    <li>
                                        <span><h6>Entrega</h6></span>
                                        <span>Rua: <?php echo $result->ruaEntrega.' '.$result->numeroEntrega?></span> <br/>
                                        <span>Complemento: <?php echo $result->complementoEntrega?></span><br/>
                                        <span>Bairro: <?php echo $result->bairroEntrega ?></span> <br/>
                                        <span>Cidade: <?php echo $result->cidadeEntrega.'/'.$result->estadoEntrega.' - '.$result->cepEntrega?></span>
                                    </li>
                                </ul>
                            </td>
                        </tr>

                        <?php if ($result->observacaoPedido) {?>
                            <tr>
                                <td colspan="3">
                                    <span><h6>Observação</h6>
                                    <?php echo $result->observacaoPedido;?>
                                </td>
                            </tr>
                        <?php }?>

                        </tbody>
                    </table>


                    <div style="margin-top: 0; padding-top: 0">


                        <?php if($produtos != null){?>

                            <table class="table table-bordered table-condensed" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th width="45%" style="font-size: 15px;text-align: left;">Produto</th>
                                    <th width="5%" style="font-size: 15px;text-align: center;">Quantidade</th>
                                    <th width="20%" style="font-size: 15px;text-align: right;">Preço de compra</th>
                                    <th width="20%" style="font-size: 15px;text-align: right;">Sub-total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($produtos as $p) {

                                    $totalProdutos = $totalProdutos + $p->subTotal;
                                    echo '<tr>';
                                    echo '<td style="text-align: left;">'.$p->descricao.'</td>';
                                    echo '<td style="text-align: center;"> '.$p->quantidade.'   </td>';
                                    echo '<td style="text-align: right;">R$ '.number_format($p->subTotal/$p->quantidade,2,',','.').'</td>';
                                    echo '<td style="text-align: right;">R$ '.number_format($p->subTotal,2,',','.').'</td>';
                                    echo '</tr>';
                                }?>

                                <tr>
                                    <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                    <td style="text-align: right;"><strong>R$ <?php echo number_format($totalProdutos,2,',','.');?></strong></td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="text-align: right"><strong>Desconto:</strong></td>
                                    <td style="text-align: right;"><strong>R$ <?php echo number_format($result->desconto,2,',','.');?></strong></td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="text-align: right"><strong>Valor do frete:</strong></td>
                                    <td style="text-align: right;"><strong>R$ <?php echo number_format($result->valorFrete,2,',','.');?></strong></td>
                                </tr>
                                </tbody>
                            </table>
                        <?php }?>


                        <h4 style="text-align: right">SubTotal: R$ <?php echo number_format($totalProdutos - $result->desconto + $result->valorFrete,2,',','.');?></h4>


                        <?php

                        $valor_devedor = $totalProdutos;
                        $valor_extenso_devedor= convert_number_to_words($valor_devedor);
                        ?>

                        <?php if($result->faturado == 1){ ?>
                            <h4 align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">RECIBO</font></h4>
                            <p align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $firma; ?><br />CNPJ: <?php echo $cnpj_cgc_firma; ?></font></p>
                            <p align="right"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">RECIBO: R$ <?php echo $valor_devedor; ?></font></p>
                            <p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="font-family:Verdana, Arial, Helvetica, sans-serif">Recebemos de (a) <strong><?php echo $nome_devedor; ?>

                                        <?php if ($cpf_cnpj_devedor) {?>
                                    </strong> portador (a) do CPF/CNPJ: <strong><?php echo $cpf_cnpj_devedor; ?></strong>
                                    <?php }?>

                                    , a import&acirc;ncia de R$ <strong><?php echo $valor_devedor; ?></strong> (<?php echo $valor_extenso_devedor; ?>) referente &agrave; <strong><?php echo $referente; ?></strong>, <?php echo $final; ?>.</font></p>

                            <?php if($cliente->rua) {?>
                                <p align="right"><font style="font-family:Verdana, Arial, Helvetica, sans-serif">
                                        <?php echo $endereco; ?></font>
                                </p>
                            <?php }?>

                            <p align="center"><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $cidade; ?> - <?php echo $estado; ?>, <?php echo $dia_pagamento; ?>/<?php echo $mes_pagamento; ?>/<?php echo $ano_pagamento; ?>.</font></p>
                            <p>&nbsp;</p>
                            <p align="center">________________________________________<br /><font style="font-family:Verdana, Arial, Helvetica, sans-serif"><?php echo $firma; ?></font></p>

                        <?php } ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $("#imprimir").click(function(){

            //pega o Html da DIV
            var divElements = document.getElementById('printOs').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;
        });
    });
</script>