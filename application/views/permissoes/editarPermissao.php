<?php $permissoes = unserialize($result->permissoes); ?>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de permissões<small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo base_url(); ?>index.php/permissoes/editar" id="formPermissao" method="post">
                    <div class="span12" style="margin-left: 0">
                        <div class="widget-box">
                            <div class="widget-content">
                                <div class="span4">
                                    <label>Nome da Permissão</label>
                                    <input name="nome" type="text" id="nome" class="form-control"
                                           value="<?php echo $result->nome; ?>"/>
                                    <input type="hidden" name="idPermissao" value="<?php echo $result->idPermissao; ?>">
                                </div>
                                <div class="span3">
                                    <label>Situação</label>

                                    <select name="situacao" id="situacao" class="form-control">
                                        <?php if ($result->situacao == 1) {
                                            $sim = 'selected';
                                            $nao = '';
                                        } else {
                                            $sim = '';
                                            $nao = 'selected';
                                        } ?>
                                        <option value="1" <?php echo $sim; ?>>Ativo</option>
                                        <option value="0" <?php echo $nao; ?>>Inativo</option>
                                    </select>
                                </div>
                                <div class="span4">
                                    <br/>
                                    <label>
                                        <input name="" type="checkbox" value="1" checked="" id="marcarTodos"/>
                                        <span class="lbl"> Marcar Todos</span>
                                    </label>
                                    <br/>
                                </div>
                                <div class="control-group">
                                    <label for="documento" class="control-label"></label>
                                    <div class="controls">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vCliente'])) {
                                                            if ($permissoes['vCliente'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vCliente" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Cliente</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aCliente'])) {
                                                            if ($permissoes['aCliente'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aCliente" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Cliente</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eCliente'])) {
                                                            if ($permissoes['eCliente'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eCliente" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Cliente</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dCliente'])) {
                                                            if ($permissoes['dCliente'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dCliente" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Cliente</span>
                                                    </label>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vProduto'])) {
                                                            if ($permissoes['vProduto'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vProduto" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Produto</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aProduto'])) {
                                                            if ($permissoes['aProduto'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aProduto" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Produto</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eProduto'])) {
                                                            if ($permissoes['eProduto'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eProduto" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Produto</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dProduto'])) {
                                                            if ($permissoes['dProduto'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dProduto" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Produto</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vPatrimonio'])) {
                                                            if ($permissoes['vPatrimonio'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vPatrimonio" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Visualizar Patrimônio</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aPatrimonio'])) {
                                                            if ($permissoes['aPatrimonio'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aPatrimonio" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Adicionar Patrimônio</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['ePatrimonio'])) {
                                                            if ($permissoes['ePatrimonio'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="ePatrimonio" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Editar Patrimônio</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dPatrimonio'])) {
                                                            if ($permissoes['dPatrimonio'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dPatrimonio" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Excluir Patrimônio</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vServico'])) {
                                                            if ($permissoes['vServico'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vServico" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Serviço</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aServico'])) {
                                                            if ($permissoes['aServico'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aServico" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Serviço</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eServico'])) {
                                                            if ($permissoes['eServico'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eServico" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Serviço</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dServico'])) {
                                                            if ($permissoes['dServico'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dServico" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Serviço</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vOs'])) {
                                                            if ($permissoes['vOs'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vOs" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar OS</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aOs'])) {
                                                            if ($permissoes['aOs'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aOs" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar OS</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eOs'])) {
                                                            if ($permissoes['eOs'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eOs" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar OS</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dOs'])) {
                                                            if ($permissoes['dOs'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dOs" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir OS</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vVistoria'])) {
                                                            if ($permissoes['vVistoria'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vVistoria" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Vistoria</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aVistoria'])) {
                                                            if ($permissoes['aVistoria'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aVistoria" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Vistoria</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eVistoria'])) {
                                                            if ($permissoes['eVistoria'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eVistoria" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Vistoria</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dVistoria'])) {
                                                            if ($permissoes['dVistoria'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dVistoria" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Vistoria</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vNfe'])) {
                                                            if ($permissoes['vNfe'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vNfe" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar NF-e</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aNfe'])) {
                                                            if ($permissoes['aNfe'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aNfe" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar NF-e</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eNfe'])) {
                                                            if ($permissoes['eNfe'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eNfe" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar NF-e</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dNfe'])) {
                                                            if ($permissoes['dNfe'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dNfe" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir NF-e</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vNfse'])) {
                                                            if ($permissoes['vNfse'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vNfse" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar NFS-e</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aNfse'])) {
                                                            if ($permissoes['aNfse'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aNfse" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar NFS-e</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eNfse'])) {
                                                            if ($permissoes['eNfse'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eNfse" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar NFS-e</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dNfse'])) {
                                                            if ($permissoes['dNfse'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dNfse" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir NFS-e</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vVenda'])) {
                                                            if ($permissoes['vVenda'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vVenda" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Venda</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aVenda'])) {
                                                            if ($permissoes['aVenda'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aVenda" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Venda</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eVenda'])) {
                                                            if ($permissoes['eVenda'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eVenda" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Venda</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dVenda'])) {
                                                            if ($permissoes['dVenda'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dVenda" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Venda</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vPedido'])) {
                                                            if ($permissoes['vPedido'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vPedido" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Pedido</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aPedido'])) {
                                                            if ($permissoes['aPedido'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aPedido" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Pedido</span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['ePedido'])) {
                                                            if ($permissoes['ePedido'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="ePedido" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Pedido</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dPedido'])) {
                                                            if ($permissoes['dPedido'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dPedido" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Pedido</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vTransferencia'])) {
                                                            if ($permissoes['vTransferencia'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vTransferencia" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Visualizar Transferência</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aTransferencia'])) {
                                                            if ($permissoes['aTransferencia'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aTransferencia" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Adicionar Transferência</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eTransferencia'])) {
                                                            if ($permissoes['eTransferencia'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eTransferencia" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Editar Transferência</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dTransferencia'])) {
                                                            if ($permissoes['dTransferencia'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dTransferencia" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Excluir Transferência</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vInventario'])) {
                                                            if ($permissoes['vInventario'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vInventario" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Visualizar Inventario</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aInventario'])) {
                                                            if ($permissoes['aInventario'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aInventario" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Adicionar Inventario</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eInventario'])) {
                                                            if ($permissoes['eInventario'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eInventario" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Editar Inventario</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dInvenatario'])) {
                                                            if ($permissoes['dInvenatario'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dInvenatario" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Excluir Inventario</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vArquivo'])) {
                                                            if ($permissoes['vArquivo'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vArquivo" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Visualizar Arquivo</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aArquivo'])) {
                                                            if ($permissoes['aArquivo'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aArquivo" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Adicionar Arquivo</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eArquivo'])) {
                                                            if ($permissoes['eArquivo'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eArquivo" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Editar Arquivo</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dArquivo'])) {
                                                            if ($permissoes['dArquivo'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dArquivo" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Excluir Arquivo</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vLancamento'])) {
                                                            if ($permissoes['vLancamento'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vLancamento" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Visualizar Lançamento</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aLancamento'])) {
                                                            if ($permissoes['aLancamento'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aLancamento" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Adicionar Lançamento</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['eLancamento'])) {
                                                            if ($permissoes['eLancamento'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="eLancamento" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Editar Lançamento</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['dLancamento'])) {
                                                            if ($permissoes['dLancamento'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="dLancamento" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Excluir Lançamento</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['rCliente'])) {
                                                            if ($permissoes['rCliente'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="rCliente" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Relatório Cliente</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['rServico'])) {
                                                            if ($permissoes['rServico'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="rServico" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Relatório Serviço</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['rOs'])) {
                                                            if ($permissoes['rOs'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="rOs" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Relatório OS</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['rProduto'])) {
                                                            if ($permissoes['rProduto'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="rProduto" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Relatório Produto</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['rVenda'])) {
                                                            if ($permissoes['rVenda'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="rVenda" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Relatório Venda</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['rFinanceiro'])) {
                                                            if ($permissoes['rFinanceiro'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="rFinanceiro" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Relatório Financeiro</span>
                                                    </label>
                                                </td>
                                                <td colspan="2"></td>

                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['cUsuario'])) {
                                                            if ($permissoes['cUsuario'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="cUsuario" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Configurar Usuário</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['cEmitente'])) {
                                                            if ($permissoes['cEmitente'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="cEmitente" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Configurar Emitente</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['cPermissao'])) {
                                                            if ($permissoes['cPermissao'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="cPermissao" class="marcar" type="checkbox"
                                                             value="1"/>
                                                        <span class="lbl"> Configurar Permissão</span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['cBackup'])) {
                                                            if ($permissoes['cBackup'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="cBackup" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Backup</span>
                                                    </label>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['vOnnerApp'])) {
                                                            if ($permissoes['vOnnerApp'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="vOnnerApp" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Exibir Menu OnnerApp</span>
                                                    </label>
                                                </td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <label>
                                                        <input <?php if (isset($permissoes['aPDV'])) {
                                                            if ($permissoes['aPDV'] == '1') {
                                                                echo 'checked';
                                                            }
                                                        } ?> name="aPDV" class="marcar" type="checkbox" value="1"/>
                                                        <span class="lbl"> Exibir PDV</span>
                                                    </label>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-actions">
                                    <div class="span12">
                                        <div class="span6 offset3">
                                            <button type="submit" class="btn btn-primary"><i
                                                        class="icon-ok icon-white"></i> Alterar
                                            </button>
                                            <a href="<?php echo base_url() ?>index.php/permissoes" id="" class="btn"><i
                                                        class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo base_url() ?>assets/js/validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {


        $("#marcarTodos").click(function () {

            debugger;

            if ($(this).prop("checked")) {
                $('.marcar').each(
                    function () {
                        $(this).attr("checked", true);
                        $(this).prop("checked", true);
                    }
                );
            } else {
                $('.marcar').each(
                    function () {
                        $(this).attr("checked", false);
                        $(this).prop("checked", false);
                    }
                );
            }

        });


        $("#formPermissao").validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo obrigatório'}
            }
        });


    });
</script>
