<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sitemap"></i> Grupos de persmissões (<?php echo count($results); ?>)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a href="<?php echo base_url();?>index.php/permissoes/adicionar" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar Permissão</a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nome</th>
                                        <th style="text-align: center;">Data de Criação</th>
                                        <th style="text-align: center;">Situação</th>
                                        <th style="text-align: center;">Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($results as $r) {
                                        if($r->situacao == 1){$situacao = 'Ativo';}else{$situacao = 'Inativo';}
                                        echo '<tr>';
                                        echo '<td >'.$r->idPermissao.'</td>';
                                        echo '<td>'.$r->nome.'</td>';
                                        echo '<td style="text-align: center;">'.date('d/m/Y',strtotime($r->data)).'</td>';
                                        echo '<td style="text-align: center;">'.$situacao.'</td>';
                                        echo '<td style="text-align: center;">
                                                  <a href="'.base_url().'index.php/permissoes/editar/'.$r->idPermissao.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>
                                                  <a href="#modal-excluir" role="button" data-toggle="modal" permissao="'.$r->idPermissao.'"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>
                                              </td>';
                                        echo '</tr>';
                                    }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





 
<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/permissoes/desativar" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Desativar Permissão</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idPermissao" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente desativar esta permissão?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>


<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var permissao = $(this).attr('permissao');
        $('#idPermissao').val(permissao);

    });

});

</script>
