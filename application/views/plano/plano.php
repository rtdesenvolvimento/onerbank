<a href="<?php echo base_url(); ?>index.php/empresa/adicionar" class="btn btn-success"><i
        class="icon-plus icon-white"></i> Adicionar Plano</a>


<?php
if (!$results){
?>

<div class="widget-box">

    <div class="col-xs-6 text-left">

        <div class="widget-title">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            <h5>Plano</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th>Nome</th>
                    <th style="text-align: left;">Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhum Plano Cadastrado</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php }else{?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-user"></i>
         </span>
            <h5>Oficinas</h5>
        </div>

        <div class="widget-content nopadding">
            <div class="col-xs-6 text-left">
                <table class="table table-bordered ">
                    <thead>
                    <tr>
                        <th style="width: 5%;text-align: center;">#</th>
                        <th style="text-align: left;">Nome</th>
                        <th style="text-align: left;">CPF/CNPJ</th>
                        <th style="text-align: left;">Telefone</th>
                        <th style="text-align: left;">Endereço</th>
                        <th style="text-align: left;">Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($results as $r) {
                        echo '<tr>';
                        echo '<td style="width: 5%;text-align: center;">' . str_pad($r->idEmpresa, 4, '0', STR_PAD_LEFT) . '</td>';
                        echo '<td style="text-align: left;">' . $r->nomeEmpresa . '</td>';
                        echo '<td style="text-align: left;">' . $r->documento . '</td>';
                        echo '<td style="text-align: left;">' . $r->telefone . '</td>';
                        echo '<td style="text-align: left;">' . $r->rua . ' nº ' . $r->numero . ' - ' . $r->bairro . ' - ' . $r->cidade . '/' . $r->estado . ' - ' . $r->cep . '</td>';

                        if ($r->status == 1) {
                            echo '<td style="text-align: center;">Ativo</td>';
                        } else {
                            echo '<td style="text-align: center;">Inativo</td>';
                        }

                        echo '<td>';
                        echo '<a href="' . base_url() . 'index.php/empresa/visualizar/' . $r->idEmpresa . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                        echo '<a href="' . base_url() . 'index.php/empresa/editar/' . $r->idEmpresa . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                        echo '<a href="#modal-excluir" role="button" data-toggle="modal" empresa="' . $r->idEmpresa . '" style="margin-right: 1%" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>';


                        echo '</td>';
                        echo '</tr>';
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php echo $this->pagination->create_links();
        } ?>

        <!-- Modal -->
        <div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <form action="<?php echo base_url() ?>index.php/empresa/inativar" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 id="myModalLabel">Excluir Empresa</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idEmpresa" name="id" value=""/>
                    <h5 style="text-align: center">Deseja realmente inativar esta oficiona?</h5>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn btn-danger">Inativar</button>
                </div>
            </form>
        </div>


        <style type="text/css">
            table.tablesorter input.tablesorter-filter,
            table.tablesorter select.tablesorter-filter {
                width: 95%;
                height: inherit;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            .busca_veiculo {
                color: #878787;
                font-size: 11px;
                top: 37px;
                margin: -17px 8px 2px 10px;
                text-align: left;
            }

            .busca_veiculo input {
                margin-bottom: 0;
            }

        </style>

        <script src="<?php echo base_url() ?>js/jquery.tablesorter.js"></script>
        <script src="<?php echo base_url() ?>js/jquery.tablesorter.widgets.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                // call the tablesorter plugin
                $("table").tablesorter({

                    // initialize zebra striping and filter widgets
                    widgets: ["zebra", "filter"],

                    // headers: { 5: { sorter: false, filter: false } },

                    widgetOptions: {

                        // css class applied to the table row containing the filters & the inputs within that row
                        filter_cssFilter: 'tablesorter-filter',

                        // If there are child rows in the table (rows with class name from "cssChildRow" option)
                        // and this option is true and a match is found anywhere in the child row, then it will make that row
                        // visible; default is false
                        filter_childRows: false,

                        // Set this option to true to use the filter to find text from the start of the column
                        // So typing in "a" will find "albert" but not "frank", both have a's; default is false
                        filter_startsWith: false,

                        // Set this option to false to make the searches case sensitive
                        filter_ignoreCase: true,

                        // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
                        // every character while typing and should make searching large tables faster.
                        filter_searchDelay: 300,

                        // See the filter widget advanced demo on how to use these special functions
                        filter_functions: {}

                    }
                    // ,debug: true
                });

                $(document).on('click', 'a', function (event) {
                    var empresa = $(this).attr('empresa');
                    $('#idEmpresa').val(empresa);

                });
            });
        </script>