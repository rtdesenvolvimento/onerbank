<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de produto <small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" >

                    <div class="control-group">
                        <label for="cProd" class="control-label">Código<span class="required">*</span></label>
                        <div class="controls">
                            <input id="cProd" type="text" class="form-control" name="cProd" required="required" value="<?php echo set_value('cProd'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição<span class="required">*</span></label>
                        <div class="controls">
                            <input id="descricao" type="text" class="form-control" required="required" name="descricao" value="<?php echo set_value('descricao'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="cEAN" class="control-label">EAN/GTIN</label>
                        <div class="controls">
                            <input id="cEAN" type="text" class="form-control" name="cEAN" value="<?php echo set_value('cEAN'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="unidade" class="control-label">Unidade Comercial<span class="required">*</span></label>
                        <div class="controls">
                            <select name="unidade" name="unidade" class="form-control">
                                <option value="UN" <?php if(set_value('unidade') == 'UN'){echo 'selected';}?>>UN</option>
                                <option value="PC" <?php if(set_value('unidade') == 'PC'){echo 'selected';}?>>PC</option>
                                <option value="CX" <?php if(set_value('unidade') == 'CX'){echo 'selected';}?>>CX</option>
                                <option value="DZ" <?php if(set_value('unidade') == 'DZ'){echo 'selected';}?>>DZ</option>
                                <option value="CJ" <?php if(set_value('unidade') == 'CJ'){echo 'selected';}?>>CJ</option>
                                <option value="MT" <?php if(set_value('unidade') == 'MT'){echo 'selected';}?>>MT</option>
                                <option value="M2" <?php if(set_value('unidade') == 'M2'){echo 'selected';}?>>M2</option>
                                <option value="M3" <?php if(set_value('unidade') == 'M3'){echo 'selected';}?>>M3</option>
                                <option value="KG" <?php if(set_value('unidade') == 'KG'){echo 'selected';}?>>KG</option>
                                <option value="FRD" <?php if(set_value('unidade') == 'FRD'){echo 'selected';}?>>FRD</option>
                                <option value="PCT" <?php if(set_value('unidade') == 'PCT'){echo 'selected';}?>>PCT</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" style="display: none;">
                        <label for="grupoProduto_id" class="control-label">Grupo de produto</label>
                        <div class="controls">
                            <select name="grupoProduto_id" class="form-control">
                                <option value="">Selecione um grupo de produto</option>
                                <?php
                                foreach($gruposProduto as $grupoproduto){
                                    echo '<option value="'.$grupoproduto->idGrupoProduto.'">'.$grupoproduto->nome.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="control-group" style="display: none;">
                        <label for="setor_id" class="control-label">Setor</label>
                        <div class="controls">
                            <select name="setor_id" class="form-control">
                                <option value="">Selecione um setor</option>
                                <?php
                                foreach($setores as $setor){
                                    echo '<option value="'.$setor->idSetor.'">'.$setor->nome.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!--
					<div class="control-group">
                        <label for="ean" class="control-label">EAN</label>
                        <div class="controls">
                            <input id="ean" type="text" name="ean" value="<?php echo set_value('ean'); ?>"  />
                        </div>
                    </div>
					!-->

                    <!--
					<div class="control-group">
                        <label for="ncm" class="control-label">NCM<a href="http://www4.receita.fazenda.gov.br/simulador/PesquisarNCM.jsp" target="_blank"> (Pesquisar NCM)</a></label>
                        <div class="controls">
                            <input id="ncm" type="text" name="ncm" value="<?php echo set_value('ncm'); ?>"  />
                        </div>
                    </div>
					!-->

                    <div class="control-group" style="display: none;">
                        <label for="precoCompra" class="control-label">Preço de Compra</label>
                        <div class="controls">
                            <input id="precoCompra" class="money" type="text" name="precoCompra" value="<?php echo set_value('precoCompra'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group" style="display: none;">
                        <label for="precoVenda" class="control-label">Preço de Venda</label>
                        <div class="controls">
                            <input id="precoVenda" class="money" type="text" name="precoVenda" value="<?php echo set_value('precoVenda'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group" style="display: none;">
                        <label for="comissao" class="control-label">Comissão</label>
                        <div class="controls">
                            <input id="comissao" class="money" type="text" name="comissao" value="<?php echo set_value('comissao'); ?>"  /> %
                        </div>
                    </div>

                    <div class="control-group" style="display: none;">
                        <label for="estoque" class="control-label">Estoque Atual</label>
                        <div class="controls">
                            <input id="estoque" type="text" name="estoque" value="<?php echo set_value('estoque'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group" style="display: none;">
                        <label for="estoqueMinimo" class="control-label">Estoque Mínimo</label>
                        <div class="controls">
                            <input id="estoqueMinimo" type="text" name="estoqueMinimo" value="<?php echo set_value('estoqueMinimo'); ?>"  />
                        </div>
                    </div>

                    <!--
                     <div class="control-group">
                        <label for="observacao" class="control-label">Observação</label>
                        <div class="controls">
                            <textarea id="observacao" name="observacao"><?php echo set_value('descricao'); ?></textarea>
                        </div>
                    </div>
					!-->

                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Cadastrar</button>
                            <a href="<?php echo base_url() ?>index.php/produtos" id="" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#formProduto').validate({
            rules :{
                descricao:          { required: true},
                unidade:            { required: true}
             },
            messages:{
                descricao:          { required: 'Campo Requerido.'},
                unidade:            {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
    });
</script>



