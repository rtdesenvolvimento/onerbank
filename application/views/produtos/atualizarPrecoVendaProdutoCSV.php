<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Atualizar Preço de Venda de produtos <small>CSV</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a class="btn btn-primary" href="<?php echo base_url().'/assets/images/exemplo-atualizar-preco-venda-produto.csv' ;?>" ><i class="fa fa-download"></i> Baixar Arquivo de Exemplo</a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo base_url(); ?>produtos/atualizarPrecoVenda" id="formProduto" enctype="multipart/form-data" method="post" class="form-horizontal" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="alert alert-info alert-dismissible " role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Informação!</strong><br/>
                                A primeira linha no arquivo CSV baixado deve permanecer como está. Por favor, não alterar a ordem das colunas.<br/>
                                A ordem da coluna correta é (Codigo, Preco venda, Filial)<br/>
                                O sistema irá atualizar o preço de venda do produto buscando pelo código conforme informado na arquivo.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="control-group" style="text-align: left;">
                                <div class="controls">
                                    <input type="file" required="required" accept=".csv" name="arquivoproduto">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Importar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
