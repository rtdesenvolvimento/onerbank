<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-shopping-cart"></i> Consulta de estoque (<?php echo count($results); ?>)</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12">
                    <form action="<?php echo base_url() ?>index.php/consultaestoque/consultaEstoque" method="post">
                        <div class="span12" style="margin-left: 0">
                            <div class="well" style="overflow: auto">
                                <div class="input-group form-group row pull-right top_search">
                                    <select class="form-control" id="tipoConsultaEstoque" name="tipoConsultaEstoque">
                                        <option>TODOS</option>
                                        <option <?php if($tipoConsultaEstoque == 'COMSALDO') echo 'selected="selected"';?> value="COMSALDO">COM ESTOQUE</option>
                                        <option <?php if($tipoConsultaEstoque == 'SEMSALDO') echo 'selected="selected"';?> VALUE="SEMSALDO">SEM ESTOQUE</option>
                                    </select>
                                    <input type="text" class="form-control" id="termo" name="termo" value="<?php if($termo) echo $termo ;?>" placeholder="Digite o termo da pesquisa de produtos">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 col-sm-12">
                    <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                        <thead>
                        <tr style="backgroud-color: #2D335B">
                            <th style="text-align: left;">Filial</th>
                            <th style="text-align: right;">Estoque</th>
                            <!--
                            <th style="width: 10%;text-align: right;">Reservado</th>
                            <th style="width: 10%;text-align: right;">Bloqueado</th>
                            <th style="width: 10%;text-align: right;">Disponível</th>
                            !-->
                            <th style="text-align: right;">Preço compra</th>
                            <th style="text-align: right;">Preço venda</th>
                            <th style="text-align: right;">Estoque Mínimo</th>
                            <th style="text-align: center;">Localização</th>
                            <th style="text-align: center;">Histórico</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($results as $r) {
                            $unidade  = $r->unidade;

                            echo '<tr>';
                            echo '<td> '.$r->produto.'<br/><small>'. $r->filial . '</small></td>';
                            echo '<td style="text-align: right;">' . number_format($r->estoque,3,',','.')   . '</td>';
                            //echo '<td style="text-align: right;">' . $r->reservado . '</td>';
                            //echo '<td style="text-align: right;">' . $r->bloqueado . '</td>';
                            //echo '<td style="text-align: right;"><b>' . ($r->estoque - $r->reservado - $r->bloqueado).' '. $r->unidade . '</b></td>';
                            echo '<td style="text-align: right;">R$' . $r->precoCompra . '</td>';
                            echo '<td style="text-align: right;">R$' . $r->precoVenda . '</td>';
                            echo '<td style="text-align: right;">' . $r->estoqueMinimo . '</td>';
                            echo '<td style="text-align: center;">' . $r->localizacao . '</td>';
                            echo '<td style="text-align: center;">';
                            if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
                                echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/consultaestoque/historicoEstoque/' . $r->idProdutos.'/'.$r->filial_id . '"><button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-calculator"> </i> </button></a>  ';
                            }
                            echo '</td>';
                            echo '</tr>';
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Produto</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#termo").keypress(function (event) {
            if (event.which === 13) {
                var tipoConsultaEstoque = $('#tipoConsultaEstoque').val();
                window.location.href = "<?php echo base_url() ?>index.php/consultaestoque/consultaEstoque/" + $(this).val()+'/'+tipoConsultaEstoque;
            }
        });

        $('#buscar').click(function (event) {
            var tipoConsultaEstoque = $('#tipoConsultaEstoque').val();
            window.location.href = "<?php echo base_url() ?>index.php/consultaestoque/consultaEstoque/" + $(this).val()+'/'+tipoConsultaEstoque;
        });
    });
</script>