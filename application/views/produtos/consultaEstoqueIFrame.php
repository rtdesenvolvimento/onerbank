
<br/><br/>
<form action="<?php echo base_url() ?>index.php/consultaestoque/consultaEstoqueIFrame" method="post">
    <div class="span12" style="margin-left: 0">

        <div class="span2">
            <select style="width: 100%" id="tipoConsultaEstoque" name="tipoConsultaEstoque">
                <option>TODOS</option>
                <option <?php if($tipoConsultaEstoque == 'COMSALDO') echo 'selected="selected"';?> value="COMSALDO">COM ESTOQUE</option>
                <option <?php if($tipoConsultaEstoque == 'SEMSALDO') echo 'selected="selected"';?> VALUE="SEMSALDO">SEM ESTOQUE</option>
            </select>
        </div>

        <div class="span6">
            <input type="text" style="width: 100%" class="span12" id="termo" name="termo" value="<?php if($termo) echo $termo ;?>" placeholder="Digite o termo da pesquisa de produtos">
        </div>

        <div class="span3">
            <button class="span12 btn" style="width: 100%" id="buscar"> <i class="icon-search"></i> </button>
        </div>
    </div>
</form>
<?php

if (!$results) {
    ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-barcode"></i>
         </span>
            <h5>Produtos</h5>

        </div>

        <div class="widget-content nopadding">
            <br>
            <br>
            <table width="100%">
                <tr>
                    <td>
                        <div class="dataTables_filter" id="fileData_filter">
                            <label>Buscar: <input type="text" id="buscar" class="form-control input-xs"></label>
                        </div>
                    </td>
                </tr>
            </table>

            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>#</th>
                    <!--<th>Grupo</th>!-->
                    <!--<th>Setor</th>!-->
                    <th>Nome</th>
                    <th>Estoque</th>
                    <th>Preço</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td colspan="5">Nenhum Produto Cadastrado</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

<?php } else { ?>

    <div class="widget-box">

        <div class="widget-title">
        <span class="icon">
            <i class="icon-barcode"></i>
         </span>
            <h5>Consulta de estoque</h5>
        </div>

        <div class="widget-content nopadding">

            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="text-align: left;">Filial</th>
                    <th style="text-align: right;">Estoque</th>
                    <!--
                    <th style="width: 10%;text-align: right;">Reservado</th>
                    <th style="width: 10%;text-align: right;">Bloqueado</th>
                    <th style="width: 10%;text-align: right;">Disponível</th>
                    !-->
                    <th style="text-align: right;">Preço venda</th>
                    <th style="text-align: right;">Estoque Mínimo</th>
                    <th style="text-align: center;">Localização</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($results as $r) {
                    $unidade  = $r->unidade;

                    echo '<tr>';
                    echo '<td> '.$r->produto.'</td>';
                    echo '<td style="text-align: right;">' . number_format($r->estoque,3,',','.')   . '</td>';
                    //echo '<td style="text-align: right;">' . $r->reservado . '</td>';
                    //echo '<td style="text-align: right;">' . $r->bloqueado . '</td>';
                    //echo '<td style="text-align: right;"><b>' . ($r->estoque - $r->reservado - $r->bloqueado).' '. $r->unidade . '</b></td>';
                    echo '<td style="text-align: right;">R$' . $r->precoVenda . '</td>';
                    echo '<td style="text-align: right;">' . $r->estoqueMinimo . '</td>';
                    echo '<td style="text-align: center;">' . $r->localizacao . '</td>';
                    echo '<td style="text-align: center;">';
                    echo '</td>';
                    echo '</tr>';
                } ?>
                <tr>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php
} ?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Produto</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>
