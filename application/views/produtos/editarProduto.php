<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de produto <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row-fluid" style="margin-top:0">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-content nopadding">
                                <ul class="nav nav-tabs">
                                    <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab"><i class="fa fa-edit"></i> Detalhes do Produto</a></li>
                                    <li id="tabFornecedor"><a href="#tab2" data-toggle="tab"><i class="fa fa-users"></i> Fornecedores</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- DETALHES DOD PRODUTO !-->
                                    <div class="tab-pane active" id="tab1">
                                        <?php echo $custom_error; ?>
                                        <form action="<?php echo current_url(); ?>" id="formProduto" enctype="multipart/form-data" method="post"
                                              class="form-horizontal">
                                            <div class="col-md-12 col-sm-12" style="text-align: left;">
                                                <label for="cProd" class="control-label">Imagem pricipal<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="hidden" value="<?php echo $result->imagem;?>" name="imagem">
                                                    <img src="<?php echo base_url().'/assets/arquivos/produto/'.$result->idProdutos.'/'.$result->imagem; ?>" height="150px" width="150px">
                                                </div>
                                            </div>
                                            <div class="control-group" style="text-align: left;">
                                                <div class="controls">
                                                    <input type="file" name="arquivoproduto">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-6">
                                                <label for="cProd" class="control-label">Código<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input id="cProd" class="form-control" type="text" name="cProd" required="required" value="<?php echo $result->cProd; ?>"  />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <?php echo form_hidden('idProdutos', $result->idProdutos) ?>
                                                <label for="descricao" class="control-label">Descrição<span
                                                        class="required">*</span></label>
                                                <div class="controls">
                                                    <input id="descricao" type="text"  class="form-control" name="descricao"
                                                           value="<?php echo $result->descricao; ?>"/>
                                                </div>
                                            </div>
                                            <div <div class="col-md-2 col-sm-6">
                                                <label for="unidade" class="control-label">Unidade<span
                                                        class="required">*</span></label>
                                                <div class="controls">
                                                    <input id="unidade" type="text" class="form-control" name="unidade"
                                                           value="<?php echo $result->unidade; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label for="grupoProduto_id" class="control-label">Grupo de produto<span
                                                        class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="grupoProduto_id" required="required" class="form-control">
                                                        <option value="">Selecione um grupo de produto</option>
                                                        <?php
                                                        foreach ($gruposProduto as $grupoproduto) {
                                                            $sel = '';
                                                            if ($grupoproduto->idGrupoProduto == $result->grupoProduto_id) {
                                                                $sel = 'selected="selected"';
                                                            }
                                                            echo '<option value="' . $grupoproduto->idGrupoProduto . '" ' . $sel . '>' . $grupoproduto->nome . '</option>';
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label for="cEAN" class="control-label">Código de Barras/EAN</label>
                                                <div class="controls">
                                                    <input id="cEAN" type="text" class="form-control" name="cEAN" value="<?php echo $result->cEAN; ?>"  />
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="controls">
                                                    <input id="permiteEstoqueNegativo" <?php if($result->permiteEstoqueNegativo == 1) echo 'checked="checked"';?> type="checkbox" value="1" name="permiteEstoqueNegativo"/> Permite estoque negativo?
                                                </div>
                                            </div>
                                            <div class="control-group" style="display: none;" >
                                                <label for="usarInformacoesFiscaisProduto" class="control-label">Usar informações fiscais do produto</label>
                                                <div class="controls">
                                                    <input id="usarInformacoesFiscaisProduto" <?php if($result->usarInformacoesFiscaisProduto == 1) echo  'checked="checked"';?> value="1" type="checkbox" name="usarInformacoesFiscaisProduto"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12" style="margin-top: 20px;">
                                                    <div class="x_panel">
                                                        <div class="x_title">
                                                            <h2> <i class="fa fa-bank"></i> Informações fiscais <small>configuração para emissão da nota fiscal de produto (NF-e / NFC-e)</small></h2>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="x_content">
                                                            <div class="span12" style="margin-left: 0">
                                                                <ul class="nav nav-tabs">
                                                                    <li class="active"><a href="#fiscais" data-toggle="tab">Dados Fiscais</a></li>
                                                                    <li><a href="#combustiveis" data-toggle="tab">Combustível</a></li>
                                                                    <li><a href="#ICMS" data-toggle="tab">ICMS</a></li>
                                                                    <li><a href="#IPI" data-toggle="tab">IPI</a></li>
                                                                    <li><a href="#PIS" data-toggle="tab">PIS</a></li>
                                                                    <li><a href="#COFINS" data-toggle="tab">COFINS</a></li>
                                                                </ul>
                                                                <div class="tab-content">
                                                                    <div class="tab-pane active" id="fiscais">
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="CFOP" class="control-label">Origem</label>
                                                                            <div class="controls">
                                                                                <select name="origem" class="form-control" required="required">
                                                                                    <option <?php if ($result->origem == 0) echo 'selected="selected"';?> value="0"> Nacional </option>
                                                                                    <option <?php if ($result->origem == 1) echo 'selected="selected"';?>  value="1"> Estrangeira </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="ncm" class="control-label">NCM</label>
                                                                            <div class="controls">
                                                                                <input id="ncm" type="text" class="form-control" name="ncm" value="<?php echo $result->ncm; ?>">
                                                                                <a href="#searchNCM" id="aSearchNCM" class="search_lupa" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="CEST" class="control-label">CEST</label>
                                                                            <div class="controls">
                                                                                <input id="CEST" type="text" class="form-control" name="CEST" value="<?php echo $result->CEST; ?>">
                                                                                <a href="#searchCEST" id="aSearchCEST" class="search_lupa" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="CFOP" class="control-label">CFOP (<small>Deixe em branco para usar o CFOP da Natureza de operação da nota fiscal</small>)</label>
                                                                            <div class="controls">
                                                                                <input id="CFOP" type="text" class="form-control" name="CFOP" value="<?php echo $result->CFOP; ?>">
                                                                                <a href="#searchCFOP" id="aSearchCFOP" class="search_lupa" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <label for="CFOP" class="control-label">% IBPT</label>
                                                                            <div class="controls">
                                                                                <input id="percentualIBPT" class="form-control" type="number" name="percentualIBPT" value="<?php echo $result->percentualIBPT; ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <label for="infoNota" class="control-label">Inf. Nota fiscal</label>
                                                                            <div class="controls">
                                                                                <textarea id="infoNota" class="form-control" rows="4" cols="8" name="infoNota"><?php echo $result->infoNota; ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="combustiveis">
                                                                        <div class="col-md-4 col-sm-4">
                                                                            <label for="cProdANP" class="control-label">Código de produto da ANP</label>
                                                                            <div class="controls">
                                                                                <input id="cProdANP" type="text" class="form-control" name="cProdANP"
                                                                                       value="<?php echo $result->cProdANP; ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4">
                                                                            <label for="descANP" class="control-label">Desc ANP</label>
                                                                            <div class="controls">
                                                                                <input id="descANP" type="text"class="form-control"  name="descANP"
                                                                                       value="<?php echo $result->descANP; ?>"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4">
                                                                            <label for="UFCons" class="control-label">UF de consumo</label>
                                                                            <div class="controls">
                                                                                <input id="UFCons" type="text"class="form-control"  name="UFCons"
                                                                                       value="<?php echo $result->UFCons; ?>"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="ICMS">
                                                                        <div class="col-md-4 col-sm-4">
                                                                            <label for="CST" class="control-label">CST/CSOSN</label>
                                                                            <div class="controls">
                                                                                <select name="CST" class="form-control">
                                                                                    <option value=""></option>
                                                                                    <?php
                                                                                    foreach ($icmsAll as $icms) {
                                                                                        $sel = '';
                                                                                        if ($icms->codigo_icms == $result->CST) {
                                                                                            $sel = 'selected="selected"';
                                                                                        }
                                                                                        echo '<option value="' . $icms->codigo_icms . '" ' . $sel . '>' . $icms->desc_icms . '</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4">
                                                                            <label for="modBCST" class="control-label">Determ. da BC ICMS ST</label>
                                                                            <div class="controls">
                                                                                <select name="modBCST" class="form-control" >
                                                                                    <option <?php if ($result->modBCST == '') echo 'selected="selected"';?> value=""></option>
                                                                                    <option <?php if ($result->modBCST == '0') echo 'selected="selected"';?> value="0"> Preço tabelado ou máximo sugerido; </option>
                                                                                    <option <?php if ($result->modBCST == '1') echo 'selected="selected"';?> value="1"> Lista Negativa (valor) </option>
                                                                                    <option <?php if ($result->modBCST == '2') echo 'selected="selected"';?> value="2"> Lista Positiva (valor) </option>
                                                                                    <option <?php if ($result->modBCST == '3') echo 'selected="selected"';?> value="3"> Lista Neutra (valor)</option>
                                                                                    <option <?php if ($result->modBCST == '4') echo 'selected="selected"';?> value="4"> Margem Valor Agregado (%)</option>
                                                                                    <option <?php if ($result->modBCST == '5') echo 'selected="selected"';?> value="5"> Pauta (valor)</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4">
                                                                            <label for="pICMSST" class="control-label">Alíquota(%) ICMS ST</label>
                                                                            <div class="controls">
                                                                                <input id="pICMSST" type="number" class="form-control" name="pICMSST" value="<?php echo $result->pICMSST; ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="IPI">
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="CSTIPI" class="control-label">IPI</label>
                                                                            <div class="controls">
                                                                                <select name="CSTIPI" id="CSTIPI" class="form-control" >
                                                                                    <option <?php if ($result->CSTIPI == '') echo 'selected="selected"';?> value=""></option>
                                                                                    <option <?php if ($result->CSTIPI == '00') echo 'selected="selected"';?> value="00">00: Entrada com recuperação de crédito</option>
                                                                                    <option <?php if ($result->CSTIPI == '01') echo 'selected="selected"';?> value="01">01: Entrada tributada com alíquota zero </option>
                                                                                    <option <?php if ($result->CSTIPI == '02') echo 'selected="selected"';?> value="02">02: Entrada isenta</option>
                                                                                    <option <?php if ($result->CSTIPI == '03') echo 'selected="selected"';?> value="03">03: Entrada não-tributada</option>
                                                                                    <option <?php if ($result->CSTIPI == '04') echo 'selected="selected"';?> value="04">04: Entrada imune</option>
                                                                                    <option <?php if ($result->CSTIPI == '05') echo 'selected="selected"';?> value="05">05: Entrada com suspensão</option>
                                                                                    <option <?php if ($result->CSTIPI == '49') echo 'selected="selected"';?> value="49">49: Outras entradas</option>
                                                                                    <option <?php if ($result->CSTIPI == '50') echo 'selected="selected"';?> value="50">50: Saída tributada</option>
                                                                                    <option <?php if ($result->CSTIPI == '51') echo 'selected="selected"';?> value="51">51: Saída tributada com alíquota zero</option>
                                                                                    <option <?php if ($result->CSTIPI == '52') echo 'selected="selected"';?> value="52">52: Saída isenta</option>
                                                                                    <option <?php if ($result->CSTIPI == '53') echo 'selected="selected"';?> value="53">53: Saída não-tributada</option>
                                                                                    <option <?php if ($result->CSTIPI == '54') echo 'selected="selected"';?> value="54">54: Saída imune</option>
                                                                                    <option <?php if ($result->CSTIPI == '55') echo 'selected="selected"';?> value="55">55: Saída com suspensão</option>
                                                                                    <option <?php if ($result->CSTIPI == '99') echo 'selected="selected"';?> value="99">99: Outras saídas</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="pIPI" class="control-label">Alíquota(%) IPI</label>
                                                                            <div class="controls">
                                                                                <input id="pIPI" type="number" class="form-control" name="pIPI" value="<?php echo $result->pIPI; ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="PIS">
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="CSTPIS" class="control-label">IPI</label>
                                                                            <div class="controls">
                                                                                <select name="CSTPIS" id="CSTPIS" class="form-control" >
                                                                                    <option <?php if ($result->CSTPIS == '') echo 'selected="selected"';?> value=""></option>
                                                                                    <option <?php if ($result->CSTPIS == '01') echo 'selected="selected"';?> value="01">01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.)</option>
                                                                                    <option <?php if ($result->CSTPIS == '02') echo 'selected="selected"';?> value="02">02: Operação tributável (BC = valor da operação (alíquota diferenciada)</option>
                                                                                    <option <?php if ($result->CSTPIS == '03') echo 'selected="selected"';?> value="03">03: Operação tributável (BC = quant. x alíq. por unidade de produto)</option>
                                                                                    <option <?php if ($result->CSTPIS == '04') echo 'selected="selected"';?> value="04">04: Operação tributável (tributação monofásica, alíquota zero)</option>
                                                                                    <option <?php if ($result->CSTPIS == '06') echo 'selected="selected"';?> value="06">06: Operação tributável (alíquota zero)</option>
                                                                                    <option <?php if ($result->CSTPIS == '07') echo 'selected="selected"';?> value="07">07: Operação isenta da contribuição</option>
                                                                                    <option <?php if ($result->CSTPIS == '08') echo 'selected="selected"';?> value="08">08: Operação sem incidência da contribuição</option>
                                                                                    <option <?php if ($result->CSTPIS == '09') echo 'selected="selected"';?> value="09">09: Operação com suspensão da contribuição</option>
                                                                                    <option <?php if ($result->CSTPIS == '49') echo 'selected="selected"';?> value="49">49: Outras Operações de Saída</option>
                                                                                    <option <?php if ($result->CSTPIS == '50') echo 'selected="selected"';?> value="50">50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTPIS == '51') echo 'selected="selected"';?> value="51">51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTPIS == '52') echo 'selected="selected"';?> value="52">52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '53') echo 'selected="selected"';?> value="53">53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTPIS == '54') echo 'selected="selected"';?> value="54">54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '55') echo 'selected="selected"';?> value="55">55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '56') echo 'selected="selected"';?> value="56">56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '60') echo 'selected="selected"';?> value="60">60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTPIS == '61') echo 'selected="selected"';?> value="61">61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTPIS == '62') echo 'selected="selected"';?> value="62">62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '63') echo 'selected="selected"';?> value="63">63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTPIS == '64') echo 'selected="selected"';?> value="64">64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '65') echo 'selected="selected"';?> value="65">65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '66') echo 'selected="selected"';?> value="66">66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                                                                    <option <?php if ($result->CSTPIS == '67') echo 'selected="selected"';?> value="67">67: Crédito Presumido - Outras Operações</option>
                                                                                    <option <?php if ($result->CSTPIS == '70') echo 'selected="selected"';?> value="70">70: Operação de Aquisição sem Direito a Crédito</option>
                                                                                    <option <?php if ($result->CSTPIS == '71') echo 'selected="selected"';?> value="71">71: Operação de Aquisição com Isenção</option>
                                                                                    <option <?php if ($result->CSTPIS == '72') echo 'selected="selected"';?> value="72">72: Operação de Aquisição com Suspensão</option>
                                                                                    <option <?php if ($result->CSTPIS == '73') echo 'selected="selected"';?> value="73">73: Operação de Aquisição a Alíquota Zero</option>
                                                                                    <option <?php if ($result->CSTPIS == '74') echo 'selected="selected"';?> value="74">74: Operação de Aquisição sem Incidência da Contribuição</option>
                                                                                    <option <?php if ($result->CSTPIS == '75') echo 'selected="selected"';?> value="75">75: Operação de Aquisição por Substituição Tributária</option>
                                                                                    <option <?php if ($result->CSTPIS == '98') echo 'selected="selected"';?> value="98">98: Outras Operações de Entrada</option>
                                                                                    <option <?php if ($result->CSTPIS == '99') echo 'selected="selected"';?> value="99">99: Outras operações</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="pPIS" class="control-label">Alíquota(%) PIS</label>
                                                                            <div class="controls">
                                                                                <input id="pPIS" type="number" class="form-control" name="pPIS" value="<?php echo $result->pPIS; ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="COFINS">
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="CSTCOFINS" class="control-label">COFINS</label>
                                                                            <div class="controls">
                                                                                <select name="CSTCOFINS" id="CSTCOFINS" class="form-control" >
                                                                                    <option <?php if ($result->CSTCOFINS == '') echo 'selected="selected"';?> value=""></option>
                                                                                    <option <?php if ($result->CSTCOFINS == '01') echo 'selected="selected"';?> value="01">01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.)</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '02') echo 'selected="selected"';?> value="02">02: Operação tributável (BC = valor da operação (alíquota diferenciada)</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '03') echo 'selected="selected"';?> value="03">03: Operação tributável (BC = quant. x alíq. por unidade de produto)</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '04') echo 'selected="selected"';?> value="04">04: Operação tributável (tributação monofásica, alíquota zero)</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '06') echo 'selected="selected"';?> value="06">06: Operação tributável (alíquota zero)</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '07') echo 'selected="selected"';?> value="07">07: Operação isenta da contribuição</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '08') echo 'selected="selected"';?> value="08">08: Operação sem incidência da contribuição</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '09') echo 'selected="selected"';?> value="09">09: Operação com suspensão da contribuição</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '49') echo 'selected="selected"';?> value="49">49: Outras Operações de Saída</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '50') echo 'selected="selected"';?> value="50">50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '51') echo 'selected="selected"';?> value="51">51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '52') echo 'selected="selected"';?> value="52">52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '53') echo 'selected="selected"';?> value="53">53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '54') echo 'selected="selected"';?> value="54">54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '55') echo 'selected="selected"';?> value="55">55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '56') echo 'selected="selected"';?> value="56">56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '60') echo 'selected="selected"';?> value="60">60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '61') echo 'selected="selected"';?> value="61">61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '62') echo 'selected="selected"';?> value="62">62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '63') echo 'selected="selected"';?> value="63">63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '64') echo 'selected="selected"';?> value="64">64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '65') echo 'selected="selected"';?> value="65">65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '66') echo 'selected="selected"';?> value="66">66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '67') echo 'selected="selected"';?> value="67">67: Crédito Presumido - Outras Operações</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '70') echo 'selected="selected"';?> value="70">70: Operação de Aquisição sem Direito a Crédito</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '71') echo 'selected="selected"';?> value="71">71: Operação de Aquisição com Isenção</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '72') echo 'selected="selected"';?> value="72">72: Operação de Aquisição com Suspensão</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '73') echo 'selected="selected"';?> value="73">73: Operação de Aquisição a Alíquota Zero</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '74') echo 'selected="selected"';?> value="74">74: Operação de Aquisição sem Incidência da Contribuição</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '75') echo 'selected="selected"';?> value="75">75: Operação de Aquisição por Substituição Tributária</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '98') echo 'selected="selected"';?> value="98">98: Outras Operações de Entrada</option>
                                                                                    <option <?php if ($result->CSTCOFINS == '99') echo 'selected="selected"';?> value="99">99: Outras operações</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <label for="pCOFINS" class="control-label">Alíquota(%) COFINS</label>
                                                                            <div class="controls">
                                                                                <input id="pCOFINS" type="number" class="form-control" name="pCOFINS" value="<?php echo $result->pCOFINS; ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 ">
                                                    <div class="x_panel">
                                                        <div class="x_title">
                                                            <h2> <i class="fa fa-building"></i> Filiais <small>configurações customizadas por filial</small></h2>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="x_content">
                                                            <div class="span12" style="margin-left: 0">
                                                                <!--
                                                <ul class="nav nav-tabs">
                                                    <?php
                                                                $contador = 1;
                                                                foreach ($filiais as $filial) {
                                                                    $isExisteProdutoFilial = $this->produtos_model->getProdutoFilial($result->idProdutos, $filial->idFilial);
                                                                    ?>
                                                        <?php if ($contador==1) {?>
                                                            <li class="active"><a href="#<?php echo $filial->idFilial;?>" data-toggle="tab"><?php echo $filial->nome;?></a></li>
                                                        <?php } else { ?>
                                                            <li><a href="#<?php echo $filial->idFilial;?>" data-toggle="tab"><?php echo $filial->nome;?></a></li>
                                                        <?php $contador = $contador + 1;} ?>
                                                    <?php } ?>
                                                </ul>
                                                !-->
                                                                <div class="tab-content" style="margin-top: 15px;">
                                                                    <div class="control-group">

                                                                        <table class="table table-bordered ">
                                                                            <thead>
                                                                            <tr style="backgroud-color: #2D335B">
                                                                                <th style="width: 2%">#</th>
                                                                                <th style="text-align: left;width: 15%;">Filial</th>
                                                                                <th style="width: 10%;text-align: right;">Estoque</th>
                                                                                <!--
                                                                                <th style="width: 10%;text-align: right;">Reservado</th>
                                                                                <th style="width: 10%;text-align: right;">Bloqueado</th>
                                                                                <th style="width: 10%;text-align: right;">Disponível</th>
                                                                                !-->
                                                                                <th style="width: 10%;text-align: right;">Preço compra</th>
                                                                                <th style="width: 10%;text-align: right;">Preço venda</th>
                                                                                <th style="width: 10%;text-align: right;">Estoque Mínimo</th>
                                                                                <th style="width: 10%;text-align: left;">Localização</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tfoot>
                                                                            <tr style="backgroud-color: #2D335B">
                                                                                <th colspan="10"></th>
                                                                            </tr>
                                                                            </tfoot>
                                                                            <tbody>
                                                                            <?php   foreach ($filiais as $filial) {

                                                                                $isExisteProdutoFilial = $this->produtos_model->getProdutoFilial($result->idProdutos, $filial->idFilial);
                                                                                ?>
                                                                                <tr>
                                                                                    <?php if (count($isExisteProdutoFilial) > 0) {?>
                                                                                        <td><input type="checkbox" checked="checked" name="usarFilial<?php echo $filial->idFilial;?>" value="1"> </td>
                                                                                        <td><?php echo $filial->nome;?></td>
                                                                                        <td style="text-align: right">
                                                                                            <input type="number" class="form-control" style="text-align: right;" name="estoque<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->estoque;?>">
                                                                                            <input type="hidden" name="estoque_original<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->estoque;?>" >
                                                                                        </td>
                                                                                        <td style="text-align: right;display: none;"><input type="number" class="form-control" name="reservado<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->reservado;?>" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;display: none;"><input type="number" class="form-control" name="bloqueado<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->bloqueado;?>" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;display: none;"><input type="number" class="form-control" name="disponivel<?php echo $filial->idFilial;?>" readonly value="<?php echo $isExisteProdutoFilial->estoque - $isExisteProdutoFilial->reservado - $isExisteProdutoFilial->bloqueado;?>" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;"><input type="number" class="form-control" name="precoCompra<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->precoCompra;?>" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;"><input type="number" class="form-control" name="precoVenda<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->precoVenda;?>" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;"><input type="number" class="form-control" name="estoqueMinimo<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->estoqueMinimo;?>" style="width: 100%;text-align: right;"></td>
                                                                                        <td><input type="text" class="form-control" name="localizacao<?php echo $filial->idFilial;?>" value="<?php echo $isExisteProdutoFilial->localizacao;?>"></td>
                                                                                    <?php } else { ?>
                                                                                        <td><input type="checkbox" name="usarFilial<?php echo $filial->idFilial;?>" value="1"> </td>
                                                                                        <td><?php echo $filial->nome;?></td>
                                                                                        <td style="text-align: right;">
                                                                                            <input type="number" class="form-control" name="estoque<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;">
                                                                                            <input type="hidden" name="estoque_original<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;">
                                                                                        </td>
                                                                                        <td style="text-align: right;display: none;"><input type="number" class="form-control" name="reservado<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;display: none;"><input type="number" class="form-control" name="bloqueado<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;display: none;"><input type="number" class="form-control" name="disponivel<?php echo $filial->idFilial;?>" readonly value="0" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;"><input type="number" class="form-control" name="precoCompra<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;"><input type="number" class="form-control" name="precoVenda<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;"></td>
                                                                                        <td style="text-align: right;"><input type="number" class="form-control" name="estoqueMinimo<?php echo $filial->idFilial;?>" value="0" style="width: 100%;text-align: right;"></td>
                                                                                        <td><input type="text" class="form-control" name="localizacao<?php echo $filial->idFilial;?>" value=""></td>
                                                                                    <?php } ?>
                                                                                </tr>
                                                                            <?php  }?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>
                                            <div class="item form-group">
                                                <div class="col-md-12" style="text-align: right;">
                                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                                        Alterar
                                                    </button>
                                                    <a href="<?php echo base_url() ?>index.php/produtos" id="" class="btn btn-primary"><i class="fa fa-backward"></i>
                                                        Voltar
                                                    </a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!--FORNECEDOR-->
                                    <div class="tab-pane" id="tab2">
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <form id="formFornecedorProduto"
                                                  action="<?php echo base_url() ?>index.php/produtos/adicionarFornecedorProduto"
                                                  method="post">
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <div class="col-md-8 col-sm-8">
                                                        <label for="">Fornecedor</label>
                                                        <select class="form-control" id="idFornecedor" name="idFornecedor" required="required">
                                                            <option value="">--selecione o fornecedor--</option>
                                                            <?php foreach ($fornecedores as $fornecedor ){?>
                                                                <option id="<?php echo $fornecedor->idFornecedor?>"><?php echo $fornecedor->nome?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <input type="hidden" name="idProduto" id="idProduto"
                                                               value="<?php echo $result->idProdutos; ?>">

                                                        <input type="hidden" name="idFornecedorproduto" id="idFornecedorproduto"
                                                               value=""/>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <label for="codigo">Código</label>
                                                        <input type="text" class="form-control" name="codigo" id="codigo"
                                                               placeholder="Digite o código do produto para o fornecedor" required="required" />
                                                    </div>
                                                </div>
                                                <div class="span12" style="padding: 1%; margin-left: 0">
                                                    <div class="span12">
                                                        <label for="observacao">Observação</label>
                                                        <textarea id="observacao" class="form-control" name="observacao" cols="30"
                                                                  rows="5"><?php echo set_value('observacao'); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="span12">
                                                    <div class="span6 offset3" style="text-align: right;">
                                                        <button class="btn btn-success" id="btnAdicionarProduto"><i
                                                                class="fa fa-save"></i> Salvar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="span12" id="divFornecedor" style="margin-left: 0">
                                            <table class="table table-bordered" id="tblProdutos">
                                                <thead>
                                                <tr>
                                                    <th style="text-align: left;">Fornecedor</th>
                                                    <th style="text-align: left;">Código</th>
                                                    <th style="text-align: left;">Observação</th>
                                                    <th style="text-align: center;width:10%;">Ação</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($fornecedoresveiculos as $fornecedores) {
                                                    echo '<tr>';
                                                    echo '<td style="text-align: left;">' . $fornecedores->nomeFornecedor . '</td>';
                                                    echo '<td style="text-align: left;">' . $fornecedores->codigo . '</td>';
                                                    echo '<td style="text-align: left;">' . $fornecedores->observacao . '</td>';
                                                    echo '<td style="text-align: center;width:10%;">
                                            <span idAcao="' . $fornecedores->idFornecedorproduto . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></span>
                                            <span codigo="' . $fornecedores->codigo . '" 
                                                  observacao="' . $fornecedores->observacao . '"
                                                  idAcaoEditar="' . $fornecedores->idFornecedorproduto . '"
                                                  idFornecedor="' . $fornecedores->idFornecedor . '"
                                                  idProduto="' . $fornecedores->idProduto . '"
                                                  class="btn btn-info"><i class="icon-pencil icon-white"></i></span>

                                          </td>';
                                                    echo '</tr>';
                                                } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal searchNCM -->
<div id="searchNCM" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchNCM"></div>
</div>

<!-- Modal searchCEST -->
<div id="searchCEST" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchCEST"></div>
</div>

<!-- Modal searchCFOP -->
<div id="searchCFOP" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchCFOP"></div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#formProduto').validate({
            rules: {
                descricao: {required: true},
                unidade: {required: true}
            },
            messages: {
                descricao: {required: 'Campo Requerido.'},
                unidade: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $("#formFornecedorProduto").validate({
            rules: {
                idFornecedor: {required: true}
            },
            messages: {},
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $("#divFornecedor").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/salvarFornecedorProduto",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                         if (data.result === true) {
                            $("#divFornecedor").load("<?php echo current_url();?> #divFornecedor");

                            $('#idFornecedorproduto').val('');
                            $("#idFornecedor").val('');
                            $("#observacao").val('');
                            $("#codigo").val('');
                            $("#idFornecedor").val('');
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar o veliculo.');
                        }
                    }
                });
                return false;
            }

        });
        $(document).on('click', 'span', function (event) {

            var idAcao = $(this).attr('idAcao');
            var idAcaoEditar = $(this).attr('idAcaoEditar');

            if ((idAcao % 1) === 0) {
                $("#divFornecedor").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/excluirFornecedorProduto",
                    data: "idFornecedorproduto=" + idAcao,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divFornecedor").load("<?php echo current_url();?> #divFornecedor");

                            $('#idFornecedorproduto').val('');
                            $("#idFornecedor").val('');
                            $("#observacao").val('');
                            $("#codigo").val('');
                            $("#idFornecedor").val('');
                        } else {
                            alert('Ocorreu um erro ao tentar excluir o veiculo.');
                        }
                    }
                });
                return false;
            } else if (idAcaoEditar !== '') {

                var codigo = $(this).attr('codigo');
                var observacao = $(this).attr('observacao');
                var idFornecedor = $(this).attr('idFornecedor');

                $('#idFornecedorproduto').val(idAcaoEditar);

                $("#codigo").val(codigo);
                $("#observacao").val(observacao);
                $('#idFornecedor').val(idFornecedor).trigger('change');
            }
        });

        $('#aSearchNCM').click(function (event) {
            $('#div_searchNCM').load("<?php echo base_url();?>index.php/impostos/searchNCM");
        });

        $('#aSearchCEST').click(function (event) {
            $('#div_searchCEST').load("<?php echo base_url();?>index.php/impostos/searchCEST");
        });

        $('#aSearchCFOP').click(function (event) {
            $('#div_searchCFOP').load("<?php echo base_url();?>index.php/impostos/searchCFOP");
        });

    });

    function fecharModelSerchNCM(cod_ncm) {
        $('#ncm').val(cod_ncm);
        $('#searchNCM').modal('hide');
    }

    function fecharModelSerchCEST(cod) {
        $('#CEST').val(cod);
        $('#searchCEST').modal('hide');
    }

    function fecharModelSerchCFOP(cod) {
        $('#CFOP').val(cod);
        $('#searchCFOP').modal('hide');
    }
</script>




