<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sitemap"></i> Histórico de Estoque de Produto<small>por filial</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                    <thead>
                                        <th style="text-align: center;">Data</th>
                                        <th style="text-align: center;">Tipo</th>
                                        <th style="text-align: center;">Origem</th>
                                        <th style="text-align: right;">Movimentado</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $totalEstoque = 0;
                                    $strProduto = '';
                                    foreach ($results as $r) {
                                        $unidade  = $r->unidade;
                                        $estoque  = $r->estoque ;

                                        if($r->tipo == 'ENTRADA') {
                                            $totalEstoque = $totalEstoque + $r->estoque;
                                        } else {
                                            $estoque = $estoque*-1;
                                            $totalEstoque = $totalEstoque - $r->estoque;
                                        }

                                        if ($strProduto != $r->produto) {
                                            echo '<tr>';
                                            echo '<td colspan="12"><h6>' . $r->produto. ' / '.$r->filial.'<br/>Estoque atual '. number_format( $r->destoqueAtual,3,',','.')   . '</h6></td>';
                                            echo '</tr>';
                                        }

                                        $strProduto = $r->produto;

                                        echo '<tr>';
                                        echo '<td style="text-align: center;">' .  date(('d/m/Y'), strtotime($r->data)) . ' '. $r->hora . '</td>';
                                        echo '<td style="text-align: center;">' . $r->tipo . '</td>';

                                        if ($r->venda_id != null) {
                                            if ($r->nfe_id != null) {
                                                echo '<td style="text-align: center;"> 
                                                        <a style="margin-right: 1%" href="' . base_url() . 'pdv/impressao/' . $r->venda_id . '" target="_blank"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-print"> </i> ' . $r->origem . '</button></a>
                                                        <a style="margin-right: 1%" href="' . base_url() . '/pdv/impressao/' . $r->venda_id . '" target="_blank"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-bicycle"> </i> NFC-e</button></a>
                                                      </td>';

                                            } else {
                                                echo '<td style="text-align: center;"> <a style="margin-right: 1%" href="' . base_url() . 'pdv/impressao/' . $r->venda_id . '" target="_blank"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-print"> </i> ' . $r->origem . '</button></a></td>';
                                            }
                                        } else {
                                            if ($r->nfe_id != null) {
                                                echo '<td style="text-align: center;"> <a style="margin-right: 1%" href="' . base_url() . 'nfce/editar/' . $r->nfe_id . '" target="_blank"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-print"> </i> ' . $r->origem . '</button></a></td>';

                                            } else {
                                                echo '<td style="text-align: center;"> <a>' . $r->origem . '</td>';

                                            }
                                        }
                                        echo '<td style="text-align: right;">' . $estoque.' '. $r->unidade . '</td>';
                                        echo '</tr>';
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Produto</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>