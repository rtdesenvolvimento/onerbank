 <div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-shopping-cart"></i> Produtos (<?php echo count($results); ?>)</small></h2>
                <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aProduto')) { ?>
                    <ul class="nav navbar-right panel_toolbox">
                        <a class="btn btn-success" href="<?php echo base_url(); ?>produtos/adicionar"><i class="fa fa-plus"></i> Cadastrar Produto</a>
                    </ul>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12">
                    <form action="<?php echo base_url() ?>produtos/buscar" method="post">
                        <div class="well" style="overflow: auto">
                            <div class="input-group form-group row pull-right top_search">
                                <input type="text" class="form-control" id="termo" name="termo" value="<?php if($termo) echo $termo ;?>" placeholder="Digite o termo da pesquisa de produtos">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" id="buscar"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="text-align: center;"><i class="fa fa-barcode"></i></th>
                                    <th style="text-align: left;">Nome</th>
                                    <th style="text-align: center;">Grupo</th>
                                    <th style="text-align: center;">CFOP</th>
                                    <th style="text-align: center;">CST</th>
                                    <th style="text-align: center;">CEST</th>
                                    <th style="text-align: center;">NCM</th>
                                    <th style="text-align: right;">Venda</th>
                                    <th style="text-align: right;">Estoque</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {

                                    $estoque = $this->produtos_model->getProdutoFilialById($r->idProdutos);
                                    $qtdEstoque = 0;
                                    $prevoVenda = 0;

                                    if (count($estoque) > 0) {
                                        $qtdEstoque = $estoque->estoque;
                                        $prevoVenda = $estoque->precoVenda;
                                    }

                                    $unidade  = $r->unidade;

                                    echo '<tr>';
                                    echo '<td style="text-align: center;">' . $r->cEAN . '</td>';
                                    echo '<td style="text-align: left;">' . $r->descricao . '</td>';
                                    echo '<td style="text-align: center;">' . $r->grupoproduto . '</td>';
                                    echo '<td style="text-align: center;">' . $r->CFOP . '</td>';
                                    echo '<td style="text-align: center;">' . $r->CST . '</td>';
                                    echo '<td style="text-align: center;">' . $r->CEST . '</td>';
                                    echo '<td style="text-align: center;">' . $r->ncm . '</td>';
                                    echo '<td style="text-align: right;">' . 'R$ '.$this->site_model->formatarValorMonetario($prevoVenda) . '</td>';
                                    echo '<td style="text-align: right;">' . $qtdEstoque.' '.$unidade.'<br/>R$'.$this->site_model->formatarValorMonetario($qtdEstoque*$prevoVenda) . '</td>';
                                    echo '<td style="text-align: center;">';

                                    echo '<div class="btn-group btn-group-sm" role="group" aria-label="...">';
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) {
                                        echo '<a href="' . base_url() . 'index.php/produtos/visualizar/' . $r->idProdutos . '" ><button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-eye"> </i> </button></a>  ';
                                    }
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eProduto')) {
                                        echo '<a href="' . base_url() . 'index.php/produtos/editar/' . $r->idProdutos . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dProduto')) {
                                        echo '<a href="#modal-excluir" role="button" data-toggle="modal" produto="' . $r->idProdutos . '" ><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>';
                                    }
                                    echo '</div>';
                                    echo '</td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/produtos/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Produto</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idProduto" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este produto?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var produto = $(this).attr('produto');
            $('#idProduto').val(produto);
        });
    });
</script>