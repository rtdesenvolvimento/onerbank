<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_content">
                <form  action="<?php echo base_url(); ?>index.php/produtos/search" id="formSearch" method="post">
                    <div class="span12" style="padding: 1%; margin-left: 0">
                        <div class="span12" style="margin-left: 0">
                            <div class="span8" style="margin-left: 0">
                                <input type="text" class="form-control" name="termo" value="<?php echo $termo;?>" placeholder="Digite o termo a pesquisar"/>
                            </div>
                            <div class="span4">
                                <button class="btn btn-primary"><i class="fa fa-search"></i> Pesquisar</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="span12" style="margin-left: 0">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead class="thead_class">
                                <tr>
                                    <th style="text-align: left;width: 250px;">Código</th>
                                    <th style="text-align: left;width: 250px;">Descrição</th>
                                    <th style="text-align: left;width: 250px;">Valor</th>
                                    <th style="text-align: left;width: 250px;">Estoque</th>
                                    <th style="text-align: left;width: 250px;">CFOP</th>
                                    <th style="text-align: left;width: 250px;">CST</th>
                                </tr>
                                </thead>
                                <tbody class="tbody_class">
                                <?php foreach ($results as $r) {
                                    echo '<tr class="searchPreenche" produtos_id='.$r->idProdutos.' style="cursor: pointer;">';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->cProd . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->descricao . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->precoVenda . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->estoque . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->CFOP . '</td>';
                                    echo '    <td style="text-align: left;width: 250px;cursor: pointer;">' . $r->CST . '</td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('.create_links').click(function (event) {
        var url = $(this).attr('href');
        if (url != undefined) {
            $('#div_searchProduto').load(url);
        }
        return false;
    });

    $('#formSearch').submit(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/produtos/search",
            data: $("#formSearch").serialize(),
            dataType: 'html',
            success: function (html) {
                $('#div_searchProduto').html(html);
            }
        });
    });

    $('.searchPreenche').click(function (event) {
        var produtos_id = $(this).attr('produtos_id');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/produtos/consultaProduto",
            data: {produtos_id:produtos_id},
            dataType: 'json',
            success: function (produto) {
                fecharModelSerchProduto(produto);
            }
        });
    });

</script>