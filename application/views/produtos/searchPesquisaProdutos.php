<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_content">
                <form action="" method="post" id="formaAdicionarProdutos">
                    <div class="modal-body">
                        <input type="hidden" value="<?php echo $nNF;?>" name="nNF" id="nNF">
                        <input type="hidden" value="" name="prodnfeid" id="prodnfeid"/>
                        <div class="spa12">
                            <div class="formBuscaGSA">
                                <div class="controls">
                                    <table width="100%">
                                        <tr>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div class="formBuscaGSA" id="divBuscaCliente">
                                                    <div class="form-group has-feedback">
                                                        <input id="cProd"  class="form-control has-feedback-left" required="required" type="text"  name="cProd">
                                                        <a href="#" data-target=".searchProduto" id="aSearchProduto" data-toggle="modal" role="button">
                                                            <span style="display: block;margin-top: 26px;"  class="fa fa-search form-control-feedback left"></span>
                                                        </a>
                                                        <input id="produtoid" name="produtoid" type="hidden"/>
                                                    </div>
                                                </div>
                                            </td>
                                            <td colspan="4">
                                                <input id="xProd" placeholder="Descrição"  class="form-control" required="required" type="text"  name="xProd">
                                            </td>
                                            <td colspan="1">
                                                <input id="uCom" placeholder="Unidade" class="form-control" required="required" type="text"  name="uCom">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label for="qtd" >QTD<span class="required">*</span></label>
                                                <input id="qtd"  class="form-control money" required="required" type="text" name="qtd">
                                            </td>
                                            <td colspan="3">
                                                <label for="vProd" >Valor Venda<span class="required">*</span></label>
                                                <input id="vProd"  class="form-control money" required="required" type="text" name="vProd">
                                            </td>
                                            <td colspan="3">
                                                <label for="vlrtotal" >Valor Total<span class="required">*</span></label>
                                                <input id="vlrtotal"  class="form-control money" readonly required="required" type="text" name="vlrtotal">
                                                <input id="valortotal"  class="form-control" readonly required="required" type="hidden" name="valortotal">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <input id="NCM" placeholder="NCM" class="form-control" required="required" type="text" name="NCM">
                                            </td>
                                            <td colspan="3">
                                                <input id="CEST" placeholder="CEST" class="form-control" type="text" name="CEST">
                                            </td>
                                            <td colspan="3">
                                                <input id="CFOP" placeholder="CFOP" class="form-control" required="required" type="text" name="CFOP">
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td colspan="4">
                                                <label for="prodEspe" >Especifico</label>
                                                <select class="form-control" name="prodEspe">
                                                    <option></option>
                                                    <option value="V">Veículo</option>
                                                    <option value="C">Combustível</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="span12" style="margin-left: 0">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#ICMS" data-toggle="tab">ICMS</a></li>
                                <li><a href="#combustiveis" data-toggle="tab">Combustível</a></li>
                                <li><a href="#IPI" data-toggle="tab">IPI</a></li>
                                <li><a href="#PIS" data-toggle="tab">PIS</a></li>
                                <li><a href="#COFINS" data-toggle="tab">COFINS</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="ICMS">
                                    <div class=" row well">
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="orig">Origem <span class="required">*</span></label>
                                            <select name="orig" class="form-control" required="required">
                                                <option value="0"> Nacional </option>
                                                <option value="1"> Estrangeira </option>
                                            </select>
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="CST">CST/CSOSN <span class="required">*</span></label>
                                            <select name="CST" id="CST" class="form-control" required="required">
                                                <option value=""></option>
                                                <option value="900">Simples Nacional: 900: Outros </option>
                                                <option value="500">Simples Nacional: 500: ICMS cobrado antes por subst trib ou antecipação </option>
                                                <option value="400">Simples Nacional: 400: Não tributada </option>
                                                <option value="300">Simples Nacional: 300: Imune </option>
                                                <option value="203">Simples Nacional: 203: Isenção ICMS p/ faixa de receita bruta e cobr do ICMS por ST </option>
                                                <option value="202">Simples Nacional: 202: Sem permissão de crédito, com cobr ICMS por Subst Trib </option>
                                                <option value="201">Simples Nacional: 201: Com permissão de crédito, com cobr ICMS por Subst Trib </option>
                                                <option value="103">Simples Nacional: 103: Isenção do ICMS para faixa de receita bruta </option>
                                                <option value="102">Simples Nacional: 102: Sem permissão de crédito </option>
                                                <option value="101">Simples Nacional: 101: Com permissão de crédito </option>
                                                <option value="41ST">Repasse 41: ICMS ST retido em operações interestaduais com repasses do Subst Trib </option>
                                                <option value="90Part">Partilha 90: Entre UF origem e destino ou definida na legislação - outros </option>
                                                <option value="10Part">Partilha 10: Entre UF origem e destino ou definida na legislação com Subst Trib </option>
                                                <option value="90">90: Outros </option><option value="70"> 70: Redução de Base Calc e cobr ICMS por subst trib </option>
                                                <option value="60">60: ICMS cobrado anteriormente por subst trib </option>
                                                <option value="51">51: Diferimento </option>
                                                <option value="50">50: Suspesão </option>
                                                <option value="41">41: Não tributada </option>
                                                <option value="40">40: Isenta </option><option value="30"> 30: Isenta ou não trib com cobr por subst trib </option>
                                                <option value="20">20: Com redução de base de cálculo </option>
                                                <option value="10">10: Tributada com cobr. por subst. trib. </option>
                                                <option value="00">00: Tributada integralmente </option>
                                            </select>
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group" style="margin-left: 0;">
                                            <label for="pICMS">(%) ICMS</label>
                                            <input id="pICMS" type="text" name="pICMS" class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vBC">Base ICMS</label>
                                            <input id="vBC" type="text" name="vBC" readonly class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vICMS">Valor ICMS</label>
                                            <input id="vICMS" type="text" name="vICMS" readonly class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group" style="margin-left: 0;">
                                            <label for="pICMSST">(%) ICMS ST</label>
                                            <input id="pICMSST" type="text" name="pICMSST" class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vBCST">Base ICMS ST</label>
                                            <input id="vBCST" type="text" readonly name="vBCST" class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vICMSST">Valor ICMS ST</label>
                                            <input id="vICMSST" type="text" readonly name="vICMSST" class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group" style="padding: 1%; margin-left: 0;display: none;">
                                            <label for="modBCST">Determ. da BC ICMS ST</label>
                                            <select name="modBCST" id="modBCST" class="form-control">
                                                <option value="">Selecione uma opção</option>
                                                <option value="0"> Preço tabelado ou máximo sugerido; </option>
                                                <option value="1"> Lista Negativa (valor) </option>
                                                <option value="2"> Lista Positiva (valor) </option>
                                                <option value="3"> Lista Neutra (valor)</option>
                                                <option value="4"> Margem Valor Agregado (%)</option>
                                                <option value="5"> Pauta (valor)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="combustiveis">
                                    <div class="row well">
                                        <div class="col-md-4 col-sm-12  form-group">
                                            <label for="cProdANP" class="control-label">Código de produto da ANP</label>
                                            <input id="cProdANP" type="text" name="cProdANP" class="form-control" value=""/>
                                        </div>
                                        <div class="col-md-4 col-sm-12  form-group">
                                            <label for="descANP" class="control-label">Desc ANP</label>
                                            <input id="descANP" type="text" class="form-control" name="descANP"
                                                       value=""/>
                                        </div>
                                        <div class="col-md-4 col-sm-12  form-group">
                                            <label for="UFCons" class="control-label">UF de consumo</label>
                                            <input id="UFCons" type="text" class="form-control" name="UFCons"
                                                   value=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="IPI">
                                    <div class="row well">
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="dataInicial">Situação Trib. <span class="required">*</span></label>
                                            <select name="CSTIPI" id="CSTIPI" class="form-control">
                                                <option></option>
                                                <option value="00">00: Entrada com recuperação de crédito </option>
                                                <option value="01">01: Entrada tributada com alíquota zero </option>
                                                <option value="02">02: Entrada isenta </option>
                                                <option value="03">03: Entrada não-tributada </option>
                                                <option value="04">04: Entrada imune </option>
                                                <option value="05">05: Entrada com suspensão </option>
                                                <option value="49">49: Outras entradas </option>
                                                <option value="50">50: Saída tributada </option>
                                                <option value="51">51: Saída tributada com alíquota zero </option>
                                                <option value="52">52: Saída isenta </option>
                                                <option value="53">53: Saída não-tributada </option>
                                                <option value="54">54: Saída imune </option>
                                                <option value="55">55: Saída com suspensão </option>
                                                <option value="99">99: Outras saídas </option>
                                            </select>
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="pIPI" class="control-label">(%) IPI</label>
                                            <input id="pIPI" type="text" name="pIPI" class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vBCIPI" class="control-label">Base</label>
                                            <input id="vBCIPI" type="text" readonly name="vBCIPI" class="form-control money" value="">
                                        </div>

                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vIPI" class="control-label">Valor IPI</label>
                                            <input id="vIPI" type="text" readonly name="vIPI" class="form-control money" value="">
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="PIS">
                                    <div class="row well">
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="dataInicial">Situação Trib. <span class="required">*</span></label>
                                            <select name="CSTPIS" id="CSTPIS" class="form-control">
                                                <option value=""></option>
                                                <option value="01"> 01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.) </option>
                                                <option value="02"> 02: Operação tributável (BC = valor da operação (alíquota diferenciada) </option>
                                                <option value="03"> 03: Operação tributável (BC = quant. x alíq. por unidade de produto) </option>
                                                <option value="04"> 04: Operação tributável (tributação monofásica, alíquota zero) </option>
                                                <option value="06"> 06: Operação tributável (alíquota zero) </option>
                                                <option value="07"> 07: Operação isenta da contribuição </option>
                                                <option value="08"> 08: Operação sem incidência da contribuição </option>
                                                <option value="09"> 09: Operação com suspensão da contribuição </option>
                                                <option value="49"> 49: Outras Operações de Saída </option>
                                                <option value="50"> 50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno </option>
                                                <option value="51"> 51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno </option>
                                                <option value="52"> 52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação </option>
                                                <option value="53"> 53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno </option>
                                                <option value="54"> 54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação </option>
                                                <option value="55"> 55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação </option>
                                                <option value="56"> 56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                                <option value="60"> 60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno </option>
                                                <option value="61"> 61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno </option>
                                                <option value="62"> 62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação </option>
                                                <option value="63"> 63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno </option>
                                                <option value="64"> 64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação </option>
                                                <option value="65"> 65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação </option>
                                                <option value="66"> 66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                                <option value="67"> 67: Crédito Presumido - Outras Operações </option>
                                                <option value="70"> 70: Operação de Aquisição sem Direito a Crédito </option>
                                                <option value="71"> 71: Operação de Aquisição com Isenção </option>
                                                <option value="72"> 72: Operação de Aquisição com Suspensão </option>
                                                <option value="73"> 73: Operação de Aquisição a Alíquota Zero </option>
                                                <option value="74"> 74: Operação de Aquisição sem Incidência da Contribuição </option>
                                                <option value="75"> 75: Operação de Aquisição por Substituição Tributária </option>
                                                <option value="98"> 98: Outras Operações de Entrada </option>
                                                <option value="99"> 99: Outras operações </option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="pPIS" class="control-label">(%) PIS</label>
                                            <input id="pPIS" type="text" name="pPIS" class="form-control money" value="">
                                        </div>
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vBCPIS" class="control-label">Base</label>
                                            <input id="vBCPIS" type="text" readonly name="vBCPIS" class="form-control money" value="">
                                        </div>
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vPIS" class="control-label">Valor PIS</label>
                                            <input id="vPIS" type="text" readonly name="vPIS" class="form-control money" value="">
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="COFINS">
                                    <div class="row well">
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="dataInicial">Situação Trib. <span class="required">*</span></label>
                                            <select name="CSTCOFINS" id="CSTCOFINS" class="form-control">
                                                <option value=""></option>
                                                <option value="01"> 01: Operação tributável (BC = Operação alíq. normal (cumul./não cumul.) </option>
                                                <option value="02"> 02: Operação tributável (BC = valor da operação (alíquota diferenciada) </option>
                                                <option value="03"> 03: Operação tributável (BC = quant. x alíq. por unidade de produto) </option>
                                                <option value="04"> 04: Operação tributável (tributação monofásica, alíquota zero) </option>
                                                <option value="06"> 06: Operação tributável (alíquota zero) </option>
                                                <option value="07"> 07: Operação isenta da contribuição </option>
                                                <option value="08"> 08: Operação sem incidência da contribuição </option>
                                                <option value="09"> 09: Operação com suspensão da contribuição </option>
                                                <option value="49"> 49: Outras Operações de Saída </option>
                                                <option value="50"> 50: Direito a Crédito. Vinculada Exclusivamente a Receita Tributada no Mercado Interno </option>
                                                <option value="51"> 51: Direito a Crédito. Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno </option>
                                                <option value="52"> 52: Direito a Crédito. Vinculada Exclusivamente a Receita de Exportação </option>
                                                <option value="53"> 53: Direito a Crédito. Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno </option>
                                                <option value="54"> 54: Direito a Crédito. Vinculada a Receitas Tributadas no Mercado Interno e de Exportação </option>
                                                <option value="55"> 55: Direito a Crédito. Vinculada a Receitas Não-Trib. no Mercado Interno e de Exportação </option>
                                                <option value="56"> 56: Direito a Crédito. Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                                <option value="60"> 60: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita Tributada no Mercado Interno </option>
                                                <option value="61"> 61: Créd. Presumido. Aquisição Vinc. Exclusivamente a Rec. Não-Trib. no Mercado Interno </option>
                                                <option value="62"> 62: Créd. Presumido. Aquisição Vinc. Exclusivamente a Receita de Exportação </option>
                                                <option value="63"> 63: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. no Mercado Interno </option>
                                                <option value="64"> 64: Créd. Presumido. Aquisição Vinc. a Rec. Trib. no Mercado Interno e de Exportação </option>
                                                <option value="65"> 65: Créd. Presumido. Aquisição Vinc. a Rec. Não-Trib. Mercado Interno e Exportação </option>
                                                <option value="66"> 66: Créd. Presumido. Aquisição Vinc. a Rec. Trib. e Não-Trib. Mercado Interno e Exportação </option>
                                                <option value="67"> 67: Crédito Presumido - Outras Operações </option>
                                                <option value="70"> 70: Operação de Aquisição sem Direito a Crédito </option>
                                                <option value="71"> 71: Operação de Aquisição com Isenção </option>
                                                <option value="72"> 72: Operação de Aquisição com Suspensão </option>
                                                <option value="73"> 73: Operação de Aquisição a Alíquota Zero </option>
                                                <option value="74"> 74: Operação de Aquisição sem Incidência da Contribuição </option>
                                                <option value="75"> 75: Operação de Aquisição por Substituição Tributária </option>
                                                <option value="98"> 98: Outras Operações de Entrada </option>
                                                <option value="99"> 99: Outras operações </option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="pCOFINS" class="control-label">(%) COFINS</label>
                                            <input id="pCOFINS" type="text" name="pCOFINS" class="form-control money" value="">
                                        </div>
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vBCCOFINS" class="control-label">Base</label>
                                            <input id="vBCCOFINS" type="text" readonly name="vBCCOFINS" class="form-control money" value="">
                                        </div>
                                        <div class="col-md-6 col-sm-12  form-group">
                                            <label for="vCOFINS" class="control-label">Valor COFINS</label>
                                            <input id="vCOFINS" type="text" readonly name="vCOFINS" class="form-control money" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="spa12">
                            <div class="formBuscaGSA">
                                <div class="col-md-12 col-sm-12  form-group">
                                    <table width="100%">
                                        <tr>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                            <td width="12.5%"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <label for="infAdProd" >Info Adicionais do Produto</label>
                                                <textarea id="infAdProd"  class="form-control" name="infAdProd"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="span6 offset3" style="text-align: right;">
                            <button class="btn btn-success" id="btnContinuar">
                                <i class="fa fa-save"></i> Salvar
                            </button>
                        </div>
                        <br/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Produtos -->
<div class="modal fade searchProduto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-users"></i> Adicionar / Editar Itens da NFC-e</h4>
                <button type="button" class="close" data-dismiss="modal" id="bt-close-search-produtos"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div id="div_searchProduto"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/searchPesquisaProdutos.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/formatar.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $("#formaAdicionarProdutos" ).submit(function() {
            event.preventDefault();
            adicionarProdutoNotaFiscal($("#formaAdicionarProdutos"), '<?php echo $nNF;?>');
        });

        $('#aSearchProduto').click(function (event) {
            $('#div_searchProduto').load("<?php echo base_url();?>index.php/produtos/search");
        });
    });

    function fecharModelSerchProduto(produto) {

        $('#cProd').val(produto.cProd);
        $('#xProd').val(produto.descricao);
        $('#uCom').val(produto.unidade);
        $('#vProd').val(produto.precoVenda);
        $('#NCM').val(produto.ncm);
        $('#CEST').val(produto.CEST);
        $('#orig').val(produto.origem);

        $('#cProdANP').val(produto.cProdANP);
        $('#descANP').val(produto.descANP);
        $('#UFCons').val(produto.UFCons);

        setTimeout(function (){$('body').addClass('modal-open');}, 1000);

        popularDadosFiscais('nfe', produto);
        calcularTributos();
        $('.searchProduto').modal('hide');

        $('#qtd').focus();
    }

    function popularDadosFiscais(tipo, produto) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/"+tipo+"/buscarNaturezaOperacao",
            data:  {
                nNF: <?php echo $nNF;?>
            },
            success: function(json) {

                json = JSON.parse(json);

                var naturezaOperacao = json.naturezaOperacao;
                var configuracao = json.configuracao;
                var nf = json.nf;

                $('#produtoid').val(produto.idProdutos);

                if (produto.CFOP !== '') $('#CFOP').val(produto.CFOP);
                else $('#CFOP').val(naturezaOperacao.CFOP);

                if (produto.CST !== '') $('#CST').val(produto.CST);
                else $('#CST').val(naturezaOperacao.ICMS);

                if (produto.CSTIPI !== '') $('#CSTIPI').val(produto.CSTIPI);
                else $('#CSTIPI').val(naturezaOperacao.IPI);

                if (produto.CSTPIS !== '0') $('#CSTPIS').val(produto.CSTPIS);
                else $('#CSTPIS').val(naturezaOperacao.PIS);

                if (produto.CSTCOFINS !== '0') $('#CSTCOFINS').val(produto.CSTCOFINS);
                else $('#CSTCOFINS').val(naturezaOperacao.COFINS);

                if (produto.pICMS > 0) $('#pICMS').val(produto.pICMS);
                else $('#pICMS').val(nf.percentualICMS);

                if (produto.pICMSST > 0) $('#pICMSST').val(produto.pICMSST);
                else $('#pICMSST').val(configuracao.pICMSST);

                if (produto.pIPI > 0) $('#pIPI').val(produto.pIPI);
                else $('#pIPI').val(configuracao.pIPI);

                if (produto.pPIS > 0) $('#pPIS').val(produto.pPIS);
                else $('#pPIS').val(configuracao.pPIS);

                if (produto.pCOFINS > 0) $('#pCOFINS').val(produto.pCOFINS);
                else $('#pCOFINS').val(configuracao.pCOFINS);

                $('#modBCST').val(produto.modBCST);
            }
        });
    }
</script>