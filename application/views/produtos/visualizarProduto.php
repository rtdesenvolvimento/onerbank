<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-shopping-cart"></i> Dados do produto</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>CNPJ</strong></td>
                                        <td><?php echo $emitente->cnpj ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Código de Barras</strong></td>
                                        <td><?php echo $result->cEAN ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; width: 30%"><strong>Descrição</strong></td>
                                        <td><?php echo $result->descricao ?></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right"><strong>Unidade</strong></td>
                                        <td><?php echo $result->unidade ?></td>
                                    </tr>
                                    <?php if ($result->setor ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Setor</strong></td>
                                            <td><?php echo $result->setor ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($result->grupoproduto ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Grupo de Produto</strong></td>
                                            <td><?php echo $result->grupoproduto ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($estoque->estoque) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Estoque</strong></td>
                                            <td><?php echo $estoque->estoque ?><?php echo ' '.$result->unidade ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($estoque->precoCompra) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Preço de Compra</strong></td>
                                            <td><?php echo 'R$ '.$this->site_model->formatarValorMonetario($estoque->precoCompra) ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($estoque->precoVenda) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Preço de Venda</strong></td>
                                            <td><?php echo  'R$ '.$this->site_model->formatarValorMonetario($estoque->precoVenda) ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($estoque->precoCompra) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Valor do estoque pela compra</strong></td>
                                            <td><?php echo  'R$ '.$this->site_model->formatarValorMonetario($estoque->precoCompra*$estoque->estoque) ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($estoque->precoVenda) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Valor do estoque pela venda</strong></td>
                                            <td><?php echo  'R$ '.$this->site_model->formatarValorMonetario($estoque->precoVenda*$estoque->estoque) ?></td>
                                        </tr>
                                    <?php } ?>

                                    <?php if ($result->comissao > 0 ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Comissão</strong></td>
                                            <td> <?php echo $result->comissao; ?>%</td>
                                        </tr>
                                    <?php } ?>

                                    <?php if ($result->CFOP > 0 ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>CFOP</strong></td>
                                            <td> <?php echo $result->CFOP; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($result->CEST > 0 ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>CEST</strong></td>
                                            <td> <?php echo $result->CEST; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($result->CST > 0 ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>CST</strong></td>
                                            <td> <?php echo $result->CST; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($result->ncm > 0 ) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>NCM</strong></td>
                                            <td> <?php echo $result->ncm; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php
                                    $contador = 1;
                                    foreach ($fornecedoresproduto as $fornecedores) { ?>
                                        <tr>
                                            <td style="text-align: right"><strong>Fornecedor <?php echo $contador; ?></strong></td>
                                            <td><?php echo $fornecedores->nomeFornecedor. '<br/>código '.$fornecedores->codigo.'<br/>'.$fornecedores->observacao; ?></td>
                                        </tr>
                                    <?php $contador = $contador + 1; }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="item form-group no-print">
                            <div class="col-md-12" style="text-align: right;">
                                <a href="<?php echo base_url() ?>produtos/editar/<?php echo $result->idProdutos;?>" class="btn btn-success"><i class="fa fa-save"></i> Editar</a>
                                <a href="<?php echo base_url() ?>index.php/produtos" id="" class="btn btn-primary"><i class="fa fa-backward"></i>Voltar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

