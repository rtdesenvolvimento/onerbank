<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Editar Manutenção preventiva</h5>
            </div>
            <div class="widget-content nopadding">


                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da Manutenção Preventiva</a></li>
                        <!--<li class="active" id="tabDetalhesDaNota"><a href="#tab1" data-toggle="tab">Detalhes da Nota</a></li>!-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divEditarVenda">

                                <form action="<?php echo current_url(); ?>" method="post" id="formVendas">
                                    <?php echo form_hidden('idProgramacaomanutencao', $result->idProgramacaomanutencao) ?>

                                    <div class="span12" style="padding: 1%; margin-left: 0">

                                        <div class="span2">
                                            <label for="numeroProgramacao">Nº</label>
                                            <input id="numeroProgramacao" class="span12" readonly type="text" name="numeroProgramacao" value="<?php echo $result->numeroProgramacao;?>"/>
                                        </div>

                                        <div class="span2">
                                            <label for="situacao">Situação</label>
                                            <select class="span12" disabled name="situacao" id="situacao">
                                                <option <?php if ($result->situacao == 1) echo 'selected="selected"';?> value="1">PENDENTE</option>
                                                <option <?php if ($result->situacao == 2) echo 'selected="selected"';?> value="2">CONFIRMADA</option>
                                            </select>
                                        </div>

                                        <div class="span4">
                                            <label for="produto_id">Patrimônio<span class="required">*</span></label>
                                            <select name="produto_id" disabled class="span12">
                                              <option value="<?php $result->idProdutos;?>"><?php echo $result->descricao;?></option>
                                            </select>
                                        </div>

                                        <div class="span4">
                                            <label for="fornecedor_id">Fornecedor<span class="required">*</span></label>
                                            <select name="fornecedor_id" class="span12">
                                                <option value="">Selecione um fornecedor</option>
                                                <?php
                                                foreach($fornecedores as $fornecedor){?>
                                                    <option <?php if ($result->fornecedor_id == $fornecedor->idFornecedor) echo 'selected="selected"';?> value="<?php echo $fornecedor->idFornecedor;?>"><?php echo $fornecedor->nomeFornecedor;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span2">
                                            <label for="dataAgendamento">Agendamento</label>
                                            <input id="dataAgendamento" required="required" class="span12" type="date" name="dataAgendamento" value="<?php echo $result->dataAgendamento;?>"/>
                                        </div>

                                        <div class="span4">
                                            <label for="prazoEntrega">Prazo entrega (dias)</label>
                                            <input id="prazoEntrega" class="span12" type="text" name="prazoEntrega" value="<?php echo $result->prazoEntrega;?>"/>
                                        </div>

                                        <div class="span4">
                                            <label for="numeroNotaFiscal">Nº NF</label>
                                            <input id="numeroNotaFiscal" class="span12" type="text" name="numeroNotaFiscal" value="<?php echo $result->numeroNotaFiscal;?>"/>
                                        </div>

                                        <div class="span2">
                                            <label for="valorManutencao">Valor Manutenação (R$)</label>
                                            <input id="valorManutencao" class="span12" type="text" name="valorManutencao" value="<?php echo $result->valorManutencao;?>"/>
                                        </div>

                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span12">
                                            <label for="observacao">Observação</label>
                                            <textarea class="span12"  id="observacao" name="observacao" cols="30" rows="5"><?php echo $result->observacao;?></textarea>
                                        </div>
                                    </div>


                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span8 offset2" style="text-align: center">

                                            <?php if ($result->situacao == 1) {?>
                                                <button class="btn btn-primary" id="btnContinuar"><i class="icon-white icon-ok"></i> Alterar</button>

                                                <a href="<?php echo base_url() ?>index.php/programacaomanutencaopreventiva/confirmarManutencao/<?php echo $result->idProgramacaomanutencao; ?>"
                                                   class="btn btn-success"><i class="icon-refresh"></i> Confirmar Manutenção</a>
                                            <?php } ?>

                                            <a href="<?php echo base_url() ?>index.php/programacaomanutencaopreventiva" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                        </div>
                                    </div>

                                </form>

                                <div class="span12" style="padding: 1%; margin-left: 0">

                                    <form id="formProdutos"
                                          action="<?php echo base_url(); ?>index.php/vendas/adicionarProduto"
                                          method="post" style="display: none;">
                                        <div class="span8">
                                            <input type="hidden" name="idProduto" id="idProduto"/>
                                            <input type="hidden" name="estoque" id="estoque" value=""/>
                                            <input type="hidden" name="preco" id="preco" value=""/>

                                            <div class="formBuscaGSA">
                                                <label for="">
                                                    Serviço
                                                    <a href="#modal-consulta-estoque" id="addConsultaProduto" data-toggle="modal">
                                                        <i id="pincliente" style="float: right;margin-right: 10px;" class="icon-bar-chart icon-white"></i></a>
                                                </label>

                                                <div class="controls">
                                                    <select name="produto" class="span12" id="produto" required="required">
                                                        <option value="">--Selecione um cliente--</option>
                                                        <?php foreach ($produtosList as $produto) {?>
                                                            <option value="<?php echo $produto->idProdutos; ?>"><?php echo $produto->descricao; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="span2">
                                            <label for="">Preço da manutenação</label>
                                            <input type="text" placeholder="valor" name="valor"
                                                   class="span12"/>
                                        </div>

                                        <div class="span2">
                                            <label for="">&nbsp</label>
                                            <button class="btn btn-success span12" id="btnAdicionarProduto"><i
                                                    class="icon-white icon-plus"></i> Adicionar
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="span12" id="divProdutos" style="margin-left: 0">
                                    <table class="table table-bordered" id="tblProdutos">
                                        <thead>
                                        <tr>
                                            <th>Serviço</th>
                                            <th>Peça</th>
                                            <th>Valor Manutençao Sugerido</th>
                                            <!--<th>Ações</th>!-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $total = 0;
                                        foreach ($servicosManutencao as $s) {
                                            $total = $total + $s->valor;
                                            echo '<tr>';
                                            echo '<td>' . $s->nome . '</td>';
                                            echo '<td>' . $s->descricao . '</td>';
                                            echo '<td>' . $s->valor . '</td>';
                                            //echo '<td><a href="" idProgramacaomanutencaoservico="' . $s->idProgramacaomanutencaoservico . '" idServicos="' . $s->idServicos . '"  class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                            echo '</tr>';
                                        } ?>

                                        <tr>
                                            <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                            <td>
                                                <strong>
                                                    R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

    });
</script>

