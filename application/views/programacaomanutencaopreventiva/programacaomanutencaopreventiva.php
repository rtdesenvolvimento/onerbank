<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Programação de manutenção agendada</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Cod.Manutenção</th>
                    <th>Peça</th>
                    <th>Empresa</th>
                    <th>Data</th>
                    <th>Prazo entrega</th>
                    <th>Valor</th>
                    <th>Situação</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="8">Nenhum Programação</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Programação de manutenção</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Cod</th>
                    <th>Patrimônio</th>
                    <th>Serviço</th>
                    <th>Peça</th>
                    <th>Empresa</th>
                    <th>Data</th>
                    <th>Prazo entrega</th>
                    <th>Valor Manutenção</th>
                    <th>Situação</th>
                    <th>Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {

                    $situacao = $r->situacao;

                    if ($situacao == 1) {
                        $situacao = 'PENDENTE';
                    }

                    if($situacao == 2) {
                        $situacao = 'CONFIRMADA';
                    }

                    echo '<tr>';
                    echo '    <td>' . $r->numeroProgramacao . '</td>';
                    echo '    <td>' . $r->produto . '</td>';
                    echo '    <td>' . $r->servico . '</td>';
                    echo '    <td>' . $r->peca. '</td>';
                    echo '    <td>' . $r->nomeFornecedor . '</td>';
                    echo '    <td>' . date(('d/m/Y'), strtotime($r->dataAgendamento)) . '</td>';
                    echo '    <td>' . $r->prazoEntrega . '</td>';
                    echo '    <td>' . $r->valorManutencao . '</td>';
                    echo '    <td>' . $situacao . '</td>';
                    echo '    <td>';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/programacaomanutencaopreventiva/editar/' . $r->idProgramacaomanutencao . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    //echo '      <a href="#modal-excluir" role="button" data-toggle="modal" programacaomanutencaopreventiva="' . $r->idProgramacaomanutencao . '" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>  ';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>



<script type="text/javascript">

</script>