<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de receita <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formReceita" method="post" class="form-horizontal">
                    <?php echo form_hidden('idReceita', $result->idReceita) ?>
                    <div class="control-group">
                        <label for="grupoProduto_id" class="control-label">Receita superior</label>
                        <div class="controls">
                            <select name="receitaSuperior_id" class="form-control">
                                <option value="">Selecione uma receita superior</option>
                                <?php
                                foreach($receitas as $receita){
                                    $sel = '';
                                    if ($receita->idReceita == $result->receitaSuperior_id) $sel = 'selected="selected"';
                                    echo '<option value="'.$receita->idReceita.'" '.$sel.'>'.$receita->nome.'</option>';
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="codigo" class="control-label">Código<span class="required">*</span></label>
                        <div class="controls">
                            <input id="codigo" type="text" class="form-control" name="codigo" value="<?php echo $result->codigo ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" class="form-control" name="nome" value="<?php echo $result->nome ?>"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Alterar</button>
                            <a href="<?php echo base_url() ?>index.php/receita" id="btnAdicionar" class="btn btn-primary"><i
                                    class="icon-arrow-left"></i> Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formReceita').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>