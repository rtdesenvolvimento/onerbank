<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="urn:layr:template">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Recibo</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/recibo/recibo.css"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
</head>
<body style="background-color: white;">
    <div id="non-printable" class="actions-div">
        <button class="btn" style="cursor: pointer;" onclick="window.close();">Cancelar</button>
        <button class="btn btn-primary print-btn" style="cursor: pointer;" onclick="window.print();">Imprimir Recibo</button>
    </div>
    <div id="printable" class="receipt-container">
        <hr class="solid-line">
        <div class="receipt-info-container">
            <div class="receipt-info-column-1">
                <div><span style="font-size: 15px;"><strong><?php echo $emitente->nome; ?></strong></span></div>
                <div class="company-info-container">
                    <strong>CNPJ: </strong> <?php echo $emitente->cnpj; ?> <strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
                </div>
                <div class="company-address-container">
                    <span> <?php echo $emitente->rua.', nº:'.$emitente->numero.', '.$emitente->bairro.' - '.$emitente->cidade.' - '.$emitente->uf; ?> </span> </br>
                    <span> E-mail: <?php echo $emitente->email.' - Fone: '.$emitente->telefone; ?></span>
                </div>
            </div>
        </div>
        <hr class="solid-line">
        <table style="width: 100%; padding: 30px 0px 5px;">
            <tbody>
            <tr>
                <td style="flex: 1; font-size: 30px;"><strong><span class="condensed">RECIBO</span></strong></td>
                <td style="flex: 1; text-align: right;"><span style="font-size: 38px;" class="condensed semi-bold"><span style="font-size: 28px;" class="gray condensed semi-bold">R$</span> <?php echo $this->site_model->formatarValorMonetario($recibo->getValor());?></span></td>
            </tr>
            </tbody>
        </table>
        <hr class="dotted-line">
        <div class="receipt-detail-container">
            <?php
            if ($recibo->getCredito()) $tipoOperacao = 'Recebemos de ';
            else $tipoOperacao = 'Pagamos para ';
            ?>
            <div class="receipt-details"><?php echo $tipoOperacao?>
                <strong><?php echo $recibo->getNomePessoa();?></strong>,<strong> <?php echo $recibo->getCpfCnpjPessoa();?></strong>, a
                importância de <strong><?php echo 'R$'.$this->site_model->formatarValorMonetario($recibo->getValor());?><?php echo $recibo->getFormaPagamento();?></strong>, <?php echo $recibo->getReferencia();?><br>Para maior clareza
                firmo(amos) o presente.
            </div>
            <div class="receipt-date"><?php echo $this->site_model->dataDeHojePorExtenso();?>.</div>
            __________________________________________________
            <div class="receipt-company">
                <?php echo $this->session->userdata('nome')?><br>
            </div>
        </div>
    </div>
</body>
</html>