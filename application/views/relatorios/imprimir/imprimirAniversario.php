<?php


if ($mes == 1) {
    $mes = 'Janeiro';
} else if ($mes == 2) {
    $mes = 'Fevereiro';
} else if ($mes == 3) {
    $mes = 'Março';
} else if ($mes == 4) {
    $mes = 'Abril';
} else if ($mes == 5) {
    $mes = 'Maio';
} else if ($mes == 6) {
    $mes = 'Junho';
} else if ($mes == 7) {
    $mes = 'Julho';
} else if ($mes == 8) {
    $mes = 'Agosto';
} else if ($mes == 9) {
    $mes = 'Setembro';
} else if ($mes == 10) {
    $mes = 'Outubro';
} else if ($mes == 11) {
    $mes = 'Novembro';
} else if ($mes == 12) {
    $mes = 'Dezembro';
}
?>

<head>
    <title>Relatório de Anivesariantes do Mês de <?php echo $mes;?></title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/fullcalendar.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/blue.css" class="skin-color"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body style="background-color: transparent">

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Aniversariantes</h4>
                </div>
                <div class="widget-content nopadding">

                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #eee;">
                            <th style="1%">Nome</th>
                            <th style="font-size: 1.2em; padding: 5px;">Telefones</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Aniversário</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">Idade</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($clientes as $c) {

                            $dataNascimento         = $c->data_nascimento;
                            $nascimento_cliente     = date(('d'),strtotime($dataNascimento));
                            $nascimento_completo    = date(('d/m/Y'),strtotime($dataNascimento));

                            // Declara a data! :P
                            $data = $nascimento_completo;

                            // Separa em dia, mês e ano
                            list($dia, $mes, $ano) = explode('/', $data);

                            // Descobre que dia é hoje e retorna a unix timestamp
                            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                            // Descobre a unix timestamp da data de nascimento do fulano
                            $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

                            // Depois apenas fazemos o cálculo já citado :)
                            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                            echo '<tr>';
                            echo '<td>' . $c->nomeCliente.'<br/>'.$c->email . '</td>';
                            echo '<td>' . $c->telefone.'<br/>'. $c->celular . '</td>';
                            echo '<td style="text-align: center;">' . $nascimento_completo. '</td>';
                            echo '<td style="text-align: center;">' . $idade. '</td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
            <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y'); ?></h5>
        </div>
    </div>
</div>
<!-- Arquivos js-->
<script src="<?php echo base_url(); ?>js/excanvas.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.peity.min.js"></script>
<script src="<?php echo base_url(); ?>js/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>js/sosmc.js"></script>
<script src="<?php echo base_url(); ?>js/dashboard.js"></script>
</body>
</html>







