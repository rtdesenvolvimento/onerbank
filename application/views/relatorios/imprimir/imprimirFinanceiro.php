<head>
    <title>Relatório Financeiro</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/fullcalendar.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/blue.css" class="skin-color"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body style="background-color: transparent">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Relatório Financeiro</h4>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered" style="font-size: 11px;">
                        <thead>
                        <tr>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Tipo</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: left;">Pessoa</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Venc.</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Valor.</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Parc.</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: right;">Acrec.</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: right;">Desc.</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Pagar</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Pago</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">Dat.Pag</th>
                            <th style="font-size: 1.2em; padding: 3px;text-align: center;">St.</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalReceita = 0;
                        $totalDespesa = 0;

                        foreach ($lancamentos as $l) {

                            $nomeCliente = $l->nomeCliente;
                            $nomFornecedor = $l->nomeFornecedor;
                            $dtUltimoPagamento = '-';
                            $cliente_fornecedor = '';

                            if ($nomeCliente) $cliente_fornecedor = $nomeCliente;
                            else  $cliente_fornecedor = $nomFornecedor;

                            if ($l->baixado == 1) {
                                $situacao = 'Pago';
                            } else {
                                $situacao = 'Pendente';
                            }
                            if ($l->dtUltimoPagamento) $dtUltimoPagamento = date('d/m/Y', strtotime($l->dtUltimoPagamento));

                            if ($l->tipo == 'receita') $totalReceita += $l->valorPago;
                            else $totalDespesa += $l->valorPago;

                            echo '<tr>';
                            echo '<td  style="text-align: center;">' . $l->tipo . '</td>';
                            echo '<td  style="text-align: left;">' . $cliente_fornecedor . '</td>';
                            echo '<td  style="text-align: center;">' . date('d/m/Y', strtotime($l->dtVencimento)) . '</td>';
                            echo '<td  style="text-align: right;">R$' . number_format($l->valorVencimento, 2, ',', '.') . '</td>';
                            echo '<td  style="text-align: center;">' . $l->numeroParcela . '/' . $l->totalParcelas . '</td>';
                            echo '<td  style="text-align: right;">R$' . number_format($l->acrescimo, 2, ',', '.') . '</td>';
                            echo '<td  style="text-align: right;">R$' . number_format($l->desconto, 2, ',', '.') . '</td>';
                            echo '<td  style="text-align: right;">R$' . number_format($l->valorPagar, 2, ',', '.') . '</td>';
                            echo '<td  style="text-align: right;">R$' . number_format($l->valorPago, 2, ',', '.') . '</td>';
                            echo '<td  style="text-align: center;">' . $dtUltimoPagamento . '</td>';
                            echo '<td  style="text-align: center;">' . $situacao . '</td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="8" style="text-align: right; color: blue"><strong>Total Receitas
                                    Recebidas:</strong></td>
                            <td colspan="4" style="text-align: right; color: blue">
                                <strong>R$ <?php echo number_format($totalReceita, 2, ',', '.') ?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="8" style="text-align: right; color: red"><strong>Total Despesas
                                    Pagas:</strong></td>
                            <td colspan="4" style="text-align: right; color: red">
                                <strong>R$ <?php echo number_format($totalDespesa, 2, ',', '.') ?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="8" style="text-align: right"><strong>Lucro Líquido = (Recebido -
                                    Pago):</strong></td>
                            <td colspan="4" style="text-align: right;">
                                <strong>R$ <?php echo number_format($totalReceita - $totalDespesa, 2, ',', '.') ?></strong>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <h5 style="text-align: right">Data de Emissão do Relatório: <?php echo date('d/m/Y'); ?></h5>
        </div>
    </div>
</div>
<!-- Arquivos js-->
<script src="<?php echo base_url(); ?>js/excanvas.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/sosmc.js"></script>
<script src="<?php echo base_url(); ?>js/dashboard.js"></script>
</body>
</html>








