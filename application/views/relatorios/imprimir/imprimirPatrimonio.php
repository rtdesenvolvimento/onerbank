<head>
    <title>Relatório de Patrimônio</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body style="background-color: transparent">
<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Patrimônio</h4>
                </div>
                <div class="widget-content nopadding">

                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #eee;">
                            <th style="font-size: 1.2em; padding: 5px;">Pat.</th>
                            <th style="font-size: 1.2em; padding: 5px;">Descrição</th>
                            <th style="font-size: 1.2em; padding: 5px;">Grupo</th>
                            <th style="font-size: 1.2em; padding: 5px;">Unidade</th>
                            <th style="font-size: 1.2em; padding: 5px;">Origem</th>
                            <th style="font-size: 1.2em; padding: 5px;">Aquisição</th>
                            <th style="font-size: 1.2em; padding: 5px;">Compra</th>
                            <th style="font-size: 1.2em; padding: 5px;">Depreciado</th>
                            <th style="font-size: 1.2em; padding: 5px;">Atual</th>
                            <th style="font-size: 1.2em; padding: 5px;">Dados</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($produtos as $p) {

                            $origem = '';
                            $dataAquisicao = $p->dataAquisicao;

                            if ($dataAquisicao) {
                                $dataAquisicao = date(('d/m/Y'), strtotime($dataAquisicao));
                            }

                            if ($p->origemAquisicao == 1) {
                                $origem = 'Compra';
                            } else if ($p->origemAquisicao == 2){
                                $origem = 'Doação';
                            } else if ($p->origemAquisicao == 3) {
                                $origem = 'Patrimonio Antigo';
                            }

                            echo '<tr>';
                            echo '<td>' . $p->cProd. '</td>';
                            echo '<td>' . $p->descricao . '</td>';
                            echo '<td>' . $p->grupoproduto . '</td>';
                            echo '<td>' . $p->centrocusto . '</td>';
                            echo '<td>' . $origem. '</td>';
                            echo '<td>' . $dataAquisicao . '</td>';
                            echo '<td>R$' . number_format( $p->valorAquisicao, 2, ',', '.') . '</td>';
                            echo '<td>R$' . number_format( $p->valorDepreciado, 2, ',', '.') . '</td>';
                            echo '<td>R$' . number_format( $p->valorAtualPosDepreciacao, 2, ',', '.') . '</td>';
                            echo '<td>' . $p->marca.'<br/>'.$p->modelo.'<br/>'.$p->numeroSerie.'<br/>'.$p->localizacao.'</td>';
                            echo '</tr>';
                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
            <h5 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h5>

        </div>
    </div>
</div>
<!-- Arquivos js-->

<script src="<?php echo base_url();?>js/excanvas.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
<script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>js/sosmc.js"></script>
<script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>

