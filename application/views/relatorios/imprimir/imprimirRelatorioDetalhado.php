<head>
    <title>Relatório Financeiro</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<style>
    body{
        font-size: 9px;
    }
    .table-bordered {
        border: 1px solid #000000;
        border-collapse: separate;
        *border-collapse: collapse;
        border-left: 0;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
    }
    .table th, .table td {
        padding: 2px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid
    }
    .table-bordered th, .table-bordered td {
        border-left: 1px solid #4F5155;
    }
</style>
<body style="background-color: transparent">

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <div class="widget-box">
                <div class="widget-title">
                    <h4 style="text-align: center">Relatório Financeiro</h4>
                    <h5 style="border-bottom: 1px solid;color: #000000;">Filtros da consulta</h5>
                    <table class="table table-bordered">
                        <tr>
                            <td width="20%"></td>
                            <td width="80%"></td>
                        </tr>
                        <?php if ($periodo){?>
                            <tr>
                                <td>Período: </td>
                                <?php if ($periodo == 'filtros_por_data'){?>
                                    <td><?php echo 'Filtros por vencimento'; ?></td>
                                <?php } else {?>
                                    <td><?php echo $periodo; ?></td>
                                <?php }?>
                            </tr>
                        <?php }?>
                        <?php if ($situacao){
                            if ($situacao == 'FATURADA' || $situacao == 'FATURADA_BOLETO') {
                                $situacaoExibe = 'Abertas';
                            } else if ($situacao == 'PAGO') {
                                $situacaoExibe = 'Pagas';
                            } else if ($situacao == 'ATRASADA') {
                                $situacaoExibe = 'Vencidas';
                            } else if ($situacao == 'cancelada'){
                                $situacaoExibe = 'Canceladas';
                            }
                            ?>
                            <tr>
                                <td>Status: </td>
                                <td><?php echo $situacaoExibe; ?></td>
                            </tr>
                        <?php }?>
                        <?php if ($termo){?>
                            <tr>
                                <td>Nome do cliente: </td>
                                <td><?php echo $termo; ?></td>
                            </tr>
                        <?php }?>
                        <?php if ($dataInicial){?>
                            <tr>
                                <td>Vencimento de: </td>
                                <td><?php echo  date("d/m/Y", strtotime($dataInicial)); ?></td>
                            </tr>
                        <?php }?>
                        <?php if ($dataFinal){?>
                            <tr>
                                <td>Vencimento até: </td>
                                <td><?php echo  date("d/m/Y", strtotime($dataFinal)); ?></td>
                            </tr>
                        <?php }?>
                    </table>
                </div>
                <div class="widget-content nopadding">

                    <table class="table table-bordered">
                        <thead>
                            <tr style="background: #4F5155;">
                                <th style="text-align: center;color: #ffffff;">Tipo</th>
                                <th style="text-align: center;color: #ffffff;">Pag.</th>
                                <th style="text-align: right;color: #ffffff;">Cat.</th>
                                <th style="text-align: left;color: #ffffff;">Pessoa</th>
                                <th style="text-align: center;color: #ffffff;">Status</th>
                                <th style="text-align: center;color: #ffffff;">Nº</th>
                                <th style="text-align: center;color: #ffffff;">Vencimento</th>
                                <th style="text-align: center;color: #ffffff;">Pagamento</th>
                                <th style="text-align: right;color: #ffffff;">Pago</th>
                                <th style="text-align: right;color: #ffffff;">Pagar</th>
                                <th style="text-align: right;color: #ffffff;">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalReceita = 0;
                        $totalDespesa = 0;
                        $totalCusto   = 0;
                        $totalPago = 0;

                        $saldo  = 0;
                        $os     = null;

                        foreach ($results as $r) {

                            if ($r->taxa == 1) continue;//TODO NAO EXIBE TAXAS DE CARTAO OU BOLETO.

                            $boleto = $this->financeiro_model->getBoletoByParcela($r->idParcela);

                            $vencimento = date(('d/m/Y'), strtotime($r->dtVencimento));
                            $data_pagamento = '-';
                            $previsaoPagamentoEditar = null;
                            $os = $r->os;
                            $totalCusto += $r->custo;
                            $plano = '';
                            $isBoleto = TRUE;
                            $isBoletoDisabled = '';
                            $labelPagamento = 'info';

                            if ($r->tipo == 'receita') $receita = $r->receita;
                            else  $receita = $r->despesa;

                            if ($r->linkpagamento_id != null) $receita = $r->linkpagamento;

                            $status = '';

                            if ($r->status == 'FATURADA') {
                                if (strtotime($r->dtVencimento) < strtotime(date('Y-m-d'))) {
                                    if ($situacao == 'FATURADA_BOLETO') continue;
                                    else  $status = 'Vencido';
                                } else {
                                    if (count($boleto) > 0) $status = 'Aberta';
                                    else $status = 'Aberta';
                                }
                            } else if ($r->status == 'PAGO') {
                                $status = 'Pago';
                            } else if ($r->status == 'ABERTO') {
                                $status = 'Aberta';
                            } else if ($r-> status == 'cancelada'){
                                $status = 'Cancelada';
                                $isBoletoDisabled = 'disabled="disabled"';
                            }

                            if ($r->status == 'PAGO') {
                                $data_pagamento = date(('d/m/Y'), strtotime($r->dtUltimoPagamento)) . '</small>';
                                $status = 'Pago';
                                $labelPagamento = 'success';
                            } else if ($r->dtUltimoPagamento) $data_pagamento = date(('d/m/Y'), strtotime($r->dtUltimoPagamento)) . '</small>';

                            if ($r->tipo == 'receita') {
                                $label = 'success';

                                if ($r->status != 'cancelada') {
                                    $totalReceita += $r->valorVencimento;
                                    $totalPago += $r->valorPago - $r->taxas;
                                }

                                $plano = $r->receita;
                                $isBoleto = TRUE;
                            } else {
                                $label = 'important';

                                if ($r->status != 'cancelada') {
                                    $totalDespesa += $r->valorVencimento;
                                    $totalPago += $r->valorPago - $r->taxas;
                                }

                                $plano = $r->despesa;
                                $isBoleto = FALSE;
                            }

                            $nomeClienteFornecedor = '';

                            if ($r->fornecedor_id) $nomeClienteFornecedor = $r->nomeFornecedor;
                            if ($r->clientes_id) $nomeClienteFornecedor = $r->nomeCliente;

                            echo '<tr>';
                            echo '<td style="text-align: center;">'.ucfirst($r->tipo).'</td>';
                            echo '<td style="text-align: center;">'.ucfirst($r->tipocobranca).'</td>';

                            if ($r->taxa == 1) echo '<td style="text-align: left;">Taxa da cobrança</td>';
                            else echo '<td style="text-align: left;">' . ucfirst($receita) . '</td>';

                            echo '<td>'.$nomeClienteFornecedor.' - <small>'. $r->cnpjCliente.'</small></td>';
                            echo '<td style="text-align: center;">'.ucfirst($status).'</td>';
                            echo '<td style="text-align: center;">'.$r->numeroParcela.'/'.$r->totalParcelas.'</td>';
                            echo '<td style="text-align: center;">'.$vencimento.'</td>';
                            echo '<td style="text-align: center;">'.$data_pagamento.'</td>';
                            echo '<td style="text-align: right;"> R$'.number_format($r->valorPago,2,',','.').'</td>';
                            echo '<td style="text-align: right;"> R$'.number_format($r->valorPagar,2,',','.').'</td>';
                            echo '<td style="text-align: right;"> R$'.number_format($r->valorVencimento,2,',','.').'</td>';
                            echo '</tr>';
                        }?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="9" style="text-align: left;"><strong>Total Pago:</strong></td>
                            <td colspan="2" style="text-align: right;"><strong>R$ <?php echo number_format($totalPago, 2, ',', '.') ?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: left; color: #00008B"><strong>Total Receitas:</strong></td>
                            <td colspan="2" style="text-align: right; color: #00008B"><strong>R$ <?php echo number_format($totalReceita, 2, ',', '.') ?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: left; color: #A52A2A;"><strong>Total Despesas:</strong></td>
                            <td colspan="2" style="text-align: right; color: #A52A2A;"><strong>R$ <?php echo number_format($totalDespesa, 2, ',', '.') ?></strong></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: left;color: green;"><strong>Lucro Líquido:</strong></td>
                            <td colspan="2" style="text-align: right;color: green;"><strong>R$ <?php echo number_format($totalReceita - $totalDespesa - $totalCusto, 2, ',', '.') ?></strong></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <h5 style="text-align: right">Data de Emissão do Relatório: <?php echo date('d/m/Y');?></h5>
        </div>
    </div>
</div>
<!-- Arquivos js-->
<script src="<?php echo base_url();?>js/excanvas.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/sosmc.js"></script>
<script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>