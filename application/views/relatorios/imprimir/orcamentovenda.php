<head>
    <title>Relatório de Vendas</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    <style>
        td {
            padding: 2px;
            line-height: 15px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #ddd;
            font-size: 10px;
            border-left: 1px solid #ddd;
        }
    </style>
</head>

<body style="background-color: transparent">

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <div class="widget-box">
                <div class="widget-content nopadding">


                    <table class="table table-bordered">
                        <tr>
                            <td style="text-align: center">
                                <img src="http://resultaonline.ddns.net:9090/imagens/logo/10770898000130/brasilcarbomnovo.jpeg">
                            </td>
                            <td>
                                <b>BrasilCarbon Importação e Comércio Produtos para Indústria</b><br/><br/>
                                Rua Ibirama, Saguacú, Joinville, Santa Catarina, Brasil<br/>
                                CNPJ: 10.770.898/0001-30   IE: 25.584.804-8<br/>
                                FONE: + 55 47 3433-1856   email: contato@brasilcarbon.com.br
                            </td>
                        </tr>

                        <tr>
                            <?php
                            $numeroOrcamento = '';
                            $observacao = '';
                            $percenticms = 0;
                            $prazoentrega = 0;
                            $valordesconto = 0;
                            $percentipi = 0;
                            $valortotal = 0;
                            $frete = '';
                            $condicaopagamento = '';
                            $cnpjcpf = '';
                            $inscricaoestadual = '';
                            $dataorcamento = '';

                            foreach ($dados as $orcamento) {

                            $numeroOrcamento = $orcamento->codigoorcameto;
                            $observacao = $orcamento->observacao;
                            $percenticms = $orcamento->percenticms;
                            $prazoentrega = $orcamento->prazoentrega;
                            $valordesconto = $orcamento->valordesconto;
                            $frete = $orcamento->frete;
                            $percentipi = $orcamento->percentipi;
                            $valortotal = $orcamento->valortotal;
                            $condicaopagamento = $orcamento->condicaopagamento;
                            $cnpjcpf = $orcamento->cnpjcpf;
                            $inscricaoestadual = $orcamento->inscricaoestadual;
                            $dataorcamento = $orcamento->dataorcamento;

                            if ($frete == 0) {
                                $frete = 'Proprio';
                            }

                            if ($frete == 1) {
                                $frete = 'Destinatário/Remetente';
                            }

                            if ($frete == 2) {
                                $frete = 'Terceiros';
                            }

                            if ($frete == 9) {
                                $frete = 'Sem Frete';
                            }
                            ?>
                            <td colspan="2">
                                Cliente  : <?php echo $orcamento->nomecliente.' - CNPJ: '.$cnpjcpf.' IE: '.$inscricaoestadual;?> <br/>
                                Contato  : <?php echo $orcamento->contato;?> <br/>
                                Fone     : <?php echo $orcamento->telefone;?> <br/>
                            </td>
                        </tr>
                        <?php break; }?>
                    </table>

                    <div class="widget-title">

                    </div>
                    <table  style="font-size: 10px;width: 100%;">
                        <caption>
                            <h4 style="text-align: center">Orçamento de Venda  número <?php echo $numeroOrcamento;?></h4>
                            <?php echo date(('d/m/Y'), strtotime($dataorcamento)).' '.date('H:i');?>
                        </caption>
                        <thead>
                        <tr style="background: #9e9e9e7a;">
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">ITEM</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">PRODUTO</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;width: 20%">DESCRIÇÃO</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">Xmm</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">Ymm</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">Zmm</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">QTD.</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;width: 10%">O.S</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;width: 10%">POS</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;width: 10%">ELE. Nº</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">PESO Kg</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">UNITÁRIO</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;width: 10%">TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalPesoKg = 0;
                        $imprimir = true;
                        $contador = 1;

                        foreach ($dados as $orcamento) {
                            $totalPesoKg = $totalPesoKg + $orcamento->pesokg;
                            $backgroun = '#eee';

                            if (!$imprimir) {
                                $backgroun = 'white';
                                $imprimir = true;
                            } else {
                                $imprimir = false;
                            }

                            echo '<tr style="background: '.$backgroun.';">';
                            echo '<td style="text-align: left;">' . $contador . '</td>';
                            echo '<td style="text-align: left;">' . $orcamento->cdproduto. '</td>';
                            echo '<td style="text-align: left;">' . $orcamento->descricaoproduto. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->largura. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->altura. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->profundidade. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->quantidade. '</td>';
                            echo '<td style="text-align: center;">' . $orcamento->os. '</td>';
                            echo '<td style="text-align: center;">' . $orcamento->posicao. '</td>';
                            echo '<td style="text-align: center;">' . $orcamento->eletrodo. '</td>';
                            echo '<td style="text-align: right;">' . number_format( $orcamento->pesokg ,2,",",".") . '</td>';
                            echo '<td style="text-align: right;">' .  number_format( $orcamento->precounitario ,2,",",".") . '</td>';
                            echo '<td style="text-align: right;">' .  number_format($orcamento->subtotal,2,",",".") . '</td>';
                            echo '</tr>';

                            $contador++;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>


                    <table  style="font-size: 10px;width: 100%;">
                        <tbody>
                        <?php if ($percenticms > 0) {?>
                            <tr>
                                <td colspan="13" style="text-align: center;background: #ffeb3b85;font-weight: bold;font-size: 17px;">
                                    *** PERMITE O APROVEITAMENTO DE <?php echo $percenticms;?>% DE ICMS ***
                                </td>
                            </tr>
                        <?php } ?>

                        <tr>
                            <td colspan="13">
                                Observações:<br/>
                                <?php echo $observacao;?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="7">
                                Prazo de entrega: <?php echo $prazoentrega;?>
                            </td>
                            <td colspan="6" style="text-align: right;">
                                Desconto: <?php echo number_format( $valordesconto,2,",",".");?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="7">
                                Frete: <?php echo $frete;?>
                            </td>

                            <td colspan="6" style="text-align: right;">
                                IPI: <?php echo number_format( $percentipi,2,",",".");?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="7">
                                Condição de pagamento: <?php echo $condicaopagamento;?>
                            </td>
                            <td colspan="6" style="text-align: right;">
                                ICMS: <?php echo  number_format( $percenticms,2,",",".");?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="7"></td>
                            <td colspan="6" style="text-align: right;font-size: 15px;"><b>Valor Total: <?php echo number_format( $valortotal,2,",",".");?></b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Arquivos js-->
<script src="<?php echo base_url();?>js/excanvas.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
<script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>js/sosmc.js"></script>
<script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>







