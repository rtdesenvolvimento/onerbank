<head>
    <title>Relatório de Vendas</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/blue.css" class="skin-color" />
    <script type="text/javascript"  src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style>
        td {
            padding: 2px;
            line-height: 15px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #ddd;
            font-size: 10px;
            border-left: 1px solid #ddd;
        }
    </style>
</head>

<body style="background-color: transparent">

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <div class="widget-box">
                <div class="widget-content nopadding">


                    <table class="table table-bordered">
                        <tr>
                            <td style="text-align: center">
                                <img src="http://resultaonline.ddns.net:9090/rapido/assets/images/avatar-1.jpg">
                            </td>
                            <td>
                                <b>BrasilCarbon Importação e Comércio Produtos para Indústria</b><br/><br/>
                                Rua Ibirama, Saguacú, Joinville, Santa Catarina, Brasil<br/>
                                CNPJ: 10.770.898/0001-30   IE: 25.584.804-8<br/>
                                FONE: + 55 47 3433-1856   email: contato@brasilcarbon.com.br
                            </td>
                        </tr>

                        <tr>
                            <?php
                                $numeroOrcamento = '';
                                foreach ($dados as $orcamento) {
                                    $numeroOrcamento = $orcamento->codigoorcameto;
                                    ?>
                                    <td colspan="2">
                                        Cliente  : <?php echo $orcamento->nomecliente;?> <br/>
                                        Contato  : <?php echo $orcamento->contato;?> <br/>
                                        Fone     : <?php echo $orcamento->telefone;?> <br/>
                                        Data     : <?php echo date(('d/m/Y'), strtotime( $orcamento->dataorcamento ));?> <br/>
                                    </td>
                                </tr>
                        <?php break; }?>
                    </table>

                    <div class="widget-title">

                    </div>
                    <table  style="font-size: 10px;width: 100%;">
                        <caption> <h4 style="text-align: center">Ordem de Corte  <?php echo $numeroOrcamento;?></h4></caption>
                        <thead>
                        <tr style="background: #9e9e9e7a;">
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">ITEM</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: left;">PRODUTO</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Xmm</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Ymm</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">Zmm</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">QTD.</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">O.S</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">POS</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: center;">ELETRODO Nº</th>
                            <th style="font-size: 1.2em; padding: 5px;text-align: right;">PESO Kg</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalPesoKg = 0;
                        $imprimir = true;
                        $contador = 1;
                        foreach ($dados as $orcamento) {
                            $totalPesoKg = $totalPesoKg + $orcamento->pesokg;
                            $backgroun = '#eee';

                            if (!$imprimir) {
                                $backgroun = 'white';
                                $imprimir = true;
                            } else {
                                $imprimir = false;
                            }

                            echo '<tr style="background: '.$backgroun.';">';
                            echo '<td style="text-align: left;">' . $contador . '</td>';
                            echo '<td style="text-align: left;">' . $orcamento->cdproduto. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->largura. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->altura. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->profundidade. '</td>';
                            echo '<td style="text-align: right;">' . $orcamento->quantidade. '</td>';
                            echo '<td style="text-align: center;">' . $orcamento->os. '</td>';
                            echo '<td style="text-align: center;">' . $orcamento->posicao. '</td>';
                            echo '<td style="text-align: center;">' . $orcamento->eletrodo. '</td>';
                            echo '<td style="text-align: right;">' .  number_format($orcamento->pesokg,2,",",".") . '</td>';
                            echo '</tr>';

                            $contador++;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="9">Peso total</td>
                            <td colspan="1" style="text-align: right;font-weight: bold"><?php echo  number_format( $totalPesoKg,2,",",".") ;?></td>
                        </tr>
                        </tfoot>
                    </table>
                    <h6 style="text-align: left">BrasilCarbon Importação e Comércio de Produtos para Indústria:: Data da Emissão: <?php echo date('d/m/Y');?></h6>

                </div>
            </div>
        </div>

    </div>
</div>




<!-- Arquivos js-->

<script src="<?php echo base_url();?>js/excanvas.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.peity.min.js"></script>
<script src="<?php echo base_url();?>js/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>js/sosmc.js"></script>
<script src="<?php echo base_url();?>js/dashboard.js"></script>
</body>
</html>







