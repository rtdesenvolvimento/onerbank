<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
ini_set('memory_limit', '-1');

ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
?>

<link href="<?php echo base_url(); ?>css/proposta/bootstrap.min2.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/matrix-style.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/matrix-media.css" rel="stylesheet"/>

<style type="text/css">

    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 14px;
        color: #0e0e0e;
        background: #ffffff;
    }

    .table th, .table td {
        padding: 6px;
        border: 0px;
        line-height: 10px;
        vertical-align: top;
    }

    .invoice-content {
        padding: 35px;
    }
</style>

<div class="span12" style="float: left;">
    <div class="invoice-content">
        <table class="table" style="margin-bottom: 0;">
            <tbody>
            <tr>
                <td colspan="2" style="text-align: center;border-top: 2px solid #0e0e0e;border-bottom: 2px solid #0e0e0e;line-height: 80px;">
                    <h4 style="font-size: 23px;font-weight: bold;float: left;margin: 10px;">TERMO DE ADESÃO AO GESTOR DE LDN 21</h4>
                    <img src="<?php echo base_url(); ?>/img/claro.jpg" style="width: 40px;float: left;margin-left: 20px;">
                </td>
            </tr>
            <tr>
                <td colspan="2"><p>À Empresa:</p></td>
            </tr>
            <tr>
                <td colspan="1">Razão Social</td>
                <td colspan="1">CNPJ</td>
            </tr>
            <tr>
                <td colspan="1" style="font-weight: normal;border: 1px solid #0e0e0e;"><?php echo strtoupper($proposta->nome);?></td>
                <td colspan="1" style="font-weight: normal;border: 1px solid #0e0e0e;"><?php echo $proposta->cnpjcpf;?></td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    Ref.: Opção pelo Código de Longa Distância “21” (EMBRATEL).
                </td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    Valendo-nos do direito de escolha do CSP na realização de chamadas de Longa Distância,
                    previsto no artigo 85 do Regulamento do Serviço Móvel Pessoal, e ciente que nas linhas
                    que possuem o módulo adicional do Gestor Online toda gestão das linhas, inclusive esse
                    direcionamento deverá ser feita através do próprio Gestor Online e são de
                    responsabilidade do administrador da ferramenta, e ainda que em caso de utilização de
                    outro CSP estará sujeita a cobrança de excedente na conta, segundo as tarifas da
                    prestadora utilizada. Venho por meio desse termo autorizar o direcionamento das ligações
                    de Longa Distância Nacional ao Código de Seleção de Prestadora de número 21 (Embratel)
                    dos telefones celulares CLARO de titularidade de nossa empresa, abaixo relacionados:
                </td>
            </tr>
            <tr>
                <td>
                    <img src="<?php echo base_url(); ?>/img/cabecalo-ldn21.jpg">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="table">
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;text-align: center;">Nº de celular</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;text-align: center;">Nº de celular</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;text-align: center;">Nº de celular</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;text-align: center;">Nº de celular</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;text-align: center;">Nº de celular</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                            <td style="border: 1px solid #0e0e0e;line-height: 20px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    Informo que o original desta carta será mantido arquivado nesta empresa pelo prazo de
                    cinco anos. Caso solicite, com aviso prévio de cinco dias úteis, uma via do documento
                    original será entregue à CLARO.
                </td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    Agradecemos desde já pelas providências.
                </td>
            </tr>
            </tbody>
        </table>

        <table class="table" style="margin-bottom: 0;">
            <tr><td colspan="4"><br/></td></tr>
            <tr>
                <td>Gestor Nomeado</td>
                <td colspan="3" style="border: 1px solid #0e0e0e;"><?php echo strtoupper($proposta->nomecontato);?></td>
            </tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr>
                <td>Local / Data</td>
                <td style="border: 1px solid #0e0e0e;"><?php echo strtoupper($proposta->strcidade);?> - <?php echo date(('d/m/Y'), strtotime($proposta->dataproposta));?></td>
                <td>Telefone de Contato</td>
                <td style="border: 1px solid #0e0e0e;"><?php echo $proposta->telefonecontato;?></td>
            </tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr>
                <td colspan="2" style="text-align: center;">__________________________________________________</td>
                <td colspan="2" style="text-align: center;">__________________________________________________</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">Assinatura Autorizada - ASSINANTE</td>
                <td colspan="2" style="text-align: center;">Assinatura Autorizada - ASSINANTE</td>
            </tr>
            <tr><td colspan="4" style="line-height: 5px;"><br/></td></tr>
        </table>

        <table class="table" style="margin-bottom: 0;">
            <tr>
                <td style="width: 10%;"></td>
                <td style="width: 40%;"></td>
                <td style="width: 10%;"></td>
                <td style="width: 40%;"></td>
            </tr>
            <tr>
                <td style="text-align: right;">Nome:</td>
                <td style="border: 1px solid #0e0e0e;font-size: 13px;"> <?php echo strtoupper($proposta->nomecontato);?> </td>
                <td style="text-align: right;">Nome:</td>
                <td style="border: 1px solid #0e0e0e;font-size: 13px;"> <?php echo strtoupper($proposta->nomecontatogestor);?> </td>
            </tr>
            <tr><td colspan="4" style="line-height: 5px;"><br/></td></tr>
            <tr>
                <td style="text-align: right;">Cargo</td>
                <td style="border: 1px solid #0e0e0e;"> ADM </td>
                <td style="text-align: right;">Cargo</td>
                <?php if($proposta->nomecontatogestor != '') {?>
                    <td style="border: 1px solid #0e0e0e;"> GESTOR</td>
                <?php } else { ?>
                    <td style="border: 1px solid #0e0e0e;"> </td>
                <?php } ?>
            </tr>
            <tr><td colspan="4" style="line-height: 5px;"><br/></td></tr>
            <tr>
                <td style="text-align: right;">CFP</td>
                <td style="border: 1px solid #0e0e0e;"> <?php echo $proposta->cpfcontato;?></td>
                <td style="text-align: right;">CFP</td>
                <td style="border: 1px solid #0e0e0e;"> <?php echo $proposta->cpfcontatogestor;?> </td>
            </tr>
        </table>

        <table class="table" style="margin-bottom: 0;">
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="1" style="text-align: right;border-top: 1px solid #0e0e0e;">
                    Página 1 de 1
                </td>
                <td colspan="1" style="text-align: center;border-top: 1px solid #0e0e0e;">
                    Versão 6.1 / 2015 4394820
                </td>
            </tr>
        </table>


    </div>
</div>