<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
ini_set('memory_limit', '-1');

ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
?>

<link href="<?php echo base_url(); ?>css/proposta/bootstrap.min2.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/matrix-style.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/matrix-media.css" rel="stylesheet"/>

<style type="text/css">

    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: sans-serif;
        font-size: 13px;
        color: #0e0e0e;
        background: #ffffff;
    }

    .table th, .table td {
        padding: 6px;
        border: 0px;
        line-height: 10px;
        vertical-align: top;
    }

    .invoice-content {
        padding: 35px;
    }
</style>

<div class="span12" style="float: left;">
    <div class="invoice-content">
        <table class="table" style="margin-bottom: 0;">
            <tbody>
            <tr>
                <td colspan="2" style="text-align: center;border-top: 2px solid #0e0e0e;border-bottom: 2px solid #0e0e0e;line-height: 80px;">
                    <h4 style="font-size: 23px;font-weight: bold;float: left;margin: 10px;">TERMO DE ADESÃO AO MÓDULO GESTOR ONLINE 3.0</h4>
                    <img src="http://localhost/rt-php-sos.net-1.0//img/claro.jpg" style="width: 40px;float: left;margin-left: 20px;">
                </td>
            </tr>
            <tr>
                <td colspan="2">Razão Social</td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: normal;border: 1px solid #0e0e0e;"><?php echo strtoupper($proposta->nome);?></td>
            </tr>
            <tr>
                <td colspan="2">CNPJ</td>
            </tr>
            <tr>
                <td colspan="1" style="font-weight: normal;border: 1px solid #0e0e0e;"><?php echo $proposta->cnpjcpf;?></td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    O serviço Gestor Online permite ao Cliente Corporativo efetuar a gestão e controle das linhas da empresa, através
                    do Site Claro, link <a href="httpz//empresasclaro.com:br/gestoronline"> httpz//empresasclaro.com:br/gestoronline</a>, ou diretamente pelo site Gestor Online, no
                    endereço: <a href="https//claroagestoronlineclaro.com.br/evpn4g/">https//claroagestoronlineclaro.com.br/evpn4g/</a>, o Gestor online poderá ser contratado para os
                    celulares habilitados nos Planos Sob Medida em Reais, Sob Medida Empresa e Plano Pronto.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;">
                    <span style="font-size: 18px;">Condições Comerciais:</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    Pelo presente instrumento particular de um lado CLARO e de outro o ASSINANTE, qualificado no Termo de
                    Contratação Pessoa Juridica, têm, entre si, certo e contratado o quanto segue:
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    1. O presente Termo de Adesão estipula as condições relativas ao Módulo "Gestor Online 3,0" comercializado
                    pela CLARO, de forma vinculada e exclusiva aos Planos PósAPagos Corporativo, Sob Medida em Reais, Sob Medida
                    Empresa e Plano Pronto, aos quais é parte integrante, conforme opção manifestada pelo ASSINANTE no Termo de
                    Contratação Pessoa Juridica.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    2. O ASSINANTE tendo exercido seu direito de opção de uma ou algumas das funcionalidades a seguir descritas,
                    para controle de uso de seus celulares, através deste Termo de Adesão, fica sujeito às caracteristicas e condições
                    descritas nos termos dos subitens 2.1 a 2.4 da presente cláusula:,
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    2.1 Controle por Horário/Calendário: permite restringir a originação de ligações de cada linha, baseada em dia e/
                    ou horário.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    2.2 Controle por Destino: permite restringir a originação de ligações de cada celular, para determinados destinos
                    e, concomitantemente, restringir o consumo a um total de minutos por linha.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    2.3 Controle por Limite de Consumo em Minutos: permite definir o limite de minutos liberados para uso de seus
                    funcionários. A limitação pode ser: acumulada ou por tipo de ligação, atribuida a um funcionario, compartilhada
                    pelo Centro de Custo ou Departamento.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    2.3.1 No Gestor Online 3.0 o Adm pode cancelar as chamadas em curso, ou seja, caso o limite de minutos definido
                    pela empresa seja atingindo durante uma ligação, o Gestor permite que esta seja terminada atraves da opção de
                    Bloqueio Imediato, onde o bloqueio é realizado no botão "Interromper a Ligação ao atingiro limite",
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    2.4 O Gestor Online controla e/ou bloqueia o tráfego de dados dentro da rede GSM/3G/4G/4.SG da Claro.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    3. As chamadas originadas a cobrar e para números 0800 não são controladas pelo Gestor Online, por serem
                    chamadas sem custo associado,
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    3.1 As chamadas originadas para o número do Administrador do Centro de Custo e para o Administrador do
                    Departamento estão sempre liberadas, porém serão cobradas normalmente de acordo com as tarifas do plano
                    contratado. Se o campo do número do Administrador do Centro de Custo ou do Departamento não for
                    preenchido, esta funcionalidade não estará habilitada. O número cadastrado poderá ser um fixo ou celular.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    4. O ASSINANTE reconhece que a pessoa nomeada no Termo de Adesão atuará como adm. dos serviços<
                    adquiridos perante a CLARO, com os seguintes poderes:
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    a) contratar e/ou alterar serviços, e/ou usuários elegíveis, por intermédio do Atendimento Claro PJ.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    b) promover o gerenciamento de uso dos celulares corporativos através do Serviço Gestor Online.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    4.1 Para consecução do disposto na cláusula 4 acima, o ASSINANTE recebera', pelo adm. por ele nomeado, uma
                    senha pessoal e intransferível, para sua utilização exclusiva relativamente ao controle de uso pelo qual tenha
                    optado com a qual o referido adm. tera' acesso às configurações das funcionalidades e ao cadastramento dos
                    usuários que terão acesso ao Serviço Gestor Online 3.0.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    4.1.1 O uso e guarda da senha indicada na cláusula 4.1 acima é de total e exclusiva responsabilidade do
                    ASSINANTE, na pessoa do gestor por ele indicado, não cabendo à CLARO qualquer responsabilidade por quaisquer
                    danos causados a terceiros pelo uso indevido da senha e das funcionalidades.
                </td>
            </tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="1" style="text-align: right;border-top: 1px solid #0e0e0e;">
                    Página 1 de 2
                </td>
                <td colspan="1" style="text-align: center;border-top: 1px solid #0e0e0e;">
                    Versão 7.0 / 20180219
                </td>
            </tr>
            </tbody>
        </table>


        <!--################"-->
        <!-- SEGUNDA PAGINDA -->
        <!--################"-->
        <table class="table" style="margin-bottom: 0;">
            <tbody>
            <tr>
                <td colspan="2" style="text-align: center;border-top: 2px solid #0e0e0e;border-bottom: 2px solid #0e0e0e;line-height: 80px;">
                    <h4 style="font-size: 23px;font-weight: bold;float: left;margin: 10px;">TERMO DE ADESÃO AO MÓDULO GESTOR ONLINE 3.0</h4>
                    <img src="http://localhost/rt-php-sos.net-1.0//img/claro.jpg" style="width: 40px;float: left;margin-left: 20px;">
                </td>
            </tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    4.1.2 O ASSINANTE deverá comunicar, mediante envio de correspondência ou por e-mail, o desligamento ou
                    substituição do adm. nomeado, providenciando, incontinente, o bloqueio de todo e qualquer acesso a que o
                    mesmo tenha por força deste instrumento, indicando ainda o novo gestor.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    4.1.3 Ao utilizar o Gestor Online 3.0 possibilitá a aplicação de uso do CSP21.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    4.1.4 O acesso ao portal só será possível através do e-mail do adm. da conta.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    5. O serviço Gestor Online 3.0 é válido em roaming nacional em todo o país para chamadas de voz e SMS MO. O
                    serviço não controla o uso de Dados GPRS/EDGE fora da rede Claro.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    6. Todos os planos Pós-Pago PME são bloqueados para Roaming Internacional, para desbloquear, o administrador
                    da conta deve entrar em contato no atendimento *468 e somente após este desbloqueio, a gestão de uso do
                    Roaming Internacional passa a ser controlada pelo Gestor Online 3.0.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    7. Na funcionalidade “Discagem abreviada”, existem as seguintes restrições quanto à definição dos ramais
                    associados a cada linha:
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    7.1 Ramais de 4 a 6 dígitos não podem ser iniciados por 0, 1 e 9, além dos números especiais utilizados pela Claro.
                    O presente Termo vigerá pelo mesmo prazo e observará as mesmas condições do Termo de Contratação Pessoa
                    Jurídica para a prestação do Serviço Móvel Pessoal, implicando a rescisão dos mesmos no cancelamento
                    automático do serviço Gestor Online 3.0.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    8. Fica estipulado, entretanto que o ASSINANTE poderá, a qualquer tempo, cancelar o presente Termo, total ou
                    parcialmente de forma isolada.
                </td>
            </tr>
            <tr>
                <td colspan="2" style="line-height: 15px;text-align: justify;padding: 0px;">
                    9. Permanecem válidas e em pleno vigor todas as disposições do Termo de Contratação Pessoa Jurídica a que
                    tenha optado o ASSINANTE, que não sejam conflitantes com os dispostos no presente instrumento.
                </td>
            </tr>
            </tbody>
        </table>
        <table class="table" style="margin-bottom: 0;">
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr>
                <td>Gestor Nomeado</td>
                <td colspan="3" style="border: 1px solid #0e0e0e;"><?php echo strtoupper($proposta->nomecontato);?></td>
            </tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr>
                <td>Local / Data</td>
                <td style="border: 1px solid #0e0e0e;"><?php echo strtoupper($proposta->strcidade);?> - <?php echo date(('d/m/Y'), strtotime($proposta->dataproposta));?></td>
                <td>Telefone de Contato</td>
                <td style="border: 1px solid #0e0e0e;"><?php echo $proposta->telefonecontato;?></td>
            </tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr><td colspan="4"><br/></td></tr>
            <tr>
                <td colspan="2" style="text-align: center;">__________________________________________________</td>
                <td colspan="2" style="text-align: center;">__________________________________________________</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">Assinatura Autorizada - ASSINANTE</td>
                <td colspan="2" style="text-align: center;">Assinatura Autorizada - ASSINANTE</td>
            </tr>
            <tr><td colspan="4"><br/></td></tr>

        </table>

        <table class="table" style="margin-bottom: 0;">
            <tr>
                <td style="width: 10%"></td>
                <td style="width: 40%"></td>
                <td style="width: 10%"></td>
                <td style="width: 40%"></td>
            </tr>
            <tr>
                <td style="text-align: right;">Nome:</td>
                <td style="border: 1px solid #0e0e0e;"> <?php echo strtoupper($proposta->nomecontato);?> </td>
                <td style="text-align: right;">Nome:</td>
                <td style="border: 1px solid #0e0e0e;"> <?php echo strtoupper($proposta->nomecontatogestor);?> </td>
            </tr>
            <tr><td colspan="4"><br/></td></tr>

            <tr>
                <td style="text-align: right;">Cargo</td>
                <td style="border: 1px solid #0e0e0e;"> ADM </td>
                <td style="text-align: right;">Cargo</td>
                <?php if($proposta->nomecontatogestor != '') {?>
                    <td style="border: 1px solid #0e0e0e;"> GESTOR</td>
                <?php } else { ?>
                    <td style="border: 1px solid #0e0e0e;"> </td>
                <?php } ?>
            </tr>
            <tr><td colspan="4"><br/></td></tr>

            <tr>
                <td style="text-align: right;">CFP</td>
                <td style="border: 1px solid #0e0e0e;"> <?php echo $proposta->cpfcontato;?>  </td>
                <td style="text-align: right;">CFP</td>
                <td style="border: 1px solid #0e0e0e;"> <?php echo $proposta->cpfcontatogestor;?> </td>
            </tr>
        </table>

        <table class="table" style="margin-bottom: 0;">

            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="1" style="text-align: right;border-top: 1px solid #0e0e0e;">
                    Página 2 de 2
                </td>
                <td colspan="1" style="text-align: center;border-top: 1px solid #0e0e0e;">
                    Versão 7.0 / 20180219
                </td>
            </tr>
        </table>


    </div>
</div>