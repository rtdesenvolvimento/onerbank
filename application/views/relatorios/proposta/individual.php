<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
ini_set('memory_limit', '-1');

ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
?>

<link href="<?php echo base_url(); ?>css/proposta/bootstrap.min2.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/matrix-style.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>css/proposta/matrix-media.css" rel="stylesheet"/>

<style type="text/css">

    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: sans-serif;
        font-size: 10px;
        color: #0e0e0e;
        background: #ffffff;
    }

    .table th, .table td {
        padding: 3px;
        border: 0px;
        line-height: 8px;
        vertical-align: top;
    }

    .tableInner {
        width: 100%;
    }

    .tableInner td {
        padding: 2px;
        border-bottom: 1px solid #0e0e0e;
        border-right: 1px solid #0e0e0e;
    }

    .tableInner th {
        border-bottom: 1px solid #0e0e0e;
        border-right: 1px solid #0e0e0e;
    }

</style>
<div class="span12" style="float: left;width: 1350px;">
    <div>
        <table class="table" style="margin-bottom: 0;">
            <tbody>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2" style="border: 0px;">
                    <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Razão Social da Revenda</td>
                            <td colspan="2" style="border-bottom: 0px;">CNPJ</td>
                            <td colspan="2" style="border-bottom: 0px;">Código da Revenda</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Segmento</td>
                        </tr>
                        <tr>
                            <td colspan="2">SEVEN ELEVEN COMUNICAÇÕES EIRELI</td>
                            <td colspan="2">30.831.687.0001-78</td>
                            <td colspan="2">YHRJ</td>
                            <td colspan="2" style="border-right: 0px;">
                                <img src="<?php echo base_url(); ?>/img/segmento-aace-pme.jpg">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Nome Completo do Consultor</td>
                            <td colspan="2" style="border-bottom: 0px;">Código do Consultor</td>
                            <td colspan="4" style="border-bottom: 0px;border-right: 0px;">Canal de Venda</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2"><?php echo strtoupper($proposta->nomeresponsavel);?></td>
                            <td colspan="2">MWJ4V</td>
                            <td colspan="4" style="border-right: 0px;">
                                <img src="<?php echo base_url(); ?>/img/canal-venda.jpg">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h5>1. Identificação da Empresa</h5></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Razão Social</td>
                            <td colspan="2" style="border-bottom: 0px;">CNPJ</td>
                            <td colspan="2" style="border-bottom: 0px;">Inscrição Estadual</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Inscrição Municipal</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2"><?php echo strtoupper($proposta->nome);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->cnpjcpf);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->inscricaoestadual);?></td>
                            <td colspan="2" style="border-right: 0px;"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">CNAE Primário</td>
                            <td colspan="2" style="border-bottom: 0px;">CNAE Secundário</td>
                            <td colspan="2" style="border-bottom: 0px;">Representante Legal</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Número de funcionários</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2"><?php echo strtoupper($proposta->cnaeprimario);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->cnaesecundario);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->nomecontato);?></td>
                            <td colspan="2" style="border-right: 0px;">
                                <img src="<?php echo base_url(); ?>/img/numero-funcionarios-ate-10.jpg">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">1º Administrador</td>
                            <td colspan="2" style="border-bottom: 0px;">CFP</td>
                            <td colspan="2" style="border-bottom: 0px;">Telefone</td>
                            <td colspan="1" style="border-bottom: 0px;">Celular</td>
                            <td colspan="1" style="border-bottom: 0px;border-right: 0px;">E-mail</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2"><?php echo strtoupper($proposta->nomecontato);?></td>
                            <td colspan="2"><?php echo $proposta->cpfcontato;?></td>
                            <td colspan="2"></td>
                            <td colspan="1"><?php echo $proposta->telefonecontato;?></td>
                            <td colspan="1" style="border-right: 0px;"><?php echo $proposta->emailcontato;?></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">2º Administrador</td>
                            <td colspan="2" style="border-bottom: 0px;">CFP</td>
                            <td colspan="2" style="border-bottom: 0px;">Telefone</td>
                            <td colspan="1" style="border-bottom: 0px;">Celular</td>
                            <td colspan="1" style="border-bottom: 0px;border-right: 0px;">E-mail</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="1" style="border-right: 0px;">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;">Endereço da Sede (o mesmo do cartão CNPJ)</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Logradouro</td>
                            <td colspan="2" style="border-bottom: 0px;">Número</td>
                            <td colspan="2" style="border-bottom: 0px;">Complemento</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Telefone Fixo</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2"><?php echo strtoupper($proposta->endereco);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->numero);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->complemento);?></td>
                            <td colspan="2" style="border-right: 0px;"><?php echo strtoupper($proposta->telefone);?></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Bairro</td>
                            <td colspan="2" style="border-bottom: 0px;">Cidade</td>
                            <td colspan="2" style="border-bottom: 0px;">Estado</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">CEP</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2"><?php echo strtoupper($proposta->strbairro);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->strcidade);?></td>
                            <td colspan="2"><?php echo strtoupper($proposta->strestado);?></td>
                            <td colspan="2" style="border-right: 0px;"><?php echo strtoupper($proposta->cep);?></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;">Referências Comerciais</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Empresa</td>
                            <td colspan="2" style="border-bottom: 0px;">CNPJ</td>
                            <td colspan="2" style="border-bottom: 0px;">Contato da Empresa</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Telefone</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2" style="border-right: 0px;">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Empresa</td>
                            <td colspan="2" style="border-bottom: 0px;">CNPJ</td>
                            <td colspan="2" style="border-bottom: 0px;">Contato da Empresa</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Telefone</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2" style="border-right: 0px;">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;">Dados Bancários</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="border-bottom: 0px;">Banco</td>
                            <td colspan="1" style="border-bottom: 0px;">Cód Banco</td>
                            <td colspan="1" style="border-bottom: 0px;">Agência</td>
                            <td colspan="1" style="border-bottom: 0px;">Conta Corrente</td>
                            <td colspan="1" style="border-bottom: 0px;">Conta no Banco</td>
                            <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Telefone do Contato</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="2" style="border-right: 0px;">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h5>2. Identificação do Produto</h5></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2"><br/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="<?php echo base_url(); ?>/img/identificacaoproduto-plano-claro-total-individual-x.jpg">
                </td>
            </tr>
            <tr>
                <td colspan="2"><br/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="4">Data do Vencimento</td>
                            <td colspan="4" style="border-right: 0px;">Tempo de Vigência de Contrato</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="4">
                                <img src="<?php echo base_url(); ?>/img/vencimento-20.jpg">
                            </td>
                            <td colspan="4" style="border-right: 0px;">
                                <img src="<?php echo base_url(); ?>/img/24meses.jpg">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="4" style="border: 0px;">Plano Claro Total Individual + BL - Claro Life</td>
                            <td colspan="4" style="border: 0px;">Página 1 de 5</td>
                            <td colspan="4" style="border: 0px;text-align: right">20180102_Claro Total Individual+BL.jan19_v4</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <?php if (count($propostafranquiasIndividual) > 0) {?>
                <!--######### CABECALHO ########!--->
                <tr><td colspan="2"><br/></td></tr>
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td colspan="2" style="border: 0px;">
                        <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                    </td>
                </tr>
                <tr><td colspan="2"><br/></td></tr>
            <?php } ?>

            </tbody>
        </table>
        <?php if (count($propostafranquiasIndividual) > 0) {?>

        <!--######### SEGUNDA PAGINA ########!--->
        <div style="border: 1px solid #0e0e0e;width: 100%;">
            <table class="table">
                <tbody>
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="4" style="border: 1px solid #0e0e0e;border-bottom: 3px solid #0e0e0e;"><h4><br/>Plano Claro Total Individual - Claro Life (Plano Anatel Nº 190)</h4></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border: 0px;width: 50%;">
                                    <img src="<?php echo base_url(); ?>/img/possuiservico-embratel-net.jpg">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br/></td>
                </tr>


                <!--######### PLANOS    ########!--->
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                           <tr>
                               <td colspan="7" style="border: 0px;"></td>
                               <td colspan="4" style="border: 1px solid #0e0e0e;line-height: 40px;text-align: center;font-weight: bold;">Modulos</td>
                               <td colspan="4" style="border: 1px solid #0e0e0e;line-height: 40px;text-align: center;font-weight: bold;">Passaporte</td>
                               <td colspan="1" style="border: 0px;"></td>
                           </tr>
                            <tr>
                                <td colspan="1" style="border: 0px;"></td>
                                <td colspan="1" style="border: 0px;"></td>

                                <td colspan="4" style="border: 1px solid #0e0e0e;text-align: center;line-height: 40px;font-weight: bold;">Franquia Individual</td>

                                <td colspan="1" style="border: 0px;"></td>

                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 40px;width: 10%;font-weight: bold;">
                                    <img src="<?php echo base_url(); ?>/img/mobilidade-waze-easytax-capify.jpg">
                                </td>

                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 40px;width: 10%;font-weight: bold;">
                                    <img src="<?php echo base_url(); ?>/img/mobilidade-redes-sociais.jpg">
                                </td>

                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 40px;font-weight: bold;">
                                    Passaporte Incluso
                                </td>

                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 40px;font-weight: bold;">
                                    Passaporte avulso
                                </td>

                                <td colspan="1" style="border: 0px;text-align: center;line-height: 40px;font-weight: bold;">
                                   Total
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Regional</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">DDD</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Tipo de Solicitação</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Plano Individual</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Valor R$</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Dobro<br/>Bônus</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Qtd<br/>Linhas</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Possui</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">R$ 0,00</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Possui</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">R$ 0,00</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Possui</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">R$ 0,00</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Contratou</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Valor</td>
                                <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">(Franquia +<br/>Plug-Ins + pacotes)</td>
                            </tr>

                            <?php
                            $totalLinhas = 0;
                            $totalFranquias = 0;

                            foreach ($propostafranquiasIndividual as $franquia) {
                                $totalLinhas = $totalLinhas + $franquia->qtdlinhas;
                                $totalFranquias = $totalFranquias + $franquia->total;

                                ?>
                                <tr>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->regional);?></td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->ddd);?></td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-size: 12px;"><?php echo strtoupper($franquia->tiposolicitacaoproposta);?></td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;"><?php echo strtoupper($franquia->franquia);?></td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $franquia->valorfranquia ,2,',','.');?></td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;"><?php echo strtoupper($franquia->bonus);?></td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $franquia->qtdlinhas;?></td>

                                    <?php if($franquia->ispossuimobilidade == 't') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">SIM</td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">NÃO</td>
                                    <?php } ?>

                                    <?php if($franquia->ispossuimobilidade == 't') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">
                                            <?php echo number_format( $franquia->valormobilidade ,2,',','.');?>
                                        </td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php } ?>

                                    <?php if($franquia->ispossuiredessociais == 't') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">SIM</td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">NÃO</td>
                                    <?php } ?>

                                    <?php if($franquia->ispossuiredessociais == 't') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">
                                            <?php echo number_format( $franquia->valorredessociais ,2,',','.');?>
                                        </td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php } ?>


                                    <?php if($franquia->passaporte != '') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">SIM</td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php } ?>

                                    <?php if($franquia->passaporte != '') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo number_format( $franquia->valorpassaporte ,2,',','.');?></td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php } ?>

                                    <?php if($franquia->passaporteavulso != '') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">SIM</td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php } ?>

                                    <?php if($franquia->passaporte != '') {?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo number_format( $franquia->valorpassaporteavulso ,2,',','.');?></td>
                                    <?php } else { ?>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php } ?>

                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-size:16px;font-weight:bold;">R$ <?php echo number_format( $franquia->total ,2,',','.');?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="4" style="border:0px;font-size:16px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-1.jpg');background-repeat: no-repeat;">
                                    R$ <?php echo number_format( $totalFranquias ,2,',','.');?>
                                </td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border:0px;font-size:16px;font-weight:bold;text-align: center;line-height: 54px;background-image: url('<?php echo base_url(); ?>/img/total-linhas.jpg');background-repeat: no-repeat;background-position-x: 45%;">
                        <?php echo $totalLinhas;?>
                    </td>
                </tr>

                <!--######### APARELHOS ########!--->
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h5>Equipamentos e Forma de Aquisição</h5></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">UF</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">DDD</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Modelo Aparelho</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Cor</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Fabricante</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Qtd Sim<br/>Card</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Preço Unit.<br/>Sim Card</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Qtd<br/>Aparelho</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Preço Unit. Real<br/>Aparelho</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Pontuação</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Faixa</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Avulso</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Total</td>
                                <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Forma de<br/>Pagamento</td>
                            </tr>
                            <tbody>
                            <?php
                            $totalSimCard = 0;
                            $totalAparelhos = 0;
                            $totalValorAparelhos = 0;
                            foreach ($propostaaparelhosIndividual as $aparelho) {
                                $totalSimCard = $totalSimCard + $aparelho->qtdsimcard;
                                $totalAparelhos = $totalAparelhos + $aparelho->qtdaparelhos;
                                $totalValorAparelhos = $totalValorAparelhos + $aparelho->total;
                                ?>
                                <tr>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->uf);?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->ddd);?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->produto);?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->cor);?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->fabricante);?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $aparelho->qtdsimcard;?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $aparelho->precounitariosimcard ,2,',','.');?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $aparelho->qtdaparelhos;?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $aparelho->precounitarioaparelho ,2,',','.');?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->pontuacao);?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"></td>
                                    <?php if ( $aparelho->isavulso == 't'){?>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">Sim</td>
                                    <?php } else { ?>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">Não</td>
                                    <?php } ?>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $aparelho->total ,2,',','.');?></td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $aparelho->formapagamento;?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-2.jpg');background-repeat: no-repeat;"><?php echo $totalSimCard;?></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-2.jpg');background-repeat: no-repeat;"><?php echo $totalAparelhos;?></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-2.jpg');background-repeat: no-repeat;"></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="border: 0px;"></td>
                                <td colspan="2" style="width: 12%;border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-3.jpg');background-repeat: no-repeat;">R$  <?php echo number_format( $totalValorAparelhos ,2,',','.');?></td>
                                <td colspan="2" style="border: 0px;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="8" style="border: 0px;text-align: right">"Parcelamento" de equipamentos elegível para valores acima de R$ 48,00</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <table class="tableInner" style="width: 50%">
                            <tr>
                                <td colspan="4" style="border: 0px;">
                                    <img src="<?php echo base_url(); ?>/img/aceita-outra-cor-sim-x-modalidade-aquisicao.jpg">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
        <?php } ?>

        <?php if (count($propostafranquiasIndividual) > 0) {?>
            <table class="table">
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="4" style="border: 0px;">Plano Claro Total Individual + BL - Claro Life</td>
                                <td colspan="4" style="border: 0px;">Página 2 de 5</td>
                                <td colspan="4" style="border: 0px;text-align: right">20180102_Claro Total Individual+BL.jan19_v4</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        <?php } ?>

        <?php if (count($propostafranquiasIndividual) > 0) {?>
            <table class="table">
                <tbody>
                <!--######### CABECALHO ########!--->
                <tr><td colspan="2"><br/></td></tr>
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td colspan="2" style="border: 0px;">
                        <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                    </td>
                </tr>
                </tbody>
            </table>

            <div style="border: 1px solid #0e0e0e;width: 100%;">
                <table class="table">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner" style="width: 70%">
                                <tr>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;width: 50%;line-height: 25px;">QTD LINHAS CONTRATADA "CT INDIVIDUAL"</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;width: 50%;text-align: center;line-height: 25px;"><?php echo $totalLinhas;?></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;width: 50%;line-height: 25px;">QTD DE APARELHOS /  PONTUAÇÃO</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;width: 50%;text-align: center;line-height: 25px;">0</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;width: 50%;line-height: 25px;">QTD DE APARELHOS / FAIXA</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;width: 50%;text-align: center;line-height: 25px;"><?php echo $totalAparelhos;?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner">
                                <tr>
                                    <td colspan="8" style="border: 0px;text-align: center;"><h5>CONSOLIDADO</h5></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner">
                                <tr>
                                    <td style="width: 20%;border: 0px;"></td>
                                    <td style="width: 10%;border: 0px;"></td>
                                    <td style="width: 20%;border: 0px;"></td>
                                    <td style="width: 10%;border: 0px;"></td>
                                    <td style="width: 10%;border: 0px;"></td>
                                    <td style="width: 20%;border: 0px;"></td>
                                    <td style="width: 10%;border: 0px;"></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style=";border: 0px;">TOTAL GERAL PONTOS</td>
                                    <td colspan="1" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-3.jpg');background-repeat: no-repeat"></td>
                                    <td colspan="1" style=";border: 0px;">TOTAL GERAL EQUIPAMENTOS "QTD / VALOR"</td>
                                    <td colspan="1" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-3.jpg');background-repeat: no-repeat"></td>
                                    <td colspan="1" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-3.jpg');background-repeat: no-repeat"></td>
                                    <td colspan="1" style=";border: 0px;">TOTAL GERAL SERVIÇOS</td>
                                    <td colspan="1" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-3.jpg');background-repeat: no-repeat"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner" style="width: 50%;">
                                <tr>
                                    <td colspan="8" style="border: 0px">
                                        <img src="<?php echo base_url(); ?>/img/modalidade-aquisicao-venda.jpg">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <br/><br/><br/><br/><br/><br/><br/><br/>
        <?php } ?>

        <?php if (count($propostafranquiasCompartilhado) > 0) {?>
            <table class="table">
                <!--######### CABECALHO ########!--->
                <tr><td colspan="2"><br/></td></tr>
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td colspan="2" style="border: 0px;">
                        <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                    </td>
                </tr>
            </table>
        <?php }?>


        <?php  if (count($propostafranquiasCompartilhado) > 0) {?>
            <!--######### SEGUNDA PAGINA - COMPARTILHADO ########!--->
            <div style="border: 1px solid #0e0e0e;width: 100%;">
                <table class="table">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner">
                                <tr>
                                    <td colspan="4" style="border: 0px;"><h4><br/>Plano Internet Móvel  - Individual</h4></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <!--######### PLANOS    ########!--->
                    <tr><td colspan="2"><br/></td></tr>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner">
                                <tr>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">COD. ANATEL</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Regional</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">DDD</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Tipo de Solicitação</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Planos</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Franquia</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Bõnus</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Qtd</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Valor Unitário</td>
                                    <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Total</td>
                                </tr>
                                <?php
                                $totalLinhas = 0;
                                $totalFranquias = 0;
                                foreach ($propostafranquiasCompartilhado as $franquia) {
                                    $totalLinhas = $totalLinhas + $franquia->qtdlinhas;
                                    $totalFranquias = $totalFranquias +  $franquia->total;
                                    ?>

                                    <tr>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->anatel);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->regional);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->ddd);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->tiposolicitacaoproposta);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->franquia);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($franquia->strfranquia);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;"><?php echo strtoupper($franquia->bonus);?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $franquia->qtdlinhas;?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $franquia->valorfranquia ,2,',','.');?></td>
                                        <td colspan="1" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $franquia->total ,2,',','.');?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-2.jpg');background-repeat: no-repeat;width: 7%;"><?php echo $totalLinhas;?></td>
                                    <td colspan="1" style="border: 0px;line-height: 15px;"></td>
                                    <td colspan="1" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-3.jpg');background-repeat: no-repeat;width: 12%;">R$ <?php echo number_format( $totalFranquias,2,',','.');?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <tr><td colspan="2"><br/></td></tr>
                    <!--######### APARELHOS ########!--->
                    <tr>
                        <td colspan="2">
                            <table class="tableInner">
                                <tr>
                                    <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h4>Equipamentos e Forma de Aquisição</h4></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table class="tableInner">
                                <tr>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">UF</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">DDD</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Modelo Aparelho</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Cor</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Fabricante</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Qtd Sim<br/>Card</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Preço Unit.<br/>Sim Card</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Qtd<br/>Aparelho</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Preço Unit. Real<br/>Aparelho</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Avulso?</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Total</td>
                                    <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Forma de<br/>Pagamento</td>
                                </tr>
                                <?php
                                $totalSimCard = 0;
                                $totalAparelhos = 0;
                                $totalValorAparelhos = 0;
                                foreach ($propostaaparelhosCompartilhado as $aparelho) {
                                    $totalSimCard = $totalSimCard + $aparelho->qtdsimcard;
                                    $totalAparelhos = $totalAparelhos + $aparelho->qtdaparelhos;
                                    $totalValorAparelhos = $totalValorAparelhos + $aparelho->total;
                                    ?>
                                    <tr>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->uf);?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->ddd);?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;">Modelo Aparelho</td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->cor);?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo strtoupper($aparelho->fabricante);?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $aparelho->qtdsimcard;?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $aparelho->precounitariosimcard ,2,',','.');?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $aparelho->qtdaparelhos;?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $aparelho->precounitarioaparelho ,2,',','.');?></td>
                                        <?php if ( $aparelho->isavulso == 't'){?>
                                            <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">Sim</td>
                                        <?php } else { ?>
                                            <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">Não</td>
                                        <?php } ?>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;font-weight: bold;">R$ <?php echo number_format( $aparelho->total ,2,',','.');?></td>
                                        <td colspan="2" style="border: 1px solid #0e0e0e;text-align: center;line-height: 15px;"><?php echo $aparelho->formapagamento;?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-2.jpg');background-repeat: no-repeat;"><?php echo $totalSimCard;?></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-2.jpg');background-repeat: no-repeat;"><?php echo $totalAparelhos;?></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border:0px;font-weight:bold;text-align: center;line-height: 27px;background-image: url('<?php echo base_url(); ?>/img/linha-vermelha-1.jpg');background-repeat: no-repeat;width: 12%">R$  <?php echo number_format( $totalValorAparelhos ,2,',','.');?></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                    <td colspan="2" style="border: 0px;"></td>
                                  </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br/></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if (count($propostaportabilidades) > 0){?>
            <table class="table" style="border: 1px solid #0e0e0e;width: 100%;">
                <tr>
                    <td colspan="2">
                        <table class="tableInner">
                            <tr>
                                <td colspan="4" style="border: 0px;">Plano Claro Total Individual + BL - Claro Life</td>
                                <td colspan="4" style="border: 0px;">Página 2 de 5</td>
                                <td colspan="4" style="border: 0px;text-align: right">20180102_Claro Total Individual+BL.jan19_v4</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!--######### CABECALHO ########!--->
                <tr><td colspan="2"><br/></td></tr>
                <tr><td colspan="2"><br/></td></tr>
                <tr>
                    <td colspan="2" style="border: 0px;">
                        <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                    </td>
                </tr>
            </table>
        <?php } ?>

        <!--######### TERCEIRA PÁGINA -  PORTABILIDADE ########!--->

        <?php foreach ($propostaportabilidades as $propostaportabilidade) { ?>
        <div style="border: 1px solid #0e0e0e;width: 100%;">
            <table class="table" style="border: 1px solid #0e0e0e;width: 100%;">
            <tbody>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h4>Dados para Portabilidade</h4></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner" style="width: 50%;">
                        <tr>
                            <td colspan="2" style="border: 0px;text-align: center;">DDD</td>
                            <td colspan="2" style="border: 0px;text-align: center;">Plano</td>
                            <td colspan="2" style="border: 0px;text-align: center;">Operadora</td>
                            <td colspan="2" style="border: 0px;text-align: center;">SimCard</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td colspan="2" style="border: 0px;text-align: center;"><?php echo $propostaportabilidade->ddd;?></td>
                            <td colspan="2" style="border: 0px;text-align: center;"><?php echo $propostaportabilidade->plano;?></td>
                            <td colspan="2" style="border: 0px;text-align: center;"><?php echo $propostaportabilidade->operadora;?></td>
                            <td colspan="2" style="border: 0px;text-align: center;"><?php echo $propostaportabilidade->simcard;?></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-top: 1px solid #0e0e0e;border-right: 0px"><h6>Relação de Números de celulares a serem portados</h6></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8">
                                <?php echo $propostaportabilidade->telefones;?>
                                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
        <?php } ?>

        <table class="table" style="margin-bottom: 0;border: 1px solid #0e0e0e;">
            <tbody>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="4" style="border: 0px;">Plano Claro Total Individual + BL - Claro Life</td>
                            <td colspan="4" style="border: 0px;">Página 3 de 5</td>
                            <td colspan="4" style="border: 0px;text-align: right">20180102_Claro Total Individual+BL.jan19_v4</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <!--######### CABECALHO ########!--->
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2" style="border: 0px;">
                    <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                </td>
            </tr>
            </tbody>
        </table>

        <!--######### QUARTA PÁGINA -  PORTABILIDADE ########!--->
        <div style="border: 1px solid #0e0e0e;width: 100%;">
            <table class="table" style="margin-bottom: 0;border: 1px solid #0e0e0e;">
            <tbody>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h4>Resumo Total Contratato</h4></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="2" style="text-align: center;line-height: 35px;">Total Mensal Serviços:</td>
                            <td colspan="1" style="border: 1px solid #0e0e0e;font-size: 15px;line-height: 35px;width: 10%;text-align: center;"><h4>R$ <?php echo number_format( $proposta->valorplanoservico ,2,',','.');?></h4></td>
                            <td colspan="1" style="text-align: center;line-height: 35px;">Total Equipamentos / Chips</td>
                            <td colspan="1" style="border: 1px solid #0e0e0e;font-size: 15px;line-height: 35px;width: 10%;text-align: center;"><h4>R$ <?php echo number_format( $proposta->valoraparelho ,2,',','.');?></h4></td>
                            <td colspan="1" style="text-align: center;line-height: 35px;">Total Equipamentos + Chips + Serviços</td>
                            <td colspan="2" style="border: 1px solid #0e0e0e;font-size: 15px;line-height: 35px;width: 10%;text-align: center;"><h4>R$ <?php echo number_format( $proposta->valorplanoservico + $proposta->valoraparelho ,2,',','.');?></h4></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="8" style="border-right: 0px;">Todos os valores do contrato estão com impostos</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner" style="width: 90%;">
                        <tr>
                            <td colspan="8" style="border-bottom: 0px;border-right: 0px;"><h4>Recebimento do Aparelho</h4></td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <table class="tableInner">
                                    <tr>
                                        <td colspan="2" style="border-bottom: 0px;">1º Responsável pelo Recebimento</td>
                                        <td colspan="2" style="border-bottom: 0px;">RG</td>
                                        <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Telefone</td>
                                    </tr>
                                    <tbody>
                                    <tr>
                                        <td colspan="2"><?php echo strtoupper($proposta->nomecontato);?></td>
                                        <td colspan="2">AQUI O RG INLCLUIR</td>
                                        <td colspan="2" style="border-right: 0px;"><?php echo strtoupper($proposta->telefonecontato);?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="border-bottom: 0px;">2º Responsável pelo Recebimento</td>
                                        <td colspan="2" style="border-bottom: 0px;">RG</td>
                                        <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Telefone</td>
                                    </tr>
                                    <tbody>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td colspan="2"></td>
                                        <td colspan="2" style="border-right: 0px;"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="4" style="border-bottom: 0px;width: 40%;">
                                <table class="tableInner">
                                    <tr>
                                        <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Local</td>
                                        <td colspan="2" style="border-bottom: 0px;border-right: 0px;">Data</td>
                                    </tr>
                                    <tbody>
                                    <tr>
                                        <td colspan="2" style="border-right: 0px;"><?php echo strtoupper($proposta->strcidade);?></td>
                                        <td colspan="2" style="border-right: 0px;"><?php echo date(('d/m/Y'), strtotime($proposta->dataproposta));?></td>
                                    </tr>
                                    <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
                                    <tr><td colspan="4" style="border-right: 0px;"><br/></td></tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 0px;border-right: 0px;"><?php echo strtoupper($proposta->nomecontato);?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 0px;border-right: 0px;">1º Representante Legal da Empresa</td>
                                    </tr>
                                    <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
                                    <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
                                    <tr><td colspan="4" style="border-right: 0px;"><br/></td></tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 0px;border-right: 0px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border-bottom: 0px;border-right: 0px;">2º Representante Legal da Empresa</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td colspan="4" style="border: 1px solid #0e0e0e;">
                                Autenticação<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr><td colspan="4" style="border-right: 0px;border-bottom: 0px;"><br/></td></tr>
                        <tr>
                            <td colspan="8" style="border-right: 0px;border-bottom: 0px;">
                                <img src="<?php echo base_url(); ?>/img/termos-1.jpg">
                            </td>
                        </tr>
                        <tr><td colspan="4" style="border-right: 0px;border-bottom: 0px;border-top: 0px;"><br/></td></tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
        <table class="table" style="margin-bottom: 0;border: 1px solid #0e0e0e;">
            <tbody>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="4" style="border: 0px;">Plano Claro Total Individual + BL - Claro Life</td>
                            <td colspan="4" style="border: 0px;">Página 4 de 5</td>
                            <td colspan="4" style="border: 0px;text-align: right">20180102_Claro Total Individual+BL.jan19_v4</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <!--######### CABECALHO ########!--->
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2" style="border: 0px;">
                    <img src="<?php echo base_url(); ?>/img/cabecalho.jpg">
                </td>
            </tr>
            <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
            </tbody>
        </table>
        <!--######### QUINTA PÁGINA -  DECLARACAO DO ASSINANTE ########!--->
        <div style="border: 1px solid #0e0e0e;width: 100%;">
            <table class="table" style="margin-bottom: 0;border: 1px solid #0e0e0e;">
                <tbody>
                <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
                <tr>
                    <td colspan="4" style="border: 0px;text-align: center;font-size: 20px;">
                        DECLARAÇÕES DO ASSINANTE
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 25px;font-size: 20px;">
                        Declaro, para os devidos fins e efeitos de direito, que (i) o preenchimento e assinatura deste instrumento não significam a aceitação
                        automática de tal solicitação pela CLARO, ficando a análise suspensa até o envio, por e-mail, de cópia dos meus documentos pessoais. (ii)
                        Autorizo a CLARO a proceder à verificação e análise das informações cadastrais, junto aos órgãos restritivos de crédito e instituições
                        assemelhadas; (iii) Estou ciente das condições de plano e aquisição assinaladas neste contrato e das obrigações constantes do Contrato de
                        Permanência. (iv) Sendo hipótese de Portabilidade Numérica, declaro que estou ciente das condições previstas no Contrato SMP e concordo
                        em receber nova (s) linha (s) no caso de meu (s) número (s) portado (s) esteja (m) cancelado (s) ou em qualquer hipótese que
                        inviabilize a conclusão da portabilidade pela Claro. (vi) conforme regra do Plano homologado, a utilização de outro Código de Seleção de
                        Prestadora estará sujeita a cobrança de excedente na conta, segundo as tarifas da prestadora utilizada; (vii) estou ciente de que a
                        renovação total implica na manutenção de todas as linhas adquiridas, inclusive linhas suspensas, até o momento da renovação; (viii) fui
                        informado que a área de cobertura para os serviços 3G, 4G e 4.5G está restrita às localidades informadas no site da Claro:
                        www.claro.com.br; (ix) para transferência de linhas, é obrigatória a assinatura do Termo de Transferência de Titularidade; (x) estou ciente
                        que o plano contratado possui o módulo do Gestor Online e a gestão das linhas e o tráfego de voz, inclusive o direcionamento das ligações
                        de longa distância Nacional , gestão de sms, bem como a gestão de limites do trafego de dados por linha e ou bloqueio de dados são de
                        responsabilidade do Administrador da Conta cadastrado no sistema da Claro, caso não configurado o plano respeitar as regras e limitações
                        descritas no plano homologado na anatel, não permitindo a gestão do administrador da conta.
                    </td>
                </tr>
                <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
                <tr><td colspan="4" style="border: 0px;"><br/></td></tr>
                <tr>
                    <td>
                        <table class="tableInner">
                            <tr>
                                <td colspan="2" style="border: 1px solid #0e0e0e;font-size: 15px;line-height: 10px;width: 10%;text-align: center;"><br/>NÃO</td>
                                <td colspan="2" style="border: 0px;font-size: 15px;line-height: 15px;">
                                    AUTORIZO O FORNECIMENTO PARA TERCEIROS DOS MEUS DADOS CADASTRAIS E/OU PESSOAIS, INCLUSIVE AS INFORMAÇÕES DE CONSUMO E
                                    REGISTRO DE COMPORTAMENTO DE UTILIZAÇÃO/NAVEGAÇÃO, NOS TERMOS DA LEI EM VIGOR.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <table class="table" style="margin-bottom: 0;border: 1px solid #0e0e0e;">
            <tbody>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr><td colspan="2"><br/></td></tr>
            <tr>
                <td colspan="2">
                    <table class="tableInner">
                        <tr>
                            <td colspan="4" style="border: 0px;">Plano Claro Total Individual + BL - Claro Life</td>
                            <td colspan="4" style="border: 0px;">Página 5 de 5</td>
                            <td colspan="4" style="border: 0px;text-align: right">20180102_Claro Total Individual+BL.jan19_v4</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>