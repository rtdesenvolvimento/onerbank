<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-heart"></i> Relatório de aniversáriantes</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="<?php echo base_url() ?>index.php/relatorios/aniversarioCustom" id="formRelatorio" method="get">
                            <div class="row well">
                                <div class="span12">
                                    <label for="">Consultar aniveráriantes do mês de:</label>
                                    <select name="mes" class="form-control">
                                        <option value="1">Janeiro</option>
                                        <option value="2">Fevereiro</option>
                                        <option value="3">Março</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Maio</option>
                                        <option value="6">Junho</option>
                                        <option value="7">Julho</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option>
                                    </select>
                                </div>
                            </div>
                            <div class="span12" style="margin-left: 0; text-align: right;">
                                <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                                <button class="btn btn-primary" id="pdf"><i class="fa fa-file-pdf-o"></i> PDF</button>
                                <button class="btn btn-info" id="excel"><i class="fa fa-file-excel-o"></i> Excel</button>
                                <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-primary span2"><i class="fa fa-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
            <h5>Preview</h5>

        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/aniversarioCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#excel').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/aniversarioCustomExcel');
            $("#formRelatorio").submit();
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/aniversarioCustom');
            $("#formRelatorio").submit();
        });

        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });

    });
</script>