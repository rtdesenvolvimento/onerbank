<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-line-chart"></i> Relatório Financeiro</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row-fluid" style="margin-top: 0" id="div_filtros">
                            <div class="span8">
                                <div class="widget-box">
                                    <div class="widget-content">
                                        <form action="<?php echo base_url()?>index.php/relatorios/financeiroCustom" id="formRelatorio" method="get">
                                            <div class="row alert alert-info">
                                                <a href="#" id="financeiroRapid" style="color: #ffffff">
                                                    CLIQUE AQUI para gerar uma preview do relatório financeiro do mês.
                                                </a>
                                            </div>
                                            <div class="row well">
                                                <div class="col-md-6 col-sm-6 ">
                                                    <label for="">Vencimento de:</label>
                                                    <input type="date" name="dataInicial" class="form-control" />
                                                </div>
                                                <div class="col-md-6 col-sm-6 " style="display: none;">
                                                    <label for="">até:</label>
                                                    <input type="date" name="dataFinal" class="form-control" />
                                                </div>
                                                <div class="col-md-6 col-sm-6 " style="display: none;">
                                                    <label for="">Previsão de Pagamento de:</label>
                                                    <input type="date" name="previsaoPagamentoInicial" class="form-control" />
                                                </div>
                                                <div class="col-md-6 col-sm-6 ">
                                                    <label for="">até:</label>
                                                    <input type="date" name="previsaoPagamentoFinal" class="form-control" />
                                                </div>
                                                <div class="col-md-6 col-sm-6 ">
                                                    <label for="">Tipo:</label>
                                                    <select name="tipo" class="form-control">
                                                        <option value="todos">Todos</option>
                                                        <option value="receita">Receita</option>
                                                        <option value="despesa">Despesa</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-sm-6 ">
                                                    <label for="">Situação:</label>
                                                    <select name="situacao" class="form-control">
                                                        <option value="todos">Todos</option>
                                                        <option value="pago">Pago</option>
                                                        <option value="pendente">Pendente</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>
                                            <div class="span12" style="margin-left: 0; text-align: right;">
                                                <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                                                <button class="btn btn-primary" id="pdf"><i class="fa fa-file-pdf-o"></i> PDF</button>
                                                <button class="btn btn-info" id="excel"><i class="fa fa-file-excel-o"></i> Excel</button>
                                                <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                                            </div>
                                        </form>
                                        &nbsp
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
                            <button id="voltar" class="btn btn-primary span2"><i class="fa fa-backward"></i> Voltar</button><br/>
                            <div class="widget-box">
                                <div class="widget-title">
                                        <span class="icon">
                                            <i class="icon-list-alt"></i>
                                        </span>
                                    <h5>Preview</h5>

                                </div>
                                <div class="widget-content">
                                    <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/financeiroCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#financeiroRapid').click(function (event) {
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/financeiroRapid",
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });

        $('#excel').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/financeiroCustomExcel');
            $("#formRelatorio").submit();
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/financeiroCustom');
            $("#formRelatorio").submit();
        });
    });
</script>