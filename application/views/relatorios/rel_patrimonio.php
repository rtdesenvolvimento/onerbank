<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-line-chart"></i> Relatório de Produtos</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo base_url() ?>index.php/relatorios/produtosCustom" id="formRelatorio" method="get">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <label for="grupoProduto_id">Grupo de produto</label>
                            <select name="grupoProduto_id" class="form-control">
                                <option value="">Selecione um grupo de produto</option>
                                <?php
                                foreach($gruposProduto as $grupoproduto){?>
                                    <option value="<?php echo $grupoproduto->idGrupoProduto;?>"><?php echo $grupoproduto->nome;?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="centro_custo_id">Unidade</label>
                            <select name="centro_custo_id" class="form-control">
                                <option value="">Selecione uma unidade</option>
                                <?php foreach($centroCustos as $centrocusto){ ?>
                                    <option value="<?php echo $centrocusto->idCentrocusto;?>"><?php echo $centrocusto->nome;?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="origemAquisicao">Origem</label>
                            <select name="origemAquisicao" class="form-control">
                                <option value="">Selecione a origem</option>
                                <option value="1">Compra</option>
                                <option value="2">Doação</option>
                                <option value="3">Patrimonio Antigo</option>
                            </select>
                        </div>
                        <div class="col-md-12 col-sm-12" style="padding: 1%; margin-left: 0">
                            <label for="descricao">Descrição</label>
                            <input id="descricao" class="form-control" type="text" name="descricao" value=""/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="span12" style="margin-left: 0; text-align: right;">
                        <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                        <button id="pdf" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> PDF</button>
                        <button id="excel" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Excel</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-inverse span2"><i class="icon-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
            <h5>Preview</h5>

        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/patrimonioCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#excel').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/patrimonioCustomExcel');
            $("#formRelatorio").submit();
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/patrimonioCustom');
            $("#formRelatorio").submit();
        });


        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });
    });
</script>