<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-line-chart"></i> Relatório PGDAS</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                    <form action="<?php echo base_url() ?>index.php/relatorios/produtosCustom" id="formRelatorio" method="get">
                        <div class="row alert alert-info">Deixe em branco caso não deseje utilizar filtros na sua consulta.</div>
                        <div class="row well">
                            <div class="col-md-6 col-sm-6 ">
                                <label for="">Data início:</label>
                                <input type="date" name="dataInicio" class="form-control" />
                            </div>
                            <div class="col-md-6 col-sm-6 ">
                                <label for="">até:</label>
                                <input type="date"  name="dataFinal" class="form-control" />
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="span12" style="margin-left: 0; text-align: right;">
                            <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                            <button class="btn btn-primary" id="pdf"><i class="fa fa-file-pdf-o"></i> PDF</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-primary span2"><i class="fa fa-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon"><i class="icon-list-alt"></i></span>
            <h5>Preview</h5>
        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/pgdasCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/pgdasCustom');
            $("#formRelatorio").submit();
        });


        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });
    });
</script>