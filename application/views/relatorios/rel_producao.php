<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<div class="span12" style="margin-left: 0">
    <form method="get" action="<?php echo base_url(); ?>index.php/relatorios/producao_consulta">

        <div class="span2">
            <select name="status" id="status" class="span12">
                <option value="">Selecione status da O.S</option>
                <?php if ($emitente->ramo_atividade == 2) { ?>
                    <option value="Orçamento" <?php if($status == 'Orçamento') echo 'selected="selected"'; ?>>Orçamento</option>
                    <option value="Garantia" <?php if($status == 'Garantia') echo 'selected="selected"'; ?>>Garantia</option>
                    <option value="Pronto" <?php if($status == 'Pronto') echo 'selected="selected"'; ?>>Pronto</option>
                    <option value="Aguardando Aprovação" <?php if($status == 'Aguardando Aprovação') echo 'selected="selected"'; ?>>Aguardando Aprovação</option>
                    <option value="Aprovado" <?php if($status == 'Aprovado') echo 'selected="selected"'; ?>>Aprovado</option>
                    <option value="S/Conserto" <?php if($status == 'S/Conserto') echo 'selected="selected"'; ?>>S/Conserto</option>
                    <option value="Retirado" <?php if($status == 'Retirado') echo 'selected="selected"'; ?>>Retirado</option>
                    <option value="Recusado" <?php if($status == 'Recusado') echo 'selected="selected"'; ?>>Recusado</option>
                    <option value="Em Bancada" <?php if($status == 'Em Bancada') echo 'selected="selected"'; ?>>Em Bancada</option>
                    <option value="Aguardando peça" <?php if($status == 'Aguardando peça') echo 'selected="selected"'; ?>>Aguardando peça</option>
                    <option value="Faturado" <?php if($status == 'Faturado') echo 'selected="selected"'; ?>>Faturado</option>
                <?php } ?>

                <?php if ($emitente->ramo_atividade == 1) { ?>
                    <option value="Aberto" <?php if($status == 'Aberto') echo 'selected="selected"'; ?>>Aberto</option>
                    <option value="Orçamento" <?php if($status == 'Orçamento') echo 'selected="selected"'; ?>>Orçamento</option>
                    <option value="Garantia" <?php if($status == 'Garantia') echo 'selected="selected"'; ?>>Garantia</option>
                    <option value="Orçamento" <?php if($status == 'Orçamento') echo 'selected="selected"'; ?>>Orçamento</option>
                    <option value="Em Andamento" <?php if($status == 'Em Andamento') echo 'selected="selected"'; ?>>Em Andamento</option>
                    <option value="Em Produção" <?php if($status == 'Em Produção') echo 'selected="selected"'; ?>>Em Produção</option>
                    <option value="Produção Pausada" <?php if($status == 'Produção Pausada') echo 'selected="selected"'; ?>>Produção Pausada</option>
                    <option value="Produzido" <?php if($status == 'Produzido') echo 'selected="selected"'; ?>>Produzido</option>
                    <option value="Faturado" <?php if($status == 'Faturado') echo 'selected="selected"'; ?>>Faturado</option>
                    <option value="Em Andamento" <?php if($status == 'Em Andamento') echo 'selected="selected"'; ?>>Em Andamento</option>
                    <option value="Finalizado" <?php if($status == 'Finalizado') echo 'selected="selected"'; ?>>Finalizado</option>
                    <option value="Cancelado" <?php if($status == 'Cancelado') echo 'selected="selected"'; ?>>Cancelado</option>
                <?php } ?>
            </select>
        </div>

        <div  class="span2">
            <select name="setor" id="setor" onchange="consultaVeiculoCliente();"  required class="span12">
                <option value="">Selecione o setor*</option>
                <?php
                $contador = 0;
                foreach ($setores as $set) {?>
                    <option <?php if($setor == $set->idSetor) echo 'selected="selected"'; ?> value="<?php echo $set->idSetor; ?>"><?php echo $set->nome; ?></option>
                <?php }?>
            </select>
        </div>

        <div  class="span3">
            <select name="funcionario" id="funcionario" class="span12">
                <option value="">Selecione o funcionario</option>
                <?php
                $contador = 0;
                foreach ($usuarios as $usuario) {
                    if ($setor == $usuario->setor_id) { ?>
                        <option  <?php if($funcionario == $usuario->idUsuarios) echo 'selected="selected"'; ?> value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                    <?php }?>
                <?php }?>
            </select>
        </div>

        <div class="span2">
            <input type="text" name="data_inicio"  id="data_inicio"  placeholder="Data Inicial" class="span12 datepicker" value="<?php echo $data_inicio; ?>">
        </div>

        <div class="span2">
            <input type="text" name="data_final"  id="data_final"  placeholder="Data Final" class="span12 datepicker" value="<?php echo $data_final; ?>" >
        </div>

        <div class="span1">
            <button class="span12 btn"> <i class="icon-search"></i> </button>
        </div>
    </form>
</div>

<?php
if (!$produtos  && !$servicos) {
    ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Ordens de Serviço</h5>

        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="width: 5%">Nº O.S</th>
                    <th>Veículo</th>
                    <th>Funcionário</th>
                    <th>Peça</th>
                    <th style="text-align: center">Data/Hora Inicio</th>
                    <th style="text-align: center">Data/Hora Final</th>
                    <th>Valor</th>
                    <th>Comissão</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="8">Nenhuma OS Cadastrada</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>


    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-tags"></i>
         </span>
            <h5>Ordens de Serviço</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-bordered ">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="width: 5%">Nº O.S</th>
                    <th style="text-align: left">Veículo</th>
                    <th style="text-align: left">Funcionário</th>
                    <th style="text-align: left">Peça</th>
                    <th style="text-align: center">Data/Hora Inicio</th>
                    <th style="text-align: center">Data/Hora Final</th>
                    <th style="text-align: center">Tempo</th>
                    <th style="text-align: right">Valor</th>
                    <th style="text-align: right">Comissão</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $contador           = 1;
                $totalSubTotal      = 0;
                $totalComissao      = 0;

                foreach ($produtos as $r) {

                    $data_produzido  = '-';
                    $data_produzido_conclusao  = '-';
                    $tempo = '-';
                    $totalSubTotal   = $totalSubTotal + $r->subTotalPeca;
                    $totalComissao   = $totalComissao + $r->valorComissao;

                    if ($r->data_produzido) {
                        $data_produzido = date('d/m/Y',  strtotime($r->data_produzido)).'<br/>'.$r->hora_produzido;
                    }

                    if ($r->data_produzido_conclusao) {
                        $data_produzido_conclusao = date('d/m/Y',  strtotime($r->data_produzido_conclusao)).'<br/>'.$r->hora_produzido_conclusao;
                    }

                    if ($r->data_produzido && $r->data_produzido_conclusao) {
                        //Calcula o tempo que a crianca usou no visita
                        $entrada    = date('Y-m-d H:i:s', strtotime($r->data_produzido.' '.$r->hora_produzido));
                        $saida      = date('Y-m-d H:i:s', strtotime($r->data_produzido_conclusao.' '.$r->hora_produzido_conclusao));

                        $datatime1 = new DateTime($entrada);
                        $datatime2 = new DateTime($saida);

                        $diff   = date_diff($datatime1, $datatime2);
                        $tempo  = ($diff->i) + ($diff->h * 60) + ($diff->days * 24);
                    }

                    $veiculo_id = $r->veiculo_id;
                    $veiculoStr = '';

                    if ($veiculo_id) {
                        $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                        if (count($veiculo) > 0) {
                            $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                        }
                    }
                    $dados_os = '';

                    switch ($r->status) {
                        case 'Retirado':
                            $cor = '#FF0000';
                            break;
                        case 'Em Bancada':
                            $cor = '#0000FF';
                            break;
                        case 'Orçamento':
                            $cor = '#008B8B';
                            break;
                        case 'Aguardando peça':
                            $cor = '#FF7F00';
                            break;
                        case 'Pronto':
                            $cor = '#00FF00';
                            break;
                        case 'Aguardando Aprovação':
                            $cor = '#FFA500';
                            break;
                        case 'Aprovado':
                            $cor = '#00FA9A';
                            break;
                        case 'Recusado':
                            $cor = '#FFD700';
                            break;
                        case 'Garantia':
                            $cor = '#8B6914';
                            break;
                        case 'S/Conserto':
                            $cor = '#9B30FF';
                            break;
                        case 'Aberto':
                            $cor = '#fbb450';
                            break;
                        case 'Faturado':
                            $cor = '#5bb75b';
                            break;
                        case 'Em Andamento':
                            $cor = '#5bc0de';
                            break;
                        case 'Finalizado':
                            $cor = '#e5e5e5';
                            break;
                        case 'Cancelado':
                            $cor = '#ee5f5b';
                            break;
                        default:
                            $cor = '#E0E4CC';
                            break;
                    }

                    echo '<tr>';
                    echo '<td style="width: 5%;text-align: center;">' . $r->idOs . '</td>';
                    echo '<td>' . $veiculoStr . '</td>';
                    echo '<td>' . $r->nomeResponsavel.'</td>';
                    echo '<td>' . $r->produto . '</td>';
                    echo '<td style="text-align: center">' . $data_produzido . '</td>';
                    echo '<td style="text-align: center">' . $data_produzido_conclusao . '</td>';
                    echo '<td style="text-align: center">' . $tempo . '</td>';
                    echo '<td style="text-align: right">R$ ' . $r->subTotalPeca . '</td>';
                    echo '<td style="text-align: right">R$ ' . $r->valorComissao . '</td>';
                    echo '<td style="text-align: center"><span class="badge" style="background-color: '.$cor.'; border-color: '.$cor.'">'.$r->status.'</span> </td>';

                    echo '<td style="width: 18%;text-align: center;">';
                    // echo '                    <div class="text-center">';
                    // echo '               <div class="btn-group text-left">';
                    // echo '         <button type="button"';
                    //echo ' class="btn btn-default btn-xs btn-primary dropdown-toggle"';
                    // echo '    data-toggle="dropdown"><spa style="color: white;"> Ações </span> <span class="caret"></span></button>';
                    // echo '    <ul class="dropdown-menu pull-right" role="menu">';
                    // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) {
                    // echo '<li>';
                    //   echo '    <a href="'.base_url() . 'index.php/os/visualizar/' . $r->idOs.'"><i class="icon-eye-open"></i>';
                    // echo 'Ver detalhes da O.S';
                    //   echo '    </a>';
                    //  echo '</li>';

                    //  echo '<li>';
                    //   echo '    <a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'"><i class="icon-print"></i>';
                    //  echo 'Imprimir O.S';
                    //   echo '    </a>';
                    //  echo '</li>';
                    //    }
                    // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
                    //   echo '<li>';
                    //   echo '    <a href="'.base_url() . 'index.php/os/editar/' . $r->idOs.'"><i class="icon-pencil icon-white"></i>';
                    //   echo 'Editar O.S';
                    //  echo '    </a>';
                    //  echo '</li>';
                    //  }
                    // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dOs')) {
                    //  echo '<li>';
                    //   echo '    <a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" ><i class="icon-remove icon-white"></i>';
                    //  echo 'Excluir O.S';
                    //  echo '    </a>';
                    //  echo '</li>';
                    // }
                    echo ' </ul>';
                    echo ' </div>';
                    echo '</div>';

                    echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top"><i class="icon-eye-open""></i></a>';
                    echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '<a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                    //echo '<a href="'.base_url() . 'index.php/os/imprimir_os/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                    echo '<a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" style="margin-right: 1%" class="btn btn-danger tip-top" ><i class="icon-remove icon-white"></i>';

                    echo '</td>';
                    echo '</td>';

                    echo '</tr>';

                    $contador = $contador + 1;
                } ?>

                <?php foreach ($servicos as $r) {

                    $data_produzido = '-';
                    $data_produzido_conclusao  = '-';
                    $totalSubTotal   = $totalSubTotal + $r->subTotalPeca;
                    $totalComissao   = $totalComissao + $r->valorComissao;
                    $tempo = '-';

                    if ($r->data_produzido) {
                        $data_produzido = date('d/m/Y',  strtotime($r->data_produzido)).'<br/>'.$r->hora_produzido;
                    }

                    if ($r->data_produzido_conclusao) {
                        $data_produzido_conclusao = date('d/m/Y',  strtotime($r->data_produzido_conclusao)).'<br/>'.$r->hora_produzido_conclusao;
                    }

                    if ($r->data_produzido && $r->data_produzido_conclusao) {
                        //Calcula o tempo que a crianca usou no visita
                        $entrada    = date('Y-m-d H:i:s', strtotime($r->data_produzido.' '.$r->hora_produzido));
                        $saida      = date('Y-m-d H:i:s', strtotime($r->data_produzido_conclusao.' '.$r->hora_produzido_conclusao));

                        $datatime1 = new DateTime($entrada);
                        $datatime2 = new DateTime($saida);

                        $diff   = date_diff($datatime1, $datatime2);
                        $tempo  = ($diff->i) + ($diff->h * 60) + ($diff->days * 24);
                    }

                    $veiculo_id = $r->veiculo;
                    $veiculoStr = '';

                    if ($veiculo_id) {
                        $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                        if (count($veiculo) > 0) {
                            $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                        }
                    }
                    $dados_os = '';

                    switch ($r->status) {
                        case 'Retirado':
                            $cor = '#FF0000';
                            break;
                        case 'Em Bancada':
                            $cor = '#0000FF';
                            break;
                        case 'Orçamento':
                            $cor = '#008B8B';
                            break;
                        case 'Aguardando peça':
                            $cor = '#FF7F00';
                            break;
                        case 'Pronto':
                            $cor = '#00FF00';
                            break;
                        case 'Aguardando Aprovação':
                            $cor = '#FFA500';
                            break;
                        case 'Aprovado':
                            $cor = '#00FA9A';
                            break;
                        case 'Recusado':
                            $cor = '#FFD700';
                            break;
                        case 'Garantia':
                            $cor = '#8B6914';
                            break;
                        case 'S/Conserto':
                            $cor = '#9B30FF';
                            break;
                        case 'Aberto':
                            $cor = '#fbb450';
                            break;
                        case 'Faturado':
                            $cor = '#5bb75b';
                            break;
                        case 'Em Andamento':
                            $cor = '#5bc0de';
                            break;
                        case 'Finalizado':
                            $cor = '#e5e5e5';
                            break;
                        case 'Cancelado':
                            $cor = '#ee5f5b';
                            break;
                        default:
                            $cor = '#E0E4CC';
                            break;
                    }

                    echo '<tr>';
                    echo '<td style="width: 5%;text-align: center;">' . $contador . '</td>';
                    echo '<td>' . $veiculoStr . '</td>';
                    echo '<td>' . $r->nomeResponsavel.'</td>';
                    echo '<td>' . $r->produto . '</td>';
                    echo '<td style="text-align: center">' . $data_produzido . '</td>';
                    echo '<td style="text-align: center">' . $data_produzido_conclusao . '</td>';
                    echo '<td style="text-align: center">' . $tempo . '</td>';
                    echo '<td style="text-align: right">R$ ' . $r->subTotalPeca . '</td>';
                    echo '<td style="text-align: right">R$ ' . $r->valorComissao . '</td>';
                    echo '<td style="text-align: center"><span class="badge" style="background-color: '.$cor.'; border-color: '.$cor.'">'.$r->status.'</span> </td>';

                    echo '<td style="width: 18%;text-align: center;">';
                    // echo '                    <div class="text-center">';
                    // echo '               <div class="btn-group text-left">';
                    // echo '         <button type="button"';
                    //echo ' class="btn btn-default btn-xs btn-primary dropdown-toggle"';
                    // echo '    data-toggle="dropdown"><spa style="color: white;"> Ações </span> <span class="caret"></span></button>';
                    // echo '    <ul class="dropdown-menu pull-right" role="menu">';
                    // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) {
                    // echo '<li>';
                    //   echo '    <a href="'.base_url() . 'index.php/os/visualizar/' . $r->idOs.'"><i class="icon-eye-open"></i>';
                    // echo 'Ver detalhes da O.S';
                    //   echo '    </a>';
                    //  echo '</li>';

                    //  echo '<li>';
                    //   echo '    <a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'"><i class="icon-print"></i>';
                    //  echo 'Imprimir O.S';
                    //   echo '    </a>';
                    //  echo '</li>';
                    //    }
                    // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eOs')) {
                    //   echo '<li>';
                    //   echo '    <a href="'.base_url() . 'index.php/os/editar/' . $r->idOs.'"><i class="icon-pencil icon-white"></i>';
                    //   echo 'Editar O.S';
                    //  echo '    </a>';
                    //  echo '</li>';
                    //  }
                    // if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dOs')) {
                    //  echo '<li>';
                    //   echo '    <a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" ><i class="icon-remove icon-white"></i>';
                    //  echo 'Excluir O.S';
                    //  echo '    </a>';
                    //  echo '</li>';
                    // }
                    echo ' </ul>';
                    echo ' </div>';
                    echo '</div>';

                    echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top" ><i class="icon-eye-open""></i></a>';
                    echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" style="margin-right: 1%" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '<a href="'.base_url() . 'index.php/os/visualizarOSImpressao/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                    //echo '<a href="'.base_url() . 'index.php/os/imprimir_os/' . $r->idOs.'" style="margin-right: 1%" class="btn tip-top"><i class="icon-print"></i>';
                    echo '<a href="#modal-excluir" data-toggle="modal" os="' . $r->idOs . '" style="margin-right: 1%" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i>';

                    echo '</td>';
                    echo '</td>';

                    echo '</tr>';

                    $contador = $contador + 1;
                } ?>
                <tr>
                    <td colspan="7" style="text-align: right">Total de peças</td>
                    <td style="text-align: right"><?php echo 'R$ '.$totalSubTotal; ?></td>
                    <td style="text-align: right"><?php echo 'R$ '.$totalComissao; ?></td>
                    <td colspan="2"> </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>


<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/os/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir OS</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idOs" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir esta OS?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#buscar").keypress(function (event) {
            if (event.which == 13) {
                window.location.href = "<?php echo base_url() ?>index.php/os/buscar/" + $(this).val();
            }
        });

        $(document).on('click', 'a', function (event) {
            var os = $(this).attr('os');
            $('#idOs').val(os);
        });

        $(".datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
    });

    function consultaVeiculoCliente() {
        $("#funcionario").html('');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/usuarios/consultaUsuarioSetor",
            data: "setor=" + $("#setor").val(),
            dataType: 'json',
            success: function (data) {
                var option = '';
                option += '<option value="">Selecione o funcionário</option>'
                $.each(data, function (index, usuario) {
                    option += '<option value="' + usuario.idUsuarios + '">' + usuario.nome + '</option>';
                });
                $("#funcionario").html(option);
            }
        });
    }
</script>