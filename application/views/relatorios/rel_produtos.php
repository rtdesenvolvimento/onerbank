<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-line-chart"></i> Relatório de Produtos</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo base_url() ?>index.php/relatorios/produtosCustom" id="formRelatorio" method="get">
                    <div class="row">
                        <ul class="nav nav-pills" role="tablist">
                            <li role="presentation" class="dropdown">
                                <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    Clique aqui para visualizar relatórios adicionais
                                    <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" id="produtosRapid" href="#">Preview de todos os produtos</a>
                                    <a class="dropdown-item" id="produtosRapidMin" href="#">Preview dos produtos próximos de acabar</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label for="">Preço de Venda de:</label>
                            <input type="text" name="precoInicial" class="form-control money" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="">até:</label>
                            <input type="text"  name="precoFinal" class="form-control money" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label for="">Estoque de:</label>
                            <input type="text" name="estoqueInicial" class="form-control" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="">até:</label>
                            <input type="text" name="estoqueFinal" class="form-control" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="span12" style="margin-left: 0; text-align: right;">
                        <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                        <button id="pdf" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> PDF</button>
                        <button id="excel" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Excel</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-primary span2"><i class="fa fa-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
            <h5>Preview</h5>
        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/produtosCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#excel').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/produtosCustomExcel');
            $("#formRelatorio").submit();
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/produtosCustom');
            $("#formRelatorio").submit();
        });

        $('#produtosRapid').click(function (event) {
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/produtosRapid",
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#produtosRapidMin').click(function (event) {
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/produtosRapidMin",
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });
    });
</script>