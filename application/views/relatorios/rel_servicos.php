<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-line-chart"></i> Relatório de serviços</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo base_url() ?>index.php/relatorios/servicosCustom" id="formRelatorio" method="get">
                    <div class="row alert alert-info">
                        <a href="#" id="servicosRapid" style="color: #ffffff">
                            CLIQUE AQUI para gerar uma preview do relatório com todos os serviços cadastros.
                        </a>
                    </div>
                    <div class="row well">
                        <div class="col-md-6 col-sm-6">
                            <label for="">Preço de:</label>
                            <input type="text" name="precoInicial" class="form-control money" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="">até:</label>
                            <input type="text"  name="precoFinal" class="form-control money" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="span12" style="margin-left: 0; text-align: right;">
                        <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                        <button class="btn btn-primary" id="pdf"><i class="fa fa-file-pdf-o"></i> PDF</button>
                        <button class="btn btn-info" id="excel"><i class="fa fa-file-excel-o"></i> Excel</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-primary span2"><i class="fa fa-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
            <h5>Preview</h5>
        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/servicosCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#excel').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/servicosCustomExcel');
            $("#formRelatorio").submit();
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/servicosCustom');
            $("#formRelatorio").submit();
        });

        $('#servicosRapid').click(function (evento) {
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/servicosRapid",
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });
    });
</script>