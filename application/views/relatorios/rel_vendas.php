<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-pie-chart"></i> Relatório de vendas</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo base_url() ?>index.php/relatorios/vendasCustom" id="formRelatorio" method="get">
                    <div class="row alert alert-info">
                        <a href="#" id="vendasRapid" style="color: #ffffff">
                            CLIQUE AQUI para gerar uma preview do relatório com todas as vendas da retaguarda do sistema.
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label for="">Data de:</label>
                            <input type="date" name="dataInicial" class="form-control" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="">até:</label>
                            <input type="date"  name="dataFinal" class="form-control" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="">Cliente:</label>
                            <input type="text"  id="cliente" class="form-control" />
                            <input type="hidden" name="cliente" id="clienteHide" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="">Vendedor:</label>
                            <input type="text" id="tecnico"   class="form-control" />
                            <input type="hidden" name="responsavel" id="responsavelHide" />
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="span12" style="margin-left: 0; text-align: right;">
                        <button id="preview" class="btn btn-success"><i class="fa fa-eye"></i> Preview</button>
                        <button id="pdf" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> PDF</button>
                        <button id="excel" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Excel</button>
                        <button type="reset" class="btn btn-warning"><i class="fa fa-trash"></i> Limpar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid" style="margin-top: 0;display: none;" id="div_iframe">
    <button id="voltar" class="btn btn-primary span2"><i class="fa fa-backward"></i> Voltar</button><br/>
    <div class="widget-box">
        <div class="widget-title">
                <span class="icon">
                    <i class="icon-list-alt"></i>
                </span>
            <h5>Preview</h5>

        </div>
        <div class="widget-content">
            <iframe src="#" id="iframePreview" frameborder="0" height="1200" width="100%"></iframe>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo base_url();?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#preview').click(function (event) {
            event.preventDefault();
            var dados = $("#formRelatorio").serialize();
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/vendasCustomPreview",
                data: dados,
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#vendasRapid').click(function (event) {
            $.ajax({
                type: "get",
                url: "<?php echo base_url();?>index.php/relatorios/vendasRapid",
                dataType: 'html',
                success: function (data) {
                    $('#iframePreview').attr('src', '<?php echo base_url();?>'+data);
                    $('#div_iframe').show();
                    $('#div_filtros').hide();
                }
            });
        });

        $('#voltar').click(function (event) {
            $('#iframePreview').attr('src', '');
            $('#div_iframe').hide();
            $('#div_filtros').show();
        });

        $("#cliente").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteCliente",
            minLength: 2,
            select: function( event, ui ) {
                 $("#clienteHide").val(ui.item.id);
            }
      });

      $("#tecnico").autocomplete({
            source: "<?php echo base_url(); ?>index.php/os/autoCompleteUsuario",
            minLength: 2,
            select: function( event, ui ) {
                 $("#responsavelHide").val(ui.item.id);
            }
      });

        $('#excel').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/vendasCustomExcel');
            $("#formRelatorio").submit();
        });

        $('#pdf').click(function (event) {
            $("#formRelatorio").attr('action', '<?php echo base_url()?>index.php/relatorios/vendasCustom');
            $("#formRelatorio").submit();
        });

    });
</script>