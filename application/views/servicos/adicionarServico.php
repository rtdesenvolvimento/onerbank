<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Serviço<small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formServico" method="post" class="form-horizontal">
                    <div class="col-md-2 col-sm-2">
                        <label for="codigoServico" class="control-label">Código de Serviço</label>
                        <div class="controls">
                            <input id="codigoServico" class="form-control" type="text" name="codigoServico" value="<?php echo set_value('codigoServico'); ?>"  />
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" class="form-control" required="required" name="nome" value="<?php echo set_value('nome'); ?>"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <label for="preco" class="control-label"><span class="required">Preço</span></label>
                        <div class="controls">
                            <input id="preco" class="form-control money" type="text" name="preco"
                                   value="<?php echo set_value('preco'); ?>"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <label for="comissao" class="control-label">Comissão</label>
                        <div class="controls">
                            <input id="comissao" class="form-control money" type="text" name="comissao" value="<?php echo set_value('comissao'); ?>"  />
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <label for="isISS" class="control-label">Configurar ISS</label>
                        <div class="controls">
                            <input id="isISS" type="checkbox" name="isISS"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <label for="aliquotaISS" class="control-label">Alíquota ISS (%)</label>
                        <div class="controls">
                            <input id="aliquotaISS" class="form-control money" type="text" name="aliquotaISS" value="<?php echo set_value('aliquotaISS'); ?>"  />
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <label for="cnae" class="control-label">CNAE</label>
                        <div class="controls">
                            <input id="cnae" type="text" class="form-control" name="cnae" value="<?php echo set_value('cnae'); ?>"  />
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <label for="descricao" class="control-label">Descrição</label>
                        <div class="controls">
                            <textarea id="descricao" class="form-control" type="text" name="descricao"><?php echo set_value('descricao'); ?></textarea>
                        </div>
                    </div>
                    <div class="form-actions" style="text-align: right;">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                    Adicionar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/servicos"
                                   id="btnAdicionar" class="btn btn-primary"><i
                                            class="fa fa-backward"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formServico').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>




                                    
