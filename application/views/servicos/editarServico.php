<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                      <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Serviço</h5>
            </div>

            <div class="widget-content nopadding">

                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes do Serviço</a></li>
                    <li id="tabFornecedor"><a href="#tab2" data-toggle="tab">Fornecedores</a></li>
                </ul>

                <div class="tab-content">
                    <?php echo $custom_error; ?>

                    <!-- DETALHES DOD PRODUTO !-->
                    <div class="tab-pane active" id="tab1">
                        <form action="<?php echo current_url(); ?>" id="formServico" method="post" class="form-horizontal">
                        <?php echo form_hidden('idServicos', $result->idServicos) ?>

                        <div class="control-group">
                            <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                            <div class="controls">
                                <input id="nome" type="text" name="nome" value="<?php echo $result->nome ?>"/>
                            </div>
                        </div>

                        <div class="control-group" style="display: none;">
                            <label for="grupoProduto_id" class="control-label">Grupo de produto</label>
                            <div class="controls">
                                <select name="grupoProduto_id" class="form-control">
                                    <option value="">Selecione um grupo de produto</option>
                                    <?php
                                    foreach($gruposProduto as $grupoproduto){
                                        $sel = '';
                                        if ($grupoproduto->idGrupoProduto == $result->grupoProduto_id){
                                            $sel = 'selected="selected"';
                                        }
                                        echo '<option value="'.$grupoproduto->idGrupoProduto.'" '.$sel.'>'.$grupoproduto->nome.'</option>';
                                    }?>
                                </select>
                            </div>
                        </div>

                        <div class="control-group" style="display: none;">
                            <label for="setor_id" class="control-label">Setor</label>
                            <div class="controls">
                                <select name="setor_id" class="form-control">
                                    <option value="">Selecione um setor</option>
                                    <?php
                                    foreach($setores as $setor){
                                        $sel = 'selected="selected"';
                                        if ($grupoproduto->idSetor == $result->setor_id){
                                            $sel = 'selected="selected"';
                                        }
                                        echo '<option value="'.$setor->idSetor.'" '.$sel.'>'.$setor->nome.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="preco" class="control-label"><span class="required">Preço</span></label>
                            <div class="controls">
                                <input id="preco" class="money" type="text" name="preco"
                                       value="<?php echo $result->preco ?>"/>
                            </div>
                        </div>

                        <div class="control-group" style="display: none;">
                            <label for="comissao" class="control-label">Comissão</label>
                            <div class="controls">
                                <input id="comissao" class="money" type="text" name="comissao" value="<?php echo $result->comissao; ?>"  /> %
                            </div>
                        </div>


                        <div class="control-group">
                            <label for="isISS" class="control-label">Configurar ISS</label>
                            <div class="controls">
                                <input id="inativo" <?php if($result->isISS == 1) echo  'checked="checked"';?> type="checkbox" name="isISS"/>
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="codigoServico" class="control-label">Código de Serviço<span class="required">*</span></label>
                            <div class="controls">
                                <input id="codigoServico" type="text" name="codigoServico" value="<?php echo $result->codigoServico; ?>"  />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="aliquotaISS" class="control-label">Alíquota ISS (%)</label>
                            <div class="controls">
                                <input id="aliquotaISS" class="money" type="text" name="aliquotaISS" value="<?php echo $result->aliquotaISS; ?>"  /> %
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="codigoServico" class="control-label">CNAE<span class="required">*</span></label>
                            <div class="controls">
                                <input id="cnae" type="text" name="cnae" value="<?php echo $result->cnae; ?>"  />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="descricao" class="control-label">Descrição</label>
                            <div class="controls">
                                <input id="descricao" type="text" name="descricao"
                                       value="<?php echo $result->descricao ?>"/>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="span12">
                                <div class="span6 offset3">
                                    <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                    </button>
                                    <a href="<?php echo base_url() ?>index.php/servicos" id="btnAdicionar" class="btn"><i
                                                class="icon-arrow-left"></i> Voltar</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>

                    <!--FORNECEDOR-->
                    <div class="tab-pane" id="tab2">
                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                            <form id="formFornecedorProduto"
                                  action="<?php echo base_url() ?>index.php/servicos/adicionarFornecedorServico" method="post">

                                <div class="span12" style="padding: 1%; margin-left: 0">

                                    <div class="span8">
                                        <label for="">Fornecedor</label>
                                        <input type="text" name="idFornecedor" id="idFornecedor"
                                               placeholder="Digite o código do serviço para o fornecedor"/>
                                        <input type="hidden" name="idServico" id="idServico" value="<?php echo $result->idServicos; ?>">
                                        <input type="hidden" name="idFornecedorservico" id="idFornecedorservico" value="" />
                                    </div>

                                    <div class="span4">
                                        <label for="codigo">Código</label>
                                        <input type="text" class="span12" name="codigo" id="codigo"
                                               placeholder="Digite o código do serviço para o fornecedor"/>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span12">
                                        <label for="observacao">Observação</label>
                                        <textarea class="span12"  id="observacao" name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span6 offset3" style="text-align: center">
                                        <button class="btn btn-success" id="btnAdicionarProduto"><i
                                                    class="icon-white icon-plus"></i> Salvar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="span12" id="divFornecedor" style="margin-left: 0">
                            <table class="table table-bordered" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th style="text-align: left;">Fornecedor</th>
                                    <th style="text-align: left;">Código</th>
                                    <th style="text-align: left;">Observação</th>
                                    <th style="text-align: center;width:10%;">Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($fornecedoresservico as $fornecedores) {
                                    echo '<tr>';
                                    echo '<td style="text-align: left;">' . $fornecedores->nomeFornecedor . '</td>';
                                    echo '<td style="text-align: left;">' . $fornecedores->codigo . '</td>';
                                    echo '<td style="text-align: left;">' . $fornecedores->observacao . '</td>';
                                    echo '<td style="text-align: center;width:10%;">
                                            <span idAcao="' . $fornecedores->idFornecedorservico . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></span>
                                            <span codigo="' . $fornecedores->codigo . '" 
                                                  observacao="' . $fornecedores->observacao . '"
                                                  idAcaoEditar="' . $fornecedores->idFornecedorservico . '"
                                                  idFornecedor="' . $fornecedores->idFornecedor . '"
                                                  idProduto="' . $fornecedores->idServico . '"
                                             class="btn btn-info"><i class="icon-pencil icon-white"></i></span>

                                          </td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formServico').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });

        $('#idFornecedor').val('').select2({
            minimumInputLength: 0,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedorById/"+ $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: "<?php echo base_url(); ?>index.php/produtos/autoCompleteFornecedorFilterSelect",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            console.log("change");
        }).on('select', function (e) {
            console.log("select");
        });


        $("#formFornecedorProduto").validate({
            rules: {
                idFornecedor: {required: true}
            },
            messages: {
                idFornecedor: {required: 'Insira um fornecedor'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $("#divFornecedor").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/servicos/salvarFornecedorServico",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        debugger;
                        if (data.result === true) {
                            $("#divFornecedor").load("<?php echo current_url();?> #divFornecedor");

                            $('#idFornecedorservico').val('');
                            $("#idFornecedor").val('');
                            $("#observacao").val('');
                            $("#codigo").val('');

                            $("#idFornecedor").select2('data', {
                                id: null,
                                text: ''
                            });
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar o fornecedor.');
                        }
                    }
                });
                return false;
            }

        });
        $(document).on('click', 'span', function (event) {

            var idAcao       = $(this).attr('idAcao');
            var idAcaoEditar    = $(this).attr('idAcaoEditar');

            if ((idAcao % 1) == 0) {
                $("#divFornecedor").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/servicos/excluirFornecedorServico",
                    data: "idFornecedorservico=" + idAcao,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divFornecedor").load("<?php echo current_url();?> #divFornecedor");

                            $('#idFornecedorservico').val('');
                            $("#idFornecedor").val('');
                            $("#observacao").val('');
                            $("#codigo").val('');

                            $("#idFornecedor").select2('data', {
                                id: null,
                                text: ''
                            });
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir o fornecedor.');
                        }
                    }
                });
                return false;
            } else if (idAcaoEditar != '') {

                var codigo          = $(this).attr('codigo');
                var observacao      = $(this).attr('observacao');
                var idFornecedor    = $(this).attr('idFornecedor');

                $('#idFornecedorservico').val(idAcaoEditar);

                $("#codigo").val(codigo);
                $("#observacao").val(observacao);
                $('#idFornecedor').val(idFornecedor).trigger('change');
            }
        });


    });
</script>



