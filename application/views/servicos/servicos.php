<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sitemap"></i> Serviços (<?php echo count($results); ?>)</small></h2>
                <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aServico')) { ?>
                    <ul class="nav navbar-right panel_toolbox">
                        <a class="btn btn-success" href="<?php echo base_url(); ?>servicos/adicionar"><i class="fa fa-plus"></i> Cadastrar Serviço</a>
                    </ul>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>Nome</th>
                                    <th style="text-align: right;">Preço</th>
                                    <th>Descrição</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    echo '<tr>';
                                    echo '<td>' . $r->nome . '</td>';
                                    echo '<td style="text-align: right">R$ ' . number_format($r->preco, 2, ',', '.') . '</td>';
                                    echo '<td>' . $r->descricao . '</td>';
                                    echo '<td style="text-align: center;">';

                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'eServico')) {
                                        echo '<a style="margin-right: 1%" href="' . base_url() . 'index.php/servicos/editar/' . $r->idServicos . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }

                                    if ($this->permission->checkPermission($this->session->userdata('permissao'), 'dServico')) {
                                        echo '<a href="#modal-excluir" role="button" data-toggle="modal" servico="' . $r->idServicos . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>  ';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/servicos/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Serviço</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idServico" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este serviço?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>




<script type="text/javascript">
    $(document).ready(function () {

        $("#buscar").keypress(function (event) {
            if (event.which == 13) {
                window.location.href = "<?php echo base_url() ?>index.php/servicos/buscar/" + $(this).val();
            }
        });

        $(document).on('click', 'a', function (event) {

            var servico = $(this).attr('servico');
            $('#idServico').val(servico);

        });

    });

</script>