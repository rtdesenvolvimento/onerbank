<a href="<?php echo base_url() ?>index.php/setor/adicionar" class="btn btn-success">
    <i class="icon-plus icon-white"></i> Adicionar Setor
</a>
<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Setores</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhum Setor Cadastrado</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Setores</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {
                    echo '<tr>';
                    echo '    <td>' . $r->nome . '</td>';
                    echo '    <td>' . $r->descricao . '</td>';
                    echo '    <td>';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/setor/editar/' . $r->idSetor . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '      <a href="#modal-excluir" role="button" data-toggle="modal" setor="' . $r->idSetor . '" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>  ';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/setor/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Setor</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idSetor" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este setor?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var setor = $(this).attr('setor');
            $('#idSetor').val(setor);
        });
    });
</script>