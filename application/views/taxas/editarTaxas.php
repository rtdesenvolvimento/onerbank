<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Taxas<small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formTaxas" method="post" class="form-horizontal">
                    <?php echo form_hidden('idTaxas', $result->idTaxas) ?>
                    <div class="control-group">
                        <label for="nome" class="control-label">Tarifa fixa por transação em reais</label>
                        <div class="controls">
                            <input id="tarifaFixa" type="number" class="form-control" name="tarifaFixa" value="<?php echo $result->tarifaFixa ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="nome" class="control-label">Taxa de intermediação em %</label>
                        <div class="controls">
                            <input id="taxaIntermediacao" type="number" class="form-control" name="taxaIntermediacao" value="<?php echo $result->taxaIntermediacao ?>"/>
                        </div>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#taxasCliente" data-toggle="tab">Taxas cobrado do cliente</a></li>
                            <li><a href="#taxasVendedor" data-toggle="tab" style="display: none;">Taxa cobrado do vendedor</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="taxasCliente">

                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 1X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamento1x" class="form-control" type="number" name="taxaParcelamento1x" value="<?php echo $result->taxaParcelamento1x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 2X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamento2x" class="form-control" type="number" name="taxaParcelamento2x" value="<?php echo $result->taxaParcelamento2x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 3X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamento3x" class="form-control" type="number" name="taxaParcelamento3x" value="<?php echo $result->taxaParcelamento3x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 4X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamento4x" class="form-control" type="number" name="taxaParcelamento4x" value="<?php echo $result->taxaParcelamento4x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 5X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamento5x" class="form-control" type="number" name="taxaParcelamento5x" value="<?php echo $result->taxaParcelamento5x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 6X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamento6x" class="form-control" type="number" name="taxaParcelamento6x" value="<?php echo $result->taxaParcelamento6x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 7X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamento7x" class="form-control" type="number" name="taxaParcelamento7x" value="<?php echo $result->taxaParcelamento7x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 8X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamento8x" class="form-control" type="number" name="taxaParcelamento8x" value="<?php echo $result->taxaParcelamento8x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 9X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamento9x" class="form-control" type="number" name="taxaParcelamento9x" value="<?php echo $result->taxaParcelamento9x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 10X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamento10x" class="form-control" type="number" name="taxaParcelamento10x" value="<?php echo $result->taxaParcelamento10x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 11X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamento11x" class="form-control" type="number" name="taxaParcelamento11x" value="<?php echo $result->taxaParcelamento11x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 12X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamento12x"class="form-control"  type="number" name="taxaParcelamento12x" value="<?php echo $result->taxaParcelamento12x ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="taxasVendedor">
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para aqui 1X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor1x" type="number" name="taxaParcelamentoVendedor1x" value="<?php echo $result->taxaParcelamentoVendedor1x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 2X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor2x" type="number" name="taxaParcelamentoVendedor2x" value="<?php echo $result->taxaParcelamentoVendedor2x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 3X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor3x" type="number" name="taxaParcelamentoVendedor3x" value="<?php echo $result->taxaParcelamentoVendedor3x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 4X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor4x" type="number" name="taxaParcelamentoVendedor4x" value="<?php echo $result->taxaParcelamentoVendedor4x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 5X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor5x" type="number" name="taxaParcelamentoVendedor5x" value="<?php echo $result->taxaParcelamentoVendedor5x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 6X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor6x" type="number" name="taxaParcelamentoVendedor6x" value="<?php echo $result->taxaParcelamentoVendedor6x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 7X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor7x" type="number" name="taxaParcelamentoVendedor7x" value="<?php echo $result->taxaParcelamentoVendedor7x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 8X </label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor8x" type="number" name="taxaParcelamentoVendedor8x" value="<?php echo $result->taxaParcelamentoVendedor8x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 9X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor9x" type="number" name="taxaParcelamentoVendedor9x" value="<?php echo $result->taxaParcelamentoVendedor9x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 10X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor10x" type="number" name="taxaParcelamentoVendedor10x" value="<?php echo $result->taxaParcelamentoVendedor10x ?>"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 11X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor11x" type="number" name="taxaParcelamentoVendedor11x" value="<?php echo $result->taxaParcelamentoVendedor11x ?>"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="nome" class="control-label">Taxa de parcelamento para 12X</label>
                                    <div class="controls">
                                        <input id="taxaParcelamentoVendedor12x" type="number" name="taxaParcelamentoVendedor12x" value="<?php echo $result->taxaParcelamentoVendedor12x ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/taxas" id="btnAdicionar" class="btn"><i
                                        class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formTaxas').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>