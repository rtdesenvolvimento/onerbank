<!DOCTYPE html>
<html lang="en">
<head>
    <title>.:<?php echo TITLE_SISTEMA;?>:.</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    !-->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mensagem.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-media.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabela_responsiva.css"/>
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>

    <script>
        var BASE_URL            = '<?php echo base_url();?>';
        var USUARIO_LOGADO_ID   = <?php echo  $this->session->userdata('id');?>;
        var CURRENT_URL         = '<?php echo current_url();?>'
    </script>

</head>
<body style="margin: 0;margin-top: -17px;background: #eee;">

<?php if (isset($view)) {
    echo $this->load->view($view);
} ?>

<!--end-Footer-part-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/matrix.js"></script>

</body>
</html>