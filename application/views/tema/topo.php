<!DOCTYPE html>
<html lang="en">
<head>
    <title>.:<?php echo TITLE_SISTEMA;?>:.</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    !-->


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mensagem.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-style.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo PACOTE;?>/matrix-media.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabela_responsiva.css"/>
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css"/>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>

    <script>
        var BASE_URL            = '<?php echo base_url();?>';
        var USUARIO_LOGADO_ID   = <?php echo  $this->session->userdata('id');?>;
        var CURRENT_URL         = '<?php echo current_url();?>'
    </script>

</head>
<body>

<!--Header-part-->
<div id="header">
    <h1><a href="">.:<?php echo NOME_SISTEMA;?>:.</a></h1>
</div>
<!--close-Header-part-->

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">

        <?php
         $filial = $this->db->get_where('filial', array('idFilial'=>$this->session->userdata('filial_id')))->row();
         $emitente = $this->db->get_where('emitente', array('filial_id'=>$this->session->userdata('filial_id')))->row();
        ?>

        <li class=""><a href="<?php echo base_url(); ?>index.php/mapos/emitente"><i
                        class="icon icon-building"></i> <span class="text"><?php echo $filial->nome.' CNPJ: '.$emitente->cnpj.'    IE: '.$emitente->ie;?></span></a></li>
        <li class=""><a href="<?php echo base_url(); ?>index.php/mapos/minhaConta"><i
                        class="icon icon-star"></i> <span class="text">Minha Conta</span></a></li>
        <li class=""><a href="<?php echo base_url(); ?>index.php/mapos/sair"><i
                        class="icon icon-share-alt"></i> <span class="text">Sair do Sistema</span></a></li>
    </ul>
</div>

<!--start-top-serch-->
<div id="search">

    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
        <form action="<?php echo base_url() ?>index.php/mapos/pesquisar">
            <input type="text" required="required" name="termo" placeholder="Pesquisar..."/>
            <button type="submit" class="tip-bottom"><i class="icon-search icon-white"></i></button>
        </form>
    <?php  } ?>
</div>
<!--close-top-serch-->

<!--sidebar-menu-->

<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-list"></i> Menu</a>
    <ul>

        <?php
        $setor_id = $this->session->userdata('setor_id');

        if (!$setor_id) {
            $setor_id = null;
        }
        ?>

        <?php if ($setor_id == null) { ?>
            <li class="<?php if (isset($menuPainel)) {
                echo 'active';
            }; ?>"><a href="<?php echo base_url() ?>"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
        <?php }?>

        <?php if ($setor_id == null) { ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'aPDV')) { ?>
                <li><a href="<?php echo base_url() ?>index.php/pdv" target="_blank"><i class="icon icon-globe"></i> <span>PDV</span></a></li>
            <?php } ?>
        <?php }?>


        <?php if ($setor_id == null &&
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vServico') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vArquivo')
        ) { ?>
            <li class="submenu <?php if (isset($menuCadastro)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-edit"></i> <span>Cadastro</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) { ?>
                        <li class="<?php if (isset($menuClientes)) { echo 'active';}; ?>"><a href="<?php echo base_url() ?>index.php/clientes"><i class="icon icon-group"></i>
                                <span>Clientes</span></a></li>
                        <li class="<?php if (isset($menuFornecedor)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/fornecedor"><i class="icon icon-user-md"></i>
                                <span>Fornecedores</span></a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vServico')) { ?>
                        <li class="<?php if (isset($menuServicos)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/servicos"><i class="icon icon-wrench"></i>
                                <span>Serviços</span></a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vServico')) { ?>
                        <li class="<?php if (isset($menuGrupoProduto)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/grupoproduto"><i class="icon icon-wrench"></i>
                                <span>Grupo Produtos</span></a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vArquivo')) { ?>
                        <li class="<?php if (isset($menuArquivos)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/arquivos"><i class="icon icon-hdd"></i>
                                <span>Arquivos</span></a></li>
                    <?php } ?>

                </ul>
            </li>
        <?php } ?>

        <?php if ($setor_id == null) { ?>
            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vVistoria')) { ?>
                <li class="<?php if (isset($menuVistoria)) {
                    echo 'active';
                }; ?>"><a href="<?php echo base_url() ?>index.php/vistoria"><i class="icon icon-list"></i>
                        <span>Checklist Veícular</span></a></li>
            <?php }?>
        <?php } ?>

        <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
            <li class="<?php if (isset($menuOs)) {
                echo 'active';
            }; ?>"><a href="<?php echo base_url() ?>index.php/os"><i class="icon icon-tags"></i>
                    <span>Ordens de Serviço</span></a></li>
        <?php } ?>

        <?php if ($setor_id != null) { ?>
            <li class="<?php if (isset($menuOsProducao)) {echo 'active';}; ?>">
                <a href="<?php echo base_url() ?>index.php/os/osProducao"><i class="icon icon-tags"></i>
                    <span>Veículos Produção</span>
                </a>
            </li>

            <?php
                $nome_setor = '';
                if ($setor_id) {
                    $setor = $this->db->get_where('setor' , array('idSetor' => $setor_id ))->row();
                    if (count($setor)) {
                        $nome_setor = $setor->nome;
                    }
                }
            ?>
            <li class="<?php if (isset($menuOsProducaoSetor)) {echo 'active';}; ?>">
                <a href="<?php echo base_url() ?>index.php/os/osSetor"><i class="icon icon-tags"></i>
                    <span>Produção <?php echo $nome_setor; ?></span>
                </a>
            </li>
        <?php } ?>


        <?php if ($setor_id == null &&
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) { ?>
            <li class="submenu <?php if (isset($menuCompra)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-exchange"></i> <span>Compras</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) { ?>
                        <li class="<?php if (isset($menuPedido)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/pedido"><i class="icon icon-adjust"></i> <span>Lista de Compras</span></a>
                        </li>
                    <?php } ?>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) { ?>
                        <li class="<?php if (isset($menuPedido)) {
                            echo '';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/pedido/adicionar"><i class="icon icon-adjust"></i> <span>Adicionar Compras</span></a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($setor_id == null &&
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vVenda')) { ?>
            <li class="submenu <?php if (isset($menuOperacao)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-shopping-cart"></i> <span>Vendas</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vVenda')) { ?>
                        <li class="<?php if (isset($menuVendas)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/vendas"><i class="icon icon-shopping-cart"></i> <span>Vendas</span></a>
                        </li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vVenda')) { ?>
                        <li class="<?php if (isset($menuVendasPDV)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/pdv/vendas"><i class="icon icon-shopping-cart"></i> <span>Vendas PDV</span></a>
                        </li>
                    <?php } ?>

                </ul>
            </li>
        <?php } ?>

        <?php if ($setor_id == null &&
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vTransferencia') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vInventario') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
            <li class="submenu <?php if (isset($menuEstoque)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-align-justify"></i> <span>Estoque</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuProdutos)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/produtos"><i class="icon icon-barcode"></i>
                                <span>Produtos</span></a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuPatrimonio)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/patrimonio"><i class="icon icon-pushpin"></i>
                                <span>Patrimônio</span></a></li>
                    <?php } ?>


                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuVeiculo)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/veiculos"><i class="icon icon-ambulance"></i>
                                <span>Veículos</span></a></li>
                    <?php } ?>


                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vTransferencia')) { ?>
                        <li class="<?php if (isset($menuTransferencia)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/transferencia"><i class="icon icon-backward"></i> <span>Transferências</span></a>
                        </li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vInventario')) { ?>
                        <li class="<?php if (isset($menuInventario)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/inventario"><i class="icon icon-plus"></i> <span>Inventário de estoque</span></a>
                        </li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuConsultaEstoque)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/consultaestoque/consultaEstoque"><i class="icon icon-search"></i>
                                <span>Consulta Estoque</span></a></li>
                    <?php } ?>

                </ul>
            </li>
        <?php } ?>


        <?php if ($setor_id == null && $this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
            <li class="submenu <?php if (isset($menuManutencao)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-refresh"></i> <span>Manutenção</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuProgramacaoManutencao)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/programacaomanutencaopreventiva"><i class="icon icon-barcode"></i>
                                <span>Preventiva Patrimonial</span></a></li>
                    <?php } ?>


                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuProgramacaoManutencaoVeiculo)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/programacaomanutencaopreventivaveiculo"><i class="icon icon-barcode"></i>
                                <span>Preventiva Veícular</span></a></li>
                    <?php } ?>

                </ul>
            </li>
        <?php } ?>


        <?php if ( $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfse')  ) { ?>
            <li class="submenu <?php if (isset($menuNFE)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-align-justify"></i> <span>Nota Fiscal</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vNfse')) { ?>
                        <li class="<?php if (isset($menuNFENFSE)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/nfse"><i class="icon icon-barcode"></i>
                                <span>NFS-e</span></a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe')) { ?>
                        <li class="<?php if (isset($menuNFENFE)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/nfe"><i class="icon icon-barcode"></i>
                                <span>NF-e</span></a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                        <li class="<?php if (isset($menuNFCE)) {
                            echo 'active';
                        }; ?>">

                            <a href="<?php echo base_url() ?>index.php/nfce"><i class="icon icon-barcode"></i>
                                <span>NFC-e</span></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ( $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe') ) { ?>
            <li class="submenu <?php if (isset($menuColetorXML)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-download"></i> <span>Coletor XML</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe')) { ?>
                        <li class="<?php if (isset($menuColetorXML)) {
                            echo 'active';
                        }; ?>"><a href="<?php echo base_url() ?>index.php/coletorxml"><i class="icon icon-barcode"></i>
                                <span>Coletor XML</span></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vLancamento')) { ?>
            <li class="submenu <?php if (isset($menuFinanceiro)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-money"></i> <span>Financeiro</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>
                    <li><a href="<?php echo base_url() ?>index.php/financeiro/lancamentos">Lançamentos</a></li>
                </ul>
            </li>
        <?php } ?>

        <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'rServico') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'rOs') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro') ||
            $this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>

            <li class="submenu <?php if (isset($menuRelatorios)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-list-alt"></i> <span>Relatórios</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/clientes">Clientes</a></li>
                    <?php } ?>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/produtos">Produtos</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/patrimonio">Patrimônio</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/servicos">Serviços</a></li>
                    <?php } ?>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/os">Ordens de Serviço</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/producao">Produção</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/vendas">Vendas</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/vendas_pdv">Vendas no PDV</a></li>
                    <?php } ?>


                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/pgdas">PGDAS</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/financeiro">Financeiro</a></li>
                    <?php } ?>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/relatorios/aniversario">Aniversário</a></li>
                    <?php } ?>

                </ul>
            </li>

        <?php } ?>

        <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cUsuario')
            || $this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')
            || $this->permission->checkPermission($this->session->userdata('permissao'), 'cPermissao')
            || $this->permission->checkPermission($this->session->userdata('permissao'), 'cBackup')) { ?>
            <li class="submenu <?php if (isset($menuConfiguracoes)) {
                echo 'active open';
            }; ?>">
                <a href="#"><i class="icon icon-cog"></i> <span>Configurações</span> <span class="label"><i
                                class="icon-chevron-down"></i></span></a>
                <ul>

                    <li class="<?php if (isset($menuTipoveiculo)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/configuracao/editar/1">Configuração</a>
                    </li>

                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cUsuario')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/usuarios">Usuários</a></li>
                    <?php } ?>

                    <li class="<?php if (isset($menuSetor)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/setor"><span>Setores</span></a>
                    </li>

                    <li class="<?php if (isset($menuVisaoautomovel)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/visaoautomovel">Visão do automóvel</a>
                    </li>

                    <li class="<?php if (isset($menuEquipamentoveiculo)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/equipamentoveiculo">Equipamento automóvel</a>
                    </li>

                    <li class="<?php if (isset($menuTipoveiculopneu)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/tipoveiculopneu">Tipos pneu veículo</a>
                    </li>

                    <li class="<?php if (isset($menuTipoavaria)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/tipoavaria">Tipos de Avaria</a>
                    </li>

                    <li class="<?php if (isset($menuAcessorio)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/acessorio">Acessórios</a>
                    </li>

                    <li class="<?php if (isset($menuTipoveiculo)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/tipoveiculo">Tipos de Veículo</a>
                    </li>

                    <li class="<?php if (isset($menuNaturezaOperacao)) {  echo 'active'; }; ?>">
                        <a href="<?php echo base_url() ?>index.php/naturezaoperacao">Natureza de operação</a>
                    </li>


                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/mapos/emitente">Emitente</a></li>
                    <?php } ?>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cPermissao')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/permissoes">Permissões</a></li>
                    <?php } ?>
                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cBackup')) { ?>
                        <li><a href="<?php echo base_url() ?>index.php/mapos/backup">Backup</a></li>
                    <?php } ?>

                </ul>
            </li>
        <?php } ?>

        <?php if ($setor_id == null) { ?>

            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
            <li class="<?php if (isset($menuPainel)) {
                echo 'active';
            }; ?>"><a href="<?php echo base_url() ?>index.php/mapos/pesquisar?termo=all"><i class="icon-search icon-white"></i> <span>Consultas</span></a></li>
            <?php  }?>

            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vOnnerApp')) { ?>
                <li class="<?php if (isset($menuPainel)) {
                echo 'active';
                }; ?>"><a href="<?php echo base_url() ?>index.php/mapos/onerApp"><i class="icon-white icon-screenshot"></i> <span>OnerApp</span></a></li>
            <?php  }?>

        <?php } ?>
    </ul>
</div>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="<?php echo base_url() ?>" class="tip-bottom"><i
                        class="icon-home"></i> Dashboard</a> <?php if ($this->uri->segment(1) != null) { ?><a
                href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) ?>" class="tip-bottom" ><?php echo ucfirst($this->uri->segment(1)); ?></a> <?php if ($this->uri->segment(2) != null) { ?><a href="<?php echo base_url() . 'index.php/' . $this->uri->segment(1) . '/' . $this->uri->segment(2) ?>" class="current tip-bottom"><?php echo ucfirst($this->uri->segment(2));
                } ?></a> <?php } ?></div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">

                <?php if ($this->session->flashdata('error') != null) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('success') != null) { ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div id="flashMessage" class="success-msg">
                    <b>Sucesso!</b><br/>
                    <span id="dSuccess-msg"></span>
                </div>

                <div id="flashMessage" class="error-msg">
                    <b>Error!</b><br/>
                    <span id="dError-msg"></span>
                </div>

                <div id="flashMessage" class="warning-msg">
                    <b>Alerta!</b><br/>
                    <span id="dWarning-msg"></span>
                </div>

                <div id="flashMessage" class="info-msg">
                    <b>Informação!</b><br/>
                    <span id="dInfo-msg"></span>
                </div>

                <?php if (isset($view)) {
                    echo $this->load->view($view);
                } ?>
            </div>
        </div>
    </div>
</div>
<!--Footer-part-->
<div class="row-fluid">
    <div id="footer" class="span12" style="color: #ffffff;"> <?php echo date('Y'); ?> &copy; <a href="<?php echo SITE_EMPRESA;?>" style="color: #ffffff;"> <?php echo NOME_SISTEMA;?> (<?php echo VERSAO;?>)</a></div>
</div>
<!--end-Footer-part-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/matrix.js"></script>

<script>
    $('#flashMessage').dblclick(function () {
       $(this).hide();
    });

    /*
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/financeiro/buscarBoletosBaixa",
            dataType: 'html',
            success: function (data) {}
        });
    });
    */
</script>
</body>
</html>







