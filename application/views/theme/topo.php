<?php
$filial = $this->db->get_where('filial', array('idFilial'=>$this->session->userdata('filial_id')))->row();
$emitente = $this->db->get_where('emitente', array('filial_id'=>$this->session->userdata('filial_id')))->row();
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/favicon.ico"/>

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>.:<?php echo TITLE_SISTEMA; ?>:.</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo base_url(); ?>assets/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo base_url(); ?>assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo base_url(); ?>assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="<?php echo base_url(); ?>assets/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Datatables -->
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- PNotify -->
    <link href="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>assets/build/css/custom.css" rel="stylesheet">

    <script>
        var BASE_URL = '<?php echo base_url();?>';
        var USUARIO_LOGADO_ID = <?php echo $this->session->userdata('id');?>;
        var CURRENT_URL = '<?php echo current_url();?>'
    </script>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <noscript><style type="text/css">#loading { display: none; }</style></noscript>
    <script type="text/javascript">
        $(window).load(function () {
           $("#loading").fadeOut("slow");
        });
    </script>
</head>

<body class="nav-md">
<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>
<div id="loading"></div>
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?php echo base_url() ?>mapos" class="site_title"><i class="fa fa-globe fa-spin"></i> <span title="<?php echo $emitente->cnpj; ?>">&nbsp;&nbsp; <?php echo $emitente->nome; ?></span></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <img src="<?php echo base_url() ?>assets/img/<?php echo PACOTE; ?>/logo.png" alt="..." class="img-circle profile_img"/>
                </div>
                <!-- /menu profile quick info -->

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a href="<?php echo base_url() ?>mapos"><i class="fa fa-home"></i> Página inicial</a></li>

                            <li><a href="<?php echo base_url() ?>pdv" target="_blank"><i class="fa fa-desktop"></i> PDV</a></li>

                            <?php if (
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vCliente')) { ?>
                            <li>
                                <a href="<?php echo base_url() ?>clientes"><i class="fa fa-users"></i> Clientes</a>
                            </li>
                            <?php }?>
                            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vLancamento')) { ?>
                                <li>
                                    <a  href="<?php echo base_url() ?>financeiro"><i class="fa fa-usd"></i> Cobranças</a>
                                </li>
                                <li>
                                    <a  href="<?php echo base_url() ?>linkpagamento"><i class="fa fa-link"></i> Links de Pagamento</a>
                                </li>
                            <?php } ?>
                            <?php if (
                                $this->permission->checkPermission($this->session->userdata('permissao'), 'vTransferencia') ||
                                $this->permission->checkPermission($this->session->userdata('permissao'), 'vInventario') ||
                                $this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                            <li><a><i class="fa fa-sitemap"></i> Estoque <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo base_url() ?>produtos">Produtos</a></li>
                                    <li><a href="<?php echo base_url() ?>patrimonio">Patrimônio</a></li>
                                    <li><a href="<?php echo base_url() ?>inventario">Inventário de estoque</a></li>
                                    <li><a href="<?php echo base_url() ?>consultaestoque/consultaEstoque">Consulta Estoque</a></li>
                                    <li><a href="<?php echo base_url() ?>produtos/importacaoProdutoCSV">Importar Produtos</a></li>
                                    <li><a href="<?php echo base_url() ?>produtos/atualizacaoEstoqueProdutoCSV">Atualizar Estoque</a></li>
                                    <li><a href="<?php echo base_url() ?>produtos/atualizacaoPrecoVendaProdutoCSV">Atualizar Preço de Venda</a></li>
                                </ul>
                            </li>
                            <?php  }?>
                            <?php if (
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) { ?>
                            <li><a href="<?php echo base_url() ?>pedido"><i class="fa fa-shopping-bag"></i> Compras</a></li>
                            <?php }?>

                            <?php if (
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vPedido')) { ?>
                            <li><a href="<?php echo base_url() ?>pdv/vendas"><i class="fa fa-cart-plus"></i> Vendas</a></li>
                            <?php }?>

                            <?php if (
                                $this->permission->checkPermission($this->session->userdata('permissao'), 'vVistoria') ||
                                $this->permission->checkPermission($this->session->userdata('permissao'), 'vOs')) { ?>
                            <li><a><i class="fa fa-clone"></i>Operações <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo base_url() ?>vistoria">Checklist Veícular</a></li>
                                    <li><a href="<?php echo base_url() ?>os">Ordem de serviços</a></li>
                                    <li><a href="<?php echo base_url() ?>transferencia">Transferências</a></li>
                                </ul>
                            </li>
                            <?php }?>

                            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vProduto')) { ?>
                            <li style="display: none;"><a><i class="fa fa-refresh"></i> Manutenções <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo base_url() ?>programacaomanutencaopreventiva">Preventiva Patrimonial</a></li>
                                    <li><a href="<?php echo base_url() ?>programacaomanutencaopreventivaveiculo">Preventiva Veícular</a></li>
                                </ul>
                            </li>
                            <?php }?>

                            <?php if (
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfse')) { ?>
                            <li><a><i class="fa fa-table"></i> Nota Fiscal <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?php echo base_url() ?>nfe">NF-e</a></li>
                                    <li><a href="<?php echo base_url() ?>nfce">NFC-e</a></li>
                                    <li><a href="<?php echo base_url() ?>nfce/forcarBaixarXML">Forçar Download XML</a></li>
                                    <li><a href="http://www.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=" target="_blank">Consulta / Importar NFe Manualmente</a></li>
                                    <li><a href="http://www.nfce.se.gov.br/portal/painelMonitor.jsp" target="_blank">Consulta Webservices NFC-e</a></li>
                                    <li><a href=http://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=" target="_blank">Consulta Webservices NF-e</a></li>
                                </ul>
                            </li>
                            <?php }?>

                            <?php if (
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfe') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vNfse')) { ?>
                            <li>
                                <a href="<?php echo base_url() ?>coletorxml"><i class="fa fa-exchange"></i> Coletor XML </a>
                            </li>
                            <?php }?>

                            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'rServico') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'rOs') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                            <li><a><i class="fa fa-bar-chart-o"></i>Relatórios <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/clientes">Clientes</a></li>
                                    <?php } ?>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/produtos">Produtos</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rProduto')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/patrimonio">Patrimônio</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rServico')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/servicos">Serviços</a></li>
                                    <?php } ?>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/os">Ordens de Serviço</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rOs')) { ?>
                                        <li style="display: none;"><a href="<?php echo base_url() ?>relatorios/producao">Produção</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/vendas">Vendas</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/vendas_pdv">Vendas no PDV</a></li>
                                    <?php } ?>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rVenda')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/pgdas">PGDAS</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rFinanceiro')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/financeiro">Financeiro</a></li>
                                    <?php } ?>

                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'rCliente')) { ?>
                                        <li><a href="<?php echo base_url() ?>relatorios/aniversario">Aniversário</a></li>
                                    <?php } ?>
                                    <li><a href="<?php echo base_url() ?>veiculos">Veículos</a></li>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if (
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vServico') ||
                            $this->permission->checkPermission($this->session->userdata('permissao'), 'vArquivo')) { ?>
                            <li><a><i class="fa fa-windows"></i>Apoio <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vServico')) { ?>
                                        <li><a href="<?php echo base_url() ?>servicos">Serviços</a></li>
                                    <?php }?>
                                    <li><a href="<?php echo base_url() ?>receita">Plano de Receitas</a></li>
                                    <li><a href="<?php echo base_url() ?>despesa">Plano de Despesas </a></li>
                                    <li><a href="<?php echo base_url() ?>condicaopagamento">Condições de pagamento</a></li>
                                    <li><a href="<?php echo base_url() ?>grupoproduto">Grupo de produtos</a></li>
                                    <li><a href="<?php echo base_url() ?>naturezaoperacao">Natureza de operação</a></li>
                                    <li><a href="<?php echo base_url() ?>taxas/editar/1">Taxas</a></li>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'vArquivo')) { ?>
                                        <li style="display: none;"><a href="<?php echo base_url() ?>arquivos">Arquivos</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <?php }?>
                            <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cUsuario')
                                || $this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')
                                || $this->permission->checkPermission($this->session->userdata('permissao'), 'cPermissao')
                                || $this->permission->checkPermission($this->session->userdata('permissao'), 'cBackup')) { ?>
                            <li>
                            <li><a><i class="fa fa-cogs"></i>Configurações <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li class="<?php if (isset($menuTipoveiculo)) {  echo 'active'; }; ?>">
                                        <a href="<?php echo base_url() ?>index.php/configuracao/editar/1">Configuração</a>
                                    </li>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cUsuario')) { ?>
                                        <li><a href="<?php echo base_url() ?>index.php/usuarios">Usuários</a></li>
                                    <?php } ?>

                                    <li class="<?php if (isset($menuSetor)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/setor"><span>Setores</span></a>
                                    </li>

                                    <li class="<?php if (isset($menuVisaoautomovel)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/visaoautomovel">Visão do automóvel</a>
                                    </li>

                                    <li class="<?php if (isset($menuEquipamentoveiculo)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/equipamentoveiculo">Equipamento automóvel</a>
                                    </li>
                                    <li class="<?php if (isset($menuTipoveiculopneu)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/tipoveiculopneu">Tipos pneu veículo</a>
                                    </li>
                                    <li class="<?php if (isset($menuTipoavaria)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/tipoavaria">Tipos de Avaria</a>
                                    </li>
                                    <li class="<?php if (isset($menuAcessorio)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/acessorio">Acessórios</a>
                                    </li>
                                    <li class="<?php if (isset($menuTipoveiculo)) {  echo 'active'; }; ?>" style="display:none;">
                                        <a href="<?php echo base_url() ?>index.php/tipoveiculo">Tipos de Veículo</a>
                                    </li>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cEmitente')) { ?>
                                        <li><a href="<?php echo base_url() ?>index.php/mapos/emitente">Emitente</a></li>
                                    <?php } ?>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cPermissao')) { ?>
                                        <li><a href="<?php echo base_url() ?>index.php/permissoes">Permissões</a></li>
                                    <?php } ?>
                                    <?php if ($this->permission->checkPermission($this->session->userdata('permissao'), 'cBackup')) { ?>
                                        <li style="display:none;"><a href="<?php echo base_url() ?>index.php/mapos/backup">Backup</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php }?>
                            <li style="display: none;">
                                <a href="<?php echo base_url() ?>index.php/mapos/pesquisar?termo=all"><i class="fa fa-search"></i> Consultas</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small" style="display: none;">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                    <ul class=" navbar-right">
                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                <!--<?php if ($emitente->url_logo != ''){?>
                                    <img src="<?php echo $emitente->url_logo; ?>" alt="">
                                <?php } else {?>

                                <?php }?>!-->
                                <img src="<?php echo $emitente->url_logo ?>" alt="">
                                <?php echo 'Bem vindo(a) '.$this->session->userdata('nome')?>
                            </a>
                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo base_url() ?>mapos/sair"><i class="fa fa-sign-out pull-right"></i> Sair do Sistema</a>
                            </div>
                        </li>

                        <li role="presentation" style="display:none;" class="nav-item dropdown open">
                            <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                                <li class="nav-item">
                                    <a class="dropdown-item">
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                                        <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="dropdown-item">
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                                        <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="dropdown-item">
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                                        <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="dropdown-item">
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                                        <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <div class="text-center">
                                        <a class="dropdown-item">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div id="custom_notifications" class="custom-notifications dsp_none">
                <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
                </ul>
                <div class="clearfix"></div>
                <div id="notif-group" class="tabbed_notifications"></div>
            </div>
            <div class="">
                <?php if (isset($view)) {echo $this->load->view($view);}?>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-center" style="text-align: center;">
                <?php echo date('Y'); ?> &copy; <a href="<?php echo SITE_EMPRESA; ?>"> <?php echo NOME_SISTEMA; ?> (<?php echo VERSAO; ?>)</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/vendors/iCheck/icheck.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url(); ?>assets/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-wysiwyg -->
<script src="<?php echo base_url(); ?>assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="<?php echo base_url(); ?>assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="<?php echo base_url(); ?>assets/vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="<?php echo base_url(); ?>assets/vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="<?php echo base_url(); ?>assets/vendors/autosize/dist/autosize.min.js"></script>

<!-- jQuery autocomplete
<script src="<?php echo base_url(); ?>assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
-->

<!-- starrr -->
<script src="<?php echo base_url(); ?>assets/vendors/starrr/dist/starrr.js"></script>
<!-- morris.js -->
<script src="<?php echo base_url(); ?>assets/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/morris.js/morris.min.js"></script>
<!-- Datatables -->
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- Skycons -->
<script src="<?php echo base_url(); ?>assets/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.time.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.stack.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo base_url(); ?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo base_url(); ?>assets/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url(); ?>assets/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>

<!-- PNotify -->
<script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<!-- Chart.js -->
<script src="<?php echo base_url(); ?>assets/vendors/Chart.js/dist/Chart.min.js"></script>

<!-- morris.js -->
<script src="<?php echo base_url(); ?>assets/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/morris.js/morris.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>assets/build/js/custom.js"></script>

<!-- Arquivo de mensagens -->
<script src="<?php echo base_url(); ?>assets/js/mensagem.js"></script>

<script type="text/javascript" charset="UTF-8">
    $(window).load(function () {
        if ( getCookie('MENU_TOGGLE') === 'true') {
            $('#menu_toggle').click();
        }
        //baixarBoletosAutomaticamente();
    });

   function baixarBoletosAutomaticamente() {
       $.ajax({
           type: "POST",
           url: "<?php echo base_url();?>index.php/financeiro/buscarBoletosBaixa",
           dataType: 'html',
           success: function (data) {}
       });
   }
</script>

</body>
</html>