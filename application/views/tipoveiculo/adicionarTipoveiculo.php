<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Tipo de Veículo</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formTipoveiculo" enctype="multipart/form-data" method="post" class="form-horizontal" >

                    <div class="control-group">
                        <label for="preco" class="control-label"> Arquivo </label>
                        <div class="controls">
                            <input id="arquivo" type="file" name="userfile" /> (txt|pdf|gif|png|jpg|jpeg)
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" required="required" value="<?php echo set_value('nome'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_eixo" class="control-label">Nº Eixos</label>
                        <div class="controls">
                            <input id="numero_eixo" type="number" min="1" name="numero_eixo"  value="<?php echo set_value('numero_eixo'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_unidade_tracao" class="control-label">Nº Unidades de Tração</label>
                        <div class="controls">
                            <input id="numero_unidade_tracao" type="number" min="0" name="numero_unidade_tracao"  value="<?php echo set_value('numero_unidade_tracao'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_unidade_acoplada" class="control-label">Nº Unidades Acopladas</label>
                        <div class="controls">
                            <input id="numero_unidade_acoplada" type="number" min="0" name="numero_unidade_acoplada"  value="<?php echo set_value('numero_unidade_acoplada'); ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição</label>
                        <div class="controls">
                            <textarea id="descricao" name="descricao"><?php echo set_value('descricao'); ?></textarea>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/tipoveiculo" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#formTipoveiculo').validate({
            rules :{
                nome:{ required: true}
            },
            messages:{
                nome :{ required: 'Campo Requerido.'}
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>