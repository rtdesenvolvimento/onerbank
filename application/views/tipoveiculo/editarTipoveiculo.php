<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                      <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Tipo veículo</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formTipoveiculo" enctype="multipart/form-data" method="post" class="form-horizontal">
                    <?php echo form_hidden('idTipoveiculo', $result->idTipoveiculo) ?>

                    <div class="control-group">
                        <div class="controls">
                            <img src="<?php echo base_url().'assets/arquivos/tipoveiculo/'.$result->anexo; ?>" width="50%" height="50%">
                         </div>
                    </div>

                    <div class="control-group">
                        <label for="preco" class="control-label"> Arquivo </label>
                        <div class="controls">
                            <input id="arquivo" type="file" name="userfile" /> (txt|pdf|gif|png|jpg|jpeg)
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" type="text" name="nome" value="<?php echo $result->nome ?>"/>
                        </div>
                    </div>


                    <div class="control-group">
                        <label for="numero_eixo" class="control-label">Nº Eixos</label>
                        <div class="controls">
                            <input id="numero_eixo" min="1" type="number" name="numero_eixo" value="<?php echo $result->numero_eixo ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_unidade_tracao" class="control-label">Nº Unidades de Tração</label>
                        <div class="controls">
                            <input id="numero_unidade_tracao" min="0" type="number" name="numero_unidade_tracao" value="<?php echo $result->numero_unidade_tracao ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="numero_unidade_acoplada" class="control-label">Nº Unidades Acopladas</label>
                        <div class="controls">
                            <input id="numero_unidade_acoplada" min="0" type="number" name="numero_unidade_acoplada" value="<?php echo $result->numero_unidade_acoplada ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição</label>
                        <div class="controls">
                            <textarea name="descricao"><?php echo $result->descricao ?></textarea>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar
                                </button>
                                <a href="<?php echo base_url() ?>index.php/tipoveiculo" id="btnAdicionar" class="btn"><i
                                        class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".money").maskMoney();
        $('#formTipoveiculo').validate({
            rules: {
                nome: {required: true}
            },
            messages: {
                nome: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>