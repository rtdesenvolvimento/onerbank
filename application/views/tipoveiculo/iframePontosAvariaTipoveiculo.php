<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
    #special {
        width: 1055px;
        height: 312px;
        border:1px ridge green;
        background: #0e90d2;
    }
</style>
<body>
<h2 id="status2" style="display: none;">0, 0</h2>
<canvas id="special" style="background-image: url('<?php echo base_url().'assets/arquivos/tipoveiculo/'.$result->anexo; ?>')">Not supported</canvas>

<script>

    var contador = 0;

    jQuery(document).ready(function(){

        $('#special').attr('height', $('#special').css('height'));
        $('#special').attr('width', $('#special').css('width'));

        $("#special").click(function(e){

            if (contador == 0) {
                var x = e.pageX - this.offsetLeft;
                var y = e.pageY - this.offsetTop;

                var ctx = this.getContext("2d");
                ctx.beginPath();
                ctx.arc(x, y, 10, 0, 2 * Math.PI, false);
                ctx.fillStyle = "white";
                ctx.fill();
                ctx.stroke();

                window.parent.atribuirXY(x, y);

                $('#status2').html(x + ', ' + y);

                contador = contador + 1;
            } else {
                alert("Só é possível selecionar UM ponto na imagem referente a esse equipamento/avaria! Caso tenha errado o ponto, cliente no botão Limpar.");
            }

        });
    })
</script>
</body>