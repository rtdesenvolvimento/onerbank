<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Setor</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formSetor" method="post" class="form-horizontal" >

                    <div class="control-group">
                        <label for="nome" class="control-label">Tipo veículo<span class="required">*</span></label>
                        <div class="controls">
                            <select name="idTipoveiculo" id="idTipoveiculo" required="required">
                                <option value=""></option>
                                <?php foreach ($tiposveiculo as $tipoveiculo) {?>
                                    <option value="<?php echo $tipoveiculo->idTipoveiculo; ?>"><?php echo $tipoveiculo->nome; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="posicao" class="control-label">Posição<span class="required">*</span></label>
                        <div class="controls">
                            <input id="posicao" required="required" type="text" name="posicao" value="<?php echo set_value('posicao'); ?>"  />
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/tipoveiculopneu" id="btnAdicionar" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>






