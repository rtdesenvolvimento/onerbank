<a href="<?php echo base_url() ?>index.php/tipoveiculopneu/adicionar" class="btn btn-success">
    <i class="icon-plus icon-white"></i> Adicionar Tipo pneu veículo
</a>
<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Tipos pneu veículo</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Tipo de veículo</th>
                    <th>Posição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhum Tipo pneu veículo Cadastrado</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Tipos pneu veículo</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>#</th>
                    <th>Tipo de veículo</th>
                    <th>Posição</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {

                    $tipoveiculo = $this->tipoveiculo_model->getById($r->idTipoveiculo);

                    echo '<tr>';
                    echo '    <td>' . $tipoveiculo->nome . '</td>';
                    echo '    <td>' . $r->posicao . '</td>';
                    echo '    <td>';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/tipoveiculopneu/editar/' . $r->idTipoveiculopneu . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '      <a href="#modal-excluir" role="button" data-toggle="modal" tipoveiculopneu="' . $r->idTipoveiculopneu . '" class="btn btn-danger tip-top"><i class="icon-remove icon-white"></i></a>  ';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/tipoveiculopneu/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Tipo pneu do veículo</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idTipoveiculopneu" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir este tipo pneu do veículo?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var tipoveiculopneu = $(this).attr('tipoveiculopneu');
            $('#idTipoveiculopneu').val(tipoveiculopneu);
        });
    });
</script>