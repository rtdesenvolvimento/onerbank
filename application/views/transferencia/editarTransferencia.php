<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Transferência <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" method="post" id="formTransferencia">
                    <?php echo form_hidden('idTransferencia', $result->idTransferencia) ?>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <label for="cliente">Origem<span class="required">*</span></label>
                            <div class="controls">
                                <select name="filial_id" readonly class="form-control" id="filial_id" required="required">
                                    <option value="<?php echo $origem->idFilial; ?>"><?php echo $origem->nome; ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="cliente">Destino<span class="required">*</span></label>
                            <div class="controls">
                                <select name="filial_destino_id" class="form-control" id="filial_destino_id" required="required">
                                    <option value="">--Selecione um estoque--</option>
                                    <?php foreach ($filiais as $filial) {?>
                                        <option <?php if ($result->filial_destino_id == $filial->idFilial) echo 'selected="selected"';?> value="<?php echo $filial->idFilial; ?>"><?php echo $filial->nome; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="tecnico">Responsável<span class="required">*</span></label>
                            <div class="controls">
                                <select name="usuarios_id" class="form-control" id="usuarios_id" required="required">
                                    <option value="">--Selecione um cliente--</option>
                                    <?php foreach ($usuarios as $usuario) {?>
                                        <option <?php if ($result->usuarios_id == $usuario->idUsuarios) echo 'selected="selected"'; ?> value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="status">Status<span class="required">*</span></label>
                            <select class="form-control" name="status" disabled id="status">
                                <option <?php if($result->status == 'Aberto'){echo 'selected';} ?> value="Aberto">Aberto</option>
                                <option <?php if($result->status == 'Confirmado'){echo 'selected';} ?> value="Confirmado">Confirmado</option>
                                <option <?php if($result->status == 'Em Trânsito'){echo 'selected';} ?> value="Em Trânsito">Em Trânsito</option>
                                <option <?php if($result->status == 'Entregue'){echo 'selected';} ?> value="Entregue">Entregue</option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="dataTransferencia">Data</label>
                            <input id="dataTransferencia" class="form-control" required="required" type="date" name="dataTransferencia"
                                   value="<?php echo $result->dataTransferencia;?>"/>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label for="previsaoEntrega">Previsão entrega</label>
                            <input id="previsaoEntrega" class="form-control" type="date" name="previsaoEntrega"
                                   value="<?php echo $result->previsaoEntrega;?>"/>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="span12" style="text-align: right;margin-bottom: 10px;">
                        <div class="span8 offset2">
                             <button class="btn btn-primary" id="btnContinuar">
                                 <i class="fa fa-save"></i> Alterar
                            </button>
                            <?php if ($result->status == 'Aberto') { ?>
                                <a href="<?php echo base_url() ?>index.php/transferencia/confirmarTransferencia/<?php echo $result->idTransferencia; ?>"
                                   class="btn btn-success"><i class="fa fa-check"></i>Confirmar
                                </a>
                            <?php }?>

                            <?php if ($result->status == 'Confirmado') { ?>
                                <a href="<?php echo base_url() ?>index.php/transferencia/emTransito/<?php echo $result->idTransferencia; ?>"
                                   class="btn btn-dark" id="confirmarPedido">
                                    <i class="fa fa-bus"></i> Em Trânsito
                                </a>
                            <?php }?>

                            <?php if ($result->status == 'Em Trânsito') { ?>
                                <a href="<?php echo base_url() ?>index.php/transferencia/entregar/<?php echo $result->idTransferencia; ?>/<?php echo $result->filial_destino_id; ?>"
                                   class="btn btn-dark" id="confirmarPedido"><i class="fa fa-bus"></i> Em Trânsito
                                </a>
                            <?php }?>

                            <a href="<?php echo base_url() ?>index.php/transferencia/visualizar/<?php echo $result->idTransferencia; ?>"
                               class="btn btn-warning"><i class="fa fa-eye"></i> Visualizar
                                Transferência
                            </a>
                            <a href="<?php echo base_url() ?>index.php/transferencia" class="btn btn-primary">
                                <i class="fa fa-backward"></i> Voltar</a>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 col-sm-12 ">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Itens da Transferência</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tablItens" data-toggle="tab">Itens</a></li>
                                        <li><a href="#tab3" data-toggle="tab">Dados de entrega</a></li>
                                        <li><a href="#tab4" data-toggle="tab">Observações do pedido</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tablItens">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <form id="formProdutos"
                                                      action="<?php echo base_url(); ?>index.php/pedido/adicionarProduto"
                                                      method="post">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <input type="hidden" name="idTransferencia" id="idTransferencia"
                                                                   value="<?php echo $result->idTransferencia ?>"/>
                                                            <input type="hidden" name="estoque" id="estoque" value=""/>
                                                            <input type="hidden" name="preco" id="preco" value=""/>
                                                            <div class="formBuscaGSA">
                                                                <label for="">Produto
                                                                    <a href="#modal-consulta-estoque" id="addConsultaProduto" data-toggle="modal">
                                                                        <i id="pincliente" style="float: right;margin-right: 10px;" class="icon-bar-chart icon-white"></i></a>
                                                                </label>

                                                                <div class="controls">
                                                                    <select name="idProduto" class="form-control" id="idProduto" required="required">
                                                                        <option value="">--Selecione um produto--</option>
                                                                        <?php foreach ($produtosList as $produto) {?>
                                                                            <option value="<?php echo $produto->idProdutos; ?>"><?php echo $produto->descricao; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <label for="">Quantidade</label>
                                                            <input type="text" placeholder="Quantidade" required="required" id="quantidade"
                                                                   name="quantidade" class="form-control" value="1"/>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <label for="">Preço de compra</label>
                                                            <input type="text" placeholder="Preço de compra" required="required" id="custo" name="custo"
                                                                   class="form-control"/>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <label for="">&nbsp</label>
                                                            <button class="btn btn-success form-control" id="btnAdicionarProduto"><i
                                                                    class="icon-white icon-plus"></i> Adicionar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="span12" id="divProdutos" style="margin-left: 0">
                                                    <table class="table table-bordered" id="tblProdutos">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: left;">Produto</th st>
                                                            <th style="text-align: right;">Quantidade</th>
                                                            <th style="text-align: right;">Preço de compra</th>
                                                            <th style="text-align: right;">SubTotal</th>
                                                            <th style="text-align: center;">Ações</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $total = 0;
                                                        $custo = 0;
                                                        foreach ($produtos as $p) {

                                                            $total = $total + $p->subTotal;
                                                            $custo = $custo + $p->custo;

                                                            echo '<tr>';
                                                            echo '<td style="text-align: left;">' . $p->descricao . '</td>';
                                                            echo '<td style="text-align: right;">' . $p->quantidade . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->custo, 2, ',', '.') . '</td>';
                                                            echo '<td style="text-align: right;">R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                                            echo '<td  style="text-align: center;"><a href="" idAcao="' . $p->idItens . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a></td>';
                                                            echo '</tr>';
                                                        } ?>
                                                        <tr>
                                                            <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                                            <td colspan="2" style="text-align: left">
                                                                <strong>
                                                                    R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="text-align: right"><strong>SubTotal:</strong></td>
                                                            <td colspan="2" style="text-align: left">
                                                                <strong>
                                                                    R$ <?php echo number_format($total , 2, ',', '.'); ?>
                                                                </strong>
                                                                <input type="hidden" id="total-venda"
                                                                       value="<?php echo number_format($total, 2); ?>">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab3">
                                            <form action="<?php echo current_url(); ?>" method="post" id="formEnderecoEntregaTransferencia">
                                                <input type="hidden" name="idTransferencia" id="idTransferencia"
                                                       value="<?php echo $result->idTransferencia ?>"/>
                                                <div class="row" style="padding: 1%; margin-left: 0">
                                                    <div class="col-md-3 col-sm-3">
                                                        <label for="cepEntrega">CEP</label>
                                                        <input id="cepEntrega" type="text" name="cepEntrega" class="form-control" onBlur="getConsultaCEP();"
                                                               value="<?php echo $result->cepEntrega; ?>"/>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6">
                                                        <label for="ruaEntrega">Rua</label>
                                                        <input id="ruaEntrega" type="text" name="ruaEntrega"  class="form-control"
                                                               value="<?php echo $result->ruaEntrega; ?>"/>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label for="numeroEntrega">Número</label>
                                                        <input id="numeroEntrega" type="text" name="numeroEntrega"  class="form-control"
                                                               value="<?php echo $result->numeroEntrega; ?>"/>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <label for="complementoEntrega">Complemento</label>
                                                        <input id="complementoEntrega" type="text" name="complementoEntrega"  class="form-control"
                                                               value="<?php echo $result->complementoEntrega; ?>"/>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5">
                                                        <label for="bairroEntrega">Bairro</label>
                                                        <input id="bairroEntrega" type="text" name="bairroEntrega"  class="form-control"
                                                               value="<?php echo $result->bairroEntrega; ?>"/>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <label for="cidadeEntrega">Cidade</label>
                                                        <input id="cidadeEntrega" type="text" name="cidadeEntrega"  class="form-control"
                                                               value="<?php echo $result->cidadeEntrega; ?>"/>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label for="estadoEntrega">Estado</label>
                                                        <input id="estadoEntrega" type="text" name="estadoEntrega"  class="form-control"
                                                               value="<?php echo $result->estadoEntrega; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="ln_solid"></div>
                                                <div class="span12" style="text-align: right;">
                                                    <div class="span8 offset2">
                                                        <button class="btn btn-success" id="btnContinuar"><i
                                                                class="fa fa-save"></i> Salvar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <form action="<?php echo current_url(); ?>" method="post" id="formObservacaoTransferencia">
                                                <?php echo form_hidden('idTransferencia', $result->idTransferencia) ?>
                                                <div class="row" style="padding: 1%; margin-left: 0">
                                                    <div class="col-md-12 col-sm-12">
                                                        <textarea class="form-control" name="observacao" cols="30" rows="5"><?php echo $result->observacao; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="ln_solid"></div>
                                                <div class="span12" style="text-align: right;">
                                                    <div class="span8 offset2">
                                                        <button class="btn btn-primary" id="btnContinuar"><i
                                                                class="fa fa-save"></i> Salvar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal consulta de estoque -->
<div id="modal-consulta-estoque" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmail" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Consulta de Estoque</h3>
        </div>
        <div class="modal-body">

            <div class="span12" style="padding: 1%; margin-left: 0;text-align: center;">
                <label for="senha"><div id="consultaEstoqueNomeProduto"></div></label>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <table class="table table-bordered" id="tblConsultaEstoque">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="text-align: left;">Filial</th>
                        <th style="width: 10%;text-align: right;">Estoque</th>
                        <!--
                        <th style="width: 10%;text-align: right;">Reservado</th>
                        <th style="width: 10%;text-align: right;">Bloqueado</th>
                        <th style="width: 10%;text-align: right;">Disponível</th>
                        !-->
                        <th style="width: 10%;text-align: right;">Venda(R$)</th>
                        <th style="width: 20%;text-align: center;">Localização</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr style="backgroud-color: #2D335B">
                        <th colspan="10"></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">

    function getConsultaCEP() {
        if($.trim($("#cepEntrega").val()) != ""){
            var url = 'http://api.postmon.com.br/v1/cep/'+$("#cepEntrega").val();
            $.get(url,{
                    cep:$("#cepEntrega").val()
                },
                function (data) {
                    if(data != -1){
                        $("#ruaEntrega").val( data.logradouro  );
                        $("#bairroEntrega").val( data.bairro );
                        $("#cidadeEntrega").val( data.cidade );
                        $("#estadoEntrega").val( data.estado );
                    }
                });
        }
    }

    $(document).ready(function () {

        $(".money").maskMoney();

        $("#formTransferencia").validate({
            rules: {
                cliente: {required: true},
                tecnico: {required: true},
                dataVenda: {required: true}
            },
            messages: {
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataVenda: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });



        $("#formEnderecoEntregaTransferencia").validate({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/transferencia/editarEnderecoEntrega",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        alert('Alterado');
                    }
                });
                return false;
            }
        });

        $("#formObservacaoTransferencia").validate({
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/transferencia/editarObservacao",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        alert('Alterado');
                    }
                });
                return false;
            }
        });

        $("#formProdutos").validate({
            rules: {
                quantidade: {required: true},
                custo: {required: true},
                idProduto: {required: true}
            },
            messages: {
                quantidade: {required: 'Insira a quantidade'},
                custo: {required: 'Insira o preço de compra'},
                idProduto: {required: 'Insira o produto'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/transferencia/adicionarProduto",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");

                            $("#idProduto").select2('data', {
                                id: null,
                                text: ''
                            });

                            $("#quantidade").val('1');
                            $("#custo").val('');
                            $("#idProduto").val('').focus();
                        }
                        else {
                            alert('Ocorreu um erro ao tentar adicionar produto.');
                        }
                    }
                });
                return false;
            }
        });

        $(document).on('click', 'a', function (event) {
            var idProduto   = $(this).attr('idAcao');
            var quantidade  = $(this).attr('quantAcao');
            var produto     = $(this).attr('prodAcao');

            if ((idProduto % 1) == 0) {
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/transferencia/excluirProduto",
                    data: "idProduto=" + idProduto + "&quantidade=" + quantidade + "&produto=" + produto,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir produto.');
                        }
                    }
                });
                return false;
            }
        });

        $('#addConsultaProduto').click(function (event) {
            event.preventDefault();
            var table = $('#tblConsultaEstoque');
            var $tbody = table.append('<tbody />').children('tbody');
            $tbody.empty();
            $('#consultaEstoqueNomeProduto').html('');
            var produto = $('#idProduto').val();

            if (produto) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/getProdutoFiliais",
                    data: "produto=" + produto,
                    dataType: 'json',
                    success: function (produtofiliais) {
                        $.each(produtofiliais, function(i, produtofilial) {

                            var table = $('#tblConsultaEstoque');
                            var $tbody = table.append('<tbody />').children('tbody');
                            $('#consultaEstoqueNomeProduto').html(produtofilial.produto);

                            $tbody.append('<tr />').children('tr:last')
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")' style='text-align: left;cursor: pointer;'>" + produtofilial.filial + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.estoque + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.reservado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.bloqueado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'><b>" + (produtofilial.estoque -  produtofilial.reservado - produtofilial.bloqueado) + ' ' + produtofilial.unidade + "</b></td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>R$" + produtofilial.precoVenda + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: center;cursor: pointer;'>" + produtofilial.localizacao + " </td>");
                        });
                    }
                });
            } else {
                $('#consultaEstoqueNomeProduto').html('Selecione um produto para consulta estoque');
            }
        });

        $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});
    });

    function preencherEstoquePorFilial(idProdutoFilial) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/produtos/getUnicProdutoFilial",
            data: "idProdutoFilial=" + idProdutoFilial,
            dataType: 'json',
            success: function (produtofiliais) {
                $("#idProduto").val(produtofiliais.produto_id);
                $("#estoque").val(produtofiliais.estoque);
                $("#custo").val(produtofiliais.precoVenda);
                $("#filial_customizada").val(produtofiliais.filial_id);
                $("#quantidade").focus();
                $("#modal-consulta-estoque .close").click()

            }
        });
    }

</script>

