<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-random"></i> Transferência (<?php echo count($results); ?>)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'aVenda')){ ?>
                        <a class="btn btn-success" href="<?php echo base_url(); ?>transferencia/adicionar"><i class="fa fa-plus"></i> Nova Transferência</a>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th style="text-align: center;">Data</th>
                                    <th style="text-align: center;">Previsão Entrega</th>
                                    <th style="text-align: left;">Origem</th>
                                    <th style="text-align: left;">Destino</th>
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    $dataTransferncia = date(('d/m/Y'),strtotime($r->dataTransferencia));
                                    $previsao   = '-';

                                    if ($r->previsaoEntrega) {
                                        $previsao = date(('d/m/Y'),strtotime($r->previsaoEntrega));
                                    }
                                    echo '<tr>';
                                    echo '<td style="text-align: center;">'.$dataTransferncia.'</td>';
                                    echo '<td style="text-align: center;">'.$previsao.'</td>';
                                    echo '<td style="text-align: left;">'.$r->origem.'</td>';
                                    echo '<td style="text-align: left;">'.$r->destino.'</td>';
                                    echo '<td style="text-align: center;">'.$r->statusTransferencia.'</td>';

                                    echo '<td style="text-align: center;">';
                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){
                                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/transferencia/visualizar/'.$r->idTransferencia.'"><button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-eye"> </i> </button></a>';
                                    }
                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                                        echo '<a style="margin-right: 1%" href="'.base_url().'index.php/transferencia/editar/'.$r->idTransferencia.'"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>';
                                    }
                                    if($this->permission->checkPermission($this->session->userdata('permissao'),'dVenda')){
                                        echo '<a href="#modal-excluir" role="button" data-toggle="modal" venda="'.$r->idTransferencia.'"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> </button></a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/vendas/excluir" method="post" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Venda</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idVenda" name="id" value="" />
            <h5 style="text-align: center">Deseja realmente excluir este pedido de compra?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', 'a', function(event) {
            var venda = $(this).attr('venda');
            $('#idVenda').val(venda);
        });
    });

</script>