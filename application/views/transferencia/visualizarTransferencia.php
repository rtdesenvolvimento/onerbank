<?php $totalProdutos = 0;?>

<style type="text/css">
    body {
        overflow-x: hidden;
        margin-top: -10px;
        font-family: 'Open Sans', sans-serif;
        font-size: 11px;
        color: #666;
    }

    .table th, .table td {
        padding: 0px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informações da Transferência <small>#<?php echo $result->idTransferencia?></small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon">
                            <i class="icon-tags"></i>
                        </span>
                        <div class="buttons">
                            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eVenda')){
                                echo '<a class="btn btn-mini btn-info" href="'.base_url().'index.php/transferencia/editar/'.$result->idTransferencia.'"><i class="icon-pencil icon-white"></i> Editar</a>';
                            } ?>

                            <a id="imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i> Imprimir</a>
                        </div>
                    </div>
                    <div class="widget-content" id="printOs">
                        <div class="invoice-content">
                            <table class="table" style="margin-top: 0">
                                <tbody>

                                <?php if($emitente == null) {?>
                                    <tr>
                                        <td colspan="3" class="alert">Você precisa configurar os dados do emitente. >>><a href="<?php echo base_url(); ?>index.php/mapos/emitente">Configurar</a><<<</td>
                                    </tr>
                                <?php } else {?>

                                    <tr>
                                        <td style="width: 10%"><img src=" <?php echo $emitente->url_logo; ?> "></td>
                                        <td> <span style="font-size: 20px; "> <?php echo $emitente->nome; ?></span> </br><span><?php echo $emitente->cnpj; ?> </br> <?php echo $emitente->rua.', nº:'.$emitente->numero.', '.$emitente->bairro.' - '.$emitente->cidade.' - '.$emitente->uf; ?> </span> </br> <span> E-mail: <?php echo $emitente->email.' - Fone: '.$emitente->telefone; ?></span></td>
                                        <td style="width: 30%; text-align: left">
                                            <b>#Transferência:</b> <span ><?php echo $result->idTransferencia?></span>
                                            </br>
                                            <b>Status:</b> <span ><?php echo $result->status;?></span>
                                            </br>
                                            <b>Origem:</b> <span ><?php echo $result->origem;?></span>
                                            </br>
                                            <b>Destino:</b> <span ><?php echo $result->destino;?></span>
                                            </br>
                                            <span><b>Emissão: </b><?php echo date(('d/m/Y'),strtotime($result->dataTransferencia));?></span>
                                            <?php if ($result->previsaoEntrega != '0000-00-00') {?>
                                                </br>
                                                <span><b>Previsão: </b><?php echo date(('d/m/Y'),strtotime($result->previsaoEntrega));?></span>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                <?php } ?>
                                </tbody>
                            </table>

                            <table class="table" style="margin-top: 0">
                                <tbody>
                                <tr>
                                    <td style="width: 30%; padding-left: 0">
                                        <ul>
                                            <li>
                                                <span><h6>Responsável</h6></span>
                                                <span><?php echo $result->nome?></span> <br/>
                                                <span>Telefone: <?php echo $result->telefone?></span><br/>
                                                <span>Email: <?php echo $result->email?></span>
                                            </li>
                                        </ul>
                                    </td>

                                    <td style="width: 30%; padding-left: 0">
                                        <ul>
                                            <li>
                                                <span><h6>Entrega</h6></span>
                                                <span>Rua: <?php echo $result->ruaEntrega.' '.$result->numeroEntrega?></span> <br/>
                                                <span>Complemento: <?php echo $result->complementoEntrega?></span><br/>
                                                <span>Bairro: <?php echo $result->bairroEntrega ?></span> <br/>
                                                <span>Cidade: <?php echo $result->cidadeEntrega.'/'.$result->estadoEntrega.' - '.$result->cepEntrega?></span>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>

                                <?php if ($result->observacao) {?>
                                    <tr>
                                        <td colspan="2">
                                            <span><h6>Observação</h6>
                                                <?php echo $result->observacao;?>
                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>


                            <div style="margin-top: 0; padding-top: 0">


                                <?php if($produtos != null){?>

                                    <table class="table table-bordered table-condensed" id="tblProdutos">
                                        <thead>
                                        <tr>
                                            <th width="55%" style="font-size: 15px;text-align: left;">Produto</th>
                                            <th width="5%" style="font-size: 15px;text-align: center;">Quantidade</th>
                                            <th width="20%" style="font-size: 15px;text-align: right;">Sub-total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        foreach ($produtos as $p) {

                                            $totalProdutos = $totalProdutos + $p->subTotal;
                                            echo '<tr>';
                                            echo '<td style="text-align: left;">'.$p->descricao.'</td>';
                                            echo '<td style="text-align: center;"> '.$p->quantidade.'   </td>';
                                            echo '<td style="text-align: right;">R$ '.number_format($p->subTotal,2,',','.').'</td>';
                                            echo '</tr>';
                                        }?>

                                        <tr>
                                            <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                                            <td style="text-align: right;"><strong>R$ <?php echo number_format($totalProdutos,2,',','.');?></strong></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                <?php }?>


                                <h4 style="text-align: right">SubTotal: R$ <?php echo number_format($totalProdutos ,2,',','.');?></h4>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $("#imprimir").click(function(){

            //pega o Html da DIV
            var divElements = document.getElementById('printOs').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;
        });
    });
</script>