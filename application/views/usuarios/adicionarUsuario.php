<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Usuário<small>Adicionar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formUsuario" method="post" class="form-horizontal" >
                    <div class="control-group">
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" class="form-control" type="text" name="nome" value="<?php echo set_value('nome'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="rg" class="control-label">RG</label>
                        <div class="controls">
                            <input id="rg" type="text" class="form-control" name="rg" value="<?php echo set_value('rg'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cpf" class="control-label">CPF</label>
                        <div class="controls">
                            <input id="cpf" type="text" class="form-control" name="cpf" value="<?php echo set_value('cpf'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="setor_id" class="control-label">Setor</label>
                        <div class="controls">
                            <select name="setor_id" class="form-control">
                                <option value="">Selecione um setor</option>
                                <?php
                                foreach($setores as $setor){
                                    echo '<option value="'.$setor->idSetor.'">'.$setor->nome.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="comissao" class="control-label">Comissão</label>
                        <div class="controls">
                            <input id="comissao" class="form-control" type="number" min="0" max="100" name="comissao" value="<?php echo set_value('comissao'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="rua" class="control-label">Rua</label>
                        <div class="controls">
                            <input id="rua" type="text" class="form-control" name="rua" value="<?php echo set_value('rua'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="numero" class="control-label">Numero</label>
                        <div class="controls">
                            <input id="numero" type="text" class="form-control" name="numero" value="<?php echo set_value('numero'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="bairro" class="control-label">Bairro</label>
                        <div class="controls">
                            <input id="bairro" type="text" class="form-control" name="bairro" value="<?php echo set_value('bairro'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cidade" class="control-label">Cidade</label>
                        <div class="controls">
                            <input id="cidade" type="text" class="form-control" name="cidade" value="<?php echo set_value('cidade'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="estado" class="control-label">Estado</label>
                        <div class="controls">
                            <input id="estado" type="text" class="form-control" name="estado" value="<?php echo set_value('estado'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="email" class="control-label">Email</label>
                        <div class="controls">
                            <input id="email" type="text" name="email" class="form-control"  value="<?php echo set_value('email'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="email" class="control-label">Usuário<span class="required">*</span></label>
                        <div class="controls">
                            <input id="usuariologin" type="text" class="form-control" name="usuariologin" value="<?php echo set_value('usuariologin'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="senha" class="control-label">Senha<span class="required">*</span></label>
                        <div class="controls">
                            <input id="senha" type="password" class="form-control" name="senha" value="<?php echo set_value('senha'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="telefone" class="control-label">Telefone<span class="required">*</span></label>
                        <div class="controls">
                            <input id="telefone" type="text" class="form-control" name="telefone" value="<?php echo set_value('telefone'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="celular" class="control-label">Celular</label>
                        <div class="controls">
                            <input id="celular" type="text" class="form-control" name="celular" value="<?php echo set_value('celular'); ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Situação*</label>
                        <div class="controls">
                            <select name="situacao" class="form-control" id="situacao">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Permissões<span class="required">*</span></label>
                        <div class="controls">
                            <select name="permissoes_id" class="form-control" id="permissoes_id">
                                  <?php foreach ($permissoes as $p) {
                                      echo '<option value="'.$p->idPermissao.'">'.$p->nome.'</option>';
                                  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-actions" style="text-align: right;">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>index.php/usuarios" id="" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script  src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){

           $('#formUsuario').validate({
            rules : {
                  nome:{ required: true},
                  telefone:{ required: true},
                  email:{ required: true},
                  senha:{ required: true}
            },
            messages: {
                  nome :{ required: 'Campo Requerido.'},
                  telefone:{ required: 'Campo Requerido.'},
                  email:{ required: 'Campo Requerido.'},
                  senha:{ required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });

      });
</script>




