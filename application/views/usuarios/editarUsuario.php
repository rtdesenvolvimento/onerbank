<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de Usuário<small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="<?php echo current_url(); ?>" id="formUsuario" method="post" class="form-horizontal" >
                    <div class="control-group">
                        <?php echo form_hidden('idUsuarios',$result->idUsuarios) ?>
                        <label for="nome" class="control-label">Nome<span class="required">*</span></label>
                        <div class="controls">
                            <input id="nome" class="form-control" type="text" name="nome" value="<?php echo $result->nome; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="rg" class="control-label">RG</label>
                        <div class="controls">
                            <input id="rg" class="form-control" type="text" name="rg" value="<?php echo $result->rg; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cpf" class="control-label">CPF</label>
                        <div class="controls">
                            <input id="cpf" class="form-control" type="text" name="cpf" value="<?php echo $result->cpf; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="setor_id" class="control-label">Setor</label>
                        <div class="controls">
                            <select name="setor_id" class="form-control">
                                <option value="">Selecione um setor</option>
                                <?php
                                foreach($setores as $setor){
                                    $sel = '';
                                    if ($setor->idSetor == $result->setor_id){
                                        $sel = 'selected="selected"';
                                    }
                                    echo '<option value="'.$setor->idSetor.'" '.$sel.'>'.$setor->nome.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="comissao" class="control-label">Comissão</label>
                        <div class="controls">
                            <input id="comissao" class="form-control" type="number" min="0" max="100" name="comissao" value="<?php echo $result->comissao; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="rua" class="control-label">Rua</label>
                        <div class="controls">
                            <input id="rua" type="text" class="form-control" name="rua" value="<?php echo $result->rua; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="numero" class="control-label">Numero</label>
                        <div class="controls">
                            <input id="numero" type="text" name="numero" class="form-control" value="<?php echo $result->numero; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="bairro" class="control-label">Bairro</label>
                        <div class="controls">
                            <input id="bairro" type="text" class="form-control" name="bairro" value="<?php echo $result->bairro; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cidade" class="control-label">Cidade</label>
                        <div class="controls">
                            <input id="cidade" type="text" class="form-control" name="cidade" value="<?php echo $result->cidade; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="estado" class="control-label">Estado</label>
                        <div class="controls">
                            <input id="estado" type="text" class="form-control" name="estado" value="<?php echo $result->estado; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="email" class="control-label">Email</label>
                        <div class="controls">
                            <input id="email" type="text" class="form-control" name="email" value="<?php echo $result->email; ?>"  />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="email" class="control-label">Usuário</label>
                        <div class="controls">
                            <input id="usuariologin" type="text" class="form-control" name="usuariologin" value="<?php echo $result->usuariologin; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="senha" class="control-label">Senha</label>
                        <div class="controls">
                            <input id="senha" type="password" class="form-control" name="senha" value=""  placeholder="Não preencha se não quiser alterar."  />
                            <i class="icon-exclamation-sign tip-top" title="Se não quiser alterar a senha, não preencha esse campo."></i>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="telefone" class="control-label">Telefone<span class="required">*</span></label>
                        <div class="controls">
                            <input id="telefone" type="text" class="form-control" name="telefone" value="<?php echo $result->telefone; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="celular" class="control-label">Celular</label>
                        <div class="controls">
                            <input id="celular" type="text" class="form-control" name="celular" value="<?php echo $result->celular; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label  class="control-label">Situação*</label>
                        <div class="controls">
                            <select name="situacao" class="form-control" id="situacao">
                                <?php if($result->situacao == 1){$ativo = 'selected'; $inativo = '';} else{$ativo = ''; $inativo = 'selected';} ?>
                                <option value="1" <?php echo $ativo; ?>>Ativo</option>
                                <option value="0" <?php echo $inativo; ?>>Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Permissões<span class="required">*</span></label>
                        <div class="controls">
                            <select name="permissoes_id" class="form-control" id="permissoes_id">
                                  <?php foreach ($permissoes as $p) {
                                     if($p->idPermissao == $result->permissoes_id){ $selected = 'selected';}else{$selected = '';}
                                      echo '<option value="'.$p->idPermissao.'"'.$selected.'>'.$p->nome.'</option>';
                                  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label  class="control-label">Filiais</label>
                        <div class="controls">
                        <p>
                            <span class="field">
                                   <?php
                                   foreach($filiais as $st){

                                       $verifica = $this->db->get_where('usuarios_filial' , array('usuario_id' => $result->idUsuarios , 'filial' => $st->idFilial))->result();

                                       if (count($verifica) > 0) {
                                           $usuario_filial = $this->db->get_where('usuarios_filial' , array('usuario_id' => $result->idUsuarios , 'filial' => $st->idFilial))->row()->idUsuariosFilial;
                                           ?>
                                           <?php echo '<a href="'.site_url('usuarios/remover_usuario_filial/'.$usuario_filial).'/'.$result->idUsuarios.'"> Já pode operar na filial '.$st->nome. '</a>'; ?><br/>
                                       <?php } else { ?>
                                           <input type="checkbox" name="filiais[]" value="<?php echo $st->idFilial; ?>"> <?php echo $st->nome; ?><br/>
                                       <?php } ?>
                                   <?php } ?>
                                </span>
                            <small class="desc">Selecione as filiais onde este usuário poderá operar, ou clique sobre a autorização para excluír a permissão.</small>
                        </p>
                        </div>
                    </div>
                    <div class="form-actions" style="text-align: right;">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Alterar</button>
                                <a href="<?php echo base_url() ?>index.php/usuarios" id="" class="btn btn-primary"><i class="fa fa-backward"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script  src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){

           $('#formUsuario').validate({
            rules : {
                    nome:{ required: true},
                    telefone:{ required: true},
                    usuariologin:{ required: true},
            },
            messages: {
                    nome :{ required: 'Campo Requerido.'},
                    telefone:{ required: 'Campo Requerido.'},
                    usuariologin:{ required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });

      });
</script>


