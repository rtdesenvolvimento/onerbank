<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-sitemap"></i> Usuários (<?php echo count($results); ?>)</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <a href="<?php echo base_url() ?>index.php/usuarios/adicionar" class="btn btn-success"><i
                                class="fa fa-save"></i> Adicionar Usuário</a>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                <tr style="backgroud-color: #2D335B">
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>Telefone</th>
                                    <th style="text-align: center;">Setor</th>
                                    <th style="text-align: center;">Nível</th>
                                    <th style="text-align: center;">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $r) {
                                    $nome_setor = '-';
                                    if ($r->setor_id) {
                                        $setor = $this->db->get_where('setor' , array('idSetor' => $r->setor_id ))->row();
                                        if (count($setor)) {
                                            $nome_setor = $setor->nome;
                                        }
                                    }
                                    echo '<tr>';
                                    echo '<td>' . $r->nome . '</td>';
                                    echo '<td>' . $r->cpf . '</td>';
                                    echo '<td>' . $r->telefone . '</td>';
                                    echo '<td style="text-align: center;">' . $nome_setor . '</td>';
                                    echo '<td style="text-align: center;">' . $r->permissao . '</td>';
                                    echo '<td style="text-align: center;">
                                      <a href="' . base_url() . 'index.php/usuarios/editar/' . $r->idUsuarios . '"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"> </i> </button></a>
                                  </td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
