<!-- Modal adicionar fotos ao veiculo -->
<div id="cadFotoVeiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Adicionar fotos do veículo</h3>
    </div>
    <div class="modal-body">
        <form id="formFotoVeiculo" enctype="multipart/form-data" action="javascript:;"
              accept-charset="utf-8" method="post">

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Observação</label>
                <textarea class="span12" rows="5" cols="5" id="observacaoimg" name="observacaoimg"></textarea>
            </div>

            <div class="span7">
                <input type="hidden" name="idVeiculoArquivo" id="idVeiculoArquivo" value=""/>
                <label for="">Fotos / Anexos*</label>
                <input type="file" class="span12" required="required" name="userfile[]" id="userfile" multiple="multiple" size="20"/>
            </div>

            <div class="span3">
                <label for="">.</label>
                <button class="btn btn-success span12"><i class="icon-white icon-plus"></i>Anexar</button>
            </div>
        </form>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div id="modal-anexos-veiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo-veiculo" style="text-align: center">
            <div class='progress progress-info progress-striped active'>
                <div class='bar' style='width: 100%'></div>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">LINK</label>
            <input type="text" id="link-visualizar" readonly name="link-visualizar"
                   class="span12"/>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">Observação</label>
            <textarea class="span12" rows="5" readonly cols="5" id="observacaoimg-visualizar" name="observacaoimg-visualizar"></textarea>
        </div>

        <div class="span12" style="margin-left: 0">
            <label for="data-visualizar">Data</label>
            <input type="date" id="data-visualizar" readonly name="data-visualizar"
                   class="span4"/>

            <label for="hora-visualizar">Hora</label>
            <input type="time"  id="hora-visualizar"  readonly name="hora-visualizar"
                   class="span4"/>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
        <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
        <a href="" target="_blank" class="btn btn-invers" id="abrir-imagem">Abrir</a>
        <a href="" link="" class="btn btn-danger" id="excluir-anexo-veiculo">Excluir Anexo</a>
    </div>
</div>

<form action="<?php echo current_url(); ?>" id="formVeiculosCliente" method="post" class="form-horizontal">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Cadastrar / Editar o Veículo</h3>
    </div>

    <div class="modal-body">

        <div class="span12" style="margin-left: 0">

            <div class="span6">
                <div class="formBuscaModealGSA">
                    <label for="placa">
                        Placa
                        <a href="#clienteveiculo" class="consulta_placa_click" style="float: right;margin-right: 4px;"><i class="icon-search tip-right"></i></i></a>
                    </label>
                    <input type="text" class="span12 consulta_placa" name="placa" required="required" id="placa"
                           placeholder="Digite a placa do veículo"/>
                </div>
            </div>

            <div class="span6">

                <div class="formBuscaModealGSA">
                    <label for="tipoVeiculo">Tipo do Veículo</label>

                    <select name="tipoveiculo_id" id="tipoveiculo_id" required="required" class="span12" required="required">
                        <option value="">selecione o tipo de veículo</option>
                        <?php foreach ($tiposveiculo as $tipoveiculo) { ?>
                            <option value="<?php echo $tipoveiculo->idTipoveiculo; ?>"><?php echo $tipoveiculo->nome?></option>
                        <?php  }?>
                    </select>
                    <input type="text" style="display: none;" class="span12" name="tipoVeiculo" id="tipoVeiculo"/>
                </div>
            </div>

        </div>

        <div class="span12" style="margin-left: 0">
            <div class="span6">
                <input type="hidden" name="idVeiculo" id="idVeiculo"/>
                <input type="hidden" name="idClienteProduto" id="idClienteProduto"
                       value=""/>
                <label for="">Modelo</label>
                <input type="text" class="span12" required="required" name="modelo" id="modelo"
                       placeholder=""/>
            </div>

            <div class="span6">
                <label for="marca">Marca</label>
                <input type="text" class="span12" required="required" name="marca" id="marca"
                       placeholder=""/>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">

            <div class="span3">
                <label for="cor">Cor</label>
                <input type="text" class="span12" name="cor" id="cor"
                       placeholder=""/>
            </div>

            <div class="span3">
                <label for="ano">Ano</label>
                <input type="number" class="span12" name="ano" id="ano"
                       placeholder=""/>
            </div>

            <div class="span3">
                <label for="municipio">Município</label>
                <input type="text" class="span12" name="municipio" id="municipio"
                       placeholder=""/>
            </div>

            <div class="span3">
                <label for="uf">UF</label>
                <input type="text" class="span12" name="uf" id="uf"
                       placeholder=""/>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">
            <br/>
            <div class="formBuscaGSA" style="padding-bottom: 9px;">
                <label for="placa">Tipos de combustível</label>
                <input type="checkbox" style="margin-top: -2px;" name="gasolina" id="gasolina" value="1">&nbsp;Gasolina&nbsp;&nbsp;&nbsp;
                <input type="checkbox" style="margin-top: -2px;" name="etanol" id="etanol" value="1">&nbsp;Etanol&nbsp;&nbsp;&nbsp;
                <input type="checkbox" style="margin-top: -2px;" name="diesel" id="diesel" value="1">&nbsp;Diesel&nbsp;&nbsp;&nbsp;
                <input type="checkbox" style="margin-top: -2px;" name="gnv" id="gnv" value="1">&nbsp;Gás veicular (GNV)&nbsp;&nbsp;&nbsp;
                <br/>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">
            <div class="span7">
                <label for="chassi">Chassi</label>
                <input type="text" class="span12" name="chassi" id="chassi"
                       placeholder="Digite o Chassi do veículo"/>
            </div>

            <div class="span5">
                <label for="chassi">Situação</label>
                <input type="text" readonly class="span12" name="situacao" id="situacao"
                       placeholder=""/>
            </div>
        </div>

        <div class="span12" style="padding: 1%; margin-left: 0">
            <div class="span12">
                <label for="observacao">Observação</label>
                <textarea class="span12"  id="observacaoVeiculo" name="observacao" cols="30" rows="5"></textarea>
            </div>
        </div>

        <div class="span12" style="margin-left: 0">
            <div class="span6 offset3" style="text-align: center">
                <button class="btn btn-success" id="btnAdicionarVeiculo"><i
                        class="icon-white icon-plus"></i> Salvar
                </button>
            </div>
        </div>

        <!--lista de veiculos -->
        <div class="span12" id="divVeiculos" style="margin-left: 0">
            <table class="table table-bordered" id="tblVeiculos">
                <thead>
                <tr>
                    <th style="text-align: left;">Veículo</th>
                    <th style="text-align: center;width:10%;">Ação</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){

        $('.consulta_placa').blur(function (event) {
            var placa = $(this).val();
            consulta_veiculo_sinesp(placa, BASE_URL);
        });

        $('.consulta_placa_click').click(function (event) {
            var placa = $('#placa').val();
            if (placa != '') {
                consulta_veiculo_sinesp(placa, BASE_URL);
            } else {
                alert('Digite uma placa para realizar a consulta.');
            }
        });

        $("#veiculo_id").blur(function (event) {
            consulta_situacao_veiculo($(this).val(), BASE_URL);
        });

        $('#tipoveiculo_id').change(function (event) {
            $('#tipoVeiculo').val( $( "#tipoveiculo_id option:selected" ).text());
        });

        $("#formVeiculosCliente" ).submit(function() {

            var cliente_id = $('#clientes_id');
            event.preventDefault();

            if (cliente_id) {
                $('#idClienteProduto').val(cliente_id.val());
                var dados = $("#formVeiculosCliente").serialize();

                var tipoCombustivel = false;

                if ($("#gasolina").is(":checked")){
                    tipoCombustivel = true;
                }

                if ($("#etanol").is(":checked")){
                    tipoCombustivel = true;
                }

                if ($("#diesel").is(":checked")){
                    tipoCombustivel = true;
                }

                if ($("#gnv").is(":checked")){
                    tipoCombustivel = true;
                }

                if (!tipoCombustivel) {
                    alert("Tipo de combustível é um campo obrigatório!");
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/adicionarVeiculo",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result === true) {
                            buscarVeiculo($('#clientes_id').val(),'<?php echo base_url();?>');
                            $('#situacao').css('background','#eee');
                            liberado = false;
                        } else {
                            alert('Ocorreu um erro ao tentar adicionar o veliculo.');
                        }
                    }
                });
            }
            return false;
        });

        $("#formFotoVeiculo").validate({
            submitHandler: function (form) {
                var dados = new FormData(form);
                $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/adicionarFotoVeiculo",
                    data: dados,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {

                        $("#userfile").val('');
                        $('#observacaoimg').val('');
                        $('#idVeiculoArquivo').val('');
                        $('#cadFotoVeiculo').modal('hide');

                        buscarVeiculo($('#clientes_id').val(), '<?php echo base_url();?>');
                    }
                });
                return false;
            }
        });
        $(document).on('click', '#excluir-anexo-veiculo', function (event) {
            event.preventDefault();
            var link = $(this).attr('link');
            $('#modal-anexos-veiculo').modal('hide');
            $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

            $.ajax({
                type: "POST",
                url: link,
                dataType: 'json',
                success: function (data) {
                    buscarVeiculo($('#clientes_id').val(), '<?php echo base_url();?>');
                }
            });
        });
    });
</script>