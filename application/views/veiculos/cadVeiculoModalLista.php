<table class="table table-bordered" id="tblProdutos">
    <thead>
    <tr>
        <th style="text-align: left;">Veículos</th>
        <th style="text-align: center;width:10%;">Ação</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($veiculos as $veiculo) {

        $strSituacao = '';
        $tipoCumbustivel = '';

        if($veiculo->situacao != 'Sem restrição') {
            $strSituacao = '<font color="red">'.$veiculo->situacao.'</font>';
        } else {
            $strSituacao = '<font color="#5bc0de">'.$veiculo->situacao.'</font>';
        }

        if ($veiculo->gasolina == 1) {
            $tipoCumbustivel = '<br/>Gasolina';
        }

        if ($veiculo->etanol == 1) {
            $tipoCumbustivel .= '<br/>Etanol';
        }

        if ($veiculo->diesel == 1) {
            $tipoCumbustivel .= '<br/>Diesel';
        }

        if ($veiculo->gnv == 1) {
            $tipoCumbustivel .= '<br/>Gás veicular (GNV)';
        }

        echo '<tr>';
        echo '<td style="text-align: left;">' .
            $veiculo->tipoVeiculo.' '.$veiculo->cor.'/'.$veiculo->ano.'<br/>'.
            'Placa: '. $veiculo->placa.
            '</br>Modelo: '.$veiculo->modelo.
            '<br/>Marca: '. $veiculo->marca.
            '<br/>Chassi: '. $veiculo->chassi.
            '<br/>Combustível(is):'.$tipoCumbustivel.
            '<br/>Localidade: '.$veiculo->municipio.'/'.$veiculo->uf.'<br/>'.$strSituacao.'<br/>';

        $anexos = $this->clientes_model->getAnexosByVeiculo($veiculo->idVeiculo);

        $cont = 1;
        $flag = 5;
        foreach ($anexos as $a) {

            if ($a->thumb == null) {
                $thumb = base_url() . 'assets/img/icon-file.png';
                $link = base_url() . 'assets/img/icon-file.png';
            } else {
                $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                $link = $a->url . $a->anexo;
            }

            if ($cont == $flag) {
                echo '<div style="margin-left: 0" class="span2"><a href="#modal-anexos-veiculo" onclick="exibirInformacoesAnexo(this)" style="width: 50%;height: 50%;" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'"  imagem="' . $a->idFotoVeiculo . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                $flag += 4;
            } else {
                echo '<div class="span2"><a href="#modal-anexos-veiculo" onclick="exibirInformacoesAnexo(this)" style="width: 50%;height: 50%;" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idFotoVeiculo . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
            }
            $cont++;
        }

        echo '</td> <td style="text-align: center;width:10%;">
                    <span onclick="excluir_editar_veiculo(this);" modelo="' . $veiculo->modelo . '" 
                          marca="' . $veiculo->marca . '"
                          placa="' . $veiculo->placa . '"
                          cor="' . $veiculo->cor . '"
                          municipio="' . $veiculo->municipio . '"
                          uf="' . $veiculo->uf . '"
                          situacao="' . $veiculo->situacao . '"
                          ano="' . $veiculo->ano . '"
                          chassi="' . $veiculo->chassi . '"
                          gasolina="' . $veiculo->gasolina . '"
                          etanol="' . $veiculo->etanol . '"
                          diesel="' . $veiculo->diesel . '"
                          gnv="' . $veiculo->gnv . '"
                          observacao="' . $veiculo->observacao . '"
                          tipoVeiculo="' . $veiculo->tipoVeiculo . '"
                          tipoveiculo_id="' . $veiculo->tipoveiculo_id . '"
                          idAcaoEditar="' . $veiculo->idVeiculo . '"
                        class="btn btn-info"><i class="icon-pencil icon-white"></i>
                        </span>
                        <a href="#cadFotoVeiculo" idVeiculo="'.$veiculo->idVeiculo.'" onclick="vincularIdentificadorVeiculo('.$veiculo->idVeiculo.')" data-toggle="modal">
                            <span class="btn"><i class="icon-picture icon-white"></i></span>
                        </a>
                    <span idAcao="' . $veiculo->idVeiculo . '" onclick="excluir_editar_veiculo(this);" class="btn btn-danger"><i class="icon-remove icon-white"></i></span>
                  </td>';
        echo '</tr>';
    } ?>
    </tbody>
</table>