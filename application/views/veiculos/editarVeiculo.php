<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro de veículo <small>Editar</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <ul class="nav nav-tabs">
                    <li class="active" ><a href="#tabVeiculos" data-toggle="tab">Veículos</a></li>
                    <li id="tabMauntencoesPreventivasAgendadas"><a href="#mauntencoesPreventivasAgendadas" data-toggle="tab">Manutenções Preventivas Agendadas</a></li>
                    <li id="tabHistoricoManutencaoPreventivas"><a href="#historicoManutencoesPreventivas" data-toggle="tab">Histórico de Manutenções Preventivas</a></li>
                </ul>
                <div class="tab-content">
                    <!--VEICULOS-->
                    <div class="tab-pane active" id="tabVeiculos">
                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                            <form id="formVeiculosCliente" action="#" method="post">
                                <?php echo form_hidden('veiculo_id', $veiculo->idVeiculo) ?>
                                <div class="col-md-6 col-sm-6">
                                    <label for="documento"> Documento </label>
                                    <input id="documento" disabled type="text" style="float: left;margin-right: 10px;" class="form-control"
                                           required="required" name="documento"
                                           value="<?php echo $result->documento; ?>"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="documento"> Cliente </label>
                                    <input id="nomeCliente"  disabled class="form-control" type="text"  required="required" name="nomeCliente"
                                           value="<?php echo $result->nomeCliente; ?>"/>
                                    <?php echo form_hidden('idClientes', $result->idClientes) ?>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="placa">
                                        Placa
                                        <a href="#Cliente" class="consulta_placa_click" style="float: right;margin-right: 4px;"><i class="icon-refresh tip-right"></i></i></a>
                                    </label>
                                    <input type="text" readonly class="form-control consulta_placa" name="placa" value="<?php echo $veiculo->placa;?>" required="required" id="placa"
                                           placeholder="Digite a placa do veículo"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="tipoVeiculo">Tipo do Veículo</label>
                                    <select name="tipoveiculo_id" id="tipoveiculo_id" required="required" class="form-control" required="required">
                                        <option value="">selecione o tipo de veículo</option>
                                        <?php foreach ($tiposveiculo as $tipoveiculo) { ?>
                                            <option <?php if($tipoveiculo->idTipoveiculo == $veiculo->tipoveiculo_id) echo 'selected="selected"';?> value="<?php echo $tipoveiculo->idTipoveiculo; ?>"><?php echo $tipoveiculo->nome?></option>
                                        <?php  }?>
                                    </select>
                                    <input type="text" style="display: none;" class="form-control" name="tipoVeiculo" value="<?php echo $veiculo->tipoVeiculo;?>" id="tipoVeiculo"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="hidden" name="idVeiculo" id="idVeiculo"/>
                                    <input type="hidden" name="idClienteProduto" id="idClienteProduto"
                                           value="<?php echo $result->idClientes ?>"/>
                                    <label for="">Modelo</label>
                                    <input type="text" class="form-control" required="required" name="modelo" value="<?php echo $veiculo->modelo;?>" id="modelo"
                                           placeholder="Digite o modelo do veículo"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="marca">Marca</label>
                                    <input type="text" class="form-control" value="<?php echo $veiculo->marca;?>" required="required" name="marca" id="marca"
                                           placeholder="Digite a marca do veículo"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="cor">Cor</label>
                                    <input type="text" class="form-control" value="<?php echo $veiculo->cor;?>" name="cor" id="cor"
                                           placeholder="Digite a cor do veículo"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="ano">Ano</label>
                                    <input type="number" class="form-control" name="ano" value="<?php echo $veiculo->ano;?>" id="ano"
                                           placeholder="Digite o ano do veículo"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="municipio">Município</label>
                                    <input type="text" class="form-control" value="<?php echo $veiculo->municipio;?>" name="municipio" id="municipio"
                                           placeholder="Digite o município"/>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label for="uf">uf</label>
                                    <input type="text" class="form-control" name="uf" value="<?php echo $veiculo->uf;?>" id="uf"
                                           placeholder="Digite a uf"/>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <label for="chassi">Chassi</label>
                                    <input type="text" class="form-control" name="chassi" value="<?php echo $veiculo->chassi;?>" id="chassi"
                                           placeholder="Digite o Chassi do veículo"/>
                                </div>

                                <div class="col-md-12 col-sm-12" style="margin-left: 0">
                                    <div class="formBuscaGSA" style="padding-bottom: 9px;">
                                        <label for="placa">Tipos de combustível</label>
                                        <input type="checkbox" <?php if($veiculo->gasolina == 1) echo 'checked="checked"';?> style="margin-top: -2px;" name="gasolina" id="gasolina" value="1">&nbsp;Gasolina&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" <?php if($veiculo->etanol == 1) echo 'checked="checked"';?> style="margin-top: -2px;" name="etanol" id="etanol" value="1">&nbsp;Etanol&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" <?php if($veiculo->diesel == 1) echo 'checked="checked"';?> style="margin-top: -2px;" name="diesel" id="diesel" value="1">&nbsp;Diesel&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" <?php if($veiculo->gnv == 1) echo 'checked="checked"';?> style="margin-top: -2px;" name="gnv" id="gnv" value="1">&nbsp;Gás veicular (GNV)&nbsp;&nbsp;&nbsp;
                                        <br/>
                                    </div>
                                </div>
                                <div class="span12" style="margin-left: 0">
                                    <div class="formBuscaGSA">
                                        <label for="situacao">Situação</label>
                                        <input type="text" readonly style="font-size: 16px;" class="form-control" value="<?php echo $veiculo->situacao;?>" name="situacao" id="situacao"
                                               placeholder=""/>
                                    </div>
                                </div>
                                <div class="span12" style="margin-left: 0">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#manutencaoPreventiva" data-toggle="tab">Manutenção Preventiva</a></li>
                                        <li><a href="#observacao" data-toggle="tab">Observação</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="manutencaoPreventiva">
                                            <div class="span12" style="padding: 1%; margin-left: 0">
                                                <table class="table table-bordered" id="tblManutencaoPreventiva">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Serviço</th>
                                                        <th>Peça</th>
                                                        <th>Tipo de Manutenção</th>
                                                        <th>Período</th>
                                                        <th>Dt. Inical Preventiva</th>
                                                        <th>Situação</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $contador= 1;
                                                    foreach ($manutencoes as $manutencao) {?>
                                                        <?php if ($manutencao->indice == $contador){?>
                                                            <tr>
                                                                <td><?php echo $contador;?></td>
                                                                <td>
                                                                    <select name="servico_id[]" class="form-control">
                                                                        <option value="">Selecione um serviço</option>
                                                                        <?php foreach($servicos as $servico){ ?>
                                                                            <option <?php if($manutencao->servico_id == $servico->idServicos) echo 'selected="selected"';?> value="<?php echo $servico->idServicos;?>"><?php echo $servico->nome;?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="peca_id[]" class="form-control">
                                                                        <option value="">Selecione uma peça</option>
                                                                        <?php foreach($pecas as $peca){ ?>
                                                                            <option <?php if($manutencao->peca_id == $peca->idProdutos) echo 'selected="selected"';?> value="<?php echo $peca->idProdutos;?>"><?php echo $peca->descricao;?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="tipoManutencao[]" class="form-control">
                                                                        <option <?php if($manutencao->tipoManutencao == 1) echo 'selected="selected"';?> value="1">Preventiva em dias</option>
                                                                        <option <?php if($manutencao->tipoManutencao == 2) echo 'selected="selected"';?> value="2">Preventiva em meses</option>
                                                                        <option <?php if($manutencao->tipoManutencao == 3) echo 'selected="selected"';?> value="3">Preventiva em anos</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input id="periodo" class="form-control" type="text" name="periodo[]" value="<?php echo $manutencao->periodo;?>"/>
                                                                </td>
                                                                <td>
                                                                    <input id="dataInicialPreventiva" class="form-control" type="date" name="dataInicialPreventiva[]" value="<?php echo $manutencao->dataInicialPreventiva;?>"/>
                                                                </td>
                                                                <td>
                                                                    <select name="situacao[]" disabled class="form-control">
                                                                        <option <?php if($manutencao->situacao == 1) echo 'selected="selected"';?> value="1">ABERTA</option>
                                                                        <option <?php if($manutencao->situacao == 2) echo 'selected="selected"';?> value="2">MANUTENÇÃO</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        <?php }?>
                                                        <?php $contador = $contador+1;} ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="observacao">
                                            <div class="span12 well" style="padding: 1%; margin-left: 0">
                                                <textarea class="form-control"  id="observacao" name="observacao" cols="30" rows="5"><?php echo $veiculo->observacao;?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span12" style="text-align: right;">
                                    <div class="span6 offset3">
                                        <button class="btn btn-success" id="btnAdicionarProduto"><i
                                                class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="span12" id="divVeiculos" style="margin-left: 0">
                            <table class="table table-bordered" id="tblProdutos">
                                <thead>
                                <tr>
                                    <th style="text-align: left;">Veículos</th>
                                    <th style="text-align: center;width:10%;">Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($veiculos as $veiculo) {

                                    $strSituacao = '';
                                    $tipoCumbustivel = '';

                                    if($veiculo->situacao != 'Sem restrição') {
                                        $strSituacao = '<font color="red">'.$veiculo->situacao.'</font>';
                                    } else {
                                        $strSituacao = '<font color="#5bc0de">'.$veiculo->situacao.'</font>';
                                    }

                                    if ($veiculo->gasolina == 1) {
                                        $tipoCumbustivel = '<br/>Gasolina';
                                    }

                                    if ($veiculo->etanol == 1) {
                                        $tipoCumbustivel .= '<br/>Etanol';
                                    }

                                    if ($veiculo->diesel == 1) {
                                        $tipoCumbustivel .= '<br/>Diesel';
                                    }

                                    if ($veiculo->gnv == 1) {
                                        $tipoCumbustivel .= '<br/>Gás veicular (GNV)';
                                    }

                                    echo '<tr>';
                                    echo '<td style="text-align: left;">' .
                                        $veiculo->tipoVeiculo.' '.$veiculo->cor.'/'.$veiculo->ano.'<br/>'.
                                        'Placa: '. $veiculo->placa.
                                        '</br>Modelo: '.$veiculo->modelo.
                                        '<br/>Marca: '. $veiculo->marca.
                                        '<br/>Chassi: '. $veiculo->chassi.
                                        '<br/>Combustível(is):'.$tipoCumbustivel.
                                        '<br/>Localidade: '.$veiculo->municipio.'/'.$veiculo->uf.'<br/>'.$strSituacao.'<br/>';

                                    $anexos = $this->clientes_model->getAnexosByVeiculo($veiculo->idVeiculo);

                                    $cont = 1;
                                    $flag = 5;
                                    foreach ($anexos as $a) {

                                        if ($a->thumb == null) {
                                            $thumb = base_url() . 'assets/img/icon-file.png';
                                            $link = base_url() . 'assets/img/icon-file.png';
                                        } else {
                                            $thumb = base_url() . 'assets/anexos/thumbs/' . $a->thumb;
                                            $link = $a->url . $a->anexo;
                                        }

                                        if ($cont == $flag) {
                                            echo '<div style="margin-left: 0" class="span2"><a href="#modal-anexo" style="width: 50%;height: 50%;" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'"  imagem="' . $a->idFotoVeiculo . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                            $flag += 4;
                                        } else {
                                            echo '<div class="span2"><a href="#modal-anexo" style="width: 50%;height: 50%;" hora="'.$a->hora.'" data="'.$a->data.'" observacaoimg="'.$a->observacaoimg.'" imagem="' . $a->idFotoVeiculo . '" link="' . $link . '" role="button" class="btn anexo" data-toggle="modal"><img src="' . $thumb . '" alt=""></a></div>';
                                        }
                                        $cont++;
                                    }

                                    echo '</td> <td style="text-align: center;width:10%;">
                                              
                                                    <a href="#cadFotoVeiculo" idVeiculo="'.$veiculo->idVeiculo.'" onclick="vincularIdentificadorVeiculo('.$veiculo->idVeiculo.')" data-toggle="modal">
                                                        <span class="btn"><i class="icon-picture icon-white"></i></span>
                                                    </a>
                                               </td>';
                                    echo '</tr>';
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="mauntencoesPreventivasAgendadas">
                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                            <div id="div_mauntencoesPreventivasAgendadas"> </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="historicoManutencoesPreventivas">
                        <div class="span12 well" style="padding: 1%; margin-left: 0">
                            <div id="div_historicoManutencoesPreventivas"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal adicionar fotos ao veiculo -->
<div id="cadFotoVeiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Adicionar fotos do veículo</h3>
    </div>
    <div class="modal-body">
        <form id="formFotoVeiculo" enctype="multipart/form-data" action="javascript:;"
              accept-charset="utf-8" method="post">

            <div class="span12" style="margin-left: 0">
                <label for="descricao">Observação</label>
                <textarea class="span12" rows="5" cols="5" id="observacaoimg" name="observacaoimg"></textarea>
            </div>

            <div class="span7">
                <input type="hidden" name="idVeiculoArquivo" id="idVeiculoArquivo" value=""/>
                <label for="">Fotos / Anexos*</label>
                <input type="file" class="span12" required="required" name="userfile[]" id="userfile" multiple="multiple" size="20"/>
            </div>

            <div class="span3">
                <label for="">.</label>
                <button class="btn btn-success span12"><i class="icon-white icon-plus"></i>Anexar</button>
            </div>
        </form>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo" style="text-align: center">
            <div class='progress progress-info progress-striped active'>
                <div class='bar' style='width: 100%'></div>
            </div>
        </div>
        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">LINK</label>
            <input type="text" id="link-visualizar" readonly name="link-visualizar"
                   class="span12"/>
        </div>
        <div class="span12" style="margin-left: 0">
            <label for="observacaoimg-visualizar">Observação</label>
            <textarea class="span12" rows="5" readonly cols="5" id="observacaoimg-visualizar" name="observacaoimg-visualizar"></textarea>
        </div>
        <div class="span12" style="margin-left: 0">
            <label for="data-visualizar">Data</label>
            <input type="date" id="data-visualizar" readonly name="data-visualizar"
                   class="span4"/>

            <label for="hora-visualizar">Hora</label>
            <input type="time"  id="hora-visualizar"  readonly name="hora-visualizar"
                   class="span4"/>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
        <a href="" id-imagem="" class="btn btn-inverse" id="download">Download</a>
        <a href="" target="_blank" class="btn btn-invers" id="abrir-imagem">Abrir</a>
        <a href="" link="" class="btn btn-danger" id="excluir-anexo">Excluir Anexo</a>
    </div>
</div>

<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/veiculo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $('#tabMauntencoesPreventivasAgendadas').click(function (event) {
            $('#div_mauntencoesPreventivasAgendadas').load('<?php echo base_url();?>index.php/programacaomanutencaopreventivaveiculo/iframeManutencaoPreventivaAgendada/<?php echo $veiculo->idVeiculo;?>');
        });

        $('#tabHistoricoManutencaoPreventivas').click(function (event) {
            $('#div_historicoManutencoesPreventivas').load('<?php echo base_url();?>index.php/programacaomanutencaopreventivaveiculo/iframeManutencaoPreventivaHistorico/<?php echo $veiculo->idVeiculo;?>');
        });

        $('#tipoveiculo_id').change(function (event) {
            $('#tipoVeiculo').val( $( "#tipoveiculo_id option:selected" ).text());
        });

        $("#formFotoVeiculo").validate({
            submitHandler: function (form) {
                var dados = new FormData(form);
                $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/adicionarFotoVeiculo",
                    data: dados,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divVeiculos").load("<?php echo current_url();?> #divVeiculos");
                            $("#userfile").val('');
                            $('#observacaoimg').val('');
                            $('#idVeiculoArquivo').val('');
                            $('#cadFotoVeiculo').modal('hide');
                        }
                        else {
                            $("#divVeiculos").html('<div class="alert fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> ' + data.mensagem + '</div>');
                        }
                    },
                    error: function () {
                        $("#divVeiculos").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><strong>Atenção!</strong> Ocorreu um erro. Verifique se você anexou o(s) arquivo(s).</div>');
                    }
                });
                return false;
            }
        });

        $(document).on('click', '.anexo', function (event) {
            event.preventDefault();
            var link            = $(this).attr('link');
            var id              = $(this).attr('imagem');
            var url             = '<?php echo base_url(); ?>index.php/clientes/excluirAnexo/';
            var data            =  $(this).attr('data');
            var hora            =  $(this).attr('hora');
            var observacaoimg   =  $(this).attr('observacaoimg');

            $("#div-visualizar-anexo").html('<img src="' + link + '" alt="">');
            $("#excluir-anexo").attr('link', url + id);

            $('#link-visualizar').val(link);
            $('#observacaoimg-visualizar').val(observacaoimg);
            $('#data-visualizar').val(data);
            $('#hora-visualizar').val(hora);
            $('#abrir-imagem').attr('href',link);
            $("#download").attr('href', "<?php echo base_url(); ?>index.php/clientes/downloadanexo/" + id);
        });

        $(document).on('click', '#excluir-anexo', function (event) {
            event.preventDefault();
            var link = $(this).attr('link');
            $('#modal-anexo').modal('hide');
            $("#divVeiculos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");

            $.ajax({
                type: "POST",
                url: link,
                dataType: 'json',
                success: function (data) {
                    if (data.result == true) {
                        $("#divVeiculos").load("<?php echo current_url();?> #divVeiculos");
                    } else {
                        alert(data.mensagem);
                    }
                }
            });
        });

        $('.consulta_placa').blur(function (event) {
            var placa = $(this).val();
            consulta_veiculo_sinesp(placa,'<?php echo base_url();?>');
        });

        $('.consulta_placa_click').click(function (event) {
            var placa = $('#placa').val();
            if (placa != '') {
                consulta_veiculo_sinesp(placa,'<?php echo base_url();?>');
            } else {
                alert('Digite uma placa para realizar a consulta.');
            }
        });
    });

    function vincularIdentificadorVeiculo(idVeiculo) {
        $('#idVeiculoArquivo').val(idVeiculo);
    }

</script>

