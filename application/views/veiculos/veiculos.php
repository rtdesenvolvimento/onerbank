<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-car"></i> Veículos (<?php echo count($results); ?>)</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered bulk_action" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Cliente</th>
                                        <th style="text-align: left;">Tipo</th>
                                        <th style="text-align: left;">Modelo</th>
                                        <th style="text-align: left;">Marca</th>
                                        <th style="text-align: center;">Placa</th>
                                        <th style="text-align: center;">Chassi</th>
                                        <th style="text-align: center;">Cor</th>
                                        <th style="text-align: center;">Ano</th>
                                        <th style="text-align: center;">Combustível(is)</th>
                                        <th style="text-align: center;">Acao</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($results as $r) {

                                        $combustivel = '';

                                        if ($r->gasolina==1){
                                            $combustivel = 'Gasolina, ';
                                        }

                                        if ($r->etanol==1){
                                            $combustivel .= 'Etanol, ';
                                        }

                                        if ($r->diesel==1){
                                            $combustivel .= 'Diesel, ';
                                        }

                                        if ($r->gnv==1){
                                            $combustivel .= 'Gás veicular (GNV), ';
                                        }

                                         echo '<tr>';
                                            echo '<td style="text-align: left;">'.$r->nomeCliente.'</td>';
                                            echo '<td style="text-align: left;">'.$r->tipoVeiculo.'</td>';
                                            echo '<td style="text-align: left;">'.$r->modelo.'</td>';
                                            echo '<td style="text-align: left;">'.$r->marca.'</td>';
                                            echo '<td style="text-align: center;">'.$r->placa.'</td>';
                                            echo '<td style="text-align: center;">'.$r->chassi.'</td>';
                                            echo '<td style="text-align: center;">'.$r->cor.'</td>';
                                            echo '<td style="text-align: center;">'.$r->ano.'</td>';
                                            echo '<td style="text-align: center;">'.$combustivel.'</td>';
                                            echo '<td align=center>';

                                            if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){
                                                echo '<a style="margin-right: 1%" href="'.base_url().'index.php/veiculos/visualizar/'.$r->cliente_id.'/'.$r->idVeiculo.'" class="btn tip-top"><i class="icon-eye-open"></i></a>';
                                            }

                                            if($this->permission->checkPermission($this->session->userdata('permissao'),'eCliente')){
                                                echo '<a href="'.base_url().'index.php/veiculos/editar/'.$r->cliente_id.'/'.$r->idVeiculo.'"><button type="button" class="btn btn-primary btn-sm"><i  style="color: #ffffff" class="fa fa-edit"></i></a>';
                                            }

                                            echo  '</td>';
                                        echo '</tr>';
                                    }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo base_url() ?>index.php/os/excluir" method="post" >
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 id="myModalLabel">Excluir OS</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="idOs" name="id" value="" />
    <h5 style="text-align: center">Deseja realmente excluir esta OS?</h5>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-danger">Excluir</button>
  </div>
  </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$( "#buscar" ).keypress(function( event ) {
		  if ( event.which === 13 ) {
		      window.location.href = "<?php echo base_url() ?>index.php/veiculos/buscar/" + $(this).val();
		  }
	});

   $(document).on('click', 'a', function(event) {
        var os = $(this).attr('os');
        $('#idOs').val(os);
    });
});
</script>