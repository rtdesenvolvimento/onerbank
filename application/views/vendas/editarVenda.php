<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-tags"></i>
                </span>
                <h5>Editar Venda</h5>
            </div>
            <div class="widget-content nopadding">


                <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                    <ul class="nav nav-tabs">
                        <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da Venda</a></li>
                        <!--<li class="active" id="tabDetalhesDaNota"><a href="#tab1" data-toggle="tab">Detalhes da Nota</a></li>!-->

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">

                            <div class="span12" id="divEditarVenda">

                                <form action="<?php echo current_url(); ?>" method="post" id="formVendas">
                                    <?php echo form_hidden('idVendas', $result->idVendas) ?>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <h3>#Venda: <?php echo $result->idVendas ?></h3>


                                        <div class="span12 well" style="margin-left: 0">

                                            <div class="span2">
                                                <div class="formBuscaGSA">
                                                    <label for="dataInicial">Data da Venda<span class="required">*</span></label>
                                                    <input id="dataVenda" class="span12" type="date" required="required" name="dataVenda" value="<?php echo $result->dataVenda;?>"  />
                                                </div>
                                            </div>

                                            <div class="span8">
                                                <div class="formBuscaGSA" id="divBuscaCliente" style="<?php if ($result->inativo == 1) echo  'background: linear-gradient(rgb(234, 31, 31) 1%, rgb(255, 255, 255) 100%);';?>">
                                                    <label for="cliente">
                                                        <a href="#searchCliente" id="aSearchCliente" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                                        Cliente<span class="required"></span>
                                                        <a href="#adicionarCliente" id="addCliente" data-toggle="modal" style="float: right;margin-right: 4px;"><i id="pincliente" class="icon-edit tip-right"></i></i></a>
                                                    </label>
                                                    <div class="controls">
                                                        <select name="clientes_id" style="<?php if ($result->inativo == 1) echo  'background: #ee5f5b00';?>" class="span12" id="clientes_id" required="required">
                                                            <option value="">--selecione um cliente--</option>
                                                            <option  selected="selected" value="<?php echo $result->clientes_id; ?>"><?php echo $result->nomeCliente; ?></option>
                                                        </select>
                                                        <?php if ($result->inativo == 1) { ?>
                                                            <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;">Cliente com restrições: <?php echo $result->observacaoInativo; ?></div>
                                                        <?php } else { ?>
                                                            <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;display: none;"></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="span2">
                                                <div class="formBuscaGSA">
                                                    <label for="tecnico">Responsável<span
                                                                class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="usuarios_id" class="span12" id="usuarios_id" required="required">
                                                            <option value="">--Selecione um cliente--</option>
                                                            <?php foreach ($usuarios as $usuario) {?>
                                                                <option <?php if ($result->usuarios_id == $usuario->idUsuarios) echo 'selected="selected"'; ?> value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="span8 offset2" style="text-align: center">

                                                <?php if ($result->faturado == 0) { ?>
                                                    <button class="btn btn-primary" id="btnContinuar"><i
                                                                class="icon-white icon-ok"></i> Alterar
                                                    </button>
                                                <?php } ?>

                                                <?php if ($result->faturado == 0) { ?>
                                                    <a href="#modal-faturar" id="btn-faturar" role="button"
                                                       data-toggle="modal" class="btn btn-success"><i class="icon-file"></i>
                                                        Faturar</a>
                                                <?php } else { ?>
                                                    <!---
                                                    <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Gerar NF-e</a>
                                                    <a href="#modal-faturar" id="btn-faturar" role="button" data-toggle="modal" class="btn btn-success"><i class="icon-file"></i> Enviar NFe Por E-mail</a>
                                                    1-->
                                                    <!--
                                                    <a href="<?php echo base_url() ?>index.php/vendas/recibo/<?php echo $result->idVendas; ?>" class="btn btn-inverse"><i class="icon-eye-open"></i> Recibo</a>
                                                    -->
                                                <?php } ?>

                                                <a href="<?php echo base_url() ?>index.php/vendas/visualizar/<?php echo $result->idVendas; ?>"
                                                   class="btn btn-inverse"><i class="icon-eye-open"></i> Visualizar
                                                    Venda</a>
                                                <a href="<?php echo base_url() ?>index.php/vendas" class="btn"><i
                                                            class="icon-arrow-left"></i> Voltar</a>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                                <div class="span12 well" style="padding: 1%; margin-left: 0">

                                    <form id="formProdutos"
                                          action="<?php echo base_url(); ?>index.php/vendas/adicionarProduto"
                                          method="post">
                                        <div class="span4">
                                            <input type="hidden" name="idProduto" id="idProduto"/>
                                            <input type="hidden" name="idVendasProduto" id="idVendasProduto"
                                                   value="<?php echo $result->idVendas ?>"/>
                                            <input type="hidden" name="estoque" id="estoque" value=""/>
                                            <input type="hidden" name="preco" id="preco" value=""/>

                                            <div class="formBuscaGSA">
                                                <label for="">
                                                    Produto
                                                    <a href="#modal-consulta-estoque" id="addConsultaProduto" data-toggle="modal">
                                                        <i id="pincliente" style="float: right;margin-right: 10px;" class="icon-bar-chart icon-white"></i></a>
                                                </label>

                                                <div class="controls">
                                                    <select name="produto" class="span12" id="produto" required="required">
                                                        <option value="">--Selecione um cliente--</option>
                                                        <?php foreach ($produtosList as $produto) {?>
                                                            <option value="<?php echo $produto->idProdutos; ?>"><?php echo $produto->descricao; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="span2">
                                            <label for="">Quantidade</label>
                                            <input type="text" placeholder="Quantidade" id="quantidade"
                                                   name="quantidade" class="span12"/>
                                        </div>

                                        <div class="span2">
                                            <label for="">Preço de custo</label>
                                            <input type="text" placeholder="Custo" id="custo" name="custo"
                                                   class="span12"/>
                                        </div>

                                        <div class="span2">
                                            <label for="">Preço de venda</label>
                                            <input type="text" placeholder="Venda" id="valorVenda" name="valorVenda"
                                                   class="span12"/>
                                        </div>

                                        <div class="span2">
                                            <label for="">&nbsp</label>
                                            <?php if ($result->faturado == 1) { ?>
                                                <button class="btn btn-success span12" disabled
                                                        id="btnAdicionarProduto"><i class="icon-white icon-plus"></i>
                                                    Adicionar
                                                </button>
                                            <?php } else { ?>
                                                <button class="btn btn-success span12" id="btnAdicionarProduto"><i
                                                            class="icon-white icon-plus"></i> Adicionar
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </form>

                                    <div class="span12" id="divProdutos" style="margin-left: 0">
                                        <table class="table table-bordered" id="tblProdutos">
                                            <thead>
                                            <tr>
                                                <th>Produto</th>
                                                <th>Quantidade</th>
                                                <th>Custo</th>
                                                <th>Valor</th>
                                                <th>Sub-total</th>
                                                <th>Ações</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $total = 0;
                                            $custo = 0;

                                            foreach ($produtos as $p) {

                                                $total = $total + $p->subTotal;
                                                $custo = $custo + $p->custo;

                                                echo '<tr>';
                                                echo '<td>' . $p->descricao . '</td>';
                                                echo '<td>' . $p->quantidade . '</td>';
                                                echo '<td>' . $p->custo . '</td>';
                                                echo '<td>' . $p->valorVenda . '</td>';

                                                echo '<td>R$ ' . number_format($p->subTotal, 2, ',', '.') . '</td>';
                                                if ($result->faturado == 0) {
                                                    echo '<td><a href="" idAcao="' . $p->idItens . '" prodAcao="' . $p->idProdutos . '" quantAcao="' . $p->quantidade . '" class="btn btn-danger"><i class="icon-remove icon-white"></i></a></td>';
                                                }
                                                echo '</tr>';
                                            } ?>

                                            <tr>
                                                <td colspan="4" style="text-align: right"><strong>Total:</strong></td>
                                                <td>
                                                    <strong>
                                                        R$ <?php echo number_format($total, 2, ',', '.'); ?>
                                                    </strong>
                                                    <input type="hidden" id="total-venda"
                                                           value="<?php echo number_format($total, 2); ?>">
                                                    <input type="hidden" id="total-custo"
                                                           value="<?php echo number_format($custo, 2); ?>">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal searchCliente -->
<div id="searchCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchCliente"></div>
</div>

<!-- Modal Faturar-->
<div id="modal-faturar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form id="formFaturar" action="<?php echo current_url() ?>" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Faturar Venda</h3>
        </div>
        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>
            <div class="span12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <input class="span12" id="descricao" type="text" name="descricao"
                       value="Fatura de Venda - #<?php echo $result->idVendas; ?> "/>

            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span12" style="margin-left: 0">
                    <label for="cliente">Cliente*</label>
                    <input class="span12" id="cliente" type="text" name="cliente"
                           value="<?php echo $result->nomeCliente ?>"/>
                    <input type="hidden" name="clientes_id" id="clientes_id" value="<?php echo $result->clientes_id ?>">
                    <input type="hidden" name="vendas_id" id="vendas_id" value="<?php echo $result->idVendas; ?>">
                </div>


            </div>
            <div class="span12" style="margin-left: 0">
                <div class="span4">
                    <label for="valor">Valor*</label>
                    <input type="hidden" id="tipo" name="tipo" value="receita"/>
                    <input class="span12 money" id="valor" type="text" name="valor"
                           value="<?php echo number_format($total, 2); ?> "/>
                </div>

                <div class="span4">
                    <label for="valor">Custo*</label>
                    <input class="span12 money" id="custoVenda" type="text" name="custoVenda"
                           value="<?php echo number_format($custo, 2); ?> "/>
                </div>

                <div class="span4">
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="span12 datepicker" id="vencimento" type="text" name="vencimento"/>
                </div>

            </div>

            <div class="span12" style="margin-left: 0">
                <div class="span4" style="margin-left: 0">
                    <label for="recebido">Recebido?</label>
                    &nbsp &nbsp &nbsp &nbsp<input id="recebido" type="checkbox" name="recebido" value="1"/>
                </div>
                <div id="divRecebimento" class="span8" style=" display: none">
                    <div class="span6">
                        <label for="recebimento">Data Recebimento</label>
                        <input class="span12 datepicker" id="recebimento" type="text" name="recebimento"/>
                    </div>
                    <div class="span6">
                        <label for="formaPgto">Forma Pgto</label>
                        <select name="formaPgto" id="formaPgto" class="span12">
                            <option value="Dinheiro">Dinheiro</option>
                            <option value="Cartão de Crédito">Cartão de Crédito</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Boleto">Boleto</option>
                            <option value="Depósito">Depósito</option>
                            <option value="Débito">Débito</option>
                        </select>
                    </div>
                </div>

            </div>


        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" id="btn-cancelar-faturar">Cancelar</button>
            <button class="btn btn-primary">Faturar</button>
        </div>
    </form>
</div>

<!-- Modal cadastrar novo cliente -->
<div id="adicionarCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Cadastrar / Editar o cliente</h3>
        </div>

        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span4">
                    <label for="tipoPessoa">Tipo de pessoa</label>
                    <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                        <option value="PF">Pessoa Física</option>
                        <option value="PJ">Pessoa Jurídica</option>
                    </select>
                </div>

                <div class="span4">
                    <label for="origem">Origem</label>
                    <select name="origem" id="origem" class="span12 chzn" required="required">
                        <option value="">selecione uma origem</option>
                        <option value="Particular">Particular</option>
                        <option value="Financeira">Financeira</option>
                        <option value="Seguradora">Seguradora</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span6">
                    <label for="nomeCliente" >Nome / Razão Social<span class="required">*</span></label>
                    <input id="nomeCliente"  class="span12" type="text" required="required" name="nomeCliente"
                           value="<?php echo set_value('nomeCliente'); ?>"/>

                    <input type="hidden" id="cliente_id" name="cliente_id" value="" />
                </div>

                <div class="span6">
                    <label for="nomeFantasiaApelido" >Nome Fantasia / Apelido</label>
                    <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                           value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                </div>


            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span8">
                    <label for="documento">CPF/CNPJ*</label>
                    <input id="documento" type="text" class="span12" required="required" name="documento"
                           value="<?php echo set_value('documento'); ?>"/>
                </div>

                <div class="span4" id="div_sexo">
                    <label for="sexo">Sexo</label>
                    <select name="sexo" id="sexo" class="span12 chzn" required="required">
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0" id="div_rg">
                <div class="span3">
                    <label for="rg">RG</label>
                    <input id="rg" type="text" class="span12" name="rg"
                           value="<?php echo set_value('rg'); ?>"/>
                    </select>
                </div>

                <div class="span3">
                    <label for="orgaoEmissor">Orgão Emissor</label>
                    <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                           value="<?php echo set_value('orgaoEmissor'); ?>"/>
                </div>

                <div class="span2">
                    <label for="estadoOrgaoEmissor">Estado</label>
                    <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                           value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                </div>

                <div class="span4">
                    <label for="dataOrgaoEmissor">Data Emissão</label>
                    <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                           value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="telefone">Telefone</label>
                    <input id="telefone" type="text" class="span12" name="telefone"
                           value="<?php echo set_value('telefone'); ?>"/>
                </div>

                <div class="span6">
                    <label for="celular">Celular</label>
                    <input id="celular" type="text" class="span12" name="celular"
                           value="<?php echo set_value('celular'); ?>"/>
                </div>



            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="span12" name="email"
                           value="<?php echo set_value('email'); ?>"/>
                </div>

                <div class="span6">
                    <label for="email">Data de nascimento</label>
                    <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                           value="<?php echo set_value('data_nascimento'); ?>"/>
                </div>
            </div>


            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Dados do endereço
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span3">
                    <label for="cep">CEP</label>
                    <input id="cep" type="text" name="cep" class="span12" onBlur="getConsultaCEP();"
                           value="<?php echo set_value('cep'); ?>"/>
                    <small>[TAB] consulta cep (Necessita Internet)</small>
                </div>

                <div class="span6">
                    <label for="rua">Rua</label>
                    <input id="rua" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                </div>

                <div class="span3">
                    <label for="numero">Número</label>
                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <label for="complemento">Complemento</label>
                    <input id="complemento" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span5">
                    <label for="bairro">Bairro</label>
                    <input id="bairro" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                </div>

                <div class="span4">
                    <label for="cidade">Cidade</label>
                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                </div>
                <div class="span3">
                    <label for="estado">Estado</label>
                    <input id="estado" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                </div>
            </div>

            <div id="div_contato" style="display: none;">
                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes">
                        <a href="#tab1" data-toggle="tab">
                            Dados do  Contato
                        </a>
                    </li>
                </ul>
                <div class="span12" style="padding: 1%; margin-left: 0">

                    <div class="span12">
                        <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                        <input id="contatoNomeCliente"  class="span12" type="text" name="contatoNomeCliente"
                               value="<?php echo set_value('contatoNomeCliente'); ?>"/>
                    </div>

                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span8">
                        <label for="contatoCpf">CPF</label>
                        <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                               value="<?php echo set_value('contatoCpf'); ?>"/>
                    </div>

                    <div class="span4">
                        <label for="contatoSexo">Sexo</label>
                        <select name="contatoSexo" id="contatoSexo" class="span12 chzn">
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                        </select>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="contatoTelefone">Telefone</label>
                        <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                               value="<?php echo set_value('contatoTelefone'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="contatoCelular">Celular</label>
                        <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                               value="<?php echo set_value('contatoCelular'); ?>"/>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="email">Email</label>
                        <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                               value="<?php echo set_value('contatoEmail'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="email">Data de nascimento</label>
                        <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                               value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Senha para acessar o portal do cliente
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <label for="senha">Senha</label>
                <input type="password" name="senha" class="span12" id="senha"
                       value=""/>
                <small>Deixe em branco e a senha padrão será o (cpj/cnpj) do cliente</small>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Observação
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <textarea class="span12" id="observacao"  name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6 offset3" style="text-align: center">
                    <button type="submit" class="btn btn-success">
                        <div id="div_buttom_adicionar">Editar</div>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- Modal consulta de estoque -->
<div id="modal-consulta-estoque" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form  action="<?php echo base_url(); ?>index.php/os/enviarEmail" method="post">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Consulta de Estoque</h3>
        </div>
        <div class="modal-body">

            <div class="span12" style="padding: 1%; margin-left: 0;text-align: center;">
                <label for="senha"><div id="consultaEstoqueNomeProduto"></div></label>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <table class="table table-bordered" id="tblConsultaEstoque">
                    <thead>
                    <tr style="backgroud-color: #2D335B">
                        <th style="text-align: left;">Filial</th>
                        <th style="width: 10%;text-align: right;">Estoque</th>
                        <!--
                        <th style="width: 10%;text-align: right;">Reservado</th>
                        <th style="width: 10%;text-align: right;">Bloqueado</th>
                        <th style="width: 10%;text-align: right;">Disponível</th>
                        !-->
                        <th style="width: 10%;text-align: right;">Venda(R$)</th>
                        <th style="width: 20%;text-align: center;">Localização</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr style="backgroud-color: #2D335B">
                        <th colspan="10"></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $(".money").maskMoney();

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#tipoPessoa').change(function (e) {
            if ($(this).val() === 'PJ' ) {
                $('#div_contato').show();
                $('#div_sexo').hide();
                $('#div_rg').hide();
            } else {
                $('#div_contato').hide();
                $('#div_sexo').show();
                $('#div_rg').show();
            }
        });

        $('#aSearchCliente').click(function (event) {
            $('#div_searchCliente').load("<?php echo base_url();?>index.php/clientes/search")
        });


        $('#clientes_id').change(function (e) {
            var clientes_id = $(this).val();
            if (clientes_id) {
                $('#pincliente').attr('class', 'icon-pencil icon-white');
                $('#div_buttom_adicionar').html("Editar");

                $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                $('#aAdicionarVeiculo').removeAttr('disabled');
            } else {
                $('#cliente_id').val('');
                $('#tipoPessoa').val('PF');
                $('#origem').val('');
                $('#nomeCliente').val('');
                $('#sexo').val('M');
                $('#nomeFantasiaApelido').val('');
                $('#documento').val('');
                $('#rg').val('');
                $('#orgaoEmissor').val('');
                $('#estadoOrgaoEmissor').val('');
                $('#dataOrgaoEmissor').val('');
                $('#data_nascimento').val('');
                $('#telefone').val('');
                $('#celular').val('');
                $('#email').val('');
                $('#rua').val('');
                $('#numero').val('');
                $('#bairro').val('');
                $('#cidade').val('');
                $('#estado').val('');
                $('#cep').val('');
                $('#complemento').val('');
                $('#observacao').val('');
                $('#contatoNomeCliente').val('');
                $('#contatoSexo').val('');
                $('#contatoCpf').val('');
                $('#contatoEmail').val('');
                $('#contatoDataNascimento').val('');
                $('#contatoTelefone').val('');
                $('#contatoCelular').val('');
                $('#dataCadastro').val('');
                $('#senha').val('');

                $('#pincliente').attr('class', 'icon-plus-sign icon-white');
                $('#div_buttom_adicionar').html("<i class=\"icon-plus icon-white\"></i> Adicionar");

                $('#aAdicionarVeiculo').attr('disabled', true);
                $('#aAdicionarVeiculo').prop("href", '#');
            }
        });

        $('#addCliente').click(function (event) {
            event.preventDefault();
            var cliente_id = $('#clientes_id').val();

            if (cliente_id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/consultaCliente",
                    data: "cliente_id=" + cliente_id,
                    dataType: 'json',
                    success: function (cliente) {

                        $('#cliente_id').val(cliente_id);
                        $('#tipoPessoa').val(cliente.tipoPessoa);
                        $('#origem').val(cliente.origem);
                        $('#nomeCliente').val(cliente.nomeCliente);
                        $('#sexo').val(cliente.sexo);
                        $('#nomeFantasiaApelido').val(cliente.nomeFantasiaApelido);
                        $('#documento').val(cliente.documento);
                        $('#rg').val(cliente.rg);
                        $('#orgaoEmissor').val(cliente.orgaoEmissor);
                        $('#estadoOrgaoEmissor').val(cliente.estadoOrgaoEmissor);
                        $('#dataOrgaoEmissor').val(cliente.dataOrgaoEmissor);
                        $('#data_nascimento').val(cliente.data_nascimento);
                        $('#telefone').val(cliente.telefone);
                        $('#celular').val(cliente.celular);
                        $('#email').val(cliente.email);
                        $('#rua').val(cliente.rua);
                        $('#numero').val(cliente.numero);
                        $('#bairro').val(cliente.bairro);
                        $('#cidade').val(cliente.cidade);
                        $('#estado').val(cliente.estado);
                        $('#cep').val(cliente.cep);
                        $('#complemento').val(cliente.complemento);
                        $('#observacao').val(cliente.observacao);
                        $('#contatoNomeCliente').val(cliente.contatoNomeCliente);
                        $('#contatoSexo').val(cliente.contatoSexo);
                        $('#contatoCpf').val(cliente.contatoCpf);
                        $('#contatoEmail').val(cliente.contatoEmail);
                        $('#contatoDataNascimento').val(cliente.contatoDataNascimento);
                        $('#contatoTelefone').val(cliente.contatoTelefone);
                        $('#contatoCelular').val(cliente.contatoCelular);
                        $('#dataCadastro').val(cliente.dataCadastro);
                        $('#senha').val(cliente.senha);
                    }
                });
            }
        });

        $( "#formCliente" ).submit(function() {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/clientes/adicionarAjax",
                data: $("#formCliente").serialize(), // serializes the form's elements.
                success: function(cliente)
                {
                    if (cliente) {
                        cliente = JSON.parse(cliente);

                        $('#clientes_id option[value='+cliente.idClientes+']').remove();

                        $('#clientes_id').append('<option value="'+cliente.idClientes+'">' + cliente.nomeCliente + '</option>');
                        $('#clientes_id').val(cliente.idClientes);
                        $('#adicionarCliente').modal('hide');
                        $('#pincliente').attr('class', 'icon-pencil icon-white');
                        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                        $('#aAdicionarVeiculo').removeAttr('disabled');
                    }
                }
            });
        });

        $('#addConsultaProduto').click(function (event) {
            event.preventDefault();
            var table = $('#tblConsultaEstoque');
            var $tbody = table.append('<tbody />').children('tbody');
            $tbody.empty();
            $('#consultaEstoqueNomeProduto').html('');
            var produto = $('#produto').val();

            if (produto) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/produtos/getProdutoFiliais",
                    data: "produto=" + produto,
                    dataType: 'json',
                    success: function (produtofiliais) {
                        $.each(produtofiliais, function(i, produtofilial) {

                            var table = $('#tblConsultaEstoque');
                            var $tbody = table.append('<tbody />').children('tbody');
                            $('#consultaEstoqueNomeProduto').html(produtofilial.produto);

                            $tbody.append('<tr />').children('tr:last')
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")' style='text-align: left;cursor: pointer;'>" + produtofilial.filial + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.estoque + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.reservado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>" + produtofilial.bloqueado + "</td>")
                                //.append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'><b>" + (produtofilial.estoque -  produtofilial.reservado - produtofilial.bloqueado) + ' ' + produtofilial.unidade + "</b></td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: right;cursor: pointer;'>R$" + produtofilial.precoVenda + "</td>")
                                .append("<td onclick='preencherEstoquePorFilial("+produtofilial.idProdutoFilial+")'  style='text-align: center;cursor: pointer;'>" + produtofilial.localizacao + " </td>");
                        });
                    }
                });
            } else {
                $('#consultaEstoqueNomeProduto').html('Selecione um produto para consulta estoque');
            }
        });

        $('#recebido').click(function (event) {
            var flag = $(this).is(':checked');
            if (flag == true) {
                $('#divRecebimento').show();
            }
            else {
                $('#divRecebimento').hide();
            }
        });

        $(document).on('click', '#btn-faturar', function (event) {
            event.preventDefault();
            valor = $('#total-venda').val();
            valor = valor.replace(',', '');
            $('#valor').val(valor);

            custo = $('#total-custo').val();
            custo = custo.replace(',', '');
            $('#custoVenda').val(custo);


        });

        $("#formFaturar").validate({
            rules: {
                descricao: {required: true},
                cliente: {required: true},
                valor: {required: true},
                vencimento: {required: true}

            },
            messages: {
                descricao: {required: 'Campo Requerido.'},
                cliente: {required: 'Campo Requerido.'},
                valor: {required: 'Campo Requerido.'},
                vencimento: {required: 'Campo Requerido.'}
            },
            submitHandler: function (form) {
                var dados = $(form).serialize();
                $('#btn-cancelar-faturar').trigger('click');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/vendas/faturar",
                    data: dados,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {

                            window.location.reload(true);
                        }
                        else {
                            alert('Ocorreu um erro ao tentar faturar venda.');
                            $('#progress-fatura').hide();
                        }
                    }
                });

                return false;
            }
        });

        $('#produto').change(function (e) {
            $.ajax({
                type: "get", async: false,
                url: "<?php echo base_url(); ?>index.php/os/autoCompleteProdutoById/"+ $(this).val(),
                dataType: "json",
                success: function (data) {
                    var item = data.results[0];
                    $("#idProduto").val(item.id);
                    $("#estoque").val(item.estoque);
                    $("#preco").val(item.preco);
                    $("#custo").val(item.custo);
                    $("#valorVenda").val(item.preco);
                    $("#quantidade").focus();
                }
            });
        });

        $("#formVendas").validate({
            rules: {
                cliente: {required: true},
                tecnico: {required: true},
                dataVenda: {required: true}
            },
            messages: {
                cliente: {required: 'Campo Requerido.'},
                tecnico: {required: 'Campo Requerido.'},
                dataVenda: {required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });


        $("#formProdutos").validate({
            rules: {
                quantidade: {required: true}
            },
            messages: {
                quantidade: {required: 'Insira a quantidade'}
            },
            submitHandler: function (form) {
                var quantidade = parseInt($("#quantidade").val());
                var estoque = parseInt($("#estoque").val());
                //if (estoque < quantidade) {
                //    alert('Você não possui estoque suficiente.');
               // }
              //  else {
                    var dados = $(form).serialize();
                    $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url();?>index.php/vendas/adicionarProduto",
                        data: dados,
                        dataType: 'json',
                        success: function (data) {
                            if (data.result === true) {
                                $("#divProdutos").load("<?php echo current_url();?> #divProdutos");

                                $("#produto").select2('data', {
                                    id: null,
                                    text: ''
                                });

                                $("#quantidade").val('');
                                $("#custo").val('');
                                $("#valorVenda").val('');
                                $("#produto").val('').focus();
                            }
                            else {
                                alert('Ocorreu um erro ao tentar adicionar produto.');
                            }
                        }
                    });

                    return false;
              //  }

            }

        });


        $(document).on('click', 'a', function (event) {
            var idProduto = $(this).attr('idAcao');
            var quantidade = $(this).attr('quantAcao');
            var produto = $(this).attr('prodAcao');
            if ((idProduto % 1) == 0) {
                $("#divProdutos").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%'></div></div>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/vendas/excluirProduto",
                    data: "idProduto=" + idProduto + "&quantidade=" + quantidade + "&produto=" + produto,
                    dataType: 'json',
                    success: function (data) {
                        if (data.result == true) {
                            $("#divProdutos").load("<?php echo current_url();?> #divProdutos");
                        }
                        else {
                            alert('Ocorreu um erro ao tentar excluir produto.');
                        }
                    }
                });
                return false;
            }

        });

        $(".datepicker").datepicker({dateFormat: 'dd/mm/yy'});

    });

    function fecharModelSercheCliente(cliente) {

        $('#searchCliente').modal('hide');
        $('#pincliente').attr('class', 'icon-edit icon-white');

        if(cliente.inativo == 1){
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ea1f1f 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#ee5f5b00');
            $('#div_cliente_restricao').show();
            $('#div_cliente_restricao').html('Cliente com restrições: '+cliente.observacaoInativo);
            $('#btnContinuar').hide();
            $('#btn-faturar').hide();
        } else {
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ededed 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#fff');
            $('#div_cliente_restricao').hide();
            $('#btnContinuar').show();
            $('#btn-faturar').show();
        }
    }

    function preencherEstoquePorFilial(idProdutoFilial) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/produtos/getUnicProdutoFilial",
            data: "idProdutoFilial=" + idProdutoFilial,
            dataType: 'json',
            success: function (produtofiliais) {
                $("#idProduto").val(produtofiliais.produto_id);

                $("#estoque").val(produtofiliais.estoque);
                $("#valorVenda").val(produtofiliais.precoVenda);
                $("#custo").val(produtofiliais.precoCompra);
                $("#filial_customizada").val(produtofiliais.filial_id);
                $("#quantidade").focus();
                $("#modal-consulta-estoque .close").click()
            }
        });
    }

</script>

