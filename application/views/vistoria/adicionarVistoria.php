<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/select2.css"/>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.js"></script>

<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Adicionar Checklist</h5>
            </div>
            <div class="widget-content nopadding">
                <?php if ($custom_error == true) { ?>
                    <div class="span12 alert alert-danger" id="divInfo" style="padding: 1%;">Dados
                        incompletos, verifique os campos com asterisco ou se selecionou corretamente.
                        <?php echo $custom_error; ?>
                    </div>
                <?php } ?>
                <form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" id="formOs">
                    <div class="span12 well" style="padding: 1%; margin-left: 0">


                        <div class="span12" style="margin-left: 0">

                            <div class="span8">
                                <div class="formBuscaGSA" id="divBuscaCliente">
                                    <label for="cliente">
                                        <a href="#searchCliente" id="aSearchCliente" data-toggle="modal"><i class="icon-search tip-right"></i></i></a>
                                        Cliente<span class="required"></span>
                                        <a href="#adicionarCliente" id="addCliente" data-toggle="modal" style="float: right;margin-right: 4px;">
                                            <i id="pincliente" class="icon-plus-sign tip-right"></i></i>
                                        </a>
                                    </label>
                                    <div class="controls">
                                        <select name="clientes_id" class="span12" id="clientes_id" required="required">
                                            <option value="">--selecione um cliente--</option>
                                        </select>
                                        <div id="div_cliente_restricao" style="text-align: left;margin-top: -8px;display: none;"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="span4">
                                <div class="formBuscaGSA">
                                    <label for="tecnico">Responsável<span
                                                class="required">*</span></label>
                                    <div class="controls">
                                        <select name="usuarios_id" class="span12" id="usuarios_id" required="required">
                                            <option value="">--Selecione um cliente--</option>
                                            <?php foreach ($usuarios as $usuario) {?>
                                                <option value="<?php echo $usuario->idUsuarios; ?>"><?php echo $usuario->nome; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="span12" style="margin-left: 0">

                            <div class="span6">
                                <div class="formBuscaGSA" id="div_buscaVeiculo" style="<?php  echo 'background: linear-gradient(rgba(100, 234, 31, 0.44) 1%, rgb(255, 255, 255) 100%);';?>">
                                    <label for="veiculo_id">
                                        <a href="#"  id="aAdicionarVeiculo" role="button" data-toggle="modal"><i id="pinAdicionarVeiculo" class="icon-plus-sign icon-white"></i></a>
                                        Veículo*
                                    </label>
                                    <select class="span12" name="veiculo_id" required id="veiculo_id">
                                        <option value="">--Selecione uma opção--</option>
                                    </select>
                                    <div id="div_veiculo_restricao" style="text-align: center;margin-top: -8px;">Sem veículo</div>
                                </div>
                            </div>
                            <div class="span6">

                                <div class="formBuscaGSA">
                                    <label for="kmentrada">Kilometragem de entrada e saída</label>

                                    <input id="kmentrada" class="span6" style="float: left;margin-right: 10px;" type="number"
                                           name="kmentrada"
                                           value=""/>

                                    <input id="kilometragemsaida" class="span6" type="number"
                                           name="kilometragemsaida"
                                           value=""/>
                                    <div id="div_veiculo_restricao" style="text-align: center;margin-top: -8px;">&nbsp;</div>
                                </div>
                            </div>
                        </div>


                        <div class="span12" style="margin-left: 0">

                            <div class="span6">
                                <label for="status">Status<span class="required">*</span></label>
                                <select class="span12" name="status" id="status" value="">
                                    <option value="Orçamento">Orçamento</option>
                                    <option value="Garantia">Garantia</option>
                                </select>
                            </div>

                            <div class="span3">
                                <label for="datavistoria">Data Checklist*</label>
                                <input id="datavistoria" class="span12" type="date"
                                       name="datavistoria" required="required" value="<?php echo date('Y-m-d');?>"/>
                            </div>

                            <div class="span3">
                                <label for="horavistoria">Hora Checklist*</label>
                                <input id="horavistoria" class="span12" type="time"
                                       name="horavistoria" required="required" value="<?php echo date('h:i');?>"/>
                            </div>

                        </div>

                        <div class="span12" style="margin-left: 0">
                            <div class="span6" style="text-align: center">
                                <label for="gnv">GNV</label>
                                <input type="radio" required="required" name="gnv" value="r">&nbsp;R&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="gnv" value="I">&nbsp;I&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="gnv" value="II">&nbsp;II&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="gnv" value="III">&nbsp;III&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="gnv" value="IIII">&nbsp;IIIII&nbsp;&nbsp;&nbsp;
                                <input type="radio" checked required="required" name="gnv" value="sem">&nbsp;sem&nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="span6"  style="text-align: center">
                                <label for="tanque">Tanque</label>
                                <input type="radio" required="required" name="tanque" value="E">&nbsp;E&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="tanque" value="1/4">&nbsp;1/4&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="tanque" value="1/2">&nbsp;1/2&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="tanque" value="3/4">&nbsp;3/4&nbsp;&nbsp;&nbsp;
                                <input type="radio" required="required" name="tanque" value="F">&nbsp;F&nbsp;&nbsp;&nbsp;
                                <input type="radio" checked required="required" name="tanque" value="sem">&nbsp;sem&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="span12" style="margin-left: 0">

                        <!-- PNEUS !-->
                        <div class="accordion" id="collapse-group">
                            <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#pneu" data-toggle="collapse">
                                            <span class="icon"><i class="icon-ok"></i></span><h5>Pneus</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse in accordion-body" id="pneu">

                                </div>
                            </div>

                            <!-- Avarias !-->
                            <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#avarias" data-toggle="collapse">
                                            <span class="icon"><i class="icon-ok"></i></span><h5>Avarias</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse in accordion-body" id="avarias">

                                </div>
                            </div>

                            <!-- Acessorios e equipamentos !-->
                            <div class="accordion-group widget-box">
                                <div class="accordion-heading">
                                    <div class="widget-title">
                                        <a data-parent="#collapse-group" href="#acessorio" data-toggle="collapse">
                                            <span class="icon"><i class="icon-ok"></i></span><h5>Acessorios do veículo</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse in accordion-body" id="acessorio">
                                    <div class="widget-content">
                                        <div class="span12" id="divacessorio" style="padding: 2%; margin-left: 0">
                                            <div class="span3 well">
                                                <?php foreach($acessorios1 as $acessorio){ ?>
                                                    <h6><input type="checkbox" name="acessorio_<?php echo $acessorio->idAcessorio; ?>"  style="margin-bottom: 7px;">&nbsp;<?php echo $acessorio->nome; ?></h6>
                                                <?php } ?>
                                            </div>

                                            <div class="span3 well">
                                                <?php foreach($acessorios2 as $acessorio){ ?>
                                                    <h6><input type="checkbox" name="acessorio_<?php echo $acessorio->idAcessorio; ?>"  style="margin-bottom: 7px;">&nbsp;<?php echo $acessorio->nome; ?></h6>
                                                <?php } ?>
                                            </div>


                                            <div class="span3 well">
                                                <?php foreach($acessorios3 as $acessorio){ ?>
                                                    <h6><input type="checkbox" name="acessorio_<?php echo $acessorio->idAcessorio; ?>" style="margin-bottom: 7px;">&nbsp;<?php echo $acessorio->nome; ?></h6>
                                                <?php } ?>
                                            </div>

                                            <div class="span3 well">
                                                <?php foreach($acessorios4 as $acessorio){ ?>
                                                    <h6><input type="checkbox" name="acessorio_<?php echo $acessorio->idAcessorio; ?>" style="margin-bottom: 7px;">&nbsp;<?php echo $acessorio->nome; ?></h6>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="span12" style="margin-left: 0">

                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#observacaoVistoria" data-toggle="tab">Observação</a></li>
                            <li><a href="#pertences" data-toggle="tab">Pertences Porta Luva</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="observacaoVistoria">
                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                    <div class="span12">
                                        <textarea class="span12" name="observacao" cols="30" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="pertences">
                                <div class="span12 well" style="padding: 1%; margin-left: 0">
                                    <div class="span12">
                                        <textarea class="span12" name="pertencesPortaLuva" cols="30" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="span12" style="padding: 1%; margin-left: 0">
                            <div class="span7 offset3" style="text-align: center">

                                <button class="btn btn-primary" id="btnContinuar"><i
                                        class="icon-white icon-ok"></i> Salvar
                                </button>

                                <a href="<?php echo base_url() ?>index.php/vistoria" class="btn"><i
                                        class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal searchCliente -->
<div id="searchCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div id="div_searchCliente"></div>
</div>

<!-- Modal cadastrar novo cliente -->
<div id="adicionarCliente" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <form action="<?php echo current_url(); ?>" id="formCliente" method="post" class="form-horizontal">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Cadastrar / Editar o cliente</h3>
        </div>

        <div class="modal-body">

            <div class="span12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com
                asterisco.
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span4">
                    <label for="tipoPessoa">Tipo de pessoa</label>
                    <select name="tipoPessoa" id="tipoPessoa" class="span12 chzn" required="required">
                        <option value="PF">Pessoa Física</option>
                        <option value="PJ">Pessoa Jurídica</option>
                    </select>
                </div>

                <div class="span4">
                    <label for="origem">Origem</label>
                    <select name="origem" id="origem" class="span12 chzn" required="required">
                        <option value="">selecione uma origem</option>
                        <option value="Particular">Particular</option>
                        <option value="Financeira">Financeira</option>
                        <option value="Seguradora">Seguradora</option>
                    </select>
                </div>

            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span6">
                    <label for="nomeCliente" >Nome / Razão Social<span class="required">*</span></label>
                    <input id="nomeCliente"  class="span12" type="text" required="required" name="nomeCliente"
                           value="<?php echo set_value('nomeCliente'); ?>"/>

                    <input type="hidden" id="cliente_id" name="cliente_id" value="" />
                </div>

                <div class="span6">
                    <label for="nomeFantasiaApelido" >Nome Fantasia / Apelido</label>
                    <input id="nomeFantasiaApelido"  class="span12" type="text" name="nomeFantasiaApelido"
                           value="<?php echo set_value('nomeFantasiaApelido'); ?>"/>
                </div>


            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span8">
                    <label for="documento">CPF/CNPJ*</label>
                    <input id="documento" type="text" class="span12" required="required" name="documento"
                           value="<?php echo set_value('documento'); ?>"/>
                </div>

                <div class="span4" id="div_sexo">
                    <label for="sexo">Sexo</label>
                    <select name="sexo" id="sexo" class="span12 chzn" required="required">
                        <option value="M">Masculino</option>
                        <option value="F">Feminino</option>
                    </select>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0" id="div_rg">
                <div class="span3">
                    <label for="rg">RG</label>
                    <input id="rg" type="text" class="span12" name="rg"
                           value="<?php echo set_value('rg'); ?>"/>
                    </select>
                </div>

                <div class="span3">
                    <label for="orgaoEmissor">Orgão Emissor</label>
                    <input id="orgaoEmissor" type="text" class="span12" name="orgaoEmissor"
                           value="<?php echo set_value('orgaoEmissor'); ?>"/>
                </div>

                <div class="span2">
                    <label for="estadoOrgaoEmissor">Estado</label>
                    <input id="estadoOrgaoEmissor" type="text" class="span12" name="estadoOrgaoEmissor"
                           value="<?php echo set_value('estadoOrgaoEmissor'); ?>"/>
                </div>

                <div class="span4">
                    <label for="dataOrgaoEmissor">Data Emissão</label>
                    <input id="dataOrgaoEmissor" type="date" class="span12" name="dataOrgaoEmissor"
                           value="<?php echo set_value('dataOrgaoEmissor'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="telefone">Telefone</label>
                    <input id="telefone" type="text" class="span12" name="telefone"
                           value="<?php echo set_value('telefone'); ?>"/>
                </div>

                <div class="span6">
                    <label for="celular">Celular</label>
                    <input id="celular" type="text" class="span12" name="celular"
                           value="<?php echo set_value('celular'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="span12" name="email"
                           value="<?php echo set_value('email'); ?>"/>
                </div>

                <div class="span6">
                    <label for="email">Data de nascimento</label>
                    <input type="date" name="data_nascimento" class="span12" id="data_nascimento"
                           value="<?php echo set_value('data_nascimento'); ?>"/>
                </div>
            </div>


            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Dados do endereço
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">

                <div class="span3">
                    <label for="cep">CEP</label>
                    <input id="cep" type="text" name="cep" class="span12" onBlur="getConsultaCEP();"
                           value="<?php echo set_value('cep'); ?>"/>
                    <small>[TAB] consulta cep (Necessita Internet)</small>
                </div>

                <div class="span6">
                    <label for="rua">Rua</label>
                    <input id="rua" type="text" name="rua"  class="span12"  value="<?php echo set_value('rua'); ?>"/>
                </div>

                <div class="span3">
                    <label for="numero">Número</label>
                    <input id="numero" type="text" name="numero"  class="span12"  value="<?php echo set_value('numero'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <label for="complemento">Complemento</label>
                    <input id="complemento" type="text" name="complemento"  class="span12"  value="<?php echo set_value('complemento'); ?>"/>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span5">
                    <label for="bairro">Bairro</label>
                    <input id="bairro" type="text" name="bairro"  class="span12"  value="<?php echo set_value('bairro'); ?>"/>
                </div>

                <div class="span4">
                    <label for="cidade">Cidade</label>
                    <input id="cidade" type="text" name="cidade"  class="span12"  value="<?php echo set_value('cidade'); ?>"/>
                </div>
                <div class="span3">
                    <label for="estado">Estado</label>
                    <input id="estado" type="text" name="estado"  class="span12"  value="<?php echo set_value('estado'); ?>"/>
                </div>
            </div>

            <div id="div_contato" style="display: none;">
                <ul class="nav nav-tabs">
                    <li class="active" id="tabDetalhes">
                        <a href="#tab1" data-toggle="tab">
                            Dados do  Contato
                        </a>
                    </li>
                </ul>
                <div class="span12" style="padding: 1%; margin-left: 0">

                    <div class="span12">
                        <label for="contatoNomeCliente" >Nome<span class="required">*</span></label>
                        <input id="contatoNomeCliente"  class="span12" type="text" name="contatoNomeCliente"
                               value="<?php echo set_value('contatoNomeCliente'); ?>"/>
                    </div>

                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span8">
                        <label for="contatoCpf">CPF</label>
                        <input id="contatoCpf" type="text" class="span12" name="contatoCpf"
                               value="<?php echo set_value('contatoCpf'); ?>"/>
                    </div>

                    <div class="span4">
                        <label for="contatoSexo">Sexo</label>
                        <select name="contatoSexo" id="contatoSexo" class="span12 chzn">
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                        </select>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="contatoTelefone">Telefone</label>
                        <input id="contatoTelefone" type="text" class="span12" name="contatoTelefone"
                               value="<?php echo set_value('contatoTelefone'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="contatoCelular">Celular</label>
                        <input id="contatoCelular" type="text" class="span12" name="contatoCelular"
                               value="<?php echo set_value('contatoCelular'); ?>"/>
                    </div>
                </div>

                <div class="span12" style="padding: 1%; margin-left: 0">
                    <div class="span6">
                        <label for="email">Email</label>
                        <input id="contatoEmail" type="email" class="span12" name="contatoEmail"
                               value="<?php echo set_value('contatoEmail'); ?>"/>
                    </div>

                    <div class="span6">
                        <label for="email">Data de nascimento</label>
                        <input type="date" name="contatoDataNascimento" class="span12" id="contatoDataNascimento"
                               value="<?php echo set_value('contatoDataNascimento'); ?>"/>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Senha para acessar o portal do cliente
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <label for="senha">Senha</label>
                <input type="password" name="senha" class="span12" id="senha"
                       value=""/>
                <small>Deixe em branco e a senha padrão será o (cpj/cnpj) do cliente</small>
            </div>

            <ul class="nav nav-tabs">
                <li class="active" id="tabDetalhes">
                    <a href="#tab1" data-toggle="tab">
                        Observação
                    </a>
                </li>
            </ul>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span12">
                    <textarea class="span12" id="observacao"  name="observacao" cols="30" rows="5"><?php echo set_value('observacao'); ?></textarea>
                </div>
            </div>

            <div class="span12" style="padding: 1%; margin-left: 0">
                <div class="span6 offset3" style="text-align: center">
                    <button type="submit" class="btn btn-success">
                        <div id="div_buttom_adicionar"><i class="icon-plus icon-white"></i> Adicionar</div>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal cadastrar novo veiculo do cliente -->
<div id="adicionarVeiculo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
     <div id="div_cadVeiculoModal"></div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/select2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/veiculo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cadVeiculoModal.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sistema/cliente.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $("#documento").mask("999.999.999-99");
        $('#usuarios_id').val(<?php echo  $this->session->userdata('id');?>);

        $('#documento').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $('#clientes_id').change(function (e) {
            var clientes_id = $(this).val();
            if (clientes_id) {
                $('#pincliente').attr('class', 'icon-edit icon-white');
                $('#div_buttom_adicionar').html("Editar");

                $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                $('#aAdicionarVeiculo').removeAttr('disabled');
            } else {
                $('#cliente_id').val('');
                $('#tipoPessoa').val('PF');
                ('#origem').val('');
                $('#nomeCliente').val('');
                $('#sexo').val('M');
                $('#nomeFantasiaApelido').val('');
                $('#documento').val('');
                $('#rg').val('');
                $('#orgaoEmissor').val('');
                $('#estadoOrgaoEmissor').val('');
                $('#dataOrgaoEmissor').val('');
                $('#data_nascimento').val('');
                $('#telefone').val('');
                $('#celular').val('');
                $('#email').val('');
                $('#rua').val('');
                $('#numero').val('');
                $('#bairro').val('');
                $('#cidade').val('');
                $('#estado').val('');
                $('#cep').val('');
                $('#complemento').val('');
                $('#observacao').val('');
                $('#contatoNomeCliente').val('');
                $('#contatoSexo').val('');
                $('#contatoCpf').val('');
                $('#contatoEmail').val('');
                $('#contatoDataNascimento').val('');
                $('#contatoTelefone').val('');
                $('#contatoCelular').val('');
                $('#dataCadastro').val('');
                $('#senha').val('');

                $('#pincliente').attr('class', 'icon-plus-sign icon-white');
                $('#div_buttom_adicionar').html("<i class=\"icon-plus icon-white\"></i> Adicionar");

                $('#aAdicionarVeiculo').attr('disabled', true);
                $('#aAdicionarVeiculo').prop("href", '#');
            }

            limparCamposVeiculo();
            consultaVeiculoCliente($('#clientes_id').val(),'<?php echo base_url();?>');
        });

        $('#aAdicionarVeiculo').click(function (e) {
            event.preventDefault();
            $('#div_cadVeiculoModal').load("<?php echo base_url();?>index.php/clientes/cadVeiculoModal",
                function(responseTxt, statusTxt, xhr){
                    if (statusTxt == "success") {
                        buscarVeiculo($('#clientes_id').val(),'<?php echo base_url();?>');
                    }
                }
            );
        });

        $('#addCliente').click(function (event) {
            event.preventDefault();
            var cliente_id = $('#clientes_id').val();

            if (cliente_id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/clientes/consultaCliente",
                    data: "cliente_id=" + cliente_id,
                    dataType: 'json',
                    success: function (cliente) {

                        $('#cliente_id').val(cliente_id);
                        $('#tipoPessoa').val(cliente.tipoPessoa);
                        $('#origem').val(cliente.origem);
                        $('#nomeCliente').val(cliente.nomeCliente);
                        $('#sexo').val(cliente.sexo);
                        $('#nomeFantasiaApelido').val(cliente.nomeFantasiaApelido);
                        $('#documento').val(cliente.documento);
                        $('#rg').val(cliente.rg);
                        $('#orgaoEmissor').val(cliente.orgaoEmissor);
                        $('#estadoOrgaoEmissor').val(cliente.estadoOrgaoEmissor);
                        $('#dataOrgaoEmissor').val(cliente.dataOrgaoEmissor);
                        $('#data_nascimento').val(cliente.data_nascimento);
                        $('#telefone').val(cliente.telefone);
                        $('#celular').val(cliente.celular);
                        $('#email').val(cliente.email);
                        $('#rua').val(cliente.rua);
                        $('#numero').val(cliente.numero);
                        $('#bairro').val(cliente.bairro);
                        $('#cidade').val(cliente.cidade);
                        $('#estado').val(cliente.estado);
                        $('#cep').val(cliente.cep);
                        $('#complemento').val(cliente.complemento);
                        $('#observacao').val(cliente.observacao);
                        $('#contatoNomeCliente').val(cliente.contatoNomeCliente);
                        $('#contatoSexo').val(cliente.contatoSexo);
                        $('#contatoCpf').val(cliente.contatoCpf);
                        $('#contatoEmail').val(cliente.contatoEmail);
                        $('#contatoDataNascimento').val(cliente.contatoDataNascimento);
                        $('#contatoTelefone').val(cliente.contatoTelefone);
                        $('#contatoCelular').val(cliente.contatoCelular);
                        $('#dataCadastro').val(cliente.dataCadastro);
                        $('#senha').val(cliente.senha);
                    }
                });
            }
        });

        $("#celular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoCelular").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#telefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $("#contatoTelefone").mask("(99) 9999-9999?9").on("focusout", function () {
            var len = this.value.replace(/\D/g, '').length;
            $(this).mask(len > 10 ? "(99) 99999-999?9" : "(99) 9999-9999?9");
        });

        $('#aSearchCliente').click(function (event) {
            $('#div_searchCliente').load("<?php echo base_url();?>index.php/clientes/search");
        });

        $( "#formCliente" ).submit(function() {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/clientes/adicionarAjax",
                data: $("#formCliente").serialize(), // serializes the form's elements.
                success: function(cliente)
                {
                    if (cliente) {
                        cliente = JSON.parse(cliente);

                        $('#clientes_id option[value='+cliente.idClientes+']').remove();

                        $('#clientes_id').append('<option value="'+cliente.idClientes+'">' + cliente.nomeCliente + '</option>');
                        $('#clientes_id').val(cliente.idClientes);
                        $('#adicionarCliente').modal('hide');
                        $('#pincliente').attr('class', 'icon-edit icon-white');
                        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
                        $('#aAdicionarVeiculo').removeAttr('disabled');
                    }
                }
            });
        });

        $('#veiculo_id').change(function (e) {

            //PNEUS
            var tipoveiculo_id = $(this).find(':selected').attr('tipoveiculo_id');
            $("#pneu").html('');
            if (tipoveiculo_id !== undefined) {
                $('#tipoveiculo_id').val(tipoveiculo_id);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/vistoria/getTemplatePneu",
                    data: "idTipoVeiculo=" + tipoveiculo_id,
                    dataType: 'html',
                    success: function (data) {
                        $("#pneu").html(data);
                    }
                });

                //AVARIAS
                $('#avarias').html('');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>index.php/vistoria/getTemplateAvaria",
                    data: "idTipoVeiculo=" + tipoveiculo_id,
                    dataType: 'html',
                    success: function (data) {
                        $("#avarias").html(data);
                    }
                });
            }
        });
        buscarVeiculo($('#clientes_id').val(),'<?php echo base_url();?>');
     });

    function exibir(id) {
        var isVisible = $( '#divavaria_' +id ).is( ":visible" );
        if (isVisible) {
            $( '#divavaria_' +id ).hide();
            $('#equipamento_' +id).prop('checked', false);

        } else {
            $( '#divavaria_' +id ).show();
            $('#equipamento_' +id).prop('checked', true);
        }

        return isVisible;
    }

    function fecharModelSercheCliente(cliente) {
        $('#searchCliente').modal('hide');

        $('#pincliente').attr('class', 'icon-edit icon-white');
        $('#aAdicionarVeiculo').prop("href", '#adicionarVeiculo');
        $('#pinAdicionarVeiculo').removeAttr('disabled');
        consultaVeiculoCliente($('#clientes_id').val(),'<?php echo base_url();?>');

        if(cliente.inativo == 1){
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ea1f1f 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#ee5f5b00');
            $('#div_cliente_restricao').show();
            $('#div_cliente_restricao').html('Cliente com restrições: '+cliente.observacaoInativo);
            $('#btnContinuar').hide();
            $('#btn-faturar').hide();
        } else {
            $('#divBuscaCliente').css('background','linear-gradient(to bottom, #ededed 1%,#ffffff 100%)');
            $('#clientes_id').css('background','#fff');
            $('#div_cliente_restricao').hide();
            $('#btnContinuar').show();
            $('#btn-faturar').show();
        }
    }
</script>





