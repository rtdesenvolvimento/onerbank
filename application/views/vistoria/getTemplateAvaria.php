<div class="widget-content">
    <div class="span12" id="divavarias" style="margin-left: 0">
        <div class="span12" style="margin-left: 0">
            <iframe height="400" style="margin: 0;" frameborder="0" scrolling="yes" src="<?php echo base_url(); ?>vistoria/iframePontosAvaria/<?php echo $tipoVeiculo.'/'.$idVistoria; ?>" width="100%"></iframe>
        </div>
    </div>

    <div class="span12" id="divacessorio" style="padding: 1%; margin-left: 0">
        <div class="span4 well">
            <h5>Visão Topo</h5><hr>
            <?php foreach($visaotopo1 as $visao){
                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria, $visao->idVisaoautomovel, $visao->idEquipamentoveiculo);
                ?>

                <?php if ($avariasVistoria) { ?>
                    <b><input type="checkbox" checked="checked" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option <?php if ($avariasVistoria->tipoavaria_id == $avaria->idTipoavaria) echo 'selected="selected"'; ?> value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select>
                        <a href="#modal-anexo"  idVistoriaavaria="<?php echo $avariasVistoria->idVistoriaavaria; ?>" style="margin-right: 1%;margin-top: -9px;" class="btn tip-top anexo" data-toggle="modal"><i class="icon-eye-open""></i></a>
                        <br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } else { ?>
                    <b><input type="checkbox"  id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>" style="display: none;">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select><br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php  }?>
            <?php } ?>
            <br/>
        </div>

        <div class="span4 well">
            <h5>Visão Traseira</h5><hr>
            <?php foreach($visaotopo2 as $visao){
                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria, $visao->idVisaoautomovel, $visao->idEquipamentoveiculo);
                ?>
                <?php if ($avariasVistoria) { ?>
                    <b><input type="checkbox" checked="checked" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option <?php if ($avariasVistoria->tipoavaria_id == $avaria->idTipoavaria) echo 'selected="selected"'; ?> value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select>
                        <a href="#modal-anexo"  idVistoriaavaria="<?php echo $avariasVistoria->idVistoriaavaria; ?>" style="margin-right: 1%;margin-top: -9px;" class="btn tip-top anexo" data-toggle="modal"><i class="icon-eye-open""></i></a>
                        <br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } else { ?>
                    <b><input type="checkbox" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>" style="display: none;">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select><br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } ?>
            <?php } ?>
            <br/><br/>
        </div>

        <div class="span4 well">
            <h5>Visão Dianteira</h5><hr>
            <?php foreach($visaotopo3 as $visao){
                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria, $visao->idVisaoautomovel, $visao->idEquipamentoveiculo);
                ?>

                <?php if ($avariasVistoria) { ?>
                    <b><input type="checkbox" checked="checked" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option <?php if ($avariasVistoria->tipoavaria_id == $avaria->idTipoavaria) echo 'selected="selected"'; ?> value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select>
                        <a href="#modal-anexo"  idVistoriaavaria="<?php echo $avariasVistoria->idVistoriaavaria; ?>" style="margin-right: 1%;margin-top: -9px;" class="btn tip-top anexo" data-toggle="modal"><i class="icon-eye-open""></i></a>
                        <br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } else { ?>
                    <b><input type="checkbox" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>" style="display: none;">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select><br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } ?>
            <?php } ?><br/>
        </div>
    </div>

    <div class="span12" id="divacessorio" style="padding: 1%; margin-left: 0">

        <div class="span6 well">
            <h5>Visão Lado Direito</h5><hr>
            <?php foreach($visaotopo4 as $visao){
                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria, $visao->idVisaoautomovel, $visao->idEquipamentoveiculo);
                ?>

                <?php if ($avariasVistoria) { ?>
                    <b><input type="checkbox" checked="checked" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option <?php if ($avariasVistoria->tipoavaria_id == $avaria->idTipoavaria) echo 'selected="selected"'; ?> value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select>
                        <a href="#modal-anexo"  idVistoriaavaria="<?php echo $avariasVistoria->idVistoriaavaria; ?>" style="margin-right: 1%;margin-top: -9px;" class="btn tip-top anexo" data-toggle="modal"><i class="icon-eye-open""></i></a>
                        <br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } else { ?>
                    <b><input type="checkbox" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>" style="display: none;">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select><br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>" class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

        <div class="span6 well">
            <h5>Visão Lado Esquerdo</h5><hr>
            <?php foreach($visaotopo5 as $visao){
                $avariasVistoria = $this->vistoria_model->getAllAvariasVistoriaVisaoEquipamento($idVistoria, $visao->idVisaoautomovel, $visao->idEquipamentoveiculo);
                ?>
                <?php if ($avariasVistoria) { ?>
                    <b><input type="checkbox" checked="checked" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option <?php if ($avariasVistoria->tipoavaria_id == $avaria->idTipoavaria) echo 'selected="selected"'; ?> value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select>
                        <a href="#modal-anexo"  idVistoriaavaria="<?php echo $avariasVistoria->idVistoriaavaria; ?>" style="margin-right: 1%;margin-top: -9px;" class="btn tip-top anexo" data-toggle="modal"><i class="icon-eye-open""></i></a>
                        <br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>"  class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } else { ?>
                    <b><input type="checkbox" id="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" name="equipamento_<?php echo $visao->idEquipamentoveiculo; ?>" onclick="exibir(<?php echo $visao->idEquipamentoveiculo; ?>);" style="margin-bottom: 7px;display: none;"> <?php echo $visao->nome; ?></b><br/>
                    <input type="hidden" name="client_x<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_x; ?>">
                    <input type="hidden" name="client_y<?php echo $visao->idEquipamentoveiculo; ?>" value="<?php echo $visao->client_y; ?>">
                    <div id="divavaria_<?php echo $visao->idEquipamentoveiculo; ?>" style="display: none;">
                        <select name="tipoavaria_id_<?php echo $visao->idEquipamentoveiculo; ?>">
                            <option></option>
                            <?php foreach($tipoavarias as $avaria){ ?>
                                <option value="<?php echo $avaria->idTipoavaria?>"><?php echo $avaria->nome?></option>
                            <?php  }?>
                        </select><br/>
                        <input type="file" name="file_<?php echo $visao->idEquipamentoveiculo; ?>"  class="btn-info" accept="image/png, image/jpeg">
                        <br/>
                    </div>
                <?php } ?>
            <?php } ?>
            <br/><br/><br/>
        </div>
    </div>
</div>

<!-- Modal visualizar anexo -->
<div id="modal-anexo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Visualizar Anexo</h3>
    </div>
    <div class="modal-body">
        <div class="span12" id="div-visualizar-anexo" style="text-align: center">

        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" id="fechar_modal_foto" aria-hidden="true">Fechar</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.anexo', function (event) {
            event.preventDefault();
            var idVistoriaavaria = $(this).attr('idVistoriaavaria');

            $("#div-visualizar-anexo").html('');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>index.php/vistoria/getAnexosAvarias",
                data: {idVistoria: '<?php echo $idVistoria; ?>', idVistoriaavaria: idVistoriaavaria},
                dataType: 'html',
                success: function (data) {
                    $("#div-visualizar-anexo").html(data);
                }
            });
        });
    });
</script>
