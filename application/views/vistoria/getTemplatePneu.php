<div class="widget-content">
    <div class="span12" id="divpneus" style="padding: 2%; margin-left: 0">
        <table class="table table-bordered" id="tblProdutos">
            <thead>
            <tr>
                <th></th>
                <th style="text-align: left;width: 20%;">Marca</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($pneus as $pneu) { ?>
                <?php if ( count($vistoriapneus) >0) {?>
                    <?php foreach ($vistoriapneus as $vistoria) {
                        if ($vistoria->idTipoveiculopneu == $pneu->idTipoveiculopneu)  {?>
                            <tr>
                                <td>
                                    <input type="radio" <?php if ($vistoria->estado == 'bom') echo 'checked="checked"' ;?> required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="bom">&nbsp;&nbsp;Bom
                                    <input type="radio" <?php if ($vistoria->estado == '1/2vida') echo 'checked="checked"' ;?> required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="1/2vida">&nbsp;&nbsp;1/2vida
                                    <input type="radio" <?php if ($vistoria->estado == 'careca') echo 'checked="checked"' ;?> required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="careca">&nbsp;&nbsp;careca
                                    <input type="radio" <?php if ($vistoria->estado == 'sem') echo 'checked="checked"' ;?> required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="sem">&nbsp;&nbsp;sem
                                </td>
                                <td style="width: 5%">
                                    <input type="text" value="<?php echo $vistoria->marca;?>" name="marca_<?php echo $pneu->idTipoveiculopneu;?>" style="width: 60%;">
                                </td>
                                <td>
                                    <?php echo $pneu->posicao; ?>
                                    <input type="hidden" name="posicao_<?php echo $pneu->idTipoveiculopneu;?>" value="<?php echo $pneu->posicao; ?>">
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php  } else { ?>
                    <tr>
                        <td>
                            <input type="radio" required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="bom">&nbsp;&nbsp;Bom
                            <input type="radio" required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="1/2vida">&nbsp;&nbsp;1/2vida
                            <input type="radio" required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="careca">&nbsp;&nbsp;careca
                            <input type="radio" required="required" name="estado_<?php echo $pneu->idTipoveiculopneu;?>" value="sem">&nbsp;&nbsp;sem
                        </td>
                        <td style="width: 5%">
                            <input type="text"  value="" name="marca_<?php echo $pneu->idTipoveiculopneu;?>" style="width: 60%;">
                        </td>
                        <td>
                            <?php echo $pneu->posicao; ?>
                            <input type="hidden" name="posicao_<?php echo $pneu->idTipoveiculopneu;?>" value="<?php echo $pneu->posicao; ?>">
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>