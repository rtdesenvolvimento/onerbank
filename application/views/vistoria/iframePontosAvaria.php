<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
    #special {
        width: 1040px;
        height: 312px;
        border:1px ridge green;
        background: #0e90d2;
    }
</style>
<body>
<h2 id="status2" style="display: none;">0, 0</h2>
<br/>
<canvas id="special" style="background-image: url('<?php echo base_url().'assets/arquivos/tipoveiculo/'.$tipoVeiculo->anexo; ?>')">Not supported</canvas>

<script>
    jQuery(document).ready(function(){

        $('#special').attr('height', $('#special').css('height'));
        $('#special').attr('width', $('#special').css('width'));

        var c = document.getElementById("special");
        var elemLeft = c.offsetLeft;
        var elemTop = c.offsetTop;
        var elements = [];

        var ctx = c.getContext("2d");
        var x = 0;
        var y = 0;

        // Add event listener for `click` events.
        c.addEventListener('click', function (event) {
            var x = event.pageX - elemLeft,
                y = event.pageY - elemTop;
            $.each(elements, function(i, element) {
                if ( (x >= element.width - 10 && x <= element.width + 10) &&
                    (y >= element.height - 10 && y <= element.height + 10) ) {

                    var visivel = window.parent.exibir(element.idEquipamentoveiculo);
                    if (!visivel) {
                        ctx.beginPath();
                        ctx.arc(element.width, element.height, 10, 0, 2 * Math.PI, false);
                        ctx.fillStyle = "red";
                        ctx.fill();
                        ctx.stroke();
                    } else {
                        ctx.beginPath();
                        ctx.arc(element.width, element.height, 10, 0, 2 * Math.PI, false);
                        ctx.fillStyle = "white";
                        ctx.fill();
                        ctx.stroke();
                    }
                }
            });

        }, false);

        <?php foreach($visaotopo1 as $visao){ ?>

            x = <?php echo $visao->client_x;?>;
            y = <?php echo $visao->client_y;?>;


            ctx.beginPath();
            ctx.arc(x, y, 10,0, 2*Math.PI,false);
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.stroke();

            elements.push({
                width: <?php echo $visao->client_x;?>,
                height: <?php echo $visao->client_y;?>,
                idEquipamentoveiculo : <?php echo $visao->idEquipamentoveiculo;?>
            });

        <?php } ?>


        <?php foreach($visaotopo2 as $visao){ ?>

            x = <?php echo $visao->client_x;?>;
            y = <?php echo $visao->client_y;?>;

            ctx.beginPath();
            ctx.arc(x, y, 10,0, 2*Math.PI,false);
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.stroke();

            elements.push({
                width: <?php echo $visao->client_x;?>,
                height: <?php echo $visao->client_y;?>,
                idEquipamentoveiculo : <?php echo $visao->idEquipamentoveiculo;?>
            });

        <?php } ?>

        <?php foreach($visaotopo3 as $visao){ ?>

            x = <?php echo $visao->client_x;?>;
            y = <?php echo $visao->client_y;?>;

            ctx.beginPath();
            ctx.arc(x, y, 10,0, 2*Math.PI,false);
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.stroke();

            elements.push({
                width: <?php echo $visao->client_x;?>,
                height: <?php echo $visao->client_y;?>,
                idEquipamentoveiculo : <?php echo $visao->idEquipamentoveiculo;?>
            });

        <?php } ?>

        <?php foreach($visaotopo4 as $visao){ ?>

            x = <?php echo $visao->client_x;?>;
            y = <?php echo $visao->client_y;?>;


            ctx.beginPath();
            ctx.arc(x, y, 10,0, 2*Math.PI,false);
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.stroke();

            elements.push({
                width: <?php echo $visao->client_x;?>,
                height: <?php echo $visao->client_y;?>,
                idEquipamentoveiculo : <?php echo $visao->idEquipamentoveiculo;?>
            });

        <?php } ?>


        <?php foreach($visaotopo5 as $visao){ ?>

            x = <?php echo $visao->client_x;?>;
            y = <?php echo $visao->client_y;?>;

            ctx.beginPath();
            ctx.arc(x, y, 10,0, 2*Math.PI,false);
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.stroke();

            elements.push({
                width: <?php echo $visao->client_x;?>,
                height: <?php echo $visao->client_y;?>,
                idEquipamentoveiculo : <?php echo $visao->idEquipamentoveiculo;?>
            });

        <?php } ?>


        <?php foreach($avarias as $avaria){ ?>
            ctx.beginPath();
            ctx.arc(<?php echo $avaria->client_x; ?>, <?php echo $avaria->client_y; ?>, 10, 0, 2 * Math.PI, false);
            ctx.fillStyle = "red";
            ctx.fill();
            ctx.stroke();
        <?php  }?>
    })
</script>
</body>