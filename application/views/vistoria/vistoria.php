<a href="<?php echo base_url() ?>index.php/vistoria/adicionar" class="btn btn-success">
    <i class="icon-plus icon-white"></i> Adicionar Checklist
</a>
<?php
if (!$results) {?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Vistorias</h5>
        </div>

        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th>Cliente</th>
                    <th>Data/Hora checklist</th>
                    <th>GNV</th>
                    <th>Tanque</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="5">Nenhuma checklist</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="icon-random"></i>
         </span>
            <h5>Checklist</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-responsive">
                <thead>
                <tr style="backgroud-color: #2D335B">
                    <th style="text-align: left;">Cliente</th>
                    <th style="text-align: center;">Data/Hora<br/>checklist</th>
                    <th style="text-align: center;">GNV</th>
                    <th style="text-align: center;">Tanque</th>
                    <th style="text-align: center;">KM Entrada<br/>KMSaída</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $r) {

                    switch ($r->status) {
                        case 'Retirado':
                            $cor = '#FF0000';
                            break;
                        case 'Em Bancada':
                            $cor = '#0000FF';
                            break;
                        case 'Orçamento':
                            $cor = '#008B8B';
                            break;
                        case 'Aguardando peça':
                            $cor = '#FF7F00';
                            break;
                        case 'Pronto':
                            $cor = '#00FF00';
                            break;
                        case 'Aguardando Aprovação':
                            $cor = '#FFA500';
                            break;
                        case 'Aprovado':
                            $cor = '#00FA9A';
                            break;
                        case 'Recusado':
                            $cor = '#FFD700';
                            break;
                        case 'Garantia':
                            $cor = '#8B6914';
                            break;
                        case 'S/Conserto':
                            $cor = '#9B30FF';
                            break;
                        case 'Aberto':
                            $cor = '#fbb450';
                            break;
                        case 'Faturado':
                            $cor = '#5bb75b';
                            break;
                        case 'Em Andamento':
                            $cor = '#5bc0de';
                            break;
                        case 'Finalizado':
                            $cor = '#e5e5e5';
                            break;
                        case 'Cancelado':
                            $cor = '#ee5f5b';
                            break;
                        default:
                            $cor = '#E0E4CC';
                            break;
                    }

                    $veiculo_id = $r->veiculo_id;
                    $veiculoStr = '';

                    if ($veiculo_id) {
                        $veiculo = $this->db->get_where('veiculos' , array('idVeiculo' => $veiculo_id ))->row();
                        if (count($veiculo) > 0) {
                            $veiculoStr = $veiculo->tipoVeiculo . ' ' . $veiculo->modelo . ' ' . $veiculo->marca . ' ' . $veiculo->placa;
                        }
                    }

                    $cliente = $this->clientes_model->getById($r->clientes_id);
                    $dados_cliente = $cliente->nomeCliente;

                    if ($cliente->telefone) {
                        $dados_cliente = $dados_cliente .'<br/>'.$cliente->telefone;
                    }

                    if ($cliente->celular) {
                        $dados_cliente = $dados_cliente .'<br/>'.$cliente->celular;
                    }

                    $usuario = $this->usuarios_model->getById($r->usuarios_id);

                    echo '<tr class="abrirVistoria" idVistoria="'.$r->idVistoria.'" >';
                    echo '    <td>' . $dados_cliente .'<br/>'.$veiculoStr . '</td>';
                    echo '    <td style="text-align: center;">' . date(('d/m/Y'), strtotime($r->datavistoria)) .'<br/>' . $r->horavistoria . '</td>';
                    echo '    <td style="text-align: center;">' . $r->gnv . '</td>';
                    echo '    <td style="text-align: center;">' . $r->tanque . '</td>';
                    echo '    <td style="text-align: center;">' . $r->kmentrada.'km<br/>'. $r->kmsaida . 'km</td>';
                    echo '    <td style="text-align: center"><span class="badge" style="background-color: '.$cor.'; border-color: '.$cor.'">'.$r->status.'</span> </td>';
                    echo '    <td style="text-align: center;">';
                    echo '      <a style="margin-right: 1%" href="' . base_url() . 'index.php/vistoria/editar/' . $r->idVistoria . '" class="btn btn-info tip-top"><i class="icon-pencil icon-white"></i></a>';
                    echo '      <a href="#modal-excluir" role="button" data-toggle="modal" vistoria="' . $r->idVistoria . '" class="btn btn-danger tip-top" ><i class="icon-remove icon-white"></i></a>  ';
                    echo '    </td>';
                    echo '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->pagination->create_links();
} ?>

<!-- Modal -->
<div id="modal-excluir" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <form action="<?php echo base_url() ?>index.php/vistoria/excluir" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h5 id="myModalLabel">Excluir Vistoria</h5>
        </div>
        <div class="modal-body">
            <input type="hidden" id="idVistoria" name="id" value=""/>
            <h5 style="text-align: center">Deseja realmente excluir esta vistoria?</h5>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button class="btn btn-danger">Excluir</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', 'a', function (event) {
            var vistoria = $(this).attr('vistoria');
            $('#idVistoria').val(vistoria);
        });

        $('.abrirVistoria').dblclick(function (event) {
            var idVistoria = $(this).attr('idVistoria');
            window.location = '<?php echo base_url();?>index.php/vistoria/editar/'+idVistoria;
        });
    });
</script>