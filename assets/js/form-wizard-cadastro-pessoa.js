var FormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#form');
    var numberOfSteps = $('.swMain > ul > li').length;

    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val === 'undefined') || val === "") {
            val = 1;
        };
        
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
			$(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
		});
    };    
    var initValidator = function () {

        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") === "radio" || element.attr("type") === "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") === "card_expiry_mm" || element.attr("name") === "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                }
            },
            ignore: ':hidden',
            rules: {
                nome: {
                    minlength: 2,
                    required: true
                },
                email: {
                    required: false,
                    email: true
                },
                cpf: {
                    required: true,
                },
                rg: {
                    required: false
                },
                gender: {
                    required: false
                },
                celular: {
                    required: false
                },
                cep: {
                    required: false
                },
                endereco: {
                    required: true
                },
                bairro: {
                    required: true
                },
                cidade: {
                    required: true
                },
                estado: {
                    required: true
                },
                valorVencimento: {
                    required: true
                }
                ,
                dataVencimento: {
                    required: true
                }
            },
            messages: {
                firstname: "Please specify your first name"
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };

    var onShowStep = function (obj, context) {

        if(context.toStep == numberOfSteps){
            $('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
        }

        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });

    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            alert('form submit function');
            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');
            //wizardForm.submit();
        }
    };
    var displayConfirm = function () {
        $('.display-value', form).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {
                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
            } else if ( input.attr("type") == "number" ) {
                $(this).html(input.val()+' dia(s)');
            } else if (input.attr("type") == "date") {
                var ano = input.val().split('-')[0];
                var mes = input.val().split('-')[1];
                var dia = input.val().split('-')[2];
                $(this).html(dia+'/'+mes+'/'+ano);
            }
        });
    };
    var salvarPessoaPrincial = function () {

        $.blockUI({ message: 'Salvando dados do pagador aguarde...' });

        $.ajax({
            type: "POST",
            url: base_url+"clientes/cadPessoaOnlineIntegracaoSubmit",
            data: $('#form').serialize(),
            dataType: 'json',
            success: function (pessoa) {
                $('#idPessoaPrincipal').val(pessoa.idClientes);
                $("#valorVencimento").focus();
                $.unblockUI();
            }
        });
    };
    function onNewtStepBoleto() {
        salvarBoleto();
    }

    function salvarBoleto() {

        var idLancamentos = $('#idLancamento').val();
        if (idLancamentos === '') {
            if (confirm('Confirmar emissao de boleto?')) {
                $.blockUI({ message: 'Gerando boleto aguarde...' });
                $.ajax({
                    type: "POST",
                    url: base_url+"clientes/cadFinanceiroOnlineIntegracaoSubmit",
                    data: $('#form').serialize(),
                    dataType: 'json',
                    success: function (boleto) {
                        if (boleto.error !== undefined) {
                            if (boleto.error.reasons[0] === 'Buyer address can not contain empty fields') {
                                alert("O endereco digitado para o cliente nao e valido. Verifique, pode estar usando um CEP generico. Volte a primeira etapa edite o endereco, e tente gerar o boleto novamente!");
                                $.unblockUI();
                            } else  {
                                alert(boleto.error.reasons[0] )
                            }
                        } else {
                            $('#idLancamento').val(boleto.fatura_id);
                            $('#valorVencimento').attr('disabled', true);
                            $('#dataVencimento').attr('disabled', true);
                            $('#limitePagamento').attr('disabled', true);
                            $('#descricaoBoleto').attr('disabled', true);

                            $('#imprimirBoleto').attr('href', boleto.url_boleto);
                            $('#divImprimirBoleto').show();
                            $.unblockUI();
                        }
                    }
                });
            }
        }
    }

    var validateSteps = function (stepnumber, nextstep) {

        var isStepValid = false;
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                if(nextstep === 2){
                    salvarPessoaPrincial();
                } else if (nextstep === 3) {
                    onNewtStepBoleto();
                }

                for (var i=stepnumber; i<=nextstep; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            animateBar(nextstep);
            return true;
        } 
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
            validateCheckRadio();
        }
    };
}();