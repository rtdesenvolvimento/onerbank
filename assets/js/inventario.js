$(document).ready(function () {

    var pressed = false;
    var chars = [];

    $(window).keypress(function(e) {

        if(e.key === '%') { pressed = true; }

        chars.push(String.fromCharCode(e.which));

        if (pressed === false) {
            setTimeout(function(){
                if (chars.length >= 8) {
                    var barcode = chars.join("");
                    $( "#idProduto" ).focus().autocomplete( "search", barcode );
                }
                chars = [];
                pressed = false;
            },200);
        }
        pressed = true;
    });

    $("#idProduto").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'get',
                url: BASE_URL+"index.php/produtos/suggestions",
                dataType: "json",
                data: {
                    term: request.term,
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        autoFocus: false,
        delay: 200,
        response: function (event, ui) {
            if ($(this).val().length >= 16 && ui.content[0].id === 0) {

                bootbox.alert('Nenhum resultado correspondente encontrado, por favor, tente novamente', function () {
                    $('#idProduto').focus();
                });

                $(this).val('');
            } else if (ui.content.length === 1 && ui.content[0].id !== 0) {
                ui.item = ui.content[0];
                $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                $(this).autocomplete('close');
            } else if (ui.content.length === 1 && ui.content[0].id === 0) {
                bootbox.alert('Nenhum resultado correspondente encontrado, por favor, tente novamente', function () {
                    $('#idProduto').focus();
                });

                $(this).val('');
            }
        },select: function (event, ui) {
            event.preventDefault();

            if (ui.item.id !== 0) {
                var produto_id = ui.item.id ;
                var label = ui.item.label;
                var precoCompra = ui.item.precoCompra;

                $('#custo').val(precoCompra);
                $('#idProduto').val(label);
                $('#product_id').val(produto_id);

                $('#quantidade').focus();
            }
        }
    });

    $('#idProduto').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            $(this).autocomplete("search");
        }
    });
});

function verificarCancelamento() {
    var zerarEstoqueProdutosForaInventario = $('#zerarEstoqueProdutoForaInventario').val();

    if (zerarEstoqueProdutosForaInventario === 'S') {
        if (confirm('Deseja realmente Confirmar este Inventário? Tenha cuidado, essa ação irá ZERAR o estoque dos produtos que não forma adicionados ao inventário!')) {
            return true;
        } else {
            return false;
        }
    }
    return true;
}

function verificarGeracaoInventarioEstoqueAtual() {
    if (confirm('Deseja realmente criar o inventário com o estoque atual? ATENÇÃO:. O sistema irá excluir os itens atualmente inseridos neste inventário.')) {
        return true;
    }
    return false;
}
