var link_nfce = '';
var pi = 'amount_1';
var cat_id = "1";
var funcaoDePagamento;
var funcaoVerificarPedidosEmEspera;
var EM_MODO_DE_FECHAMENTO_RAPIDO = false;
var PROCESSANDO_PAGAMENTO = false;
var TOTAL_PARCELADO = 0;
var LANCAMENTO_ID = null;

$(document).ready(function(){
    $(".money").maskMoney();

    $('#add_item').focus();

    atualizarGridProdutos();

    $(document).on('click', '#submit-sale', function () {
        enviarPagamento();
    });

    $('#imprimir-naofiscal').click(function (event) {
        imprimirVendaNaoFiscal();
    });

    $('#imprimir').click(function (event) {
        var vendas_id = $('#vendas_id').val();
        $('#paymentModal').modal('hide');
        gerarNFCE(vendas_id);
    });

    $('#payment').click(function (event) {

        if( $("#aguardandoPagamento").is(":checked") === false) {
            $('#diGrupoLinkGerado').hide();
            $('#divAguardandoPagamento').hide();
        }

        if( !$('#paymentModal').is(':visible')) {
            setTimeout(function (args) {
                $('#amount_1').focus();
                $('#amount_1').select();

                $('#amount_1').val('');
                $('#desconto').val('0.00');
                $('#acrescimo').val('0.00');
                $('#cartaoCredito').val('0.00');
                $('#cartaoDebito').val('0.00');

                atualizarTotalPago();
            }, 1000);
        }

     });

    $('#amount_1').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#pAcrescimo').focus();
    });

    $('#pAcrescimo').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#pDesconto').focus();
    });

    $('#pDesconto').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#cartaoCredito').focus();
    });

    $('#cartaoCredito').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#cartaoDebito').focus();
    });

    $('#cartaoDebito').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            if (!PROCESSANDO_PAGAMENTO) {
                $('#submit-sale').click();
                PROCESSANDO_PAGAMENTO = true;
            }
        }
    });

    $('#parcelas').bind('keypress', function (e) {
        e.preventDefault();
        if (e.keyCode === 13) $('#tipoCobranca').focus();
    });

    $('#tipoCobranca').bind('keypress', function (e) {
        e.preventDefault();
        if (e.keyCode === 13) {
            //$('#parcela1').focus();
            //document.getElementById('parcela1').focus();
            setTimeout($('#parcela1').focus(),3000);
        }
    });

    $('#tipoPessoa').bind('keypress', function (e) {
        e.preventDefault();
        if (e.keyCode === 13) $('#documento').focus();
    });

    $('#documento').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#nomeCliente').focus();
    });

    $('#nomeCliente').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#telefone').focus();
    });

    $('#telefone').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#celular').focus();
    });

    $('#celular').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#email').focus();
    });

    $('#email').bind('keypress', function (e) {
        if (e.keyCode === 13) $('#btnCadastroCiente').click();
    });

    $('#print_bill').click(function () {
        window.open(BASE_URL+'pdv/orcamento/' + $('#vendas_id').val() + '', '',
            'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
    });

    $('#amount_1').keypress(function (event) {
        atualizarTotalPago();
    });

    $('#cartaoCredito').keypress(function (event) {
        atualizarTotalPago();
    });

    $('#cartaoDebito').keypress(function (event) {
        atualizarTotalPago();
    });

    $('#pDesconto').keyup(function (event) {
        atualizarTotalPago();
    });

    $('#pAcrescimo').keyup(function (event) {
        atualizarTotalPago();
    });

    $('#pos-sale-form').submit(function (event) {
        event.preventDefault();
        return false;
    });

    $(function(){
        $('.money').click(function(){$(this).select();});
        $('.money').focus(function(){$(this).select();});
    });

    selecaoProximoCampoParcela();

    $(document).on('click', '#gerador-link', function(event) {
        var venda_id = $('#vendas_id').val();
        var faltaPagar = $('#falta_pagar').html().replace('R$','').replace(',','.').trim();
        var parcelas = $('#parcelas').val();

        $.ajax({
            type: "POST",
            url: BASE_URL+"index.php/pdv/gerarLinkPagamentoPDV",
            data: {
                venda_id : venda_id,
                faltaPagar: faltaPagar,
                parcelas: parcelas
            },
            dataType: 'json',
            success: function(data) {
                $('#divLinkGerado').html(data.url);
                $('#diGrupoLinkGerado').show();
                $('#divAguardandoPagamento').show();
            }
        });
        return false;
    });

    $('#tipoPagamento').change(function (event) {
        verificaTipoPagamento();
    });

    $('#parcelas').change(function (event) {
        gerarTabelaDeParcelamento();
    });

    $(document).on('click', '#aguardandoPagamento', function(event) {
        verificarPagamentoLinkDePagamento();
    });

    $(document).on('click','#btNovoPedido', function (event) {
        if (confirm('Deseja realmente abrir uma nova Venda?')){
            window.location.href = BASE_URL+'index.php/pdv/excluirVenda/'+$('#vendas_id').val();
        }
    });

    $(document).on('click','#tipoDesconto', function (event) {
        atualizarTotalPago();
    });

    $(document).on('click', '#venda-espera', function (event) {
        porPedidoEmEspera();
    });

    $(document).on('click', '#btVoltarItens', function (event) {
        $('#ajaxproducts').show();
        $('#voltarItens').hide();
        $('#ulPedidosEmEspera').show();
        $('#add_item').focus();
        $('#ajaxpedidosespera').hide();

        clearInterval(funcaoVerificarPedidosEmEspera);
    });

    $(document).on('click', '#btPedidosEmEspera', function (event) {
        verificarPedidosEmEspera();
        funcaoVerificarPedidosEmEspera = setInterval(function(){
            verificarPedidosEmEspera();
        }, 5000);
    });

    funcaoDePagamento = setInterval(function () {
        verificarPagamentoLinkDePagamento();
    }, 5000);
});

function selecaoProximoCampoParcela() {
    let table = $('#tbParcelamento');

    table.on('keyup', 'input', function(event) {
        if (event.which === 13) {

            let generico = table.find('input:visible');
            let indice = generico.index(event.target) + 1;
            let seletor = $(generico[indice]).focus();

            if (seletor.length === 0) {
                if (!PROCESSANDO_PAGAMENTO){
                    $('#submit-sale').click();
                    PROCESSANDO_PAGAMENTO = true;
                }
            }
        }
    });
}

function porPedidoEmEspera() {
    var resposta = prompt('Digite uma nota de referência e confirme para  por essa venda em espera.',REF);

    if (resposta !== '' && resposta != null) {

        var vendas_id = $('#vendas_id').val();

        $.ajax({
            type: "POST",
            url: BASE_URL+"index.php/pdv/porVendaEmEspera",
            data: {
                vendas_id: vendas_id,
                referencia_espera: resposta
            },
            dataType: 'json',
            success: function (venda) {
                abrirVenda(venda.id)
            }
        });
    }
}

function enviarPagamento() {

    var faltaPagar = parseFloat($('#falta_pagar').html().replace('R$','').replace(',','.').trim());
    var tipoPagamento = $('#tipoPagamento').val();

    if (faltaPagar < 0) {
        bootbox.alert('Não é possível encerrar a venda com valor zerado.', function (e) {  setTimeout( function () {
            PROCESSANDO_PAGAMENTO = false;

            desativarFechamentoRapidoSubTotal();
        }, 800) });

        return false;
    }

    if (faltaPagar > 0 && tipoPagamento === '1') {

        bootbox.alert('Não é possível finalizar a venda pois falta pagar R$ '+faltaPagar, function (e) {  setTimeout( function () {
            PROCESSANDO_PAGAMENTO = false;

            $('#amount_1').focus();

        }, 800) });

        return false;
    }

    let totalValorParcelado = totalValorVencimentoParcelado();
    let clienteId = $('#clientes_id').val();

    if (totalValorParcelado > 0 && clienteId === CLIENTE_PADRAO) {

        bootbox.alert('Não é possivel parcelar usando consumidor não identificado.', function (e) {  setTimeout( function () {
            PROCESSANDO_PAGAMENTO = false;

            desativarFechamentoRapidoSubTotal();

            $('#clientes_id').select2("open");
        }, 800) });

        return false;
    }

    $.blockUI({message: '<h3><img src="'+BASE_URL+'assets/js/images/busy.gif" /> Aguarde! Fechando documento...</h3>'});
    $('#paymentModal').modal('hide');

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/pagar",
        data: $('#pos-sale-form').serialize(),
        dataType: 'json',
        success: function (venda) {

            let totalParcelado = parseFloat(venda.valorParcelado);
            let contaReceberId = venda.lancamentoParceladoId;

            if (VENDA_EM_MODO_FISCAL) {
                TOTAL_PARCELADO = totalParcelado;
                LANCAMENTO_ID = contaReceberId;

                gerarNFCE($('#vendas_id').val());
            } else {
                if (totalParcelado > 0) abrirNotaPromissoria(contaReceberId);

                imprimirVendaNaoFiscal();

                location.reload();
            }
            //notificacaoDePagamento();
        },
        error: function (erro) {
            alert(erro);
            $.unblockUI();
        }
    });
    return false;
}

function abrirVenda(id) {
    window.location = BASE_URL+'index.php/pdv/edit/'+id;
}

function imprimirVendaNaoFiscal() {
    var vendas_id = $('#vendas_id').val();

    window.open(BASE_URL+'pdv/impressao/' + vendas_id + '', '',
        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
}

function abrirNotaPromissoria(lancamento) {
    window.open(BASE_URL+'promissoria/gerarPromissoriaContaReceber/' + lancamento + '', '',
        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
}

function abrirNotaPromissoriaParcelamento(lancamento, valor) {
    if (valor > 0) abrirNotaPromissoria(lancamento);
}

function notificacaoDePagamento() {

    $('#submit-sale').addClass('btn-success');
    $('#submit-sale').html("PAGO !");

    $(":input").prop("disabled", true);
    $(".posdel").prop("disabled", true);
    $('#editCliente').prop("disabled", true);
    $('#adicionarCliente').prop("disabled", true);

    $("#submit-nfce").prop("disabled", false);
    $("#recarregar").prop("disabled", false);
    $('#fechamento-click').prop("disabled", false);
    $('#imprimir-naofiscal').prop("disabled", false);

    $('#recarregar').show();
    $('#submit-nfce').show();
    $('#imgPagamentoRealizadoComSucesso').show();

    $('#gerador-link').hide();
    $('#submit-sale').hide();
    $('#tbValoresPagamento').hide();
    $('#venda-espera').hide();

    $('#submit-nfce').focus();

    $('#imprimir-naofiscal').show();

    clearInterval(funcaoDePagamento);
}

function atualizarGridProdutos() {
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/montarTabelaDeProdutos",
        data: {
            venda_id: VENDA,
            grupo_id : GRUPO_SELECIONADO
        },
        dataType: 'html',
        success: function (html) {
            $('#ajaxproducts').html(html);

            $('.product').click(function (evet) {
                adicionarProduto($(this));
            });
        }
    });
}

function limparDadosCliente() {
    $('#tipoPessoa').val('PF');
    $('#clienteId').val('');
    $('#documento').val('');
    $('#nomeCliente').val('');
    $('#telefone').val('');
    $('#celular').val('');
    $('#email').val('');
}

function preencherDadosCliente() {
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/clientes/consultaCliente",
        data: {
            cliente_id: $('#clientes_id').val()
        },
        dataType: 'json',
        success: function (cliente) {

            if (cliente.tipoPessoa === 'PF') $("#documento").mask("999.999.999-99");
            else $("#documento").mask("99.999.999/9999-99");

            $('#clienteId').val(cliente.idClientes);
            $('#tipoPessoa').val(cliente.tipoPessoa);
            $('#documento').val(cliente.documento);
            $('#nomeCliente').val(cliente.nomeCliente);
            $('#telefone').val(cliente.telefone);
            $('#celular').val(cliente.celular);
            $('#email').val(cliente.email);

            $('#cadClientes').modal('show');

            setTimeout( function () {
                $('#documento').focus();
            }, 800);
        }
    });
}

function verificarPedidosEmEspera() {
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/montarTabelaDePedidosEmEspera",
        data: {},
        dataType: 'html',
        success: function (html) {
            $('#ajaxpedidosespera').html(html);
            $('#ajaxpedidosespera').show();
            $('#add_item').focus();

            $('#ajaxproducts').hide();
            $('#voltarItens').show();
            $('#ulPedidosEmEspera').hide();
        }
    });
}

function verificaTipoPagamento() {

    if ($('#tipoPagamento').val() === '1') {
        $('#avista').show();
        $('#aprazo').hide();

    } else {
        $('#avista').hide();
        $('#aprazo').show();

        gerarTabelaDeParcelamento();
    }
}

function verificarPagamentoLinkDePagamento() {
    if( $("#aguardandoPagamento").is(":checked") === true) {

        var vendas_id = $('#vendas_id').val();

        $("#pos-sale-form :input").prop("disabled", true);
        $('#aguardandoPagamento').prop('disabled', false);
        $('#imgAguardandoPagamento').show();
        verificaTipoPagamento();

        $.ajax({
            type: "POST",
            url: BASE_URL+"index.php/pdv/consultarPagamentoVendaPDV",
            data: {
                vendas_id: vendas_id
            },
            dataType: 'json',
            success: function (venda) {

                if (venda.faturado === '1') {

                    notificacaoDePagamento();

                    $('#diGrupoLinkGerado').hide();

                    $('#imgAguardandoPagamento').attr('src',BASE_URL+'assets/js/images/sucesso.gif');
                    $('#imgAguardandoPagamento').css('width','30%');

                    var faltaPagar = $('#falta_pagar').html().replace('R$','').replace(',','.').trim();

                    $('#cartaoCredito').val(faltaPagar);
                    atualizarTotalPago();
                    clearInterval(funcaoDePagamento);

                    $('#submit-nfce').focus();
                }
            }
        });
    }
}

$(document).ready(function() {

    var pressed = false;
    var chars = [];

    $(window).keypress(function(e) {
        if(e.key === '%') { pressed = true; }

        chars.push(String.fromCharCode(e.which));

        if (pressed === false) {
            setTimeout(function(){
                if (chars.length >= 8) {
                    var barcode = chars.join("");
                    $( "#add_item" ).focus().autocomplete( "search", barcode );
                }
                chars = [];
                pressed = false;
            },200);
        }
        pressed = true;
    });

    $("#add_item").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: 'get',
                url: BASE_URL+"index.php/produtos/suggestions",
                dataType: "json",
                data: {
                    term: request.term,
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        autoFocus: false,
        delay: 200,
        response: function (event, ui) {

            if (ui.content.length === 0) {
                bootbox.alert('Nenhum resultado correspondente encontrado, por favor, tente novamente', function () {
                    setTimeout(function (){
                        $('#add_item').focus();
                    }, 600);
                });
                $(this).val('');
            } else if (ui.content.length === 1 && ui.content[0].id !== 0) {
                let item = ui.content[0];
                if (adicionarItemAoPDV(item)) {
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                }
            }
        },
        select: function (event, ui) {
            event.preventDefault();

            let item = ui.item;

            if (item === undefined) return;

            if (item.id !== 0) {
                adicionarItemAoPDV(ui.item);
            } else {
                bootbox.alert('Nenhum resultado correspondente encontrado, por favor, tente novamente');
            }
        }
    });

    $('#add_item').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            $(this).autocomplete("search");
        }
    });

    $('#qtdItemAdicionar').bind('keypress', function (e) {
        if (e.keyCode === 13) {
            $('#add_item').focus();
        }
    });

    $('#reset').click(function (event) {
       excluirVenda();
    });

    $(document).on('click', '.quick-cash', function () {

        var $quick_cash = $(this);
        var amt = $quick_cash.contents().filter(function () {
            return this.nodeType === 3;
        }).text();

        var th = ',';
        var $pi = $('#' + pi);

        amt = formatDecimal(amt.split(th).join("")) * 1 + $pi.val() * 1;
        $pi.val(formatDecimal(amt)).focus();

        var note_count = $quick_cash.find('span');

        if (note_count.length === 0) $quick_cash.append('<span class="badge">1</span>');
        else note_count.text(parseInt(note_count.text()) + 1);

        atualizarTotalPago();
    });

    $(document).on('click', '.posdel', function () {
        var row = $(this).closest('tr');
        var idItens = $(this).attr('iditens');
        excluirProduto(idItens, row);
    });

    $("#expandir_tela").click(function (event) {
        var el = document.documentElement, rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen;
        rfs.call(el);
    });

    $(document).on('click', '.category', function () {
        var id = $(this).val();
        window.location = BASE_URL+'index.php/pdv/edit/'+VENDA+'/'+id;
    });

    $('#category-' + cat_id).addClass('active');

    $(".open-category").click(function () {
        $('#category-slider').toggle('slide', { direction: 'right' }, 700);
    });

    $(document).on('click', function(e){
        if (!$(e.target).is(".open-category, .cat-child") && !$(e.target).parents("#category-slider").size() && $('#category-slider').is(':visible')) {
            $('#category-slider').toggle('slide', { direction: 'right' }, 700);
        }
    });

    $('#recarregar').click(function (event) {
        if (FATURADA === '1' || VENDA_STATUS === '2') {
            if (confirm('Deseja realmente finalizar esta venda?')) {
                $.ajax({
                    type: "POST",
                    url: BASE_URL + "index.php/pdv/finalizarVendaEmEspera",
                    data: {
                        vendas_id: VENDA
                    },
                    dataType: 'html',
                    success: function (html) {
                        window.location = BASE_URL + 'index.php/pdv';
                    }
                });
            }
        } else {
            window.location = BASE_URL + 'index.php/pdv';
        }
    });
});

function adicionarItemAoPDV(ui) {

    var estoque = paraNumerico(ui.estoque);
    var preco = ui.precoVenda;
    var custo = ui.precoCompra;
    var vendas_id = $('#vendas_id').val();
    var produto_id = ui.id ;
    var quantidade = paraNumerico($('#qtdItemAdicionar').val());
    let unidade = ui.unidade;

    if (quantidade === '') quantidade = 1;

    if (!verificaEstoqueDisponivel(quantidade, estoque, unidade)) return false;

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/adicionarProdutoPDV",
        data: {
            preco: preco,
            custo: custo,
            valorVenda: preco,
            quantidade: quantidade,
            idProduto: produto_id,
            idVendasProduto: vendas_id,
        },
        dataType: 'json',
        success: function (data) {
            atualizarGridProduto(data, preco);
        }
    });

    return true;
}

function verificaEstoqueDisponivel(quantidadeDigitada, estoqueDisponivel, unidade) {

    if (PERMITE_ESTOQUE_NEGATIVO) return true;
    if (quantidadeDigitada <= estoqueDisponivel) return true;

    bootbox.alert('Não há estoque o suficiente para adicionar o item ao carrinho!<br/>Estoque atual do produto disponível: '+estoqueDisponivel+' '+unidade, function () {
        setTimeout(function () {
            $('#qtdItemAdicionar').focus();
        }, 800);
    });

    $('#qtdItemAdicionar').val('1');
    $('#add_item').val('');

    return false;
}

function gerarTabelaDeParcelamento() {
    var faltaPagar = $('#falta_pagar').html().replace('R$','').replace(',','.').trim();
    var parcelas = $('#parcelas').val();

    if (parcelas === '') $('#tbParcelamento tbody').html('');

    else {
        $.ajax({
            type: "POST",
            url: BASE_URL + "index.php/pdv/getTabelaDeParcelamento",
            data: {
                faltaPagar: faltaPagar,
                condicao: parcelas
            },
            dataType: 'html',
            success: function (html) {
                $('#tbParcelamento tbody').html(html);
                $('#parcelas').focus();

                $('.money').click(function(){$(this).select();});
                $('.money').focus(function(){$(this).select();});
            }
        });
    }
}

function adicionarProduto(tag) {

    var produto = tag[0];
    var preco = produto.getAttribute('preco');
    var custo = produto.getAttribute('custo');
    var vendas_id = produto.getAttribute('vendas_id');
    var produto_id = produto.id;
    var quantidade = $('#qtdItemAdicionar').val();
    if (quantidade === '') quantidade = 1;

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/adicionarProdutoPDV",
        data: {
            preco: preco,
            custo: custo,
            valorVenda: preco,
            idProduto: produto_id,
            idVendasProduto: vendas_id,
            quantidade: quantidade
        },
        dataType: 'json',
        success: function (data) {
            atualizarGridProduto(data, preco);
        }
    });
}

function fechamentoRapidoSubTotal() {

    EM_MODO_DE_FECHAMENTO_RAPIDO = true;

    $('.fechamento').removeClass('totalizador');
    $('.fechamento').addClass('sub-total');

    $('#lbTotalizador').html('SubTotal');
    $('#reset').hide();
    $('#print_bill').hide();
    $('#fechamento-click').hide();

    $('#exibir-atalhos-pagamento').show();

    $('#qtdItemAdicionar').prop("disabled", true);
    $('#add_item').prop("disabled", true);
    $(".product ").prop("disabled", true);
}

function desativarFechamentoRapidoSubTotal() {

    EM_MODO_DE_FECHAMENTO_RAPIDO = false;
    PROCESSANDO_PAGAMENTO = false;

    $('.fechamento').addClass('totalizador');
    $('.fechamento').removeClass('sub-total');

    $('#lbTotalizador').html('Total');
    $('#reset').show();
    $('#print_bill').show();
    $('#fechamento-click').show();

    $('#exibir-atalhos-pagamento').hide();
    $('#sckModalFormasDePagamentoRapido').modal('hide');
    $('#paymentModal').modal('hide');

    $('#qtdItemAdicionar').prop("disabled", false);
    $('#add_item').prop("disabled", false);
    $(".product ").prop("disabled", false);

    $('#qtdItemAdicionar').focus();
}

function fechamentoRapidoFormaDePagamento(formaPagamento) {

    if (!EM_MODO_DE_FECHAMENTO_RAPIDO) return;
    if (PROCESSANDO_PAGAMENTO) return;

    PROCESSANDO_PAGAMENTO = true;

    $('#sckModalFormasDePagamentoRapido').modal('hide');
    $('#paymentModal').modal('hide');

    $.blockUI({message: '<h3><img src="'+BASE_URL+'assets/js/images/busy.gif" /> Aguarde Fechando o documento em ('+formaPagamento+')...</h3>'});

    $('#acrescimo').val('0.00');
    $('#desconto').val('0.00');
    $('#tbParcelamento tbody').html('');

    atualizarTotalPago();

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/pagar",
        data: $('#pos-sale-form').serialize(),
        dataType: 'json',
        success: function (data) {
            $.unblockUI();

            if (!VENDA_EM_MODO_FISCAL){
                imprimirVendaNaoFiscal();
                location.reload();
            } else {
                gerarNFCE($('#vendas_id').val());
            }
        },
        error: function(data) {
            alert(data);
            $.unblockUI();
        }
    });
}

function fechamentoRapidoEmDinheiro() {
    $('#amount_1').val($('#total').html().replace('R$','').replace(',','.').trim());

    $('#cartaoCredito').val('0.00');
    $('#cartaoDebito').val('0.00');

    fechamentoRapidoFormaDePagamento('DINHEIRO');
}

function fechamentoRapidoEmCartaoDeCredito() {
    $('#cartaoCredito').val($('#total').html().replace('R$','').replace(',','.').trim());

    $('#amount_1').val('0.00');
    $('#cartaoDebito').val('0.00');

    fechamentoRapidoFormaDePagamento('CARTÃO DE CRÉDITO');
}

function fechamentoRapidoEmCartaoDeDebito() {
    $('#cartaoDebito').val($('#total').html().replace('R$','').replace(',','.').trim());

    $('#amount_1').val('0.00');
    $('#cartaoCredito').val('0.00');

    fechamentoRapidoFormaDePagamento('CARTÃO DE DÉBITO');
}

function atualizarGridProduto(data, preco) {

    var newProduto = $('<tr></tr>');
    var tr_html = '';

    tr_html = tr_html + '<td>';
    tr_html = tr_html + '<span class="sname">'+data.descricao+'</span>';
    tr_html = tr_html + '</td>';
    tr_html = tr_html + '    <td class="text-right">';
    tr_html = tr_html + '    <span class="text-right sprice">'+preco+'</span>';
    tr_html = tr_html + ' </td>';
    tr_html = tr_html + '<td>';
    tr_html = tr_html + '<input class="form-control input-sm kb-pad text-center rquantity" id="idItens_'+data.idItens+'" minlength="1" min="1" onchange="atualizarProduto(this, event);" valorVenda="'+data.valorVenda+'" iditens="'+data.idItens+'" name="quantidade" type="number" value="'+data.quantidade+'">';
    tr_html = tr_html + '    </td>';
    tr_html = tr_html + '    <td class="text-right">';
    tr_html = tr_html + '    <span class="text-right ssubtotal">'+data.subTotal+'</span>';
    tr_html = tr_html + '</td>';
    tr_html = tr_html + '<td class="text-center" iditens="'+data.idItens+'">';
    tr_html = tr_html + '    <i class="fa fa-times tip pointer posdel" iditens="'+data.idItens+'" title="Remove" style="cursor:pointer;"></i>';
    tr_html = tr_html + '</td>';

    newProduto.html(tr_html);

    newProduto.prependTo("#posTable");
    $('#qtdItemAdicionar').val('1');

    totalItens();
    totalPedido();
    atualizarTotalPago();
    atualizarGridProdutos();

    $('#add_item').val('');
    $('#add_item').focus();
}

function excluirProduto(idItens, row) {

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/excluirProdutoPDV",
        data: {
            idItens: idItens,
        },
        dataType: 'json',
        success: function (data) {
            row.remove();

            totalItens();
            totalPedido();
            atualizarTotalPago();
            atualizarGridProdutos();

            $('#add_item').focus();
        }
    });
}

function totalItens() {
    var data = $('.rquantity');
    var quantidade = 0;
    $.each(data, function(i, item) {
        var dQuantidade = parseFloat(item.value);
        quantidade = quantidade + dQuantidade;
    });
    $('#titems').html(quantidade);
}

function totalPedido() {

    var data = $('.ssubtotal');
    var subTotal = 0;

    $.each(data, function(i, item) {
        var dSubTotal = parseFloat(item.innerText);
        subTotal = subTotal + dSubTotal;
    });

    $('#total').html('R$ '+subTotal.toFixed(2));
    $('#twt').html('R$ '+subTotal.toFixed(2));

    $('#amount_1').val('');
}

function atualizarTotalPago() {

    var valorEmDinheiro = getDouble($('#amount_1').val());
    var cartaoCredito = getDouble($('#cartaoCredito').val());
    var cartaoDebito = getDouble($('#cartaoDebito').val());
    var totalPagar = getDouble($('#twt').html().replace('R$','').replace(',','.').trim());

    let desconto = $('#pDesconto').val() ? $('#pDesconto').val() : '0';
    let acrescimo = $('#pAcrescimo').val() ? $('#pAcrescimo').val() : '0';

    if (desconto.indexOf("%") === -1) {

        desconto = getDouble(desconto);

    } else {
        desconto = desconto.split("%");

        if (!isNaN(desconto[0])) desconto = (totalPagar*desconto[0])/100;
        else desconto = getDouble(desconto[0]);
    }


    if (acrescimo.indexOf("%") === -1) {

        acrescimo = getDouble(acrescimo);

    } else {
        acrescimo = acrescimo.split("%");

        if (!isNaN(acrescimo[0])) acrescimo = (totalPagar*acrescimo[0])/100;
        else acrescimo = getDouble(acrescimo[0]);
    }

    totalPagar = totalPagar + acrescimo - desconto ;
    totalPagar = getDouble(totalPagar.toFixed(2));

    var totalPago = valorEmDinheiro + cartaoCredito + cartaoDebito;
    var faltaPagar =  (totalPago - totalPagar)*-1;
    var troco = totalPago - totalPagar;

    if (cartaoCredito > 0 && troco > 0) {
        bootbox.alert('Não é possível informar um pagamento em cartão de crédito acima do valor que falta pagar, pois não podemos devolver troco de um crédito. ', function () {return;});

        $('#cartaoCredito').val('0.00');
        $('#cartaoCredito').focus();

        atualizarTotalPago();

        return;
    }

    if (cartaoDebito > 0 && troco > 0) {
        bootbox.alert('Não é possível informar um pagamento em cartão de débito acima do valor que falta pagar, pois não podemos devolver troco de um débito.', function () {return;});

        $('#cartaoDebito').val('0.00');
        $('#cartaoDebito').focus();

        atualizarTotalPago();
        return;
    }

    faltaPagar =  (totalPago - totalPagar)*-1;
    troco = totalPago - totalPagar;

    if  (totalPago >= totalPagar) {
        $('#falta_pagar').html('R$ 0,00');
        $('#balance').html('R$ ' + troco.toFixed(2) );
        $('#troco').val( troco.toFixed(2) );
    } else {
        $('#falta_pagar').html('R$ ' + faltaPagar.toFixed(2));
        $('#balance').html('R$ 0.00');
        $('#troco').val(0);
    }

    $('#total_paying').html('R$ ' + totalPago.toFixed(2));
    $('#subTotal_oferecido').html('R$ '+ totalPagar.toFixed(2) );
    $('#desconto_oferecido').html('R$ '+desconto.toFixed(2) );
    $('#acrescimo_oferecido').html('R$ '+acrescimo.toFixed(2) );
    $('#tbPagamentoDinheiro').html('R$ '+valorEmDinheiro);
    $('#tbPagamentoCredito').html('R$ '+cartaoCredito);
    $('#tbPagamentoDebito').html('R$ '+cartaoDebito);

    $('#desconto').val(desconto.toFixed(2) );
    $('#acrescimo').val(acrescimo.toFixed(2));

    gerarTabelaDeParcelamento();
}

function excluirVenda() {
    if (confirm('Deseja realmente excluir esta venda?')) {
        window.location.href = BASE_URL+'index.php/pdv/excluirVenda/'+$('#vendas_id').val();
    }
}

function atribuirVendaNaoFiscal() {
    var vendaId = $('#vendas_id').val();

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/vendaNaoFiscal",
        data: {
            vendaId: vendaId
        },
        dataType: 'json',
        success: function (retorno) {
            location.reload();
        }
    });
}


function atribuirVendaFiscal() {
    var vendaId = $('#vendas_id').val();

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/vendaFiscal",
        data: {
            vendaId: vendaId
        },
        dataType: 'json',
        success: function (retorno) {
            location.reload();
        }
    });
}

function atualizarProduto(tag, event) {

    var quantidade   = tag.value;
    var idItens      = tag.getAttribute('iditens');
    var valorVenda   = tag.getAttribute('valorVenda');

    if (quantidade === '') {
        quantidade = 1;
        tag.value = 1;
    }

    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/pdv/atualizarProdutoPDV",
        data: {
            idItens: idItens,
            quantidade: quantidade,
            valorVenda: valorVenda,
        },
        dataType: 'json',
        success: function (data) {
            recarregarItemNoPDV();
        }
    });
    return false;
}

function recarregarItemNoPDV() {
    $("#posTable").load(BASE_URL + "index.php/pdv #posTable", function () {
        totalItens();
        totalPedido();
        atualizarTotalPago();
        atualizarGridProdutos();
        $('#add_item').focus();
    });
}

function formatDecimal(x, d) {
    if (!d) { d =  2; }
    return parseFloat(accounting.formatNumber(x, d, '', '.'));
}

function gerarNFCE(idVendas) {

    var cpf = prompt('Digite o CPF (Apenas números) para informar na NFC-e ou deixe em branco, para continuar','');

    if (cpf !== '' && cpf !== null) {
        var valido = valida_cpf(cpf);

        if (!valido) {
            alert('CPF Digitado inválido!');
            return;
        }
    }

    $.ajax({
        type: "GET",
        url: BASE_URL+"index.php/pdv/gerarNFCE/"+idVendas,
        data : {
            cpf : cpf
        },
        dataType: "json",
        success: function (nfce) {
            if (nfce.nNF !== ''){
                link_nfce = '<div style="color: #ffffff;font-size: 14px;"> <br/>Para editar e corrigir as informações da NFC-e clique no link a seguir <a href="'+BASE_URL+'index.php/nfce/editar/'+nfce.nNF+'">NFC-e</a> </div>';
                GeraNFe(nfce.nNF);
            }
        }
    });
}

function GeraNFe(valor) {
    $.blockUI({message: '<h3><img src="'+BASE_URL+'assets/js/images/busy.gif" /> Aguarde Gerando XML...</h3>'});

    $.ajax({
        type: "GET",
        url: BASE_URL+"emissores/v4/nfephp-master/geracaonfce/gera_nfce.php?nNF="+valor,
        dataType: "json",
        success: function (data) {
            $.unblockUI();
            if (data.Status === 'Ok') {
                AssinaNFE(valor);
            } else {
                alert('Erro ao Exportar Arquivo.');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseText) {
                var str = jqXHR.responseText;
                var retorno = jQuery.parseJSON(str.substr(3));
                if (retorno.Status === 'Ok') {
                    SituacaoNFeA3();
                } else {
                    toastr.warning('Erro ao Exportar Arquivo.');
                }
            }
        }
    });
}

function AssinaNFE(valor) {
    $.blockUI({message: '<h3><img src="'+BASE_URL+'assets/js/images/busy.gif" /> Assinando XML...</h3>'});
    $.ajax({
        type: "GET",
        url: BASE_URL + "emissores/v4/nfephp-master/geracaonfce/assina_nfce.php",
        data: {
            arquivo: valor
        },
        dataType: "xml",
        success: function (retorno) {
            $.unblockUI();
            var status = $(retorno).find("status").text();

            if (status === 'OK') TransmiteNFE(valor);
            else $.blockUI({ css: { backgroundColor: '#a742a9', color: '#fff' } , message: $(retorno).find("motivo").text()+link_nfce  });
        }
    });
}

function TransmiteNFE(valor) {
    $.blockUI({message: '<h3><img src=""'+BASE_URL+'"assets/js/images/busy.gif" /> Enviando XML ao Sefaz...</h3>'});
    $.ajax({
        type: "GET",
        url: BASE_URL+"emissores/v4/nfephp-master/geracaonfce/envia_nfce.php?arquivo=" + valor,
        dataType: "xml",
        success: function (retorno) {
            $.unblockUI();
            var status = $(retorno).find("status").text();

            if (status === 'OK') RetornoNFE($(retorno).find("recibo").text(), valor);
            else $.blockUI({message: $(retorno).find("motivo").text()+link_nfce });
        }
    });
}


function RetornoNFE(valor, nct) {
    $.blockUI({message: '<h3><img src="'+BASE_URL+'assets/js/images/busy.gif" /> Retornando Dados do Sefaz</h3>'});
    $.ajax({
        type: "GET",
        url: BASE_URL+"emissores/v4/nfephp-master/geracaonfce/consulta_recibo.php?arquivo="+nct,
        dataType: "html",
        success: function (retorno) {
            $.unblockUI();
            var status = $(retorno).find("status").text();

            if (status === 'OK') {
                window.open(BASE_URL+'emissores/v4/sped-da-master/geracao/danfce.php?nNFbd=' + nct + '', '',
                    'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');

                if (TOTAL_PARCELADO > 0) abrirNotaPromissoria(LANCAMENTO_ID);

                location.reload();
            } else {
                $.blockUI({message: $(retorno).find("motivo").text()+link_nfce });
            }
        }
    });
}

function imprimirDANFE(nNF) {
    window.open(BASE_URL+'emissores/v4/sped-da-master/geracao/danfce.php?nNFbd=' + nNF + '', '',
        'height=600,width=800,scrollbars=yes,statusbar=no,resizable=no,toolbar=0');
}

function totalValorVencimentoParcelado() {

    let tagVencimentos = $("input[name='valorVencimento[]']");
    let valorTotalVencimentoParcelado = 0;

    $(tagVencimentos).each(function(index, element ){
        valorTotalVencimentoParcelado = +valorTotalVencimentoParcelado + parseFloat(element.value);
    });

    return valorTotalVencimentoParcelado;
}

function atualizarValorVencimentoAoEditarParcela(tag, pointer) {

    let valorDigitado = 0;
    let totalParcelasSuperior = 0;
    let tagVencimentos = $("input[name='valorVencimento[]']");
    let valorVencimento = $('#falta_pagar').html().replace('R$','').replace(',','.').trim();
    let totalParcelas =  tagVencimentos.length;

    if (valorVencimento === '') valorVencimento = 0;
    else valorVencimento = parseFloat(valorVencimento);

    $(tagVencimentos).each(function(index, element ){

        var valorElementoSuperior = element.value;

        if (index > pointer) {

            let novoVencimentoElement = (valorVencimento - valorDigitado)/(totalParcelas-totalParcelasSuperior);

            if (novoVencimentoElement <= 0) {
                alert('A somatória dos vencimentos não pode ultrapassou o valor da conta e o valor dos vencimentos das parcelas deve ser maior que ZERO.');

                tag.value = tag.getAttribute('vencimento');
                atualizarValorVencimentoAoEditarParcela(tag, pointer);
                return false;
            } else {
                element.value = novoVencimentoElement.toFixed(2);
                element.setAttribute('vencimento', novoVencimentoElement);
            }
        } else {
            valorDigitado += parseFloat(valorElementoSuperior);
            totalParcelasSuperior++;
        }
    });
}

function abrirFormularioCadastroCliente() {

    limparDadosCliente();

    $('#cadClientes').modal('show');

    setTimeout( function () {
        $('#documento').focus();
    }, 800);
}

shortcut.add('F8', function() {
    if (!EM_MODO_DE_FECHAMENTO_RAPIDO) fechamentoRapidoSubTotal();
    else desativarFechamentoRapidoSubTotal();

}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('F7', function() {

    if (EM_MODO_DE_FECHAMENTO_RAPIDO) {
        fechamentoRapidoEmCartaoDeCredito();
    } else {
        $('#btconsultaEstoque').click();
    }

}, { 'type':'keydown', 'propagate':false, 'target':document} );


shortcut.add('F9', function() {

    if (EM_MODO_DE_FECHAMENTO_RAPIDO) {
        fechamentoRapidoEmCartaoDeDebito();
    } else {
        if ($('#paymentModal').is(':visible')) {
            $('#imprimir-naofiscal').click();
        } else {
            $('#print_bill').click();
        }
    }

}, { 'type':'keydown', 'propagate':false, 'target':document} );


shortcut.add('F6', function() {
    fechamentoRapidoEmDinheiro();
}, { 'type':'keydown', 'propagate':false, 'target':document} );


shortcut.add('F2', function() {

    if (EM_MODO_DE_FECHAMENTO_RAPIDO) {
        $('#sckModalFormasDePagamentoRapido').modal('hide');

        $('#tipoPagamento').val('2');
        verificaTipoPagamento();

        $("#payment").trigger('click');

    } else {
        $('#qtdItemAdicionar').focus();
    }

}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('ESC', function() {
    desativarFechamentoRapidoSubTotal();
}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('F4', function() {

    if (EM_MODO_DE_FECHAMENTO_RAPIDO) {
        $('#sckModalFormasDePagamentoRapido').modal('show');
    } else {
        $('#reset').click();
    }

}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('F10', function() {

    if (EM_MODO_DE_FECHAMENTO_RAPIDO) {
        $('#sckModalFormasDePagamentoRapido').modal('hide');

        $('#tipoPagamento').val('1');
        verificaTipoPagamento();

        $("#payment").trigger('click');
        $('#amount_1').focus();
        $('#amount_1').select();
    }

    }, { 'type':'keydown', 'propagate':false, 'target':document} );


shortcut.add('Ctrl+F3', function() {
     $('#add_item').focus();
}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+X', function() {
    preencherDadosCliente();
}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+E', function() {
    $('#clientes_id').select2("open");

}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+I', function() {

    abrirFormularioCadastroCliente();

}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+Q', function() {
    if (VENDA_EM_MODO_FISCAL) atribuirVendaNaoFiscal();
    else atribuirVendaFiscal();
}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+P', function() {
    porPedidoEmEspera();
}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+M', function() {
    window.location = BASE_URL+"index.php/pdv";
}, { 'type':'keydown', 'propagate':false, 'target':document} );

shortcut.add('Ctrl+A', function() {
    $('#atalhos').click();
}, { 'type':'keydown', 'propagate':false, 'target':document} );


