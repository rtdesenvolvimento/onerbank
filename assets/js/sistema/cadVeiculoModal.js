
function buscarVeiculo(cliente_id, base_url) {

    var table       = $('#tblVeiculos');
    var $tbody      = table.append('<tbody />').children('tbody');

    limparCamposVeiculo();

    $tbody.empty();

    if (cliente_id) {
        $.ajax({
            type: "POST",
            url: base_url + "index.php/veiculos/getListaVeiculos",
            data: "clientes_id=" + cliente_id,
            dataType: 'html',
            success: function (html) {
                $('#divVeiculos').html(html);
                consultaVeiculoCliente(cliente_id, base_url);
            }
        });
    }
}

function consultaVeiculoCliente(cliente_id, base_url) {

    $("#veiculo_id").val('');
    $.ajax({
        type: "POST",
        url: base_url+"index.php/os/consultaVeiculo",
        data: "idCliente=" + cliente_id,
        dataType: 'json',
        success: function (data) {
            var option = '';
            option += '<option value="">--Selecine um veículo para o cliente--</option>'
            $.each(data, function (index, veiculo) {
                option += '<option tipoveiculo_id="'+veiculo.tipoveiculo_id+'" value="' + veiculo.idVeiculo + '">' + veiculo.tipoVeiculo + '/' + veiculo.modelo + '/' + veiculo.marca + '/' + veiculo.placa + '</option>';
            });
            $("#veiculo_id").html(option);
            limpar_alerta_veiculo();
        }
    });
}

function consultaVeiculoClientePreencher(cliente_id, base_url, veiculo_id) {

    $("#veiculo_id").val('');
    $.ajax({
        type: "POST",
        url: base_url+"index.php/os/consultaVeiculo",
        data: "idCliente=" + cliente_id,
        dataType: 'json',
        success: function (data) {
            var option = '';
            option += '<option value="">--Selecine um veículo para o cliente--</option>'
            $.each(data, function (index, veiculo) {
                option += '<option tipoveiculo_id="'+veiculo.tipoveiculo_id+'" value="' + veiculo.idVeiculo + '">' + veiculo.tipoVeiculo + '/' + veiculo.modelo + '/' + veiculo.marca + '/' + veiculo.placa + '</option>';
            });
            $("#veiculo_id").html(option);
            $("#veiculo_id").val(veiculo_id);
            limpar_alerta_veiculo();
        }
    });
}

function limparCamposVeiculo() {
    $("#marca").val('');
    $("#placa").val('');
    $("#municipio").val('');
    $("#uf").val('');
    $("#situacao").val('');
    $("#tipoVeiculo").val('');
    $('#tipoveiculo_id').val('');
    $("#cor").val('');
    $("#ano").val('');
    $('#idVeiculo').val('');
    $('#chassi').val('');
    $('#observacaoVeiculo').val('');
    $('#gasolina').prop( "checked", false );
    $('#etanol').prop( "checked", false );
    $('#diesel').prop( "checked", false );
    $('#gnv').prop( "checked", false );
    $("#modelo").val('').focus();
}

function vincularIdentificadorVeiculo(idVeiculo) {
    $('#idVeiculoArquivo').val(idVeiculo);
}

function exibirInformacoesAnexo(tag) {

    var link            = tag.getAttribute('link');
    var id              = tag.getAttribute('imagem');
    var data            = tag.getAttribute('data');
    var hora            = tag.getAttribute('hora');
    var observacaoimg   = tag.getAttribute('observacaoimg');
    var url             = BASE_URL + 'index.php/clientes/excluirAnexo/';

    $("#div-visualizar-anexo-veiculo").html('<img src="' + link + '" alt="">');
    $("#excluir-anexo-veiculo").attr('link', url + id);
    $('#link-visualizar').val(link);
    $('#observacaoimg-visualizar').val(observacaoimg);
    $('#data-visualizar').val(data);
    $('#hora-visualizar').val(hora);
    $('#abrir-imagem').attr('href',link);
    $("#download").attr('href', BASE_URL+"index.php/clientes/downloadanexo/" + id);
}

function excluir_editar_veiculo(tag) {

    var idVeiculo       = tag.getAttribute('idAcao');
    var idAcaoEditar    = tag.getAttribute('idAcaoEditar');

    if (idVeiculo != null) {
        if (confirm('Deseja realmente excluir o veículo?') ) {
            $.ajax({
                type: "POST",
                url: BASE_URL+"index.php/clientes/excluirVeiculo",
                data: "idVeiculo=" + idVeiculo,
                dataType: 'json',
                success: function (data) {
                    if (data.result === true) {
                        buscarVeiculo($('#clientes_id').val(), BASE_URL);
                        $('#situacao').css('background', '#eee');
                        liberado = false;
                    }
                    else {
                        alert('Ocorreu um erro ao tentar excluir o veiculo.');
                    }
                }
            });
        }
    } else if (idAcaoEditar != '' && idAcaoEditar != undefined) {

        var marca           = tag.getAttribute('marca');
        var placa           = tag.getAttribute('placa');
        var municipio       = tag.getAttribute('municipio');
        var uf              = tag.getAttribute('uf');
        var situacao        = tag.getAttribute('situacao');
        var tipoVeiculo     = tag.getAttribute('tipoVeiculo');
        var tipoveiculo_id  = tag.getAttribute('tipoveiculo_id');
        var cor             = tag.getAttribute('cor');
        var ano             = tag.getAttribute('ano');
        var modelo          = tag.getAttribute('modelo');
        var chassi          = tag.getAttribute('chassi');
        var observacao      = tag.getAttribute('observacao');
        var gasolina        = tag.getAttribute('gasolina');
        var etanol          = tag.getAttribute('etanol');
        var diesel          = tag.getAttribute('diesel');
        var gnv             = tag.getAttribute('gnv');

        $('#gasolina').prop( "checked", false );
        $('#etanol').prop( "checked", false );
        $('#diesel').prop( "checked", false );
        $('#gnv').prop( "checked", false );

        if (gasolina==1){
            $('#gasolina').prop( "checked", true );
        }

        if (etanol==1){
            $('#etanol').prop( "checked", true );
        }

        if (diesel==1){
            $('#diesel').prop( "checked", true );
        }

        if (gnv==1){
            $('#gnv').prop( "checked", true );
        }

        $("#marca").val(marca);
        $("#placa").val(placa);
        $("#municipio").val(municipio);
        $("#uf").val(uf);
        $("#situacao").val(situacao);
        $("#tipoVeiculo").val(tipoVeiculo);
        $("#cor").val(cor);
        $("#ano").val(ano);
        $('#idVeiculo').val(idAcaoEditar);
        $('#tipoveiculo_id').val(tipoveiculo_id);

        if (situacao != 'Sem restrição') {
            $('#situacao').css('background','red');
            $('#situacao').val(situacao);
            liberado = true;
            piscarVermelho();
        } else {
            $('#situacao').css('background','#71d80036');
            liberado = false;
        }

        $("#modelo").val(modelo).focus();
        $("#chassi").val(chassi).focus();
        $("#observacaoVeiculo").val(observacao).focus();
    }

}