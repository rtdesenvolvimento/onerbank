

$(document).ready(function () {

    $('#tipoPessoa').change(function (e) {
        $('#documento').val('');
        if ($(this).val() === 'PJ' ) {

            $('#tabContato').show();
            $('#div_sexo').hide();
            $('#div_rg').hide();
            $('#div_informacoes_fiscais').show();

            $('#div_dataNascimento').html('Data da fundação');
            $('#div_nome').html('Razão Social*');
            $('#div_nomeFantasia').html('Nome Fantasia');
            $('#div_cpfCnpj').html('CNPJ / Nome*');

            $("#documento").mask("99.999.999/9999-99");

        } else {
            $('#tabContato').hide();
            $('#div_sexo').show();
            $('#div_rg').show();
            $('#div_informacoes_fiscais').hide();

            $('#div_dataNascimento').html('Data de nascimento');
            $('#div_nome').html('Nome*');
            $('#div_nomeFantasia').html('Apelido');
            $('#div_cpfCnpj').html('CPF / Nome*');

            $("#documento").mask("999.999.999-99");
        }
    });
});

function getConsultaCEP() {
    if ($.trim($("#cep").val()) !== "") {
        var cep = $("#cep").val();
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        var url = 'https://api.postmon.com.br/v1/cep/'+cep;
        $.get(url,
            function (data) {
            if (data !== -1) {
                    $("#rua").val(data.logradouro);
                    $("#bairro").val(data.bairro);
                    $("#cidade").val(data.cidade);
                    $("#estado").val(data.estado);
                    $('#codIBGECidade').val(data.cidade_info.codigo_ibge);
                    $('#codIBGEEstado').val(data.estado_info.codigo_ibge);
                    $('#numero').focus();
                } else {
                    alert("Endereço não encontrado");
                }
            });
    }
}

function consultaPessoa(tagCpfCnpj, tipoPessoa, base_url) {

    var cpf_cnpj = tagCpfCnpj.val();

    if (cpf_cnpj != '' && cpf_cnpj != '___.___.___-__' && cpf_cnpj != '__.___.___/____-__') {
        if (tipoPessoa === 'PJ') {
            if (!valida_cnpj(cpf_cnpj)) {
                alert('CNPJ inválido!');
                tagCpfCnpj.val('');
            } else {

                var cpf_cnpj = cpf_cnpj.replace(/[^0-9]/g, '');
                $("#divProgress").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>Buscando informações do cliente</div></div>");

                $.ajax({
                    type: "POST",
                    url: base_url + "index.php/clientes/getConsutaWSCliente/"+cpf_cnpj,
                    dataType: 'json',
                    success: function (empresa) {

                        if (empresa.status === undefined) {
                            alert("Não foi possível encontrar o cnpj");
                            return;
                        }

                        if (empresa.status === 'ERROR') {
                            $("#divProgress").html('');
                            alert(empresa.message);
                            return;
                        }
                        var d = new Date(empresa.abertura);
                        var date = [
                            d.getFullYear(),
                            ('0' + (d.getMonth() + 1)).slice(-2),
                            ('0' + d.getDate()).slice(-2)
                        ].join('-');

                        if (empresa.situacao !== 'ATIVA') {

                            if (confirm('Esta empres encontra-se na situação '+empresa.situacao+', motivo ' + empresa.motivo_situacao+'. Deseja realmente importar seus dados?')) {
                                $('#nomeCliente').val(empresa.nome);
                                $('#nomeFantasiaApelido').val(empresa.fantasia);
                                $('#telefone').val(empresa.telefone);
                                $('#email').val(empresa.email);
                                $('#data_nascimento').val(date);
                                $('#cep').val(empresa.cep);
                                $('#rua').val(empresa.logradouro);
                                $('#numero').val(empresa.numero);
                                $('#complemento').val(empresa.complemento);
                                $('#bairro').val(empresa.bairro);
                                $('#cidade').val(empresa.municipio);
                                $('#estado').val(empresa.uf);

                                if (empresa.atividade_principal.length > 0) {
                                    $('#observacao').val(empresa.atividade_principal[0].text);
                                }
                            }
                        } else {
                            $('#nomeCliente').val(empresa.nome);
                            $('#nomeFantasiaApelido').val(empresa.fantasia);
                            $('#telefone').val(empresa.telefone);
                            $('#email').val(empresa.email);
                            $('#data_nascimento').val(date);
                            $('#cep').val(empresa.cep);
                            $('#rua').val(empresa.logradouro);
                            $('#numero').val(empresa.numero);
                            $('#complemento').val(empresa.complemento);
                            $('#bairro').val(empresa.bairro);
                            $('#cidade').val(empresa.municipio);
                            $('#estado').val(empresa.uf);

                            if (empresa.atividade_principal.length > 0) {
                                $('#observacao').val(empresa.atividade_principal[0].text);
                            }
                        }
                        $("#divProgress").html('');
                        getConsultaCEP();
                    }
                });
            }
        } else {
            if (!valida_cpf(cpf_cnpj)) {
                alert('CPF inválido!');
                tagCpfCnpj.val('');
            }
        }
    }
}