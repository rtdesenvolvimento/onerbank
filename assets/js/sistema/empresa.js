function consultaDadosEmpresa(base_url) {

    var cpf_cnpj = $('#documento').val().replace(/[^0-9]/g, '');
    $("#divProgress").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>Buscando informações do cliente</div></div>");

    $.ajax({
        type: "POST",
        url: base_url + "index.php/clientes/getConsutaWSCliente/"+cpf_cnpj,
        dataType: 'json',
        success: function (empresa) {

            debugger;

            if (empresa.status === undefined) {
                alert("Não foi possível encontrar o cnpj");
                return;
            }

            if (empresa.status === 'ERROR') {
                $("#divProgress").html('');
                alert(empresa.message);
                return;
            }
            var d = new Date(empresa.abertura);
            var date = [
                d.getFullYear(),
                ('0' + (d.getMonth() + 1)).slice(-2),
                ('0' + d.getDate()).slice(-2)
            ].join('-');

            if (empresa.situacao !== 'ATIVA') {

                if (confirm('Esta empres encontra-se na situação '+empresa.situacao+', motivo ' + empresa.motivo_situacao+'. Deseja realmente importar seus dados?')) {

                    $('#nomeEmpresa').val(empresa.nome);
                    $('#nomeFantasiaApelido').val(empresa.fantasia);
                    $('#telefone').val(empresa.telefone);
                    $('#email').val(empresa.email);
                    $('#data_nascimento').val(date);
                    $('#cep').val(empresa.cep);
                    $('#rua').val(empresa.logradouro);
                    $('#numero').val(empresa.numero);
                    $('#complemento').val(empresa.complemento);
                    $('#bairro').val(empresa.bairro);
                    $('#cidade').val(empresa.municipio);
                    $('#estado').val(empresa.uf);
                    $('#natureza_juridica').val(empresa.natureza_juridica);

                    if (empresa.atividade_principal.length > 0) {
                        $('#observacao').val(empresa.atividade_principal[0].text);
                    }
                }
            } else {

                $('#nomeEmpresa').val(empresa.nome);
                $('#nomeFantasiaApelido').val(empresa.fantasia);
                $('#telefone').val(empresa.telefone);
                $('#email').val(empresa.email);
                $('#data_nascimento').val(date);
                $('#cep').val(empresa.cep);
                $('#rua').val(empresa.logradouro);
                $('#numero').val(empresa.numero);
                $('#complemento').val(empresa.complemento);
                $('#bairro').val(empresa.bairro);
                $('#cidade').val(empresa.municipio);
                $('#estado').val(empresa.uf);
                $('#natureza_juridica').val(empresa.natureza_juridica);

                if (empresa.atividade_principal.length > 0) {
                    $('#observacao').val(empresa.atividade_principal[0].text);
                }
            }
            $("#divProgress").html('');
            getConsultaCEP();
        }
    });
}

function getConsultaCEP() {

    if ($.trim($("#cep").val()) !== "") {

        var cep = $("#cep").val();
        cep = cep.replace('-','');
        cep = cep.replace('.','');

        var url = 'https://api.postmon.com.br/v1/cep/'+cep;

        $.ajax({
            type: "GET",
            url:  url,
            dataType: 'json',
            success: function (data) {
                if (data !== -1) {
                    $("#rua").val(data.logradouro);
                    $("#bairro").val(data.bairro);
                    $("#cidade").val(data.cidade);
                    $("#estado").val(data.estado);

                    $('#codIBGECidade').val(data.cidade_info.codigo_ibge);
                    $('#codIBGEEstado').val(data.estado_info.codigo_ibge);
                    $('#numero').focus();
                } else {
                    alert("Endereço não encontrado");
                }

            }
        });
    }
}