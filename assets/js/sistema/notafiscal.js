$(document).ready(function(){
    atualizarTotalizadorNFE();
});

function getConsultaCEP() {
    if ($.trim($("#destCEP").val()) !== "") {
        var cep = $("#destCEP").val();
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        var url = 'https://api.postmon.com.br/v1/cep/'+cep;
        $.get(url,
            function (data) {
                if (data !== -1) {
                    $("#destxLgr").val(data.logradouro);
                    $("#destxBairro").val(data.bairro);
                    $("#destxMun").val(data.cidade);
                    $("#destUF").val(data.estado);
                    $('#destcMun').val(data.cidade_info.codigo_ibge);
                    $('#numero').focus();
                } else {
                    alert("Endereço não encontrado");
                }
            });
    }
}

function adicionarProdutoNotaFiscal(formulario, nNF) {
    event.preventDefault();
    var dados = formulario.serialize();
    $.ajax({
        type: "POST",
        url: BASE_URL + "index.php/nfe/adicionarProduto",
        data: dados,
        dataType: 'json',
        success: function (data) {
            $(".searchPesquisaProdutos").modal('hide');
            if (data.resultado) atualizarDadosProdutos(nNF);
            else alert('Erro ao incluir produto na nfe');
        }
    });
    return false;
}

function atualizarTotalizadorNFE() {
    var dados = $("#formNFe").serialize();
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/nfe/atualizarTotalizadorNFE",
        data: dados,
        dataType: 'json',
        success: function (nfe) {
            $("#divTotalizadores").load(BASE_URL+'index.php/nfe/editar/'+nfe.nNF+' #divTotalizadores');
        }
    });
}

function atualizarDadosProdutos(id) {
    $('#tblProdutos').load(BASE_URL+'index.php/nfe/editar/'+id+' #tblProdutos');
    atualizarTotalizadorNFE();
}

function buscarProduto(prodnfeid) {

    $.ajax({
        type: "POST",
        url: BASE_URL+'index.php/nfe/getProdutosById/',
        data: {
            prodnfeid: prodnfeid
        },
        dataType: "json",
        success: function (produto) {

            if (produto !== undefined) {

                if (produto.prodnfeid !== '') {
                    $("input[name='qtd']").val(produto.qtd);
                    $("input[name='vDesc']").val(produto.vDesc);
                    $("input[name='valortotal']").val(produto.valortotal);
                    $("input[name='vlrtotal']").val(produto.vlrtotal);
                    $("input[name='prodnfeid']").val(produto.prodnfeid);
                }

                $("input[name='produtoid']").val(produto.produtoid);
                $("input[name='cProd']").val(produto.cProd);
                $("input[name='cProdForn']").val(produto.cProdForn);
                $("input[name='fornecedor']").val(produto.fornecedor);
                $("input[name='xProd']").val(produto.xProd);
                $("input[name='cEAN']").val(produto.cEan);
                $("input[name='uCom']").val(produto.uCom);
                $("input[name='vProd']").val(produto.vProd);
                $("input[name='vComp']").val(produto.vComp);
                $("input[name='NCM']").val(produto.NCM);
                $("input[name='CEST']").val(produto.CEST);
                $("input[name='CFOP']").val(produto.CFOP);
                $("select[name='orig']").val(produto.orig);
                $("select[name='CST']").val(produto.CST);
                $("input[name='modBC']").val(produto.modBC);
                $("input[name='pICMS']").val(produto.pICMS);
                $("input[name='pRedBC']").val(produto.pRedBC);
                $("input[name='pBCOP']").val(produto.pBCOP);
                $("input[name='pRedBCST']").val(produto.pRedBCST);
                $("input[name='modBCST']").val(produto.modBCST);
                $("input[name='pMVAST']").val(produto.pMVAST);
                $("input[name='pICMSST']").val(produto.pICMSST);
                $("input[name='vBCST']").val(produto.vBCST);
                $("input[name='vICMS']").val(produto.vICMS);
                $("input[name='vICMSST']").val(produto.vICMSST);
                $("input[name='motDesICMS']").val(produto.motDesICMS);
                $("select[name='CSTIPI']").val(produto.CSTIPI);
                $("input[name='clEnq']").val(produto.clEnq);
                $("input[name='cEnq']").val(produto.cEnq);
                $("input[name='CNPJProd']").val(produto.CNPJProd);
                $("input[name='cSelo']").val(produto.cSelo);
                $("input[name='qSelo']").val(produto.qSelo);
                $("input[name='vBCIPI']").val(produto.vBCIPI);
                $("input[name='vBC']").val(produto.vBC);
                $("input[name='pIPI']").val(produto.pIPI);
                $("input[name='qUnidIPI']").val(produto.qUnidIPI);
                $("input[name='vUnidIPI']").val(produto.vUnidIPI);
                $("input[name='vIPI']").val(produto.vIPI);
                $("select[name='CSTPIS']").val(produto.CSTPIS);
                $("input[name='vBCPIS']").val(produto.vBCPIS);
                $("input[name='pPIS']").val(produto.pPIS);
                $("input[name='vPIS']").val(produto.vPIS);
                $("input[name='qBCProdPIS']").val(produto.qBCProdPIS);
                $("input[name='vAliqProdPIS']").val(produto.vAliqProdPIS);
                $("input[name='vBCPISST']").val(produto.vBCPISST);
                $("input[name='pPISST']").val(produto.pPISST);
                $("input[name='qBCProdPISST']").val(produto.qBCProdPISST);
                $("input[name='vAliqProdPISST']").val(produto.vAliqProdPISST);
                $("select[name='CSTCOFINS']").val(produto.CSTCOFINS);
                $("input[name='qBCProdCOFINS']").val(produto.qBCProdCOFINS);
                $("input[name='vAliqProdCOFINS']").val(produto.vAliqProdCOFINS);
                $("input[name='vCOFINSST']").val(produto.vCOFINSST);
                $("input[name='pCOFINSST']").val(produto.pCOFINSST);
                $("input[name='qBCProdCOFINSST']").val(produto.qBCProdCOFINSST);
                $("input[name='vAliqProdCOFINSST']").val(produto.vAliqProdCOFINSST);
                $("input[name='vBCCOFINSST']").val(produto.vBCCOFINSST);
                $("textarea[name='infAdProd']").val(produto.infAdProd);
                $("input[name='cProdANP']").val(produto.cProdANP);
                $("input[name='descANP']").val(produto.descANP);
                $("input[name='UFCons']").val(produto.UFCons);
                $("input[name='pCOFINS']").val(produto.pCOFINS);
                $("input[name='vBCCOFINS']").val(produto.vBCCOFINS);
                $("input[name='vCOFINS']").val(produto.vCOFINS);
            }
        }
    });
}

