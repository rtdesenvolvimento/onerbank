$(document).ready(function () {

    $('#qtd').change(function (event) {
        calularTotalProdutos();
    });

    $('#vProd').change(function (event) {
        calularTotalProdutos();
    });

    $('#qtd').blur(function (event) {
        calularTotalProdutos();
    });

    $('#vProd').blur(function (event) {
        calularTotalProdutos();
    });

    $('#pICMS').blur(function (event) {
       calcularTributos();
    });

    $('#pICMSST').blur(function (event) {
       calcularTributos();
    });

    $('#pIPI').blur(function (event) {
        calcularTributos();
    });

    $('#pPIS').blur(function (event) {
       calcularTributos();
    });

    $('#pCOFINS').blur(function (event) {
        calcularTributos();
    });

});

function calularTotalProdutos() {

    var qtd     = $('#qtd').val();
    var vProd   = $('#vProd').val();

    if (qtd !== '') qtd = parseFloat(qtd);
    else qtd = 0;
    

    if (vProd !== '') vProd = parseFloat(vProd);
    else vProd = 0;
    
    var total = (qtd*vProd);
    
    $('#vlrtotal').val(total.toFixed(2));
    $('#valortotal').val(total.toFixed(2));

    calcularTributos();
}


function calcularTributos() {
    calcularValorICMS();
    calcularValorICMSST();
    calcularValorIPI();
    calcularValorPIS();
    calcularValorCOFINS();
}


function calcularValorCOFINS() {

    var pCOFINS = $('#pCOFINS').val();
    var vlrtotal = $('#vlrtotal').val();

    if (pCOFINS !== '') pCOFINS = parseFloat(pCOFINS);
    else pCOFINS = 0;

    if (vlrtotal !== '') vlrtotal = parseFloat(vlrtotal);
    else vlrtotal = 0;

    if (pCOFINS > 0 ) {
        var vBCCOFINS = vlrtotal*pCOFINS/100;
        $('#vBCCOFINS').val(vlrtotal);
        $('#vCOFINS').val(vBCCOFINS.toFixed(2));
    } else {
        $('#vBCCOFINS').val(0);
        $('#vCOFINS').val(0);
    }
}

function calcularValorPIS() {

    var pPIS = $('#pPIS').val();
    var vlrtotal = $('#vlrtotal').val();

    if (pPIS !== '') pPIS = parseFloat(pPIS);
    else pPIS = 0;

    if (vlrtotal !== '') vlrtotal = parseFloat(vlrtotal);
    else vlrtotal = 0;

    if (pPIS > 0 ) {
        var vPIS = vlrtotal*pPIS/100;
        $('#vBCPIS').val(vlrtotal);
        $('#vPIS').val(vPIS.toFixed(2));
    } else {
        $('#vBCPIS').val(0);
        $('#vPIS').val(0);
    }
}

function calcularValorIPI() {

    var pIPI = $('#pIPI').val();
    var vlrtotal = $('#vlrtotal').val();

    if (pIPI !== '') pIPI = parseFloat(pIPI);
    else pIPI = 0;

    if (vlrtotal !== '') vlrtotal = parseFloat(vlrtotal);
    else vlrtotal = 0;

    if (pIPI > 0 ) {
        var vIPI = vlrtotal*pIPI/100;
        $('#vBCIPI').val(vlrtotal);
        $('#vIPI').val(vIPI.toFixed(2));
    } else {
        $('#vBCIPI').val(0);
        $('#vIPI').val(0);
    }
}

function calcularValorICMS() {

    var pICMS = $('#pICMS').val();
    var vlrtotal = $('#vlrtotal').val();

    if (pICMS !== '') pICMS = parseFloat(pICMS);
    else pICMS = 0;

    if (vlrtotal !== '') vlrtotal = parseFloat(vlrtotal);
    else vlrtotal = 0;

    if (pICMS > 0 ) {
        var vICMS = vlrtotal*pICMS/100;
        $('#vBC').val(vlrtotal);
        $('#vICMS').val(vICMS.toFixed(2));
    } else {
        $('#vBC').val(0);
        $('#vICMS').val(0);
    }
}

function calcularValorICMSST() {

    var pICMSST = $('#pICMSST').val();
    var vlrtotal = $('#vlrtotal').val();

    if (pICMSST !== '') pICMSST = parseFloat(pICMSST);
    else pICMSST = 0;

    if (vlrtotal !== '') vlrtotal = parseFloat(vlrtotal);
    else vlrtotal = 0;

    if (pICMSST > 0 ) {
        var vICMSST = vlrtotal*pICMSST/100;
        $('#vBCST').val(vlrtotal);
        $('#vICMSST').val(vICMSST.toFixed(2));
    } else {
        $('#vBCST').val(0);
        $('#vICMSST').val(0);
    }
}

function carregaICMS(CST) {
     $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/impostos/getICMS",
        data: {
            CST: CST
        },
        dataType: 'html',
        success: function (html) {
            $("#recebeicms").html(html);
        },
         error: function (xhr, ajaxOptions, thrownError) {
             $("#recebeicms").html('');
        }
    });
}

function carregaIPI(CSTIPI) {
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/impostos/getIPI",
        data: {
            CSTIPI: CSTIPI
        },
        dataType: 'html',
        success: function (html) {
            $("#recebeipi").html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#recebeipi").html('');
        }
    });
}

function ajaxCarregaPIS(cod) {
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/impostos/getPIS"+cod,
        dataType: 'html',
        success: function (html) {
            $("#recebepis").html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#recebepis").html('');
        }
    });
}

function carregaPIS(valor) {
    if (valor === '02') {
        ajaxCarregaPIS('01');
    } else if (valor === '01') {
        ajaxCarregaPIS('01');
    } else if (valor === '03') {
        ajaxCarregaPIS('03');
    } else if (valor === '05') {
        ajaxCarregaPIS('05');
    } else if (valor >= '49') {
        ajaxCarregaPIS('49');
    } else {
        $("#recebepis").html('');
    }
}

function ajaxCarregaCOFINS(cod) {
    $.ajax({
        type: "POST",
        url: BASE_URL+"index.php/impostos/getCOFINS"+cod,
        dataType: 'html',
        success: function (html) {
            $("#recebecofins").html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#recebecofins").html('');
        }
    });
}

function carregaCofins(valor, produto) {
    if (valor === '02') {
        ajaxCarregaCOFINS('01');
    } else if (valor === '01') {
        ajaxCarregaCOFINS('01');
    } else if (valor === '03') {
        ajaxCarregaCOFINS('03');
    } else if (valor === '05') {
        ajaxCarregaCOFINS('05');
    } else if (valor >= '49') {
        ajaxCarregaCOFINS('49');
    } else {
        $("#recebecofins").html('');
    }
}

