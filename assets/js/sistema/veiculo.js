var liberado = false;

function consulta_veiculo_sinesp(placa, base_url) {

    if (placa != '') {

        placa = placa.replace('-','');
        placa = placa.replace(" ", "");

        $.ajax({
            type: "POST",
            url: base_url + "veiculos/consulta_veiculo_sinesp/" + placa,
            dataType: 'html',
            success: function (strVeiculo) {
                try {
                    var veiculo = JSON.parse(strVeiculo);
                    if (veiculo.situacao != 'Sem restrição') {
                        $('#situacao').css('background', 'red');
                        $('#situacao').val(veiculo.situacao);
                        liberado = true;
                        piscarVermelho();
                    } else {
                        $('#situacao').css('background', '#71d80036');
                        $('#situacao').val(veiculo.situacao);
                        liberado = false;
                    }

                    $('#ano').val(veiculo.ano);
                    $("#chassi").val(veiculo.chassi);
                    $("#cor").val(veiculo.cor);
                    $('#marca').val(veiculo.marca);
                    $('#modelo').val(veiculo.modelo);
                    $('#municipio').val(veiculo.municipio);
                    $('#uf').val(veiculo.uf);
                } catch(ex) {
                    alert(strVeiculo);
                }

            }
        });
    }
}

function consulta_situacao_veiculo(veiculo_id, base_url) {
    if (veiculo_id != '') {
        $.ajax({
            type: "POST",
            url: base_url + "index.php/clientes/buscar_veiculo",
            data: "veiculo_id=" + veiculo_id,
            dataType: 'json',
            success: function (veiculo) {

                if (veiculo.situacao == null || veiculo.situacao == 'null' || veiculo.situacao == '') {
                    alerta_sem_restricao('Veículo não consultado!');
                    return;
                }

                if (veiculo != undefined) {
                    if (veiculo.situacao  != 'Sem restrição') {
                        alerta_de_roubo(veiculo.situacao);
                    } else {
                        alerta_sem_restricao(veiculo.situacao);
                    }
                }
            }
        });
    } else {
        limpar_alerta_veiculo();
    }
}

function alerta_sem_restricao(menagem) {
    $('#div_buscaVeiculo').css('background', 'linear-gradient(rgba(100, 234, 31, 0.44) 1%, rgb(255, 255, 255) 100%)');
    $('#div_veiculo_restricao').html(menagem);
}

function limpar_alerta_veiculo() {
    $('#div_buscaVeiculo').css('background', 'linear-gradient(rgba(100, 234, 31, 0.44) 1%, rgb(255, 255, 255) 100%)');
    $('#div_veiculo_restricao').html('Sem veículo');
}

function alerta_de_roubo(menagem) {
    $('#div_buscaVeiculo').css('background', 'linear-gradient(rgb(234, 31, 31) 1%, rgb(255, 255, 255) 100%)');
    $('#div_veiculo_restricao').html(menagem);
}

function piscarVermelho() {
    if (liberado) {
        $('#situacao').css('background', 'red');
        setTimeout('piscarAzul()', 600);
    }
}

function piscarAzul() {
    if (liberado) {
        $('#situacao').css('background', '#71d80036');
        setTimeout('piscarVermelho()', 600);
    }
}