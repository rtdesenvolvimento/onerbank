﻿<?php

/*

error_reporting(0);
ini_set("display_errors", 0);
error_reporting(E_ALL);
ini_set('display_errors', 'On');
*/

if (!isset($_SESSION)):
    session_start();
endif;

if (!isset($_SESSION['BD'])){
    $BANCO = $_GET['BD'];
} else {
    $BANCO =  $_SESSION['BD'];
}
//define('PREFIXO', 'sysonerc_');//TODO
define('PREFIXO', '');
define('DBNAME', $BANCO);
define('HOST', 'localhost');
define('CHARSET', 'utf8');
define('USER', 'root');
define('PASSWORD', '');

$conexao = 'S';

class Conexao {

    private static $pdo;

    private function __construct() {}

    private function __destruct() {}

    /*
     * Método estático para retornar uma conexão válida  
     * Verifica se já existe uma instância da conexão, caso não, configura uma nova conexão  
     */

    public static function getInstance() {
        if (!isset(self::$pdo)) {
            try {
                $db = DBNAME;
                if (is_numeric(DBNAME)) {
                    $db = PREFIXO.DBNAME;
                }
                $opcoes = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::ATTR_PERSISTENT => TRUE);
                self::$pdo = new PDO("mysql:host=" . HOST . "; dbname=" . $db . "; charset=" . CHARSET . ";", USER, PASSWORD, $opcoes);
            } catch (PDOException $e) {
                //print "Erro: " . $e->getMessage();
                $erro[] = array("situacao" => "-1","erro"=>$e->getMessage());
                echo json_encode($erro);
            }
        }
        return self::$pdo;
    }

}
?>