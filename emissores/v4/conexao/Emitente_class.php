<?php

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where id ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$config = [
    "atualizacao" => "2018-06-24 06:01:21",
    "tpAmb" => intval($emitente->ambiente),
    "razaosocial" => $emitente->nome,
    "siglaUF" => $emitente->uf,
    "cnpj" => $emitente->cnpj,
    "schemes" => "PL_009_V4",
    "versao" => "4.00",
    "tokenIBPT" => "AAAAAAA",
    "CSC" => $emitente->csc,
    "CSCid" => $emitente->idcsc,
    "aProxyConf" => [
        "proxyIp" => "",
        "proxyPort" => "",
        "proxyUser" => "",
        "proxyPass" => ""
    ]
];
$json = json_encode($config);
