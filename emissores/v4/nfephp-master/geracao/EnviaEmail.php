<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
require_once "../../_backend/_class/Emitente_class.php";
require_once '../bootstrap.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

//NOTA: o envio de email com o DANFE somente funciona para modelo 55
//      o modelo 65 nunca requer o envio do DANFCE por email
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;

$json1 = file_get_contents('../config/' . $_SESSION['BD'] . '/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/' . $_SESSION['BD'] . '/' . $json1['certPfxName']);
$tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));


$nNFbd = $_GET['arquivo'];
//$tools->setModelo('55');

$tpAmb = $json1['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');

$sql = "SELECT dhEmi,chavenfe,recibo from nfe where nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pasta = parseDate($dadosnfe->dhEmi, "Ym");

$chave = $dadosnfe->chavenfe;

$pathXml = "{$tools->aConfig['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-protNFe.xml";
$pathPdf = "{$tools->aConfig['pathNFeFiles']}/{$pastaxml}/pdf/{$pasta}/{$chave}-danfe.pdf";

//echo $pathPdf;

$aMails = array(); //se for um array vazio a classe Mail irá pegar os emails do xml
$templateFile = ''; //se vazio usará o template padrão da mensagem
$comPdf = true; //se true, anexa a DANFE no e-mail
try {
    $tools->enviaMail($pathXml, $aMails, $templateFile, $comPdf, $pathPdf, $chave);
    $msg = "DANFE enviada com sucesso!!!";
} catch (NFePHP\Common\Exception\RuntimeException $e) {
    $msg = $e->getMessage();
}

$array = array("status" => "OK", "msg" => $msg);

echo json_encode($array);
