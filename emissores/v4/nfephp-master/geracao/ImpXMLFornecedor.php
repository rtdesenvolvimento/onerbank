<?php
require_once '../bootstrap.php';

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

function formatar($tipo = "", $string, $size = 10) {

    $string = preg_replace("[^0-9]", "", $string);

    switch ($tipo) {
        case 'fone':
            if ($size === 10) {
                $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 4)
                        . '-' . substr($tipo, 6);
            } else
            if ($size === 11) {
                $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 5)
                        . '-' . substr($tipo, 7);
            }
            break;
        case 'cep':
            $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
            break;
        case 'cpf':
            $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) .
                    '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
            break;
        case 'cnpj':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
                    '.' . substr($string, 5, 3) . '/' .
                    substr($string, 8, 4) . '-' . substr($string, 12, 2);
            break;
        case 'rg':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
                    '.' . substr($string, 5, 3);
            break;
        default:
            $string = 'É ncessário definir um tipo(fone, cep, cpg, cnpj, rg)';
            break;
    }
    return $string;
}

$importarNotasSaida     = $_GET['importarNotasSaida'];
$manifestaid            = $_GET['manifestaid'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'manifesta');
$sql = "SELECT * from manifesta where manifestaid in (".$manifestaid.'-1)';
$arrayParam = '';
$dadosmanifesta = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$filename = '';

$nomeFornecedor  = '';
foreach ($dadosmanifesta as $manifesta) {
    $filename       = "../XML/{$emitente->cnpj}/NF-e/producao/recebidas/".$manifesta->chave."-nfe.xml"; // Ambiente Windows
    $nomeFornecedor = $manifesta->xNome;
}

$xml = simplexml_load_file($filename);
$nNF = $xml->NFe->infNFe->ide->nNF;

function VerificaFornecer($valor) {
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'cliente');
    $sql = "SELECT * from fornecedor where documento = '$valor'";
    $arrayParam = '';
    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    $total = count($dados);
    if ($total > 0) :
        $total = 1;
    else :
        $total = 0;
    endif;
    return $total;
}


function VerificaCliente($valor) {
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'cliente');
    $sql = "SELECT * from clientes where documento = '$valor'";
    $arrayParam = '';
    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    $total = count($dados);
    if ($total > 0) :
        $total = 1;
    else :
        $total = 0;
    endif;
    return $total;
}

function VerificaProduto($valor) {
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'cliente');
    $sql = "SELECT * from produto where cProd = '$valor'";
    $arrayParam = '';
    $dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    $total = count($dados);
    if ($total > 0) :
        $total = 1;
    else :
        $total = 0;
    endif;
    return $total;
}

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'cliente');
$sql = "SELECT * from nfe_fornecedor where nNF = '$nNF'";
$arrayParam = '';
$nfeb = $crud->getSQLGeneric($sql, $arrayParam, false);

if (!empty($nfeb->nNF)) :

    $pdo = Conexao::getInstance();
    $crudpnf = Crud::getInstance($pdo, 'produtonfe');
    $retorno = $crudpnf->Sql("DELETE FROM produtonfe_fornecedor where NFe = '$nfeb->nNF'");
    $pdo = null;
    $crudfpnf = null;

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'nfe');
    $retorno = $crud->Sql("DELETE FROM nfe_fornecedor where nNF = '$nfeb->nNF'");
    $pdo = null;
    $crud = null;

    $pdo = Conexao::getInstance();
    $crudpnf = Crud::getInstance($pdo, 'produtonfe');
    $retorno = $crudpnf->Sql("DELETE FROM duplicatanfe_fornecedor where nfe = '$nfeb->nNF'");
    $pdo = null;
    $crudfpnf = null;

endif;

$dhemi = substr($xml->NFe->infNFe->ide->dhEmi, 8, 2);
$dhemi .= "/";
$dhemi .= substr($xml->NFe->infNFe->ide->dhEmi, 5, 2);
$dhemi .= "/";
$dhemi .= substr($xml->NFe->infNFe->ide->dhEmi, 0, 4);

$hEmi = substr($xml->NFe->infNFe->ide->dhEmi, 11, 5);

$dhSaiEnt = '';
$hSai = '';

if ($xml->NFe->infNFe->ide->dhSaiEnt != '') :
    $dhSaiEnt = substr($xml->NFe->infNFe->ide->dhSaiEnt, 8, 2);
    $dhSaiEnt .= "/";
    $dhSaiEnt .= substr($xml->NFe->infNFe->ide->dhSaiEnt, 5, 2);
    $dhSaiEnt .= "/";
    $dhSaiEnt .= substr($xml->NFe->infNFe->ide->dhSaiEnt, 0, 4);
    $hSai = substr($xml->NFe->infNFe->ide->dhSaiEnt, 11, 5);
endif;

$dadosIde = array("cUF" => $xml->NFe->infNFe->ide->cUF, "cNF" => $xml->NFe->infNFe->ide->cNF, "natOp" => $xml->NFe->infNFe->ide->natOp,
    "indPag" => $xml->NFe->infNFe->ide->indPag, "modelo" => $xml->NFe->infNFe->ide->mod, "serie" => $xml->NFe->infNFe->ide->serie,
    "nNF" => $xml->NFe->infNFe->ide->nNF, "dhEmi" => $dhemi, "hEmi" => $hEmi, "dhSaiEnt" => $dhSaiEnt, "hSaiEnt" => $hSai,
    "tpEmis" => $xml->NFe->infNFe->ide->tpEmis, "finNFe" => $xml->NFe->infNFe->ide->finNFe, "indPres" => $xml->NFe->infNFe->ide->indPres,
    "status" => $xml->protNFe->infProt->cStat, "chavenfe" => $xml->protNFe->infProt->chNFe, "nProt" => $xml->protNFe->infProt->nProt );

if ($xml->NFe->infNFe->dest->CNPJ != '') :
    $destCNPJ = $xml->NFe->infNFe->dest->CNPJ;
    $destCNPJ1 = formatar('cnpj', $destCNPJ);
else :
    $destCNPJ = $xml->NFe->infNFe->dest->CPF;
    $destCNPJ1 = formatar('cpf', $destCNPJ);
endif;

if ($xml->NFe->infNFe->dest->CNPJ != '') :
    $emitCNPJ = $xml->NFe->infNFe->emit->CNPJ;
    $emitCNPJ1 = formatar('cnpj', $emitCNPJ);
else :
    $emitCNPJ = $xml->NFe->infNFe->emit->CPF;
    $emitCNPJ1 = formatar('cpf', $emitCNPJ);
endif;

$dadosEmit = array(
    "emitCNPJ" => $emitCNPJ1,
    "emitFone" => $xml->NFe->infNFe->emit->enderEmit->fone,
    "emitIE" => $xml->NFe->infNFe->emit->IE,
    "emitxNome" => $xml->NFe->infNFe->emit->xNome,
    "emitxFant"     => $xml->NFe->infNFe->emit->emitxFant,
    "emitxLgr" => $xml->NFe->infNFe->emit->enderEmit->xLgr,
    "emitnro" => $xml->NFe->infNFe->emit->enderEmit->nro,
    "emitxBairro" => $xml->NFe->infNFe->emit->enderEmit->xBairro,
    "emitcMun" => $xml->NFe->infNFe->emit->enderEmit->cMun,
    "emitxMun" => $xml->NFe->infNFe->emit->enderEmit->xMun,
    "emitUF" => $xml->NFe->infNFe->emit->enderEmit->UF,
    "emitCEP" => $xml->NFe->infNFe->emit->enderEmit->CEP,
    "emitxCpl"    => $xml->NFe->infNFe->emit->enderEmit->xCpl,
    "emitemail" => $xml->NFe->infNFe->emit->email
);


if (!empty($xml->NFe->infNFe->dest)) {
    $dadosEmit_saida = array(
        "emitCNPJ" => $destCNPJ1,
        "emitIE" => $xml->NFe->infNFe->dest->IE,
        "emitxNome" => $xml->NFe->infNFe->dest->xNome,
        "emitxFant" => $xml->NFe->infNFe->dest->emitxFant,
        "emitxLgr" => $xml->NFe->infNFe->dest->enderDest->xLgr,
        "emitnro" => $xml->NFe->infNFe->dest->enderDest->nro,
        "emitxBairro" => $xml->NFe->infNFe->dest->enderDest->xBairro,
        "emitcMun" => $xml->NFe->infNFe->dest->enderDest->cMun,
        "emitxMun" => $xml->NFe->infNFe->dest->enderDest->xMun,
        "emitUF" => $xml->NFe->infNFe->dest->enderDest->UF,
        "emitCEP" => $xml->NFe->infNFe->dest->enderDest->CEP,
        "emitFone" => $xml->NFe->infNFe->dest->enderDest->fone,
        "emitxCpl" => $xml->NFe->infNFe->dest->enderDest->xCpl,
        "emitemail" => $xml->NFe->infNFe->dest->email
    );
}

$dadosDest = array(
    "destCNPJ"      => $emitCNPJ1,
    "destIE"        => $xml->NFe->infNFe->emit->IE,
    "destxNome"     => $xml->NFe->infNFe->emit->xNome,
    "destxLgr"      => $xml->NFe->infNFe->emit->enderEmit->xLgr,
    "destnro"       => $xml->NFe->infNFe->emit->enderEmit->nro,
    "destxBairro"   => $xml->NFe->infNFe->emit->enderEmit->xBairro,
    "destcMun"      => $xml->NFe->infNFe->emit->enderEmit->cMun,
    "destxMun"      => $xml->NFe->infNFe->emit->enderEmit->xMun,
    "destUF"        => $xml->NFe->infNFe->emit->enderEmit->UF,
    "destCEP"       => $xml->NFe->infNFe->emit->enderEmit->CEP,
    "destFone"      => $xml->NFe->infNFe->emit->enderEmit->fone
);

//notas de saida
if (!empty($xml->NFe->infNFe->dest)) {
    $dadosDest_saida = array(
        "destCNPJ" => $destCNPJ1,
        "destIE" => $xml->NFe->infNFe->dest->IE,
        "destxNome" => $xml->NFe->infNFe->dest->xNome,
        "destxLgr" => $xml->NFe->infNFe->dest->enderDest->xLgr,
        "destnro" => $xml->NFe->infNFe->dest->enderDest->nro,
        "destxBairro" => $xml->NFe->infNFe->dest->enderDest->xBairro,
        "destcMun" => $xml->NFe->infNFe->dest->enderDest->cMun,
        "destxMun" => $xml->NFe->infNFe->dest->enderDest->xMun,
        "destUF" => $xml->NFe->infNFe->dest->enderDest->UF,
        "destCEP" => $xml->NFe->infNFe->dest->enderDest->CEP,
        "destFone" => $xml->NFe->infNFe->dest->enderDest->fone
    );
}

if ($xml->NFe->infNFe->ide->mod == '55') {
    if ($xml->NFe->infNFe->emit->xNome != $nomeFornecedor) $importarNotasSaida = '1';
}

//se for nota de sainda entao o cliente é o destinatario
if ($importarNotasSaida === '1') {
    $dadosDest = $dadosDest_saida;
    $dadosEmit = $dadosEmit_saida;
}

$dadostotaltd = array("vBCnf" => $xml->NFe->infNFe->total->ICMSTot->vBC, "vICMSnf" => $xml->NFe->infNFe->total->ICMSTot->vICMS, "vICMSDesonnf" => $xml->NFe->infNFe->total->ICMSTot->vICMSDeson,
    "vBCSTnf" => $xml->NFe->infNFe->total->ICMSTot->vBCST, "vSTnf" => $xml->NFe->infNFe->total->ICMSTot->vST, "vProdnf" => $xml->NFe->infNFe->total->ICMSTot->vProd,
    "vFretenf" => $xml->NFe->infNFe->total->ICMSTot->vFrete, "vSegnf" => $xml->NFe->infNFe->total->ICMSTot->vSeg, "vDescnf" => $xml->NFe->infNFe->total->ICMSTot->vDesc,
    "VIInf" => '0', "vIPInf" => $xml->NFe->infNFe->total->ICMSTot->vIPI, "vPISnf" => $xml->NFe->infNFe->total->ICMSTot->vPIS,
    "vCOFINSnf" => $xml->NFe->infNFe->total->ICMSTot->vCOFINS, "vOutronf" => $xml->NFe->infNFe->total->ICMSTot->vOutro, "vNF" => $xml->NFe->infNFe->total->ICMSTot->vNF);

$dadostotal = array_filter($dadostotaltd);

$dadosfrete = array("modFrete" => $xml->NFe->infNFe->transp->modFrete);

$dadosadicionais = array("infCpl" => $xml->NFe->infNFe->infAdic->infCpl);

$dados = array_merge($dadosIde, $dadosDest, $dadostotal, $dadosfrete, $dadosfrete, $dadosadicionais);

$total  = count($dados);
$pdo    = Conexao::getInstance();
$crud   = Crud::getInstance($pdo, 'nfe_fornecedor');
$sql    = $crud->SQLinsert('nfe_fornecedor', $dados, $total);

$pdo    = Conexao::getInstance();
$crud   = Crud::getInstance($pdo, 'nfe_fornecedor');

// Atribui uma instância da classe Crud, passando como parâmetro a conexão PDO e o nome da tabela
$retorno    = $crud->getSQLGeneric($sql);
$totalprod  = count($xml->NFe->infNFe->det);

$semResultado = 0;
$nNFprod = array("NFe" => $xml->NFe->infNFe->ide->nNF);

for ($i = 0; $i < 200; $i++) :
    if (!empty($xml->NFe->infNFe->det[$i]->prod->cProd)) :

        $dadosprod = array(
            "cProd" => $xml->NFe->infNFe->det[$i]->prod->cProd,
            "xProd" => $xml->NFe->infNFe->det[$i]->prod->xProd,
            "NCM" => $xml->NFe->infNFe->det[$i]->prod->NCM,
            "cEAN" => $xml->NFe->infNFe->det[$i]->prod->cEAN,
            "CEST" => $xml->NFe->infNFe->det[$i]->prod->CEST,
            "CFOP" => $xml->NFe->infNFe->det[$i]->prod->CFOP,
            "uCom" => $xml->NFe->infNFe->det[$i]->prod->uCom,
            "qtd" => $xml->NFe->infNFe->det[$i]->prod->qCom,
            "vProd" => $xml->NFe->infNFe->det[$i]->prod->vUnCom,
            "valortotal" => $xml->NFe->infNFe->det[$i]->prod->vProd
        );

        //    ICMS60
        if ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS60->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS60->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS60->CST);
            $dadospis = array("CSTPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->CST, "vBCPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->vBC, "pPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->pPIS
        );

        $dadoscofins = array("CSTCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->CST, "vBCCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->vBC,
            "pCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->pCOFINS, "vCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->vCOFINS);

        //ICMS00
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->CST,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->vBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->pICMS, "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS00->vICMS
            );

        //    ICMS20
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->CST,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->vBC,
                "pRedBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->pRedBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->pICMS, "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->vICMS,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS20->motDesICMS
            );

        //    ICMS10
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->CST,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->vBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->pICMS, "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->vICMS,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->modBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->pMVAST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->pRedBCST, "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->pRedBCST,
                "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->vBCST, "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->pICMSST,
                "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS10->vICMSST
            );

        //    ICMS30
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->CST,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->modBCST, "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->vBCST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->pRedBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->pMVAST,
                "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->pICMS, "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->vICMS,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS30->motDesICMS
            );

        //    ICMS40
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS40->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS40->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS40->CST,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS40->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS40->motDesICMS
            );

        //    ICMS41
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS41->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS41->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS41->CST,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS41->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS41->motDesICMS
            );

        //    ICMS50
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS50->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS50->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS50->CST,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS50->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS50->motDesICMS
            );

        //    ICMS51
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->CST,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->vBC,
                "pRedBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->pRedBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->pICMS, "vICMSOp" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->vICMSOp,
                "pDif" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->pDif, "vICMSDif" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->vICMSDif,
                "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS51->vICMS
            );

        //    ICMS70
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->CST,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->vBC,
                "pRedBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->pRedBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->pICMS, "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->vICMS,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->modBCST, "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->vBCST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->pRedBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->pMVAST,
                "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->pICMS, "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->vICMS,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS70->motDesICMS
            );

        //    ICMS90
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->CST,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->vBC,
                "pRedBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->pRedBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->pICMS, "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->vICMS,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->modBCST, "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->vBCST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->pRedBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->pMVAST,
                "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->pICMS, "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->vICMS,
                "vICMSDeson" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->vICMSDeson, "motDesICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMS90->motDesICMS
            );

        //    ICMS101
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->CSOSN,
                "pCredSN" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->pCredSN, "vCredICMSSN" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->vCredICMSSN
            );

        //    ICMS102
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN102->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN102->orig,
                "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN102->CSOSN
            );

        //    ICMS201
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->CSOSN,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->modBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->pMVAST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->pRedBCST, "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->vBCST,
                "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->pICMSST, "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->vICMSST,
                "pCredSN" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->pCredSN, "vCredICMSSN" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN201->vCredICMSSN
            );

        //    ICMS202
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->orig != '') :
            $dadosimposto = array("orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->CSOSN,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->modBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->pMVAST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->pRedBCST, "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->vBCST,
                "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->pICMSST, "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN202->vICMSST
            );

        //    ICMS500
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN500->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN500->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN500->CSOSN,
                "vBCSTRet" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN500->vBCSTRet, "vICMSSTRet" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN500->vICMSSTRet
            );


        //    ICMS900
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->orig != '') :
            $dadosimposto = array(
                "orig" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->orig, "CST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->CSOSN,
                "modBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->modBC, "vBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->vBC,
                "pRedBC" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->pRedBC,
                "pICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->pICMS, "vICMS" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->vICMS,
                "modBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->modBCST, "pMVAST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->pMVAST,
                "pRedBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->pRedBCST, "vBCST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->vBCST,
                "pICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->pICMSST, "vICMSST" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN900->vICMSST,
                "pCredSN" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->pCredSN, "vCredICMSSN" => $xml->NFe->infNFe->det[$i]->imposto->ICMS->ICMSSN101->vCredICMSSN
            );
        endif;

        //    PIS
        if ($cProd = $xml->NFe->infNFe->det[$i]->imposto->PIS->PISAliq->CST != '') :
            $dadospis = array(
                "CSTPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISAliq->CST,
                "vBCPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISAliq->vBC,
                "pPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISAliq->pPIS,
                "vPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISAliq->vPIS
            );

        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->PIS->PISQtde->CST != '') :
            $dadospis = array(
                "CSTPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISQtde->CST,
                "qBCProdPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISQtde->qBCProd,
                "vAliqProdPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISQtde->vAliqProd,
                "vPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISQtde->vPIS
            );
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->PIS->PISNT->CST != '') :
            $dadospis = array(
                "CSTPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISNT->CST
            );
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->CST != '') :
            $dadospis = array(
                "CSTPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->CST,
                "vBCPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->vBC,
                "pPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->pPIS,
                "qBCProdPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->qBCProd->pPIS,
                "vAliqProdPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->vAliqProd,
                "vPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISOutr->vPIS
            );
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->PIS->PISST->vBC != '') :
            $dadospis = array(
                "vBCPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISST->vBC,
                "pPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISST->pPIS,
                "qBCProdPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISST->pPIS,
                "vAliqProdPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISST->vAliqProd,
                "vPIS" => $xml->NFe->infNFe->det[$i]->imposto->PIS->PISST->vPIS
            );
        endif;

        //    COFINS
        if ($cProd = $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSAliq->CST != '') :
            $dadosCOFINS = array(
                "CSTCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSAliq->CST,
                "vBCCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSAliq->vBC,
                "pCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSAliq->pCOFINS,
                "vCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSAliq->vCOFINS
            );

        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSQtde->CST != '') :
            $dadosCOFINS = array(
                "CSTCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSQtde->CST,
                "qBCProdCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSQtde->qBCProd,
                "vAliqProdCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSQtde->vAliqProd,
                "vCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSQtde->vCOFINS
            );
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSNT->CST != '') :
            $dadosCOFINS = array(
                "CSTCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSNT->CST
            );
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->CST != '') :
            $dadosCOFINS = array(
                "CSTCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->CST,
                "vBCCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->vBC,
                "pCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->pCOFINS,
                "qBCProdCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->qBCProd->pCOFINS,
                "vAliqProdCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->vAliqProd,
                "vCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSOutr->vCOFINS
            );
        elseif ($cProd = $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSST->vBC != '') :
            $dadosCOFINS = array(
                "vBCCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSST->vBC,
                "pCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSST->pCOFINS,
                "qBCProdCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSST->pCOFINS,
                "vAliqProdCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSST->vAliqProd,
                "vCOFINS" => $xml->NFe->infNFe->det[$i]->imposto->COFINS->COFINSST->vCOFINS
            );
        endif;

        $dadospisf      = array_filter($dadospis);
        $dadosCOFINSf   = array_filter($dadosCOFINS);
        $dadosprodutos  = array_merge($nNFprod, $dadosprod, $dadosimposto, $dadospisf, $dadosCOFINSf);
        $dadosprodutos  = array_filter($dadosprodutos);

        $CST = $dadosimposto['CST'];

        foreach($dadosprodutos aS $subarray) {

            if ($CST === '60') {
                $CFOP = '5405';
                $CST = '500';

                $subarray['CFOP'] = $CFOP;
                $subarray['CST'] = $CST;

            } else {
                $CFOP = '5102';
                $CST = '102';

                $subarray['CFOP'] = $CFOP;
                $subarray['CST'] = $CST;
            }

            $dadosprodutos['CFOP'] = $subarray['CFOP'];
            $dadosprodutos['CST'] = $subarray['CST'];
        }

        //print_r($dadosprodutos);

        $total = count($dadosprodutos);
        $pdo = Conexao::getInstance();
        $crud = Crud::getInstance($pdo, 'produtonfe_fornecedor');
        $sql = $crud->SQLinsert('produtonfe_fornecedor', $dadosprodutos, $total);
        $retorno = $crud->Sql($sql);

        //echo $sql;

        /*
        if (VerificaProduto($dadosprod['cProd']) <= 0) :
            $dadosprodo = array("CST" => '102', "emitente" => $emitente->id);
            $dprod = array_merge($dadosprodo, $dadosprod);
            unset($dprod['qtd']);
            unset($dprod['valortotal']);
            $total = count($dprod);
            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'produto');
            $sql = $crud->SQLinsert('produto', $dprod, $total);
            $retornop = $crud->Sql($sql);
//        echo $sql;
        endif;
        */

    else :
        $semResultado ++;
    endif;

    if ($semResultado >= 10) :
        break;
    endif;

endfor;

if ($retorno) :


    if ($importarNotasSaida == '1') {
        if (VerificaCliente($dadosEmit['emitCNPJ']) <= 0) :

            $totaldoc = strlen($dadosEmit['emitCNPJ']);

            if ($totaldoc > 14) $tipo = "PJ";
            else $tipo = "PF";

            $cliente = array(
                "documento" => $dadosEmit['emitCNPJ'],
                "tipoPessoa" => $tipo,
                "IE" => $dadosEmit['emitIE'],
                "nomeCliente" => $dadosEmit['emitxNome'],
                "nomeFantasiaApelido" => $dadosEmit['emitxFant'],
                "cep" => $dadosEmit['emitCEP'],
                "rua" => $dadosEmit['emitxLgr'],
                "numero" => $dadosEmit['emitnro'],
                "bairro" => $dadosEmit['emitxBairro'],
                "cidade" => $dadosEmit['emitxMun'],
                "estado" => $dadosEmit['emitUF'],
                "codIBGECidade" => $dadosEmit['emitcMun'],
                "telefone" => $dadosEmit['emitFone'],
                "complemento" => $dadosEmit['emitxCpl'],
                "email" => $dadosEmit['emitemail']
            );

            $total = count($cliente);
            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'clientes');
            $sql = $crud->SQLinsert('clientes', $cliente, $total);
            $retorno = $crud->Sql($sql);
        endif;
    } else {
        if (VerificaFornecer($dadosEmit['emitCNPJ']) <= 0) :

            $totaldoc = strlen($dadosEmit['emitCNPJ']);

            if ($totaldoc > 14) $tipo = "PJ";
            else $tipo = "PF";

            $cliente = array(
                "documento" => $dadosEmit['emitCNPJ'],
                "tipoPessoa" => $tipo,
                "IE" => $dadosEmit['emitIE'],
                "nomeFornecedor" => $dadosEmit['emitxNome'],
                "nomeFantasiaApelido" => $dadosEmit['emitxFant'],
                "cep" => $dadosEmit['emitCEP'],
                "rua" => $dadosEmit['emitxLgr'],
                "numero" => $dadosEmit['emitnro'],
                "bairro" => $dadosEmit['emitxBairro'],
                "cidade" => $dadosEmit['emitxMun'],
                "estado" => $dadosEmit['emitUF'],
                "codIBGECidade" => $dadosEmit['emitcMun'],
                "telefone" => $dadosEmit['emitFone'],
                "complemento" => $dadosEmit['emitxCpl'],
                "email" => $dadosEmit['emitemail']
            );

            $total = count($cliente);
            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'fornecedor');
            $sql = $crud->SQLinsert('fornecedor', $cliente, $total);
            $retorno = $crud->Sql($sql);
        endif;
    }

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'manifesta');
    $sql = "UPDATE manifesta set importada = 'S' where chave = '".$xml->protNFe->infProt->chNFe."'";
    $crud->Sql($sql);

    $msg = "Nota Fiscal Importada com sucesso!";
    $status = "ok";
else :
    $msg = "Houve um erro ao importar NF-e! <br/>Use novamente o comando Baixar XML p/Servidor e tente o Cadastro novamente!";
    $status = "erro";
endif;

$arr = array("msg" => $msg, "status" => $status, "rs"=>$retorno);
$json = json_encode($arr);
echo $json;
