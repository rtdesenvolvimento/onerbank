<?php

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

///error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once '../bootstrap.php';

$json1 = file_get_contents('../config/' . $emitente->cnpj . '/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/' . $emitente->cnpj . '/' . $json1['certPfxName']);

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

$tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));

$tools->model('65');
$tools->setEnvironment(1);

try {
    //$response = $tools->sefazDownload('29200110239779000156650010000034291000000091');

    $response = $tools->sefazConsultaChave('29200110239779000156650010000034291000000091');
    //$response = $tools->sefazConsultaRecibo('329200011358525', 1);
    print_r($response);

    $stdCl = new Standardize($response);
    $std = $stdCl->toStd();
    $arr = $stdCl->toArray();
    $json = $stdCl->toJson();

}  catch (\Exception $e) {
    echo $e->getMessage() ;
}

