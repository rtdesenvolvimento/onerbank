﻿<?php


require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';


//error_reporting(E_ALL);
//ini_set('display_errors', 'On');


//
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;

$nNFbd = $_GET['nNF'];
$xCorrecao = $_GET['correcao'];
$data = date("d/m/Y");

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$chave = $dadosnfe->chavenfe;

$tpAmb = $emitente->ambiente;
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$json1 = file_get_contents('../config/'.$emitente->cnpj.'/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/'.$emitente->cnpj.'/' . $json1['certPfxName']);

$pasta = date("Ym");
$nomepasta = "{$json1['pathNFeFiles']}/{$pastaxml}/cartacorrecao/$pasta";
if (!is_dir($nomepasta)) :
    mkdir($nomepasta);
endif;

try {

    $certificate = Certificate::readPfx($content, $json1['certPassword']);
    $tools = new Tools($json, $certificate);
    $tools->model('55');
    $nSeqEvento = 1;
    $response = $tools->sefazCCe($chave, $xCorrecao, $nSeqEvento);

    $stdCl = new Standardize($response);
    $std = $stdCl->toStd();
    $arr = $stdCl->toArray();
    $json = $stdCl->toJson();

//    echo '<pre>';
//    print_r($arr);
//    echo '</pre>';
    //verifique se o evento foi processado
    if ($std->cStat != 128) {
        /*
        header('Content-type: text/xml; charset=UTF-8');
        $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $std->xMotivo . " </motivo>
                    </retorno>";
        */
        echo $std->xMotivo;
    } else {
        $cStat = $std->retEvento->infEvento->cStat;
        if ($cStat == '135' || $cStat == '136') {
            //SUCESSO PROTOCOLAR A SOLICITAÇÂO ANTES DE GUARDAR
            $xml = Complements::toAuthorize($tools->lastRequest, $response);
            //grave o XML protocolado 
            $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/cartacorrecao/$pasta/{$chave}-nfe.xml"; // Ambiente Windows
            file_put_contents($filename, $xml);
            chmod($filename, 0777);

            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'nfe');
            $retorno = $crud->Sql("UPDATE nfe set cce = 'S', datacce='$data' where nNF = {$nNFbd}");

            /*
            header('Content-type: text/xml; charset=UTF-8');
            $xml = "<retorno>
                    <status>OK</status>
                    <motivo>" . $std->retEvento->infEvento->xMotivo . " </motivo>
                    </retorno>";
            */
            echo 'OK';
        } else {
            /*
            $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $std->retEvento->infEvento->xMotivo . " </motivo>
                    </retorno>";
            */
            echo $std->retEvento->infEvento->xMotivo ;
        }
    }
} catch (\Exception $e) {
    /*
    header('Content-type: text/xml; charset=UTF-8');
    $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $e->getMessage() . " </motivo>
                    </retorno>";
    */
    echo $e->getMessage() ;
}
?>