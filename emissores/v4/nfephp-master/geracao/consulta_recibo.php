﻿<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once '../bootstrap.php';

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

use NFePHP\NFe\Convert;
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;


$nNFbd = $_GET['arquivo'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe, recibo from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$json1 = file_get_contents('../config/'.$emitente->cnpj.'/config.json');
$json1 = json_decode($json1, true);

$chave = $dadosnfe->chavenfe;
$recibo = $dadosnfe->recibo;

$tpAmb = $emitente->ambiente;
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$content = file_get_contents('../certs/'.$emitente->cnpj.'/' . $json1['certPfxName']);
$xml = file_get_contents("{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfe.xml");

sleep(5);

try {

    $certificate = Certificate::readPfx($content, $json1['certPassword']);
    $tools = new Tools($json, $certificate);
    $tools->model('55');

    $response = $tools->sefazConsultaRecibo($recibo, intval($emitente->ambiente));

    $stdCl = new Standardize($response);
    $std = $stdCl->toStd();
    $arr = $stdCl->toArray();
    $json = $stdCl->toJson();

    $pasta = date("Ym");
    $nomepasta = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta";
    if (!is_dir($nomepasta)) :
        mkdir($nomepasta);
    endif;

    // $totalarr = count($arr);
    //echo '<pre>';
    //print_r($arr);
    //echo '</pre>';

    if (!isset($arr['protNFe']['infProt']['cStat'])) :
        $response = $tools->sefazConsultaChave($chave);
        $stdCl = new Standardize($response);
        $std = $stdCl->toStd();
        $arr = $stdCl->toArray();
        $json = $stdCl->toJson();
    endif;

    if ($arr['cStat'] == '105') :
        $response = $tools->sefazConsultaRecibo($recibo, intval($emitente->ambiente));
        $stdCl = new Standardize($response);
        $std = $stdCl->toStd();
        $arr = $stdCl->toArray();
        $json = $stdCl->toJson();
    endif;



    if ($arr['protNFe']['infProt']['cStat'] == '100') :

        $request = $xml;
        $respon = $stdCl;
        try {
            $xml = Complements::toAuthorize($request, $respon);
            $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfe.xml"; // Ambiente Windows
            file_put_contents($filename, $xml);
            chmod($filename, 0777);

            $protocolo = $arr['protNFe']['infProt']['nProt'];

            $pdo = Conexao::getInstance();
            $crudup = Crud::getInstance($pdo, 'nfe');
            $pdo->beginTransaction();
            $retorno = $crudup->Sql("UPDATE nfe set nProt = '$protocolo', status = '100' where nNF = {$nNFbd}");
            $pdo->commit();

            $xml = "<retorno>
                    <status>OK</status>
                    <nProt>" . $protocolo . "</nProt>
                    <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
            echo $xml;
        } catch (\Exception $e) {
            $xml = "<retorno>
        <status>erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
            echo $xml;
        //} elseif ($arr['protNFe']['infProt']['cStat'] == '204') :
        } elseif ($arr['protNFe']['infProt']['cStat'] == 'xxx') :
        $recibo_retorno = $arr['protNFe']['infProt']['xMotivo'];

        //$recibo_retorno = explode('[', $recibo_retorno);
        //$recibo_retorno = explode(':', $recibo_retorno[1]);
        //$recibo_retorno = substr($recibo_retorno[1], 0, -1);

        $recibo_retorno = explode('nRec:', $recibo_retorno);
        $recibo_retorno = substr($recibo_retorno[1], 0, -1);

        $response = $tools->sefazConsultaRecibo($recibo_retorno, intval($emitente->ambiente));


        $stdCl = new Standardize($response);
        $std = $stdCl->toStd();
        $arr = $stdCl->toArray();
        $json = $stdCl->toJson();

        if ($arr['protNFe']['infProt']['cStat'] == '100') :
            $request = $xml;
            $respon = $stdCl;
            try {
                $xml = Complements::toAuthorize($request, $respon);
                $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfe.xml"; // Ambiente Windows
                file_put_contents($filename, $xml);
                chmod($filename, 0777);

                $protocolo = $arr['protNFe']['infProt']['nProt'];

                $pdo = Conexao::getInstance();
                $crudup = Crud::getInstance($pdo, 'nfe');
                $pdo->beginTransaction();
                $retorno = $crudup->Sql("UPDATE nfe set nProt = '$protocolo', status = '100' where nNF = {$nNFbd}");
                $pdo->commit();

                $xml = "<retorno>
                    <status>OK</status>
                    <nProt>" . $protocolo . "</nProt>
                    <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
                echo $xml;

            } catch (\Exception $e) {
                $xml = "<retorno>
        <status>erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
                echo $xml;
            }
        endif;
    else :
        $xml = "<retorno>
        <status>erro</status>
        <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . " - " . $arr['xMotivo'] . "</motivo>
            </retorno>";
        echo $xml;
    endif;
} catch (\Exception $e) {
    $xml = "<retorno>
        <status>erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
    echo $xml;
}
?>