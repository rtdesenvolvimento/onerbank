<?php

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

///error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once '../bootstrap.php';

$json1 = file_get_contents('../config/' . $emitente->cnpj . '/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/' . $emitente->cnpj . '/' . $json1['certPfxName']);

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

$tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));

$tools->model('55');
$tools->setEnvironment(1);

$pdo                = Conexao::getInstance();
$crud               = Crud::getInstance($pdo, 'manifesta');
$maxNSUManifesta    = $crud->getSQLGeneric("SELECT max(nsu) as maxnsu from manifesta", '', TRUE);

$ultNSU = 0;
foreach ($maxNSUManifesta as $value) :
    $ultNSU = $value->maxnsu;
endforeach;

$maxNSU = $ultNSU;
$loopLimit = 20;
$iCount = 0;

while ($ultNSU <= $maxNSU) {

    $iCount++;
    if ($iCount >= $loopLimit) break;

    try {
        $resp = $tools->sefazDistDFe($ultNSU);
    } catch (\Exception $e) {
       echo $e->getMessage();
    }

    $dom = new \DOMDocument();
    $dom->loadXML($resp);
    $node = $dom->getElementsByTagName('retDistDFeInt')->item(0);
    $tpAmb = $node->getElementsByTagName('tpAmb')->item(0)->nodeValue;
    $verAplic = $node->getElementsByTagName('verAplic')->item(0)->nodeValue;
    $cStat = $node->getElementsByTagName('cStat')->item(0)->nodeValue;
    $xMotivo = $node->getElementsByTagName('xMotivo')->item(0)->nodeValue;
    $dhResp = $node->getElementsByTagName('dhResp')->item(0)->nodeValue;
    $ultNSU = $node->getElementsByTagName('ultNSU')->item(0)->nodeValue;
    $maxNSU = $node->getElementsByTagName('maxNSU')->item(0)->nodeValue;
    $lote = $node->getElementsByTagName('loteDistDFeInt')->item(0);

    if (empty($lote)) continue;

    $docs = $lote->getElementsByTagName('docZip');

    foreach ($docs as $doc) {

        $numnsu = $doc->getAttribute('NSU');
        $schema = $doc->getAttribute('schema');

        $content = gzdecode(base64_decode($doc->nodeValue));
        $tipo = substr($schema, 0, 6);

        $xml = new Standardize();
        $xml->toArray(gzdecode(base64_decode($doc->nodeValue)));
        $xml1 = json_decode($xml->json);

        if ($schema == 'resNFe_v1.01.xsd') {

            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'manifesta');
            $verificar = $crud->getSQLGeneric("SELECT * from manifesta where chave ='" . $xml1->chNFe."'", '', TRUE);

            if (count($verificar) <= 0) {
                $data = parseDate(substr($xml1->dhRecbto, 0, 10), 'd/m/Y');
                $pdo = Conexao::getInstance();
                $crud = Crud::getInstance($pdo, 'manifesta');
                $sql = "INSERT INTO manifesta(chave, xml, xNome, data, valor, emitente, nsu) VALUES ('$xml1->chNFe', '','$xml1->xNome','$data','$xml1->vNF','$emitente->id',$ultNSU)";
                $dados = $crud->Sql($sql);
            }

        } else if ($schema == 'procNFe_v4.00.xsd') {

            $xNome =  $xml1->NFe->infNFe->emit->CNPJ;;
            $nNF = $xml1->NFe->infNFe->ide->nNF;
            $dhEmi = $xml1->NFe->infNFe->ide->dhEmi;
            $chave = $xml1->protNFe->infProt->chNFe;
            $valorNF = $xml1->NFe->infNFe->total->ICMSTot->vNF;

            $filename = "../XML/{$emitente->cnpj}/NF-e/producao/recebidas/{$chave}-nfe.xml"; // Ambiente Windows

            if (file_exists($filename)) unlink($filename);

            $saveXml = gzdecode(base64_decode($doc->nodeValue));
            file_put_contents($filename, $saveXml);

            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'manifesta');
            $verificar = $crud->getSQLGeneric("SELECT * from manifesta where chave ='" .$chave."'", '', TRUE);

           if (count($verificar) <= 0) {
                $data = parseDate(substr($dhEmi, 0, 10), 'd/m/Y');
                $pdo = Conexao::getInstance();
                $crud = Crud::getInstance($pdo, 'manifesta');
                $sql = "INSERT INTO manifesta(chave, xml, xNome, data, valor, emitente, manifestada, nsu) VALUES ('$chave', '','$xNome','$data','$valorNF','$emitente->id','S',$ultNSU)";
                $dados = $crud->Sql($sql);
           } else {
               $pdo = Conexao::getInstance();
               $crud = Crud::getInstance($pdo, 'manifesta');
               $sql = "UPDATE MANIFESTA SET manifestada = 'S', donwload = 'S' WHERE CHAVE ='".$chave."'";
               $dados = $crud->Sql($sql);
           }
        }
    }

    sleep(2);
}

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'caixa');
$sql = "UPDATE emitente set NSU = '$ultNSU' where emitenteid = '$emitente->id' ";
$dados = $crud->Sql($sql);

$array = array("status" => "ok", "msg" => 'Busca Realizada!');
echo json_encode($array);