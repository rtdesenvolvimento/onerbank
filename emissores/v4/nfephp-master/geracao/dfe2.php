<?php

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

error_reporting(0);
ini_set('display_errors', 0 );

require_once '../bootstrap.php';

$json1 = file_get_contents('../config/' . $emitente->cnpj . '/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/' . $emitente->cnpj . '/' . $json1['certPfxName']);

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

$tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));

//só funciona para o modelo 55
$tools->model('55');
//este serviço somente opera em ambiente de produção
$tools->setEnvironment(1);
//
//este numero deverá vir do banco de dados nas proximas buscas para reduzir 
//a quantidade de documentos, e para não baixar várias vezes as mesmas coisas.

$pdo                = Conexao::getInstance();
$crud               = Crud::getInstance($pdo, 'manifesta');
$maxNSUManifesta    = $crud->getSQLGeneric("SELECT max(nsu) as maxnsu from manifesta", '', TRUE);

$ultNSU = 0;
foreach ($maxNSUManifesta as $value) :
    $ultNSU = $value->maxnsu;
endforeach;

$maxNSU     = $ultNSU;
$loopLimit  = 5;
$iCount     = 0;

//executa a busca de DFe em loop
while ($ultNSU <= $maxNSU) {

    $iCount++;
    if ($iCount >= $loopLimit) break;

    try {
        //echo $ultNSU.'<br/>';
        $resp = $tools->sefazDistDFe($ultNSU);
    } catch (\Exception $e) {
        $array = array("error" => "ok", "msg" => $e->getMessage());
        echo json_encode($array);
        die();
    }

    //extrair e salvar os retornos
    $dom = new \DOMDocument();
    $dom->loadXML($resp);
    $node = $dom->getElementsByTagName('retDistDFeInt')->item(0);
    $tpAmb = $node->getElementsByTagName('tpAmb')->item(0)->nodeValue;
    $verAplic = $node->getElementsByTagName('verAplic')->item(0)->nodeValue;
    $cStat = $node->getElementsByTagName('cStat')->item(0)->nodeValue;
    $xMotivo = $node->getElementsByTagName('xMotivo')->item(0)->nodeValue;
    $dhResp = $node->getElementsByTagName('dhResp')->item(0)->nodeValue;
    $ultNSU = $node->getElementsByTagName('ultNSU')->item(0)->nodeValue;
    $maxNSU = $node->getElementsByTagName('maxNSU')->item(0)->nodeValue;
    $lote = $node->getElementsByTagName('loteDistDFeInt')->item(0);

    if (empty($lote)) continue;

    //essas tags irão conter os documentos zipados
    $docs = $lote->getElementsByTagName('docZip');

    foreach ($docs as $doc) {

        $xml = new Standardize();
        $xml->toArray(gzdecode(base64_decode($doc->nodeValue)));
        $xml1 = json_decode($xml->json);

        if ($xml1->chNFe) {

            $pdo        = Conexao::getInstance();
            $crud       = Crud::getInstance($pdo, 'manifesta');
            $verificar  = $crud->getSQLGeneric("SELECT * from manifesta where chave =".$xml1->chNFe, '', TRUE);

            if (count($verificar) <= 0) {
                $data = parseDate(substr($xml1->dhRecbto, 0, 10), 'd/m/Y');
                $pdo = Conexao::getInstance();
                $crud = Crud::getInstance($pdo, 'manifesta');
                $sql = "INSERT INTO manifesta(chave, xNome, data, valor, emitente, nsu) VALUES ('$xml1->chNFe','$xml1->xNome','$data','$xml1->vNF','$emitente->id',$ultNSU)";
                $dados = $crud->Sql($sql);
            }
        }
    }
    sleep(2);
}

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "UPDATE emitente set nsu = '$ultNSU' where id = '$emitente->id' ";
$dados = $crud->Sql($sql);

$array = array("status" => "ok", "msg" => 'Busca Realizada!');
echo json_encode($array);
