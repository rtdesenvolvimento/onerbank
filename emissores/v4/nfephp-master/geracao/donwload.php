<?php

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

$json1 = file_get_contents('../config/' . $emitente->cnpj . '/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/' . $emitente->cnpj . '/' . $json1['certPfxName']);

try {

    $idManifesto = $_GET['idManifesto'];

    $tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));
    $tools->model('55');
    $tools->setEnvironment(1);

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'manifesta');
    $sql = "SELECT * from manifesta where donwload = 'N' and  manifestaid in (".$idManifesto.'-1)';
    $arrayParam = '';
    $dadosmanifesta = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

    foreach ($dadosmanifesta as $value) {

        $filename = "../XML/{$emitente->cnpj}/NF-e/producao/recebidas/{$value->chave}-nfe.xml"; // Ambiente Windows
        if (file_exists($filename)) unlink($filename);

        $chave = $value->chave;
        $response = $tools->sefazDownload($chave);

        try {
            $stz = new Standardize($response);
            $std = $stz->toStd();

            if ($std->cStat != 138) {
                $array = array("error" => "ok", "msg" => "Documento não retornado. [$std->cStat] $std->xMotivo");
                echo json_encode($array);
                die;
            }

            $zip = $std->loteDistDFeInt->docZip;
            $xml = gzdecode(base64_decode($zip));

            file_put_contents($filename, $xml);

            $pdo = Conexao::getInstance();
            $crud = Crud::getInstance($pdo, 'manifesta');
            $sql = "UPDATE manifesta set donwload = 'S' where manifestaid = '$value->manifestaid'";
            $retorno = $crud->Sql($sql);

        } catch (\Exception $e) {
            $array = array("error" => "ok", "msg" => str_replace("\n", "<br/>", $e->getMessage()));
            echo json_encode($array);
        }
        sleep(4);
    }

    $array = array("status" => "ok", "msg" => 'Download realizado com sucesso.');
    echo json_encode($array);
} catch (\Exception $e) {
    $array = array("error" => "ok", "msg" => $e->getMessage());
    echo json_encode($array);
}