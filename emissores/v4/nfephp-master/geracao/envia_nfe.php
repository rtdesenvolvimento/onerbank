﻿<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once '../bootstrap.php';

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';


header('Content-type: text/xml; charset=UTF-8');

$nNFbd = $_GET['arquivo'];

use NFePHP\NFe\Convert;
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$chave = $dadosnfe->chavenfe;


$json1 = file_get_contents('../config/'.$emitente->cnpj.'/config.json');
$json1 = json_decode($json1, true);

$tpAmb = $emitente->ambiente;
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$content = file_get_contents('../certs/'.$emitente->cnpj.'/' . $json1['certPfxName']);
$xml = file_get_contents("{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfe.xml");

try {

    $tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));
    $idLote = str_pad($nNFbd, 15, '0', STR_PAD_LEFT);
    $resp = $tools->sefazEnviaLote([$xml], $idLote);
    $st = new Standardize();
    $std = $st->toStd($resp);

    if ($std->cStat != 103) {
        $resposta = array("Status" => 'Error', "Recibo" => "[$std->cStat] $std->xMotivo");
        echo json_encode($resposta);
        exit;
    }

    $recibo = $std->infRec->nRec;
    $pdo = Conexao::getInstance();
    $crudup = Crud::getInstance($pdo, 'nfe');
    $pdo->beginTransaction();
    $retorno = $crudup->Sql("UPDATE nfe set recibo = '$recibo' where nNF = {$nNFbd}");
    $pdo->commit();
    $pdo = null;
    $curd = null;

    $xml = "<retorno>
        <status>OK</status>
        <recibo>" . $recibo . "</recibo>
            </retorno>";
    echo $xml;
} catch (\Exception $e) {
    $xml = "<retorno>
        <status>erro</status>
        <motivo>" . str_replace("\n", "<br/>", $e->getMessage()) . "</motivo>
            </retorno>";
    echo $xml;
}
?>