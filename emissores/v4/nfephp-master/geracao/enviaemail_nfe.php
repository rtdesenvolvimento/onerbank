<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
require_once "../../_backend/_class/Emitente_class.php";
require_once "../../_backend/_class/configura_email_class.php";


error_reporting(E_ALL);
ini_set('display_errors', 'On');

$nNFbd = $_GET['arquivo'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe, recibo from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$json1 = file_get_contents('../config/' . $_SESSION['BD'] . '/config.json');
$json1 = json_decode($json1, true);


//print_r($json1);
require_once '../bootstrap.php';
require_once '../src/mail.php';

try {
    //a configuração é uma stdClass com os campos acima indicados
    //esse parametro é OBRIGATÓRIO
    $mail = new Mail($config);

    //use isso para inserir seu próprio template HTML com os campos corretos 
    //para serem substituidos em execução com os dados dos xml
    $htmlTemplate = '';
    $mail->loadTemplate($htmlTemplate);
    //aqui são passados os documentos, tanto pode ser um path como o conteudo
    //desses documentos
    $xml = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfe.xml";
    $pdf = ''; //não é obrigatório passar o PDF, tendo em vista que é uma BOBAGEM
    $mail->loadDocuments($xml, $pdf);

    //se não for passado esse array serão enviados apenas os emails
    //que estão contidos no XML, isto se existirem
    $addresses = [];

    //envia emails, se false apenas para os endereçospassados
    //se true para todos os endereços contidos no XML e mais os indicados adicionais
    $mail->send($addresses, true);
} catch (\InvalidArgumentException $e) {
    echo "Falha: " . $e->getMessage();
} catch (\RuntimeException $e) {
    echo "Falha: " . $e->getMessage();
} catch (\Exception $e) {
    echo "Falha: " . $e->getMessage();
}  