﻿<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
require_once "../../_backend/_class/Emitente_class.php";
require_once '../bootstrap.php';

header('Content-type: text/xml; charset=UTF-8');
//
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;

$nSerie = '1';
$nIni = $_GET['nIni'];
$nFin = $_GET['nFin'];
$xJust = $_GET['xJust'];
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');


$json1 = file_get_contents('../config/' . $_SESSION['BD'] . '/config.json');
$json1 = json_decode($json1, true);

$content = file_get_contents('../certs/' . $_SESSION['BD'] . '/' . $json1['certPfxName']);


try {

    $certificate = Certificate::readPfx($content, $json1['certPassword']);
    $tools = new Tools($json, $certificate);
//    $tools->model('55');
    $response = $tools->sefazInutiliza($nSerie, $nIni, $nFin, $xJust);
    $stdCl = new Standardize($response);
    //nesse caso $std irá conter uma representação em stdClass do XML
    $std = $stdCl->toStd();
    //nesse caso o $arr irá conter uma representação em array do XML
    $arr = $stdCl->toArray();
    //nesse caso o $json irá conter uma representação em JSON do XML
    $json = $stdCl->toJson();

//    echo '<pre>';
//    print_r($arr);
//    echo '</pre>';

    $xml = "<retorno>
                    <status>OK</status>
                    <motivo>" . $std->infInut->xMotivo . " </motivo>
                    </retorno>";
    echo $xml;
} catch (\Exception $e) {
    $xml = "<retorno>
                    <status>OK</status>
                    <motivo>" . $e->getMessage() . " </motivo>
                    </retorno>";
    echo $xml;
}
?>