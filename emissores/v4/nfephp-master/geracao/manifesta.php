<?php

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$json1      = file_get_contents('../config/' . $emitente->cnpj. '/config.json');
$json1      = json_decode($json1, true);
$content    = file_get_contents('../certs/' . $emitente->cnpj . '/' . $json1['certPfxName']);

try {

    $idManifesto = $_GET['idManifesto'];

    $tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));
    $tools->model('55');
    $std    = new stdClass();
    $consulta = true;

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'manifesta');
    $sql = "SELECT * from manifesta where manifestada = 'N' and manifestaid in (".$idManifesto."-1)";
    $dadosmanifesta = $crud->getSQLGeneric($sql, '', TRUE);

    $i = 0;
    foreach ($dadosmanifesta as $value) :
        $std->evento[$i] = new stdClass();
        $std->evento[$i]->chNFe = $value->chave;
        $std->evento[$i]->tpEvento = 210210; //evento ciencia da operação
        $std->evento[$i]->xJust = null;
        $std->evento[$i]->nSeqEvento = 1;
        $i++;

        $pdo = Conexao::getInstance();
        $crud = Crud::getInstance($pdo, 'manifesta');
        $sql = "UPDATE manifesta set manifestada = 'S' where manifestaid = '$value->manifestaid'";
        $retorno = $crud->Sql($sql);
    endforeach;

    if (count($dadosmanifesta) > 0) $response = $tools->sefazManifestaLote($std);

    $array = array("status" => "ok", "msg" => 'Notas Manifestadas');
    echo json_encode($array);
} catch (\Exception $e) {
    $array = array("error" => "ok", "msg" => $e->getMessage());
    echo json_encode($array);
}