﻿<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

require_once '../bootstrap.php';

use NFePHP\NFe\Convert;
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

//print_r($json);

$nNFbd = $_GET['arquivo'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$chave = $dadosnfe->chavenfe;

$json = file_get_contents('../config/' . $_SESSION['BD'] . '/config.json');
$json1 = json_decode($json, true);


$tpAmb = '2';
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

//echo $xml;
//print_r($json);

$config = [
    "atualizacao" => "2015-10-02 06:01:21",
    "tpAmb" => 2,
    "razaosocial" => "CARRETINHAS BRASIL LTDA - ME",
    "siglaUF" => "MG",
    "cnpj" => "07524720000103",
    "schemes" => "PL_009_V4",
    "versao" => "4.00",
    "tokenIBPT" => "AAAAAAA",
    "CSC" => "GPB0JBWLUR6HWFTVEAS6RJ69GPCROFPBBB8G",
    "CSCid" => "000002",
    "aProxyConf" => [
        "proxyIp" => "",
        "proxyPort" => "",
        "proxyUser" => "",
        "proxyPass" => ""
    ]
];
$json = json_encode($config);

$content = file_get_contents('../certs/' . $_SESSION['BD'] . '/' . $json1['certPfxName']);
$xml = file_get_contents("{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfe.xml"); // Ambiente Windows


try {
    $tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));
    $idLote = str_pad($nfemit->id, 15, '0', STR_PAD_LEFT);
    //envia o xml para pedir autorização ao SEFAZ
    $resp = $this->tools->sefazEnviaLote([$xml], $idLote);
    //transforma o xml de retorno em um stdClass
    $st = new Standardize();
    $std = $st->toStd($resp);
    if ($std->cStat != 103) {
        //erro registrar e voltar
        return "[$std->cStat] $std->xMotivo";
    }
    $recibo = $std->infRec->nRec;
    //esse recibo deve ser guardado para a proxima operação que é a consulta do recibo
    header('Content-type: text/xml; charset=UTF-8');
    echo $resp;
} catch (\Exception $e) {
    echo str_replace("\n", "<br/>", $e->getMessage());
}
?>