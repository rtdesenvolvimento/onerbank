<?php

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

set_time_limit(0);

$chave = $_GET['chave'].'-nfe.xml';

define('DIR',dirname(dirname(dirname(__FILE__))));

$arquivo  = "".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/producao/recebidas/".$chave; // Nome do Arquivo

header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="'.$chave.'"');
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($arquivo));
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Expires: 0');

readfile($arquivo);


?>