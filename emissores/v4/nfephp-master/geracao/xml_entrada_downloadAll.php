<?php

if (!isset($_SESSION)):
    session_start();
endif;

if (!isset($_SESSION['BD'])){
    $BANCO = $_GET['BD'];
} else {
    $BANCO =  $_SESSION['BD'];
}


require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

define('DIR',dirname(dirname(dirname(__FILE__))));

$tpAmb = $emitente->ambiente;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pastaxml = "producao";

$zip = new ZipArchive();

if ($zip->open("".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/{$_GET['periodo']}_entrada.zip", ZipArchive::CREATE) === true) {


    foreach (glob("".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/recebidas/*.*") as $current) {

        $xml    = simplexml_load_file($current);
        $dhemi  = substr($xml->NFe->infNFe->ide->dhEmi, 5, 2);
        $dhemi .= substr($xml->NFe->infNFe->ide->dhEmi, 0, 4);

        if ($_GET['periodo'] == $dhemi) {
            $zip->addFile($current, 'Recebidas_'.$_GET['periodo'].'/' . basename($current));
        }
    }
    $zip->close();
}


$arquivo = "".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/{$_GET['periodo']}_entrada.zip"; // Nome do Arquivo
$local_arquivo = $arquivo; // Concatena o diretorio com o nome do arquivo

if (stripos($arquivo, './') !== false || stripos($arquivo, '../') !== false || !file_exists($local_arquivo)) {
    echo 'O comando não pode ser executado.';
} else {

    $nomeArquivo = "{$_GET['periodo']}_entrada";

    header('Cache-control: private');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: filename='.$nomeArquivo.'.zip"');
    header('Content-Disposition: attachment; filename='.$nomeArquivo.'.zip');

    readfile($local_arquivo);
}
?>