﻿<?php

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

require_once '../bootstrap.php';

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;


//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

$json1 = file_get_contents('../config/' . $_SESSION['BD'] . '/config.json');
$json1 = json_decode($json1, true);

$nNFbd = $_GET['arquivo'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe from nfce where nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$chave = $dadosnfe->chavenfe;
//echo $sql;
$tpAmb = $emitente->ambiente;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$xml = file_get_contents("{$json1['pathNFeFiles']}/{$pastaxml}/entradas/{$chave}-nfce.xml"); // Ambiente Windows

try {
    $content = file_get_contents('../certs/' . $_SESSION['BD'] . '/' . $json1['certPfxName']);
    $tools = new Tools($json, Certificate::readPfx($content, $json1['certPassword']));
    $response = $tools->signNFe($xml);

    $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfce.xml"; // Ambiente Windows
    file_put_contents($filename, $response);

    header('Content-type: text/xml; charset=UTF-8');

    $xml = "<retorno>
        <status>OK</status>
            </retorno>";
    echo $xml;
} catch (\Exception $e) {
    $arr = array("Status" => "erro", "mensagem" => $e->getMessage());
    $xml = "<retorno>
        <status>erro</status>
        <motivo> " . $e->getMessage() . "</motivo>
            </retorno>";
    echo $xml;
}