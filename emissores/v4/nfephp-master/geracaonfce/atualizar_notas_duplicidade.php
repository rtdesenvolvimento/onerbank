<?php

header('Content-type: text/xml; charset=UTF-8');

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once '../bootstrap.php';

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;

$nNFbd = $_GET['nNF'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe, recibo from nfce where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$json1 = file_get_contents('../config/'.$emitente->cnpj.'/config.json');
$json1 = json_decode($json1, true);

$chave = $dadosnfe->chavenfe;
$recibo = $dadosnfe->recibo;

$tpAmb = $emitente->ambiente;

if ($tpAmb == '1') : $pastaxml = "producao";
else : $pastaxml = "homologacao"; endif;

$json1 = file_get_contents('../config/'.$emitente->cnpj.'/config.json');
$json1 = json_decode($json1, true);
$content = file_get_contents('../certs/'.$emitente->cnpj.'/' . $json1['certPfxName']);

if (!file_exists("{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfce.xml")) {
    echo "<retorno><status>erro</status><motivo>Arquivo nao encontrato.</motivo></retorno>";
    return;
};

$xml = file_get_contents("{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfce.xml");

sleep(5);

try {

    $certificate = Certificate::readPfx($content, $json1['certPassword']);
    $tools = new Tools($json, $certificate);
    $tools->model('65');

    //$response = $tools->sefazConsultaRecibo($recibo, intval($emitente->ambiente));
    $response = $tools->sefazConsultaChave($chave);
    $stdCl = new Standardize($response);

    $std = $stdCl->toStd();
    $arr = $stdCl->toArray();
    $json = $stdCl->toJson();

    if ($arr['xMotivo'] != null) {
        $xml = "<retorno>
                    <status>OK</status>
                     <motivo>" .$arr['xMotivo'] . "</motivo>
                    </retorno>";
        echo $xml;
        return;
    }
    if ($arr['protNFe']['infProt']['cStat'] == '100') {

        $protocolo  = $arr['protNFe']['infProt']['nProt'];
        $dhRecbto   = $arr['protNFe']['infProt']['dhRecbto'];

        $request    = $xml;
        $respon     = $stdCl;

        //$dhEmi      = date('Y-m-d', strtotime($dhRecbto));
        //$hEmi       = date('H:i:s', strtotime($dhRecbto));
        $pasta      = date('Ym', strtotime($dhRecbto));

        $nomepasta  = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta";

        if (!is_dir($nomepasta)) mkdir($nomepasta);

        $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfce.xml"; // Ambiente Windows

        if (!file_exists($filename)) {
            $pdo = Conexao::getInstance();
            $crudup = Crud::getInstance($pdo, 'nfe');
            $pdo->beginTransaction();
            //$retorno = $crudup->Sql("UPDATE nfce set hEmi='$hEmi', dhEmi='$dhEmi', nProt = '$protocolo', status = '100' where nNF = $nNFbd");
            $retorno = $crudup->Sql("UPDATE nfce set nProt = '$protocolo', status = '100' where nNF = $nNFbd");
            $pdo->commit();

            $xml = Complements::toAuthorize($request, $respon);
            file_put_contents($filename, $xml);
            chmod($filename, 0777);

            $xml = "<retorno>
                    <status>OK</status>
                    <nProt>" . $protocolo . "</nProt>
                    <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
            echo $xml;
        } else {

            $pdo = Conexao::getInstance();
            $crudup = Crud::getInstance($pdo, 'nfe');
            $pdo->beginTransaction();
            //$retorno = $crudup->Sql("UPDATE nfce set hEmi='$hEmi', dhEmi='$dhEmi', nProt = '$protocolo', status = '100' where nNF = $nNFbd");
            $retorno = $crudup->Sql("UPDATE nfce set nProt = '$protocolo', status = '100' where nNF = $nNFbd");
            $pdo->commit();

            $xml = "<retorno>
                    <status>OK</status>
                    <nProt>" . $protocolo . "</nProt>
                    <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
            echo $xml;
        }
    }

} catch (\Exception $e) {

    $xml = "<retorno><status>erro</status><motivo>".$e->getMessage()."</motivo></retorno>";

    $pdo = Conexao::getInstance();
    $crudup = Crud::getInstance($pdo, 'nfe');
    $pdo->beginTransaction();
    $retorno = $crudup->Sql("UPDATE nfce set status = '' where nNF = $nNFbd");
    $pdo->commit();

    echo $xml;
}
?>