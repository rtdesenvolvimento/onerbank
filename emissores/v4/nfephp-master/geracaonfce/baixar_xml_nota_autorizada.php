﻿﻿<?php
require_once '../bootstrap.php';

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
//error_reporting(0);

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;

$json1 = file_get_contents('../config/' .$emitente->cnpj . '/config.json');
$json1 = json_decode($json1, true);

$nNFbd =  $_GET['nNF'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe, recibo, dhEmi from nfce where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$chave = $dadosnfe->chavenfe;
$recibo = $dadosnfe->recibo;
$pasta =  date('Ym', strtotime($dadosnfe->dhEmi));


//echo $recibo;
$tpAmb = $emitente->ambiente;
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$content = file_get_contents('../certs/' .$emitente->cnpj. '/' . $json1['certPfxName']);
$xml = file_get_contents("{$json1['pathNFeFiles']}/{$pastaxml}/assinadas/{$chave}-nfce.xml");

try {

    $certificate = Certificate::readPfx($content, $json1['certPassword']);
    $tools = new Tools($json, $certificate);
    $tools->model('65');

    $response = $tools->sefazConsultaChave($chave);
    //$response = $tools->sefazConsultaRecibo($recibo, intval($emitente->ambiente));

    $stdCl = new Standardize($response);
    $std = $stdCl->toStd();
    $arr = $stdCl->toArray();
    $json = $stdCl->toJson();

    $nomepasta = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta";
    if (!is_dir($nomepasta)) :
        mkdir($nomepasta);
    endif;

    if ($arr['cStat'] == '217') :
        $xml = "<retorno>
                    <status>erro</status>
                     <motivo>" . $arr['xMotivo'] . "</motivo>
                    </retorno>";
        echo $xml;
        exit;
    endif;

    if ($arr['protNFe']['infProt']['cStat'] == '204') :

        //$aRetorno = $tools->sefazDownload($chave, $tpAmb, $emitente->cnpj, $aRetorno);
        //$aRetorno = $tools->sefazManifesta($chave,'210200' );

        $xml = "<retorno>
                    <status>erro</status>
                     <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
        echo $xml;
        exit;
    endif;

    if ($arr['protNFe']['infProt']['cStat'] == '100') :


        $request = $xml;
        $respon = $stdCl;
        try {
            $xml = Complements::toAuthorize($request, $respon);
            $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfce.xml"; // Ambiente Windows
            file_put_contents($filename, $xml);
            chmod($filename, 0777);

            $protocolo = $arr['protNFe']['infProt']['nProt'];

            $pdo = Conexao::getInstance();
            $crudup = Crud::getInstance($pdo, 'nfce');
            $pdo->beginTransaction();
            $retorno = $crudup->Sql("UPDATE nfce set nProt = '$protocolo', status = '100' where nNF = {$nNFbd}");
            $pdo->commit();

            $xml = "<retorno>
                    <status>OK</status>
                    <nProt>" . $protocolo . "</nProt>
                    <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
            echo $xml;

//            echo $xml;
        } catch (\Exception $e) {
            $xml = "<retorno>
        <status>erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
            echo $xml;
        } elseif ($arr['protNFe']['infProt']['cStat'] == '204') :

        /*
        $recibo = $arr['protNFe']['infProt']['xMotivo'];
        $recibo = explode('[', $recibo);
        $recibo = explode(':', $recibo[1]);
        $recibo = substr($recibo[1], 0, -1);
        */

        $recibo_retorno = $arr['protNFe']['infProt']['xMotivo'];

        //$recibo_retorno = explode('[', $recibo_retorno);
        //$recibo_retorno = explode(':', $recibo_retorno[1]);
        //$recibo_retorno = substr($recibo_retorno[1], 0, -1);

        $recibo_retorno = explode('nRec:', $recibo_retorno);

        //echo $recibo;

        //$response = $tools->sefazConsultaChave($chave);
        $response = $tools->sefazConsultaRecibo($recibo, intval($emitente->ambiente));

        $stdCl = new Standardize($response);
        $std = $stdCl->toStd();
        $arr = $stdCl->toArray();
        $json = $stdCl->toJson();

        if ($arr['protNFe']['infProt']['cStat'] == '100') :
            $request = $xml;
            $respon = $stdCl;
            try {
                $xml = Complements::toAuthorize($request, $respon);
                $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfce.xml"; // Ambiente Windows
                file_put_contents($filename, $xml);
                chmod($filename, 0777);

                $protocolo = $arr['protNFe']['infProt']['nProt'];

                $pdo = Conexao::getInstance();
                $crudup = Crud::getInstance($pdo, 'nfce');
                $pdo->beginTransaction();
                $retorno = $crudup->Sql("UPDATE nfce set nProt = '$protocolo', status = '100' where nNF = {$nNFbd}");
                $pdo->commit();


                $xml = "<retorno>
                    <status>OK</status>
                    <nProt>" . $protocolo . "</nProt>
                    <motivo>" . $arr['protNFe']['infProt']['xMotivo'] . "</motivo>
                    </retorno>";
                echo $xml;

//            echo $xml;
            } catch (\Exception $e) {
                $xml = "<retorno>
                <status>erro</status>
                <motivo>" . $e->getMessage() . "</motivo>
                    </retorno>";
                echo $xml;
            }
        endif;
    else :

//        $cstat = $arr['protNFe']['infProt']['cStat'];

        if (!empty($arr['xMotivo'])) {
            $motivo = $arr['xMotivo'];
            $encontrado = (strpos($motivo, 'Duplicidade de NF-e') !== 0);

            if ($encontrado) {
                $chave = explode('NF-e', $motivo);

                if (count($chave) > 1) {
                    $chave = explode(']', $chave[1]);
                    $chave = $chave[0];
                    $chave = trim (str_replace ('[','',$chave));

                    $pdo = Conexao::getInstance();
                    $crudup = Crud::getInstance($pdo, 'nfce');
                    $pdo->beginTransaction();
                    $retorno = $crudup->Sql("UPDATE nfce set chavenfe = '$chave' where nNF = {$nNFbd}");
                    $pdo->commit();

                    $xml = "<retorno>
                            <status>erro</status>
                            <motivo>" . $motivo . " CONSEGUIMOS ATUALIZAR A CHAVE NA NFC-E tente enivar novamente na Tela de Pendencias!</motivo>
                        </retorno>";
                    echo $xml;
                } else {
                    $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $motivo . "</motivo>
                        </retorno>";
                    echo $xml;
                }
            } else {
                $xml = "<retorno>
            <status>erro</status>
            <motivo>" . $motivo . "</motivo>
                </retorno>";
                echo $xml;
            }
        }
    endif;
} catch (\Exception $e) {
    $xml = "<retorno>
        <status>erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
    echo $xml;
}
?>