﻿<?php

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

//header('Content-type: text/xml; charset=UTF-8');

$nNFbd = $_GET['nNF'];

use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

$json1 = file_get_contents('../config/' .$emitente->cnpj. '/config.json');
$json1 = json_decode($json1, true);

//print_r($json);

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe, recibo, nProt from nfce where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$chave = $dadosnfe->chavenfe;

$nProt = $dadosnfe->nProt;
$xJust = $_GET['justificativa'];
//$xJust = 'Erro de digitação nos dados dos produtos';

$tpAmb = $emitente->ambiente;
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;


$pasta = date("Ym");
$nomepasta = "{$json1['pathNFeFiles']}/{$pastaxml}/canceladas/$pasta";
if (!is_dir($nomepasta)) :
    mkdir($nomepasta);
endif;

$content = file_get_contents('../certs/' .$emitente->cnpj. '/' . $json1['certPfxName']);
//

try {

    $certificate = Certificate::readPfx($content, $json1['certPassword']);
    $tools = new Tools($json, $certificate);
    $tools->model('65');

    $response = $tools->sefazCancela($chave, $xJust, $nProt);

    //você pode padronizar os dados de retorno atraves da classe abaixo
    //de forma a facilitar a extração dos dados do XML
    //NOTA: mas lembre-se que esse XML muitas vezes será necessário, 
    //      quando houver a necessidade de protocolos
    $stdCl = new Standardize($response);
    //nesse caso $std irá conter uma representação em stdClass do XML
    $std = $stdCl->toStd();
    //nesse caso o $arr irá conter uma representação em array do XML
    $arr = $stdCl->toArray();
    //nesse caso o $json irá conter uma representação em JSON do XML
    $json = $stdCl->toJson();

//    echo '<pre>';
//    print_r($arr);
//    echo '</pre>';
    //verifique se o evento foi processado
    if ($std->cStat != 128) {
        /*
        $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $std->xMotivo . " </motivo>
                    </retorno>";
        echo $xml;
        */
        echo $std->xMotivo;
    } else {
        $cStat = $std->retEvento->infEvento->cStat;
        if ($cStat == '101' || $cStat == '155' || $cStat == '135') {
            //SUCESSO PROTOCOLAR A SOLICITAÇÂO ANTES DE GUARDAR
            $xml = Complements::toAuthorize($tools->lastRequest, $response);
            $filename = "{$json1['pathNFeFiles']}/{$pastaxml}/canceladas/$pasta/{$chave}-nfce.xml"; // Ambiente Windows
            file_put_contents($filename, $xml);
            chmod($filename, 0777);
            //grave o XML protocolado 


            $pdo = Conexao::getInstance();
            $crudup = Crud::getInstance($pdo, 'nfe');
            $pdo->beginTransaction();
            $retorno = $crudup->Sql("UPDATE nfce set status='101' where nNF = {$nNFbd}");
            $pdo->commit();


            /*
            $xml = "<retorno>
                    <status>OK</status>
                    <motivo>" . $std->retEvento->infEvento->xMotivo . " </motivo>
                    </retorno>";
            echo $xml;
            */
            echo $std->retEvento->infEvento->xMotivo;
        } else {
            /*
            $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $std->retEvento->infEvento->xMotivo . " </motivo>
                    </retorno>";
            echo $xml;
            */
            echo $std->retEvento->infEvento->xMotivo;
        }
    }
} catch (\Exception $e) {
    /*
    $xml = "<retorno>
                    <status>erro</status>
                    <motivo>" . $e->getMessage() . " </motivo>
                    </retorno>";
    echo $xml;
    */
    echo $e->getMessage();
}
?>