<?php
date_default_timezone_set("Brazil/East");


require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
//echo $_SESSION['BD'];
//$nfeTools = new ToolsNFe('../config/' . $_SESSION['BD'] . '/config.json');

function removeAcentos($texto) {
    $de = array('Á', 'Í', 'Ó', 'Ú', 'É', 'Ä', 'Ï', 'Ö', 'Ü', 'Ë', 'À', 'Ì', 'Ò', 'Ù', 'È', 'Ã', 'Õ', 'Â', 'Î', 'Ô', 'Û', 'Ê', 'á', 'í', 'ó', 'ú', 'é', 'ä', 'ï', 'ö', 'ü', 'ë', 'à', 'ì', 'ò', 'ù', 'è', 'ã', 'õ', 'â', 'î', 'ô', 'û', 'ê', 'Ç', 'ç', ' ');
    $para = array('A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'I', 'O', 'U', 'E', 'A', 'O', 'A', 'I', 'O', 'U', 'E', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'i', 'o', 'u', 'e', 'a', 'o', 'a', 'i', 'o', 'u', 'e', 'C', 'c', ' ');
    return preg_replace("/[^a-zA-Z0-9_-]/", " ", str_replace($de, $para, $texto));
}

function soNumero($str) {
    return preg_replace("/[^0-9]/", "", $str);
}

function ICMSInter() {
    $ano = date("Y");
    switch ($ano) {
        case '2016':
            $valor = 40;
            break;
        case '2017':
            $valor = 60;
            break;
        case '2018':
            $valor = 100;
            break;
        default:
            $valor = 100;
            break;
    }
    return $valor;
}

function ICMSInterOr() {
    $ano = date("Y");
    switch ($ano) {
        case '2016':
            $valor = 60;
            break;
        case '2017':
            $valor = 40;
            break;
        case '2018':
            $valor = 20;
            break;
        default:
            $valor = 0;
            break;
    }
    return $valor;
}

function BuscaAliquota($ufdest, $ufemitente) {
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'nfe_aliquota_interestadual');
    $sql = "SELECT * from nfe_aliquota_interestadual";
    $sql .= " where origem = '$ufemitente' and destino = '$ufdest'";
    $arrayParam = '';
    $aliq = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
    $pdo = null;
    $curd = null;

    return $aliq;
}

//NUMERO DA NOTA VIA GET
$nNFbd = $_GET['nNF'];
$nNF = $_GET['nNF'];

//BUSCA CONFIGURACAO
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from configuracao where idConfiguracao ='1'";
$arrayParam = '';
$configuracao = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

//BUSCA EMITENTE
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where id ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

//BUSCA NOTA FISCAL
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');
$sql = "SELECT * from nfce where nNF ='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;
//
//$cNFt = strlen($dadosnfe->nNF);
$cNFt = strlen(mt_rand());

$cNF1 = '';

for ($i = $cNFt; $i < 8; $i++) {
    $cNF1 .= '0';
}

function AliqInterna($estado) {
    switch ($estado) :
        case 'RO':
            return '17';
            break;
        case 'AC':
            return '17';
            break;
        case 'AM':
            return '18';
            break;
        case 'RR':
            return '17';
            break;
        case 'PA':
            return '17';
            break;
        case 'AP':
            return '18';
            break;
        case 'TO':
            return '18';
            break;
        case 'MA':
            return '18';
            break;
        case 'PI':
            return '17';
            break;
        case 'CE':
            return '17';
            break;
        case 'RN':
            return '18';
            break;
        case 'PB':
            return '18';
            break;
        case 'PE':
            return '18';
            break;
        case 'AL':
            return '17';
            break;
        case 'SE':
            return '18';
            break;
        case 'BA':
            return '18';
            break;
        case 'MG':
            return '18';
            break;
        case 'ES':
            return '17';
            break;
        case 'RJ':
            return '19';
            break;
        case 'SP':
            return '18';
            break;
        case 'PR':
            return '18';
            break;
        case 'SC':
            return '17';
            break;
        case 'RS':
            return '18';
            break;
        case 'MS':
            return '17';
            break;
        case 'MT':
            return '17';
            break;
        case 'GO':
            return '17';
            break;
        case 'DF':
            return '18';
            break;
    endswitch;
}

function CodigoUF($estado) {
    switch ($estado) :
        case 'RO':
            return '11';
            break;
        case 'AC':
            return '12';
            break;
        case 'AM':
            return '13';
            break;
        case 'RR':
            return '14';
            break;
        case 'PA':
            return '15';
            break;
        case 'AP':
            return '16';
            break;
        case 'TO':
            return '17';
            break;
        case 'MA':
            return '21';
            break;
        case 'PI':
            return '22';
            break;
        case 'CE':
            return '23';
            break;
        case 'RN':
            return '24';
            break;
        case 'PB':
            return '25';
            break;
        case 'PE':
            return '26';
            break;
        case 'AL':
            return '27';
            break;
        case 'SE':
            return '28';
            break;
        case 'BA':
            return '29';
            break;
        case 'MG':
            return '31';
            break;
        case 'ES':
            return '32';
            break;
        case 'RJ':
            return '33';
            break;
        case 'SP':
            return '35';
            break;
        case 'PR':
            return '41';
            break;
        case 'SC':
            return '42';
            break;
        case 'RS':
            return '43';
            break;
        case 'MS':
            return '50';
            break;
        case 'MT':
            return '51';
            break;
        case 'GO':
            return '52';
            break;
        case 'DF':
            return '53';
            break;
    endswitch;
}

//Constroe a classe
use NFePHP\NFe\Make;
use NFePHP\Ibpt\Ibpt;

$tpAmb = $emitente->ambiente;

$nfe = new Make();
//-----------------
//Gera taginfN
$std = new stdClass();
$std->versao = '4.00'; //versão do layout
$std->Id = null; //se o Id de 44 digitos não for passado será gerado automaticamente
$std->pk_nItem = null; //deixe essa variavel sempre como NULL
$elem = $nfe->taginfNFe($std);
//-------------------
//gera tagid
$std = new stdClass();
$std->cUF = CodigoUF($emitente->uf);
$std->cNF = $cNF1 . $cNFt;
$std->natOp = removeAcentos($dadosnfe->natOp);
$std->indPag = null; //$dadosnfe->indPag; //NÃO EXISTE MAIS NA VERSÃO 4.00
$std->mod = '65';
$std->serie = $dadosnfe->serie;
$std->nNF = $dadosnfe->nNF;
//$datanf = $dadosnfe->dhEmi . " " . $dadosnfe->hEmi;
//$dataSnf = $dadosnfe->dhSaiEnt . " " . $dadosnfe->hSaiEnt;
$std->dhEmi = date("Y-m-d\TH:i:sP");
$std->dhSaiEnt = null;
$std->tpNF = 1;
if ($dadosnfe->destUF != '') :
    if ($emitente->uf != $dadosnfe->destUF):
        $std->idDest = '2'; //1=Operação interna; 2=Operação interestadual; 3=Operação com exterior.
    else :
        $std->idDest = '1'; //1=Operação interna; 2=Operação interestadual; 3=Operação com exterior.
    endif;

else :
    $std->idDest = '1';
endif;
$std->cMunFG = $emitente->cMun;
$std->tpImp = 4;
$std->tpEmis = $dadosnfe->tpEmis;
$std->cDV = null;
$std->tpAmb = $emitente->ambiente;
$std->finNFe = $dadosnfe->finNFe;
$destdocind = strlen($dadosnfe->destCNPJ);
if ($destdocind <= '11') :
    $std->indFinal = '1'; //0=Normal; 1=Consumidor final;
else :
    if ($dadosnfe->destIE == '') :
        $std->indFinal = '1'; //0=Normal; 1=Consumidor final;
    else :
        $std->indFinal = '0'; //0=Normal; 1=Consumidor final;
    endif;
endif;
$std->indPres = 1;
$std->procEmi = '0';
$std->verProc = '2020.1';
$std->dhCont = null;
$std->xJust = null;
$elem = $nfe->tagide($std);

//----------------
//infRespTec
$std = new stdClass();
$std->CNPJ = '26487592000101';
$std->xContato = 'Diego Fernando Hoffmann';
$std->email = 'resultatec@gmail.com';
$std->fone  = '48998235746';
$elem = $nfe->taginfRespTec($std);

//-------------------
//
//gera tagrefCTe referente a NFe referenciada

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfenfe');
$sql = "SELECT * from nfenfe where nNF = '$nNF'";
$arrayParam = '';
$nfeRef = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($nfeRef as $nfreferenciada) {
    $std = new stdClass();
    $std->refNFe = $nfe->tagrefNFe($std);
}
//-----------------------------
//gera tagrefECF referente a ECF referenciada
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'cfnfe');
$sql = "SELECT * from cfnfe where nNF = '$nNF'";
$arrayParam = '';
$cfRef = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($cfRef as $cfreferenciada) {
    $std = new stdClass();
    $std->mod = '2C';
    $std->nECF = $cfreferenciada->ncf;
    $std->nCOO = $cfreferenciada->ncoo;
    $elem = $nfe->tagrefECF($std);
}
//-----------------------------
//Gera tagemit
$std = new stdClass();
$std->xNome = $emitente->nome;
$std->xFant = $emitente->nomeFanstasia;
$std->IE = $emitente->ie;
$std->IEST = $emitente->inscst;
$std->IM = null;
$std->CNAE = null;
$std->CRT = $emitente->regimeTributario;
$std->CNPJ = $emitente->cnpj; //indicar apenas um CNPJ ou CPF
$elem = $nfe->tagemit($std);

//Gera tagenderEmit
//
//$std = new stdClass();
$std = new stdClass();
$std->xLgr = removeAcentos($emitente->rua);
$std->nro = removeAcentos($emitente->numero);
$std->xCpl = removeAcentos($emitente->complemento);
$std->xBairro = removeAcentos($emitente->bairro);
$std->cMun = $emitente->cMun;
$std->xMun = removeAcentos($emitente->cidade);
$std->UF = $emitente->uf;
$std->CEP = $emitente->cep;
$std->cPais = "1058";
$std->xPais = "BRASIL";

$telefone_emitente = $emitente->telefone;
$telefone_emitente = str_replace('(', '',$telefone_emitente);
$telefone_emitente = str_replace(')', '',$telefone_emitente);
$telefone_emitente = str_replace('-', '',$telefone_emitente);
$telefone_emitente = str_replace(' ', '',$telefone_emitente);

$std->fone = $telefone_emitente;
$elem = $nfe->tagenderEmit($std);
//-----------------------------
//
//gera function tagdest
$destdoc = strlen($dadosnfe->destCNPJ);
if ($dadosnfe->destCNPJ != '' && $dadosnfe->destCNPJ != '99999999999') :
    $std = new stdClass();
    if ($destdoc <= '11') :
        $std->CPF = $dadosnfe->destCNPJ;
        $std->IE = '';
        $std->indIEDest = '9';
    else :
        if ($dadosnfe->destIE == '') :
            $std->CNPJ = $dadosnfe->destCNPJ;
            $std->IE = '';
            $std->indIEDest = '9';
        else :
            $std->CNPJ = $dadosnfe->destCNPJ;
            $std->IE = $dadosnfe->destIE;
            $std->indIEDest = '1';
        endif;
    endif;
    $std->xNome = removeAcentos($dadosnfe->destxNome);
    $std->ISUF = null;
    $std->IM = null;
    $std->email = $dadosnfe->destemail;
    $std->idEstrangeiro = null;
    $elem = $nfe->tagdest($std);

$caracter = strlen($dadosnfe->destxLgr);
if($caracter > 2) {
    $std = new stdClass();
    $std->xLgr = removeAcentos($dadosnfe->destxLgr);
    $std->nro = removeAcentos($dadosnfe->destnro);
    $std->xCpl = removeAcentos($dadosnfe->destxCpl);
    $std->xBairro = removeAcentos($dadosnfe->destxBairro);
    $std->cMun = $dadosnfe->destcMun;
    $std->xMun = removeAcentos($dadosnfe->destxMun);
    $std->UF = removeAcentos($dadosnfe->destUF);
    $std->CEP = $dadosnfe->destCEP;
    $std->cPais = '1058';
    $std->xPais = 'BRASIL';
    $std->fone = soNumero($dadosnfe->destfone);

    $elem = $nfe->tagenderDest($std);
}
endif;

//----------
//
//function tagenderDest($std):DOMElement
//----------------
//-----------------------------
// Gera  tagprod
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'produtonfe');
$sql = "SELECT * from produtonfe where NFce ='$nNFbd'";
$arrayParam = '';
$dadosprod = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;
$item = '1';
$totalprod = count($dadosprod);
$totalitemdesc = count($dadosprod);
$tdesconto = 0;
$maxdesconto = 0;
$maxproduto = 0;
$contadorItem = 0;
$desconto = 0;
$vProd =0;

if ($dadosnfe->vDescnf != '0') :
    $proddesc = $dadosnfe->vDescnf / $totalprod;
    $desconto = $dadosnfe->vDescnf;
endif;

foreach ($dadosprod as $prod) :

    //print_r($prod);

    $std        = new stdClass();
    $std->item  = $item; //item da NFe
    $std->cProd = removeAcentos($prod->cProd);
    $std->cEAN  = 'SEM GTIN';

    if ($tpAmb == '2') :
        $std->xProd = 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
    else :
        $std->xProd = removeAcentos($prod->xProd);
    endif;

    $std->NCM = $prod->NCM;
    $std->cBenef = null; //incluido no layout 4.00
    $std->EXTIPI = $prod->EXTIPI;
    $std->CFOP = $prod->CFOP;
    $std->uCom = $prod->uCom;
    $std->qCom = $prod->qtd;
    $std->vUnCom = $prod->vProd;
    $std->vProd = $prod->valortotal;
    $std->cEANTrib = 'SEM GTIN';
    $std->uTrib = $prod->uCom;
    $std->qTrib = $prod->qtd;
    $std->vUnTrib = $prod->vProd;

    //frete
    if (isset($prodfrete)):
        if ($item == $totalprod) :
            $tfrete = $frete - $tfrete;
            $std->vFrete = number_format($tfrete, 2, '.', '');
        else :
            $vFrete = number_format($prodfrete, 2, '.', '');
            $std->vFrete = $tfrete + $vFrete;
        endif;
    else :
        $std->vFrete = null;
    endif;
    //--
    //seguro
    if (isset($prodseg)):
        $std->vSeg = number_format($prodseg, 2, '.', '');
    else :
        $std->vSeg = '';
    endif;
    //--
    //Desconto

    $totalDescontoNF = $desconto;
    $totalNota = $dadosnfe->vProdnf;
    $vProd = ($prod->vProd * $prod->qtd);

    $v = $totalDescontoNF * $vProd/$totalNota;
    $v = number_format($v, 2, '.', '');

    $maxdesconto = $maxdesconto+$v;
    $maxproduto = $maxproduto+ $vProd;

    if ($maxdesconto > $totalDescontoNF ) {
        $sobra = ($maxdesconto - $totalDescontoNF);
        $v =  $v - $sobra;
        //echo 'sobra'.$sobra.'<br/>';
        $maxdesconto = $maxdesconto - $sobra;
    } else if ($contadorItem >= count($dadosprod)) {
        $sobra = ($totalDescontoNF - $maxdesconto);
        //echo 'sobra'.$sobra.'<br/>';
        $v =  $v + $sobra;
        $maxdesconto = $maxdesconto + $sobra;
    }

    $contadorItem++;

    if (count($dadosprod) == $contadorItem) {
       // echo 'fim';
        if ($maxdesconto < $totalDescontoNF) {
            $sobra = ($maxdesconto - $totalDescontoNF);
            $v = $v - $sobra;
            $maxdesconto = $maxdesconto + $sobra;
        }
    }

    if ($v > 0) $std->vDesc = $v;


    //echo $totalDescontoNF.' - valor do produto = '.$vProd. ' valor do desconto= '.$v.' soma desconto = '.$maxdesconto.' soma do valor do produto '.$maxproduto.'<br/>';

/*
    $vProd =0;
    $vdesc2 = 0;

    if (isset($proddesc)):
        if ($item == $totalitemdesc) :
            $vDesca = $desconto - $tdesconto;
            $vDesc = number_format($vDesca, 2, '.', '');

            if ($vDesc > 0)   {
                $std->vDesc = $vDesc;
            } else {}
        else :
            $descontoitf = number_format($proddesc, 2, '.', '');
            if ($vProd < $descontoitf) :
                $vDesc = $vProd;
                $tdesconto = $tdesconto + $vDesc;

                if ($vDesc > 0)   {
                    $std->vDesc = $vDesc;
                } else {}
            else :
                if ($vProd > (($desconto - $tdesconto) / 3)) :
                    $vdesc2 = ($desconto - $tdesconto) / 3;
                    $vDesc = number_format($vdesc2, 2, '.', '');
                    $tdesconto = $tdesconto + $vDesc;

                    if ($vDesc > 0)   {
                        $std->vDesc = $vDesc;
                    } else {}

                else :
                    $vDesc = number_format($proddesc, 2, '.', '');
                    $tdesconto = $tdesconto + $vDesc;

                    if ($vDesc > 0)   {
                        $std->vDesc = $vDesc;
                    } else {}
                endif;
            endif;
        // echo $vDesc;
        endif;
    else :
        $std->vDesc = null;
    endif;
*/





    //--
    //Outros
    if ($prod->vOutro > 0) :
        $std->vOutro = number_format($prod->vOutro, 2, '.', '');
    else :
        $std->vOutro = null;
    endif;
    //--

    $std->indTot = 1;
    $std->xPed = $dadosnfe->nNF;
    $std->nItemPed = $item;
    $std->nFCI = $prod->nFCI;
    $elem = $nfe->tagprod($std);

    //Gera taginfAdProd
    if ($prod->infAdProd != '') :
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->infAdProd = removeAcentos($prod->infAdProd);
        $elem = $nfe->taginfAdProd($std);
    endif;
    //-----
//Gera CEST
    if ($prod->CEST != '') :
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->CEST = $prod->CEST;
        $std->indEscala = 'S'; //incluido no layout 4.00
        $std->CNPJFab = null; //incluido no layout 4.00
        $elem = $nfe->tagCEST($std);
    endif;
//-----
    //Gera tagveicProd
    if ($prod->chassi != '') :
        $std = new stdClass();
        $nItem = $item;
        $std->tpOp = $prod->tpOp;
        $std->chassi = $prod->chassi;
        $std->cCor = $prod->cCor;
        $std->xCor = $prod->xCor;
        $std->pot = $prod->pot;
        $std->cilin = $prod->cilin;
        $std->pesoL = $prod->pesoL;
        $std->pesoB = $prod->pesoB;
        $std->nSerie = $prod->nSerie;
        $std->tpComb = $prod->tpComb;
        $std->nMotor = $prod->nMotor;
        $std->cmt = $prod->CMT;
        $std->dist = $prod->dist;
        $std->anoMod = $prod->anoMod;
        $std->anoFab = $prod->anoFab;
        $std->tpPint = $prod->tpPint;
        $std->tpVeic = $prod->tpVeic;
        $std->espVeic = $prod->espVeic;
        $std->vIn = 'N';
        $std->condVeic = $prod->condVeic;
        $std->cMod = $prod->cMod;
        $std->cCorDENATRAN = $prod->cCorDENATRAN;
        $std->lota = $prod->lota;
        $std->tpRest = $prod->tpRest;
        $elem = $nfe->tagveicProd($std);
    endif;
    //-----
    //Gera tagcomb Combustivel
    if ($prod->cProdANP != '') :
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->cProdANP = $prod->cProdANP;

        $std->pMixGN = null; //removido no layout 4.00

        $std->descANP = $prod->descANP; //incluido no layout 4.00
        $std->pGLP = $prod->pGLP; //incluido no layout 4.00
        $std->pGNn = $prod->pGNn; //incluido no layout 4.00
        $std->pGNi = $prod->pGNi; //incluido no layout 4.00
        $std->vPart = $prod->vPart; //incluido no layout 4.00

        $std->CODIF = $prod->CODIF;
        $std->qTemp = $prod->qTemp;
        $std->UFCons = $prod->UFCons;
        $std->qBCProd = null; //$prod->qBCProd;
        $std->vAliqProd = null; //$prod->vAliqProd;
        $std->vCIDE = $prod->vCIDE;
        $elem = $nfe->tagcomb($std);
    endif;
    //----
    //
    //
    //
    //$std = newC stdClass();
    $std->item = $item; //item da NFe
    $std->vTotTrib = $prod->vTotTrib;//TODO ADICIONAR

    $elem = $nfe->tagimposto($std);
//    Gera ICMS
//    echo $prod->CST;
    if ($prod->CST < 100) :
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->orig = $prod->orig;
        $std->CST = $prod->CST;
        if ($prod->modBC == '') :
            $std->modBC = '0';
        else :
            $std->modBC = $prod->modBC;
        endif;
        $std->vBC = $prod->vBC;
        $std->pICMS = $prod->pICMS;
        $std->vICMS = $prod->vICMS;
        $std->pFCP = null;
        $std->vFCP = null;
        $std->vBCFCP = null;

        $std->modBCST = $prod->modBCST;
        $std->pMVAST = $prod->pMVAST;

        $std->pRedBCST = $prod->pRedBCST;

        $std->vBCST = $prod->vBCST;
        $std->pICMSST = $prod->pICMSST;
        $std->vICMSST = $prod->vICMSST;

        $std->vBCFCPST = null;
        $std->pFCPST = null;
        $std->vFCPST = null;
        $std->vICMSDeson = null;
        if ($prod->CST == '90') :
            $std->motDesICMS = '9';
        else :
            $std->motDesICMS = $prod->motDesICMS;
        endif;
        $std->pRedBC = $prod->pRedBC;
        $std->vICMSOp = $prod->vICMSOp;
        $std->pDif = $prod->pDif;
        $std->vICMSDif = $prod->vICMSDif;
        if ($prod->pST > 0) :
            $std->vBCSTRet = $prod->vBCSTRet;
            $std->pST = $prod->pST;
            $std->vICMSSTRet = $prod->vICMSSTRet;
        else:
            $std->vBCSTRet = null;
            $std->pST = null;
            $std->vICMSSTRet = null;
        endif;

        $std->vBCFCPSTRet = null;
        $std->pFCPSTRet = null;
        $std->vFCPSTRet = null;

        $elem = $nfe->tagICMS($std);
    //--
    //
          else :

//      function tagICMSSN($std):DOMElement
//Node referente Tributação ICMS pelo Simples Nacional do item da NFe

        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->orig = $prod->orig;
        $std->CSOSN = $prod->CST;
        $std->pCredSN = $prod->pCredSN;
        $std->vCredICMSSN = $prod->vCredICMSSN;
        $std->modBCST = $prod->modBCST;
        $std->pMVAST = $prod->pMVAST;
        $std->pRedBCST = $prod->pRedBCST; //AQUI
        $std->vBCST = $prod->vBCST;
        $std->pICMSST = $prod->pICMSST;
        $std->vICMSST = $prod->vICMSST;
        $std->vBCFCPST = null; //incluso no layout 4.00
        $std->pFCPST = null; //incluso no layout 4.00
        $std->vFCPST = null; //incluso no layout 4.00
        $std->pCredSN = $prod->pCredSN;
        $std->vCredICMSSN = $prod->vCredICMSSN;
         if ($prod->pST > 0) :
            $std->vBCSTRet = $prod->vBCSTRet;
            $std->pST = $prod->pST;
            $std->vICMSSTRet = $prod->vICMSSTRet;
        else:
            $std->vBCSTRet = null;
            $std->pST = null;
            $std->vICMSSTRet = null;
        endif;
        $std->vBCFCPSTRet = null; //incluso no layout 4.00
        $std->pFCPSTRet = null; //incluso no layout 4.00
        $std->vFCPSTRet = null; //incluso no layout 4.00
        if ($prod->modBC == '') :
            $std->modBC = '0';
        else :
            $std->modBC = $prod->modBC;
        endif;
        $std->vBC = $prod->vBC;
        $std->pRedBC = $prod->pRedBC;
        $std->pICMS = $prod->pICMS;
        $std->vICMS = $prod->vICMS;

        $elem = $nfe->tagICMSSN($std);
    //------
    endif;

//    print_R($elem);
    //Gera tagICMSPart
    //Node com informações da partilha do ICMS entre a UF de origem e UF de destino ou a UF definida na legislação.
//    $destdoc = strlen($dadosnfe->destCNPJ);
//    if ($destdoc <= '11') :
//        if ($dadosnfe->destUF != $emitente->UF) :
//            $std = new stdClass();
//            $std->item = $item; //item da NFe
//            $std->orig = 0;
//            $std->CST = $prod->CST;
//            if ($prod->modBC == '') :
//                $std->modBC = '0';
//            else :
//                $std->modBC = $prod->modBC;
//            endif;
//            $std->vBC = $prod->vBC;
//            $std->pRedBC = $prod->pRedBC;
//            $std->pICMS = $prod->pICMS;
//            $std->vICMS = $prod->vICMS;
//            $std->modBCST = $prod->modBCST;
//            $std->pMVAST = $prod->pMVAST;
//            $std->pRedBCST = null;
//            $std->vBCST = $prod->vBCST;
//            $std->pICMSST = $prod->pICMSST;
//            $std->vICMSST = $prod->vICMSST;
//            $std->pBCOp = $prod->pBCOp;
//            $std->UFST = $emitente->UF;
//
//            $elem = $nfe->tagICMSPart($std);
//        endif;
//    endif;
//------
//
//GERA function tagICMSST($std):DOMElement
//Node Repasse de ICMS ST retido anteriormente em operações interestaduais com repasses através do Substituto Tributário
//    if ($prod->vICMSSTRet > 0) :
//        $std = new stdClass();
//        $std->item = $item; //item da NFe
//        $std->orig = $prod->orig;
//        $std->CST = $prod->CST;
//        $std->vBCSTRet = $prod->vBCSTRet;
//        $std->vICMSSTRet = $prod->vICMSSTRet;
//        $std->vBCSTDest = $prod->vBCSTDest;
//        $std->vICMSSTDest = $prod->vICMSSTDest;
//
//        $elem = $nfe->tagICMSST($std);
//    endif;
//------
//
//    function tagICMSUFDest($std):DOMElement
//Node de informação do ICMS Interestadual do item na NFe
//    if ($destdoc <= '11') :
//        if ($dadosnfe->destUF != $emitente->UF) :
//            $ufdest = BuscaAliquota($dadosnfe->destUF, $emitente->UF);
//            $std = new stdClass();
//            $std->item = $item; //item da NFe
//            $std->vBCUFDest = $prod->valortotal;
//            $std->vBCFCPUFDest = null;
//            $std->pFCPUFDest = 0;
//            $std->pICMSUFDest = AliqInterna($emitente->UF);
//            $std->pICMSInter = $ufdest->icms;
//            $std->pICMSInterPart = ICMSInter();
//            $std->vFCPUFDest = '0.00';
//            $std->vICMSUFDest = $prod->valortotal * (ICMSInter() / 1000);
//            $std->vICMSUFRemet = $prod->valortotal * (ICMSInterOr() / 1000);
//            $elem = $nfe->tagICMSUFDest($std);
//        endif;
//    endif;
//------
//    function tagIPI($std):DOMElement
//Node referente ao IPI do item da NFe
    if ($prod->CSTIPI != '') :
        $std = new stdClass();
        $std->item = $item; //item da NFe
        $std->clEnq = $prod->clEnq;
        $std->CNPJProd = $prod->CNPJProd;
        $std->cSelo = $prod->cSelo;
        $std->qSelo = $prod->qSelo;
        $std->cEnq = $prod->cEnq;
        $std->CST = $prod->CSTIPI;
        $std->vIPI = $prod->vIPI;
        $std->vBC = $prod->vBCIPI;
        $std->pIPI = $prod->pIPI;
        $std->qUnid = null;
        $std->vUnid = null;

        $elem = $nfe->tagIPI($std);
    endif;
    //-------
//    function tagPIS($std):DOMElement
//Node PIS do item da NFe
    $std = new stdClass();
    $std->item = $item; //item da NFe
    if ($prod->CSTPIS == '') :
        $std->CST = '99'; //Operação Tributável (base de cálculo = quantidade vendida x alíquota por unidade de produto)
    else :
        $std->CST = $prod->CSTPIS; //Operação Tributável (base de cálculo = quantidade vendida x alíquota por unidade de produto)
    endif;
    $std->vBC   = $prod->vBCPIS;
    $std->pPIS  = $prod->pPIS;
    $std->vPIS  = $prod->vPIS;
    $std->qBCProd = null; //$prod->qBCProdPIS;
    $std->vAliqProd = null; //$prod->vAliqProdPIS;

    $elem = $nfe->tagPIS($std);
//-----
//
// function tagPISST($std):DOMElement
//Node PIS Substituição Tributária do item da NFe
//    $std = new stdClass();
//$std->item = 1; //item da NFe
//$std->vPIS =  16.00;
//$std->vBC = 1000.00
//$std->pPIS = 1.60;
//$std->qBCProd = null;
//$std->vAliqProd = null;
//
//$elem = $nfe->tagPISST($std);
    //---------
    //
    //
//    function tagCOFINS($std):DOMElement
//Node COFINS do item da NFe
    $std = new stdClass();
    $std->item = $item; //item da NFe
    if ($prod->CSTCOFINS == '') :
        $std->CST = '99';
    else :
        $std->CST = $prod->CSTCOFINS;
    endif;

    $std->vBC = $prod->vBCCOFINS;
    $std->pCOFINS = $prod->pCOFINS;
    $std->vCOFINS = $prod->vCOFINS;
    $std->qBCProd = null; //$prod->qBCProdCOFINS;
    $std->vAliqProd = null; //$prod->vAliqProdCOFINS;

    $elem = $nfe->tagCOFINS($std);
    //---------
    //

    $item++;
endforeach;


//    function tagtransp($std):DOMElement
//Node indicativo da forma de frete
$std = new stdClass();
$std->modFrete = 9;
$elem = $nfe->tagtransp($std);
//----
//
//    function tagtransporta($std):DOMElement
//Node com os dados da tranportadora

if ($dadosnfe->transpxNome != '') :
    $std = new stdClass();
    $std->xNome = removeAcentos($dadosnfe->transpxNome);
    if ($dadosnfe->transpCNPJ <= '11') :
        $std->CPF = $dadosnfe->transpCNPJ;
        $std->IE = '';
    else :
        $std->CNPJ = $dadosnfe->transpCNPJ;
        $std->IE = $dadosnfe->transpIE;
    endif;
    $std->xEnder = removeAcentos($dadosnfe->transpxLgr) . " " . removeAcentos($dadosnfe->transpnro) . "  " . removeAcentos($dadosnfe->transpxCpl) . "  " . removeAcentos($dadosnfe->transpxBairro);
    ;
    $std->xMun = removeAcentos($dadosnfe->transpxMun);
    $std->UF = removeAcentos($dadosnfe->transpUF);
    $elem = $nfe->tagtransporta($std);
endif;
//--------------------------
//function tagveicTransp($std):DOMElement
//Node para informação do veículo trator


if ($dadosnfe->qVol >0) {
    $std = new stdClass();
    $std->item = 1; //indicativo do numero do volume
    $std->qVol = $dadosnfe->qVol;
    $std->esp = $dadosnfe->esp;
    $std->marca = $dadosnfe->marca;
    $std->nVol = $dadosnfe->nVol;
    $std->pesoL = null; //$dadosnfe->pesoL;
    $std->pesoB = null; //$dadosnfe->pesoB;
    $elem = $nfe->tagvol($std);
}


$std = new stdClass();

if ($dadosnfe->vTroco > 0) $std->vTroco =$dadosnfe->vTroco; //incluso no layout 4.00, obrigatório informar para NFCe (65)
else $std->vTroco = NULL;

$elem = $nfe->tagpag($std);


if ($dadosnfe->nFatcob != '') :
    $std = new stdClass();
    $std->nFat = $dadosnfe->nFatcob;
    $std->vOrig = $dadosnfe->vOrigcob;
    $std->vDesc = null;
    $std->vLiq = $dadosnfe->vLiqcob;
    $elem = $nfe->tagfat($std);
endif;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');

$sql = "SELECT * from duplicatanfe where nfe ='$nNFbd'";
$arrayParam = '';
$dadosduplicata = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

if (count($dadosduplicata) > 0) :
    foreach ($dadosduplicata as $dup) {
        $std = new stdClass();
        $std->nDup = $dup->duplicata;
        $std->dVenc = parseDate($dup->vencimento, "Y-m-d");
        $std->vDup = $dup->valor;
        $elem = $nfe->tagdup($std);
    }
endif;


/*
 *  01=Dinheiro
    02=Cheque
    03=Cartão de Crédito
    04=Cartão de Débito
    05=Crédito Loja
    10=Vale Alimentação
    11=Vale Refeição
    12=Vale Presente
    13=Vale Combustível
    99=Outros
 */


//pagamento em dinheiro
if ($dadosnfe->dinheiro > 0) {

    $std = new stdClass();
    $std->tPag = '01';//DINHEIRO
    $std->vPag = $dadosnfe->dinheiro;

    if ($dadosnfe->cAut != '') :
        $std->CNPJ = '00000000000000';
        $std->tBand = $dadosnfe->tBand;
        $std->cAut = $dadosnfe->cAut;
        $std->tpIntegra = 1; //incluso na NT 2015/002
    else :
        $std->CNPJ = null;
        $std->tBand = null;
        $std->cAut = null;
        $std->tpIntegra = null; //incluso na NT 2015/002
    endif;

    $std->indPag = "0"; //0= Pagamento à Vista 1= Pagamento à Prazo
    $elem = $nfe->tagdetPag($std);
}

//pagamento com cartao de debito
if ($dadosnfe->cartao_debito > 0) {

    $std = new stdClass();
    $std->tPag = '04';//CARTAO DE DEBITO
    $std->vPag = $dadosnfe->cartao_debito;

    $std->CNPJ = $configuracao->CNPJCredenciadoraCartao;
    $std->tBand = $configuracao->tBand;

    $std->cAut = $nNF;
    $std->tpIntegra = 1; //incluso na NT 2015/002

    $std->indPag = "0"; //0= Pagamento à Vista 1= Pagamento à Prazo
    $elem = $nfe->tagdetPag($std);
}

/*
 *
 * 01 - Visa
 * 02 - Mastercard
 * 03 - American Express
 * 04 - Sorocred
 * 99 - Outros
 */

//pagamento com cartao de credito
if ($dadosnfe->cartao_credito > 0) {

    $std = new stdClass();
    $std->tPag = '03';//CARTAO DE CREDITO
    $std->vPag = $dadosnfe->cartao_credito;

    $std->CNPJ = $configuracao->CNPJCredenciadoraCartao;
    $std->tBand = $configuracao->tBand;

    $std->cAut = $nNF;
    $std->tpIntegra = 1; //incluso na NT 2015/002

    $std->indPag = "0"; //0= Pagamento à Vista 1= Pagamento à Prazo
    $elem = $nfe->tagdetPag($std);
}

//pagamento parcelado
if ($dadosnfe->valorParcelado > 0) {

    $std = new stdClass();
    $std->tPag = '05';//CREDITO EM LOJA
    $std->vPag = $dadosnfe->valorParcelado;

    $std->CNPJ = null;
    $std->tBand = null;
    $std->cAut = null;
    $std->tpIntegra = null; //incluso na NT 2015/002

    $std->indPag = "1"; //0= Pagamento à Vista 1= Pagamento à Prazo
    $elem = $nfe->tagdetPag($std);
}

if ($dadosnfe->infCpl) {
    $std = new stdClass();
    $std->infAdFisco = '';
    $std->infCpl = $dadosnfe->infCpl;
    $elem = $nfe->taginfAdic($std);
}

$std = new stdClass();
$std->vBC = $dadosnfe->vBCnf;
$std->vICMS = $dadosnfe->vICMSnf;
$std->vICMSDeson = 0;
$std->vFCP = 0; //incluso no layout 4.00
$std->vBCST = $dadosnfe->vBCSTnf;
$std->vST = $dadosnfe->vSTnf;
$std->vFCPST = 0; //incluso no layout 4.00
$std->vFCPSTRet = 0; //incluso no layout 4.00
$std->vProd = $dadosnfe->vProdnf;
$std->vFrete = $dadosnfe->vFretenf;
$std->vSeg = $dadosnfe->vSegnf;
$std->vDesc = $dadosnfe->vDescnf;
$std->vII = $dadosnfe->vIInf;
$std->vIPI = $dadosnfe->vIPInf;
$std->vIPIDevol = 0; //incluso no layout 4.00
$std->vPIS = $dadosnfe->vPISnf;
$std->vCOFINS = $dadosnfe->vCOFINSnf;
$std->vOutro = $dadosnfe->vOutronf;
$std->vNF = $dadosnfe->vNF;
$std->vTotTrib = 0;//TODO AQUIIII

$elem = $nfe->tagICMSTot($std);


//echo '<pre>';
//print_r($prod);
//echo '</pre>';


$resp = $nfe->montaNFe();
$xml = $nfe->getXML();
$chave = $nfe->getChave();


$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'nfe');
$pdo->beginTransaction();
$retorno = $crudup->Sql("UPDATE nfce set chavenfe = {$chave} where nNF = {$nNFbd}");
$pdo->commit();
$pdo = null;
$curd = null;
//--------------------------


if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

//$resp = $nfe->montaNFe();
if ($resp) {
//    header('Content-type: text/xml; charset=UTF-8');
// $filename = "/var/www/nfe/homologacao/entradas/{$chave}-nfe.xml"; // Ambiente Linux
    $filename = "../XML/{$_SESSION['BD']}/NF-e/{$pastaxml}/entradas/{$chave}-nfce.xml"; // Ambiente Windows
    file_put_contents($filename, $xml);
    chmod($filename, 0777);
//    echo $xml;
}
if (!$resp) {
    $arr = array("Status" => 'Erro');
} else {
    $arr = array("Status" => 'Ok');
}

echo json_encode($arr);

//echo $chave;
