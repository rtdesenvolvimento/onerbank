<?php

//entradas copy (ano_mes_dia)
$ano_mes_dia = date("Ymd");
$hora = date("hi");

if (!isset($_SESSION)):
    session_start();
endif;

if (!isset($_SESSION['BD'])){
    $BANCO = $_GET['BD'];
} else {
    $BANCO =  $_SESSION['BD'];
}



$nome_arquivo_dowload  = $BANCO.'_'.$ano_mes_dia.$hora;

header('Cache-control: private');
header('Content-Type: application/octet-stream');
header('Content-Disposition: filename=ARQUIVOS_'.$nome_arquivo_dowload.'.zip"');
header('Content-Disposition: attachment; filename=ARQUIVOS_'.$nome_arquivo_dowload.'.zip');

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

/*
error_reporting(E_ALL);
ini_set('display_errors', 'On');
*/

define('DIR',dirname(dirname(dirname(__FILE__))));

$tpAmb = $emitente->ambiente;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;


if (isset($_GET['arquivo'])) :
    $nNFbd = $_GET['arquivo'];

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'nfe');

    $sql = "SELECT recibo,chavenfe,dhEmi,status status from nfce where nNF='$nNFbd'";
    $arrayParam = '';
    $dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

    $pasta = parseDate($dadosnfe->dhEmi, "Ym");

    if ($dadosnfe->status == '101') :
        $local = "../XML/{$emitente->cnpj}/NF-e/{$pastaxml}/canceladas/{$pasta}/";
    else :
        $local = "../XML/{$emitente->cnpj}/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/";
    endif;

    $arquivo = '' . $dadosnfe->chavenfe . '-nfce.xml'; // Nome do Arquivo
    $local_arquivio = $local . $arquivo; // Concatena o diretório com o nome do arquivo

else :
    $zip = new ZipArchive();

//    if ($zip->open('arquivo.zip', ZipArchive::OVERWRITE)) {
//        foreach (glob('./pasta/desejada/*.*') as $current)
//            $zip->addFile($current, basename($current));
//
//        $zip->close();
//    }
//echo DIR;
    if ($zip->open("".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/{$_GET['periodo']}.zip", ZipArchive::CREATE) === true) {

        foreach (glob("".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/enviadas/aprovadas/{$_GET['periodo']}/*.*") as $current)
            $zip->addFile($current, 'Aprovadas/'.basename($current));
        
        foreach (glob("".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/pdf/{$_GET['periodo']}/*.*") as $current)
            $zip->addFile($current, 'PDF/'.basename($current));
        
        foreach (glob("".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/canceladas/{$_GET['periodo']}/*.*") as $current)
            $zip->addFile($current, 'Canceladas/'.basename($current));
        $zip->close();
    }

    $arquivo = "".DIR."/nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/{$_GET['periodo']}.zip"; // Nome do Arquivo

    $local_arquivo = $arquivo; // Concatena o diretório com o nome do arquivo

endif;

if (stripos($arquivo, './') !== false || stripos($arquivo, '../') !== false || !file_exists($local_arquivo)) {
    echo 'O comando não pode ser executado.';
} else {

    readfile($local_arquivo);
}
?>