<?php

namespace NFePHP\install;

define('DIR',dirname(dirname(dirname(__FILE__))));

//require_once('' . DIR . '/nfephp-master/bootstrap.php');


error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once '../bootstrap.php';
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';

require_once '../bootstrap.php';


if (!defined('PATH_NFEPHP')) {
    define('PATH_NFEPHP', dirname(dirname(__FILE__)));
}


$pdo = \Conexao::getInstance();
$crud = \Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente WHERE id = 1";
$arrayParam = '';
$dados = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$defaultconfigfolder = PATH_NFEPHP . '/config/' . $dados->cnpj . '';

$siteUrl = str_replace('index.php', '', 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["SCRIPT_NAME"]);

$aDocFormat = array(
    'format' => 'P',
    'paper' => 'A4',
    'southpaw' => '1',
    'pathLogoFile' => "" . DIR . "/_frontend/img/" . $dados->cnpj . "/" . $dados->url_logo . "",
    'pathLogoNFe' => "" . DIR . "/_frontend/img/" . $dados->cnpj . "/" . $dados->url_logo . "",
    'pathLogoNFCe' => "" . DIR . "/_frontend/img/" . $dados->cnpj . "/" . $dados->url_logo . "",
    'logoPosition' => 'L',
    'font' => "Times",
    'printer' => ""
);

$aMailConf = array(
    "mailAuth" => "1",
    "mailFrom" => "mail.novoemissorfiscal.com.br",
    "mailSmtp" => "mail.novoemissorfiscal.com.br",
    "mailUser" => "nfe@novoemissorfiscal.com.br",
    "mailPass" => "Sistemanfe.1",
    "mailProtocol" => "SSL",
    "mailPort" => "465",
    "mailFromMail" => "nfe@novoemissorfiscal.com.br",
    "mailFromName" => $dados->nome,
    "mailReplayToMail" => false,
    "mailReplayToName" => "",
    "mailImapHost" => null,
    "mailImapPort" => null,
    "mailImapSecurity" => null,
    "mailImapNocerts" => null,
    "mailImapBox" => null
);

$aProxyConf = array(
    "proxyIp" => "",
    "proxyPort" => "",
    "proxyUser" => "",
    "proxyPass" => ""
);

$aConfig = array(
    'atualizacao' => date('Y-m-d h:i:s'),
    'tpAmb' => $dados->ambiente,
    'pathXmlUrlFileNFe' => "wsnfe_4.00_mod55.xml",
    'pathXmlUrlFileCTe' => "cte_ws3.xml",
    'pathXmlUrlFileMDFe' => "mdfe_ws3.xml",
    'pathXmlUrlFileCLe' => "",
    'pathXmlUrlFileNFSe' => "",
    'pathNFeFiles' => "" . DIR . "/nfephp-master/XML/" . $dados->cnpj . "/NF-e",
    'pathCTeFiles' => "" . DIR . "/sped-cte/XML/" . $dados->cnpj . "/CTe",
    'pathMDFeFiles' => "" . DIR . "/sped-mdfe/XML/" . $dados->cnpj . "/MDFe",
    'pathCLeFiles' => "" . DIR . "/nfephp-master/XML/" . $dados->cnpj . "/CL-e",
    'pathNFSeFiles' => "" . DIR . "/nfephp-master/XML/" . $dados->cnpj . "/NFS-e",
    'pathCertsFiles' => "" . DIR . "/nfephp-master/certs/" . $dados->cnpj . "/",
    'siteUrl' => $siteUrl,
    "schemesNFe" => "PL_009_V4",
    "schemesCTe" => "PL_CTe_300",
    "schemesMDFe" => "PL_MDFe_300",
    "schemesCLe" => "",
    "schemesNFSe" => "",
    'razaosocial' => $dados->nome,
    'nomefantasia' => $dados->nomeFanstasia,
    'siglaUF' => $dados->uf,
    'cnpj' => $dados->cnpj,
    'ie' => $dados->ie,
    'im' => $dados->insmun,
    'iest' => $dados->inscst,
    'cnae' => $dados->cnae,
    'regime' => $dados->regimeTributario,
    'tokenIBPT' => "XgLl8SpXM-2jUbT4USlGXfwK_WlLFVXP9v5W2ZaD_F9f3bXLV2lpjOmANwRtKR6u",
    'tokenNFCe' => $dados->csc,
    'tokenNFCeId' => $dados->idcsc,
    'certPfxName' => $dados->cert,
    'certPassword' => $dados->certsenha,
    'certPhrase' => "",
    'aDocFormat' => $aDocFormat,
    'aMailConf' => $aMailConf,
    'aProxyConf' => $aProxyConf
);

$content = json_encode($aConfig);

$filename =   "../../nfephp-master/config/$dados->cnpj/config.json"; // Ambiente Windows




file_put_contents($filename, $content);

//echo $filename;

//header('Content-type: text/xml; charset=UTF-8');
//
//$xml = "<root>
//        <status>OK</status>
//        <motivo> Dados de Configuração alterados com sucesso! </motivo>
//        </root>";
//echo $xml;
//
//$msg = 'Dados de Configuração alterados com sucesso!.';

//$aResp = array('status' => ($resdefault && $res), 'msg' => $msg);

//print json_encode($aResp);
