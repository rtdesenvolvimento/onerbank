﻿<?php


use NFePHP\Common\Signer;
use NFePHP\Common\Certificate;


require_once "../../../_backend/_class/Conexao_class.php";
require_once "../../../_backend/_class/Crud_class.php";

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$json = file_get_contents('../../config/' . $_SESSION['BD'] . '/config.json');
$json = json_decode($json, true);


$nNFbd = $_GET['arquivo'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$chave = $dadosnfe->chavenfe;

$tpAmb = '2';
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$xml = "{$json['aConfig']['pathNFeFiles']}/{$pastaxml}/entradas/{$chave}-nfe.xml"; // Ambiente Windows

print_r($json);

//$xml = "<aqui fica sua string XML que deverá ser assinada>";

$tagname = 'infNFe'; //tag a ser assinada, 
                     //se este campo for deixado vazio a tag raiz será assinada 

$mark = 'Id'; //indica se a assinatura fará referencia a uma tag 
              //com atributo de identificação definido,
              //se for assinar a raiz do documento este campo deverá 
              //ser deixado em branco 

$algorithm = OPENSSL_ALGO_SHA1; //algoritimo de encriptação a ser usado

$canonical = [true,false,null,null]; //veja função C14n do PHP

$rootname = ''; //este campo indica em qual node a assinatura deverá ser inclusa

try {

    $pfx = file_get_contents('../../certs/' . $_SESSION['BD'] . '/'.$json['certPfxName'].'');
    $certificate = Certificate::readPfx($pfx, 'Pinheiro123');
//
    $signed = Signer::sign(
        $certificate,
        $xml,
        $tagname,
        $mark,
        $algorithm,
        $canonical,
        $rootname
    );
    //$signed contêm o XML assinado
    
    header('Content-type: text/xml; charset=UTF-8');
    echo $signed;

} catch (\Exception $e) {
    //aqui você trata a exceção
    echo $e->getMessage();
}
?>