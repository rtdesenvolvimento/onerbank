<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once '../bootstrap.php';
use NFePHP\CTe\Tools;


$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$aResposta = array();

$nctbd = $_GET['arquivo'];

$tpAmb = $cteTools ->aConfig['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'cte');
$sql = "SELECT dhemi,chavecte,recibo from ctebdos where nct='$nctbd'";
$arrayParam = '';
$dadoscte = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$pasta = parseDate($dadoscte->dhemi, "Ym");

$indSinc = '0'; //0=asíncrono, 1=síncrono
$chave = $dadoscte->chavecte;
$recibo = $dadoscte->recibo;

$pathProtfile = "{$cteTools ->aConfig['pathCTeFiles']}/{$pastaxml}/temporarias/{$pasta}/{$chave}-CancCTe-retEnvEvento.xml";
$pathNFefile = "{$cteTools ->aConfig['pathCTeFiles']}/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-protCTe.xml";
//$pathProtfile = '/var/www/cte/homologacao/temporarias/201501/35150158716523000119550010000000071000000076-CancNFe-retEnvEvento.xml';
$saveFile = true;
$retorno = $cteTools->addCancelamento($pathNFefile, $pathProtfile, $saveFile);

$arr = array("Status" => 'Ok');
echo json_encode($arr);

//echo '<br><br><PRE>';
//echo htmlspecialchars($retorno);
//echo '</PRE><BR>';
//echo "<br>";
