<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
require_once '../bootstrap.php';

use NFePHP\CTe\Tools;

$nctbd = $_GET['nct'];

$xJust = $_GET['justificativa'];


$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'cte');
$sql = "SELECT dhEmi,chavecte,recibo,nProt from ctebd where nct='$nctbd'";
$arrayParam = '';
$dadoscte = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$chave = $dadoscte->chavecte;
$nProt = $dadoscte->nProt;
//$dadoscte->nProt;

$aResposta = array();
//echo $chave;

$tpAmb = $cteTools->aConfig['tpAmb'];

//echo $tpAmb;

$retorno = $cteTools->sefazCancela($chave, $tpAmb, $xJust, $nProt, $aResposta);
//echo '<pre>';
////echo htmlspecialchars($cteTools->soapDebug);
//print_r($aResposta);
//echo "</pre>";

if ($aResposta[cStat] != '135') :
    $retorno = -1;
    $msg = $aResposta[xMotivo];
else :
    $pdo = Conexao::getInstance();
    $crudup = Crud::getInstance($pdo, 'cte');
    $pdo->beginTransaction();
    $retorno = $crudup->Sql("UPDATE ctebd set status='101' where nct='$nctbd'");
    $pdo->commit();
    $pdo = null;
    $curd = null;

    $retorno = true;
    $msg = "Conhecimento de Transporte Cancelado com Sucesso!";
endif;

$retcan = array("retorno" => $retorno, "mensagem" => $msg, "nct" => $nctbd);

//echo $aResposta[evento][0][cStat];
//echo $aResposta[evento][0][xMotivo];

echo json_encode($retcan);
//
//
//echo '<br><br><PRE>';
//echo htmlspecialchars($cteTools->soapDebug);
//echo '</PRE><BR>';
//echo json_encode($aResposta);
//echo "<br>";