<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

$nctbd = $_GET['nct'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'cte');
$sql = "SELECT dhEmi,chavecte,recibo,nProt from ctebd where nct='$nctbd'";
$arrayParam = '';
$dadoscte = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$chave = $dadoscte->chavecte;
$nProt = $dadoscte->nProt;

error_reporting(E_ALL);
ini_set('display_errors', 'On');
include_once '../bootstrap.php';

use NFePHP\CTe\Tools;

$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$aResposta = array();


$siglaUF = $cteTools->aConfig['siglaUF'];
// Identificação do Ambiente: 1 – Produção 2 – Homologação
$tpAmb = $cteTools->aConfig['tpAmb'];
// Informar o CNPJ do autor do Evento
$cnpj = $cteTools->aConfig['cnpj'];
// Chave de Acesso do CT-e vinculado ao Evento

$nSeqEvento = $_GET['item'];


$infCorrecao[] = array(
    "grupoAlterado" => $_GET['grupo'], // Indicar o grupo de informações que pertence
    "campoAlterado" => $_GET['campo'], // Nome do campo modificado do CT-e Original.
    "valorAlterado" => $_GET['info'], // Valor correspondente à alteração.
    "nroItemAlterado" => $_GET['item']// Preencher com o indice do item alterado caso a alteração ocorra em uma lista.
);

$cteTools->sefazCartaCorrecao(
        $chave, $tpAmb, $nSeqEvento, $infCorrecao, $aResposta
);

if ($aResposta['cStat'] != '135') :
    $retorno = -1;
    $msg = $aResposta['xMotivo'];
else :
    $retorno = true;
    $msg = "Carta de Correção Criada com Sucesso!";
endif;


$retcce = array("retorno" => $retorno, "mensagem" => $msg);

echo json_encode($retcce);

//echo '<pre>';
////echo htmlspecialchars($cteTools->soapDebug);
//print_r($aResposta);
//echo "</pre>";