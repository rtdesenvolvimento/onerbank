<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

$nctbd = $_GET['nct'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'cte');
$sql = "SELECT dhEmi,chavecte,recibo,nProt from ctebd where nct='$nctbd'";
$arrayParam = '';
$dadoscte = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$chave = $dadoscte->chavecte;
$nProt = $dadoscte->nProt;

error_reporting(E_ALL);
ini_set('display_errors', 'On');
include_once '../bootstrap.php';
use NFePHP\CTe\Tools;

$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$aResposta = array();


$siglaUF = $cteTools->aConfig['siglaUF'];
// Identificação do Ambiente: 1 – Produção 2 – Homologação
$tpAmb = $cteTools->aConfig['tpAmb'];
// Informar o CNPJ do autor do Evento
$cnpj = $cteTools->aConfig['cnpj'];
// Chave de Acesso do CT-e vinculado ao Evento

$nSeqEvento = $_GET['item'];
       
// Indicar o grupo de informações que pertence
// o campoAlterado. Ex: ide
$grupoAlterado = $_GET['grupo'];
// Nome do campo modificado do CT-e Original.
// Ex: natOp
$campoAlterado = $_GET['campo'];
// Valor correspondente à alteração.
$valorAlterado = $_GET['info'];
// Preencher com o indice do item alterado caso a alteração ocorra em uma lista.
// Por exemplo: Se corrigir uma das NF-e do remetente, 
// esta tag deverá indicar a posição da NF-e alterada na lista.
// OBS: O indice inicia sempre em 01
$nroItemAlterado= $_GET['item'];
// Transmite o arquivo

$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'notafiscala3');
$pdo->beginTransaction();
$retorno = $crudup->Sql("UPDATE notafiscala3 set situacao = 'N'");
$pdo->commit();
$pdo = null;
$curd = null;


$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'notafiscala3');
$pdo->beginTransaction();
$sql = "INSERT INTO notafiscala3(chavenfe,tipo, situacao,cce, xJust,campo, item, grupo) 
    VALUES ('$chave', 'cte', 'S', 'S', '$valorAlterado ', '$campoAlterado', '$nroItemAlterado', '$grupoAlterado')";
$retorno = $crudup->Sql($sql);
$pdo->commit();
$pdo = null;
$curd = null;
echo $sql;

$retcan = array("retorno" => true);
echo json_encode($retcan);

//echo '<pre>';
////echo htmlspecialchars($cteTools->soapDebug);
//print_r($aResposta);
//echo "</pre>";