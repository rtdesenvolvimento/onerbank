<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
require_once '../bootstrap.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

//NOTA: o envio de email com o DANFE somente funciona para modelo 55
//      o modelo 65 nunca requer o envio do DANFCE por email
use NFePHP\CTe\Tools;

$nfe = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');
$nNFbd = $_GET['arquivo'];

$tpAmb = $nfe->aConfig['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'ctebd');

$sql = "SELECT dhemi,chavecte,recibo from ctebd where nct='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pasta = parseDate($dadosnfe->dhemi, "Ym");

$chave = $dadosnfe->chavecte;


$pathXml = "{$nfe->aConfig['pathCTeFiles']}/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-protCTe.xml";
//$pathPdf = "{$nfe->aConfig['pathCTeFiles']}/{$pastaxml}/pdf/{$pasta}/{$chave}-danfe.pdf";
$pathPdf = "";
//
//echo $pathPdf;

$aMails = array($_GET['email']); //se for um array vazio a classe Mail irá pegar os emails do xml
$templateFile = ''; //se vazio usará o template padrão da mensagem
$comPdf = true; //se true, anexa a DANFE no e-mail
try {
    $nfe->enviaMail($pathXml, $aMails, $templateFile, $comPdf, $pathPdf, $chave);
    $msg = "DACTE enviada com sucesso!!!";
} catch (NFePHP\Common\Exception\RuntimeException $e) {
    $msg = $e->getMessage();
}

$array = array("status" => "OK" , "msg" => $msg);

echo json_encode($array);