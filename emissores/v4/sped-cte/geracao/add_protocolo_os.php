<?php

if (!isset($_SESSION)):
    session_start();
endif;

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'ctebdos');
$sql = "SELECT * from ctebdos where nct ='" . $_GET['arquivo'] . "'";
$arrayParam = '';
$ctebdos = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

include_once '../bootstrap.php';

use NFePHP\CTe\Tools;

$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $cteTools->aConfig['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;




//$doccte = new Dom();
$aResposta = array();
$indSinc = '0'; //0=asíncrono, 1=síncrono
$chave = $ctebdos->chavecte;
//$recibo = $ctebdos->recibo;
//$recibo = $_GET['recibo'];

//echo $recibo;

$pathCTeFile = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/entradas/{$chave}-cte.xml";

//echo $pathCTeFile;
$pasta = date("Ym");
//echo $pasta;
if (!$indSinc) {
    $pathProtfile = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/temporarias/$pasta/{$chave}-retConsSitCTe.xml";
} else {
    $pathProtfile = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/temporarias/$pasta/{$chave}-consSitCTe.xml";
}
//echo $pathProtfile;
$saveFile = true;
$retorno = $cteTools->addProtocolo($pathCTeFile, $pathProtfile, $saveFile);
//echo '<br><br><pre>';
//echo htmlspecialchars($retorno);
//echo "</pre><br>";
$arr = array("Status" => 'Ok');
echo json_encode($arr);
