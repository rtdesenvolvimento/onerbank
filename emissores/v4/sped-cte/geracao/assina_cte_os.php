<?php
if (!isset($_SESSION)):
    session_start();
endif;
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once '../bootstrap.php';

use NFePHP\CTe\Make;
use NFePHP\CTe\Tools;


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'ctebdos');
$sql = "SELECT * from ctebdos where nct ='" . $_GET['arquivo'] . "'";
$arrayParam = '';
$ctebdos = $crud->getSQLGeneric($sql, $arrayParam, FALSE);


$cte = new Make();
$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $cteTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$filename = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/entradas/" . $ctebdos->chavecte . "-cte.xml";

$xml = file_get_contents($filename);
$xml = $cteTools->assina($xml);

file_put_contents($filename, $xml);
chmod($filename, 0777);

$arr = array("Status" => 'Ok');

echo json_encode($arr);
