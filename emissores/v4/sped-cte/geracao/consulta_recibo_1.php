<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once '../bootstrap.php';

use NFePHP\NFe\ToolsNFe;

$nfe = new ToolsNFe('../config/' . $_SESSION['BD'] . '/config.json');
$nfe->setModelo('55');

$nNFbd = $_GET['arquivo'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');

$sql = "SELECT recibo,chavenfe from nfe where nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);


$aResposta = array();
$tpAmb = $nfe->aConfig['tpAmb'];

$SVCRS = array("AM", "BA", "CE", "GO", "MA", "MS", "MT", "PA", "PE", "PI", "PR");


if (in_array($nfe->aConfig['siglaUF'], $SVCRS)) {
    $chave = $dadosnfe->chavenfe;
    $retorno = $nfe->sefazConsultaChave($chave, $tpAmb, $aResposta);

    $pdo = Conexao::getInstance();
    $crudup = Crud::getInstance($pdo, 'nfe');
    $pdo->beginTransaction();
    $retorno = $crudup->Sql("UPDATE nfe set nProt = {$aResposta['aProt']['nProt']} where nNF = {$nNFbd}");
    $pdo->commit();
    $pdo = null;
    $curd = null;
} else {
    $recibo = $dadosnfe->recibo;
    $retorno = $nfe->sefazConsultaRecibo($recibo, $tpAmb, $aResposta);

    $pdo = Conexao::getInstance();
    $crudup = Crud::getInstance($pdo, 'nfe');
    $pdo->beginTransaction();
    $retorno = $crudup->Sql("UPDATE nfe set nProt = {$aResposta['aProt'][0]['nProt']} where nNF = {$nNFbd}");
    $pdo->commit();
    $pdo = null;
    $curd = null;
}




$json = json_encode($aResposta);
echo $json;
