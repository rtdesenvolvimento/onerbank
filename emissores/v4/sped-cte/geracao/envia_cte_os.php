<?php

if (!isset($_SESSION)):
    session_start();
endif;
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'ctebdos');
$sql = "SELECT * from ctebdos where nct ='" . $_GET['arquivo'] . "'";
$arrayParam = '';
$ctebdos = $crud->getSQLGeneric($sql, $arrayParam, FALSE);


include_once '../bootstrap.php';

use NFePHP\CTe\Make;
use NFePHP\CTe\Tools;

$cte = new Make();
$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $cteTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$filename = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/entradas/" . $ctebdos->chavecte . "-cte.xml";

$xml = file_get_contents($filename);

$aRetorno = array();
$idLote = '';
$indSinc = '1';
$flagZip = false;
$retorno = $cteTools->sefazEnviaCTeOS($xml, $tpAmb , $idLote, $aRetorno, $indSinc, $flagZip);

$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'ctebdos');
$pdo->beginTransaction();
$retorno = $crudup->Sql("UPDATE ctebdos set recibo = {$aRetorno['nRec']} where nct = {$ctebdos->nct}");
$pdo->commit();
$pdo = null;
$curd = null;

//echo '<pre>';
//echo htmlspecialchars($cteTools->soapDebug);
//print_r($aRetorno);
//echo "</pre>";
//
$resposta = array("Status" => 'Ok', "Recibo" => $aRetorno['nRec']);
//
echo json_encode($resposta);
?>