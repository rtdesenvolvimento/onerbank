<?php

if (!isset($_SESSION)):
    session_start();
endif;

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');

function removeAcentos($nome) {
    $nome = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $nome));
    return $nome;
}

$nct = $_GET['arquivo'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where emitenteid ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from ctebd where nct='$nct'";
$arrayParam = '';
$ctebd = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

require_once '../bootstrap.php';

use NFePHP\CTe\Make;
use NFePHP\CTe\Tools;

$cte = new Make();
$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$datanf = $ctebd->dhemi;
$dhEmi = parseDate($datanf, "Y-m-d\TH:i:sP");
$numeroCTE = $ctebd->nct;

$cteTools->aConfig['schemesCTe'] = "PL_CTe_300"; // Versao do XML
$cteTools->aConfig['pathXmlUrlFileCTe'] = "cte_ws3.xml";

function CodigoUF($estado) {
    switch ($estado) :
        case 'RO':
            return '11';
            break;
        case 'AC':
            return '12';
            break;
        case 'AM':
            return '13';
            break;
        case 'RR':
            return '14';
            break;
        case 'PA':
            return '15';
            break;
        case 'AP':
            return '16';
            break;
        case 'TO':
            return '17';
            break;
        case 'MA':
            return '21';
            break;
        case 'PI':
            return '22';
            break;
        case 'CE':
            return '23';
            break;
        case 'RN':
            return '24';
            break;
        case 'PB':
            return '25';
            break;
        case 'PE':
            return '26';
            break;
        case 'AL':
            return '27';
            break;
        case 'SE':
            return '28';
            break;
        case 'BA':
            return '29';
            break;
        case 'MG':
            return '31';
            break;
        case 'ES':
            return '32';
            break;
        case 'RJ':
            return '33';
            break;
        case 'SP':
            return '35';
            break;
        case 'PR':
            return '41';
            break;
        case 'SC':
            return '42';
            break;
        case 'RS':
            return '43';
            break;
        case 'MS':
            return '50';
            break;
        case 'MT':
            return '51';
            break;
        case 'GO':
            return '52';
            break;
        case 'DF':
            return '53';
            break;
    endswitch;
}

$ncCt = $numeroCTE;
$ncCt .= '00000000';

$ncCt = substr($ncCt, 0, 8);
$chave = $cte->montaChave(
        $cUF = CodigoUF($emitente->UF), // Codigo da UF da tabela do IBGE: 41-PR
        $ano = date('y', strtotime($dhEmi)), $mes = date('m', strtotime($dhEmi)), $cnpj = $emitente->CNPJ, $mod = '57', // Modelo do documento fiscal: 57 para identificação do CT-e
        $serie = $ctebd->serie, // Serie do CTe
        $numero = $numeroCTE, // Numero do CTe
        $tpEmis = $ctebd->tpemis, // Forma de emissao do CTe: 1-Normal; 4-EPEC pela SVC; 5-Contingência
        $cCT = $ncCt
);


$resp = $cte->infCteTag($chave, $versao = '3.00');
$cDV = substr($chave, -1);      //Digito Verificador
$resp = $cte->ideTag(
        $cUF = CodigoUF($emitente->UF), // Codigo da UF da tabela do IBGE
        $cCT = $ncCt, // Codigo numerico que compoe a chave de acesso
        $CFOP = $ctebd->cfop, // Codigo fiscal de operacoes e prestacoes
        $natOp = substr(removeAcentos($ctebd->natop), 0, 60), // Natureza da operacao
        //$forPag = '',              // 0-Pago; 1-A pagar; 2-Outros
        $mod = $ctebd->mode, // Modelo do documento fiscal: 57 para identificação do CT-e
        $serie = $ctebd->serie, // Serie do CTe
        $nCT = $numeroCTE, // Numero do CTe
        $dhEmi, // Data e hora de emissão do CT-e: Formato AAAA-MM-DDTHH:MM:DD
        $tpImp = '1', // Formato de impressao do DACTE: 1-Retrato; 2-Paisagem.
        $tpEmis = '1', // Forma de emissao do CTe: 1-Normal; 4-EPEC pela SVC; 5-Contingência
        $cDV, // Codigo verificador
        $tpAmb = $emitente->tpAmb, // 1- Producao, 2-homologacao
        $tpCTe = '0', // 0- CT-e Normal; 1 - CT-e de Complemento de Valores;
        // 2 -CT-e de Anulação; 3 - CT-e Substituto
        $procEmi = '0', // Descricao no comentario acima
        $verProc = '3.0', // versao do aplicativo emissor
        $indGlobalizado = '',
        //$refCTE = '',             // Chave de acesso do CT-e referenciado
        $cMunEnv = $ctebd->cmunemi, // Utilizar a tabela do IBGE. Informar 9999999 para as operações com o exterior.
        $xMunEnv = $ctebd->xmunemi, // Informar PAIS/Municipio para as operações com o exterior.
        $UFEnv = $ctebd->ufemi, // Informar 'EX' para operações com o exterior.
        $modal = $ctebd->modal, // Preencher com:01-Rodoviário; 02-Aéreo; 03-Aquaviário;04-
        $tpServ = $ctebd->tpserv, // 0- Normal; 1- Subcontratação; 2- Redespacho;
        // 3- Redespacho Intermediário; 4- Serviço Vinculado a Multimodal
        $cMunIni = $ctebd->cmunini, // Utilizar a tabela do IBGE. Informar 9999999 para as operações com o exterior.
        $xMunIni = $ctebd->xmunini, // Informar 'EXTERIOR' para operações com o exterior.
        $UFIni = $ctebd->ufini, // Informar 'EX' para operações com o exterior.
        $cMunFim = $ctebd->cmunfim, // Utilizar a tabela do IBGE. Informar 9999999 para operações com o exterior.
        $xMunFim = $ctebd->xmunfim, // Informar 'EXTERIOR' para operações com o exterior.
        $UFFim = $ctebd->uffim, // Informar 'EX' para operações com o exterior.
        $retira = '1', // Indicador se o Recebedor retira no Aeroporto, Filial,
        // Porto ou Estação de Destino? 0-sim; 1-não
        $xDetRetira = '', // Detalhes do retira
        $indIEToma = '1', 
        $dhCont = '', // Data e Hora da entrada em contingência; no formato AAAAMM-DDTHH:MM:SS
        $xJust = ''                 // Justificativa da entrada em contingência
);

if ($ctebd->toma != '4') :
    $resp = $cte->toma3Tag(
            $toma = $ctebd->toma                 // Indica o "papel" do tomador: 0-Remetente; 1-Expedidor; 2-Recebedor; 3-Destinatário
    );
endif;

$qtsdoctoma = strlen($ctebd->cnpjcpf);
if ($qtsdoctoma <= '11') :
    $cpfcnpj = $ctebd->cnpjcpf;
    $cnpjcpf = '';
else :
    $cpfcnpj = '';
    $cnpjcpf = $ctebd->cnpjcpf;
endif;

if ($emitente->tpAmb == '2') :
    $xnome = "CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
else :
    $xnome = $ctebd->xnome;
endif;

$resp = $cte->toma4Tag(
        $toma = '4', // 4-Outros, informar os dados cadastrais do tomador quando ele for outros
        $CNPJ = $cnpjcpf, // CNPJ
        $CPF = $cpfcnpj, // CPF
        $IE = $ctebd->ie, // Iscricao estadual
        $xNome = $xnome, // Razao social ou Nome
        $xFant = $ctebd->xfant, // Nome fantasia
        $fone = '', // Telefone
        $email = ''   // email
);
$resp = $cte->enderTomaTag(
        $xLgr = $ctebd->xlgr, // Logradouro
        $nro = $ctebd->xnum, // Numero
        $xCpl = $ctebd->xcpl, // COmplemento
        $xBairro = $ctebd->xbairro, // Bairro
        $cMun = $ctebd->cmun, // Codigo do municipio do IBEGE Informar 9999999 para operações com o exterior
        $xMun = $ctebd->xmun, // Nome do município (Informar EXTERIOR para operações com o exterior.
        $CEP = $ctebd->cep, // CEP
        $UF = $ctebd->uf, // Sigla UF (Informar EX para operações com o exterior.)
        $cPais = $ctebd->cpais, // Codigo do país ( Utilizar a tabela do BACEN )
        $xPais = $ctebd->xpais                   // Nome do pais
);


$resp = $cte->emitTag(
        $CNPJ = $emitente->CNPJ, // CNPJ do emitente
        $IE = $emitente->IE, // Inscricao estadual
        $IEST = "", // Inscricao estadual
        $xNome = $emitente->xNome, // Razao social
        $xFant = $emitente->xFant // Nome fantasia
);
$resp = $cte->enderEmitTag(
        $xLgr = $emitente->xLgr, // Logradouro
        $nro = $emitente->nro, // Numero
        $xCpl = $emitente->xCpl, // Complemento
        $xBairro = $emitente->xBairro, // Bairro
        $cMun = $emitente->cMun, // Código do município (utilizar a tabela do IBGE)
        $xMun = $emitente->xMun, // Nome do municipio
        $CEP = $emitente->CEP, // CEP
        $UF = $emitente->UF, // Sigla UF
        $fone = ''                       // Fone
);


$qtsrem = strlen($ctebd->remcnpj);
if ($qtsrem == '14') :
    $remcnpj = $ctebd->remcnpj;
    $remcpf = '';
else :
    $remcnpj = '';
    $remcpf = $ctebd->remcnpj;
endif;

if ($emitente->tpAmb == '2') :
    $xnomer = "CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
else :
    $xnomer = $ctebd->remxnome;
endif;

$resp = $cte->remTag(
        $CNPJ = $remcnpj, // CNPJ
        $CPF = $remcpf, // CPF
        $IE = $ctebd->remie, // Inscricao estadual
        $xNome = $xnomer, $Fant = $ctebd->remxfant, // Nome fantasia
        $fone = $ctebd->remtelefone, // Fone
        $email = ''                           // Email
);
$resp = $cte->enderRemeTag(
        $xLgr = $ctebd->remxlgr, // Logradouro
        $nro = $ctebd->remnro, // Numero
        $xCpl = $ctebd->remcpl, // Complemento
        $xBairro = $ctebd->remxbairro, // Bairro
        $cMun = $ctebd->remcmun, // Codigo Municipal (Informar 9999999 para operações com o exterior.)
        $xMun = $ctebd->remxmun, // Nome do municipio (Informar EXTERIOR para operações com o exterior.)
        $CEP = $ctebd->remcep, // CEP
        $UF = $ctebd->remxuf, // Sigla UF (Informar EX para operações com o exterior.)
        $cPais = '1058', // Codigo do pais ( Utilizar a tabela do BACEN )
        $xPais = $ctebd->remxpais                   // Nome do pais
);

$qtscomdest = strlen($ctebd->comdestcnpj);
if ($qtscomdest == '14') :
    $comdestcnpj = $ctebd->comdestcnpj;
    $comdestcpf = '';
else :
    $comdestcnpj = '';
    $comdestcpf = $ctebd->comdestcnpj;
endif;

if ($emitente->tpAmb == '2') :
    $xnomed = "CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
else :
    $xnomed = $ctebd->comdestxnome;
endif;

$resp = $cte->destTag(
        $CNPJ = $comdestcnpj, // CNPJ
        $CPF = $comdestcpf, // CPF
        $IE = $ctebd->comdestie, // Inscriao estadual
        $xNome = $xnomed, $fone = $ctebd->comdestfone, // Fone
        $ISUF = '', // Inscrição na SUFRAMA
        $email = ''                           // Email
);
$resp = $cte->enderDestTag(
        $xLgr = $ctebd->comdestxlgr, // Logradouro
        $nro = $ctebd->comdestnro, // Numero
        $xCpl = $ctebd->comdestxcpl, // COmplemento
        $xBairro = $ctebd->comdestxbairro, // Bairro
        $cMun = $ctebd->comdestcmun, // Codigo Municipal (Informar 9999999 para operações com o exterior.)
        $xMun = $ctebd->comdestxmun, // Nome do Municipio (Informar EXTERIOR para operações com o exterior.)
        $CEP = $ctebd->comdestcep, // CEP
        $UF = $ctebd->comdestuf, // Sigla UF (Informar EX para operações com o exterior.)
        $cPais = '1058', // Codigo do Pais (Utilizar a tabela do BACEN)
        $xPais = $ctebd->comdestxpais               // Nome do pais
);


$resp = $cte->vPrestTag(
        $vTPrest = $ctebd->vprest, // Valor total da prestacao do servico
        $vRec = $ctebd->vrec      // Valor a receber
);
$resp = $cte->compTag(
        $xNome = 'FRETE VALOR', // Nome do componente
        $vComp = $ctebd->vprest  // Valor do componente
);
$resp = $cte->icmsTag(
        $cst = $ctebd->cst, // 00 - Tributacao normal ICMS
        $pRedBC = $ctebd->predbc, // Percentual de redução da BC (3 inteiros e 2 decimais)
        $vBC = $ctebd->vbc, // Valor da BC do ICMS
        $pICMS = $ctebd->picms, // Alícota do ICMS
        $vICMS = $ctebd->vicms, // Valor do ICMS
        $vBCSTRet = '', // Valor da BC do ICMS ST retido
        $vICMSSTRet = '', // Valor do ICMS ST retido
        $pICMSSTRet = '', // Alíquota do ICMS
        $vCred = $ctebd->vcred, // Valor do Crédito Outorgado/Presumido
        $vTotTrib = '10.00', // Valor de tributos federais, estaduais e municipais
        $outraUF = false    // ICMS devido à UF de origem da prestação, quando diferente da UF do emitente
);



$resp = $cte->infCTeNormTag();              // Grupo de informações do CT-e Normal e Substituto
$resp = $cte->infCargaTag(
        $vCarga = $ctebd->vmerc, // Valor total da carga
        $prodPred = $ctebd->propred, // Produto predominante
        $xOutCat = ''                           // Outras caracteristicas da carga
);


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'qtdcarga');
$sql = "SELECT * from qtdcarga Where cte = '" . $numeroCTE . "'";
$arrayParam = '';
$dadosqtdcarga = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

foreach ($dadosqtdcarga as $value) :
    $resp = $cte->infQTag(
            $cUnid = $value->cunid, // Código da Unidade de Medida: ( 00-M3; 01-KG; 02-TON; 03-UNIDADE; 04-LITROS; 05-MMBTU
            $tpMed = $value->tpmed, // Tipo de Medida
            // ( PESO BRUTO, PESO DECLARADO, PESO CUBADO, PESO AFORADO, PESO AFERIDO, LITRAGEM, CAIXAS e etc)
            $qCarga = $value->qcarga  // Quantidade (15 posições, sendo 11 inteiras e 4 decimais.)
    );
endforeach;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfecte');
$sql = "SELECT * from nfecte Where cte = '" . $numeroCTE . "'";
$arrayParam = '';
$dadosnfecte = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

$resp = $cte->infDocTag();

foreach ($dadosnfecte as $rownfe) :
    $resp = $cte->infNFeTag(
            $pChave = $rownfe->chave, // Chave de acesso da NF-e
            $PIN = '', // PIN SUFRAMA
            $dPrev = date("Y-m-d")                                      // Data prevista de entrega
    );
endforeach;


if ($ctebd->tpDoc != '') :

    if ($ctebd->dEmiDoc != '') :
        $dhemidoc = parseDate($ctebd->dEmiDoc, "Y-m-d");
    else :
        $dhemidoc = '';
    endif;
    if ($ctebd->dPrevDoc != '') :
        $dPrevDoc = parseDate($ctebd->dPrevDoc, "Y-m-d");
    else :
        $dPrevDoc = '';
    endif;
    $resp = $cte->infOutrosTag(
            $ctebd->tpDoc, $ctebd->descOutros, $ctebd->nDoc, $dhemidoc, $ctebd->vDocFisc, $dPrevDoc
    );
endif;

$resp = $cte->infModalTag($versaoModal = '3.00');
$resp = $cte->rodoTag(
        $RNTRC = $ctebd->rntrc    // Registro Nacional de Transportadores Rodoviários de Carga
);


$resp = $cte->complTag($xCaracAd = '', $xCaracSer = '', $xEmi = '', $origCalc = '', $destCalc = '', $ctebd->xobs);

$resp = $cte->montaCTe();
$tpAmb = $emitente->tpAmb;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;



$filename = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/entradas/{$chave}-cte.xml";

if ($resp) {
    //header('Content-type: text/xml; charset=UTF-8');
    $xml = $cte->getXML();
    file_put_contents($filename, $xml);
    //chmod($filename, 0777);
    //echo $xml;
} else {
    header('Content-type: text/html; charset=UTF-8');
    foreach ($cte->erros as $err) {
        echo 'tag: &lt;' . $err['tag'] . '&gt; ---- ' . $err['desc'] . '<br>';
    }
}
$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'ctebd');
$pdo->beginTransaction();
$retorno = $crudup->Sql("UPDATE ctebd set chavecte = {$chave} where nct = {$numeroCTE}");
$pdo->commit();
$pdo = null;
$curd = null;

if ($emitente->certificado == 'A3') :
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'notafiscala3');
    $retorno = $crud->Sql("UPDATE notafiscala3 set situacao = 'N'");

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'notafiscala3');
    $sql = "INSERT INTO notafiscala3(chavenfe,situacao,tipo) VALUES ('$chave', 'S', 'cte')";
    $retorno = $crud->Sql($sql);

endif;

$resposta = array("Status" => 'Ok');
echo json_encode($resposta);
