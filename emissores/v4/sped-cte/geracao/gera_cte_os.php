<?php

if (!isset($_SESSION)):
    session_start();
endif;

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

function removeAcentos($nome) {
    $nome = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $nome));
    return $nome;
}

$nct = $_GET['arquivo'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where emitenteid ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from ctebdos where nct='$nct'";
$arrayParam = '';
$ctebdos = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

require_once '../bootstrap.php';

use NFePHP\CTe\Make;
use NFePHP\CTe\Tools;

$cte = new Make();
$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$datanf = $ctebdos->dhemi;
$dhEmi = parseDate($datanf, "Y-m-d\TH:i:sP");
$numeroCTE = $ctebdos->nct;

$cteTools->aConfig['schemesCTe'] = "PL_CTe_300"; // Versao do XML
$cteTools->aConfig['pathXmlUrlFileCTe'] = "cte_ws3.xml";

function CodigoUF($estado) {
    switch ($estado) :
        case 'RO':
            return '11';
            break;
        case 'AC':
            return '12';
            break;
        case 'AM':
            return '13';
            break;
        case 'RR':
            return '14';
            break;
        case 'PA':
            return '15';
            break;
        case 'AP':
            return '16';
            break;
        case 'TO':
            return '17';
            break;
        case 'MA':
            return '21';
            break;
        case 'PI':
            return '22';
            break;
        case 'CE':
            return '23';
            break;
        case 'RN':
            return '24';
            break;
        case 'PB':
            return '25';
            break;
        case 'PE':
            return '26';
            break;
        case 'AL':
            return '27';
            break;
        case 'SE':
            return '28';
            break;
        case 'BA':
            return '29';
            break;
        case 'MG':
            return '31';
            break;
        case 'ES':
            return '32';
            break;
        case 'RJ':
            return '33';
            break;
        case 'SP':
            return '35';
            break;
        case 'PR':
            return '41';
            break;
        case 'SC':
            return '42';
            break;
        case 'RS':
            return '43';
            break;
        case 'MS':
            return '50';
            break;
        case 'MT':
            return '51';
            break;
        case 'GO':
            return '52';
            break;
        case 'DF':
            return '53';
            break;
    endswitch;
}

$ncCt = $numeroCTE;
$ncCt .= '00000000';

$ncCt = substr($ncCt, 0, 8);
$chave = $cte->montaChave(
        $cUF = CodigoUF($emitente->UF), // Codigo da UF da tabela do IBGE: 41-PR
        $ano = date('y', strtotime($dhEmi)), $mes = date('m', strtotime($dhEmi)), $cnpj = $emitente->CNPJ, $mod = '67', // Modelo do documento fiscal: 57 para identificação do CT-e
        $serie = $ctebdos->serie, // Serie do CTe
        $numero = $numeroCTE, // Numero do CTe
        $tpEmis = $ctebdos->tpemis, // Forma de emissao do CTe: 1-Normal; 4-EPEC pela SVC; 5-Contingência
        $cCT = $ncCt
);


$resp = $cte->infCteTag($chave, $versao = '3.00');
$cDV = substr($chave, -1);      //Digito Verificador....

$resp = $cte->ideTagCTeOS(
        $cUF = CodigoUF($emitente->UF), // Codigo da UF da tabela do IBGE
        $cCT = $ncCt, // Codigo numerico que compoe a chave de acesso
        $CFOP = $ctebdos->cfop, // Codigo fiscal de operacoes e prestacoes
        $natOp = substr(removeAcentos($ctebdos->natop), 0, 60), // Natureza da operacao
        $mod = '67', $serie = $ctebdos->serie, // Serie do CTe
        $nCT = $numeroCTE, // Numero do CTe
        $dhEmi, // Data e hora de emissão do CT-e: Formato AAAA-MM-DDTHH:MM:DD
        $tpImp = '1', // Formato de impressao do DACTE: 1-Retrato; 2-Paisagem.
        $tpEmis = '1', // Forma de emissao do CTe: 1-Normal; 4-EPEC pela SVC; 5-Contingência
        $cDV, // Codigo verificador
        $tpAmb = $emitente->tpAmb, // 1- Producao, 2-homologacao
        $tpCTe = '0', $procEmi = '0', $verProc = '3.0', $cMunEnv = $ctebdos->cmunemi, // Utilizar a tabela do IBGE. Informar 9999999 para as operações com o exterior.
        $xMunEnv = $ctebdos->xmunemi, // Informar PAIS/Municipio para as operações com o exterior.
        $UFEnv = $ctebdos->ufemi, // Informar 'EX' para operações com o exterior.
        $modal = $ctebdos->modal, // Preencher com:01-Rodoviário; 02-Aéreo; 03-Aquaviário;04-
        $tpServ = $ctebdos->tpserv, // 0- Normal; 1- Subcontratação; 2- Redespacho;
        $indIEToma = '1',
        $cMunIni = $ctebdos->cmunini, // Utilizar a tabela do IBGE. Informar 9999999 para as operações com o exterior.
        $xMunIni = $ctebdos->xmunini, // Informar 'EXTERIOR' para operações com o exterior.
        $UFIni = $ctebdos->ufini, // Informar 'EX' para operações com o exterior.
        $cMunFim = $ctebdos->cmunfim, // Utilizar a tabela do IBGE. Informar 9999999 para operações com o exterior.
        $xMunFim = $ctebdos->xmunfim, // Informar 'EXTERIOR' para operações com o exterior.
        $UFFim = $ctebdos->uffim, // Informar 'EX' para operações com o exterior.
        $dhCont = '', $xJust = ''
);



$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'percursocte');
$sql = "SELECT * FROM percursocte where nct = {$_GET['arquivo']}";
$arrayParam = '';
$dados = $crud->getSQLGeneric($sql, $arrayParam, TRUE);

foreach ($dados as $row) :
    $res = $cte->infPercursoTag($uf = $row->UFPer);
endforeach;


$qtsdoctoma = strlen($ctebdos->cnpjcpf);
if ($qtsdoctoma <= '11') :
    $cpfcnpj = $ctebdos->cnpjcpf;
    $cnpjcpf = '';
else :
    $cpfcnpj = '';
    $cnpjcpf = $ctebdos->cnpjcpf;
endif;

if ($emitente->tpAmb == '2') :
    $xnome = "CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
else :
    $xnome = $ctebdos->xnome;
endif;


$resp = $cte->tomaTag(
        $CNPJ = $cnpjcpf, // CNPJ
        $CPF = $cpfcnpj, // CPF
        $IE = $ctebdos->ie, 
        $xNome = $xnome, // Razao social ou Nome
        $xFant = '', // Nome fantasia
        $fone = '3730165552', // Telefone
        $email = '', // email
        $xLgr = $ctebdos->xlgr, // Logradouro
        $nro = $ctebdos->xnum, // Numero
        $xCpl = $ctebdos->xcpl, // COmplemento
        $xBairro = $ctebdos->xbairro, // Bairro
        $cMun = $ctebdos->cmun, // Codigo do municipio do IBEGE Informar 9999999 para operações com o exterior
        $xMun = $ctebdos->xmun, // Nome do município (Informar EXTERIOR para operações com o exterior.
        $CEP = $ctebdos->cep, // CEP
        $UF = $ctebdos->uf, // Sigla UF (Informar EX para operações com o exterior.)
        $cPais = $ctebdos->cpais, // Codigo do país ( Utilizar a tabela do BACEN )
        $xPais = $ctebdos->xpais
);

// $CNPJ = '',
//        $CPF = '',
//        $IE = '',
//        $xNome = '',
//        $xFant = '',
//        $fone = '',
//        $email = '',
//        $xLgr = '',
//        $nro = '',
//        $xCpl = '',
//        $xBairro = '',
//        $cMun = '',
//        $xMun = '',
//        $CEP = '',
//        $UF = '',
//        $cPais = '',
//        $xPais = ''


$resp = $cte->emitTag(
        $CNPJ = $emitente->CNPJ, // CNPJ do emitente
        $IE = $emitente->IE, // Inscricao estadual
        $IEST = "", // Inscricao estadual
        $xNome = $emitente->xNome, // Razao social
        $xFant = $emitente->xFant // Nome fantasia
);
$resp = $cte->enderEmitTag(
        $xLgr = $emitente->xLgr, // Logradouro
        $nro = $emitente->nro, // Numero
        $xCpl = $emitente->xCpl, // Complemento
        $xBairro = $emitente->xBairro, // Bairro
        $cMun = $emitente->cMun, // Código do município (utilizar a tabela do IBGE)
        $xMun = $emitente->xMun, // Nome do municipio
        $CEP = $emitente->CEP, // CEP
        $UF = $emitente->UF, // Sigla UF
        $fone = ''                       // Fone
);


$resp = $cte->vPrestTag(
        $vTPrest = $ctebdos->vprest, // Valor total da prestacao do servico
        $vRec = $ctebdos->vrec      // Valor a receber
);

$resp = $cte->icmsTag(
        $cst = 'SN', // 00 - Tributacao normal ICMS
        $pRedBC = $ctebdos->predbc, // Percentual de redução da BC (3 inteiros e 2 decimais)
        $vBC = $ctebdos->vbc, // Valor da BC do ICMS
        $pICMS = $ctebdos->picms, // Alícota do ICMS
        $vICMS = $ctebdos->vicms, // Valor do ICMS
        $vBCSTRet = '', // Valor da BC do ICMS ST retido
        $vICMSSTRet = '', // Valor do ICMS ST retido
        $pICMSSTRet = '', // Alíquota do ICMS
        $vCred = $ctebdos->vcred, // Valor do Crédito Outorgado/Presumido
        $vTotTrib = '0', // Valor de tributos federais, estaduais e municipais
        $outraUF = false    // ICMS devido à UF de origem da prestação, quando diferente da UF do emitente
);

$res = $cte->infTribFedTag(
        $vPIS = '', $vCOFINS = '', $vIR = '', $vINSS = '0.00', $vCSLL = ''
);


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'veiculo');
$sql = "SELECT * from veiculo Where placa ='$ctebdos->veiculo'";
$arrayParam = '';
$dadosveic = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;


$resp = $cte->rodoOSTag($TAF = $ctebdos->TAF, $nroRegEstadual = $ctebdos->nroRegEstadual);

$resp = $cte->veicCTeOSTag(
        $placa = $dadosveic->placa, $RENAVAM = $dadosveic->renavam, $uf = $dadosveic->uf, $CPF = '', $CNPJ = '', $taf = '', $nroRegEstadual = '', $xNome = '', $IE = '', $ufProp = '', $tpProp = '');


$resp = $cte->infCTeNormTag();   // Grupo de informações do CT-e Normal e Substituto


$resp = $cte->infServicoTag(
        $xDescServ = $ctebdos->xDescServ, $qCarga = $ctebdos->qCarga
);

$resp = $cte->complTag($xCaracAd = '', $xCaracSer = '', $xEmi = '', $origCalc = '', $destCalc = '', $ctebdos->xobs);


$resp = $cte->infModalTag($versaoModal = '3.00');

$resp = $cte->montaCTeOs();


$tpAmb = $emitente->tpAmb;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$filename = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/entradas/{$chave}-cte.xml";

if ($resp) {
    //header('Content-type: text/xml; charset=UTF-8');
    $xml = $cte->getXML();
    file_put_contents($filename, $xml);
    //chmod($filename, 0777);
    //echo $xml;
} else {
    header('Content-type: text/html; charset=UTF-8');
    foreach ($cte->erros as $err) {
        echo 'tag: &lt;' . $err['tag'] . '&gt; ---- ' . $err['desc'] . '<br>';
    }
}
$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'ctebdos');
$pdo->beginTransaction();
$retorno = $crudup->Sql("UPDATE ctebdos set chavecte = {$chave} where nct = {$numeroCTE}");
$pdo->commit();
$pdo = null;
$curd = null;

if ($emitente->certificado == 'A3') :
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'notafiscala3');
    $retorno = $crud->Sql("UPDATE notafiscala3 set situacao = 'N'");

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'notafiscala3');
    $sql = "INSERT INTO notafiscala3(chavenfe,situacao,tipo) VALUES ('$chave', 'S', 'cte')";
    $retorno = $crud->Sql($sql);

endif;

$resposta = array("Status" => 'Ok');
echo json_encode($resposta);
