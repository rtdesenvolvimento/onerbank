<?php
if (!isset($_SESSION)):
    session_start();
endif;
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include_once '../bootstrap.php';

use NFePHP\CTe\Tools;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'ctebd');
$sql = "SELECT * from ctebd where nct ='" . $_GET['arquivo'] . "'";
$arrayParam = '';
$ctebd = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $cteTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

//$filename = "/Applications/XAMPP/xamppfiles/htdocs/projetos/sped-cte/xml/cte/{$chave}-cte.xml";
$filename = "../XML/{$_SESSION['BD']}/CTe/{$pastaxml}/entradas/" . $ctebd->chavecte . "-cte.xml";


if (!$cteTools->validarXml($filename) || sizeof($cteTools->erros)) {
    $erros = array();
    foreach ($cteTools->erros as $erro) {
        $erros[] = $erro;
    }
    $arr = array("Status" => 'Erro', "Erro" => $erros);
    echo json_encode($arr);
    exit;
}

$arr = array("Status" => 'Ok');

echo json_encode($arr);

