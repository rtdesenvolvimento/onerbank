<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';
require_once '../bootstrap.php';

$nNFbd = $_GET['nNFbd'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where id ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');
$sql = "SELECT dhEmi,chavenfe,recibo from nfe where nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);


$chave = $dadosnfe->chavenfe;
$pasta = parseDate($dadosnfe->dhEmi, "Ym");
$tpAmb = $emitente->ambiente;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

use NFePHP\DA\NFe\Dacce;
use NFePHP\DA\Legacy\FilesFolders;

$aEnd = array(
    'razao' => $emitente->nome,
    'logradouro' => $emitente->rua,
    'numero' => $emitente->numero,
    'complemento' => $emitente->complemento,
    'bairro' => $emitente->bairro,
    'CEP' => $emitente->cep,
    'municipio' => $emitente->cidade,
    'UF' => $emitente->uf,
    'telefone' => $emitente->telefone,
    'email' => $emitente->email
);

if ($emitente->certificado == 'A3') :
    $xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/cartacorrecao/{$pasta}/{$chave}-nfe.xml";
else :
    $xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/cartacorrecao/{$pasta}/{$chave}-nfe.xml";
endif;

use NFePHP\DA\NFe\Daevento;

if ($emitente->url_logo != '') {

    $url = $emitente->url_logo;
    $url = explode('/', $url);
    $url = $url[count($url)-1];
    $url = '../../../../assets/uploads/'.$url;

    if (file_exists($url)) {
        $logo = 'data://text/plain;base64,' . base64_encode(file_get_contents($url));
    } else {
        $logo = 'data://text/plain;base64,'. base64_encode(file_get_contents(realpath('nfce.png')));
    }

} else {
    $logo = 'data://text/plain;base64,'. base64_encode(file_get_contents(realpath('nfce.png')));
}

try {

    $docxml = file_get_contents($xml); // Ambiente Windows

    $daevento = new Daevento($docxml, $aEnd);
    $daevento->debugMode(true);
    $daevento->creditsIntegratorFooter('Resultatec Sistemas Digitais - http://www.resultatec.com.br');
    $pdf = $daevento->render($logo);

    header('Content-Type: application/pdf');
    echo $pdf;
} catch (\Exception $e) {
    echo $e->getMessage();
}