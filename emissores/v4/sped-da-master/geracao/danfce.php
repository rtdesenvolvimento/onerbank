<?php
require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';

$nNFbd = $_GET['nNFbd'];

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where id ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');
$sql = "SELECT dhEmi,chavenfe,recibo from nfce where nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);


$chave = $dadosnfe->chavenfe;
$pasta = parseDate($dadosnfe->dhEmi, "Ym");

$tpAmb = $emitente->ambiente;
//$pasta = '201712';
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once '../bootstrap.php';

use NFePHP\DA\NFe\Danfce;
//use NFePHP\DA\Legacy\FilesFolders;

if ($emitente->certificado == 'A3') :
    $xml = "../../nfephp-master/XML/".$emitente->cnpj."/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-nfce.xml";
else :
    $xml = "../../nfephp-master/XML/".$emitente->cnpj."/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-nfce.xml";
endif;

if (file_exists($xml)) :
    echo '';
else :
    $xml = "../../nfephp-master/XML/".$emitente->cnpj."/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-protNFe.xml";
endif;

$docxml = file_get_contents($xml); // Ambiente Windows


if ($emitente->url_logo != '') {

    $url = $emitente->url_logo;
    $url = explode('/', $url);
    $url = $url[count($url)-1];
    $url = '../../../../assets/uploads/'.$url;

    if (file_exists($url)) {
        $logo = 'data://text/plain;base64,' . base64_encode(file_get_contents($url));
    } else {
        $logo = 'data://text/plain;base64,'. base64_encode(file_get_contents(realpath('nfce.png')));
    }

} else {
    $logo = 'data://text/plain;base64,'. base64_encode(file_get_contents(realpath('nfce.png')));
}

$danfce = new Danfce($docxml);
$danfce->debugMode(true);
$danfce->creditsIntegratorFooter('Resultatec Sistemas Digitais - http://www.resultatec.com.br');

header('Content-Type: application/pdf');

$pdf = $danfce->render($logo);
echo $pdf;