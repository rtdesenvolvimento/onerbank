<?php

header('Content-Type: application/pdf');

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';

$nNFbd = $_GET['nNFbd'];


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where id ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');
$sql = "SELECT dhEmi,chavenfe,recibo from nfe where nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);


$chave = $dadosnfe->chavenfe;
$pasta = parseDate($dadosnfe->dhEmi, "Ym");


$tpAmb = $emitente->ambiente;
//$pasta = '201712';
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once '../bootstrap.php';

use NFePHP\DA\NFe\Danfe;
use NFePHP\DA\Legacy\FilesFolders;

if ($emitente->certificado == 'A3') :
    $xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-nfe.xml";
else :
    $xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-nfe.xml";
endif;

if (file_exists($xml)) :
    echo '';
else :
    $xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-protNFe.xml";
endif;

if (file_exists($xml)) :
    echo '';
else :
    $xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/{$pastaxml}/entradas/{$chave}-nfe.xml";
endif;

$docxml = FilesFolders::readFile($xml);
try {

    if ($emitente->url_logo != '') {

        $url = $emitente->url_logo;
        $url = explode('/', $url);
        $url = $url[count($url)-1];
        $url = '../../../../assets/uploads/'.$url;

        if (file_exists($url)) {
            $logo = 'data://text/plain;base64,' . base64_encode(file_get_contents($url));
        } else {
            $logo = 'data://text/plain;base64,'. base64_encode(file_get_contents(realpath('nfce.png')));
        }

    } else {
        $logo = 'data://text/plain;base64,'. base64_encode(file_get_contents(realpath('nfe.jpg')));
    }

    $danfe = new Danfe($docxml);
    $danfe->debugMode(true);
    $danfe->creditsIntegratorFooter('Resultatec Sistemas Digitais - http://www.resultatec.com.br');

    // Caso queira mudar a configuracao padrao de impressao
    /*  $this->printParameters( $orientacao = '', $papel = 'A4', $margSup = 2, $margEsq = 2 ); */
    //Informe o numero DPEC
    /*  $danfe->depecNumber('123456789'); */
    //Configura a posicao da logo
    /*  $danfe->logoParameters($logo, 'C', false);  */
    //Gera o PDF
    $pdf = $danfe->render($logo);

    header('Content-Type: application/pdf');
    echo $pdf;

} catch (InvalidArgumentException $e) {
    echo "Ocorreu um erro durante o processamento :" . $e->getMessage();
}   