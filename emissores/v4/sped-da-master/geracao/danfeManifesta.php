<?php

require_once '../../conexao/Conexao_class.php';
require_once '../../conexao/Crud_class.php';
require_once '../../conexao/Emitente_class.php';
require_once '../bootstrap.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$nNFbd = $_GET['arquivo'];


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfe');
$sql = "SELECT * from manifesta where manifestaid='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

//echo $sql;
$chave = $dadosnfe->chave;
//$pasta = parseDate($dadosnfe->dhEmi, "Ym");



use NFePHP\DA\NFe\Danfe;
use NFePHP\DA\Legacy\FilesFolders;

$xml = "../../nfephp-master/XML/{$emitente->cnpj}/NF-e/producao/recebidas/{$chave}-nfe.xml";


if (file_exists($xml)) :
    echo '';
else :
    echo 'Nota fiscal não manifestada';
    exit;
endif;


$docxml = FilesFolders::readFile($xml);
try {
    $danfe = new Danfe($docxml, 'P', 'A4', '', 'I', '');
    $id = $danfe->montaDANFE();
    $pdf = $danfe->render();
    //o pdf porde ser exibido como view no browser
    //salvo em arquivo
    //ou setado para download forçado no browser 
    //ou ainda gravado na base de dados
    header('Content-Type: application/pdf');
    echo $pdf;
} catch (InvalidArgumentException $e) {
    echo "Ocorreu um erro durante o processamento :" . $e->getMessage();
}   