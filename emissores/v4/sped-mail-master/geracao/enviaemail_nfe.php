<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
require_once "../../_backend/_class/Emitente_class.php";
require_once "../../_backend/_class/configura_email_class.php";

//
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
//
$nNFbd = $_GET['arquivo'];
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT chavenfe, dhEmi, recibo from nfe where  nNF='$nNFbd'";
$arrayParam = '';
$dadosnfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$chave = $dadosnfe->chavenfe;
$pasta = parseDate($dadosnfe->dhEmi, "Ym");

$json1 = file_get_contents('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');
$json1 = json_decode($json1, true);

$tpAmb = $emitente->tpAmb;
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

//print_r($json1);
require_once '../bootstrap.php';

use NFePHP\Mail\Mail;

try {
    //a configuração é uma stdClass com os campos acima indicados
    //esse parametro é OBRIGATÓRIO
    $mail = new Mail($config);

    //use isso para inserir seu próprio template HTML com os campos corretos 
    //para serem substituidos em execução com os dados dos xml
    $htmlTemplate = '';
    $mail->loadTemplate($htmlTemplate);
    //aqui são passados os documentos, tanto pode ser um path como o conteudo
    //desses documentos
    $xml = "{$json1['pathNFeFiles']}/{$pastaxml}/enviadas/aprovadas/$pasta/{$chave}-nfe.xml";

//    echo $xml;
    $pdf = ''; //não é obrigatório passar o PDF, tendo em vista que é uma BOBAGEM
    $mail->loadDocuments($xml, $pdf);

    //se não for passado esse array serão enviados apenas os emails
    //que estão contidos no XML, isto se existirem
    $addresses = [];

    //envia emails, se false apenas para os endereçospassados
    //se true para todos os endereços contidos no XML e mais os indicados adicionais
    $mail->send($addresses, true);
    $xml = "<retorno>
        <status>OK</status>
        <motivo>E-mail Enviado com sucesso!</motivo>
            </retorno>";
    echo $xml;
} catch (\InvalidArgumentException $e) {
    $xml = "<retorno>
        <status>Erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
    echo $xml;
} catch (\RuntimeException $e) {
    $xml = "<retorno>
        <status>Erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
    echo $xml;
} catch (\Exception $e) {
    $xml = "<retorno>
        <status>Erro</status>
        <motivo>" . $e->getMessage() . "</motivo>
            </retorno>";
    echo $xml;
}  