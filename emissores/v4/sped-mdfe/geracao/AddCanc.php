<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once '../bootstrap.php';
use NFePHP\MDFe\Tools;


$mdfe = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$aResposta = array();

$nMDF = $_GET['arquivo'];

$tpAmb = $mdfe->aConfig['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'mdfe');
$sql = "SELECT dhEmi,chavemdfe,recibo from mdfe where nMDF='$nMDF'";
$arrayParam = '';
$dadosmdfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$pasta = parseDate($dadosmdfe->dhEmi, "Ym");

$indSinc = '0'; //0=asíncrono, 1=síncrono
$chave = $dadosmdfe->chavemdfe;
$recibo = $dadosmdfe->recibo;

$pathProtfile = "{$mdfe->aConfig['pathMDFeFiles']}/{$pastaxml}/temporarias/{$pasta}/{$chave}-CancMDFe-eventoMDFe.xml";
$pathMDFefile = "{$mdfe->aConfig['pathMDFeFiles']}/{$pastaxml}/enviadas/aprovadas/{$pasta}/{$chave}-protMDFe.xml";
//$pathProtfile = '/var/www/nfe/homologacao/temporarias/201501/35150158716523000119550010000000071000000076-CancNFe-retEnvEvento.xml';
$saveFile = true;
$retorno = $mdfe->addCancelamento($pathMDFefile, $pathProtfile, $saveFile);

$arr = array("Status" => 'Ok');
echo json_encode($arr);

//echo '<br><br><PRE>';
//echo htmlspecialchars($retorno);
//echo '</PRE><BR>';
//echo "<br>";
