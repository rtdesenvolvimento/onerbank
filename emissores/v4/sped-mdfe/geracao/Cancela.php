<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once '../bootstrap.php';

use NFePHP\MDFe\Tools;

$nMDF = $_GET['nMDF'];
$xJust = $_GET['justificativa'];

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'mdfe');
$sql = "SELECT dhEmi,chavemdfe,recibo,nprot from mdfe where nMDF='$nMDF'";
$arrayParam = '';
$dadoscte = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$tpAmb = $mdfeTools->aConfig['tpAmb'];

$chave = $dadoscte->chavemdfe;
$nProt = $dadoscte->nprot;
//$dadoscte->nProt;

$aResposta = array();
//echo $chave;

$retorno = $mdfeTools->sefazCancela($chave, $tpAmb, $nSeqEvento = '1', $nProt,  $xJust, $aResposta);
//echo '<pre>';
//////echo htmlspecialchars($mdfeTools->soapDebug);
//print_r($aResposta);
//echo "</pre>";

if ($aResposta['cStat'] != '135') :
    $retorno = -1;
    $msg = $aResposta[xMotivo];
else :
    $pdo = Conexao::getInstance();
    $crudup = Crud::getInstance($pdo, 'mdfe');
    $pdo->beginTransaction();
    $retorno = $crudup->Sql("UPDATE mdfe set status='101' where nMDF='$nMDF'");
    $pdo->commit();
    $pdo = null;
    $curd = null;

    $retorno = true;
    $msg = "MDF-e Cancelado com Sucesso!";
endif;

$retcan = array("retorno" => $retorno, "mensagem" => $msg, "nMDF" => $nMDF);

//echo $aResposta[evento][0][cStat];
//echo $aResposta[evento][0][xMotivo];

echo json_encode($retcan);
//
//
