<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once '../bootstrap.php';

$nMDF = $_GET['arquivo'];


use NFePHP\MDFe\Make;
use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');


$aResposta = array();
$indSinc = '0'; //0=as�ncrono, 1=s�ncrono
$chave = '';

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'mdfe');
$sql = "SELECT dhEmi,chavemdfe,recibo from mdfe where nMDF='$nMDF'";
$arrayParam = '';
$dadosmdfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

$indSinc = '0'; //0=asíncrono, 1=síncrono
$chave = $dadosmdfe->chavemdfe;
$recibo = $dadosmdfe->recibo;
$pasta = parseDate($dadosmdfe->dhEmi, "Ym");

$tpAmb = $mdfeTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pathCTefile = "../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/entradas/" . $nMDF . ".xml";


if (!$indSinc) {
    $pathProtfile = "{$mdfeTools->aConfig['pathMDFeFiles']}/{$pastaxml}/temporarias/{$pasta}/{$recibo}-retConsReciMDFe.xml";
} else {
    $pathProtfile = "{$mdfeTools->aConfig['pathMDFeFiles']}/{$pastaxml}/temporarias/{$pasta}/{$recibo}-retEnviMDFe.xml";
}
$saveFile = true;
$retorno = $mdfeTools->addProtocolo($pathCTefile, $pathProtfile, $saveFile);

$arr = array("Status" => 'Ok');
echo json_encode($arr);
