<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once '../bootstrap.php';

use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $mdfeTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$aResposta = array();
$recibo = $_GET['recibo'];

$retorno = $mdfeTools->sefazConsultaRecibo($recibo, $tpAmb, $aResposta);

$pdo = Conexao::getInstance();
$crudup = Crud::getInstance($pdo, 'mdfe');
$pdo->beginTransaction();
$sql = "UPDATE mdfe set tpamb = {$tpAmb}, chavemdfe='{$aResposta['aProt']['chMDFe']}', recibo = '{$aResposta['nRec']}', nprot = '{$aResposta['aProt']['nProt']}' where nMDF = {$_GET['nMDF']}";
$retorno = $crudup->Sql($sql);
$pdo->commit();
$pdo = null;
$curd = null;

$arr = array("Recibo" => "$recibo");

$json = json_encode($aResposta);
echo $json;
