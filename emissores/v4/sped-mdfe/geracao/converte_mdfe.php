<?php
if (!isset($_SESSION)):
    session_start();
endif;

include_once '../bootstrap.php';
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

error_reporting(E_ALL);
ini_set('display_errors', 'all');

require_once('../src/ConvertMDFePHP.class.php');
require_once ('../libs/Extras/CommonNFePHP.class.php');


use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');
$tpAmb = $mdfeTools->aConfig['tpAmb'];
$mdfeconv = new ConvertMDFePHP();
$mdfeconv->tpAmb = $tpAmb;

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$filename = $_GET['arquivo'] . '.txt';
$xml = $mdfeconv->MDFetxt2xml("../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/txt/" . $filename, $tpAmb, '1');

$mdfefile = $xml[0];

//echo $mdfefile;

if ($mdfefile != '') {
    $filexml = substr($filename, 0, strlen($filename) - 4);
    $filexml.= ".xml";
    // removo LF,CR,TAB e brancos
    $mdfefile = str_replace("\n", "", $mdfefile);
    $mdfefile = str_replace("\r", "", $mdfefile);
    $mdfefile = str_replace("\t", "", $mdfefile);
    $mdfefile = str_replace("  ", " ", $mdfefile);
    $mdfefile = str_replace("  ", " ", $mdfefile);
    $mdfefile = str_replace("  ", " ", $mdfefile);
    $mdfefile = str_replace("  ", " ", $mdfefile);
    $mdfefile = str_replace("  ", " ", $mdfefile);
    $mdfefile = str_replace("> <", "><", $mdfefile);
    $mdfefile = utf8_encode($mdfefile);

    if (!file_put_contents("../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/entradas/".$filexml, $mdfefile)) {
        $arr = array("Status" => 'Erro');
    } else {
        $arr = array("Status" => 'Ok');
    }
}

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where emitenteid ='1'";
$arrayParam = '';
$emitente = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

if ($emitente->certificado == 'A3') :
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'notafiscala3');
    $retorno = $crud->Sql("UPDATE notafiscala3 set situacao = 'N'");
    
    $chave = $_GET['arquivo'];
    
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'notafiscala3');
    $sql = "INSERT INTO notafiscala3(chavenfe,situacao,tipo) VALUES ('$chave', 'S', 'mdfe')";
    $retorno = $crud->Sql($sql);

endif;

echo json_encode($arr);
