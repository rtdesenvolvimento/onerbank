<?php
require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once '../bootstrap.php';
use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $mdfeTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'mdfe');
$sql = "SELECT dhEmi,chavemdfe,recibo from mdfe where nMDF={$_GET['arquivo']}";
$arrayParam = '';
$dadosmdfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pasta = parseDate($dadosmdfe->dhEmi, "Ym");

require_once('../libs/DamdfeNFePHP.class.php');
//echo $dadosmdfe->chavemdfe;
$arq = "../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/enviadas/aprovadas/{$pasta}/" . $dadosmdfe->chavemdfe . "-protMDFe.xml";
//$arq = ''.$dadosmdfe->chavemdfe.'-protMDFe.xml';
//echo $arq;
if (is_file($arq)) {
    $damdfe = new DamdfeNFePHP($arq, 'P', 'A4', '../images/logo.jpg', 'I');
    $teste = $damdfe->printMDFe('teste', 'I');
}