<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include_once '../bootstrap.php';

use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

function BuscaIso($valor) {
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'state');
    $sql = "SELECT * from state WHERE letter = '$valor'";
    $arrayParam = '';
    $returnstate = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
    $pdo = null;
    $curd = null;

    return $returnstate->iso;
}


$offset = timezone_offset_get(new DateTimeZone('America/Sao_Paulo'), new DateTime());
$tz = sprintf("%s%02d:%02d", ( $offset >= 0 ) ? '+' : '-', abs($offset / 3600), abs($offset % 3600));
$mdfeTools->aConfig['tz']= $tz;

$aResposta = array();
//$recibo = $_GET['recibo'];
$chave = $_GET['chave'];
$nSeqEvento = '1';
$nProt = $_GET['protocolo'];
$cUF = BuscaIso($_GET['cUF']);
$cMun = $_GET['cMun'];
$tpAmb = $mdfeTools->aConfig['tpAmb'];
$retorno = $mdfeTools->sefazEncerra($chave, $tpAmb, $nSeqEvento, $nProt, $cUF, $cMun, $aResposta);



//echo '<pre>';
//print_r($aResposta);
//echo '</pre>';

//if ($aResposta['cStat'] == '135') :
//    $pdo = Conexao::getInstance();
//    $crudup = Crud::getInstance($pdo, 'mdfe');
//    $pdo->beginTransaction();
//    $sql = "UPDATE mdfe set status = '132', nprotencerramento ='{$aResposta['aEvent']['nProt']}' where nMDF = {$_GET['nMDF']}";
//    $retorno = $crudup->Sql($sql);
//    $pdo->commit();
//    $pdo = null;
//    $curd = null;
//endif;


//echo $sql;

$json = json_encode($aResposta);
echo $json;


