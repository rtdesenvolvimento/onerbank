<?php
if (!isset($_SESSION)):
    session_start();
endif;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once '../bootstrap.php';

use NFePHP\MDFe\Make;
use NFePHP\MDFe\Tools;
use NFePHP\MDFe\Response;


$mdfe = new Make();
$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $mdfeTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

//$mdfeTools->setSSLProtocol('TLSv1.0');

$filename = "../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/entradas/" . $_GET['arquivo'] . ".xml";

$xml = file_get_contents($filename);
//$xml = $mdfeTools->assina($xml);
//$filename = "C:/xampp/htdocs/sped-cte/xml/cte/{$chave}-cte.xml";
//file_put_contents($filename, $xml);
//chmod($filename, 0777);
//echo $xml;

$retorno = array();
$idLote = '';
$indSinc = '1';
$flagZip = false;

$aRetorno = $mdfeTools->sefazEnviaLote($xml, $tpAmb, $idLote, $retorno);

$resposta = array("Status" => 'Ok', "Recibo" => $retorno['nRec']);
//
echo json_encode($resposta);

?>