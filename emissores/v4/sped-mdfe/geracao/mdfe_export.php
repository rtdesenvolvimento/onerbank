<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$nMDFEbd = $_GET['nMDF'];

function removeAcentos($texto) {
    $aFind = array('&', 'á', 'à', 'ã', 'â', 'é', 'ê',
        'í', 'ó', 'ô', 'õ', 'ú', 'ü', 'ç', 'Á', 'À', 'Ã', 'Â',
        'É', 'Ê', 'Í', 'Ó', 'Ô', 'Õ', 'Ú', 'Ü', 'Ç');
    $aSubs = array('e', 'a', 'a', 'a', 'a', 'e', 'e',
        'i', 'o', 'o', 'o', 'u', 'u', 'c', 'A', 'A', 'A', 'A',
        'E', 'E', 'I', 'O', 'O', 'O', 'U', 'U', 'C');
    $novoTexto = str_replace($aFind, $aSubs, $texto);
    $novoTexto = preg_replace("/[^a-zA-Z0-9 @,-.;:\/_]/", "", $novoTexto);
    return $novoTexto;
}

//function removeAcentos($nome) {
//    $nome = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $nome));
//    return $nome;
//}


function BuscaIso($valor) {
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'state');
    $sql = "SELECT * from state WHERE letter = '$valor'";
    $arrayParam = '';
    $returnstate = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
    $pdo = null;
    $curd = null;

    return $returnstate->iso;
}

$expor = '';


$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'mdfe');
$sql = "SELECT * from mdfe where nMDF ='$nMDFEbd'";
$arrayParam = '';
$dadosmdfe = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'emitente');
$sql = "SELECT * from emitente where emitenteid = '1'";
$arrayParam = '';
$resultemi = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
$pdo = null;
$curd = null;

$datanf = $dadosmdfe->dhEmi . " " . $dadosmdfe->hEmi;
$dataSnf = $dadosmdfe->dhIniViagem . " " . $dadosmdfe->hIniViagem;

$dhEmi = parseDate($datanf, "Y-m-d\TH:i:s"); //Formato: “AAAA-MM-DDThh:mm:ssTZD” (UTC - Universal Coordinated Time).
$dhIniViagem = parseDate($dataSnf, "Y-m-d\TH:i:s"); //Não informar este campo para a NFC-e.


$expor .= 'REGISTROSMDFE|1';
$expor .= "\r\n";
$expor .= 'MDFE|3.00||';
$expor .= "\r\n";

//gera ide
$expor .= 'ide|';
$expor .= Buscaiso($resultemi->UF) . '|';
$expor .= '|';
$expor .= $dadosmdfe->tpEmit . '|';
$expor .= $dadosmdfe->modelo . '|';
$expor .= $dadosmdfe->serie . '|';
$expor .= $dadosmdfe->nMDF . '|';
$expor .= '|';
$expor .= '|';
$expor .= $dadosmdfe->modal . '|';
$expor .= $dhEmi . '-03:00|';
$expor .= $dadosmdfe->tpEmis . '|';
$expor .= '0|';
$expor .= '1.0.0|';
$expor .= $dadosmdfe->UFIni . '|';
$expor .= $dadosmdfe->UFFim . '|';
$expor .= '1|';
$expor .= "\r\n";


//gera MunCarrega
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'muncarregamdfe');
$sql = "SELECT * from muncarregamdfe where nMDFE = {$nMDFEbd}";
$arrayParam = '';
$resultMunCarrega = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($resultMunCarrega as $MunCarrega) :
    $expor .= 'infMunCarrega|';
    $expor .= $MunCarrega->cMunCarrega . '|';
    $expor .= removeAcentos($MunCarrega->xMunCarrega);
    $expor .= "\r\n";
endforeach;

//gera InfPercurso
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'percursomdfe');
$sql = "SELECT * from percursomdfe where nMDFE = {$nMDFEbd}";
$arrayParam = '';
$resultInfPercurso = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($resultInfPercurso as $InfPercurso) :
    $expor .= 'infPercurso|';
    $expor .= $InfPercurso->UFPer;
    $expor .= "\r\n";
endforeach;


//Gera Emitente
$expor .= 'emit|';
$expor .= $resultemi->CNPJ . '|';
$expor .= $resultemi->IE . '|';
$expor .= removeAcentos($resultemi->xNome);
$expor .= "\r\n";

$expor .= 'enderEmit|';
$expor .= removeAcentos($resultemi->xLgr) . '|';
$expor .= $resultemi->nro . '|';
$expor .= removeAcentos($resultemi->xCpl) . '|';
$expor .= removeAcentos($resultemi->xBairro) . '|';
$expor .= removeAcentos($resultemi->cMun) . '|';
$expor .= removeAcentos($resultemi->xMun) . '|';
$expor .= $resultemi->CEP . '|';
$expor .= $resultemi->UF;
$expor .= "\r\n";

//Gera Modal
$expor .= 'infModal|';
$expor .= '1.00';
$expor .= "\r\n";


if ($dadosmdfe->modal == '1') :


//Gera rodo
    $expor .= 'rodo|';
    $expor .= "\r\n";


    $expor .= 'infANTT|';
    $expor .= $dadosmdfe->RNTRC . '|';
    $expor .= $dadosmdfe->CIOT;
//    $expor .= '00496586000631|';
    // $expor .= $dadosmdfe->CIOT;
    $expor .= "\r\n";


    $expor .= 'infContratante|';
    $expor .= $dadosmdfe->CnpjContratante . '|';
    $expor .= "\r\n";

//Gera VeicTracao
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'veiculo');
    $sql = "SELECT * from veiculo Where placa ='$dadosmdfe->veiculo'";
    $arrayParam = '';
    $dadosveic = $crud->getSQLGeneric($sql, $arrayParam, FALSE);
    $pdo = null;
    $curd = null;



    $expor .= 'veicTracao|';
    $expor .= $dadosveic->cint . '|';
    $expor .= $dadosveic->placa . '|';
    $expor .= $dadosveic->renavam . '|';
    $expor .= $dadosveic->tara . '|';
    $expor .= $dadosveic->capkg . '|';
    $expor .= $dadosveic->capm3 . '|';
    if ($dadosveic->tprod == '00') :
        $expor .= '06|';
    else :
        $expor .= $dadosveic->tprod . '|';
    endif;
    $expor .= $dadosveic->tpcar . '|';
    $expor .= $dadosveic->uf . '|';
    $expor .= "\r\n";

    if ($dadosveic->tpprop == 'T') :
        $expor .= 'prop|';
        $qtsdocveic = strlen($dadosveic->cnpj);
        if ($qtsdocveic <= '11') :
            $expor .= '|';
            $expor .= $dadosveic->cnpj . '|';
            $ie = '';
        else :
            $expor .= $dadosveic->cnpj . '|';
            $expor .= '|';
            $ie = $dadosveic->ie . '|';
        endif;
        $expor .= $dadosveic->rntrc . '|';
        $expor .= $dadosveic->xnome . '|';
        if ($ie != '') :
            $expor .= $dadosveic->ie . '|';
        else :
            $expor .= 'ISENTO|';
        endif;
        $expor .= $dadosveic->propuf . '|';
        $expor .= $dadosveic->proptpprop . '|';
        $expor .= "\r\n";
    endif;

//Gera Motorista
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'motoristamdfe');
    $sql = "SELECT * from motoristamdfe WHERE nMDFE ='$nMDFEbd'";
    $arrayParam = '';
    $motorista = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;


    foreach ($motorista as $row) :
        $expor .= 'condutor|';
        $expor .= $row->motnome . '|';
        $expor .= $row->motcpf . '|';
        $expor .= "\r\n";
    endforeach;

//Gera Veic Reboque
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'veiculo');
    $sql = "SELECT * from veiculo WHERE veiculo ='$dadosveic->placa'";
    $arrayParam = '';
    $veic = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;

    foreach ($veic as $row) :
        $expor .= 'veicReboque|';
        $expor .= $row->cint . '|';
        $expor .= $row->placa . '|';
        $expor .= $row->renavam . '|';
        $expor .= $row->tara . '|';
        $expor .= $row->capkg . '|';
        $expor .= $row->capm3 . '|';
        if ($row->tprod == '00') :
            $expor .= '06|';
        else :
            $expor .= $row->tprod . '|';
        endif;
        $expor .= $row->tpcar . '|';
        $expor .= $row->uf . '|';
        $expor .= "\r\n";

        if ($row->tpprop == 'T') :
            $expor .= 'propReboque|';
            $qtsdocveic = strlen($row->cnpj);
            if ($qtsdocveic <= '11') :
                $expor .= '|';
                $expor .= $row->cnpj . '|';
                $ie = '';
            else :
                $expor .= $row->cnpj . '|';
                $expor .= '|';
                $ie = $row->ie . '|';
            endif;
            $expor .= $row->rntrc . '|';
            $expor .= $row->xnome . '|';
            if ($ie != '') :
                $expor .= $row->ie . '|';
            else :
                $expor .= 'ISENTO|';
            endif;
            $expor .= $row->propuf . '|';
            $expor .= $row->proptpprop . '|';
            $expor .= "\r\n";
        endif;
    endforeach;

else :
    $expor .= 'aquav|';
    if ($dadosmdfe->irin == '00000000') :
        $expor .= '|';
    else :
        $expor .= $dadosmdfe->irin . '|';
    endif;
    $expor .= $dadosmdfe->tpEmb . '|';
    $expor .= $dadosmdfe->cEmbar . '|';
    $expor .= $dadosmdfe->xEmbar . '|';
    $expor .= $dadosmdfe->nViag . '|';
    $expor .= $dadosmdfe->cPrtEmb . '|';
    $expor .= $dadosmdfe->cPrtDest . '|';
    $expor .= $dadosmdfe->prtTrans . '|';
    $expor .= $dadosmdfe->tpNav . '|';
    $expor .= $dadosmdfe->CNPJAgeNav . '|';
    $expor .= "\r\n";


//    Gera infTermCarreg  
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'infTermCarreg');
    $sql = "SELECT * from infTermCarreg where nMDF = {$nMDFEbd}";
    $arrayParam = '';
    $resultinfTermCarreg = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;

    foreach ($resultinfTermCarreg as $infTermCarreg) :
        $expor .= 'infTermCarreg|';
        $expor .= $infTermCarreg->cTermCarreg . '|';
        $expor .= $infTermCarreg->xTermCarreg . '|';
        $expor .= "\r\n";
    endforeach;

//    Gera infTermDescarreg 
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'infTermDescarreg');
    $sql = "SELECT * from infTermDescarreg where nMDF = {$nMDFEbd}";
    $arrayParam = '';
    $resultinfTermDescarreg = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;

    foreach ($resultinfTermDescarreg as $infTermDescarreg) :
        $expor .= 'infTermDescarreg|';
        $expor .= $infTermDescarreg->cTermDescarreg . '|';
        $expor .= $infTermDescarreg->xTermDescarreg . '|';
        $expor .= "\r\n";
    endforeach;


//    Gera infEmbComb 
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'infEmbComb');
    $sql = "SELECT * from infEmbComb where nMDF = {$nMDFEbd}";
    $arrayParam = '';
    $resultinfEmbComb = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;

    foreach ($resultinfEmbComb as $infEmbComb) :
        $expor .= 'infEmbComb|';
        $expor .= $infEmbComb->cEmbComb . '|';
        $expor .= $infEmbComb->xBalsa . '|';
        $expor .= "\r\n";
    endforeach;


//    Gera infEmbComb 
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'infUnidCargaVazia');
    $sql = "SELECT * from infUnidCargaVazia where nMDF = {$nMDFEbd}";
    $arrayParam = '';
    $resultinfUnidCargaVazia = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;

    foreach ($resultinfUnidCargaVazia as $infUnidCargaVazia) :
        $expor .= 'infUnidCargaVazia|';
        $expor .= $infUnidCargaVazia->idUnidCargaVazia . '|';
        $expor .= $infUnidCargaVazia->tpUnidCargaVazia . '|';
        $expor .= "\r\n";
    endforeach;


//    Gera infEmbComb 
    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'infUnidTranspVazia');
    $sql = "SELECT * from infUnidTranspVazia where nMDF = {$nMDFEbd}";
    $arrayParam = '';
    $resultinfUnidTranspVazia = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
    $pdo = null;
    $curd = null;

    foreach ($resultinfUnidTranspVazia as $infUnidTranspVazia) :
        $expor .= 'infUnidTranspVazia|';
        $expor .= $infUnidTranspVazia->idUnidTranspVazia . '|';
        $expor .= $infUnidTranspVazia->tpUnidTranspVazia . '|';
        $expor .= "\r\n";
    endforeach;


endif;



//gera MunDescarga
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'mundescargamdfe');
$sql = "SELECT * from mundescargamdfe where nMDFE = {$nMDFEbd} order by mundescargamdfeid desc LIMIT 1";
$arrayParam = '';
$resultMunDescarga = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($resultMunDescarga as $MunDescarga) :
    $expor .= 'infMunDescarga|';
    $expor .= $MunDescarga->cMunDescarga . '|';
    $expor .= $MunDescarga->xMunDescarga;
    $expor .= "\r\n";
endforeach;

//Gera infNFe
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'nfemdfe');
$sql = "SELECT * from nfemdfe where nMDFE = {$nMDFEbd}";
$arrayParam = '';
$resultNFe = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($resultNFe as $NFe) :
    $expor .= 'infNFe|';
    $expor .= $NFe->chavenfe . '|';
    $expor .= "\r\n";
endforeach;

//Gera infCTe
$pdo = Conexao::getInstance();
$crud = Crud::getInstance($pdo, 'ctemdfe');
$sql = "SELECT * from ctemdfe where nMDFE = {$nMDFEbd}";
$arrayParam = '';
$resultCTe = $crud->getSQLGeneric($sql, $arrayParam, TRUE);
$pdo = null;
$curd = null;

foreach ($resultCTe as $CTe) :
    $expor .= 'infCTe|';
    $expor .= $CTe->chavecte . '|';
    $expor .= "\r\n";
endforeach;

if ($dadosmdfe->qCTe == 0) :
    $qcte = '';
else :
    $qcte = $dadosmdfe->qCTe;
endif;

//if($dadosmdfe->qNFe == 0) :
//    $qnfe = '';
//else :
//    $qnfe = '';
//endif;
//Gera tot
$expor .= 'seg|';
$expor .= $dadosmdfe->nApol . '|';
$expor .= $dadosmdfe->nAver . '|';
$expor .= "\r\n";


$expor .= 'infResp|';
$expor .= $dadosmdfe->respSeg . '|';
$totalrespseg = strlen($dadosmdfe->CnpjrespSeg);
if ($totalrespseg > 11) :
    $expor .= $dadosmdfe->CnpjrespSeg . '|';
    $expor .= '|';
else :
    $expor .= '|';
    $expor .= $dadosmdfe->CnpjrespSeg . '|';
endif;
$expor .= "\r\n";

$expor .= 'infSeg|';
$expor .= $dadosmdfe->xSeg . '|';
$expor .= $dadosmdfe->CNPJxSeg . '|';
$expor .= "\r\n";



$expor .= 'tot|';
$expor .= $qcte . '|';
$expor .= $dadosmdfe->qNFe . '|';
$expor .= '|';
$expor .= $dadosmdfe->vCarga . '|';
$expor .= $dadosmdfe->cUnid . '|';
$expor .= $dadosmdfe->qCarga . '|';

include_once '../bootstrap.php';

use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $mdfeTools->aConfig['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;


$arquivo = fopen("../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/txt/" . $dadosmdfe->nMDF . ".txt", "w");
$texto = $expor;
fwrite($arquivo, $texto);
fclose($arquivo);

$arr = array("Status" => 'Ok');
echo json_encode($arr);
