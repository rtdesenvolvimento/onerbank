<?php
if (!isset($_SESSION)):
    session_start();
endif;
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include_once '../bootstrap.php';

use NFePHP\MDFe\Tools;

$mdfeTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $mdfeTools->aConfig['tpAmb'];
if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;

$filename = "../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/entradas/" . $_GET['arquivo'] . ".xml";


if (!$mdfeTools->validarXml($filename) || sizeof($mdfeTools->errors)) {
    $erros = array();
    foreach ($mdfeTools->errors as $erro) {
        $erros[] = $erro;
    }
    $arr = array("Status" => 'Erro', "Erro" => $erros);
    echo json_encode($arr);
    exit;
}

$arr = array("Status" => 'Ok');

echo json_encode($arr);

