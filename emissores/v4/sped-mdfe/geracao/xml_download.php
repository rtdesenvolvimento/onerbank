<?php

require_once "../../_backend/_class/Conexao_class.php";
require_once "../../_backend/_class/Crud_class.php";
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once '../bootstrap.php';

use NFePHP\MDFe\Make;
use NFePHP\MDFe\Tools;

$cteTools = new Tools('../../nfephp-master/config/' . $_SESSION['BD'] . '/config.json');

$tpAmb = $cteTools->aConfig['tpAmb'];

if ($tpAmb == '1') :
    $pastaxml = "producao";
else :
    $pastaxml = "homologacao";
endif;


if (isset($_GET['arquivo'])) :
    $nMDF = $_GET['arquivo'];

    $pdo = Conexao::getInstance();
    $crud = Crud::getInstance($pdo, 'nfe');

    $sql = "SELECT status,dhEmi, chavemdfe from mdfe where nMDF='$nMDF'";
    $arrayParam = '';
    $dadoscte = $crud->getSQLGeneric($sql, $arrayParam, FALSE);

    $pasta = parseDate($dadoscte->dhEmi, "Ym");

    if ($dadoscte->status == '101') :
        $local = "../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/canceladas/{$pasta}/";
    else :
        $local = "../XML/{$_SESSION['BD']}/MDFe/{$pastaxml}/enviadas/aprovadas/{$pasta}/";
    endif;

    $arquivo = '' . $dadoscte->chavemdfe . '-protMDFe.xml'; // Nome do Arquivo

    $local_arquivio = $local . $arquivo; // Concatena o diret�rio com o nome do arquivo

else :
    $zip = new ZipArchive();
     if ($zip->open("{$cteTools->aConfig['pathMDFeFiles']}/{$pastaxml}/{$_GET['periodo']}.zip", ZipArchive::CREATE) === true) {

        foreach (glob("{$cteTools->aConfig['pathMDFeFiles']}/{$pastaxml}/enviadas/aprovadas/{$_GET['periodo']}/*.*") as $current)
            $zip->addFile($current, 'Aprovadas/'.basename($current));
        
        foreach (glob("{$cteTools->aConfig['pathMDFeFiles']}/{$pastaxml}/pdf/{$_GET['periodo']}/*.*") as $current)
            $zip->addFile($current, 'PDF/'.basename($current));
        
        foreach (glob("{$cteTools->aConfig['pathMDFeFiles']}/{$pastaxml}/canceladas/{$_GET['periodo']}/*.*") as $current)
            $zip->addFile($current, 'Canceladas/'.basename($current));
        $zip->close();
    }

    $arquivo = "{$cteTools->aConfig['pathMDFeFiles']}/{$pastaxml}/{$_GET['periodo']}.zip"; // Nome do Arquivo

    $local_arquivio = $arquivo; // Concatena o diret�rio com o nome do arquivo
endif;

if (stripos($arquivo, './') !== false || stripos($arquivo, '../') !== false || !file_exists($local_arquivio)) {
    echo 'O comando não pode ser executado.';
} else {
    header('Cache-control: private');
    header('Content-Type: application/octet-stream');
    header('Content-Length: ' . filesize($local_arquivio));
    header('Content-Disposition: filename=' . $arquivo);
    header("Content-Disposition: attachment; filename=" . basename($local_arquivio));

    // Envia o arquivo Download
    readfile($local_arquivio);
}
?>