<?php

/**
 * Este arquivo é parte do projeto NFFePHP - Nota Fiscal eletrônica em PHP.
 *
 * Este programa é um software livre: você pode redistribuir e/ou modificá-lo
 * sob os termos da Licença Pública Geral GNU como é publicada pela Fundação
 * para o Software Livre, na versão 3 da licença, ou qualquer versão posterior.
 * e/ou
 * sob os termos da Licença Pública Geral Menor GNU (LGPL) como é publicada pela
 * Fundação para o Software Livre, na versão 3 da licença, ou qualquer versão posterior.
 *
 * Este programa é distribuído na esperança que será útil, mas SEM NENHUMA
 * GARANTIA; nem mesmo a garantia explícita definida por qualquer VALOR COMERCIAL
 * ou de ADEQUAÇÃO PARA UM PROPÓSITO EM PARTICULAR,
 * veja a Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Publica GNU e da
 * Licença Pública Geral Menor GNU (LGPL) junto com este programa.
 * Caso contrário consulte
 * <http://www.fsfla.org/svnwiki/trad/GPLv3>
 * ou
 * <http://www.fsfla.org/svnwiki/trad/LGPLv3>.
 * 
 * Esta classe atende aos critérios estabelecidos no
 * Manual de Importação/Exportação TXT Notas Fiscais eletrônicas versão 2.0.0
 *
 * @package     NFFePHP
 * @name        ConvertMDFePHP
 * @version     1.0.0
 * @license     http://www.gnu.org/licenses/gpl.html GNU/GPL v.3
 * @license     http://www.gnu.org/licenses/lgpl.html GNU/LGPL v.3
 * @copyright   2009-2014 &copy; MDFePHP
 * @link        http://www.nfephp.org/
 * @author      Roberto L. Machado <linux.rlm at gmail dot com>
 * @author      Leandro C. Lopez <leandro.castoldi at gmail dot com>
 *
 *
 *        CONTRIBUIDORES (em ordem alfabetica):
 *
 */
class ConvertMDFePHP {

    /**
     * xml
     * XML da MDFe
     * @var string 
     */
    public $xml = '';

    /**
     * chave
     * ID da MDFe 44 digitos
     * @var string 
     */
    public $chave = '';

    /**
     * txt
     * @var string TXT com MDFe
     */
    public $txt = '';

    /**
     * errMsg
     * Mensagens de erro do API
     * @var string
     */
    public $errMsg = '';

    /**
     * errStatus
     * Status de erro
     * @var boolean
     */
    public $errStatus = false;

    /**
     * tpAmb
     * Tipo de ambiente
     * @var string
     */
    public $tpAmb = '';

    /**
     * $tpEmis
     * Tipo de emissão
     * @var string
     */
    public $tpEmis = '';

    /**
     * limpar_string
     * Se for = true remove caracteres especiais na conversão de TXT pra XML
     * @var boolean
     */
    public $limpar_string = true;

    /**
     * __contruct
     * Método contrutor da classe
     *
     * @package NFePHP
     * @name __contruct
     * @version .0
     * @param boolean $limpar_string Ativa flag para limpar os caracteres especiais e acentos
     * @return none
     */
    function __construct($limpar_string = true) {
        $this->limpar_string = $limpar_string;
    }

    /**
     * MDFetxt2xml
     * Converte o arquivo txt em um array para ser mais facilmente tratado
     *
     * @name MDFetxt2xml
     * @param mixed $txt Path para o arquivo txt, array ou o conteudo do txt em uma string
     * @return string xml construido
     */
    public function MDFetxt2xml($txt, $tpAmb, $tipEmiss) {
        if (is_file($txt)) {
            $aDados = file($txt, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES | FILE_TEXT);
        } else {
            if (is_array($txt)) {
                $aDados = $txt;
            } else {
                if (strlen($txt) > 0) {
                    $aDados = explode("\n", $txt);
                }
            }
        }
        return $this->MDFeTxt2XmlArrayComLinhas($aDados, $tpAmb, $tipEmiss);
    }

//fim MDFetxt2xml

    /**
     * MDFeTxt2XmlArrayComLinhas
     * Método de conversão das MDFe de txt para xml, conforme
     * especificações do Manual de Importação/Exportação TXT
     * Notas Fiscais eletrônicas versão 2.0.0 (24/08/2010)
     *
     * @name MDFeTxt2XmlArrayComLinhas
     * @param string $arrayComAsLinhasDoArquivo Array de Strings onde cada elemento é uma linha do arquivo
     * @return string xml construido
     */
    protected function MDFeTxt2XmlArrayComLinhas($arrayComAsLinhasDoArquivo, $tpAmb, $tipEmiss) {
        $arquivo = $arrayComAsLinhasDoArquivo;
        $notas = array();
        $currnota = -1;


//lê linha por linha do arquivo txt
        for ($l = 0; $l < count($arquivo); $l++) {
//separa os elementos do arquivo txt usando o pipe "|"
            $dados = explode("|", $arquivo[$l]);
//remove todos os espaços adicionais, tabs, linefeed, e CR
//de todos os campos de dados retirados do TXT
            for ($x = 0; $x < count($dados); $x++) {
                if (!empty($dados[$x])) {
                    $dados[$x] = trim(preg_replace('/\s\s+/', " ", $dados[$x]));
                    if ($this->limpar_string) {
                        $dados[$x] = $this->__limpaString($dados[$x]);
                    }
                } //end if
            } //end for
//monta o dado conforme o tipo, inicia lendo o primeiro campo da matriz
            switch ($dados[0]) {
                case "REGISTROSMDFE":
                    break;
                case "MDFE":
                    $currnota++;
//                    unset($dom, $MDFe, $infMDFe);
/// limpar todas variaveis utilizadas por cada MDFe....    
                    unset($dom, $MDFe, $infMDFe, $ide, $nMDF, $cMDF, $cDV, $dhEmi, $tpEmis, $verProc, $UFIni, $UFFim, $UFPer, $cMunDescarga, $xMunDescarga, $chMDFe, $SegCodBarra, $tpUnidTransp, $idUnidTransp, $placa, $tara, $tpRod, $tpCar, $UF, $nome, $cpf, $placa, $tara, $capKG, $tpCar, $UF, $tpRod, $qMDFe, $vCarga, $cUnid, $qCarga);

                    $this->chave = '';
                    $this->tpAmb = $tpAmb;
                    $this->xml = '';
                    $this->tpEmis = $tipEmiss;

                    $notas[$currnota] = array(
                        'dom' => false,
                        'MDFe' => false,
                        'infMDFe' => false,
                        'chave' => '',
                        'tpAmb' => $this->tpAmb);

//cria o objeto DOM para o xml
                    $notas[$currnota]['dom'] = new DOMDocument('1.0', 'UTF-8');
                    $dom = &$notas[$currnota]['dom'];
                    $dom->formatOutput = True;
                    $dom->preserveWhiteSpace = false;
                    $notas[$currnota]['MDFe'] = $dom->createElement("MDFe");
                    $MDFe = &$notas[$currnota]['MDFe'];
                    $MDFe->setAttribute("xmlns", "http://www.portalfiscal.inf.br/mdfe");
                    $notas[$currnota]['infMDFe'] = $dom->createElement("infMDFe");
                    $infMDFe = &$notas[$currnota]['infMDFe'];
                    $infMDFe->setAttribute("Id", $dados[2]);
                    $infMDFe->setAttribute("versao", '3.10');
//pega a chave de 44 digitos excluindo o a sigla MDFe
                    $this->chave = substr($dados[2], 3, 44);
                    $notas[$currnota]['chave'] = $this->chave;
                    break;
                case "ide": //identificadores [infMDFe]
//infMDFe => ide|cUF|tpAmb|tpEmit|mod|serie|nMDF|cMDF|cDV|modal|dhEmi|tpEmis|procEmi|verProc|UFIni|UFFim|tpTransp
                    $ide = $dom->createElement("ide");
                    $cUF = $dom->createElement("cUF", $dados[1]);
                    $ide->appendChild($cUF);
                    $tpAmb = $dom->createElement("tpAmb", $this->tpAmb);
                    $ide->appendChild($tpAmb);
                    $tpEmit = $dom->createElement("tpEmit", $dados[3]);
                    $ide->appendChild($tpEmit);
                    $tpTransp = $dom->createElement("tpTransp", $dados[16]);
                    $ide->appendChild($tpTransp);
                    $mod = $dom->createElement("mod", $dados[4]);
                    $ide->appendChild($mod);
                    $serie = $dom->createElement("serie", (int) $dados[5]);
                    $ide->appendChild($serie);
                    $nMDF = $dom->createElement("nMDF", $dados[6]);
                    $ide->appendChild($nMDF);
                    $cMDF = $dom->createElement("cMDF", $dados[7]);
                    $ide->appendChild($cMDF);
                    $cDV = $dom->createElement("cDV", $dados[8]);
                    $ide->appendChild($cDV);
                    $modal = $dom->createElement("modal", $dados[9]);
                    $ide->appendChild($modal);
                    $dhEmi = $dom->createElement("dhEmi", $dados[10]);
                    $ide->appendChild($dhEmi);
                    $tpEmis = $dom->createElement("tpEmis", $this->tpEmis);
                    $ide->appendChild($tpEmis);
                    $procEmi = $dom->createElement("procEmi", $dados[12]);
                    $ide->appendChild($procEmi);
                    $verProc = $dom->createElement("verProc", $dados[13]);
                    $ide->appendChild($verProc);
                    $UFIni = $dom->createElement("UFIni", $dados[14]);
                    $ide->appendChild($UFIni);
                    $UFFim = $dom->createElement("UFFim", $dados[15]);
                    $ide->appendChild($UFFim);
                    $infMDFe->appendChild($ide);

                    $dhIniViagem = $dom->createElement("dhIniViagem", $dados[10]);
                    $ide->appendChild($dhIniViagem);
                    break;
                case "infMunCarrega":
// infMDFe => ide => infMunCarrega|cMunCarrega|xMunCarrega
//                    if (!isset($infMunCarrega)) {
                    $notas[$currnota]['infMunCarrega'] = $dom->createElement("infMunCarrega");
                    $infMunCarrega = & $notas[$currnota]['infMunCarrega'];
                    $ide->appendChild($infMunCarrega);

                    $cMunCarrega = $dom->createElement("cMunCarrega", $dados[1]);
                    $infMunCarrega->appendChild($cMunCarrega);
                    $xMunCarrega = $dom->createElement("xMunCarrega", $dados[2]);
                    $infMunCarrega->appendChild($xMunCarrega);
                    $ide->insertBefore($ide->appendChild($infMunCarrega), $dhIniViagem);
                    break;
                case "infPercurso":
// infMDFe => ide => infPercurso|UFPer
                    $infPercurso = $dom->createElement("infPercurso");
                    $ide->insertBefore($ide->appendChild($infPercurso), $infMunCarrega);
                    $ide->appendChild($infPercurso);

                    $UFPer = $dom->createElement("UFPer", $dados[1]);
                    $infPercurso->appendChild($UFPer);
                    break;
                case "emit":
// infMDFe => emit|CNPJ|IE|xNome|
                    $emit = $dom->createElement("emit");
                    $cnpj = $dom->createElement("CNPJ", $dados[1]);
                    $emit->appendChild($cnpj);
                    $IE = $dom->createElement("IE", $dados[2]);
                    $emit->appendChild($IE);
                    $xNome = $dom->createElement("xNome", $dados[3]);
                    $emit->appendChild($xNome);

                    $infMDFe->appendChild($emit);
                    break;
                case "enderEmit":
// infMDFe => emit => enderEmit|xLgr|nro|xCpl|xBairro|cMun|xMun|CEP|UF
                    $enderEmi = $dom->createElement("enderEmit");
                    $xLgr = $dom->createElement("xLgr", $dados[1]);
                    $enderEmi->appendChild($xLgr);
                    $dados[2] = abs((int) $dados[2]);
                    $nro = $dom->createElement("nro", $dados[2]);
                    $enderEmi->appendChild($nro);
                    if (!empty($dados[3])) {
                        $xCpl = $dom->createElement("xCpl", $dados[3]);
                        $enderEmi->appendChild($xCpl);
                    }
                    $xBairro = $dom->createElement("xBairro", $dados[4]);
                    $enderEmi->appendChild($xBairro);
                    $cMun = $dom->createElement("cMun", $dados[5]);
                    $enderEmi->appendChild($cMun);
                    $xMun = $dom->createElement("xMun", $dados[6]);
                    $enderEmi->appendChild($xMun);
                    if (!empty($dados[7])) {
                        $CEP = $dom->createElement("CEP", $dados[7]);
                        $enderEmi->appendChild($CEP);
                    }
                    $UF = $dom->createElement("UF", $dados[8]);
                    $enderEmi->appendChild($UF);

//$emit->insertBefore($emit->appendChild($enderEmi), $xNome);

                    $emit->appendChild($enderEmi);
                    break;
                case "infModal":
// infMDFe => infModal|versaoModal
                    $infModal = $dom->createElement("infModal");
                    $infModal->setAttribute("versaoModal", '3.00');

                    $infMDFe->appendChild($infModal);
                    break;
//               
                case "rodo":
// rodo|RNTRC|CIOT|
                    $rodo = $dom->createElement("rodo");
                    $rodo->setAttribute("xmlns", 'http://www.portalfiscal.inf.br/mdfe');
//                    $rntrc = $dom->createElement("RNTRC", $dados[1]);
//                    $rodo->appendChild($rntrc);
//                    if ($dados[2] != '') :
//                        $ciot = $dom->createElement("CIOT", $dados[2]);
//                        $rodo->appendChild($ciot);
//                    endif;
                    $infModal->appendChild($rodo);

                    break;
                case "infANTT":
                    $infANTT = $dom->createElement("infANTT");
                    $rntrc = $dom->createElement("RNTRC", $dados[1]);
                    $infANTT->appendChild($rntrc);
//                    $CPF = $dom->createElement("CPF", $dados[2]);
//                    $condutor->appendChild($CPF);
                    $rodo->appendChild($infANTT);
                    break;

                case "infContratante":
//                    infContratante|CNPJ
                    $infContratante = $dom->createElement("infContratante");
                    $CNPJ = $dom->createElement("CNPJ", $dados[1]);
                    $infContratante->appendChild($CNPJ);

                    $infANTT->appendChild($infContratante);

                    break;
                case "veicTracao":
// infMDFe => infModal => rodo => veicTracao|cInt|placa|RENAVAM|tara|capKG|capM3|tpRod|tpCar|UF
//                    $rodo = $dom->createElement("rodo");
//                    $infModal->appendChild($rodo);
//                    
                    $veicTracao = $dom->createElement("veicTracao");
//                    $cInt = $dom->createElement("cInt", $dados[1]);
//                    $veicTracao->appendChild($cInt);
                    $placa = $dom->createElement("placa", $dados[2]);
                    $veicTracao->appendChild($placa);
                    $RENAVAM = $dom->createElement("RENAVAM", $dados[3]);
                    $veicTracao->appendChild($RENAVAM);
                    $tara = $dom->createElement("tara", $dados[4]);
                    $veicTracao->appendChild($tara);
                    $capKG = $dom->createElement("capKG", $dados[5]);
                    $veicTracao->appendChild($capKG);
                    $capM3 = $dom->createElement("capM3", $dados[6]);
                    $veicTracao->appendChild($capM3);
                    $tpRod = $dom->createElement("tpRod", $dados[7]);
                    $veicTracao->appendChild($tpRod);
                    $tpCar = $dom->createElement("tpCar", $dados[8]);
                    $veicTracao->appendChild($tpCar);
                    $UF = $dom->createElement("UF", $dados[9]);
                    $veicTracao->appendChild($UF);

                    $rodo->appendChild($veicTracao);
                    break;
                case "prop":
                    $prop = $dom->createElement("prop");
                    if (!empty($dados[1])) {
                        $CNPJ = $dom->createElement("CNPJ", $dados[1]);
                        $prop->appendChild($CNPJ);
                    } else {
                        $CPF = $dom->createElement("CPF", $dados[2]);
                        $prop->appendChild($CPF);
                    }
                    $RNTRC = $dom->createElement("RNTRC", $dados[3]);
                    $prop->appendChild($RNTRC);
                    $xNome = $dom->createElement("xNome", $dados[4]);
                    $prop->appendChild($xNome);
                    if (!empty($dados[5])) {
                        $IE = $dom->createElement("IE", $dados[5]);
                        $prop->appendChild($IE);
                    }
                    if (!empty($dados[6])) {
                        $UF = $dom->createElement("UF", $dados[6]);
                        $prop->appendChild($UF);
                    }
                    $tpProp = $dom->createElement("tpProp", $dados[7]);
                    $prop->appendChild($tpProp);

                    $veicTracao->insertBefore($veicTracao->appendChild($prop), $tpRod);
//                    $veicTracao->appendChild($prop);
                    break;
                case "condutor":
// infMDFe => infModal => rodo => veicTracao => condutor|xNome|CPF
                    $condutor = $dom->createElement("condutor");
                    $xNome = $dom->createElement("xNome", $dados[1]);
                    $condutor->appendChild($xNome);
                    $CPF = $dom->createElement("CPF", $dados[2]);
                    $condutor->appendChild($CPF);

                    $veicTracao->insertBefore($veicTracao->appendChild($condutor), $tpRod);
                    break;
                case "veicReboque":
// infMDFe => infModal => rodo => veicReboque|placa|tara|capKG|tpCar|UF
                    $veicReboque = $dom->createElement("veicReboque");
//                    s
                    $placa = $dom->createElement("placa", $dados[2]);
                    $veicReboque->appendChild($placa);
                    $RENAVAM = $dom->createElement("RENAVAM", $dados[3]);
                    $veicReboque->appendChild($RENAVAM);
                    $tara = $dom->createElement("tara", $dados[4]);
                    $veicReboque->appendChild($tara);
                    $capKG = $dom->createElement("capKG", $dados[5]);
                    $veicReboque->appendChild($capKG);
                    $capM3 = $dom->createElement("capM3", $dados[6]);
                    $veicReboque->appendChild($capM3);
                    $tpCar = $dom->createElement("tpCar", $dados[8]);
                    $veicReboque->appendChild($tpCar);
                    $UF = $dom->createElement("UF", $dados[9]);
                    $veicReboque->appendChild($UF);

                    $rodo->appendChild($veicReboque);
                    break;
                case "propReboque":
                    $prop = $dom->createElement("prop");
                    if (!empty($dados[1])) {
                        $CNPJ = $dom->createElement("CNPJ", $dados[1]);
                        $prop->appendChild($CNPJ);
                    } else {
                        $CPF = $dom->createElement("CPF", $dados[2]);
                        $prop->appendChild($CPF);
                    }
                    $RNTRC = $dom->createElement("RNTRC", $dados[3]);
                    $prop->appendChild($RNTRC);
                    $xNome = $dom->createElement("xNome", $dados[4]);
                    $prop->appendChild($xNome);
                    if (!empty($dados[5])) {
                        $IE = $dom->createElement("IE", $dados[5]);
                        $prop->appendChild($IE);
                    }
                    if (!empty($dados[6])) {
                        $UF = $dom->createElement("UF", $dados[6]);
                        $prop->appendChild($UF);
                    }
                    $tpProp = $dom->createElement("tpProp", $dados[7]);
                    $prop->appendChild($tpProp);

                    $veicReboque->insertBefore($veicReboque->appendChild($prop), $tpRod);
//                    $veicTracao->appendChild($prop);
                    break;
                case "aquav":
                    $aquav = $dom->createElement("aquav");

                    if (!empty($dados[10])) {
                        $CNPJAgeNav = $dom->createElement("CNPJAgeNav", $dados[10]);
                        $aquav->appendChild($CNPJAgeNav);
                    }

                    if (!empty($dados[1])) {
                        $irin = $dom->createElement("irin", $dados[1]);
                        $aquav->appendChild($irin);
                    }

                    $tpEmb = $dom->createElement("tpEmb", $dados[2]);
                    $aquav->appendChild($tpEmb);

                    $cEmbar = $dom->createElement("cEmbar", $dados[3]);
                    $aquav->appendChild($cEmbar);

                    $xEmbar = $dom->createElement("xEmbar", $dados[4]);
                    $aquav->appendChild($xEmbar);

                    $nViag = $dom->createElement("nViag", $dados[5]);
                    $aquav->appendChild($nViag);

                    $cPrtEmb = $dom->createElement("cPrtEmb", $dados[6]);
                    $aquav->appendChild($cPrtEmb);

                    $cPrtDest = $dom->createElement("cPrtDest", $dados[7]);
                    $aquav->appendChild($cPrtDest);

//                    $prtTrans = $dom->createElement("prtTrans", $dados[8]);
//                    $aquav->appendChild($prtTrans);

                    if (!empty($dados[9])) {
                        $tpNav = $dom->createElement("tpNav", $dados[9]);
                        $aquav->appendChild($tpNav);
                    }


                    $infModal->appendChild($aquav);

                    break;
                case "infTermCarreg":
                    $infTermCarreg = $dom->createElement("infTermCarreg");

                    $cTermCarreg = $dom->createElement("cTermCarreg", $dados[1]);
                    $infTermCarreg->appendChild($cTermCarreg);

                    $xTermCarreg = $dom->createElement("xTermCarreg", $dados[2]);
                    $infTermCarreg->appendChild($xTermCarreg);

                    $aquav->appendChild($infTermCarreg);

                    break;
                case "infTermDescarreg":
                    $infTermDescarreg = $dom->createElement("infTermDescarreg");

                    $cTermDescarreg = $dom->createElement("cTermDescarreg", $dados[1]);
                    $infTermDescarreg->appendChild($cTermDescarreg);

                    $xTermDescarreg = $dom->createElement("xTermDescarreg", $dados[2]);
                    $infTermDescarreg->appendChild($xTermDescarreg);

                    $aquav->appendChild($infTermDescarreg);

                    break;
                case "infEmbComb":
                    $infEmbComb = $dom->createElement("infEmbComb");

                    $cEmbComb = $dom->createElement("cEmbComb", $dados[1]);
                    $infEmbComb->appendChild($cEmbComb);

                    $xBalsa = $dom->createElement("xBalsa", $dados[2]);
                    $infEmbComb->appendChild($xBalsa);

                    $aquav->appendChild($infEmbComb);

                    break;
                case "infUnidCargaVazia":
                    $infUnidCargaVazia = $dom->createElement("infUnidCargaVazia");

                    $idUnidCargaVazia = $dom->createElement("idUnidCargaVazia", $dados[1]);
                    $infUnidCargaVazia->appendChild($idUnidCargaVazia);

                    $tpUnidCargaVazia = $dom->createElement("tpUnidCargaVazia", $dados[2]);
                    $infUnidCargaVazia->appendChild($tpUnidCargaVazia);

                    $aquav->appendChild($infUnidCargaVazia);

                    break;
                case "infUnidTranspVazia":
                    $infUnidTranspVazia = $dom->createElement("infUnidTranspVazia");

                    $idUnidTranspVazia = $dom->createElement("idUnidTranspVazia", $dados[1]);
                    $infUnidTranspVazia->appendChild($idUnidTranspVazia);

                    $tpUnidTranspVazia = $dom->createElement("tpUnidTranspVazia", $dados[2]);
                    $infUnidTranspVazia->appendChild($tpUnidTranspVazia);

                    $aquav->appendChild($infUnidTranspVazia);

                    break;
                case "infMunDescarga":
// infMDFe => infDoc => infMunDescarga|cMunDescarga|xMunDescarga
                    if (!isset($infDoc)) {
                        $infDoc = $dom->createElement("infDoc");
//$infMDFe->insertBefore($infMDFe->appendChild($infDoc), $infModal);
                        $infMDFe->appendChild($infDoc);
                    }
                    $infMunDescarga = $dom->createElement("infMunDescarga");
                    $infDoc->appendChild($infMunDescarga);

                    $cMunDescarga = $dom->createElement("cMunDescarga", $dados[1]);
                    $infMunDescarga->appendChild($cMunDescarga);
                    $xMunDescarga = $dom->createElement("xMunDescarga", $dados[2]);
                    $infMunDescarga->appendChild($xMunDescarga);

                    break;
                case "infCTe":
// infMDFe => infDoc => infMunDescarga => infCTe|chCTe|SegCodBarra

                    $infCTe = $dom->createElement("infCTe");
                    $infMunDescarga->appendChild($infCTe);

                    $chCTe = $dom->createElement("chCTe", $dados[1]);
                    $infCTe->appendChild($chCTe);

//                    if (!empty($dados[2])) {
//                        $SegCodBarra = $dom->createElement("SegCodBarra", $dados[2]);
//                        $infNFe->appendChild($SegCodBarra);
//                    }
                    break;
                case "infNFe":
// infMDFe => infDoc => infMunDescarga => infNFe|chNFe|SegCodBarra

                    $infNFe = $dom->createElement("infNFe");
                    $infMunDescarga->appendChild($infNFe);

                    $chNFe = $dom->createElement("chNFe", $dados[1]);
                    $infNFe->appendChild($chNFe);

                    if (!empty($dados[2])) {
                        $SegCodBarra = $dom->createElement("SegCodBarra", $dados[2]);
                        $infNFe->appendChild($SegCodBarra);
                    }
                    break;


                case "seg":
//                    seg|nApol|nAver|
                    $seg = $dom->createElement("seg");

                    $nApol = $dom->createElement("nApol", $dados[1]);
                    $seg->appendChild($nApol);

                    $nAver = $dom->createElement("nAver", $dados[2]);
                    $seg->appendChild($nAver);

                    $infMDFe->appendChild($seg);

                    break;
                case "infResp":
//                    infResp|respSeg|CNPJ|CPF
                    $infResp = $dom->createElement("infResp");
                    $respSeg = $dom->createElement("respSeg", $dados[1]);
                    $infResp->appendChild($respSeg);
                    if (!empty($dados[2])) :
                        $CNPJ = $dom->createElement("CNPJ", $dados[2]);
                        $infResp->appendChild($CNPJ);
                    endif;
                    if (!empty($dados[3])) :
                        $CPF = $dom->createElement("CPF", $dados[3]);
                        $infResp->appendChild($CPF);
                    endif;
                    $seg->insertBefore($seg->appendChild($infResp), $nApol);

                    break;

                case "infSeg":
                    $infSeg = $dom->createElement("infSeg");
                    $xSeg = $dom->createElement("xSeg", $dados[1]);
                    $infSeg->appendChild($xSeg);

                    if (!empty($dados[2])) :
                        $CNPJ = $dom->createElement("CNPJ", $dados[2]);
                        $infSeg->appendChild($CNPJ);
                    endif;
                    
                    if (!empty($dados[3])) :
                        $CPF= $dom->createElement("CPF", $dados[3]);
                        $infSeg->appendChild($CPF);
                    endif;

                    $seg->insertBefore($seg->appendChild($infSeg), $nApol);
//                    $seg->appendChild($infSeg);
                    break;
                case "tot":
// infMDFe => tot|qCTe|qNFe|qMDFe|vCarga|cUnid|qCarga
                    $tot = $dom->createElement("tot");

                    if ($dados[1] != '') :
                        $qCTe = $dom->createElement("qCTe", $dados[1]);
                        $tot->appendChild($qCTe);
                    endif;

                    if ($dados[2] != '0') :
                        $qNFe = $dom->createElement("qNFe", $dados[2]);
                        $tot->appendChild($qNFe);
                    endif;

                    if ($dados[3] != '') :
                        $qMDFe = $dom->createElement("qMDFe", $dados[3]);
                        $tot->appendChild($qMDFe);
                    endif;

                    $vCarga = $dom->createElement("vCarga", $dados[4]);
                    $tot->appendChild($vCarga);
                    $cUnid = $dom->createElement("cUnid", $dados[5]);
                    $tot->appendChild($cUnid);
                    $qCarga = $dom->createElement("qCarga", number_format($dados[6], 4, ".", ""));
                    $tot->appendChild($qCarga);

                    $infMDFe->appendChild($tot);
                    break;
            } //end switch
        } //end for
        $arquivos_xml = array();
        foreach ($notas as $nota) {
            unset($dom, $MDFe, $infMDFe);
            $dom = $nota['dom'];
            $MDFe = $nota['MDFe'];
            $infMDFe = $nota['infMDFe'];
//$this->chave = $nota['chave'];
//$this->tpAmb = $nota['tpAmb'];
            $this->xml = '';
//salva o xml na variável se o txt não estiver em branco
            if (!empty($infMDFe)) {
                $MDFe->appendChild($infMDFe);
                $dom->appendChild($MDFe);
                $this->montaChaveXML($dom);
                $xml = $dom->saveXML();
                $this->xml = $dom->saveXML();
                $xml = str_replace('<?xml version="1.0" encoding="UTF-8  standalone="no"?>', '<?xml version="1.0" encoding="UTF-8"?>', $xml);
//remove linefeed, carriage return, tabs e multiplos espaços
                $xml = preg_replace('/\s\s+/', ' ', $xml);
                $xml = str_replace("> <", "><", $xml);
                $arquivos_xml[] = $xml;
                unset($xml);
            }
        }
        return($arquivos_xml);
    }

//end function

    /**
     * __limpaString
     * Remove todos dos caracteres especiais do texto e os acentos
     * preservando apenas letras de A-Z numeros de 0-9 e os caracteres @ , - ; : / _
     * @param   string $texto string a ser limpa
     * @return  string Texto sem caractere especiais
     */
    private function __limpaString($texto) {
        $aFind = array('&', 'á', 'à', 'ã', 'â', 'é', 'ê', 'í', 'ó', 'ô', 'õ', 'ú', 'ü', 'ç', '�?', 'À', 'Ã', 'Â', 'É', 'Ê', '�?', 'Ó', 'Ô', 'Õ', 'Ú', 'Ü', 'Ç');
        $aSubs = array('e', 'a', 'a', 'a', 'a', 'e', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'c', 'A', 'A', 'A', 'A', 'E', 'E', 'I', 'O', 'O', 'O', 'U', 'U', 'C');
        $novoTexto = str_replace($aFind, $aSubs, $texto);
        $novoTexto = preg_replace("/[^a-zA-Z0-9 @,-.;:\/_]/", "", $novoTexto);
        return $novoTexto;
    }

//fim limpaString

    /**
     * calculaDV
     * Função para o calculo o digito verificador da chave da MDFe
     * 
     * @name calculaDV
     * @param string $chave43
     * @return string 
     */
    private function calculaDV($chave43) {
        $multiplicadores = array(2, 3, 4, 5, 6, 7, 8, 9);
        $i = 42;
        $soma_ponderada = 0;
        while ($i >= 0) {
            for ($m = 0; $m < count($multiplicadores) && $i >= 0; $m++) {
                $soma_ponderada += $chave43[$i] * $multiplicadores[$m];
                $i--;
            }
        }
        $resto = $soma_ponderada % 11;
        if ($resto == '0' || $resto == '1') {
            $cDV = 0;
        } else {
            $cDV = 11 - $resto;
        }
        return $cDV;
    }

//fim calculaDV

    /**
     * montaChaveXML
     * Monta a chave da MDFe de 44 digitos com base em seus dados
     * Isso é útil no caso da chave formada no txt estar errada
     * 
     * @name montaChaveXML
     * @param object $dom 
     */
    private function montaChaveXML($dom) {
        $ide = $dom->getElementsByTagName("ide")->item(0);
        $emit = $dom->getElementsByTagName("emit")->item(0);
        $cUF = $ide->getElementsByTagName('cUF')->item(0)->nodeValue;
        $dEmi = $ide->getElementsByTagName('dhEmi')->item(0)->nodeValue;
        $CNPJ = $emit->getElementsByTagName('CNPJ')->item(0)->nodeValue;
        $mod = $ide->getElementsByTagName('mod')->item(0)->nodeValue;
        $serie = $ide->getElementsByTagName('serie')->item(0)->nodeValue;
        $nMDF = $ide->getElementsByTagName('nMDF')->item(0)->nodeValue;
        $tpEmis = $this->tpEmis; // $ide->getElementsByTagName('tpEmis')->item(0)->nodeValue;
        $cMDF = $ide->getElementsByTagName('cMDF')->item(0)->nodeValue;
        if (strlen($cMDF) != 8) {
            $cMDF = $ide->getElementsByTagName('cMDF')->item(0)->nodeValue = rand(10000001, 99999999);
        }
        $tmpData = explode("T", $dEmi);
        $tempData = $dt = explode("-", $tmpData[0]);
        $forma = "%02d%02d%02d%s%02d%03d%09d%01d%08d";
        $tempChave = sprintf($forma, $cUF, $tempData[0] - 2000, $tempData[1], $CNPJ, $mod, $serie, $nMDF, $tpEmis, $cMDF);
        $cDV = $ide->getElementsByTagName('cDV')->item(0)->nodeValue = $this->calculaDV($tempChave);
        $this->chave = $tempChave .= $cDV;
        $infMDFe = $dom->getElementsByTagName("infMDFe")->item(0);
        $infMDFe->setAttribute("Id", "MDFe" . $this->chave);
        $infMDFe->setAttribute("versao", '3.00');
    }

//fim calculaChave
}

//fim da classe
