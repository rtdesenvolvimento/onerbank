function formatarNumero( obj, mask ){
    if ( mask == null ) {
        mask = "0.00";
    }
    if ( mask.indexOf(".") > -1 ) {
        if ( mask != null && mask != ""   ) {
            var maskArray = mask.split(".");
            mask = maskArray[1];
        } else {
            mask = "00";
        }
        if ( obj.value != "" ){
            if ( eNumerico( obj.value ) ){
                if ( obj.valorMinimo != null ){
                    if ( paraNumerico( obj.valorMinimo ) > paraNumerico( obj.value ) ){
                        alert("Informe um valor maior ou igual a " + obj.valorMinimo + ". ");
                        obj.focus();
                        return false;
                    }
                }
                if ( obj.value.indexOf(",") > 0 ){
                    lstValor = obj.value.split(",");
                    if ( lstValor[1].length == 0 ){
                        obj.value += mask;
                    } else if ( lstValor[1].length > 0 ) {
                        for (var i = 1; i <= lstValor[1].length; i++  ) {
                            if ( lstValor[1].length == i ) {
                                obj.value += mask.substring(i, mask.length);
                            }
                        }
                    }
                }
                else{
                    obj.value += ","+ mask;
                    if ( obj.quatroDigitos == "1" ) obj.value += mask;
                }
            }
            else{
                alert("Informe um valor numerico.");
                obj.value = ""
                obj.focus();
                return false;
            }
        }
        else{
            obj.value = "0," + mask;
        }
    } else {
        formatarInteiro(obj, mask);
    }
    return true;
}
function eNumerico( valor ){
    if ( typeof valor == "number" ||  valor.replace(",",".") * 0 == 0 ){
        return true;
    }
    else{
        return false;
    }
}
function formatarInteiro( objP, mask  ) {
    var obj = null;
    if(typeof objP == "object"){
        obj = objP.value;
    }else{
        obj = objP;
    }
    if ( obj != null && obj != "" ) {
        if ( eNumerico( obj ) ) {
            for ( var i = 1; i <= obj.length; i++) {
                if ( obj.length == i  ) {
                    obj = mask.substring(i, mask.length) + obj;
                }
            }
        } else {
            obj = "";
        }
    } else {
        obj = mask;
    }
    if(typeof objP == "object"){
        objP.value = obj;
    }
    return obj;
}
function paraNumerico( valor ){
    if (eNumerico( valor )){
        return valor.replace(",",".") * 1;
    }
    else{
        return 0.00;
    }
}

function getDouble(valor) {

    if (valor === undefined) return 0;
    if (valor === '') return 0;

    valor = valor.replace(',','.');
    valor = parseFloat(valor);

    if (isNaN(valor)) valor = 0;

    return valor;
}