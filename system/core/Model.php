<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		log_message('debug', "Model Class Initialized");
	}

	/**
	 * __get
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

    function __toArray() {
        return get_object_vars($this);
    }

    function __toModel($array, $object) {
        foreach ($array as $k=> $v) $object->{$k} = $v;

        return $object;
    }

    /*
    function __toModelAll($arrayItens, $object) {
	    foreach ($arrayItens as $item) {
            $this->__toModel($item, $object);
        }
    }
    */

    function __save($table, $data)
    {
        $data['filial_id'] = $this->session->userdata('filial_id');
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return $this->db->insert_id($table);
        }
        return FALSE;
    }

    function __update($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }
        return FALSE;
    }


    function __delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }
        return FALSE;
    }

    function __getById($table, $id){
        $this->db->where('id'.$table,$id);
        $this->db->limit(1);
        return $this->db->get($table)->row();
    }

    function __exist($table, $id) {
        $this->db->where('id'.$table,$id);
        $this->db->limit(1);
        return count($this->db->get($table)->row()) > 0;
    }

    public function __getByIdToModel($table, $id, $object) {
        return $this->__toModel($this->__getById($table, $id), $object);
    }

    public function __getAll($table)
    {
        $this->db->from($table);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->get()->result();
    }

    public function __getAllItens($table, $fatherField, $fatherId) {
        $this->db->from($table);
        $this->db->where($fatherField, $fatherId);
        $this->db->where('filial_id', $this->session->userdata('filial_id'));

        return $this->db->get()->result();
    }

    function __count($table)
    {
        $this->db->where('filial_id', $this->session->userdata('filial_id'));
        return $this->db->count_all($table);
    }

}
// END Model Class

/* End of file Model.php */
/* Location: ./system/core/Model.php */